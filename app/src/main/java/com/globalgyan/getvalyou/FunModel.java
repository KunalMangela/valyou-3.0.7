package com.globalgyan.getvalyou;

/**
 * Created by Globalgyan on 20-02-2018.
 */

public class FunModel {
    String smname,landingpage_img,bgcolor,bgimg,dc_bg_img,dc_bg_color,bgsound,sm_icon,id,companyname,sm_icon_img,gif,desc,gif_load_time,titlecolor,titledescriptioncolor,group;

    int status_id;
    String lg_id,course_id,certification_id,learning_group_name,course_name,certification_name,temp_status,extra_param,sm_type,sm_articulate_url;

    public String getSm_articulate_url() {
        return sm_articulate_url;
    }

    public void setSm_articulate_url(String sm_articulate_url) {
        this.sm_articulate_url = sm_articulate_url;
    }

    public String getSm_type() {
        return sm_type;
    }

    public void setSm_type(String sm_type) {
        this.sm_type = sm_type;
    }

    public String getTitlecolor() {
        return titlecolor;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getStatus() {
        return status_id;
    }

    public void setStatus(int status) {
        this.status_id = status;
    }

    public String getExtra_param() {
        return extra_param;
    }

    public void setExtra_param(String extra_param) {
        this.extra_param = extra_param;
    }

    public String getLg_id() {
        return lg_id;
    }

    public String getTemp_status() {
        return temp_status;
    }

    public void setTemp_status(String temp_status) {
        this.temp_status = temp_status;
    }

    public void setLg_id(String lg_id) {
        this.lg_id = lg_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getCertification_id() {
        return certification_id;
    }

    public void setCertification_id(String certification_id) {
        this.certification_id = certification_id;
    }

    public String getLearning_group_name() {
        return learning_group_name;
    }

    public void setLearning_group_name(String learning_group_name) {
        this.learning_group_name = learning_group_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCertification_name() {
        return certification_name;
    }

    public void setCertification_name(String certification_name) {
        this.certification_name = certification_name;
    }

    public void setTitlecolor(String titlecolor) {
        this.titlecolor = titlecolor;
    }

    public String getTitledescriptioncolor() {
        return titledescriptioncolor;
    }

    public void setTitledescriptioncolor(String titledescriptioncolor) {
        this.titledescriptioncolor = titledescriptioncolor;
    }

    public String getGif_load_time() {
        return gif_load_time;
    }

    public void setGif_load_time(String gif_load_time) {
        this.gif_load_time = gif_load_time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getGif() {
        return gif;
    }

    public void setGif(String gif) {
        this.gif = gif;
    }

    public String getSmname() {
        return smname;
    }

    public void setSmname(String smname) {
        this.smname = smname;
    }

    public String getDc_bg_img() {
        return dc_bg_img;
    }

    public void setDc_bg_img(String dc_bg_img) {
        this.dc_bg_img = dc_bg_img;
    }


    public String getDc_bg_color() {
        return dc_bg_color;
    }

    public void setDc_bg_color(String dc_bg_color) {
        this.dc_bg_color = dc_bg_color;
    }

    public String getLandingpage_img() {
        return landingpage_img;
    }

    public void setLandingpage_img(String landingpage_img) {
        this.landingpage_img = landingpage_img;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }

    public String getBgimg() {
        return bgimg;
    }

    public void setBgimg(String bgimg) {
        this.bgimg = bgimg;
    }

    public String getBgsound() {
        return bgsound;
    }

    public void setBgsound(String bgsound) {
        this.bgsound = bgsound;
    }

    public String getSm_icon() {
        return sm_icon;
    }

    public void setSm_icon(String sm_icon) {
        this.sm_icon = sm_icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getSm_icon_img() {
        return sm_icon_img;
    }

    public void setSm_icon_img(String sm_icon_img) {
        this.sm_icon_img = sm_icon_img;
    }
}
