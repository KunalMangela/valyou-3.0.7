package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.iambedant.text.OutlineTextView;

public class FirstAssessmentCompletedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_assessment_completed);
        OutlineTextView heading=(OutlineTextView)findViewById(R.id.heading);
      /*  Typeface typeface3 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        heading.setTypeface(typeface3,Typeface.BOLD);
*/

    }

    public void next(View view){
        Intent intent = new Intent(FirstAssessmentCompletedActivity.this, ScoresActivity.class);
        // intent.putExtra("tabs", "normal");

        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
    }
}
