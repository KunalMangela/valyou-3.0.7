package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.cms.response.ChanegePasswordResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.utils.ValidationUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

/**
 * Created by NaNi on 10/09/17.
 */

public class ResetPasswordActivity extends AppCompatActivity implements GUICallback {

    EditText txtConfirmPassword = null;
    EditText txtPassword = null;
    private String userId = null;
    Dialog kProgressHUD;
    AppCompatButton reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmpass);

        userId=getIntent().getStringExtra("id");
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        reset=(AppCompatButton)findViewById(R.id.reset);
        txtPassword=(EditText)findViewById(R.id.input_password);
        txtConfirmPassword=(EditText)findViewById(R.id.input_password_confirm);

        reset.setTypeface(typeface1);

        txtPassword.setTypeface(typeface1);
        txtConfirmPassword.setTypeface(typeface1);

//        kProgressHUD = KProgressHUD.create(ResetPasswordActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        kProgressHUD = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        kProgressHUD.requestWindowFeature(Window.FEATURE_NO_TITLE);
        kProgressHUD.setContentView(R.layout.planet_loader);
        Window window1 = kProgressHUD.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        kProgressHUD.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        kProgressHUD.setCancelable(false);
        GifView gifView2 = (GifView) kProgressHUD.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        reset.setEnabled(true);
                    }
                }.start();

                if(txtPassword.getText().toString().length()>=6){
                    if(txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString())){
                        if(ValidationUtils.passwordValid(txtPassword, ResetPasswordActivity.this)){
                            kProgressHUD.show();
                            new CountDownTimer(1500, 1500) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {

                                    Log.e("vl",""+ValidationUtils.passwordValid(txtPassword, ResetPasswordActivity.this));
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ResetPasswordActivity.this,ResetPasswordActivity.this);
                                    processor.changePassword(userId,txtConfirmPassword.getText().toString());

                                }
                            }.start();
                        }else{
                            Toast.makeText(ResetPasswordActivity.this, "Password mismatched", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(ResetPasswordActivity.this, "Password mismatched", Toast.LENGTH_LONG).show();
                    }


                }
                else if(txtPassword.getText().toString().length()==0){
                    Toast.makeText(ResetPasswordActivity.this, "Please enter the password", Toast.LENGTH_LONG).show();

                }
                else{
                    Toast.makeText(ResetPasswordActivity.this, "Password must be of minimum 6 digits", Toast.LENGTH_LONG).show();
                }

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#06030E"));
        }
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(kProgressHUD.isShowing()&&kProgressHUD!=null){
            kProgressHUD.dismiss();
        }
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {
                if (guiResponse instanceof ChanegePasswordResponse) {
                    ChanegePasswordResponse response = (ChanegePasswordResponse) guiResponse;
                    if (response.isStatus()) {
                        Toast.makeText(ResetPasswordActivity.this, "Your password changed successfully", Toast.LENGTH_LONG).show();
                       //do changes
                        Intent intent = new Intent(ResetPasswordActivity.this, Welcome.class);
                        intent.putExtra("gotologinpage","loginpage");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    } else {
                        Toast.makeText(ResetPasswordActivity.this, "Failed to change your password", Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //do changes
        Intent forgot = new Intent(this, Welcome.class);
        forgot.putExtra("gotologinpage","loginpage");
        //forgot.putExtra("SIGNUP","YES");
        startActivity(forgot);
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

        finish();

    }
}
