package com.globalgyan.getvalyou;

public class ToadysgameModel {
    String name,subname,tg_id,available_date,lock_status,status,type_is,idis,lg_id,course_id,certification_id,url_ic,learning_group_name,course_name,certification_name,temp_status,extra_param,sm_type,sm_articulate_url;
    int status_id,pic_frequency;
    String ass_uid,ass_gdid,ass_category,ass_game,ass_groupid,ass_group,ass_descgame,ass_gametime;
    String sm_landingpage_img,sm_bgcolor,sm_bg_img,sm_bgsound,sm_icon,sm_dc_bgcolor,sm_dc_bgimg,sm_company,sm_ic_img,sm_gif,sm_desc,sm_giftime,sm_titlecolor,sm_title_desc_color;
    public ToadysgameModel() {
    }

    public String getSm_type() {
        return sm_type;
    }

    public void setSm_type(String sm_type) {
        this.sm_type = sm_type;
    }

    public String getSm_articulate_url() {
        return sm_articulate_url;
    }

    public void setSm_articulate_url(String sm_articulate_url) {
        this.sm_articulate_url = sm_articulate_url;
    }

    public String getAss_uid() {
        return ass_uid;
    }

    public String getUrl_ic() {
        return url_ic;
    }

    public void setUrl_ic(String url_ic) {
        this.url_ic = url_ic;
    }

    public int getPic_frequency() {
        return pic_frequency;
    }

    public void setPic_frequency(int pic_frequency) {
        this.pic_frequency = pic_frequency;
    }

    public String getAss_group() {
        return ass_group;
    }

    public void setAss_group(String ass_group) {
        this.ass_group = ass_group;
    }

    public String getSm_company() {
        return sm_company;
    }

    public void setSm_company(String sm_company) {
        this.sm_company = sm_company;
    }

    public void setAss_uid(String ass_uid) {
        this.ass_uid = ass_uid;
    }

    public String getAss_gdid() {
        return ass_gdid;
    }

    public String getSm_icon() {
        return sm_icon;
    }

    public String getSm_title_desc_color() {
        return sm_title_desc_color;
    }

    public void setSm_title_desc_color(String sm_title_desc_color) {
        this.sm_title_desc_color = sm_title_desc_color;
    }

    public void setSm_icon(String sm_icon) {
        this.sm_icon = sm_icon;
    }

    public void setAss_gdid(String ass_gdid) {
        this.ass_gdid = ass_gdid;
    }

    public String getAss_category() {
        return ass_category;
    }

    public void setAss_category(String ass_category) {
        this.ass_category = ass_category;
    }

    public String getAss_game() {
        return ass_game;
    }

    public void setAss_game(String ass_game) {
        this.ass_game = ass_game;
    }

    public String getAss_groupid() {
        return ass_groupid;
    }

    public void setAss_groupid(String ass_groupid) {
        this.ass_groupid = ass_groupid;
    }

    public String getAss_descgame() {
        return ass_descgame;
    }

    public void setAss_descgame(String ass_descgame) {
        this.ass_descgame = ass_descgame;
    }

    public String getAss_gametime() {
        return ass_gametime;
    }

    public void setAss_gametime(String ass_gametime) {
        this.ass_gametime = ass_gametime;
    }

    public String getSm_landingpage_img() {
        return sm_landingpage_img;
    }

    public void setSm_landingpage_img(String sm_landingpage_img) {
        this.sm_landingpage_img = sm_landingpage_img;
    }

    public String getSm_bgcolor() {
        return sm_bgcolor;
    }

    public void setSm_bgcolor(String sm_bgcolor) {
        this.sm_bgcolor = sm_bgcolor;
    }

    public String getSm_bg_img() {
        return sm_bg_img;
    }

    public void setSm_bg_img(String sm_bg_img) {
        this.sm_bg_img = sm_bg_img;
    }

    public String getSm_bgsound() {
        return sm_bgsound;
    }

    public void setSm_bgsound(String sm_bgsound) {
        this.sm_bgsound = sm_bgsound;
    }



    public String getSm_dc_bgcolor() {
        return sm_dc_bgcolor;
    }

    public void setSm_dc_bgcolor(String sm_dc_bgcolor) {
        this.sm_dc_bgcolor = sm_dc_bgcolor;
    }

    public String getSm_dc_bgimg() {
        return sm_dc_bgimg;
    }

    public void setSm_dc_bgimg(String sm_dc_bgimg) {
        this.sm_dc_bgimg = sm_dc_bgimg;
    }

    public String getSm_ic_img() {
        return sm_ic_img;
    }

    public void setSm_ic_img(String sm_ic_img) {
        this.sm_ic_img = sm_ic_img;
    }

    public String getSm_gif() {
        return sm_gif;
    }

    public void setSm_gif(String sm_gif) {
        this.sm_gif = sm_gif;
    }

    public String getSm_desc() {
        return sm_desc;
    }

    public void setSm_desc(String sm_desc) {
        this.sm_desc = sm_desc;
    }

    public String getSm_giftime() {
        return sm_giftime;
    }

    public void setSm_giftime(String sm_giftime) {
        this.sm_giftime = sm_giftime;
    }

    public String getSm_titlecolor() {
        return sm_titlecolor;
    }

    public void setSm_titlecolor(String sm_titlecolor) {
        this.sm_titlecolor = sm_titlecolor;
    }



    public String getAvailable_date() {
        return available_date;
    }

    public void setAvailable_date(String available_date) {
        this.available_date = available_date;
    }

    public String getTg_id() {
        return tg_id;
    }

    public String getLock_status() {
        return lock_status;
    }

    public void setLock_status(String lock_status) {
        this.lock_status = lock_status;
    }

    public void setTg_id(String tg_id) {
        this.tg_id = tg_id;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getExtra_param() {
        return extra_param;
    }

    public void setExtra_param(String extra_param) {
        this.extra_param = extra_param;
    }

    public String getIdis() {
        return idis;
    }

    public String getTemp_status() {
        return temp_status;
    }

    public void setTemp_status(String temp_status) {
        this.temp_status = temp_status;
    }

    public String getLg_id() {
        return lg_id;
    }

    public void setLg_id(String lg_id) {
        this.lg_id = lg_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getCertification_id() {
        return certification_id;
    }

    public void setCertification_id(String certification_id) {
        this.certification_id = certification_id;
    }

    public String getLearning_group_name() {
        return learning_group_name;
    }

    public void setLearning_group_name(String learning_group_name) {
        this.learning_group_name = learning_group_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCertification_name() {
        return certification_name;
    }

    public void setCertification_name(String certification_name) {
        this.certification_name = certification_name;
    }

    public void setIdis(String idis) {
        this.idis = idis;
    }

    public String getType_is() {
        return type_is;
    }

    public void setType_is(String type_is) {
        this.type_is = type_is;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }
}
