package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.globalgyan.getvalyou.apphelper.PrefManager;

public class AssesmentSelectionActivity extends AppCompatActivity {
    Animation animation_fade1,fade2,fade3;
    TextView sim,ass,fun;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assesment_selection);
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        sim=(TextView)findViewById(R.id.sim_banner);
        ass=(TextView)findViewById(R.id.ass_banner);
        fun=(TextView)findViewById(R.id.fungame_banner);


        sim.setTypeface(typeface1);
        ass.setTypeface(typeface1);
        fun.setTypeface(typeface1);



        animation_fade1 =
                AnimationUtils.loadAnimation(AssesmentSelectionActivity.this,
                        R.anim.fadeing_out_animation_planet);
        fade2 =
                AnimationUtils.loadAnimation(AssesmentSelectionActivity.this,
                        R.anim.fadeing_out_animation_planet);
        fade3 =
                AnimationUtils.loadAnimation(AssesmentSelectionActivity.this,
                        R.anim.fadeing_out_animation_planet);


        ass.startAnimation(animation_fade1);
        new CountDownTimer(1000, 200) {
            @Override
            public void onTick(long millisUntilFinished) {
                ass.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                sim.startAnimation(fade2);
                // assessmentplanet.setVisibility(View.VISIBLE);
                new CountDownTimer(1000, 200) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        sim.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFinish() {
                        fun.startAnimation(fade3);
                        //  learningplanet.setVisibility(View.VISIBLE);
                        new CountDownTimer(1000, 200) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                fun.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onFinish() {

                            }
                        }.start();

                    }
                }.start();

            }
        }.start();



    }
    public void assesmentClick(View view){
        new PrefManager(AssesmentSelectionActivity.this).saveAsstype("Ass");
        Intent intent = new Intent(AssesmentSelectionActivity.this, HomeActivity.class);
        intent.putExtra("tabs", "normal");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    public void simulationClick(View view){
        new PrefManager(AssesmentSelectionActivity.this).saveAsstype("sim");
        Intent intent = new Intent(AssesmentSelectionActivity.this, HomeActivity.class);
        intent.putExtra("tabs", "normal");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }



    public void fungamesClick(View view){
        new PrefManager(AssesmentSelectionActivity.this).saveAsstype("fun");
        Intent intent = new Intent(AssesmentSelectionActivity.this, HomeActivity.class);
        intent.putExtra("tabs", "normal");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
