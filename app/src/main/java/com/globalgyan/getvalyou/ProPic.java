package com.globalgyan.getvalyou;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.ListeningGame.MAQ;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.CompressorPic;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.UploadResponse;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import de.morrox.fontinator.FontTextView;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static cz.msebera.android.httpclient.protocol.HTTP.USER_AGENT;

/**
 * Created by NaNi on 19/10/17.
 */

public class ProPic extends AppCompatActivity implements UploadStatusDelegate{

    public static UUID mFaceId1;
    RelativeLayout probg;
    boolean wtf;
    boolean kya;
    PrefManager prefManager;
    File f;
    FaceServiceClient faceServiceClient;
    String fromwhere="";
    AppCompatButton tk,sl,ok,ca;
    TextView status_detection;
    Uri selectedImage;
    Uri fileUri_cam,fileUri_gallary;
    String camerafile_path,imgpath_gallary;
    ConnectionUtils connectionUtils;
    Bitmap thumbnail;
    boolean flag_netcheck=false;
    Dialog progress_profile;
    ImageView rotate;
    boolean facedetected = false;
    FontTextView rotatepc;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.propic);
         probg=(RelativeLayout)findViewById(R.id.probg);
        tk=(AppCompatButton)findViewById(R.id.takepic);
        sl=(AppCompatButton)findViewById(R.id.selecpic);
        ok=(AppCompatButton)findViewById(R.id.picok);
        ca=(AppCompatButton)findViewById(R.id.picno);

        rotate = (ImageView)findViewById(R.id.rotatepic);
        rotatepc = (FontTextView)findViewById(R.id.rotate);
       // rotate.setEnabled(false);

       Typeface tf1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
       tk.setTypeface(tf1);
       sl.setTypeface(tf1);
       ok.setTypeface(tf1);
       ca.setTypeface(tf1);
        ok.setEnabled(true);
        status_detection=(TextView)findViewById(R.id.status_detection);
        connectionUtils=new ConnectionUtils(ProPic.this);
        prefManager= new PrefManager(ProPic.this);
//        progress_profile = KProgressHUD.create(ProPic.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setDimAmount(0.7f)
//                .setCancellable(false);



        progress_profile = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        progress_profile.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress_profile.setContentView(R.layout.planet_loader);
        Window window1 = progress_profile.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        progress_profile.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progress_profile.setCancelable(false);
        GifView gifView2 = (GifView) progress_profile.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();


        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        //DataBaseHelper dataBaseHelper = new DataBaseHelper(ProPic.this);
        //long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
//        if(dataBaseHelper.KEY_PROFILE_IMAGE_URL != null){
//            probg.set
//        }

        fromwhere=getIntent().getStringExtra("fromwhere");
        sl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                startActivityForResult(pickIntent, 2);
            }
        });

        tk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                status_detection.setVisibility(View.GONE);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri_cam = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri_cam);
                // start the image capture Intent
                startActivityForResult(intent, 1);



                /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
               f = new File("/storage/emulated/0/Movies/valYou/", "temp.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, 1);*/


            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(connectionUtils.isConnectionAvailable()){

                    status_detection.setText("Detecting face...");
                    status_detection.setVisibility(View.VISIBLE);



                    progress_profile.show();
                    final CheckInternetTask t918=new CheckInternetTask();
                    t918.execute();

                    new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            t918.cancel(true);

                            if (flag_netcheck) {
                                flag_netcheck = false;

                                detectImageTask(thumbnail);

                                if (!facedetected) {

                                    try {
                                        if(!progress_profile.isShowing()&&progress_profile!=null) {
                                            progress_profile.dismiss();
                                            status_detection.setText("No face detected");
                                        }

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }

                                }
                                else {
                                    /*if (kya) {
                                        try {
                                            persistImage(thumbnail, fileUri_cam.getPath());
                                            ///data/user/0/com.globalgyan.getvalyou/cache/images/IMG_20171123_173023.jpg
                                            // File compressd = new File("/storage/emulated/0/Movies/valYou/", "templll.jpg");
                                            f = new CompressorPic(ProPic.this).compressToFile(new File(fileUri_cam.getPath()));
                                            String candidateid = PreferenceUtils.getCandidateId(ProPic.this);
                                            Log.e("CANKALBLB", candidateid);
                                            MultipartUploadRequest request = new MultipartUploadRequest(ProPic.this, AppConstant.BASE_URL + "user/" + candidateid + "/candidateProfilePicUpload");
                                            request.addFileToUpload(f.getAbsolutePath(), "profilePic");
                                            Log.e("pathcam", f.getAbsolutePath());
                                            // request.setNotificationConfig(getNotificationConfig("PROFILE PIC"));
                                            request.setCustomUserAgent(USER_AGENT);
                                            request.setUtf8Charset();
                                            request.setMethod("POST");
                                            request.setAutoDeleteFilesAfterSuccessfulUpload(false);
                                            request.setUsesFixedLengthStreamingMode(true);
                                            request.setMaxRetries(3);
                                            request.setUtf8Charset();
                                            String upp = request.setDelegate(ProPic.this).startUpload();
                                            Log.e("lcbslbs", "");


                                        } catch (FileNotFoundException exc) {

                                        } catch (IllegalArgumentException exc) {

                                        } catch (MalformedURLException exc) {

                                        } catch (IOException ee) {

                                        }
                                    } else {
                                        try {
                                            persistImage(thumbnail, imgpath_gallary);

                                            f = new CompressorPic(ProPic.this).compressToFile(new File(imgpath_gallary));
                                            String candidateid = PreferenceUtils.getCandidateId(ProPic.this);
                                            Log.e("CANKALBLB", candidateid);
                                            MultipartUploadRequest request = new MultipartUploadRequest(ProPic.this, AppConstant.BASE_URL + "user/" + candidateid + "/candidateProfilePicUpload");
                                            request.addFileToUpload(f.getAbsolutePath(), "profilePic");
                                            Log.e("pathcam", f.getAbsolutePath());
                                            //  request.setNotificationConfig(getNotificationConfig("PROFILE PIC"));
                                            request.setCustomUserAgent(USER_AGENT);
                                            request.setUtf8Charset();
                                            request.setMethod("POST");
                                            request.setAutoDeleteFilesAfterSuccessfulUpload(false);
                                            request.setUsesFixedLengthStreamingMode(true);
                                            request.setMaxRetries(3);
                                            request.setUtf8Charset();
                                            String uppo = request.setDelegate(ProPic.this).startUpload();
                                            Log.e("lcbslbs", "");


                                        } catch (FileNotFoundException exc) {
                                            exc.printStackTrace();
                                        } catch (IllegalArgumentException exc) {
                                            exc.printStackTrace();
                                        } catch (MalformedURLException exc) {
                                            exc.printStackTrace();
                                        } catch (IOException ee) {
                                            ee.printStackTrace();
                                        }
                                    }


                                    //Intent it = new Intent(ProPic.this,ProfileActivity.class);
                                    // it.putExtra("tabs","normal");
                                    // startActivity(it);
                                    try {
                                        progress_profile.dismiss();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
*/
                                }
                            }else {
                                progress_profile.dismiss();
                                flag_netcheck=false;
                                Toast.makeText(ProPic.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.start();






                }else {
                    Toast.makeText(ProPic.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }

            }
        });

        ca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ok.setVisibility(View.GONE);
                tk.setVisibility(View.VISIBLE);
              //  sl.setVisibility(View.VISIBLE);
                ca.setVisibility(View.GONE);
            }
        });

        tk.performClick();
    }


    public void upload_pic(){

        if (kya) {
            try {
                persistImage(thumbnail, fileUri_cam.getPath());
                ///data/user/0/com.globalgyan.getvalyou/cache/images/IMG_20171123_173023.jpg
                // File compressd = new File("/storage/emulated/0/Movies/valYou/", "templll.jpg");
                f = new CompressorPic(ProPic.this).compressToFile(new File(fileUri_cam.getPath()));
                String candidateid = PreferenceUtils.getCandidateId(ProPic.this);
                Log.e("CANKALBLB", candidateid);
                MultipartUploadRequest request = new MultipartUploadRequest(ProPic.this, AppConstant.BASE_URL + "user/" + candidateid + "/candidateProfilePicUpload");
                request.addFileToUpload(f.getAbsolutePath(), "profilePic");
                Log.e("pathcam", f.getAbsolutePath());
                // request.setNotificationConfig(getNotificationConfig("PROFILE PIC"));
                request.setCustomUserAgent(USER_AGENT);
                request.setUtf8Charset();
                request.setMethod("POST");
                request.setAutoDeleteFilesAfterSuccessfulUpload(false);
                request.setUsesFixedLengthStreamingMode(true);
                request.setMaxRetries(3);
                request.setUtf8Charset();
                String upp = request.setDelegate(ProPic.this).startUpload();
                Log.e("lcbslbs", "");


            } catch (FileNotFoundException exc) {

            } catch (IllegalArgumentException exc) {

            } catch (MalformedURLException exc) {

            } catch (IOException ee) {

            }
        } else {
            try {
                persistImage(thumbnail, imgpath_gallary);

                f = new CompressorPic(ProPic.this).compressToFile(new File(imgpath_gallary));
                String candidateid = PreferenceUtils.getCandidateId(ProPic.this);
                Log.e("CANKALBLB", candidateid);
                MultipartUploadRequest request = new MultipartUploadRequest(ProPic.this, AppConstant.BASE_URL + "user/" + candidateid + "/candidateProfilePicUpload");
                request.addFileToUpload(f.getAbsolutePath(), "profilePic");
                Log.e("pathcam", f.getAbsolutePath());
                //  request.setNotificationConfig(getNotificationConfig("PROFILE PIC"));
                request.setCustomUserAgent(USER_AGENT);
                request.setUtf8Charset();
                request.setMethod("POST");
                request.setAutoDeleteFilesAfterSuccessfulUpload(false);
                request.setUsesFixedLengthStreamingMode(true);
                request.setMaxRetries(3);
                request.setUtf8Charset();
                String uppo = request.setDelegate(ProPic.this).startUpload();
                Log.e("lcbslbs", "");


            } catch (FileNotFoundException exc) {
                exc.printStackTrace();
            } catch (IllegalArgumentException exc) {
                exc.printStackTrace();
            } catch (MalformedURLException exc) {
                exc.printStackTrace();
            } catch (IOException ee) {
                ee.printStackTrace();
            }
        }


        //Intent it = new Intent(ProPic.this,ProfileActivity.class);
        // it.putExtra("tabs","normal");
        // startActivity(it);
        try {
            ok.setEnabled(false);
            progress_profile.dismiss();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private UploadNotificationConfig getNotificationConfig(String filename) {


        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_upload)
                .setCompletedIcon(R.drawable.ic_upload_success)
                .setErrorIcon(R.drawable.ic_upload_error)
                .setTitle(filename)
                .setInProgressMessage("uploading")
                .setCompletedMessage("upload_success")
                .setErrorMessage("upload_error")
                .setAutoClearOnSuccess(false)
                .setClearOnAction(true)
                .setRingToneEnabled(true);
    }


    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {

        Log.e("Progress", String.format(Locale.getDefault(), "ID: %1$s (%2$d%%) at %3$.2f Kbit/s",
                uploadInfo.getUploadId(), uploadInfo.getProgressPercent(),
                uploadInfo.getUploadRate()));


    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, Exception exception) {
        Log.e("onError", "Error with ID: " + uploadInfo.getUploadId() + ": "
                + exception.getLocalizedMessage(), exception);
    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {

        if(kya){
            try {
                Bitmap thumbnail = (BitmapFactory.decodeFile(camerafile_path));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                prefManager.savePic(encodedImage);

                if(fromwhere.equalsIgnoreCase("home")) {

                    Intent intent = new Intent(ProPic.this, MainPlanetActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();
                }

                else   if(fromwhere.equalsIgnoreCase("assesslanding")) {

          /*          Intent intent = new Intent(ProPic.this, AssessLanding.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);*/
                    finish();
                }


                else{

                    Intent intent = new Intent(ProPic.this, ProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else {
            try {
                Bitmap thumbnail = (BitmapFactory.decodeFile(imgpath_gallary));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                Log.e("ENCID",encodedImage);
                prefManager.savePic(encodedImage);
                if(fromwhere.equalsIgnoreCase("home")) {

                    Intent intent = new Intent(ProPic.this, MainPlanetActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();
                }
                else if(fromwhere.equalsIgnoreCase("assesslanding")) {

/*                    Intent intent = new Intent(ProPic.this, AssessLanding.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
*/
                    finish();
                    ProPic.this.overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }

                else{

                    Intent intent = new Intent(ProPic.this, ProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        Log.e("completeded", String.format(Locale.getDefault(),
                "ID %1$s: completed in %2$ds at %3$.2f Kbit/s. Response code: %4$d, body:[%5$s]",
                uploadInfo.getUploadId(), uploadInfo.getElapsedTime() / 1000,
                uploadInfo.getUploadRate(), serverResponse.getHttpCode(),
                serverResponse.getBodyAsString()));
        Intent brodcast_receiver_screen=new Intent("Set pic");
        sendBroadcast(brodcast_receiver_screen);
        try {
            UploadResponse uploadResponse = new Gson().fromJson(serverResponse.getBodyAsString(), UploadResponse.class);
            if ( uploadResponse != null ) {
                DataBaseHelper dataBaseHelper = new DataBaseHelper(ProPic.this);
                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                if ( id > 0 ) {
                    if ( !TextUtils.isEmpty(uploadResponse.getProfilePicId()) ) {
                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, uploadResponse.getProfilePicId());

                    }
                }


            }
        } catch (Exception ex) {
            Log.e("Exception", "" + ex.getMessage());
        }

    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {
        Log.e("cancelled", "Upload with ID " + uploadInfo.getUploadId() + " is cancelled");
    }

    private class DetectionTask extends AsyncTask<InputStream, String, Face[]> {
        // Index indicates detecting in which of the two images.
        private int mIndex;
        private boolean mSucceed = true;




        @Override
        protected Face[] doInBackground(InputStream... params) {
            // Get an instance of face service client to detect faces in image.
            FaceServiceClient faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points,new PrefManager(ProPic.this).get_frkey());
            try{
                publishProgress("Detecting...");

                // Start detection.
                Face[] res= faceServiceClient.detect(
                        params[0],  /* Input stream of image to detect */
                        true,       /* Whether to return face ID */
                        false,       /* Whether to return face landmarks */
                        /* Which face attributes to analyze, currently we support:
                           age,gender,headPose,smile,facialHair */
                        null);
                if(res!=null){
                    wtf=true;
                }else wtf=false;

                return res;
            }  catch (Exception e) {
                mSucceed = false;
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(String... progress) {
            Log.e("PROGRESS",progress[0]);

        }

        @Override
        protected void onPostExecute(Face[] result) {
            Log.e("WTF",wtf+"");
            Log.e("FCID",result.length+""+wtf);
            if(result.length==1){
                ok.setVisibility(View.VISIBLE);
            }else{

            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                kya=true;
                /*File f = new File("/storage/emulated/0/Movies/valYou/");
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;

                        break;
                    }
                }*/

                String pathfile = fileUri_cam.toString();
                Log.v("paths", pathfile);
                String[] msg = pathfile.split("/");
                for (int i = 0; i < msg.length; i++) {
                    Log.v("paths :" + i + "", msg[i]);
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 3; i < msg.length; i++) {
                    sb.append("/");
                    sb.append(msg[i].toString());
                }
                String sbuilder = sb.toString();
                sb.setLength(0);
                //sb.append("[");
                sb.append(sbuilder);
                camerafile_path=sb.toString();
                //sb.append("]");
                //count++;

                try {
                     thumbnail = (BitmapFactory.decodeFile(sb.toString()));
                   //  detectImageTask(thumbnail);
                    Drawable dr = new BitmapDrawable(thumbnail);
                    probg.setBackground(dr);
//                    ok.setVisibility(View.VISIBLE);
//                    ca.setVisibility(View.VISIBLE);
                   // tk.setVisibility(View.GONE);
                    tk.setVisibility(View.VISIBLE);

                    sl.setVisibility(View.GONE);


                } catch (Exception e) {
                    e.printStackTrace();
                }


              /*  ok.setVisibility(View.VISIBLE);
                ca.setVisibility(View.VISIBLE);
                tk.setVisibility(View.GONE);
                sl.setVisibility(View.GONE);
*/
            } else if (requestCode == 2)
            {
                kya=false;
                /*selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();*/
                 fileUri_gallary = data.getData();
                imgpath_gallary = getRealPathFromURI(fileUri_gallary);
                 thumbnail = (BitmapFactory.decodeFile(imgpath_gallary));
                Log.e("path", imgpath_gallary+"");
                Drawable dr = new BitmapDrawable(thumbnail);
                probg.setBackground(dr);


            }
        }
    }

    private void detectImageTask(Bitmap thumbnail) {




        rotate.setEnabled(false);
        rotatepc.setEnabled(false);


        //  ok.setVisibility(View.GONE);
        //ca.setVisibility(View.GONE);
        //tk.setEnabled(false);
        faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(ProPic.this).get_frkey());
        detectpic(thumbnail);

    }
    private void detectpic(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {


                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            facedetected = false;
                            try {
                                if(progress_profile.isShowing()&&progress_profile!=null) {
                                    progress_profile.dismiss();
                                    status_detection.setText("No face detected");
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


                            //    Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                           // Log.e("facedetection","No face detected");
                            rotate.setEnabled(true);
                            rotatepc.setEnabled(true);
                            ok.setEnabled(true);
                        //    ok.setVisibility(View.GONE);
                            ca.setVisibility(View.GONE);
                            tk.setVisibility(View.VISIBLE);
                            tk.setEnabled(true);
                         //   sl.setVisibility(View.VISIBLE);
                        }

                        else if(result != null && result.length>1){
                            facedetected = false;
                            try {
                                if(progress_profile.isShowing()&&progress_profile!=null) {
                                    progress_profile.dismiss();
                                    status_detection.setText("No face detected");
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


                            rotate.setEnabled(true);
                            rotatepc.setEnabled(true);
                            ok.setEnabled(true);
                       //     ok.setVisibility(View.GONE);
                            ca.setVisibility(View.GONE);
                            tk.setVisibility(View.VISIBLE);
                            tk.setEnabled(true);
                        }

                        else {

                        //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","face detected");

                            try {


                                List<Face> faces;

                                assert result != null;
                                faces = Arrays.asList(result);
                                for (Face face : faces) {
                                    mFaceId1 = face.faceId;
                                    String faceid = mFaceId1.toString();

                                    prefManager.setprofilefaceid(faceid);
                                    prefManager.detectionDone(true);
                                    //txt.setText(faceid);
                                    // Log.e("fid",mFaceId1+"  "+mface);
                                }
                                status_detection.setText("Face detected");
                                facedetected = true;
                                upload_pic();
                                rotate.setEnabled(true);
                                rotatepc.setEnabled(true);
                                tk.setEnabled(true);
                                ok.setVisibility(View.VISIBLE);
                                ca.setVisibility(View.GONE);
                                tk.setVisibility(View.GONE);
                                sl.setVisibility(View.GONE);
                            }

                            catch (Exception e){
                                e.printStackTrace();
                                try {
                                    if(progress_profile.isShowing()&&progress_profile!=null) {
                                        progress_profile.dismiss();
                                        status_detection.setText("No face detected");
                                        facedetected = false;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }



                            }
                        }



                    }
                };
        detectTask.execute(inputStream);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                AppConstant.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Fail", "Oops! Failed create "
                        + AppConstant.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File("/storage/emulated/0/DCIM/Camera" + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }
    private String getRealPathFromURI(Uri selectedImageURI) {
        String result;
        Cursor cursor = getContentResolver().query(selectedImageURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = selectedImageURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    public void rotatepic(View view){
        if(thumbnail!=null) {
            thumbnail=rotateBitmap(thumbnail, -90);
            Drawable dr = new BitmapDrawable(thumbnail);
            probg.setBackground(dr);
            ok.setVisibility(View.VISIBLE);
            ca.setVisibility(View.GONE);
            tk.setVisibility(View.VISIBLE);
            sl.setVisibility(View.GONE);
          //  detectImageTask(thumbnail);

        }else {
            Toast.makeText(this,"Please select an image",Toast.LENGTH_SHORT).show();
        }

    }

    public static Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    public  void persistImage(Bitmap bitmap, String name) {
        File filesDir = ProPic.this.getFilesDir();
        File imageFile = new File(name);

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e("Propic", "Error writing bitmap", e);
        }
    }
    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)ProPic.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

    @Override
    public void onBackPressed() {
        /*if(fromwhere.equalsIgnoreCase("profile")){
            Intent intent = new Intent(ProPic.this, ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            finish();
        }else {
            Intent intent = new Intent(ProPic.this, MainPlanetActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            finish();
        }*/


        if(fromwhere.equalsIgnoreCase("home")) {
         /*   new PrefManager(ProPic.this).profile_skiped(true);

            Intent intent = new Intent(ProPic.this, MainPlanetActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
*/
            finish();
            ProPic.this.overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

        }

        else   if(fromwhere.equalsIgnoreCase("assesslanding")) {

            Intent intent = new Intent(ProPic.this, HomeActivity.class);
            intent.putExtra("tabs", "normal");

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            finish();
        }


        else{

            Intent intent = new Intent(ProPic.this, ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            finish();
        }

    }
}
