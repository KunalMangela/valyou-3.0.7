package com.globalgyan.getvalyou;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.AssessFragment;
import com.globalgyan.getvalyou.simulation.SimulationStartPageActivity;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Globalgyan on 06-12-2017.
 */

public class SimulationAdapter extends RecyclerView.Adapter<SimulationAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<FunModel> fungame;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView title,lower_label;
        LinearLayout bg;


        public MyViewHolder(View view) {
            super(view);

            title = (FontTextView) view.findViewById(R.id.title);
            lower_label = (FontTextView) view.findViewById(R.id.lower_label);
            bg=(LinearLayout) view.findViewById(R.id.main_bg);

        }
    }


    public SimulationAdapter(Context mContext, ArrayList<FunModel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_list_items, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final FunModel funModel=fungame.get(position);
       // holder.bg.setBackgroundResource(R.drawable.wordwaterfallbanner);

            holder.lower_label.setText(funModel.getCompanyname());
            holder.title.setText(funModel.getSmname());

        holder.bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.bg.setEnabled(false);
                new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        holder.bg.setEnabled(true);
                    }
                }.start();
                ConnectionUtils connectionUtils=new ConnectionUtils(mContext);
                if(connectionUtils.isConnectionAvailable()) {
                    if (new PrefManager(mContext).getRetryresponseString().equalsIgnoreCase("notdone")) {
                        Toast.makeText(mContext, "Video uploading is in progrss,Can't access game right now", Toast.LENGTH_SHORT).show();

                    } else {
                                Intent i = new Intent(mContext, SimulationStartPageActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.putExtra("sm_name",funModel.getSmname());
                                i.putExtra("sm_landing_page_image",funModel.getLandingpage_img());
                                i.putExtra("sm_bg_color",funModel.getBgcolor());
                                i.putExtra("sm_bg_image",funModel.getBgimg());
                                i.putExtra("sm_decision_bg_color",funModel.getDc_bg_color());
                                i.putExtra("sm_decision_bg_image",funModel.getDc_bg_img());
                                i.putExtra("sm_bg_sound",funModel.getBgsound());
                                i.putExtra("sm_icon",funModel.getSm_icon());
                                i.putExtra("sm_id",funModel.getId());
                                i.putExtra("sm_company",funModel.getCompanyname());
                                i.putExtra("sm_icon_image",funModel.getSm_icon_img());
                                i.putExtra("gif_file",funModel.getGif());
                                i.putExtra("sm_description",funModel.getDesc());
                                i.putExtra("sm_gif_loading_time",funModel.getGif_load_time());
                                i.putExtra("sm_title_color",funModel.getTitlecolor());
                                i.putExtra("sm_title_description_color",funModel.getTitledescriptioncolor());
                                mContext.startActivity(i);

                    }
                }else {
                    Toast.makeText(mContext,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return fungame.size();
    }
}

