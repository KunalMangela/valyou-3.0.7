package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.Performance;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.NotificationListModel;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by techniche-android on 17/1/17.
 */

@SuppressWarnings("ALL")
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Context context;
    private List<NotificationListModel> lls = null;
    private DataBaseHelper dataBaseHelper = null;
    private FragmentManager fragmentManager = null;
    private GUICallback guiCallback = null;
    String monthname,date_suffix;
    int [] icons={R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check,
            R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check,
            R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check,
            R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check,
            R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check,
            R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check,
            R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check,
            R.drawable.ntf_camera,R.drawable.ntf_game,R.drawable.ntf_calender,R.drawable.ntf_check};


    public NotificationAdapter(Context context,List<NotificationListModel> me) {
        this.context = context;
        this.lls=me;



    }
    public NotificationAdapter() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_card, null);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotificationListModel notificationListModel=lls.get(position);
//        final Notifications notifications = notificationses.get(position);
        dataBaseHelper  = new DataBaseHelper(context);
        Log.e("refresh","rfresh");
        holder.title.setText(notificationListModel.getType());
        holder.description.setText(notificationListModel.getMessage());
        String [] datearray=notificationListModel.getCreated_at().split("T");
        String [] actualdate=datearray[0].split("-");
        String date=actualdate[2];
        String month=actualdate[1];
        String year=actualdate[0];

        switchcase(month);
        switchcasedate_suffix(String.valueOf(date.charAt(date.length() - 1)));
        holder.date.setText(date+date_suffix+" "+monthname);
       /* if(new PrefManager(context).getvideoFrom().equalsIgnoreCase("contribution")) {
            holder.progress.setVisibility(View.INVISIBLE);
            holder.progressvalue.setVisibility(View.INVISIBLE);
        }else {

        }*/
        if(notificationListModel.getType().equalsIgnoreCase("game")){
            holder.notificationImage.setImageResource(R.drawable.ntf_game);
            holder.status.setVisibility(View.INVISIBLE);
            holder.pr_linearLayout.setVisibility(View.INVISIBLE);

        }else {
            holder.notificationImage.setImageResource(R.drawable.ntf_camera);
            holder.progress.setProgressColor(Color.parseColor("#0130BC"));
            holder.progress.setProgressBackgroundColor(Color.parseColor("#ffd1d2d4"));
            holder.progress.setMax(100);
            try {
                String status=dataBaseHelper.getUploadedVideoID(notificationListModel.getObj_id()
                );
                //String status = dataBaseHelper.getVideoFromTableWithId(DataBaseHelper.TABLE_UPLOAD_VIDEO_RESPONSE, DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_STATUS, notificationListModel.getId());
                if (status.equalsIgnoreCase("true")) {
                    Log.e("liststats", "uploaded");
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setBackgroundResource(R.drawable.uploaded);

                } else {
                    Log.e("liststats", "not_uploaded");
                    //holder.status.setVisibility(View.INVISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setBackgroundResource(R.drawable.red_dot);

                }
            }catch (Exception e){
                e.printStackTrace();
                holder.status.setVisibility(View.VISIBLE);
                holder.status.setBackgroundResource(R.drawable.red_dot);

            }
            Log.e("refresh", String.valueOf(new PrefManager(context).getRetryresponseString()));
            try {
                if (new PrefManager(context).getRetryresponseString().equalsIgnoreCase("notdone")) {
                    if (notificationListModel.getObj_id().equals(new PrefManager(context).getVideolistIndex())) {
                        holder.pr_linearLayout.setVisibility(View.VISIBLE);
                        holder.progress.setProgress(HomeActivity.pr);
                        holder.progressvalue.setText(HomeActivity.pr+"%");
                        Log.e("refresh", String.valueOf(HomeActivity.pr+"%"));

                        Log.e("refresh", String.valueOf(new PrefManager(context).getVideolistIndex()));
                       // holder.status.setVisibility(View.INVISIBLE);
                        holder.status.setVisibility(View.VISIBLE);
                        holder.status.setBackgroundResource(R.drawable.red_dot);

                    } else {
                        holder.pr_linearLayout.setVisibility(View.INVISIBLE);
                    }

                } else {
                    holder.pr_linearLayout.setVisibility(View.INVISIBLE);
                    Log.e("refresh", String.valueOf(HomeActivity.pr+"%"));
                }
            }catch (Exception e){
                e.printStackTrace();
                Log.e("refresh", String.valueOf("Exc"));
            }

        }

        holder.intc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.intc.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        holder.intc.setEnabled(true);
                    }
                }.start();

                new PrefManager(context).setvideoFrom("videoresponse");
                if (notificationListModel.getType().equalsIgnoreCase("game")) {
                    new HomeActivity().mTabLayout.getTabAt(0).select();
                } else {
                    ConnectionUtils connectionUtils = new ConnectionUtils(context);
                    if (connectionUtils.isConnectionAvailable()) {
                        if (new PrefManager(context).getRetryresponseString().equalsIgnoreCase("notdone")) {
                            Toast.makeText(context, "Video upload is in progress,please try after video get uploaded", Toast.LENGTH_SHORT).show();
                            holder.intc.setEnabled(false);
                            new CountDownTimer(3000, 3000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    holder.intc.setEnabled(true);
                                }
                            }.start();
                        } else {
                           /* try {
                                new PrefManager(context).removeVideolistIndex();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/

                            try {
                                String status = dataBaseHelper.getUploadedVideoID(notificationListModel.getObj_id());
                                if (status.equalsIgnoreCase("true")) {
                                    Log.e("liststats", "uploaded");
                                    Toast.makeText(context, "Video response is already uploaded", Toast.LENGTH_SHORT).show();
                                } else {
                                 //   Performance.inVisiblebars();
                                    new PrefManager(context).saveVideolistIndex(notificationListModel.getObj_id());
                                    Intent intent = new Intent(context, VideoPerformanceActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("objectId", notificationListModel.getObj_id());
                                    context.startActivity(intent);
                                    ((Activity) context).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                new PrefManager(context).saveVideolistIndex(notificationListModel.getObj_id());
                                Intent intent = new Intent(context, VideoPerformanceActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("objectId", notificationListModel.getObj_id());
                                context.startActivity(intent);
                                ((Activity) context).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            }


                        }
                    } else {
                        Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                    }


                }




            }
        });


    }

    private void switchcasedate_suffix(String c) {
        switch (c){
            case  "0":date_suffix="th";
                break;
            case  "1":date_suffix="st";
                break;
            case  "2":date_suffix="nd";
                break;
            case  "3":date_suffix="rd";
                break;
            case  "4":date_suffix="th";
                break;
            case  "5":date_suffix="th";
                break;
            case  "6":date_suffix="th";
                break;
            case  "7":date_suffix="th";
                break;
            case  "8":date_suffix="th";
                break;
            case  "9":date_suffix="th";
                break;
        }
    }

    private void switchcase(String month) {
        switch (month){
            case "1":monthname="Jan";
                    break;
            case "2":monthname="Feb";
                break;
            case "3":monthname="Mar";
                break;
            case "4":monthname="Apr";
                break;
            case "5":monthname="May";
                break;
            case "6":monthname="Jun";
                break;
            case "7":monthname="Jul";
                break;
            case "8":monthname="Aug";
                break;
            case "9":monthname="Sep";
                break;
            case "10":monthname="Oct";
                break;
            case "01":monthname="Jan";
                break;
            case "02":monthname="Feb";
                break;
            case "03":monthname="Mar";
                break;
            case "04":monthname="Apr";
                break;
            case "05":monthname="May";
                break;
            case "06":monthname="Jun";
                break;
            case "07":monthname="Jul";
                break;
            case "08":monthname="Aug";
                break;
            case "09":monthname="Sep";
                break;
            case "11":monthname="Nov";
                break;
            case "12":monthname="Dec";
                break;
        }
    }


    @Override
    public int getItemCount() {
        return lls == null ? 0 : lls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FontTextView title,date;
        FontTextView description;
        FontTextView progressvalue;
        ImageView notificationImage;
        LinearLayout intc;
        RoundCornerProgressBar progress;
        ImageView status;
        LinearLayout pr_linearLayout,status_layout;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (FontTextView) itemView.findViewById(R.id.notititle);
            date = (FontTextView) itemView.findViewById(R.id.date);
            description = (FontTextView) itemView.findViewById(R.id.notidesc);
            progressvalue = (FontTextView) itemView.findViewById(R.id.pr_value);
            status = (ImageView) itemView.findViewById(R.id.status);
            notificationImage = (ImageView) itemView.findViewById(R.id.notiimage);
            intc=(LinearLayout)itemView.findViewById(R.id.intclick);
            progress = (RoundCornerProgressBar) itemView.findViewById(R.id.progress_1);
            pr_linearLayout=(LinearLayout)itemView.findViewById(R.id.pogresslayout);

        }
    }
    public void removeItem(int position) {
        lls.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, lls.size());
    }

}
