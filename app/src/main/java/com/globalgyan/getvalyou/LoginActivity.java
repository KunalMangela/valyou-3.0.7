package com.globalgyan.getvalyou;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.cms.response.LoginResponse;
import com.globalgyan.getvalyou.cms.response.SocialLoginResponse;
import com.globalgyan.getvalyou.cms.response.SocialRegisterResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Dates;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.delight.android.location.SimpleLocation;

//
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;


/**
 * Created by NaNi on 09/09/17.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,GUICallback,ActivityCompat.OnRequestPermissionsResultCallback{

        @BindView(R.id.input_email_tp)TextInputLayout emailtw;
        @BindView(R.id.input_password_tp)TextInputLayout passtw;
   // @BindView(R.id.input_email_signin)EditText emailed;
    //@BindView(R.id.input_password_signin)EditText passed;

    private static final int RC_SIGN_IN = 007;
    private String providerTypeStr = "";
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String email,pass;
    ConnectionUtils connectionUtils;
    AppCompatButton signin_btn;
//    CallbackManager callbackManager;
//    GraphRequest request;

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;

    private SimpleLocation location;
    KProgressHUD login_progrss;
    String latlong="";
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectionUtils=new ConnectionUtils(LoginActivity.this);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        signin_btn=(AppCompatButton)findViewById(R.id.signin);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //progress dialogue
        login_progrss = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Logging in")
                .setDimAmount(0.7f)
                .setCancellable(false);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();



        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(LoginActivity.this, LoginActivity.this);

    }





    @OnClick({R.id.signin, R.id.signin_signup, R.id.forgot})
    void whenClicked(View view){
        if(view.getId()==R.id.forgot){
            Intent forgot = new Intent(this, ForgotActivity.class);
            forgot.putExtra("SIGNUP","NO");
            startActivity(forgot);
        }

        if(view.getId()==R.id.signin_signup){
            Log.e("FUCK","ME");
            Intent forgot = new Intent(this, SignupActivity.class);
            //forgot.putExtra("SIGNUP","YES");
            startActivity(forgot);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }





        if(view.getId()==R.id.signin){
            hideKeyboard();

//            if (!Utils.validateEmail(emailtw.getEditText())) {
//                emailtw.setError("Not a valid email address!");
//
//            } else if (!Utils.matchMinLength(passtw.getEditText(), 6)) {
//                passtw.setError("Not a valid password!");
//            } else {

            if(emailtw.getEditText().getText().length()>6&&passtw.getEditText().getText().toString().length()>6) {


                if (connectionUtils.isConnectionAvailable()) {
                    signin_btn.setEnabled(false);
                    signin_btn.setBackgroundColor(Color.parseColor("#ABBAC2"));

                    Log.e("signinbtn", "clicked");
                    try {
                        new CountDownTimer(1500, 1500) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                signin_btn.setEnabled(true);
                                signin_btn.setBackgroundColor(Color.parseColor("#2f27c9"));
                            }
                        }.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    emailtw.setErrorEnabled(false);
                    passtw.setErrorEnabled(false);
                    // email=emailed.getText().toString();
                    // pass=passed.getText().toString();
                    login_progrss.show();
                    new CountDownTimer(2000, 2000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            RetrofitRequestProcessor processor = new RetrofitRequestProcessor(LoginActivity.this, LoginActivity.this);
                            processor.doLogin(emailtw.getEditText().getText().toString(), passtw.getEditText().getText().toString(), FirebaseInstanceId.getInstance().getToken(), "0", "ANDROID", getAndroidVersion(), getDevice());

                        }
                    }.start();
                } else {
                    signin_btn.setEnabled(true);
                    signin_btn.setBackgroundColor(Color.parseColor("#2f27c9"));
                    Toast.makeText(LoginActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(LoginActivity.this, "Please enter valid credentials !", Toast.LENGTH_SHORT).show();

            }

//            }
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       // callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if(connectionUtils.isConnectionAvailable()){
              /*  GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);*/
            }else {
                Toast.makeText(LoginActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

            }
        }
    }

/*
    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", acct.getId());
                jsonObject.put("email", acct.getEmail());
                jsonObject.put("name", acct.getDisplayName());
                AppConstant.socialData = jsonObject;
                Log.e("Google json", AppConstant.socialData + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("Photo", acct.getPhotoUrl() + "");
            String providerId = acct.getId();
            if (!TextUtils.isEmpty(providerId)) {

                RetrofitRequestProcessor processor = new RetrofitRequestProcessor(LoginActivity.this, LoginActivity.this);
                Log.e("ofdbfodsfoadnofbno",acct.getEmail());
                processor.doSocialSignIn(acct.getEmail(), PreferenceUtils.getFcmToken(LoginActivity.this));
            } else {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                intent.putExtra("providertype", providerTypeStr);
                startActivity(intent);
            }

        } else {
            Log.e("Login ", "failed");
        }
    }
*/


    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public String getAndroidVersion() {
        String release = android.os.Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.e("RELEASE",release);
        return release;
    }

    public String getDevice() {
        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

        return deviceName;
    }


    @Override
    public void requestProcessed(ValYouResponse
                                         guiResponse, RequestStatus status) {

        Log.e("signin","resp");
        try {
            if (login_progrss.isShowing() && login_progrss != null) {
                login_progrss.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if (guiResponse != null) {
            if (status.equals(RequestStatus.SUCCESS)) {

                if (guiResponse instanceof LoginResponse) {

                    LoginResponse loginResponse = (LoginResponse) guiResponse;
                    if (loginResponse != null) {
                        if (loginResponse.isStatus()) {

                            try {
                                FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                            } catch (Exception ex) {

                            }

                            DataBaseHelper dataBaseHelper = new DataBaseHelper(LoginActivity.this);
                            dataBaseHelper.deleteAllTables();
                            dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, loginResponse.getLoginModel().getDisplayName());

                            if (loginResponse.getLoginModel() != null && loginResponse.getLoginModel().isProfileComplete())
                                PreferenceUtils.setProfileDone(LoginActivity.this);
                            if (loginResponse.getLoginModel() != null) {
                                LoginModel loginModel = loginResponse.getLoginModel();
                                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                String dob = loginModel.getDateOfBirth();
                                if (!TextUtils.isEmpty(dob)) {
                                    try {
                                        //String date = DateUtils.UtcToJavaDateFormat(dob, new SimpleDateFormat("dd-MM-yyyy"));
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                    } catch (Exception ex) {

                                    }
                                }
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());

                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());

                                PreferenceUtils.setCandidateId(loginModel.get_id(), LoginActivity.this);

                              //  List<EducationalDetails> educationalDetailses = loginResponse.getLoginModel().getCurrentEducation();
                                List<Professional> professionals = loginResponse.getLoginModel().getProfessionalExperience();
                              /*  if (educationalDetailses != null) {
                                    for (EducationalDetails details : educationalDetailses) {
                                        String date = "";
                                        Dates start = details.getStartDate();

                                        if (start != null && start.getMonth() < 13) {
                                            int index = start.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(start.getYear());
                                        }

                                        details.setFrom(date);
                                        Dates end = details.getEndDate();

                                        if (end != null && end.getMonth() < 13) {
                                            int index = end.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(end.getYear());
                                        }
                                        details.setTo(date);
                                        dataBaseHelper.createEducation(details);
                                    }
                                }*/
                                if (professionals != null) {
                                    for (Professional details : professionals) {
                                        String date = "";
                                        Dates start = details.getStartDate();

                                        if (start != null && start.getMonth() < 13) {
                                            int index = start.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(start.getYear());
                                        }
                                        details.setFrom(date);

                                        Dates end = details.getEndDate();
                                        try {
                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                        } catch (Exception ex) {

                                        }
                                        details.setTo(date);
                                        dataBaseHelper.createProfessionlDetails(details);
                                    }
                                }

                                if (loginModel.isMobileVerified()) {
                                    PreferenceUtils.setVerified(LoginActivity.this);
                                    Log.e("ibbbp",loginModel.isMobileVerified()+""+loginModel.isProfileComplete());
                                    if (PreferenceUtils.isProfileDone(LoginActivity.this)) {
                                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                        intent.putExtra("tabs","normal");
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();

                                    }else {
                                        Intent intent = new Intent(LoginActivity.this, Activity_Myself.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {

                                    AppConstant.OTP_FROM_SIGNUP = true;
                                    Intent intent = new Intent(LoginActivity.this, EnterOtpActivity.class);
                                    intent.putExtra("phn", loginModel.getMobile());
                                    intent.putExtra("id", loginModel.get_id());
                                    intent.putExtra("session_id", " ");
                                    intent.putExtra("otpf","no");
                                    startActivity(intent);
                                    finish();

                                }


                            }

                        } else {
                            Toast.makeText(this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else
                        Toast.makeText(this, "INVALID USER DETAILS", Toast.LENGTH_SHORT).show();
                } else if (guiResponse instanceof SocialRegisterResponse) {
                    SocialRegisterResponse registerResponse = (SocialRegisterResponse) guiResponse;
                    if (registerResponse != null) {
                        if (registerResponse.isStatus()) {

                            try {
                                FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                            } catch (Exception ex) {

                            }

                            DataBaseHelper dataBaseHelper = new DataBaseHelper(LoginActivity.this);
                            dataBaseHelper.deleteAllTables();

                            if (registerResponse.getLoginModel() != null && registerResponse.getLoginModel().isProfileComplete())
                                PreferenceUtils.setProfileDone(LoginActivity.this);

                            String displayName = registerResponse.getLoginModel().getDisplayName();
                            if (TextUtils.isEmpty(displayName))
                                displayName = registerResponse.getLoginModel().getFirstName();
                            dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, displayName);
                            PreferenceUtils.setCandidateId(registerResponse.getLoginModel().get_id(), LoginActivity.this);
                            if (registerResponse.getLoginModel() != null) {
                                LoginModel loginModel = registerResponse.getLoginModel();
                                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                String dob = loginModel.getDateOfBirth();
                                if (!TextUtils.isEmpty(dob)) {
                                    try {
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                    } catch (Exception ex) {

                                    }
                                }
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());


                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());


                               // List<EducationalDetails> educationalDetailses = registerResponse.getLoginModel().getCurrentEducation();
                                List<Professional> professionals = registerResponse.getLoginModel().getProfessionalExperience();
                               /* if (educationalDetailses != null) {
                                    for (EducationalDetails details : educationalDetailses) {
                                        String date = "";
                                        Dates start = details.getStartDate();

                                        if (start != null && start.getMonth() < 13) {
                                            int index = start.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(start.getYear());
                                        }

                                        details.setFrom(date);
                                        Dates end = details.getEndDate();

                                        if (end != null && end.getMonth() < 13) {
                                            int index = end.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(end.getYear());
                                        }
                                        details.setTo(date);
                                        dataBaseHelper.createEducation(details);
                                    }
                                }*/
                                if (professionals != null) {
                                    for (Professional details : professionals) {
                                        String date = "";
                                        Dates start = details.getStartDate();

                                        if (start != null && start.getMonth() < 13) {
                                            int index = start.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(start.getYear());
                                        }
                                        details.setFrom(date);

                                        Dates end = details.getEndDate();
                                        try {
                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                        } catch (Exception ex) {

                                        }
                                        details.setTo(date);
                                        dataBaseHelper.createProfessionlDetails(details);
                                    }
                                }
                            }

                            if (PreferenceUtils.isProfileDone(LoginActivity.this)) {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.putExtra("tabs","normal");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Toast.makeText(this, "Username already exists", Toast.LENGTH_SHORT).show();
                        }
                    } else
                        Toast.makeText(this, "Server issuse", Toast.LENGTH_SHORT).show();


                } else if (guiResponse instanceof SocialLoginResponse) {
                    SocialLoginResponse loginResponse = (SocialLoginResponse) guiResponse;
                    if (loginResponse != null) {
                        if (loginResponse.isStatus()) {

                            try {
                                FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                            } catch (Exception ex) {

                            }

                            DataBaseHelper dataBaseHelper = new DataBaseHelper(LoginActivity.this);
                            dataBaseHelper.deleteAllTables();

                            if (loginResponse.getLoginModel() != null && loginResponse.getLoginModel().isProfileComplete())
                                PreferenceUtils.setProfileDone(LoginActivity.this);


                            String displayName = loginResponse.getLoginModel().getDisplayName();
                            if (TextUtils.isEmpty(displayName))
                                displayName = loginResponse.getLoginModel().getFirstName();
                            dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, displayName);
                            PreferenceUtils.setCandidateId(loginResponse.getLoginModel().get_id(), LoginActivity.this);

                            if (loginResponse.getLoginModel() != null) {
                                PreferenceUtils.setVerified(LoginActivity.this);
                                LoginModel loginModel = loginResponse.getLoginModel();
                                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                String dob = loginModel.getDateOfBirth();
                                if (!TextUtils.isEmpty(dob)) {
                                    try {

                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                    } catch (Exception ex) {

                                    }
                                }


                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());
                                if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());

                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());


                               // List<EducationalDetails> educationalDetailses = loginResponse.getLoginModel().getCurrentEducation();
                                List<Professional> professionals = loginResponse.getLoginModel().getProfessionalExperience();
                               /* if (educationalDetailses != null) {
                                    for (EducationalDetails details : educationalDetailses) {
                                        String date = "";
                                        Dates start = details.getStartDate();

                                        if (start != null && start.getMonth() < 13) {
                                            int index = start.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(start.getYear());
                                        }

                                        details.setFrom(date);
                                        Dates end = details.getEndDate();

                                        if (end != null && end.getMonth() < 13) {
                                            int index = end.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(end.getYear());
                                        }
                                        details.setTo(date);
                                        dataBaseHelper.createEducation(details);
                                    }
                                }*/
                                if (professionals != null) {
                                    for (Professional details : professionals) {
                                        String date = "";
                                        Dates start = details.getStartDate();

                                        if (start != null && start.getMonth() < 13) {
                                            int index = start.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(start.getYear());
                                        }
                                        details.setFrom(date);

                                        Dates end = details.getEndDate();
                                        try {
                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                        } catch (Exception ex) {

                                        }
                                        details.setTo(date);
                                        dataBaseHelper.createProfessionlDetails(details);
                                    }
                                }
                            }

                            if (PreferenceUtils.isProfileDone(LoginActivity.this)) {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.putExtra("tabs","normal");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Intent intent = new Intent(LoginActivity.this, SocialsignupActivity.class);
                            intent.putExtra("providertype", providerTypeStr);
                            startActivity(intent);
                            //  finish();
                        }
                    } else
                        Toast.makeText(this, "Server Pissue", Toast.LENGTH_SHORT).show();


                }

            } else lodin();
                //Toast.makeText(this, "Server PPissue", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, "NETWORK ISSUE", Toast.LENGTH_SHORT).show();
    }


    private void lodin(){
        Intent intent = new Intent(LoginActivity.this, SocialsignupActivity.class);
        intent.putExtra("type","social");
        intent.putExtra("providertype", providerTypeStr);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

    }
}
