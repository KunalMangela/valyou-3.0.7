package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.globalgyan.getvalyou.model.Organization;

import java.util.List;

/**
 * Created by techniche-v1 on 2/11/16.
 */

public class OrganisationAdapter extends RecyclerView.Adapter<OrganisationAdapter.ViewHolder> {
    List<Organization> industries = null;
    Context context = null;

    public String getStringBuffer() {
        return stringBuffer;
    }

    private String stringBuffer = null;

    private String selected = null;

    public OrganisationAdapter(List<Organization> langList, Context context, String selected) {
        this.industries = langList;
        this.context = context;
        this.selected = selected;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_language, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Organization industry = industries.get(position);
        holder.txtName.setText(TextUtils.isEmpty(industry.getName()) ? "" :industry.getName().toUpperCase());
        if(!TextUtils.isEmpty(selected) && industry.getName().equalsIgnoreCase(selected))
            holder.checkbox.setChecked(true);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if (holder.checkbox.isChecked()) {
                    stringBuffer = stringBuffer.replaceAll(holder.txtName.getText().toString() + " ", "");
                } else {*/
                    if (!TextUtils.isEmpty(stringBuffer))
                        stringBuffer = stringBuffer + holder.txtName.getText().toString() + " ";
                    else
                        stringBuffer = holder.txtName.getText().toString() + " ";

                    if (!TextUtils.isEmpty(stringBuffer)) {
                        Activity activity = (Activity) context;
                        Intent intentMessage = new Intent();
                        intentMessage.putExtra("INDUSTRIES", stringBuffer);
                        intentMessage.putExtra("id",industry.get_id());
                        activity.setResult(activity.RESULT_OK, intentMessage);

                        if (activity.getParent() == null) {
                            activity.setResult(activity.RESULT_OK, intentMessage);
                        } else {
                            activity.getParent().setResult(activity.RESULT_OK, intentMessage);
                        }
                        activity.finish();
                       // activity.overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }


                holder.checkbox.setChecked(!holder.checkbox.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return industries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName = null;
        TextView txtSubname = null;
        CheckBox checkbox = null;
        RelativeLayout layout = null;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtSubname = (TextView) itemView.findViewById(R.id.txtSubname);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);
        }
    }
}
