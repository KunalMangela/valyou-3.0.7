package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class LearningGroupCoursesAdapter extends RecyclerView.Adapter<LearningGroupCoursesAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<LearningGroupModel> fungame;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,progress,startdate,enddate,desc,desc_text_head,long_desc,to;
        LinearLayout main_bg_lg,desc_linear;
        ImageView learninggroup_cover,arrow_desc;
        RelativeLayout desc_relative;
        public MyViewHolder(View view) {
            super(view);

            name=(TextView)view.findViewById(R.id.txtname);
            main_bg_lg=(LinearLayout)view.findViewById(R.id.main_bg_lg);
            startdate=(TextView)view.findViewById(R.id.startd);
            enddate=(TextView)view.findViewById(R.id.endd);
            desc=(TextView)view.findViewById(R.id.long_desc);
            desc_text_head=(TextView)view.findViewById(R.id.desc_text);
            learninggroup_cover=(ImageView)view.findViewById(R.id.learninggroup_cover);
            desc_relative=(RelativeLayout)view.findViewById(R.id.desc_relative);
            desc_linear=(LinearLayout)view.findViewById(R.id.desc_linear);
            arrow_desc=(ImageView)view.findViewById(R.id.arrow_desc);
            long_desc=(TextView)view.findViewById(R.id.long_desc);
            to=(TextView)view.findViewById(R.id.to);
        }
    }


    public LearningGroupCoursesAdapter(Context mContext, ArrayList<LearningGroupModel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.get_learn_groups_in_courses_item, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LearningGroupCoursesAdapter.MyViewHolder holder, final int position) {
        final LearningGroupModel learningGroupModel=fungame.get(position);
        holder.name.setText(learningGroupModel.getName());
        holder.name.setSelected(true);

        Typeface typeface1 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.name.setTypeface(typeface1);
        holder.desc.setTypeface(typeface1);
        holder.startdate.setTypeface(typeface1,Typeface.BOLD);
        holder.enddate.setTypeface(typeface1,Typeface.BOLD);
        holder.desc_text_head.setTypeface(typeface1);
        holder.to.setTypeface(typeface1);
        SimpleDateFormat format = new SimpleDateFormat("yyyyy.MMMMM.dd GGG hh:mm aaa");
        try {
            Date date1 = format.parse(learningGroupModel.getStartdate().toString());
            Date date2 = format.parse(learningGroupModel.getEnddate().toString());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.long_desc.setText(learningGroupModel.getDesc());

        if(GetLearningGroupsActivity.desc_learning_group.get(position).equalsIgnoreCase("close")){
            holder.arrow_desc.setImageResource(R.drawable.ic_arrow_drop_down_white_24dp);
            holder.long_desc.setVisibility(View.GONE);
        }else {
            holder.arrow_desc.setImageResource(R.drawable.ic_arrow_drop_up_white_24dp);
            holder.long_desc.setVisibility(View.VISIBLE);

        }

        String datestartis=formattedDateFromString("yyyy-MM-dd hh:mm:ss","dd MMMyy hh:mmaaa",learningGroupModel.getStartdate().toString());
        String dateendis=formattedDateFromString("yyyy-MM-dd hh:mm:ss","dd MMMyy hh:mmaaa",learningGroupModel.getEnddate().toString());
        holder.startdate.setText(""+datestartis);
        holder.enddate.setText(dateendis);

        holder.desc.setText(learningGroupModel.getDesc());

        try {
            Glide.with(mContext).load(learningGroupModel.getLearning_group_icon())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.learninggroup_cover);
        }catch (Exception e){
            e.printStackTrace();
            Glide.with(mContext).load(R.drawable.round_feeback)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.learninggroup_cover);
        }




        holder.desc_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(GetLearningGroupsActivity.desc_learning_group.get(position).equalsIgnoreCase("close")){
                    holder.arrow_desc.setImageResource(R.drawable.ic_arrow_drop_up_white_24dp);
                    holder.long_desc.setVisibility(View.VISIBLE);
                    GetLearningGroupsActivity.desc_learning_group.set(position,"open");
                }else {
                    holder.arrow_desc.setImageResource(R.drawable.ic_arrow_drop_down_white_24dp);
                    holder.long_desc.setVisibility(View.GONE);
                    GetLearningGroupsActivity.desc_learning_group.set(position,"close");
                }

            }
        });







        holder.main_bg_lg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    holder.main_bg_lg.setBackgroundColor(Color.parseColor("#ffffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            holder.main_bg_lg.setBackgroundResource(R.drawable.rounded_corner_courselist_items);

                        }
                    }.start();

                }


                return false;
            }
        });


        holder.main_bg_lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionUtils connectionUtils=new ConnectionUtils(mContext);
                if(connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(mContext, CourseLIstActivity.class);
                    // intent.putExtra("tabs", "normal");
                    new PrefManager(mContext).saveLearningGroups(String.valueOf(learningGroupModel.getId()));
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).finish();
                        ((Activity) mContext).startActivity(intent);
                        ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }


                    Log.e("idis", String.valueOf(learningGroupModel.getId()));
                    //ActiveCoursesActivity.getProgress(learningGroupModel.getId());
                }else {
                    Toast.makeText(mContext,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return fungame.size();
    }

    public static void clickCourses(){

    }



    public static String formattedDateFromString(String inputFormat, String outputFormat, String inputDate){
        if(inputFormat.equals("")){ // if inputFormat = "", set a default input format.
            inputFormat = "yyyy-MM-dd hh:mm:ss";
        }
        if(outputFormat.equals("")){
            outputFormat = "EEEE d 'de' MMMM 'del' yyyy"; // if inputFormat = "", set a default output format.
        }
        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        // You can set a different Locale, This example set a locale of Country Mexico.
        //SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, new Locale("es", "MX"));
        //SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, new Locale("es", "MX"));

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            Log.e("formattedDateFromString", "Exception in formateDateFromstring(): " + e.getMessage());
        }
        return outputDate;

    }
}
