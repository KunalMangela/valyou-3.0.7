package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.cms.request.UpdateProfileRequest;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.customwidget.NumberPicker;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.ActionBarUpdator;
import com.globalgyan.getvalyou.interfaces.CalendarPicker;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import de.morrox.fontinator.FontCheckedTextView;
import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 15/10/17.
 */

public class Activity_Myself extends AppCompatActivity implements CalendarPicker, GUICallback {
    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {

    }

    Typeface tf1;
    String kya = "";
    // private DatePickerFragment dFragment = null;
    NumberPicker yearPicker;
    private TextView txtYear = null;
    private TextView txtIndustry = null;
    private AppCompatButton doneButtonView = null;
    private DataBaseHelper dataBaseHelper = null;
    private int INDUSTRY_PICKER_REQUEST_CODE = 111;
    private ActionBarUpdator actionBarUpdator = null;
    // private CalendarDatePickerDialogFragment cdp = null;
    private boolean isGender = false;
    private Calendar startDate = null;
    private Calendar endCalendar = null;
    private ImageView genderMaleImage = null;
    private TextView femaleLabel = null;
    private ImageView genderFemaleImage = null;
    Dialog dialog;
    String from="";
    ConnectionUtils connectionUtils;
    ImageView back;
    boolean flag_netcheck=false;
    Dialog progress_profile;


    String date, txtyear_txt;
    boolean flag_date_selected = false;
    String tag;

    boolean flag_gender_selected = false;
    String industries;
    private TextView maleLabel = null;
    View.OnClickListener genderSelectHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
             tag = (String) v.getTag();
             tag=tag.toLowerCase();
            if (!TextUtils.isEmpty(tag)) {
                if (tag.equalsIgnoreCase("Male")) {
                    genderMaleImage.setImageResource(R.drawable.male_on);
                    maleLabel.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
                    genderFemaleImage.setImageResource(R.drawable.female_off);
                    femaleLabel.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.text_gray));
                } else {
                    genderMaleImage.setImageResource(R.drawable.male_off);
                    maleLabel.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.text_gray));
                    genderFemaleImage.setImageResource(R.drawable.female_on);
                    femaleLabel.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
                }
               /* long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                if (id > 0) {
                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, tag);
                }*/
                isGender = true;
                SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(Activity_Myself.this).edit();
                prefEditor.putString("tag", tag);
                prefEditor.apply();

                if (isValidateFields()) {
                    doneButtonView.setVisibility(View.VISIBLE);
                }
            }

        }
    };
    private boolean isedit = false;

    public void setIsedit(boolean isedit) {
        this.isedit = isedit;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myself);
        connectionUtils = new ConnectionUtils(Activity_Myself.this);
        tf1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        createProgress();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
        back=(ImageView)findViewById(R.id.backarrow);
        try {
            isedit = getIntent().getExtras().getBoolean("forEdit");
        } catch (Exception ex) {

        }
        try{
            from=getIntent().getStringExtra("from");
        }catch (Exception e){
            from="";
        }
        try {
            if (from.equalsIgnoreCase("signup")) {
                back.setVisibility(View.INVISIBLE);
            } else {
                back.setVisibility(View.VISIBLE);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(from.equalsIgnoreCase("signup")){

                }else {
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);



                }
            }
        });
           genderMaleImage = (ImageView) findViewById(R.id.genderMaleImage);
        maleLabel = (TextView) findViewById(R.id.maleLabel);
        genderFemaleImage = (ImageView) findViewById(R.id.genderFemaleImage);
        femaleLabel = (TextView) findViewById(R.id.femaleLabel);

        femaleLabel.setTypeface(tf1);
        maleLabel.setTypeface(tf1);

        genderMaleImage.setTag("Male");
        maleLabel.setTag("Male");
        genderMaleImage.setOnClickListener(genderSelectHandler);
        maleLabel.setOnClickListener(genderSelectHandler);

        doneButtonView = (AppCompatButton) findViewById(R.id.personalnext);

        doneButtonView.setTypeface(tf1);

        genderFemaleImage.setTag("Female");
        femaleLabel.setTag("Female");
        genderFemaleImage.setOnClickListener(genderSelectHandler);
        femaleLabel.setOnClickListener(genderSelectHandler);
        dataBaseHelper = new DataBaseHelper(Activity_Myself.this);

        String genderSelected = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER);
        if (!TextUtils.isEmpty(genderSelected)) {
            if (genderSelected.equalsIgnoreCase("Male")) {
                genderMaleImage.setImageResource(R.drawable.male_on);
                maleLabel.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
                SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(Activity_Myself.this).edit();
                prefEditor.putString("tag", genderSelected);
                prefEditor.apply();

            } else {
                genderFemaleImage.setImageResource(R.drawable.female_on);
                femaleLabel.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
                SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(Activity_Myself.this).edit();
                prefEditor.putString("tag", genderSelected);
                prefEditor.apply();

            }
            isGender = true;

            if (isValidateFields()) {
                doneButtonView.setVisibility(View.VISIBLE);
            }
        }


        /////DOB Section


        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 15);
        //MonthAdapter.CalendarDay endDate = new MonthAdapter.CalendarDay(calendar);
        final Calendar endDate = calendar;
        calendar.set(Calendar.YEAR, 1970);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        final Calendar selectedCalendar = Calendar.getInstance();
        selectedCalendar.set(Calendar.YEAR, 1988);
        selectedCalendar.set(Calendar.MONTH, 11);
        selectedCalendar.set(Calendar.DAY_OF_MONTH, 15);

        endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.YEAR, 2012);
        endCalendar.set(Calendar.MONTH, 11);
        endCalendar.set(Calendar.DAY_OF_MONTH, 31);

        startDate = calendar;


        txtYear = (TextView) findViewById(R.id.txtYear);
        txtYear.setTypeface(tf1);
        txtIndustry = (TextView) findViewById(R.id.txtIndustry);
        txtIndustry.setTypeface(tf1);

        findViewById(R.id.dateOfYearLabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openDialogoue();

            }
        });
        txtYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   dFragment.show(ActivityMySelf.this.getSupportFragmentManager(), "Date Picker");
                openDialogoue();
            }
        });

        String dobSelectd = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB);
        if (!TextUtils.isEmpty(dobSelectd)) {
            txtYear.setText(dobSelectd);
            txtYear.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
            if (isValidateFields()) {
                doneButtonView.setVisibility(View.VISIBLE);
            }
        }

        findViewById(R.id.industryFocus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.industryFocus).setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        findViewById(R.id.industryFocus).setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(Activity_Myself.this, IndustryActivity.class);
                    startActivityForResult(intent, INDUSTRY_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);



                } else {
                    Toast.makeText(Activity_Myself.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }

            }
        });
        txtIndustry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtIndustry.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        txtIndustry.setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(Activity_Myself.this, IndustryActivity.class);
                    startActivityForResult(intent, INDUSTRY_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                } else {
                    Toast.makeText(Activity_Myself.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }

            }
        });

        String industry = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY);
        if (!TextUtils.isEmpty(industry)) {
            txtIndustry.setText(industry);
            txtIndustry.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
            if (isValidateFields()) {
                doneButtonView.setVisibility(View.VISIBLE);
            }
        }


        doneButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                doneButtonView.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        doneButtonView.setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {



                    progress_profile.show();
                    final CheckInternetTask t921=new CheckInternetTask();
                    t921.execute();

                    new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            t921.cancel(true);
                            progress_profile.dismiss();
                            if (flag_netcheck) {
                                flag_netcheck = false;


                                if (isValidateFields()) {


                                    dialog = new Dialog(Activity_Myself.this, android.R.style.Theme_Translucent_NoTitleBar);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.popup_domain);
//                    Window window = dialog.getWindow();
//                    WindowManager.LayoutParams wlp = window.getAttributes();
//
//                    wlp.gravity = Gravity.CENTER;
//                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//                    window.setAttributes(wlp);
//                    dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                                    dialog.show();
                                    final FontCheckedTextView stud = (FontCheckedTextView) dialog.findViewById(R.id.stud);


                                    final FontCheckedTextView prof = (FontCheckedTextView) dialog.findViewById(R.id.prof);


                                    stud.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            stud.setChecked(true);
                                            Boolean studChecked = stud.isChecked();
                                            Log.e("STUD", studChecked + "");
                                            if (studChecked) {
                                                Log.e("STUDT", studChecked + "");
                                                kya = "stud";
                                                stud.setCheckMarkDrawable(R.drawable.ic_check_normal_light);
                                                prof.setCheckMarkDrawable(R.drawable.ic_check_normal_dark);
                                            } else {
                                                Log.e("STUDF", studChecked + "");
                                            }
                                        }
                                    });

                                    prof.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            prof.setChecked(true);
                                            Boolean profChecked = prof.isChecked();
                                            Log.e("prof", profChecked + "");
                                            if (profChecked) {
                                                Log.e("proft", profChecked + "");
                                                kya = "prof";
                                                stud.setCheckMarkDrawable(R.drawable.ic_check_normal_dark);
                                                prof.setCheckMarkDrawable(R.drawable.ic_check_normal_light);
                                            } else {
                                                Log.e("proff", profChecked + "");
                                            }
                                        }
                                    });
                                    Button b = (Button) dialog.findViewById(R.id.submitclg);
                                    b.setTypeface(tf1);
                                    b.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //put birth date in database
                                            date = txtYear.getText().toString();

                                            long id1 = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                                            if (id1 > 0) {
                                                dataBaseHelper.updateTableDetails(String.valueOf(id1), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, date);
                                            }

                                            //put gender in database
                                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                            String mm = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER);
                                            String tag1="";
                                                tag1 = prefs.getString("tag", "");


                                            long id2 = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                                            if (id2 > 0) {
                                                dataBaseHelper.updateTableDetails(String.valueOf(id2), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, tag1);
                                            }

                                            //put functional area in database

                                            industries = txtIndustry.getText().toString();
                                            long id3 = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                                            if (id3 > 0) {
                                                dataBaseHelper.updateTableDetails(String.valueOf(id3), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, industries);
                                            }

                                            if (kya.length() > 2) {
                                                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                                                if (id > 0) {
                                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMPLOY_STATUS, "professional");
                                                }
                                                if (isedit) {


                                                    String candidateId = PreferenceUtils.getCandidateId(Activity_Myself.this);
                                                    if (!TextUtils.isEmpty(candidateId)) {
            /*UpdateProfileRequest request = new UpdateProfileRequest(candidateId, getActivity());
            new AsyncTaskExecutor<ValYouRequest, Void, ValYouResponse>().execute(
                    new RequestProcessorPut(getActivity(),
                            ProfileFragment.this, true), request);*/

                                                        UpdateProfileRequest request = new UpdateProfileRequest(candidateId, Activity_Myself.this);

                                                        JsonParser jsonParser = new JsonParser();
                                                        JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());

                                                        Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                                                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(Activity_Myself.this, Activity_Myself.this);
                                                        processor.updateProfile(candidateId, gsonObject);
                                                        finish();
                                                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);



                                                    }

                                                } else {


                                                    ArrayList<EducationalDetails> arrayList = dataBaseHelper.getEducationalDetails();
                                                    if (arrayList != null && arrayList.size() > 0) {
                                                        Intent intent = new Intent(Activity_Myself.this, ListEducationActivity.class);
                                                        intent.putExtra("kya", kya);
                                                        startActivity(intent);
                                                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                                    } else {

                                                        Intent intent = new Intent(Activity_Myself.this, EducationalDetailsActivity.class);
                                                        intent.putExtra("kya", kya);
                                                        startActivity(intent);
                                                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                                    }

                                                }


                                                dialog.dismiss();

                                            } else {
                                                Toast.makeText(Activity_Myself.this, "Please select option to proceed", Toast.LENGTH_SHORT).show();
                                            }


                                        }
                                    });


                                }else {
                                    Toast.makeText(Activity_Myself.this, "Please enter all the details", Toast.LENGTH_SHORT).show();

                                }


                            }else {

                                flag_netcheck=false;
                                Toast.makeText(Activity_Myself.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.start();


                } else {
                    Toast.makeText(Activity_Myself.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();

                }


            }
        });


    }

        public void createProgress() {
            progress_profile = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
            progress_profile.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progress_profile.setContentView(R.layout.planet_loader);
            Window window = progress_profile.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
            progress_profile.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



            GifView gifView1 = (GifView) progress_profile.findViewById(R.id.loader);
            gifView1.setVisibility(View.VISIBLE);
            gifView1.play();
            gifView1.setGifResource(R.raw.loader_planet);
            gifView1.getGifResource();
            progress_profile.setCancelable(false);
           // progress_profile.show();

        }

    private void openDialogoue() {
        final Dialog dialog = new Dialog(Activity_Myself.this, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dob_dialogue);
        yearPicker = (NumberPicker) dialog.findViewById(R.id.numberPicker);
        AppCompatButton btnYes = (AppCompatButton) dialog.findViewById(R.id.dobsave);
        FontTextView btnNo = (FontTextView) dialog.findViewById(R.id.btnCancel);

        btnNo.setTypeface(tf1);
        btnYes.setTypeface(tf1);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });



        int year = 1995;

        yearPicker.setMaxValue(year + 20);
        yearPicker.setMinValue(1930);
        yearPicker.setWrapSelectorWheel(false);
        yearPicker.setValue(year);
        yearPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtYear.setText(String.valueOf(yearPicker.getValue()));
                dialog.cancel();
                txtYear.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
//                 date = txtYear.getText().toString();

              /*  long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                if (id > 0) {
                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, date);
                }*/
            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private boolean isValidateFields() {
        boolean isValidate = true;
        try {

            if (!isGender) {
                isValidate = false;
                //Toast.makeText(FragmentMySelf.this.getContext(), "Please enter your Gender.", Toast.LENGTH_SHORT).show();
            } else if (txtYear.getText().toString().equalsIgnoreCase("Enter your year of birth")) {
                isValidate = false;
                //Toast.makeText(FragmentMySelf.this.getContext(), "Please enter your Date of Birth.", Toast.LENGTH_SHORT).show();
            } else if (txtIndustry.getText().toString().equalsIgnoreCase("Enter your industry focus")) {
                isValidate = false;
                //Toast.makeText(FragmentMySelf.this.getContext(), "Please enter an industry.", Toast.LENGTH_SHORT).show();
            }
            return isValidate;
        } catch (Exception ex) {
            return isValidate;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INDUSTRY_PICKER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                 industries = data.getStringExtra("INDUSTRIES");
                if (!TextUtils.isEmpty(industries)) {
                    Log.e("industry--", industries);
                    txtIndustry.setText(industries);
                    txtIndustry.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
                  /*  long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                    if (id > 0) {
                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, industries);
                    }*/
                    if (isValidateFields()) {
                        doneButtonView.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }


    @Override
    public void onDateSelected(int year, int month, int day) {
        txtYear.setText(String.valueOf(year));
        txtYear.setTextColor(ContextCompat.getColor(Activity_Myself.this, R.color.green_profile));
        String date = txtYear.getText().toString();
        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
        if (id > 0) {
            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, date);
        }
        Calendar selected = Calendar.getInstance();
        selected.set(Calendar.YEAR, year);
        selected.set(Calendar.MONTH, month - 1);
        selected.set(Calendar.DAY_OF_MONTH, day);


        if (isValidateFields()) {
            doneButtonView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        kya = "";
    }

    @Override
    public void onBackPressed() {

        if(from.equalsIgnoreCase("signup")){

        }else {
            super.onBackPressed();
        }
    }



    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)Activity_Myself.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

}
