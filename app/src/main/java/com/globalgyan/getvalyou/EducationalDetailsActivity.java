package com.globalgyan.getvalyou;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.globalgyan.getvalyou.cms.response.AddNewCollegeResponse;
import com.globalgyan.getvalyou.cms.response.AddNewDegreeResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.CalendarPicker;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 15/10/17.
 */

public class EducationalDetailsActivity extends AppCompatActivity implements GUICallback, CalendarPicker {

    private int COLLEGE_PICKER_REQUEST_CODE = 111;
    private int DEGREE_PICKER_REQUEST_CODE = 222;
    private TextView collegeName = null;
    private TextView degreeName = null;
    private int dateSelection = 0;
    private TextView startDateText = null;
    private RippleView doneButtonView = null;
    private TextView endDateText = null;
    private Button btnDone = null;
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    //date picker
    ConnectionUtils connectionUtils;
    long start_date_mili,end_date_mili;
    Calendar calendarE,calendar;
    private DatePicker datePicker = null;
    private DataBaseHelper dataBaseHelper = null;
    private DatePicker enddatePicker = null;
    private Button saveButton = null;
    private CardView cardView = null;
    private FontTextView pickerTitle = null, cancel = null;
    private RippleView saveView = null;
    private ImageView nextArrow = null, radio_button = null;
    String startDateString = "", endDateString = "";
    int count = 0;
    private EducationalDetails educationalDetails = null;
    LinearLayout select_linear = null;
    String kya;
    YearMonthPickerDialog yearMonthPickerDialog;
    String dateclicked="";
    public  boolean isclgcorrect=false,isdegreecorrect=false;
    Typeface typeface1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_educational);
         typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        connectionUtils = new ConnectionUtils(EducationalDetailsActivity.this);
         try {
            kya = getIntent().getExtras().getString("kya");
        } catch (Exception e) {

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }

        yearMonthPickerDialog = new YearMonthPickerDialog(this, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month) {

                if(dateclicked.equalsIgnoreCase("start")){
                    calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");
                    startDateText.setText(dateFormat.format(calendar.getTime()));
                    startDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
                  /*  long offset = calendar.get(Calendar.ZONE_OFFSET) +
                            calendar.get(Calendar.DST_OFFSET);

                    long sinceMidnight = (calendar.getTimeInMillis() + offset) %
                            (24 * 60 * 60 * 1000);*/
                  if(startDateText.getText().toString().equalsIgnoreCase("Month/Year")||endDateText.getText().toString().equalsIgnoreCase("Month/Year")){

                  }else {
                      calendarE = Calendar.getInstance();
                      if(calendarE.compareTo(calendar)==-1){
                          Toast.makeText(EducationalDetailsActivity.this,"Please choose valid dates",Toast.LENGTH_SHORT).show();
                          btnDone.setEnabled(false);
                          btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));

                      }else {
                          btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                          btnDone.setEnabled(true);
                      }
                  }

                Log.e("startDate",String.valueOf(start_date_mili));
                }else {
                     calendarE = Calendar.getInstance();
                    calendarE.set(Calendar.YEAR, year);
                    calendarE.set(Calendar.MONTH, month);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");
                    endDateText.setText(dateFormat.format(calendarE.getTime()));
                    endDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
                    /*long offset = calendar.get(Calendar.ZONE_OFFSET) +
                            calendar.get(Calendar.DST_OFFSET);

                    long sinceMidnight_end= (calendar.getTimeInMillis() + offset) %
                            (24 * 60 * 60 * 1000);*/

                    if(startDateText.getText().toString().equalsIgnoreCase("Month/Year")||endDateText.getText().toString().equalsIgnoreCase("Month/Year")){

                    }else {
                        if(calendarE.compareTo(calendar)==-1){
                            Toast.makeText(EducationalDetailsActivity.this,"Please choose valid dates",Toast.LENGTH_SHORT).show();
                            btnDone.setEnabled(false);
                            btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));

                        }else {
                            btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                            btnDone.setEnabled(true);
                        }
                    }

                }
                //yearMonth.setText(dateFormat.format(calendar.getTime()));
            }
        });



        collegeName = (TextView) findViewById(R.id.collegeName);
        dataBaseHelper = new DataBaseHelper(EducationalDetailsActivity.this);
        findViewById(R.id.collegeLabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.collegeLabel).setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        findViewById(R.id.collegeLabel).setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(EducationalDetailsActivity.this, CollegeActivity.class);
                    startActivityForResult(intent, COLLEGE_PICKER_REQUEST_CODE);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


                } else {
                    Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        collegeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collegeName.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        collegeName.setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(EducationalDetailsActivity.this, CollegeActivity.class);
                    startActivityForResult(intent, COLLEGE_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                } else {
                    Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.degreelabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.degreelabel).setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        findViewById(R.id.degreelabel).setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(EducationalDetailsActivity.this, DegreeActivity.class);
                    startActivityForResult(intent, DEGREE_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                } else {
                    Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        degreeName = (TextView) findViewById(R.id.degreeName);
        degreeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                degreeName.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        degreeName.setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(EducationalDetailsActivity.this, DegreeActivity.class);
                    startActivityForResult(intent, DEGREE_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                } else {
                    Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }


            }
        });


        Calendar calendar3 = Calendar.getInstance();

        calendar3.set(Calendar.YEAR, 1990);
        calendar3.set(Calendar.MONTH, 1);
        calendar3.set(Calendar.DAY_OF_MONTH, 1);


//      final  Calendar c = Calendar.getInstance();
//
//        calendar.set(Calendar.YEAR, 2030);
//        calendar.set(Calendar.MONTH, 12);
//        calendar.set(Calendar.DAY_OF_MONTH, 31);


        // new date picker
        datePicker = (DatePicker) findViewById(R.id.datePicker);
        datePicker.setMinDate(calendar3.getTime().getTime());
        // datePicker.setMaxDate(c.getTimeInMillis());

        enddatePicker = (DatePicker) findViewById(R.id.endDatePicker);
        saveButton = (Button) findViewById(R.id.saveButton);
        cardView = (CardView) findViewById(R.id.dateSelect_cardview);
        pickerTitle = (FontTextView) findViewById(R.id.pickerTitle);
        // saveView = (RippleView) findViewById(R.id.saveButtonView);
        //nextArrow = (ImageView) findViewById(R.id.nextArrow);
        radio_button = (ImageView) findViewById(R.id.radio_button);
        cancel = (FontTextView) findViewById(R.id.cancel);
        select_linear = (LinearLayout) findViewById(R.id.select_linear);


        //For start date
        LinearLayout pickerParentLayout = (LinearLayout) datePicker.getChildAt(0);

        LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);


        int datePickerCount = pickerSpinnersHolder.getChildCount();
        if (datePickerCount == 3) {
            View firstView = pickerSpinnersHolder.getChildAt(0);

            NumberPicker firstNumber = (NumberPicker) firstView;

            if (firstNumber != null && firstNumber.getMaxValue() > 12 && firstNumber.getMaxValue() < 32) {
                pickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
            } else {
                View secondView = pickerSpinnersHolder.getChildAt(1);
                NumberPicker secondNumber = (NumberPicker) secondView;
                if (secondNumber != null && secondNumber.getMaxValue() > 12 && secondNumber.getMaxValue() < 32) {
                    pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                }
            }
        }


        //for end date


        LinearLayout endDatePickerParentLayout = (LinearLayout) enddatePicker.getChildAt(0);

        LinearLayout endDatePickerSpinnersHolder = (LinearLayout) endDatePickerParentLayout.getChildAt(0);


        int endDatePickerCount = endDatePickerSpinnersHolder.getChildCount();
        if (endDatePickerCount == 3) {
            View firstView = endDatePickerSpinnersHolder.getChildAt(0);

            NumberPicker firstNumber = (NumberPicker) firstView;

            if (firstNumber != null && firstNumber.getMaxValue() > 12 && firstNumber.getMaxValue() < 32) {
                endDatePickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
            } else {
                View secondView = endDatePickerSpinnersHolder.getChildAt(1);
                NumberPicker secondNumber = (NumberPicker) secondView;
                if (secondNumber != null && secondNumber.getMaxValue() > 12 && secondNumber.getMaxValue() < 32) {
                    endDatePickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                }
            }
        }


        radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    radio_button.setImageResource(R.drawable.ic_radio_button_checked_black_24dp);
                    count = 1;
                    saveButton.setText("SAVE");
                    //  nextArrow.setVisibility(View.GONE);
                } else {
                    radio_button.setImageResource(R.drawable.ic_radio_button_unchecked_black_24dp);
                    count = 0;
                    saveButton.setText("NEXT");
                    // nextArrow.setVisibility(View.VISIBLE);
                }
            }
        });

        findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        startDateText = (TextView) findViewById(R.id.startDateText);


        findViewById(R.id.startDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openStartDatePicker();
            }
        });
        startDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openStartDatePicker();
            }
        });

        endDateText = (TextView) findViewById(R.id.endDateText);
        endDateText.setText("Month/Year");
        findViewById(R.id.endDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEndDatePicker();
            }
        });
        endDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEndDatePicker();
            }
        });


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectionUtils.isConnectionAvailable()) {

                    if (saveButton.getText().toString().equalsIgnoreCase("NEXT")) {
                        cardView.setVisibility(View.VISIBLE);

                        if (pickerTitle.getText().toString().equalsIgnoreCase("START DATE")) {
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            int month = datePicker.getMonth();
                            String mon = months[month];
                            startDateString = mon + "-" + datePicker.getYear();
                            Log.e("start date", startDateString + "a");
                            startDateText.setText(startDateString);
                            startDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
                        }
                        saveButton.setText("SAVE");
                        //nextArrow.setVisibility(View.GONE);
                        pickerTitle.setText("END DATE");
                        pickerTitle.setTextColor(Color.parseColor("#183989"));
                        datePicker.setVisibility(View.GONE);
                        enddatePicker.setVisibility(View.VISIBLE);
                        select_linear.setVisibility(View.GONE);

                        Calendar startDateCal = Calendar.getInstance();
                        startDateCal.set(Calendar.YEAR, datePicker.getYear());
                        startDateCal.set(Calendar.MONTH, datePicker.getMonth());
                        startDateCal.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                        enddatePicker.setMinDate(startDateCal.getTime().getTime());
                        //  enddatePicker.setMaxDate(c.getTimeInMillis());


                    } else {
                        cardView.setVisibility(View.GONE);
                        btnDone.setEnabled(true);
                        startDateText.setEnabled(true);
                        endDateText.setEnabled(true);
                        findViewById(R.id.startDate).setEnabled(true);
                        findViewById(R.id.endDate).setEnabled(true);
                        if (pickerTitle.getText().toString().equalsIgnoreCase("END DATE")) {
                            int month = enddatePicker.getMonth();
                            String mon = months[month];
                            endDateString = mon + "-" + enddatePicker.getYear();
                            Log.e("end date", endDateString + "a");


                            Calendar startDateCal = Calendar.getInstance();

                            startDateCal.set(Calendar.YEAR, enddatePicker.getYear());
                            startDateCal.set(Calendar.MONTH, enddatePicker.getMonth());
                            startDateCal.set(Calendar.DAY_OF_MONTH, enddatePicker.getDayOfMonth());

                            //    datePicker.setMaxDate(c.getTimeInMillis());

                            endDateText.setText(endDateString);
                            endDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
                            if (isValidateFields()) {
                                btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                                btnDone.setText("DONE");
                            } else {
                                if (!kya.equalsIgnoreCase("stud")) {
                                    btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));
                                    btnDone.setText("SKIP");
                                }
                            }
                        }

                        if (count == 1) {
                            int month = datePicker.getMonth();
                            String mon = months[month];
                            startDateString = mon + "-" + datePicker.getYear();
                            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                            int mnth = calendar.get(Calendar.MONTH);
                            String newmon = months[mnth];
                            endDateString = newmon + "-" + calendar.get(Calendar.YEAR);
                            Log.e("start date", startDateString + "aa");
                            Log.e("end date", endDateString + "aa");
                            startDateText.setText(startDateString);
                            endDateText.setText(endDateString);
                            startDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
                            endDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
                            if (isValidateFields()) {
                                btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                                btnDone.setText("DONE");
                            } else {
                                if (!kya.equalsIgnoreCase("stud")) {
                                    btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));
                                    btnDone.setText("SKIP");
                                }
                            }
                        }
                    }


                } else {
                    Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }


            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardView.setVisibility(View.GONE);
                btnDone.setEnabled(true);
                startDateText.setEnabled(true);
                endDateText.setEnabled(true);
                findViewById(R.id.startDate).setEnabled(true);
                findViewById(R.id.endDate).setEnabled(true);
            }
        });


        btnDone = (Button) findViewById(R.id.btnDone);

        btnDone.setTypeface(typeface1);
        if (!kya.equalsIgnoreCase("stud")) {
            btnDone.setText("SKIP");
        } else {
            btnDone.setText("DONE");
        }
        // btnDone.setTypeface(FontUtils.getInstance(this).getNexaTypeFace());

        //  doneButtonView = (RippleView) findViewById(R.id.doneButtonView);
        String selectedEducationId = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON);

        if (!TextUtils.isEmpty(selectedEducationId)) {
            educationalDetails = dataBaseHelper.getEducationFromId(selectedEducationId);
            collegeName.setText(educationalDetails.getCollegeName());
            degreeName.setText(educationalDetails.getDegreeName());
            startDateText.setText(educationalDetails.getFrom());
            endDateText.setText(educationalDetails.getTo());

            calendar=stringToCalendar(educationalDetails.getFrom(),"MMM-yyyy");
            calendarE=stringToCalendar(educationalDetails.getFrom(),"MMM-yyyy");

            if (!TextUtils.isEmpty(educationalDetails.getFrom())) {
                String[] dates = educationalDetails.getFrom().split("-");
                if (dates != null && dates.length == 2) {
                    Calendar startDate = Calendar.getInstance();
                    int index = -1;
                    for (int i = 0; i < months.length; i++) {
                        if (months[i].equals(dates[0])) {
                            index = i;
                            break;
                        }
                    }
                    if (index != -1) {
                        startDate.set(Calendar.YEAR, Integer.parseInt(dates[1]));
                        startDate.set(Calendar.MONTH, index);
                        startDate.set(Calendar.DAY_OF_MONTH, 1);

                        enddatePicker.setMinDate(startDate.getTime().getTime());
                        //       enddatePicker.setMaxDate(c.getTimeInMillis());
                    }
                }

            }

            if (!TextUtils.isEmpty(educationalDetails.getTo())) {
                String[] dates = educationalDetails.getTo().split("-");
                if (dates != null && dates.length == 2) {
                    Calendar startDate = Calendar.getInstance();
                    int index = -1;
                    for (int i = 0; i < months.length; i++) {
                        if (months[i].equals(dates[0])) {
                            index = i;
                            break;
                        }
                    }
                    if (index != -1) {
                        startDate.set(Calendar.YEAR, Integer.parseInt(dates[1]));
                        startDate.set(Calendar.MONTH, index);
                        startDate.set(Calendar.DAY_OF_MONTH, 1);

                        //        datePicker.setMaxDate(c.getTimeInMillis());
                    }
                }

            }


            if (isValidateFields()) {
                btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                btnDone.setText("DONE");
            } else {
                if (!kya.equalsIgnoreCase("stud")) {
                    btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));
                    btnDone.setText("SKIP");
                }
            }

            collegeName.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
            degreeName.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
            startDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
            endDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
        }


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDone.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                    btnDone.setEnabled(true);
                    }
                }.start();
                if (connectionUtils.isConnectionAvailable()) {
                    if (btnDone.getText().toString().equalsIgnoreCase("SKIP") && !kya.equalsIgnoreCase("stud")) {
                        if (!TextUtils.isEmpty(dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT))) {
                            long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                            if (selectedid > 0) {
                                dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                            } else {
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                            }
                            Intent intentMessage = new Intent();

                            if (getParent() == null) {
                                setResult(RESULT_OK, intentMessage);
                            } else {
                                getParent().setResult(RESULT_OK, intentMessage);
                            }
                            finish();
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


                        } else {
                            ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
                            if (professionals != null && professionals.size() > 0) {
                                Intent intent = new Intent(EducationalDetailsActivity.this, ListProfessionalDetailsActivity.class);
                                intent.putExtra("kya", kya);
                                intent.putExtra("save", "no");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            } else {

                                Intent intent = new Intent(EducationalDetailsActivity.this, ProfessionalDetailsActivity.class);
                                intent.putExtra("kya", kya);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            }
                        }

                    }

                    if (isValidateFields()&& isclgcorrect && isdegreecorrect) {
                        if (!TextUtils.isEmpty(collegeName.getText().toString()) && !TextUtils.isEmpty(degreeName.getText().toString()) && !TextUtils.isEmpty(startDateText.getText().toString()) && !TextUtils.isEmpty(endDateText.getText().toString())) {
                            EducationalDetails educationalDetails = new EducationalDetails();
                            educationalDetails.setCollegeName(collegeName.getText().toString());
                            educationalDetails.setDegreeName(degreeName.getText().toString());

                   /* timePeriod = txtStart.getText().toString() + " to " + txtEnd.getText().toString();
                    educationalDetails.setYearOfGraduation(timePeriod);*/
                            educationalDetails.setFrom(startDateText.getText().toString());
                            educationalDetails.setTo(endDateText.getText().toString());
                            String selectedEducationId = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON);

                            if (TextUtils.isEmpty(selectedEducationId)) {
                                dataBaseHelper.createEducation(educationalDetails);
                            } else {
                                dataBaseHelper.updateEducation(selectedEducationId, educationalDetails);
                                long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                                if (selectedid > 0) {
                                    dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON, "");
                                }
                            }

                            Intent intent = new Intent(EducationalDetailsActivity.this, ListEducationActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("kya", kya);
                            intent.putExtra("save", "no");
                            startActivity(intent);

                            finish();
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);



                        } else {
                            Toast.makeText(EducationalDetailsActivity.this, "Please enter all the details", Toast.LENGTH_SHORT).show();
                        }

                    }else {
                        if(!isdegreecorrect && !isclgcorrect){
                            Toast.makeText(EducationalDetailsActivity.this, "Please enter correct college and degree details", Toast.LENGTH_SHORT).show();
                        }else if(!isclgcorrect){
                            Toast.makeText(EducationalDetailsActivity.this, "Please enter correct college details", Toast.LENGTH_SHORT).show();

                        }else if(!isdegreecorrect){
                            Toast.makeText(EducationalDetailsActivity.this, "Please enter correct degree details", Toast.LENGTH_SHORT).show();

                        }else {
                            Toast.makeText(EducationalDetailsActivity.this, "Please enter all the details", Toast.LENGTH_SHORT).show();

                        }

                    }


                } else {
                    Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();

                }

            }
        });
    }


    private void openStartDatePicker() {
        dateclicked="start";
        yearMonthPickerDialog.show();
    }

    private void openEndDatePicker() {
        dateclicked="end";
        yearMonthPickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        String selectedEducationId = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON);
        if (!TextUtils.isEmpty(selectedEducationId)) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                            if (selectedid > 0) {
                                dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON, "");
                            }

                            Intent intent = new Intent(EducationalDetailsActivity.this, ProfileActivity.class);

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(EducationalDetailsActivity.this);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(EducationalDetailsActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(EducationalDetailsActivity.this);
            }

            builder.setMessage("This action will cancel editing data. Do you want to continue?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        } else {

            super.onBackPressed();
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COLLEGE_PICKER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // DegreeFragment fragment = null;


                String college = data.getStringExtra("COLLEGENAME");
                String collegeId = data.getStringExtra("id");
                if (!TextUtils.isEmpty(college)) {

                    if (!TextUtils.isEmpty(collegeId) && !collegeId.equalsIgnoreCase("new")) {

                        collegeName.setText(college);
                        collegeName.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));

                    } else {

                        if (connectionUtils.isConnectionAvailable()) {
                            RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EducationalDetailsActivity.this, EducationalDetailsActivity.this);
                            processor.addCollege(college);
                        } else {
                            Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();

                        }


                    }
                }


            }
        } else if (requestCode == DEGREE_PICKER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String college = data.getStringExtra("DEGREE");
                String collegeId = data.getStringExtra("id");
                if (!TextUtils.isEmpty(college) && !collegeId.equalsIgnoreCase("new")) {

                    degreeName.setText(college);
                    degreeName.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));

                } else {

                    if (connectionUtils.isConnectionAvailable()) {
                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EducationalDetailsActivity.this, EducationalDetailsActivity.this);
                        processor.addDegree(college);
                    } else {
                        Toast.makeText(EducationalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();

                    }


                }

            }
        }
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if (guiResponse != null) {
            if (status.equals(RequestStatus.SUCCESS)) {
                if (guiResponse instanceof AddNewCollegeResponse) {

                    AddNewCollegeResponse getAllCollegeResponse = (AddNewCollegeResponse) guiResponse;
                    if (getAllCollegeResponse != null && getAllCollegeResponse.getCollege() != null) {
                        collegeName.setText(getAllCollegeResponse.getCollege().getName());
                        collegeName.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));

                    } else {
                        Toast.makeText(EducationalDetailsActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                    }
                } else if (guiResponse instanceof AddNewDegreeResponse) {

                    AddNewDegreeResponse degreeResponse = (AddNewDegreeResponse) guiResponse;
                    if (degreeResponse != null && degreeResponse.getDegree() != null) {
                        degreeName.setText(degreeResponse.getDegree().getName());
                        degreeName.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));

                    } else {
                        Toast.makeText(EducationalDetailsActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                    }
                }

            } else {
                Toast.makeText(EducationalDetailsActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDateSelected(int year, int month, int day) {

        if (dateSelection == 0) {
            int monthIndex = month - 1;
            /*if (ISEDIT) {
                if (AppConstant.selectedEducation != null) {
                    String id = AppConstant.selectedEducation.getId();
                    if (!TextUtils.isEmpty(id)) {
                        dataBaseHelper.updateTableDetails(id, DataBaseHelper.TABLE_COLLEGE, DataBaseHelper.KEY_COLLEGE_YEAR_FROM, months[monthIndex] + "-" + year);
                        dataBaseHelper.updateTableDetails(id, DataBaseHelper.TABLE_COLLEGE, DataBaseHelper.KEY_COLLEGE_YEAR_TO, "");
                    }
                    AppConstant.selectedEducation = null;
                }
            }*/
            Log.e("--------------", "" + months[monthIndex] + "-" + year);
            startDateText.setText(months[monthIndex] + "-" + year);
            startDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
            Calendar startDate = Calendar.getInstance();

            startDate.set(Calendar.YEAR, year);
            startDate.set(Calendar.MONTH, monthIndex);
            startDate.set(Calendar.DAY_OF_MONTH, 1);

            Calendar calendar = Calendar.getInstance();

            calendar.set(Calendar.YEAR, 1990);
            calendar.set(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);


            endDateText.setText("Month/Year");


        } else {
            int monthIndex = month - 1;
            /*if (ISEDIT) {
                if (AppConstant.selectedEducation != null) {
                    String id = AppConstant.selectedEducation.getId();
                    if (!TextUtils.isEmpty(id))
                        dataBaseHelper.updateTableDetails(id, DataBaseHelper.TABLE_COLLEGE, DataBaseHelper.KEY_COLLEGE_YEAR_TO, months[monthIndex] + "-" + year);
                    AppConstant.selectedEducation = null;
                }
            }*/
            endDateText.setText(months[monthIndex] + "-" + year);

            endDateText.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.green_profile));
            // loadEducationData();
            Calendar startDate = Calendar.getInstance();

            startDate.set(Calendar.YEAR, year);
            startDate.set(Calendar.MONTH, monthIndex);
            startDate.set(Calendar.DAY_OF_MONTH, 1);
        }
    }


    private boolean isValidateFields() {

        isclgcorrect=false;
        if(collegeName.getText().toString().length()>1){
            isclgcorrect=true;
        }else {
            isclgcorrect=false;
        }
        isdegreecorrect=false;
        if(degreeName.getText().toString().length()>1){
            isdegreecorrect=true;
        }else {
            isdegreecorrect=false;
        }
        boolean isValidate = true;
        if (collegeName.getText().toString().equalsIgnoreCase("Enter your college name")) {
            isValidate = false;
        } else if (degreeName.getText().toString().equalsIgnoreCase("Enter your degree name")) {
            isValidate = false;
        } else if (startDateText.getText().toString().equalsIgnoreCase("Month/Year")) {
            isValidate = false;
            isValidate = false;
        } else if (endDateText.getText().toString().equalsIgnoreCase("Month/Year")) {
            isValidate = false;
        }
        return isValidate;
    }

    public static Calendar stringToCalendar(String stringDate, String datePattern) {
        if (stringDate == null) {
            return null;
        }
        Calendar calendar = new GregorianCalendar();
        try {
            Timestamp newDate = Timestamp.valueOf(stringDate);
            calendar.setTime(newDate);
        }
        catch (Exception e) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
            try {
                calendar.setTime(simpleDateFormat.parse(stringDate));
            }
            catch (ParseException pe) {
                calendar = null;
            }
        }
        return calendar;
    }
}
