package com.globalgyan.getvalyou;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.cms.request.SocialMediaSignUpRequest;
import com.globalgyan.getvalyou.cms.response.SocialRegisterResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Dates;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.globalgyan.getvalyou.utils.ValidationUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NaNi on 10/09/17.
 */

public class SocialsignupActivity extends AppCompatActivity implements GUICallback{

    String providerTypeSTr = "";
    String email,phn,username,type;
    @BindView(R.id.input_phone_sstp)TextInputLayout phonetw;
    String id = null;
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socialsignup);
        ButterKnife.bind(this);
        Intent intent = getIntent();

        type=intent.getStringExtra("type");
        if(type.equalsIgnoreCase("social")){
            providerTypeSTr = intent.getStringExtra("providertype");
        }
        else{
            email=intent.getStringExtra("email");
            username=intent.getStringExtra("username");
            phn=intent.getStringExtra("phn");
            id=intent.getStringExtra("id");
        }

        if (providerTypeSTr.equalsIgnoreCase("facebook")) {
            if (AppConstant.socialData != null) {

                id = AppConstant.socialData.optString("id");
                username = AppConstant.socialData.optString("name");
                email = AppConstant.socialData.optString("email");


            }
        }  else if (providerTypeSTr.equalsIgnoreCase("googleplus")) {


            providerTypeSTr = intent.getStringExtra("providertype");


            email = AppConstant.socialData.optString("email");
           username = AppConstant.socialData.optString("name");
            id = AppConstant.socialData.optString("id");
        }

    }

    @OnClick({R.id.verify})
     void verifyNum(View view){
        if(view.getId()==R.id.verify){
            if(validatePhone()){
                phn=phonetw.getEditText().getText().toString();
                SocialMediaSignUpRequest registerRequest = new SocialMediaSignUpRequest();
                registerRequest.setFcmToken(PreferenceUtils.getFcmToken(SocialsignupActivity.this));
                registerRequest.setEmail(email);
                registerRequest.setMobile(phn);
                if (AppConstant.socialData != null) {
                    registerRequest.setProviderId(id);
                    registerRequest.setProviderType(providerTypeSTr);
                    registerRequest.setProviderData(AppConstant.socialData);
                }
                registerRequest.setUsername(username);
                JsonParser jsonParser = new JsonParser();
                JsonObject gsonObject = (JsonObject) jsonParser.parse(registerRequest.getURLEncodedPostdata().toString());


                RetrofitRequestProcessor processor = new RetrofitRequestProcessor(SocialsignupActivity.this, SocialsignupActivity.this);
                processor.doSocialSignUp(gsonObject);


            } else {
                Toast.makeText(SocialsignupActivity.this, "Enter a valid phone number", Toast.LENGTH_SHORT).show();
            }

        }
        }




    private boolean validatePhone() {
        boolean returnValue = true;
        if (!ValidationUtils.nameValid(phonetw.getEditText(), this)) {
            returnValue = false;

        }

        return returnValue;

    }
    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if (guiResponse != null) {
            if (status.equals(GUICallback.RequestStatus.SUCCESS)) {
                AppConstant.socialData = null;
                SocialRegisterResponse registerResponse = (SocialRegisterResponse) guiResponse;
                if (registerResponse != null) {
                    if (registerResponse.isStatus()) {

                        try {
                            FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                            FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                        } catch (Exception ex) {

                        }

                        DataBaseHelper dataBaseHelper = new DataBaseHelper(SocialsignupActivity.this);
                        dataBaseHelper.deleteAllTables();
                        if (registerResponse.getLoginModel() != null && registerResponse.getLoginModel().isProfileComplete())
                            PreferenceUtils.setProfileDone(SocialsignupActivity.this);


                        if (registerResponse.getLoginModel() != null) {
                            PreferenceUtils.setVerified(SocialsignupActivity.this);
                            String displayName = registerResponse.getLoginModel().getDisplayName();
                            if (TextUtils.isEmpty(displayName))
                                displayName = registerResponse.getLoginModel().getFirstName();
                            dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, displayName);
                            PreferenceUtils.setCandidateId(registerResponse.getLoginModel().get_id(), SocialsignupActivity.this);

                            LoginModel loginModel = registerResponse.getLoginModel();
                            long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                            String dob = loginModel.getDateOfBirth();
                            if (!TextUtils.isEmpty(dob)) {
                                try {
                                    //String date = DateUtils.UtcToJavaDateFormat(dob, new SimpleDateFormat("dd-MM-yyyy"));
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                } catch (Exception ex) {

                                }
                            }


                            //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOMAIN, loginModel.getDomain().toString());
                            //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_LOCATION, loginModel.getCurrentLocation());
                            //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMPLOY_STATUS, loginModel.getEmploymentStatus());
                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());
                            if(loginModel.getVideos()!=null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());


                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());

                            PreferenceUtils.setCandidateId(loginModel.get_id(), SocialsignupActivity.this);

                          //  List<EducationalDetails> educationalDetailses = registerResponse.getLoginModel().getCurrentEducation();
                            //  List<PriorExperience> priorExperiences = loginResponse.getLoginModel().getPriorWorkExperience();
                            List<Professional> professionals = registerResponse.getLoginModel().getProfessionalExperience();
                         /*   if (educationalDetailses != null) {
                                for (EducationalDetails details : educationalDetailses) {
                                    String date = "";
                                    Dates start = details.getStartDate();

                                    if (start != null && start.getMonth() < 13) {
                                        int index = start.getMonth();
                                        index--;
                                        date = months[index] + "-" + String.valueOf(start.getYear());
                                    }

                                    details.setFrom(date);
                                    Dates end = details.getEndDate();

                                    if (end != null && end.getMonth() < 13) {
                                        int index = end.getMonth();
                                        index--;
                                        date = months[index] + "-" + String.valueOf(end.getYear());
                                    }
                                    details.setTo(date);
                                    dataBaseHelper.createEducation(details);
                                }
                            }*/
                            if (professionals != null) {
                                for (Professional details : professionals) {
                                    String date = "";
                                    Dates start = details.getStartDate();

                                    if (start != null && start.getMonth() < 13) {
                                        int index = start.getMonth();
                                        index--;
                                        date = months[index] + "-" + String.valueOf(start.getYear());
                                    }
                                    details.setFrom(date);

                                    Dates end = details.getEndDate();
                                    try {
                                        if (end != null && end.getMonth() < 13) {
                                            int index = end.getMonth();
                                            index--;
                                            date = months[index] + "-" + String.valueOf(end.getYear());
                                        }
                                    } catch (Exception ex) {

                                    }
                                    details.setTo(date);
                                    dataBaseHelper.createProfessionlDetails(details);
                                }
                            }
                        }

                        if (registerResponse.getLoginModel().isMobileVerified()) {
                            Intent intent = new Intent(SocialsignupActivity.this, MainPlanetActivity.class);
                            intent.putExtra("tabs","normal");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            AppConstant.OTP_FROM_SIGNUP=true;
                            Intent intent = new Intent(SocialsignupActivity.this, EnterOtpActivity.class);
                            intent.putExtra("phn",phn);
                            intent.putExtra("id",id);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    } else {

                        Toast.makeText(this, "Phone number already registered", Toast.LENGTH_SHORT).show();
                    }
                } else
                    Toast.makeText(this, "Server issuse. Please try again", Toast.LENGTH_SHORT).show();


            } else
                Toast.makeText(this, "Server issuse. Please try again", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, "Network error. Please try again", Toast.LENGTH_SHORT).show();
    }
}
