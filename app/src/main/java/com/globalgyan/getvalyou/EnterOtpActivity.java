package com.globalgyan.getvalyou;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.MasterSlaveGame.New_MSQ;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.request.UpdateMobileVerificationRequest;
import com.globalgyan.getvalyou.cms.response.RegisterResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.UpdateMobileVerificationResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.response.VerifyOtpResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.helper.SmsReceiver;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.interfaces.SmsListener;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;

import static com.thefinestartist.utils.service.ServiceUtil.getInputMethodManager;

public class EnterOtpActivity extends AppCompatActivity implements GUICallback,ActivityCompat.OnRequestPermissionsResultCallback{

    private EditText otpTxt = null,et1,et2,et3,et4,et5,et6;
    private String phoneNumber = null,type="";
    private String userId = null,email_is=null;
    private String sessionId = null,otpf="";
    Button done;
    String otpText="";
    boolean is_otp_to_signup_api_call=false;
    final int MY_PERMISSIONS_REQUEST_ACCESS_CODE=222;
    ConnectionUtils connectionUtils;
    String username,password,email,phone_no;
    FontTextView edit_no;
    Dialog kProgressHUD;
     FontTextView resendOtp;
    MyCountDownTimer countDownTimer;
    public int n;
    FontTextView timer;
    FontTextView otptext;
    boolean msg_recevied = false;

    private SimpleLocation location;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#06030E"));
        }
        AppConstant.isotpty_active=true;
        location = new SimpleLocation(this);
        otpTxt = (EditText) findViewById(R.id.input_otp);
        et1=(EditText)findViewById(R.id.et1);
        otptext=(FontTextView) findViewById(R.id.otptext);
        et2=(EditText)findViewById(R.id.et2);
        et3=(EditText)findViewById(R.id.et3);
        et4=(EditText)findViewById(R.id.et4);
        et5=(EditText)findViewById(R.id.et5);
        et6=(EditText)findViewById(R.id.et6);




        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        done=(Button)findViewById(R.id.verifyotp);

        done.setTypeface(typeface1);
        edit_no=(FontTextView)findViewById(R.id.edit_no);
        timer=(FontTextView)findViewById(R.id.timer_otp);


        resendOtp = (FontTextView) findViewById(R.id.resendOtp);
        connectionUtils =new ConnectionUtils(EnterOtpActivity.this);
        checkPermissions();

       createProgress();
        edit_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });
        resendOtp.setEnabled(false);
        done.setEnabled(false);
        resendOtp.setTextColor(Color.parseColor("#ABBAC2"));
        done.setBackgroundColor(Color.parseColor("#ABBAC2"));

        countDownTimer = new MyCountDownTimer(40000, 1000);

        countDownTimer.start();
      /*  new CountDownTimer(40000, 40000) {
            @Override
            public void onTick(long l) {



                Log.e("time",""+l);
                timer.setText(" (0:"+l/1000+")");

            }

            @Override
            public void onFinish() {
                resendOtp.setEnabled(true);
                resendOtp.setTextColor(Color.parseColor("#463cc0"));
                done.setBackgroundColor(Color.parseColor("#463cc0"));
                done.setEnabled(true);

            }
        }.start();
*/







        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(connectionUtils.isConnectionAvailable()){
                    kProgressHUD.show();
                    resendOtp.setEnabled(false);
                    resendOtp.setTextColor(Color.parseColor("#ABBAC2"));
                    done.setBackgroundColor(Color.parseColor("#ABBAC2"));
                    new CountDownTimer(1500, 1500) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            if (!otpf.equalsIgnoreCase("YES")) {
                                Random rnd = new Random();
                                n = 100000 + rnd.nextInt(900000);
                                String urlis=AppConstant.OTP_BASE_URL+"+91"+phoneNumber+"/"+n;
                                new SEndForgotPassOtp().execute(urlis);
                            }else {
                                RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);
                                processor.sendOtp(phoneNumber);
                            }


                        }
                    }.start();


                   countDownTimer = new MyCountDownTimer(40000,1000);

                   countDownTimer.start();

                }else {
                    Toast.makeText(EnterOtpActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }


            }
        });



        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                edit_no.setEnabled(false);
                new CountDownTimer(5000, 5000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        edit_no.setEnabled(true);

                    }
                }.start();
                Log.e("Text", messageText);
                char[] cArray = messageText.toCharArray();
                et1.setText(String.valueOf(cArray[0]));
                et2.setText(String.valueOf(cArray[1]));
                et3.setText(String.valueOf(cArray[2]));
                et4.setText(String.valueOf(cArray[3]));
                et5.setText(String.valueOf(cArray[4]));
                et6.setText(String.valueOf(cArray[5]));
                et6.setSelection(et6.getText().length());
               /* Log.e("totp value",et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                        et5.getText()+""+et6.getText());*/
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        if(connectionUtils.isConnectionAvailable()){
                              /*  RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);
                            processor.verifyOtp(sessionId, et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                                    et5.getText()+""+et6.getText());
                            Log.e("totp value",et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                                    et5.getText()+""+et6.getText());*/
                              msg_recevied=true;
                            done.performClick();
                            msg_recevied=false;

                        }else {
                            Toast.makeText(EnterOtpActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                        }
                    }
                }.start();

            }
        });
        sessionId = getIntent().getStringExtra("session_id");
       // Log.e("SESS",sessionId);
        otpf=getIntent().getStringExtra("otpf");
        try {
            type=getIntent().getExtras().getString("type");
            phoneNumber = getIntent().getExtras().getString("phn");
            userId = getIntent().getExtras().getString("id");
            email_is=getIntent().getStringExtra("emailis");

        } catch (Exception ex) {
//            type="";
//            sessionId = null;
        }

        try{
            username=getIntent().getStringExtra("username");
            password=getIntent().getStringExtra("password");
            email=getIntent().getStringExtra("email");

        }catch (Exception e){

        }
        try {
            if (!otpf.equalsIgnoreCase("YES")) {
                //bn


                String newString = phoneNumber.substring(phoneNumber.length()-4);
                newString="******"+newString;
                String sop="OTP has been sent to <b>"+email_is+"</b> and <b>"+newString+"</b>";

                otptext.setText(Html.fromHtml(sop));
                if (connectionUtils.isConnectionAvailable()) {
                    Random rnd = new Random();
                     n = 100000 + rnd.nextInt(900000);

                    String urlis=AppConstant.OTP_BASE_URL+"+91"+phoneNumber+"/"+n;
                    new SEndForgotPassOtp().execute(urlis);
               /*     RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);

                    processor.sendOtp(phoneNumber);*/
                } else {
                    Toast.makeText(EnterOtpActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();

                }

            }
        }catch (Exception e){
            e.printStackTrace();
            Intent intent = new Intent(EnterOtpActivity.this, Welcome.class);
            intent.putExtra("gotologinpage","loginpage");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //changes
            finish();
        }


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(connectionUtils.isConnectionAvailable()){
                    String value=et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                            et5.getText()+""+et6.getText();
                    if (value.length() == 6) {
                        done.setEnabled(false);
                        resendOtp.setEnabled(false);
                        done.setTextColor(Color.parseColor("#ffffff"));
                        resendOtp.setTextColor(Color.parseColor("#ABBAC2"));
                        kProgressHUD.show();
                        new CountDownTimer(1500, 1500) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);
                                processor.verifyOtp(sessionId,et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                                        et5.getText()+""+et6.getText());
                                Log.e("totp value",et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                                        et5.getText()+""+et6.getText());
                                Log.e("truedone", "true");
                            }
                        }.start();

                        new CountDownTimer(10000, 10000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                Log.e("truefinish", "true");
                                done.setEnabled(true);
                           //     resendOtp.setEnabled(true);
                                done.setTextColor(Color.parseColor("#ffffff"));
                            //    resendOtp.setTextColor(Color.parseColor("#463cc0"));
                            }
                        }.start();
                    } else {
                        Toast.makeText(EnterOtpActivity.this, "Please enter correct OTP", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Log.e("truelast", "true");
                    done.setEnabled(false);
                    new CountDownTimer(3000, 3000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            done.setEnabled(true);
                        }
                    }.start();
                    Toast.makeText(EnterOtpActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }



                           }
        });

        editTextfocuschanger();
        editTextFocusChangerOnBack();
    }


    private void createProgress() {
        kProgressHUD = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        kProgressHUD.requestWindowFeature(Window.FEATURE_NO_TITLE);
        kProgressHUD.setContentView(R.layout.planet_loader);
        Window window = kProgressHUD.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        kProgressHUD.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) kProgressHUD.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        kProgressHUD.setCancelable(false);
      //  kProgressHUD.show();

    }

    private void editTextfocuschanger() {




        et1.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    et2.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });
        et2.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {


                if (s.length() ==1) {
                    et3.requestFocus();
                }else {

                        //et1.requestFocus();

                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });
        et3.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    et4.requestFocus();
                }else {
                   // et2.requestFocus();

                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });
        et4.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    et5.requestFocus();
                }else {
                   // et3.requestFocus();

                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });
        et5.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    et6.requestFocus();
                }else {
                   // et4.requestFocus();

                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });
        et6.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(final Editable s) {


                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        if (s.length() ==1&&!msg_recevied) {
                            edit_no.setEnabled(false);
                            new CountDownTimer(5000, 5000) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    edit_no.setEnabled(true);

                                }
                            }.start();
                            new CountDownTimer(1000, 1000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(connectionUtils.isConnectionAvailable()){
                              /*  RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);
                                processor.verifyOtp(sessionId, et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                                        et5.getText()+""+et6.getText());
                                Log.e("totp ",et1.getText()+""+et2.getText()+""+et3.getText()+""+et4.getText()+""+
                                        et5.getText()+""+et6.getText());
*/
                                        done.performClick();
                                        msg_recevied=false;
                                    }else {
                                        Toast.makeText(EnterOtpActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                                    }
                                }
                            }.start();
                        }else {
                            //et5.requestFocus();

                        }

                    }
                }.start();


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });
    }

    public void editTextFocusChangerOnBack(){
        et2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // You can identify which key pressed buy checking keyCode value
                // with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL&&et2.length()==0&&event.getAction()==0) {
                    // this is for backspace
                    et1.requestFocus();
                    Log.e("IME_TEST", "DEL KEY2");
                }
                return false;
            }
        });
        et3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // You can identify which key pressed buy checking keyCode value
                // with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL&&et3.length()==0&&event.getAction()==0) {
                    // this is for backspace
                    et2.requestFocus();
                    Log.e("IME_TEST", "DEL KEY3");
                }
                return false;
            }
        });


        et4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // You can identify which key pressed buy checking keyCode value
                // with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL&&et4.length()==0&&event.getAction()==0) {
                    // this is for backspace
                    et3.requestFocus();
                    Log.e("IME_TEST", "DEL KEY4");
                }
                return false;
            }
        });

        et5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // You can identify which key pressed buy checking keyCode value
                // with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL&&et5.length()==0&&event.getAction()==0) {
                    // this is for backspace
                    et4.requestFocus();
                    Log.e("IME_TEST", "DEL KEY5");
                }
                return false;
            }
        });
        et6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // You can identify which key pressed buy checking keyCode value
                // with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL&&et6.length()==0&&event.getAction()==0) {
                    // this is for backspace
                    et5.requestFocus();
                    Log.e("IME_TEST", "DEL KEY6");
                }
                return false;
            }
        });

    }

    @Override
    protected void onDestroy() {
        SmsReceiver.unBindListener();
        AppConstant.isotpty_active=false;
        try {
            if (kProgressHUD.isShowing() && kProgressHUD != null) {
                kProgressHUD.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
            kProgressHUD.dismiss();
        }
        super.onDestroy();
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(kProgressHUD.isShowing()&&kProgressHUD!=null){
            kProgressHUD.dismiss();
        }
        if (guiResponse != null) {

            if(is_otp_to_signup_api_call){
                is_otp_to_signup_api_call=false;
                 if (guiResponse != null) {
            if (status.equals(RequestStatus.SUCCESS)) {

                if (guiResponse instanceof RegisterResponse) {
                    RegisterResponse registerResponse = (RegisterResponse) guiResponse;
                    try {
                        LoginModel loginModelis = registerResponse.getLoginModel();
                        Log.e("wayis", loginModelis.getAuth_token());
                        new PrefManager(EnterOtpActivity.this).saveAuthToken(loginModelis.getAuth_token());
                    }catch (Exception e){
                        Log.e("error","token inot found");
                    }
                    if (registerResponse != null) {
                        if (registerResponse.isStatus()) {

                            try {
                                FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                            } catch (Exception ex) {

                            }

                            DataBaseHelper dataBaseHelper = new DataBaseHelper(EnterOtpActivity.this);
                            dataBaseHelper.deleteAllTables();
                            dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, registerResponse.getLoginModel().getDisplayName());
                            PreferenceUtils.setCandidateId(registerResponse.getLoginModel().get_id(), EnterOtpActivity.this);


                            if (registerResponse.getLoginModel() != null) {
                                LoginModel loginModel = registerResponse.getLoginModel();
                                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                String dob = loginModel.getDateOfBirth();
                                if (!TextUtils.isEmpty(dob)) {
                                    try {
                                        //String date = DateUtils.UtcToJavaDateFormat(dob, new SimpleDateFormat("dd-MM-yyyy"));
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                    } catch (Exception ex) {

                                    }
                                }


                                //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOMAIN, loginModel.getDomain().toString());
                                //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_LOCATION, loginModel.getCurrentLocation());
                                //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMPLOY_STATUS, loginModel.getEmploymentStatus());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());


                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());

                                PreferenceUtils.setCandidateId(loginModel.get_id(), EnterOtpActivity.this);

                                AppConstant.OTP_FROM_SIGNUP = false;
                                String candidateId = PreferenceUtils.getCandidateId(EnterOtpActivity.this);
                                if (!TextUtils.isEmpty(candidateId)) {
                                    UpdateMobileVerificationRequest request = new UpdateMobileVerificationRequest();
                                    request.setMobileVerified(true);
                                    // PreferenceUtils.setProfileDone(EnterOtpActivity.this);
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);
                                    JsonParser jsonParser = new JsonParser();
                                    JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());
                                    processor.updateMobileVerificationStatus(candidateId, gsonObject);
                                }



                            if (PreferenceUtils.isProfileDone(EnterOtpActivity.this)) {
                                Intent intent = new Intent(EnterOtpActivity.this, MainPlanetActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                //changes
                                finish();
                            } else {
                                Intent intent = new Intent(EnterOtpActivity.this, ProfileActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                //changes
                                finish();
                            }
                            }
                        } else {
                            //do changes
                           // Toast.makeText(this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            //Intent forgot = new Intent(this, LoginSignupActivity.class);
                            //forgot.putExtra("SIGNUP","YES");
                            finish();
                            //startActivity(forgot);
                        }
                    } else
                        Toast.makeText(this, "Server issuse", Toast.LENGTH_SHORT).show();
                }else if (guiResponse instanceof UpdateMobileVerificationResponse) {
                    UpdateMobileVerificationResponse response  = (UpdateMobileVerificationResponse) guiResponse;
                    if(response!=null && response.isStatus()){
                        PreferenceUtils.setVerified(EnterOtpActivity.this);
                        // PreferenceUtils.setProfileDone(EnterOtpActivity.this);
                        Log.e("stattsssss",""+response.isStatus());
                        if (response.isStatus()) {

                            if (PreferenceUtils.isProfileDone(EnterOtpActivity.this)) {
                                Intent intent = new Intent(EnterOtpActivity.this, MainPlanetActivity.class);
                                intent.putExtra("tabs","normal");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                finish();

                            }else {
                                Intent intent = new Intent(EnterOtpActivity.this, MainPlanetActivity.class);
                                intent.putExtra("tabs","normal");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                finish();
                            }

                        } else {
                            //do changes
                            Intent intent = new Intent(EnterOtpActivity.this, Welcome.class);
                            intent.putExtra("gotologinpage","loginpage");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            //changes
                            finish();
                        }

                    }

                }

            } else
                Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
            }else {
                if (status.equals(RequestStatus.SUCCESS)) {

                    if (guiResponse instanceof VerifyOtpResponse) {
                        VerifyOtpResponse response = (VerifyOtpResponse) guiResponse;
                        if (response != null && response.isStatus()) {
                            if (!AppConstant.OTP_FROM_SIGNUP) {
                                // PreferenceUtils.setProfileDone(EnterOtpActivity.this);
                                if(otpf.equalsIgnoreCase("yes")){
                                    is_otp_to_signup_api_call=true;
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);
                                    Log.e("FCM TOKEN", FirebaseInstanceId.getInstance().getToken()+"");
                                    processor.doSignUp(email.toString(), password.toString(), FirebaseInstanceId.getInstance().getToken(), username.toString(),phoneNumber ,"null",location.getLatitude()+","+location.getLongitude(),"ANDROID",getAndroidVersion(),getDevice());
                                }else{
                                    Intent intent = new Intent(EnterOtpActivity.this, ResetPasswordActivity.class);
                                    intent.putExtra("id", userId);
                                    startActivity(intent);
                                    //changes
                                    finish();
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                }

                            } else {
                                AppConstant.OTP_FROM_SIGNUP = false;
                                String candidateId = PreferenceUtils.getCandidateId(EnterOtpActivity.this);
                                if (!TextUtils.isEmpty(candidateId)) {

                                    UpdateMobileVerificationRequest request = new UpdateMobileVerificationRequest();
                                    request.setMobileVerified(true);
                                    // PreferenceUtils.setProfileDone(EnterOtpActivity.this);
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(EnterOtpActivity.this, EnterOtpActivity.this);
                                    JsonParser jsonParser = new JsonParser();
                                    JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());
                                    processor.updateMobileVerificationStatus(candidateId, gsonObject);

                                }

                            }
                        } else {
                            if(response != null)
                                Toast.makeText(EnterOtpActivity.this, response.getDetails(), Toast.LENGTH_LONG).show();
                            else{
                                Toast.makeText(EnterOtpActivity.this, "server error", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else if (guiResponse instanceof SendOtpResponse) {
                        SendOtpResponse sendOtpResponse = (SendOtpResponse) guiResponse;
                        if (sendOtpResponse.isStatus())
                        {
                            sessionId = sendOtpResponse.getDetails();
                        }
                    } else if (guiResponse instanceof UpdateMobileVerificationResponse) {
                        UpdateMobileVerificationResponse response  = (UpdateMobileVerificationResponse) guiResponse;
                        if(response!=null && response.isStatus()){
                            PreferenceUtils.setVerified(EnterOtpActivity.this);
                            // PreferenceUtils.setProfileDone(EnterOtpActivity.this);
                            Log.e("stattsssss",""+response.isStatus());
                            if (response.isStatus()) {

                                if (PreferenceUtils.isProfileDone(EnterOtpActivity.this)) {
                                    Intent intent = new Intent(EnterOtpActivity.this, MainPlanetActivity.class);
                                    intent.putExtra("tabs","normal");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    finish();

                                }else {
                                    //after signup
                                    //need
                                    SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(EnterOtpActivity.this).edit();
                                    prefEditor.putString("login_success", "login_success");
                                    prefEditor.apply();

                                    Intent intent = new Intent(EnterOtpActivity.this, MainPlanetActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("tabs","normal");
                                    intent.putExtra("from","signup");
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                    finish();
                                }

                            } else {
                                //do changes
                                Intent intent = new Intent(EnterOtpActivity.this, Welcome.class);
                                intent.putExtra("gotologinpage","loginpage");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                //changes
                                finish();
                            }

                        }

                    }
                } else {
                  //  Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }
            }

        }








        else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS,
                Manifest.permission.READ_PHONE_STATE
        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }

    public String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.e("RELEASE",release);
        return release;
    }

    public String getDevice() {
        String deviceName = Build.MANUFACTURER + " " + Build.MODEL;

        return deviceName;
    }

    @Override
    public void onBackPressed() {
        finish();
 overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }
    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }

        @Override
        public void onTick(long millisUntilFinished) {

        //   timer.setText(" (0:" + millisUntilFinished / 1000 %60+")");
            resendOtp.setEnabled(false);
            resendOtp.setTextColor(Color.parseColor("#ABBAC2"));
            Log.e("false", "false");
            timer.setText(" "+String.format("(%02d:%02d)",
                    TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
        }


        @Override

        public void onFinish() {
            timer.setText("(00:00)");
            Log.e("true", "true");
            resendOtp.setEnabled(true);
            resendOtp.setTextColor(Color.parseColor("#463cc0"));
            done.setBackgroundColor(Color.parseColor("#463cc0"));
            done.setEnabled(true);


        }
    }

    public class SEndForgotPassOtp extends AsyncTask<String ,String,String > {


        //ProgressDialog progressDialog= new ProgressDialog(LoginActivity.this);


        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection httpURLConnection=null;
            String result="",responcestring="";




            try {
                url=new URL(params[0]);
                httpURLConnection=(HttpURLConnection)url.openConnection();

                responcestring=readStream(httpURLConnection.getInputStream());
                result=responcestring;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(httpURLConnection!=null){
                    httpURLConnection.disconnect();
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            Log.e("LoginAct json", s);
           try{
               kProgressHUD.dismiss();
           }catch (Exception e){
               e.printStackTrace();
           }
            if(s.contains("Success")){
                try {
                    JSONObject jsonObject=new JSONObject(s);
                    sessionId=jsonObject.getString("Details");

                    new SendOtpForEmailVerification().execute(AppConstant.Ip_url+"Player/send_otp_email");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                Toast.makeText(EnterOtpActivity.this,"Error occured!",Toast.LENGTH_SHORT).show();
            }



        }
    }

    private String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    private class SendOtpForEmailVerification extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";

            try {
                url = new URL(params[0]);
                 ref_id = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("email", email_is)
                        .object("otp", n)
                        .build().toString();


                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");

                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();

                responseString = readStream(urlConnection.getInputStream());
                Log.v("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);


        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }

}
