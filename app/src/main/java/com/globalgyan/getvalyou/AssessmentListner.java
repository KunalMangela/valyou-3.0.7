package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;

import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;

public class AssessmentListner extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().toString().equalsIgnoreCase("open_assessment_group_list")) {
            Log.e("message", "broadcast assess");
            new PrefManager(context).saveRatingtype("assessment_group");

            new PrefManager(context).saveAsstype("Ass");
            Intent intentis = new Intent(context, AssessmentGroupsActivity.class);
            new PrefManager(context).saveNav("Ass");
            new PrefManager(context).saveback_status("Assgroup");
            intentis.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intentis);

        }
        if (intent.getAction().toString().equalsIgnoreCase("open_assessment_list")) {
            Log.e("message", "broadcast assess");
            new PrefManager(context).saveRatingtype("assessment_group");

            new PrefManager(context).saveAsstype("Ass");
            Intent intents = new Intent(context, HomeActivity.class);
            intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intents.putExtra("pushis", "yes");
            intents.putExtra("ass_ce_id", intent.getStringExtra("ass_ce_id"));

            intents.putExtra("tabs", "normal");
            new PrefManager(context).saveNav("Ass");

            // intent.putExtra("navigatefrom", "mainplanet");


            context.startActivity(intents);



        }
    }
}
