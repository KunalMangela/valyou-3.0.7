package com.globalgyan.getvalyou;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.widget.Toast;

import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.okhttp.OkHttpStack;


/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 15/11/16
 *         Module : Valyou.
 */
public class ValYouApplication extends MultiDexApplication {

    public static Context context= null;


    @Override
    public void onCreate() {

        super.onCreate();
      /*  RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);
*/
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        UploadService.HTTP_STACK = new OkHttpStack();

        context = getApplicationContext();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Toast.makeText(this,"Your device has low memory", Toast.LENGTH_SHORT).show();
    }


}
