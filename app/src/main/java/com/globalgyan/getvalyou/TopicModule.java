package com.globalgyan.getvalyou;

public class TopicModule {
    String type, obj_name, lms_url, childrens, is_locked, author, warn_text,desc,isfeedback,sm_type,sm_articulate_url,typelms;
    int id, sm_id, orderid;

    public TopicModule() {
    }

    public String getSm_type() {
        return sm_type;
    }

    public void setSm_type(String sm_type) {
        this.sm_type = sm_type;
    }

    public String getSm_articulate_url() {
        return sm_articulate_url;
    }

    public void setSm_articulate_url(String sm_articulate_url) {
        this.sm_articulate_url = sm_articulate_url;
    }

    public String getIsfeedback() {
        return isfeedback;
    }

    public String getTypelms() {
        return typelms;
    }

    public void setTypelms(String typelms) {
        this.typelms = typelms;
    }

    public void setIsfeedback(String isfeedback) {
        this.isfeedback = isfeedback;
    }

    public String getWarn_text() {
        return warn_text;
    }

    public void setWarn_text(String warn_text) {
        this.warn_text = warn_text;
    }

    public String getAuthor() {
        return author;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(String is_locked) {
        this.is_locked = is_locked;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObj_name() {
        return obj_name;
    }

    public void setObj_name(String obj_name) {
        this.obj_name = obj_name;
    }

    public String getLms_url() {
        return lms_url;
    }

    public void setLms_url(String lms_url) {
        this.lms_url = lms_url;
    }

    public String getChildrens() {
        return childrens;
    }

    public void setChildrens(String childrens) {
        this.childrens = childrens;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSm_id() {
        return sm_id;
    }

    public void setSm_id(int sm_id) {
        this.sm_id = sm_id;
    }
}
