package com.globalgyan.getvalyou.model;

/**
 * Created by Goutam Kumar K. on 18/3/17.
 */

public class UploadResponse {

    private String videoProfileId = null;
    private String profilePicId = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getProfilePicId() {
        return profilePicId;
    }

    public void setProfilePicId(String profilePicId) {
        this.profilePicId = profilePicId;
    }

    public String getVideoProfileId() {
        return videoProfileId;
    }

    public void setVideoProfileId(String videoProfileId) {
        this.videoProfileId = videoProfileId;
    }

    private String success = null;
}
