package com.globalgyan.getvalyou.model;

/**
 * Created by Globalgyan on 05-01-2018.
 */

public class QuestionModel {
    String QT_id,Q_id,question,opt1,opt2,opt3,opt4,opt5,correctanswer,time,q_level;

    public QuestionModel() {
    }

    public String getQT_id() {
        return QT_id;
    }

    public void setQT_id(String QT_id) {
        this.QT_id = QT_id;
    }

    public String getQ_level() {
        return q_level;
    }

    public void setQ_level(String q_level) {
        this.q_level = q_level;
    }

    public String getQ_id() {
        return Q_id;
    }

    public void setQ_id(String q_id) {
        Q_id = q_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOpt1() {
        return opt1;
    }

    public void setOpt1(String opt1) {
        this.opt1 = opt1;
    }

    public String getOpt2() {
        return opt2;
    }

    public void setOpt2(String opt2) {
        this.opt2 = opt2;
    }

    public String getOpt3() {
        return opt3;
    }

    public void setOpt3(String opt3) {
        this.opt3 = opt3;
    }

    public String getOpt4() {
        return opt4;
    }

    public void setOpt4(String opt4) {
        this.opt4 = opt4;
    }

    public String getOpt5() {
        return opt5;
    }

    public void setOpt5(String opt5) {
        this.opt5 = opt5;
    }

    public String getCorrectanswer() {
        return correctanswer;
    }

    public void setCorrectanswer(String correctanswer) {
        this.correctanswer = correctanswer;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
