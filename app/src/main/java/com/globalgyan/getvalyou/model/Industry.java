package com.globalgyan.getvalyou.model;


/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 15/11/16
 *         Module : Valyou.
 */
public class Industry {

    private String name = null;

    private String description = null;

    private String id = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Industry{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
