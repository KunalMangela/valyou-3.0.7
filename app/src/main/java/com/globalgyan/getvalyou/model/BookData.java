package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 20/1/17
 *         Module : Valyou.
 */

public class BookData {


    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    private String bookName = null;
    private String _id = null;
    private String link = null;
    private String domain = null;
    private String industryFocus = null;

    public String getIndustryFocus() {
        return industryFocus;
    }

    public void setIndustryFocus(String industryFocus) {
        this.industryFocus = industryFocus;
    }

    private CoverImage coverImage = null;


    public String getArticleName() {
        return bookName;
    }

    public void setArticleName(String articleName) {
        this.bookName = articleName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public CoverImage getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(CoverImage coverImage) {
        this.coverImage = coverImage;
    }
}
