package com.globalgyan.getvalyou.model;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 10/3/17
 *         Module : Valyou.
 */

public class UserDetailsModel {
    private String _id = null;
    private String salt = null;
    private String displayName = null;
    private String username = null;
    private String firstName = null;
    private String email = null;
    private String mobile = null;
    private String profileImageURL = null;
    private String videoProfileURL = null;
    private List<Notifications> notificationList = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public String getVideoProfileURL() {
        return videoProfileURL;
    }

    public void setVideoProfileURL(String videoProfileURL) {
        this.videoProfileURL = videoProfileURL;
    }

    public List<Notifications> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<Notifications> notificationList) {
        this.notificationList = notificationList;
    }
}
