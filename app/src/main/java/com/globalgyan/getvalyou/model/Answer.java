package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 5/4/17
 *         Module : Valyou.
 */

public class Answer {

    private String question = null;

    private String _id = null;

    public Video getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(Video videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    private Video videoUrl = null;
}
