package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/12/16
 *         Module : Valyou.
 */
public class Position {

    private String _id = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name = null;
}
