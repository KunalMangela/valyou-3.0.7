package com.globalgyan.getvalyou.model;

import java.util.List;

public class JobList{
	private String country;
	private List<Object> qualified;
	private String requirements;
	private String code;
	private List<Object> disqualified;
	private String heading;
	private String city;
	private List<String> applied;
	private String lastUpdatedAt;
	private String description;
	private String industry;
	private Salary salary;
	private Experience experience;
	private String createdAt;
	private String educationLevel;
	private List<String> sourced;
	private int __v;
	private String _id;
	private String state;
	private String department;
	private String status;

	public String getCode(){
		return code; 
	}

	public void setSalary(Salary salary){
		this.salary = salary; 
	}

	public void setStatus(String status){
		this.status = status; 
	}

	public List<Object> getQualified(){
		return qualified; 
	}

	public void setLastUpdatedAt(String lastUpdatedAt){
		this.lastUpdatedAt = lastUpdatedAt; 
	}

	public String get_id(){
		return _id; 
	}

	public List<String> getSourced(){
		return sourced; 
	}

	public void setDepartment(String department){
		this.department = department; 
	}

	public String getCountry(){
		return country; 
	}

	public String getHeading(){
		return heading; 
	}

	public String getStatus(){
		return status; 
	}

	public Experience getExperience(){
		return experience; 
	}

	public void setDisqualified(List<Object> disqualified){
		this.disqualified = disqualified; 
	}

	public List<String> getApplied(){
		return applied; 
	}

	public String getLastUpdatedAt(){
		return lastUpdatedAt; 
	}

	public void setCountry(String country){
		this.country = country; 
	}

	public void setState(String state){
		this.state = state; 
	}

	public String getDepartment(){
		return department; 
	}

	public String getRequirements(){
		return requirements; 
	}

	public String getIndustry(){
		return industry; 
	}

	public int get__v(){
		return __v; 
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt; 
	}

	public void setCode(String code){
		this.code = code; 
	}

	public String getDescription(){
		return description; 
	}

	public String getCreatedAt(){
		return createdAt; 
	}

	public void setDescription(String description){
		this.description = description; 
	}

	public void setExperience(Experience experience){
		this.experience = experience; 
	}

	public void setSourced(List<String> sourced){
		this.sourced = sourced; 
	}

	public void setHeading(String heading){
		this.heading = heading; 
	}

	public String getEducationLevel(){
		return educationLevel; 
	}

	public void set_id(String _id){
		this._id = _id; 
	}

	public List<Object> getDisqualified(){
		return disqualified; 
	}

	public Salary getSalary(){
		return salary; 
	}

	public void setQualified(List<Object> qualified){
		this.qualified = qualified; 
	}

	public void setRequirements(String requirements){
		this.requirements = requirements; 
	}

	public String getCity(){
		return city; 
	}

	public void setEducationLevel(String educationLevel){
		this.educationLevel = educationLevel; 
	}

	public void setApplied(List<String> applied){
		this.applied = applied; 
	}

	public void setIndustry(String industry){
		this.industry = industry; 
	}

	public void setCity(String city){
		this.city = city; 
	}

	public String getState(){
		return state; 
	}

	public void set__v(int __v){
		this.__v = __v; 
	}

}