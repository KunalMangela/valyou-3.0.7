package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 20/1/17
 *         Module : Valyou.
 */

public class CourseData {


    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    private String courseName = null;
    private String _id = null;
    private String link = null;
    private String domain = null;
    private String level = null;
    private String industryFocus = null;

    public String getIndustryFocus() {
        return industryFocus;
    }

    public void setIndustryFocus(String industryFocus) {
        this.industryFocus = industryFocus;
    }

    private CoverImage coverImage = null;


    public String getArticleName() {
        return courseName;
    }

    public void setArticleName(String courseName) {
        this.courseName = courseName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }



    public CoverImage getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(CoverImage coverImage) {
        this.coverImage = coverImage;
    }
}
