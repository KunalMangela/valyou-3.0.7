package com.globalgyan.getvalyou.model;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class Messages {

    private String senderId = null;
    private String messageContent = null;
    private String _id = null;
    private boolean isEvent = false;
    private List<VisibleAudience> visibleAudience = null;
    private String sentOn = null;

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isEvent() {
        return isEvent;
    }

    public void setEvent(boolean event) {
        isEvent = event;
    }

    public List<VisibleAudience> getVisibleAudience() {
        return visibleAudience;
    }

    public void setVisibleAudience(List<VisibleAudience> visibleAudience) {
        this.visibleAudience = visibleAudience;
    }

    public String getSentOn() {
        return sentOn;
    }

    public void setSentOn(String sentOn) {
        this.sentOn = sentOn;
    }
}
