package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class Book {


    private String _id = null;

    private String bookName = null;

    private String domain = null;

    private String link = null;

    private String price = null;

    private String industryFocus = null;

    private String level = null;

    private String uploadedFileId = null;

    private CoverImage coverImage = null;

    public CoverImage getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(CoverImage coverImage) {
        this.coverImage = coverImage;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIndustryFocus() {
        return industryFocus;
    }

    public void setIndustryFocus(String industryFocus) {
        this.industryFocus = industryFocus;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getUploadedFileId() {
        return uploadedFileId;
    }

    public void setUploadedFileId(String uploadedFileId) {
        this.uploadedFileId = uploadedFileId;
    }
}
