package com.globalgyan.getvalyou.model;


public class Experience{
	private int from;
	private int to;

	public int getTo(){
		return to; 
	}

	public int getFrom(){
		return from; 
	}

	public void setFrom(int from){
		this.from = from; 
	}

	public void setTo(int to){
		this.to = to; 
	}

}