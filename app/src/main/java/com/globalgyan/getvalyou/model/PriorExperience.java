package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/11/16
 *         Module : Valyou.
 */
public class PriorExperience {

    private String organisationName = null;

    private String natureOfWork = null;

    private String timePeriod = null;

    private Dates endDate = null;
    private Dates startDate = null;

    public Dates getStartDate() {
        return startDate;
    }

    public void setStartDate(Dates startDate) {
        this.startDate = startDate;
    }

    public Dates getEndDate() {
        return endDate;
    }

    public void setEndDate(Dates endDate) {
        this.endDate = endDate;
    }



    public String getOrganizationName() {
        return organisationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organisationName = organizationName;
    }

    public String getNatureOfWork() {
        return natureOfWork;
    }

    public void setNatureOfWork(String natureOfWork) {
        this.natureOfWork = natureOfWork;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    @Override
    public String toString() {
        return "organizationName='" + organisationName + '\'' +
                ", natureOfWork='" + natureOfWork + '\'' +
                ", timePeriod='" + timePeriod + '\'' +
                '}';
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    private String id = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
