package com.globalgyan.getvalyou.model;


public class Salary{
	private int from;
	private String currency;

	public int getFrom(){
		return from; 
	}

	public void setFrom(int from){
		this.from = from; 
	}

	public String getCurrency(){
		return currency; 
	}

	public void setCurrency(String currency){
		this.currency = currency; 
	}

}