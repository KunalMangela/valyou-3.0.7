package com.globalgyan.getvalyou.model;

/**
 * Created by Globalgyan on 05-12-2017.
 */

public class GameModel {
    String gdid,tgid,ceid,uid,lg_id,course_id,certification_id,learning_group_name,course_name,url_ic,certification_name,extra_param;
    String game,category,displayname,status,group,groupid,ce_status,ce_picavailable,ce_pic_frequency,description_game,game_time,temp_status;

    int status_id,pic_frequency;

    public GameModel() {
    }

    public int getStatus_id() {
        return status_id;
    }

    public int getPic_frequency() {
        return pic_frequency;
    }

    public void setPic_frequency(int pic_frequency) {
        this.pic_frequency = pic_frequency;
    }

    public String getUrl_ic() {
        return url_ic;
    }

    public void setUrl_ic(String url_ic) {
        this.url_ic = url_ic;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getExtra_param() {
        return extra_param;
    }

    public void setExtra_param(String extra_param) {
        this.extra_param = extra_param;
    }

    public String getTemp_status() {
        return temp_status;
    }

    public void setTemp_status(String temp_status) {
        this.temp_status = temp_status;
    }

    public String getLg_id() {
        return lg_id;
    }

    public void setLg_id(String lg_id) {
        this.lg_id = lg_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getCertification_id() {
        return certification_id;
    }

    public void setCertification_id(String certification_id) {
        this.certification_id = certification_id;
    }

    public String getLearning_group_name() {
        return learning_group_name;
    }

    public void setLearning_group_name(String learning_group_name) {
        this.learning_group_name = learning_group_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCertification_name() {
        return certification_name;
    }

    public void setCertification_name(String certification_name) {
        this.certification_name = certification_name;
    }

    public String getGame_time() {
        return game_time;
    }

    public void setGame_time(String game_time) {
        this.game_time = game_time;
    }

    public String getDescription_game() {
        return description_game;
    }

    public void setDescription_game(String description_game) {
        this.description_game = description_game;
    }

    public String getGdid() {
        return gdid;
    }

    public void setGdid(String gdid) {
        this.gdid = gdid;
    }

    public String getTgid() {
        return tgid;
    }

    public void setTgid(String tgid) {
        this.tgid = tgid;
    }

    public String getCeid() {
        return ceid;
    }

    public void setCeid(String ceid) {
        this.ceid = ceid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getCe_status() {
        return ce_status;
    }

    public void setCe_status(String ce_status) {
        this.ce_status = ce_status;
    }

    public String getCe_picavailable() {
        return ce_picavailable;
    }

    public void setCe_picavailable(String ce_picavailable) {
        this.ce_picavailable = ce_picavailable;
    }

    public String getCe_pic_frequency() {
        return ce_pic_frequency;
    }

    public void setCe_pic_frequency(String ce_pic_frequency) {
        this.ce_pic_frequency = ce_pic_frequency;
    }
}
