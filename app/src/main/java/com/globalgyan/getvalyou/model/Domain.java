package com.globalgyan.getvalyou.model;

import java.util.List;

public class Domain{
	private List<SubDomainItem> childDomain;
	private String domainName;
	private String _id;

	public List<SubDomainItem> getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(List<SubDomainItem> childDomain) {
		this.childDomain = childDomain;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}
}