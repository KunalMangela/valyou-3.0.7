package com.globalgyan.getvalyou.model;


/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 15/11/16
 *         Module : Valyou.
 */
public class SubCategory {

    private String title = null;

    private boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
