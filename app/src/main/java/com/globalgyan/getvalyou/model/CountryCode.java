package com.globalgyan.getvalyou.model;

/**
 * Created by Goutam Kumar K. on 3/3/17.
 */

public class CountryCode {
    private String countryName = null;
    private String isoCode = null;
    private String countryCode = null;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getCountryCode() {
        return "+"+countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
