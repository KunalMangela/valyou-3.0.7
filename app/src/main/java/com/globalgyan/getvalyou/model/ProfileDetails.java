package com.globalgyan.getvalyou.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 4/11/16
 *         Module : Valyou.
 */
public class ProfileDetails {

    private String name = "Techniche";

    private String gender = null;

    private String dob = null;

    private String location = null;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private ArrayList<EducationalDetails> educationalDetailses = new ArrayList<>();

    private ArrayList<PriorExperience> priorExperiences = new ArrayList<>();


    private ArrayList<Professional> professionals = new ArrayList<>();

    private String employmentStatus = null;

    private String industries = null;


    private List<Domain> domanSelected = new ArrayList<>();


    public ArrayList<Professional> getProfessionals() {
        return professionals;
    }

    public void setProfessionals(ArrayList<Professional> professionals) {
        this.professionals = professionals;
    }

    public void addProfessional(Professional priorExperience) {
        priorExperience.setId(String.valueOf(System.currentTimeMillis()));
        professionals.add(priorExperience);
    }
    public void updateProfessional(Professional priorExperience,String id) {
        int i = 0;
        for(Professional professional: professionals){
            if(professional.getId().equals(id)){
                professionals.set(i,priorExperience);
            }
            i++;
        }

    }

    public ArrayList<PriorExperience> getPriorExperiences() {
        return priorExperiences;
    }

    public void setPriorExperiences(ArrayList<PriorExperience> priorExperiences) {
        this.priorExperiences = priorExperiences;
    }

    public void addPriorWorkExp(PriorExperience priorExperience) {
        priorExperience.setId(String.valueOf(System.currentTimeMillis()));
        priorExperiences.add(priorExperience);
    }

    public void updatePriorWorkExp(PriorExperience priorExperience,String id) {

        int i = 0;
        for(PriorExperience professional: priorExperiences){
            if(professional.getId().equals(id)){
                priorExperiences.set(i,priorExperience);
            }
            i++;
        }

    }


    public String getIndustries() {
        return industries;
    }

    public void setIndustries(String industries) {
        this.industries = industries;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public ArrayList<EducationalDetails> getEducationalDetailses() {
        return educationalDetailses;
    }


    public void addEducationalDetails(EducationalDetails educationalDetails) {
        educationalDetails.setId(String.valueOf(System.currentTimeMillis()));
        educationalDetailses.add(educationalDetails);
    }

    public void updateEducationalDetails(EducationalDetails priorExperience,String id) {

        int i = 0;
        for(EducationalDetails professional: educationalDetailses){
            if(professional.getId().equals(id)){
                educationalDetailses.set(i,priorExperience);
            }
            i++;
        }

    }


    public List<Domain> getDomanSelected() {
        return domanSelected;
    }

    public void setDomanSelected(Domain domanSelected) {
        this.domanSelected.add(domanSelected);
    }



    /* @Override
    public String toString() {
        return
               *//* "My name is " + name +"\n" +
                        " I know " + languagesKnown +"\n" +
                        " My date of birth is " + dob +"\n" +
                        " am " + gender +"\n" +
                        " my work status is " + workStatus +"\n" +
                        " and my Marital status is " + materialStatus ;*//*

                "My name is " + name +
                        " I know " + languagesKnown+
                        " My date of birth is " + dob +
                        " am " + gender +
                        " my work status is " + workStatus +
                        " and my Marital status is " + materialStatus ;
    }*/

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", dob='" + dob + '\'' +
                ", educationalDetailses=" + educationalDetailses +
                ", priorExperiences=" + priorExperiences +
                ", professionals=" + professionals +
                ", employmentStatus='" + employmentStatus + '\'' +
                ", industries='" + industries + '\'' +
                ", domanSelected=" + domanSelected +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}