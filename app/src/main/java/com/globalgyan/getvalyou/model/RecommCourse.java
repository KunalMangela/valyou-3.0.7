package com.globalgyan.getvalyou.model;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 20/1/17
 *         Module : Valyou.
 */

public class RecommCourse {

    private String _id = null;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<CourseData> getData() {
        return data;
    }

    public void setData(List<CourseData> data) {
        this.data = data;
    }

    private List<CourseData> data = null;

}
