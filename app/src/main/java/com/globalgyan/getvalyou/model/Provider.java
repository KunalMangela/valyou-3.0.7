package com.globalgyan.getvalyou.model;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/3/17
 *         Module : Valyou.
 */

public class Provider {

    private String providerType = null;

    private String providerId = null;

    public JSONObject getProviderData() {
        return providerData;
    }

    public void setProviderData(JSONObject providerData) {
        this.providerData = providerData;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    private JSONObject providerData = null;
}
