package com.globalgyan.getvalyou.model;

/**
 * Created by jasmini-android on 2/3/17.
 */

public class CreateChatModel {
    public String message;

    public CreateChatModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CreateChatModel(String message) {
        this.message = message;
    }
}
