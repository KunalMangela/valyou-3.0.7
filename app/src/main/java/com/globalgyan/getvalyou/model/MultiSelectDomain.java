package com.globalgyan.getvalyou.model;

import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 29/11/16
 *         Module : Valyou.
 */
public class MultiSelectDomain extends MultiCheckExpandableGroup {


    public MultiSelectDomain(String title, List<SubDomainItem> items) {
        super(title, items);
    }


}
