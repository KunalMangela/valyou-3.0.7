package com.globalgyan.getvalyou.model;

/**
 * Created by Goutam Kumar K. on 6/3/17.
 */

public class GetMeetingModel {
    /*{
        "_id": "58639d44fa20a16804f077d7",
            "agenda": "interview for new job",
            "meetingTime": "2016-11-30T01:00:00.000Z",
            "message": "notificationObj",
            "jobId": "58356fc6158066d80a4c6f4a",
            "candidateId": "58344ccfe67e968246c92376",
            "recruiterId": "5838357c2ce8057c3be1e39a",
            "__v": 0
    }*/

    private String _id = null;
    private String agenda = null;
    private String meetingTime = null;
    private String message = null;
    private String jobId = null;
    private String candidateId = null;
    private String recruiterId = null;
    private String meetingType = null;
    private String companyName = null;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    private String __v = null;

    public String getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(String meetingType) {
        this.meetingType = meetingType;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getRecruiterId() {
        return recruiterId;
    }

    public void setRecruiterId(String recruiterId) {
        this.recruiterId = recruiterId;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }
}
