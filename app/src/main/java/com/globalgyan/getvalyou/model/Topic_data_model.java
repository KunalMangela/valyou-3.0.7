package com.globalgyan.getvalyou.model;

public class Topic_data_model {

    String type,lock;
    public Topic_data_model() {
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLock() {
        return lock;
    }

    public void setLock(String lock) {
        this.lock = lock;
    }
}
