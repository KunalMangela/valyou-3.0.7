package com.globalgyan.getvalyou.model;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class Chat {

    private String _id = null;

    private List<Messages> messages = null;

    private String createdOn = null;

    private String[]admin = null;

    private List<Participant> currentParticipants = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<Messages> getMessages() {
        return messages;
    }

    public void setMessages(List<Messages> messages) {
        this.messages = messages;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String[] getAdmin() {
        return admin;
    }

    public void setAdmin(String[] admin) {
        this.admin = admin;
    }

    public List<Participant> getCurrentParticipants() {
        return currentParticipants;
    }

    public void setCurrentParticipants(List<Participant> currentParticipants) {
        this.currentParticipants = currentParticipants;
    }
}
