package com.globalgyan.getvalyou.model;


public class Interview{
	private String meetingTime;
	private String recruiterId;
	private String jobId;
	private int __v;
	private String _id;
	private String message;
	private String candidateId;
	private String agenda;

	public String getRecruiterId(){
		return recruiterId; 
	}

	public String get_id(){
		return _id; 
	}

	public void setCandidateId(String candidateId){
		this.candidateId = candidateId; 
	}

	public void set_id(String _id){
		this._id = _id; 
	}

	public void setMessage(String message){
		this.message = message; 
	}

	public void setMeetingTime(String meetingTime){
		this.meetingTime = meetingTime; 
	}

	public String getJobId(){
		return jobId; 
	}

	public String getCandidateId(){
		return candidateId; 
	}

	public void setJobId(String jobId){
		this.jobId = jobId; 
	}

	public String getAgenda(){
		return agenda; 
	}

	public int get__v(){
		return __v; 
	}

	public String getMeetingTime(){
		return meetingTime; 
	}

	public void setRecruiterId(String recruiterId){
		this.recruiterId = recruiterId; 
	}

	public String getMessage(){
		return message; 
	}

	public void setAgenda(String agenda){
		this.agenda = agenda; 
	}

	public void set__v(int __v){
		this.__v = __v; 
	}

}