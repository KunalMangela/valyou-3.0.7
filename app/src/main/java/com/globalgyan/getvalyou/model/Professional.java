package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/11/16
 *         Module : Valyou.
 */
public class Professional {

    private String positionHeld = null;

    private String organisationName = null;

    private String From  = null;



    public Dates getEndDate() {
        return endDate;
    }

    public void setEndDate(Dates endDate) {
        this.endDate = endDate;
    }

    public Dates getStartDate() {
        return startDate;
    }

    public void setStartDate(Dates startDate) {
        this.startDate = startDate;
    }

    private Dates endDate = null;

    private Dates startDate = null;

    public String getPositionHeld() {
        return positionHeld;
    }

    public void setPositionHeld(String positionHeld) {
        this.positionHeld = positionHeld;
    }

    public String getOrganizationName() {
        return organisationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organisationName = organizationName;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    private String to = null;

    private String id = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "positionHeld='" + positionHeld + '\'' +
                ", organizationName='" + organisationName + '\'' +
                ", From='" + From + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
}
