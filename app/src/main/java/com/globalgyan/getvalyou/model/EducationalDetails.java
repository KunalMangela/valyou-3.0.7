package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/11/16
 *         Module : Valyou.
 */
public class EducationalDetails {

    private String id = null;


    private String from = null;

    private String to  = null;



    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String collegeName = null;

    private String degreeName = null;


    private Dates endDate = null;
    private Dates startDate = null;

    public Dates getStartDate() {
        return startDate;
    }

    public void setStartDate(Dates startDate) {
        this.startDate = startDate;
    }

    public Dates getEndDate() {
        return endDate;
    }

    public void setEndDate(Dates endDate) {
        this.endDate = endDate;
    }

    public String getYearOfGraduation() {
        return yearOfCompletion;
    }

    public void setYearOfGraduation(String yearOfGraduation) {
        yearOfCompletion = yearOfGraduation;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    private String yearOfCompletion = null;

    @Override
    public String toString() {
        return "collegeName='" + collegeName + '\'' +
                ", degreeName='" + degreeName + '\'' +
                ", YearOfGraduation='" + yearOfCompletion + '\'' +
                '}';
    }
}
