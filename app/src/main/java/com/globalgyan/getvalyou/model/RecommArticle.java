package com.globalgyan.getvalyou.model;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 20/1/17
 *         Module : Valyou.
 */

public class RecommArticle {

    private String _id = null;

    public List<ArticleData> getData() {
        return data;
    }

    public void setData(List<ArticleData> data) {
        this.data = data;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    private List<ArticleData> data = null;

}
