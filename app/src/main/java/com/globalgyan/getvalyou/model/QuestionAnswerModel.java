package com.globalgyan.getvalyou.model;

/**
 * Created by Jasmini Mishra. on 10/5/17.
 */

public class QuestionAnswerModel {
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    private String question = null;
    private String answer = null;
    private String rightAnswer = null;
  private String id= null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }
}
