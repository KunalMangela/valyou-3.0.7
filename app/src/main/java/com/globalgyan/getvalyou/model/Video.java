package com.globalgyan.getvalyou.model;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 5/4/17
 *         Module : Valyou.
 */

public class Video {

    private String s3valyouinputbucket = null;

    private List<String> s3valyououtputbucket = null;

    private List<String> cloudFront = null;

    public String getS3valyouinputbucket() {
        return s3valyouinputbucket;
    }

    public void setS3valyouinputbucket(String s3valyouinputbucket) {
        this.s3valyouinputbucket = s3valyouinputbucket;
    }

    public List<String> getS3valyououtputbucket() {
        return s3valyououtputbucket;
    }

    public void setS3valyououtputbucket(List<String> s3valyououtputbucket) {
        this.s3valyououtputbucket = s3valyououtputbucket;
    }

    public List<String> getCloudFront() {
        return cloudFront;
    }

    public void setCloudFront(List<String> cloudFront) {
        this.cloudFront = cloudFront;
    }
}
