package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 14/4/17
 *         Module : Valyou.
 */

public class Meeting {

    private String _id = null;

    private String meetingTime = null;
    private String agenda = null;
    private String meetingType = null;
    private String openTokApiKey = null;
    private String openTokToken = null;
    private String openTokSessionId = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public String getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(String meetingType) {
        this.meetingType = meetingType;
    }

    public String getOpenTokApiKey() {
        return openTokApiKey;
    }

    public void setOpenTokApiKey(String openTokApiKey) {
        this.openTokApiKey = openTokApiKey;
    }

    public String getOpenTokToken() {
        return openTokToken;
    }

    public void setOpenTokToken(String openTokToken) {
        this.openTokToken = openTokToken;
    }

    public String getOpenTokSessionId() {
        return openTokSessionId;
    }

    public void setOpenTokSessionId(String openTokSessionId) {
        this.openTokSessionId = openTokSessionId;
    }
}
