package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 31/1/17
 *         Module : Valyou.
 */

public class Interactions {

    public Interactions(String title, String description) {
        this.title = title;
        this.description = description;
    }

    private String title = null;

    private String description = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
