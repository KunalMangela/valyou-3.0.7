package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class VisibleAudience {

    private String audienceId = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getAudienceId() {
        return audienceId;
    }

    public void setAudienceId(String audienceId) {
        this.audienceId = audienceId;
    }

    private String _id = null;
}
