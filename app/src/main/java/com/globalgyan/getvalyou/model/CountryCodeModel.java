package com.globalgyan.getvalyou.model;

import java.util.List;

/**
 * Created by Goutam Kumar K. on 3/3/17.
 */

public class CountryCodeModel {

    private List<CountryCode> countryList = null;

    public List<CountryCode> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<CountryCode> countryList) {
        this.countryList = countryList;
    }
}
