package com.globalgyan.getvalyou.model;


/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 15/11/16
 *         Module : Valyou.
 */
public class ProfileData  {

    private String name = null;

    private String dob = null;

    private String sex = null;

    private String employmentStatus = null;

    private String industryExperience = null;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getIndustryExperience() {
        return industryExperience;
    }

    public void setIndustryExperience(String industryExperience) {
        this.industryExperience = industryExperience;
    }
}
