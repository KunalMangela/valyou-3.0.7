package com.globalgyan.getvalyou.model;

/**
 * Created by Goutam Kumar K. on 17/3/17.
 */

public class Providers {
    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    private String providerType=null;
}
