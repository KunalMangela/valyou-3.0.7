package com.globalgyan.getvalyou.model;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 16/12/16
 *         Module : Valyou.
 */
public class Dates {

    private int year  ;

    private int month  = 14;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
}
