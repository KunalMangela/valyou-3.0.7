package com.globalgyan.getvalyou.interfaces;

public interface GameUIUpdator {
   public void startGame();

   public void updateActionBar(boolean status);

   public void setTab(int index, int learningTabIndex);
   public int getTab();
}