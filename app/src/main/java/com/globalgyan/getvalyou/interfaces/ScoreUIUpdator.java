package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/5/17
 *         Module : Valyou.
 */

public interface ScoreUIUpdator {

    void onScoreUpdated();
}
