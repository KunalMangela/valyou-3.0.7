package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 4/1/17
 *         Module : Valyou.
 */
public interface RecordTimerUpdator {

    public void onUpdateTime(String text, float progress);
}
