package com.globalgyan.getvalyou.interfaces;

public interface FileReadyListener {
	public void finishedSavingFile(String file);
}
