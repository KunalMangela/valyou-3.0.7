package com.globalgyan.getvalyou.interfaces;

/**
 * Created by techniche-android on 11/1/17.
 */

public interface TimePeriodUpdator {

    public void onTimeClicked(boolean isStart);
}
