package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 11/11/16
 *         Module : Valyou.
 */
public interface RecorderOptionListner {

    public void optionChanged(String option);
}
