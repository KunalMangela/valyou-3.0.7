package com.globalgyan.getvalyou.interfaces;

import com.globalgyan.getvalyou.cms.response.ValYouResponse;

public interface GUICallback {

    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status);

    public static enum RequestStatus {SUCCESS, FAILED}

    ;
}
