package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/11/16
 *         Module : Valyou.
 */
public interface ProfileEditUpdator {

    public void onFinishUpdating();

    public void onDataModified();


}
