package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 7/1/17
 *         Module : Valyou.
 */

public interface ActionBarUpdator {

    public void onChangeLadyImageVisibility(boolean isVisible);

    public void onChangeActionBarVisibility(boolean isVisible);

    public void onChangeTitleVisibility(boolean isVisible, String name);

    public void onChangeLeftArrowVisibility(boolean isVisible);

    public void onChangeRightArrowVisibility(boolean isVisible);

    public void onValyouAlreadySelected(boolean selected);

}
