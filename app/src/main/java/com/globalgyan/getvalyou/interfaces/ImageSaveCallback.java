package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 14/11/16
 *         Module : Valyou.
 */
public interface ImageSaveCallback {

    public void imageCreated(String filePath);
}
