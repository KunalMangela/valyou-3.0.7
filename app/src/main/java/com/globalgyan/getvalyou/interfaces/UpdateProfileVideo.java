package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/4/17
 *         Module : Valyou.
 */

public interface UpdateProfileVideo {

    public void onUpdateProfileData();
}
