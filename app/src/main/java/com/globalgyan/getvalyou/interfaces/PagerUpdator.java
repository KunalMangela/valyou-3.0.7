package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 2/1/17
 *         Module : Valyou.
 */
public interface PagerUpdator {

    public void onUpdateViewPager();
}
