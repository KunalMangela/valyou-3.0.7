package com.globalgyan.getvalyou.interfaces;

/**
 * Created by Jasmini Mishra. on 11/5/17.
 */

public interface AssistVideoUpdator {

    String getQuestion();
    String getSampleAnswer();
    String getAnswer();
    String getId();
    int getTotalQuestions();
    int getCurrentIndex();
    void increaseCurrentIndex();
    void decreaseCurrentIndex();


}
