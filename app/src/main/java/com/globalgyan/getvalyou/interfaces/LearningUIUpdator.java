package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/2/17
 *         Module : Valyou.
 */

public interface LearningUIUpdator {
    public void learningPageStart();
}
