package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 20/4/17
 *         Module : Valyou.
 */

public interface VideoReziser {

    public void onVideoResized(String filePath, int currentIndex, boolean queue);
}
