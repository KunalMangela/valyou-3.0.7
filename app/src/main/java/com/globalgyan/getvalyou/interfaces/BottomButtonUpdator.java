package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 3/11/16
 *         Module : Valyou.
 */
public interface BottomButtonUpdator {

    public void chageButtonVisibility(boolean isPrevVisile, boolean isNextVisible);
}
