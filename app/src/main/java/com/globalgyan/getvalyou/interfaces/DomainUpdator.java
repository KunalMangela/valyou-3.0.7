package com.globalgyan.getvalyou.interfaces;

import com.globalgyan.getvalyou.model.SubDomainItem;

import java.util.ArrayList;


/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 16/11/16
 *         Module : Valyou.
 */
public interface DomainUpdator {

    public void onUpdateDomain(String title, ArrayList<SubDomainItem> categories);
}
