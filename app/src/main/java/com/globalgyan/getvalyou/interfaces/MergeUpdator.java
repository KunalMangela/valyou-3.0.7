package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 4/10/16
 *         Module : VideoSample.
 */
public interface MergeUpdator {

    public void onMergeFinished();
}
