package com.globalgyan.getvalyou.interfaces;

/**
 * Created by Goutam Kumar K. on 28/3/17.
 */

public interface LogoutHandler {

    public void onLoggedOut();
}
