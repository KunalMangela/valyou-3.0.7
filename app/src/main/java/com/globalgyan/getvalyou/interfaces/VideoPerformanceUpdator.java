package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 4/3/17
 *         Module : Valyou.
 */

public interface VideoPerformanceUpdator {


    public String getQuestion();

    public int totalQuestions();

    public int currentQuestionIndex();

    public String getFilePath();

    public void updateQuestionIndex();

    public void showHideToolbar(boolean status);

    public void startRecordingPageAgain();

    public void findis();

    public String getId();
}
