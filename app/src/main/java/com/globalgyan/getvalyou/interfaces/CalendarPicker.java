package com.globalgyan.getvalyou.interfaces;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 19/1/17
 *         Module : Valyou.
 */

public interface CalendarPicker {

    public void onDateSelected(int year, int month, int day);
}
