package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.Leaderboard;
import com.globalgyan.getvalyou.fragments.NotificationFragment;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;


public class LeaderboardActivity extends AppCompatActivity {
    LeaderboardAdapter2 leaderboardAdapter;
    public static LeaderBoardRankingAdapter leaderBoardRankingAdapter;
    ArrayList<LeaderBoardAssgroupModel> assgroup_names=new ArrayList<>();
    public static ArrayList<LeaderBoardUserRankingModel> leadeboard_ranks=new ArrayList<>();
    public static RecyclerView leader_list;
    public static String myName;
    public static String ceids,grp_name;
    public static FontTextView num,progress;
    public static boolean clickis=false;
    ImageView leader_bg, skip,back;
    public static DataBaseHelper dataBaseHelper = null;
    public static int name_no=1000;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;
    boolean isPowerSaveMode;
    PowerManager pm;
    BatteryManager bm;

    public static FrameLayout load_frame;
    public static ImageView load_img;
    FrameLayout frame_leader;
    public static ObjectAnimator imageViewObjectAnimator1;
    public static boolean flag_loader=false;


    static FontTextView no_ass_group;
    int batLevel;
    public static MaterialRefreshLayout swipe;
    AppConstant appConstant;
    static InputStream is = null;
    String result1 ="";
    static JSONObject jObj = null,jObjeya=null;
    HttpURLConnection urlConnection;
    public static String token="";
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="";
    int responseCode,responseCod,responseCo;
    public static Dialog alertDialog;
    ImageView myjourney;
    TextView textlead;
    ImageView red_planetastro;
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    public static Context mcontext;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);

        mcontext=LeaderboardActivity.this;
        appConstant = new AppConstant(this);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        Log.e("battery", ""+batLevel);
        textlead=(TextView)findViewById(R.id.textlead);
        myjourney=(ImageView)findViewById(R.id.myjourney);
        no_ass_group=(FontTextView)findViewById(R.id.no_assessment_groups_text);
        frame_leader = (FrameLayout)findViewById(R.id.frame_leader);
        swipe=(MaterialRefreshLayout)findViewById(R.id.swipe);
        leader_list = (RecyclerView) findViewById(R.id.leaderboard_list);
        load_frame=(FrameLayout)findViewById(R.id.load_frameis);
        load_img=(ImageView)findViewById(R.id.load_icis);

        skip = (ImageView) findViewById(R.id.skip);
        back = (ImageView) findViewById(R.id.back_arrow);
        progress=(FontTextView)findViewById(R.id.progress);
        red_planetastro=(ImageView)findViewById(R.id.red_planetastro);
        final Animation animation_pend = AnimationUtils.loadAnimation(LeaderboardActivity.this, R.anim.pendulam_type_animation);
        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            red_planetastro.startAnimation(animation_pend);

        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(clickis){
                    clickis=false;
                    name_no=1000;

                    ConnectionUtils connectionUtils=new ConnectionUtils(LeaderboardActivity.this);
                    if(connectionUtils.isConnectionAvailable()){
                        flag_loader=true;
                        try {
                            progress.setText("ASSESSMENT GROUPS");
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false);
                            leader_list.setLayoutManager(linearLayoutManager);
                            leader_list.setAdapter(leaderboardAdapter);
                            leaderboardAdapter.notifyDataSetChanged();
                            swipe.autoRefresh();
                        } catch (Exception e) {

                        }
                    }else {
                        progress.setText("ASSESSMENT GROUPS");
                        leader_list.setAdapter(leaderboardAdapter);
                        leaderboardAdapter.notifyDataSetChanged();
                    }

                }else {
                    try {
                        new GetContacts().cancel(true);
                    } catch (Exception e) {

                    }
                    try {
                        new GetRanks().cancel(true);
                    } catch (Exception e) {

                    }
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }

            }
        });
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        textlead.setTypeface(typeface1);

        textlead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(clickis){
                    clickis=false;
                    name_no=1000;

                    ConnectionUtils connectionUtils=new ConnectionUtils(LeaderboardActivity.this);
                    if(connectionUtils.isConnectionAvailable()){
                        flag_loader=true;
                        try {
                            progress.setText("ASSESSMENT GROUPS");
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false);
                            leader_list.setLayoutManager(linearLayoutManager);
                            leader_list.setAdapter(leaderboardAdapter);
                            leaderboardAdapter.notifyDataSetChanged();
                            swipe.autoRefresh();
                        } catch (Exception e) {

                        }
                    }else {
                        progress.setText("ASSESSMENT GROUPS");
                        leader_list.setAdapter(leaderboardAdapter);
                        leaderboardAdapter.notifyDataSetChanged();
                    }

                }else {
                    try {
                        new GetContacts().cancel(true);
                    } catch (Exception e) {

                    }
                    try {
                        new GetRanks().cancel(true);
                    } catch (Exception e) {

                    }
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }

            }
        });


        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);
        alertDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.planet_loader);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        imageViewObjectAnimator1 = ObjectAnimator.ofFloat(load_img,
                "rotation", 0f, 360f);
        imageViewObjectAnimator1.setDuration(500); // miliseconds
        imageViewObjectAnimator1.setStartDelay(100);
        imageViewObjectAnimator1.setRepeatCount(600);

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);


        GifView gifView1 = (GifView) alertDialog.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        alertDialog.setCancelable(false);


        myjourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new GetContacts().cancel(true);
                } catch (Exception e) {

                }
                try {
                    new GetRanks().cancel(true);
                } catch (Exception e) {

                }
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            }
        });
        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            startanimation();

        }
        else {}

        DataBaseHelper dataBaseHelper = null;
        dataBaseHelper = new DataBaseHelper(getApplicationContext());
        String myName = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);




        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        leader_list.setLayoutManager(linearLayoutManager);

        leaderboardAdapter = new LeaderboardAdapter2(this,assgroup_names);

        leaderBoardRankingAdapter=new LeaderBoardRankingAdapter(this,leadeboard_ranks);
        leader_list.setAdapter(leaderboardAdapter);




        swipe.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override

            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //refreshing...
                ConnectionUtils connectionUtils=new ConnectionUtils(LeaderboardActivity.this);
                if(connectionUtils.isConnectionAvailable()){

                    if(!clickis){
                        getLeadeBoard();
                    }else {
                        new GetRanks().execute();
                    }
                }else {
                    swipe.finishRefresh();
                    Toast.makeText(LeaderboardActivity.this,"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }

            }

            @Override

            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //load more refreshing...

            }
        });

        new CountDownTimer(100, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
                frame_leader.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFinish() {
                swipe.autoRefresh();

            }
        }.start();

    }
    private void getLeadeBoard() {
        new GetContacts().execute();
    }

    private class GetContacts extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {
            if(flag_loader) {
                load_frame.setVisibility(View.VISIBLE);
                imageViewObjectAnimator1.start();
            }else {
                load_frame.setVisibility(View.GONE);

            }
            no_ass_group.setVisibility(View.GONE);

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        if(flag_loader) {
                            imageViewObjectAnimator1.cancel();
                            load_frame.setVisibility(View.GONE);
                        }
                        no_ass_group.setVisibility(View.VISIBLE);
                        no_ass_group.setText("No Assessment Groups Available");
                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        if(flag_loader) {
                            imageViewObjectAnimator1.cancel();
                            load_frame.setVisibility(View.GONE);
                        }
                        no_ass_group.setVisibility(View.VISIBLE);
                        no_ass_group.setText("No Assessment Groups Available");
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        if(flag_loader) {
                            imageViewObjectAnimator1.cancel();
                            load_frame.setVisibility(View.GONE);
                        }
                        no_ass_group.setVisibility(View.VISIBLE);
                        no_ass_group.setText("No Assessment Groups Available");
                    }
                });
            }
            try {
                jObj = new JSONObject(result1);
                token=jObj.getString("access_token");
                token_type=jObj.getString("token_type");
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(responseCod != urlConnection.HTTP_OK){

                try{
                    swipe.finishRefresh();
                }catch (Exception e){

                }
                if(flag_loader) {
                    imageViewObjectAnimator1.cancel();
                    load_frame.setVisibility(View.GONE);
                }
                Toast.makeText(LeaderboardActivity.this,"Error while loading user data",Toast.LENGTH_SHORT).show();
                flag_loader=false;
            }else {


                new GetAssGroups().execute();

            }
        }
    }

    private class GetAssGroups extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            no_ass_group.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false);
            leader_list.setLayoutManager(linearLayoutManager);

            leader_list.setAdapter(leaderboardAdapter);
            assgroup_names.clear();
            leadeboard_ranks.clear();
            progress.setText("ASSESSMENT GROUPS");
            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            HttpURLConnection urlConnection1 = null;
            try {



                String uuid=PreferenceUtils.getCandidateId(LeaderboardActivity.this);
                try {
                    try {
                        URL url=new URL(AppConstant.BASE_URL+"userWiseUserGroup/"+uuid);
                        urlConnection1=(HttpURLConnection)url.openConnection();

                        is= urlConnection1.getInputStream();

                        String responseString = readStream(is);
                        Log.e("Course", responseString);
                        result1 = responseString;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_ass_group.setVisibility(View.VISIBLE);
                                no_ass_group.setText("No Assessment Groups Available");
                            }
                        });


                    } catch (IOException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_ass_group.setVisibility(View.VISIBLE);
                                no_ass_group.setText("No Assessment Groups Available");
                            }
                        });


                    }finally {
                        if(urlConnection1!=null){
                            urlConnection1.disconnect();
                        }
                    }
                    return result1;





                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipe.finishRefresh();
                            }catch (Exception e){

                            }
                            if(flag_loader) {
                                imageViewObjectAnimator1.cancel();
                                load_frame.setVisibility(View.GONE);
                            }
                            no_ass_group.setVisibility(View.VISIBLE);
                            no_ass_group.setText("No Assessment Groups Available");
                        }
                    });


                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            assgroup_names.clear();
            Log.e("respmsg",s);
            try{
                swipe.finishRefresh();
            }catch (Exception e){

            }
            if(flag_loader) {
                imageViewObjectAnimator1.cancel();
                load_frame.setVisibility(View.GONE);
            }
           //String ss=new PrefManager(ActiveCoursesActivity.this).getAuthToken().toString();

            if(s.contains("error")){
                Log.e("respmsg","error");
                // swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONObject js=new JSONObject(s);
                    String errormsg=js.getString("error");
                    AppConstant appConstant=new AppConstant(LeaderboardActivity.this);
                    appConstant.showLogoutDialogue(errormsg);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("respmsg","errorexc");
                }

            }else {
                String resp=s;
                Log.e("respmsg","noerror");
                try {
                    no_ass_group.setVisibility(View.GONE);

                    JSONArray jsonArray=new JSONArray(s);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);
                        Log.e("respmsgis",String.valueOf(jsonObject));
                        LeaderBoardAssgroupModel leaderBoardAssgroupModel=new LeaderBoardAssgroupModel();
                        leaderBoardAssgroupModel.setId(jsonObject.getString("_id"));
                        leaderBoardAssgroupModel.setName(jsonObject.getString("displayName"));
                        leaderBoardAssgroupModel.setAss_ids(String.valueOf(jsonObject.getJSONArray("assessmentsList")));
                       /* LearningGroupModel learningGroupModel=new LearningGroupModel();
                        learningGroupModel.setId(jsonObject.getInt("lg_id"));
                        learningGroupModel.setIslock(jsonObject.getString("is_locked"));
                        learningGroupModel.setName(jsonObject.getString("learning_group_name"));
                        learningGroupModel.setStartdate(jsonObject.getString("start_date"));
                        learningGroupModel.setEnddate(jsonObject.getString("end_date"));
                        learningGroupModel.setType(jsonObject.getString("type"));
                        arrayListlg.add(learningGroupModel);
                        Log.e("respmsg","success");
                        learningGroupAdapter.notifyDataSetChanged();*/
                       assgroup_names.add(leaderBoardAssgroupModel);
                       leaderboardAdapter.notifyDataSetChanged();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            if(assgroup_names.size()<1){

                no_ass_group.setVisibility(View.VISIBLE);
                no_ass_group.setText("No Assessment Groups Available");
            }else {
                no_ass_group.setVisibility(View.GONE);
                no_ass_group.setText("No Assessment Groups Available");
            }

            flag_loader=false;
        }
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
    public void startanimation() {

        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }
    private String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }
    public static String readStreamis(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }
    public static class GetRanks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            if(flag_loader) {
                load_frame.setVisibility(View.VISIBLE);
                imageViewObjectAnimator1.start();
            }else {
                load_frame.setVisibility(View.GONE);
            }
            leader_list.setAdapter(leaderBoardRankingAdapter);
            no_ass_group.setVisibility(View.GONE);
           // leadeboard_ranks.clear();
            progress.setText(grp_name);
            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            HttpURLConnection urlConnection1 = null;
            try {


                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(mcontext))
                        .object("CE_Id",ceids)
                        .build().toString();

                try {
                    // httpClient = new DefaultHttpClient();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/get_leaderboard");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    int responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStreamis(is);
                    Log.e("Course", responseString);
                    result1 = responseString;





                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (mcontext instanceof Activity) {
                        ((Activity) mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_ass_group.setVisibility(View.VISIBLE);
                                no_ass_group.setText("No Users Available");
                            }
                        });
                    }

                } catch (ClientProtocolException e) { e.printStackTrace();
                    if (mcontext instanceof Activity) {
                        ((Activity) mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_ass_group.setVisibility(View.VISIBLE);
                                no_ass_group.setText("No Users Available");
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (mcontext instanceof Activity) {
                        ((Activity) mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_ass_group.setVisibility(View.VISIBLE);
                                no_ass_group.setText("No Users Available");
                            }
                        });
                    }
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            leadeboard_ranks.clear();
            Log.e("respmsg",s);
            try{
                swipe.finishRefresh();
            }catch (Exception e){

            }
            if(flag_loader) {
                imageViewObjectAnimator1.cancel();
                load_frame.setVisibility(View.GONE);
            }
            String ss=new PrefManager(mcontext).getAuthToken().toString();


            if(s.contains("error")){
                Log.e("respmsg","error");
                // swipeRefreshLayout.setRefreshing(false);


            }else {
                no_ass_group.setVisibility(View.GONE);

                myName = PreferenceUtils.getCandidateId(mcontext);

                String resp=s;
                Log.e("respmsg","noerror");
                try {
                    JSONArray jsonArray=new JSONArray(s);
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);
                        Log.e("respmsgis",String.valueOf(jsonObject));
                        LeaderBoardUserRankingModel leaderBoardUserRankingModel=new LeaderBoardUserRankingModel();
                        leaderBoardUserRankingModel.setName(jsonObject.getString("name"));
                        leaderBoardUserRankingModel.setRank(jsonObject.getString("rank"));
                        leaderBoardUserRankingModel.setPercent(jsonObject.getString("percent"));
                        leaderBoardUserRankingModel.setUid(jsonObject.getString("U_Id"));
                        leaderBoardUserRankingModel.setScore(jsonObject.getString("score"));

                        leadeboard_ranks.add(leaderBoardUserRankingModel);
                        leaderBoardRankingAdapter.notifyDataSetChanged();
                        if ( !TextUtils.isEmpty(myName) ) {
                            if(myName.equalsIgnoreCase(jsonObject.getString("U_Id"))){
                                name_no=i;

                                new CountDownTimer(500, 500) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        if ( !TextUtils.isEmpty(myName) ) {
                                            leader_list.smoothScrollToPosition(name_no);
                                        } else {

                                            // upper_heading.setText("Welcome ");

                                        }

                                    }
                                }.start();

                            }
                        } else {
                            // upper_heading.setText("Welcome ");

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                leaderBoardRankingAdapter.notifyDataSetChanged();


            }

            if(leadeboard_ranks.size()<1){

                no_ass_group.setVisibility(View.VISIBLE);
                no_ass_group.setText("No Users Available");
            }else {
                no_ass_group.setVisibility(View.GONE);


            }
            flag_loader=false;
        }
    }
    public static void rankGet(String ceid, String name){
        ceid=ceid.substring(1,ceid.length()-1);
        ceids=ceid;
        grp_name=name;
        flag_loader=true;
        new CountDownTimer(100, 50) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                swipe.autoRefresh();

            }
        }.start();
        // new GetRanks().execute();
    }
    @Override
    public void onBackPressed() {
        if(clickis){
            clickis=false;
            name_no=1000;
            ConnectionUtils connectionUtils=new ConnectionUtils(LeaderboardActivity.this);
            if(connectionUtils.isConnectionAvailable()){
                flag_loader=true;
                try {
                    progress.setText("ASSESSMENT GROUPS");
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false);
                    leader_list.setLayoutManager(linearLayoutManager);
                    leader_list.setAdapter(leaderboardAdapter);
                    leaderboardAdapter.notifyDataSetChanged();
                    swipe.autoRefresh();
                } catch (Exception e) {

                }
            }else {
                progress.setText("ASSESSMENT GROUPS");
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false);
                leader_list.setLayoutManager(linearLayoutManager);
                leader_list.setAdapter(leaderboardAdapter);
                leaderboardAdapter.notifyDataSetChanged();
            }

        }else {
            try {
                new GetContacts().cancel(true);
            } catch (Exception e) {

            }
            try {
                new GetRanks().cancel(true);
            } catch (Exception e) {

            }
            finish();
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

        }

    }
}
