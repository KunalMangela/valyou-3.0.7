package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.globalgyan.getvalyou.helper.DataBaseHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ScoreActivity extends AppCompatActivity implements View.OnClickListener{
    Button b1, b2;
    TextView answeris, ans,scoreis, score;
    static int count = 0;
    Typeface tf1;
    DataBaseHelper database;
    Bundle bundle;
    String countno, userscore;
    String countgamecycle;
    String gamename = "Word_Waterfall";
    String totalscore= "Total_Score";
    TextView scorelabel,wordlabel,completedlabel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        scorelabel = (TextView) findViewById(R.id.score_label);
        wordlabel=(TextView)findViewById(R.id.words_label);
        completedlabel=(TextView)findViewById(R.id.completedlabel);


        bundle = getIntent().getExtras();
         userscore = getIntent().getStringExtra("score");
         countno = getIntent().getStringExtra("count");

        count++;

        countgamecycle = String.valueOf(count);

        Date currentDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        String DateToStr = format.format(currentDate);

        //  counterKey = DateToStr + "-counter";




// editor.clear();


        database = new DataBaseHelper(this);
        b1 = (Button)findViewById(R.id.playagn);
        b2 = (Button)findViewById(R.id.quit);
        //answeris = (TextView) findViewById(R.id.txt1);
        ans = (TextView) findViewById(R.id.txt2);
        scoreis = (TextView) findViewById(R.id.txt3);
        score = (TextView) findViewById(R.id.tx4);

        tf1 = Typeface.createFromAsset(getAssets(),"fonts/Kidsn.ttf");

        ans.setTypeface(tf1, Typeface.BOLD);
        score.setTypeface(tf1, Typeface.BOLD);
        b1.setTypeface(tf1, Typeface.BOLD);
        b2.setTypeface(tf1, Typeface.BOLD);
        wordlabel.setTypeface(tf1, Typeface.BOLD);
        scorelabel.setTypeface(tf1, Typeface.BOLD);
        completedlabel.setTypeface(tf1, Typeface.BOLD);

        //answeris.setTypeface(tf1, Typeface.BOLD);
        scoreis.setTypeface(tf1, Typeface.BOLD);


       /* SharedPreferences prefs = getSharedPreferences("scoring", MODE_PRIVATE);
        String countno = prefs.getString("noOfAns", null);
        String userscore = prefs.getString("score", null);*/


        if (countno != null && userscore != null) {
            ans.setText(countno);
            score.setText(userscore);
            database.insertInWordWaterfallscore(gamename, userscore, totalscore , DateToStr, countgamecycle);


        }



        b1.setOnClickListener(this);
        b2.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if(view == b1){

            Intent i = new Intent(ScoreActivity.this, WordGameActivity.class );
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
            i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            finish();
        }
        if(view == b2){


            Intent i = new Intent(ScoreActivity.this, HomeActivity.class );
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
            i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            i.putExtra("tabs","normal");
            startActivity(i);
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            finish();
        }
    }
    @Override
    public void onBackPressed() {


    }
}
