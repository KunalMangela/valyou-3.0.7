package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class AboutUsActivity extends AppCompatActivity {
    ImageView backButton = null;
    ConnectionUtils connectionUtils;
    private WebView webView = null;
    private Dialog dialog = null;
    FloatingActionButton fb;
    boolean flag_netcheck=false;
    KProgressHUD progress;
    Dialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        webView = (WebView) findViewById(R.id.webView);
           backButton = (ImageView) findViewById(R.id.bckbtn);
       // fb=(FloatingActionButton)findViewById(R.id.email);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });
        progress = KProgressHUD.create(AboutUsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setDimAmount(0.7f)
                .setCancellable(false);
      /*  fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","engage@getvalyou.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, ""));
            }
        });*/

        connectionUtils = new ConnectionUtils(this);
        if (connectionUtils.isConnectionAvailable()) {
           /* try {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"example.yahoo.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "App feedback");
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(AboutUsActivity.this, "There are no email client installed on your device.",Toast.LENGTH_SHORT).show();
            }*/


           // progress.show();


            alertDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.planet_loader);
            Window window = alertDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
            alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



            GifView gifView1 = (GifView) alertDialog.findViewById(R.id.loader);
            gifView1.setVisibility(View.VISIBLE);
            gifView1.play();
            gifView1.setGifResource(R.raw.loader_planet);
            gifView1.getGifResource();
            alertDialog.setCancelable(false);
            alertDialog.show();





            final CheckInternetTask t916=new CheckInternetTask();
            t916.execute();

            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    t916.cancel(true);

                    if (flag_netcheck) {
                        flag_netcheck = false;

                        webView.setWebViewClient(new MyBrowser());
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.loadUrl(AppConstant.BASE_URL + "candidateMobileAppStaticPage/AboutUs");
                      //  progress.dismiss();
                        alertDialog.dismiss();

                    }else {
                        alertDialog.dismiss();

                        //  progress.dismiss();
                        flag_netcheck=false;
                        Toast.makeText(AboutUsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    }
                }
            }.start();


        } else {
            Log.e("Not Connected", "to internet");
            Toast.makeText(AboutUsActivity.this, "There is no internet.Please connect internet.", Toast.LENGTH_SHORT).show();
        }


    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            dialog = new Dialog(AboutUsActivity.this, R.style.Theme_Dialog);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            dialog.setCancelable(false);
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dialog.dismiss();
        }
    }


    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)AboutUsActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
    }
}
