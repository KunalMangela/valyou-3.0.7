package com.globalgyan.getvalyou;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.AssessFragment;
import com.globalgyan.getvalyou.model.GameModel;
import com.globalgyan.getvalyou.simulation.SimulationStartPageActivity;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.ArrayList;
import java.util.List;

import de.morrox.fontinator.FontTextView;

public class FunGamesAdapter extends RecyclerView.Adapter<FunGamesAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<FunGameModel> fungame;
    public FunGamesAdapter(Context mContext, ArrayList<FunGameModel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView title,lower_label;
        LinearLayout bg;


        public MyViewHolder(View view) {
            super(view);

            title = (FontTextView) view.findViewById(R.id.title);
            lower_label = (FontTextView) view.findViewById(R.id.lower_label);
            bg=(LinearLayout) view.findViewById(R.id.main_bg);

        }
    }




    @Override
    public FunGamesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_list_items, parent, false);




        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FunGameModel funGameModel=fungame.get(position);
        holder.title.setText(funGameModel.getGamename());
        holder.lower_label.setText(funGameModel.getSubname());

        holder.bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionUtils connectionUtils=new ConnectionUtils(mContext);
                if(connectionUtils.isConnectionAvailable()) {
                    if (new PrefManager(mContext).getRetryresponseString().equalsIgnoreCase("notdone")) {
                        Toast.makeText(mContext, "Video uploading is in progrss,Can't access game right now", Toast.LENGTH_SHORT).show();

                    } else {


                        AssessFragment.waterfall_visibility=false;
                        Intent i = new Intent(mContext, WordGameActivity.class);
                        mContext.startActivity(i);

                    }
                }else {
                    Toast.makeText(mContext,"Please check your internet connection !",Toast.LENGTH_SHORT).show();


                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return fungame.size();
    }
}


