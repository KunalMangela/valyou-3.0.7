package com.globalgyan.getvalyou.listeners;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by Globalgyan on 21-12-2017.
 */

public class TimerService extends Service {

    /** indicates how to behave if the service is killed */
    int mStartMode;
    String currentDateTimeString;
    Calendar today;
    /** interface for clients that bind */
    IBinder mBinder;

    /** indicates whether onRebind should be used */
    boolean mAllowRebind;
    long sinceMid;
    /** Called when the service is being created. */
    @Override
    public void onCreate() {

    }

    /** The service is starting, due to a call to startService() */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        today = Calendar.getInstance();
        today.set(Calendar.SECOND, 59);
        today.set(Calendar.MINUTE, 59);
        today.set(Calendar.HOUR_OF_DAY, 23
        );
         sinceMid = (today.getTimeInMillis()) %
                (24 * 60 * 60 * 1000);
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

               /* currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                Log.e("timeis",currentDateTimeString);
*/

        /*long offset = today.get(Calendar.ZONE_OFFSET) +
                today.get(Calendar.DST_OFFSET);*/
                long availablesec=(System.currentTimeMillis()) %
                        (24 * 60 * 60 * 1000);
                long countdowntime=Long.parseLong(String.valueOf(Integer.parseInt(String.valueOf(sinceMid))-Integer.parseInt(String.valueOf(availablesec))));
                SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                long mls=countdowntime*1000;
                int hrs = (int) TimeUnit.MILLISECONDS.toHours(mls) % 24;
                int min = (int) TimeUnit.MILLISECONDS.toMinutes(mls) % 60;
                int sec = (int) TimeUnit.MILLISECONDS.toSeconds(mls) % 60;
                Log.e("dateis",hrs+":"+min+":"+sec);


            }
        }, 0, 1000);
        return mStartMode;
    }

    /** A client is binding to the service with bindService() */

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}