package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.cms.response.ForgetPasswordResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.ValidationUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

/**
 * Created by NaNi on 09/09/17.
 */

public class ForgotActivity extends AppCompatActivity implements GUICallback{

    TextInputLayout phntw;
    FormEditText phn;
    String type="";
    AppCompatButton reset;
    String signup="";
   // LinearLayout ll;
    ConnectionUtils connectionUtils;
    Dialog progressHUD;
    FrameLayout main_bg;
    LinearLayout linear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#06030E"));
        }
        Typeface tf2 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");

        phntw=(TextInputLayout)findViewById(R.id.input_phone_tp);
        phn=(FormEditText) findViewById(R.id.input_phone);
        reset=(AppCompatButton)findViewById(R.id.reset);
        main_bg=(FrameLayout) findViewById(R.id.main_bg);
        linear=(LinearLayout) findViewById(R.id.linear);


        phn.setTypeface(tf2);
        reset.setTypeface(tf2);
       createProgress();
       // signupp=(AppCompatButton)findViewById(R.id.signup_forgot);
       // ll=(LinearLayout)findViewById(R.id.linearLayout2);
        connectionUtils=new ConnectionUtils(ForgotActivity.this);
        try{
            type=getIntent().getStringExtra("type");
            if(type.equalsIgnoreCase("socialsignin")){
                Log.e("FFF",type.equalsIgnoreCase("socialsignin")+"");
                reset.setText("SUBMIT");
            }


        }catch (Exception eee){

        }


        try {
            signup = getIntent().getStringExtra("SIGNUP");
//        Log.e("SSS",signup);
            if (signup.equalsIgnoreCase("yes")) {
                reset.setText("SUBMIT");
                //ll.setVisibility(View.GONE);
            } else {
                Log.e("SSS", signup);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        main_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                    reset.setEnabled(true);
                    }
                }.start();
                    if(connectionUtils.isConnectionAvailable()){


                            reset.setEnabled(false);
                            reset.setBackgroundColor(Color.parseColor("#ABBAC2"));
                            if(signup.equalsIgnoreCase("yes")){
                                progressHUD.show();
                                new CountDownTimer(1000, 1000) {
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ForgotActivity.this, ForgotActivity.this);
                                        processor.sendOtp(phn.getText().toString());
                                    }
                                }.start();

                            }else{
                                progressHUD.show();
                                new CountDownTimer(1000, 1000) {
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ForgotActivity.this, ForgotActivity.this);
                                        processor.forgotPassword(phn.getText().toString());
                                    }
                                }.start();


                            }

                            new CountDownTimer(5000, 5000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    reset.setEnabled(true);
                                    reset.setBackgroundColor(Color.parseColor("#2f27c9"));

                                }
                            }.start();


                    }else {
                        Toast.makeText(ForgotActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
                    }




            }
        });

       /* signupp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent forgot = new Intent(ForgotActivity.this, ForgotActivity.class);
                forgot.putExtra("SIGNUP","YES");
                startActivity(forgot);
                finish();

            }
        });*/


    }

    private void createProgress() {
        progressHUD = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        progressHUD.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressHUD.setContentView(R.layout.planet_loader);
        Window window = progressHUD.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progressHUD.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progressHUD.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progressHUD.setCancelable(false);
      //  progressHUD.show();

    }

    @Override
    public void requestProcessed( ValYouResponse guiResponse, RequestStatus status) {
        if(progressHUD.isShowing()&&progressHUD!=null){
            progressHUD.dismiss();
        }
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {

                if (guiResponse instanceof ForgetPasswordResponse) {
                    ForgetPasswordResponse response = (ForgetPasswordResponse) guiResponse;
                    if (response.isStatus()) {

                        Intent intent = new Intent(ForgotActivity.this, EnterOtpActivity.class);
                        intent.putExtra("otpf","no");
                        intent.putExtra("phn", response.getMobile());
                        intent.putExtra("id", response.get_id());
                        intent.putExtra("emailis", phn.getText().toString());
                        intent.putExtra("session_id", "jok");
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);



                    } else {
                        if (!TextUtils.isEmpty(response.getMessage()))
                            Toast.makeText(ForgotActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(ForgotActivity.this, "This phone number is not registered with us", Toast.LENGTH_SHORT).show();
                    }

                } else if (guiResponse instanceof SendOtpResponse) {
                    SendOtpResponse sendOtpResponse = (SendOtpResponse) guiResponse;
                    if (sendOtpResponse.isStatus()) {
                        Intent intent = new Intent(ForgotActivity.this, EnterOtpActivity.class);
                        intent.putExtra("otpf","yes");
                        intent.putExtra("phn", phn.getText().toString());
                        intent.putExtra("session_id", sendOtpResponse.getDetails());
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                }
            } else {
                Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }
}
