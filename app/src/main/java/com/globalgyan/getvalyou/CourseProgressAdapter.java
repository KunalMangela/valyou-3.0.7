package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.globalgyan.getvalyou.apphelper.PrefManager;

import java.util.ArrayList;

import static com.thefinestartist.utils.content.ContextUtil.startActivity;

public class CourseProgressAdapter extends RecyclerView.Adapter<CourseProgressAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<LearningGroupCourseProgressModel> fungame;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,progress;
        SeekBar progress_seekbar;
        LinearLayout main_linear;
        public MyViewHolder(View view) {
            super(view);

            name=(TextView)view.findViewById(R.id.coursename);
            progress=(TextView)view.findViewById(R.id.progress);
            progress_seekbar=(SeekBar)view.findViewById(R.id.seekBar_luminosite);
            main_linear=(LinearLayout)view.findViewById(R.id.main_linear);

        }
    }


    public CourseProgressAdapter(Context mContext, ArrayList<LearningGroupCourseProgressModel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.course_litem_ayout, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final LearningGroupCourseProgressModel funModel=fungame.get(position);
        holder.progress_seekbar.setEnabled(false);
        holder.name.setText(funModel.getCoursename());
        holder.name.setSelected(true);
        Typeface typeface1 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.name.setTypeface(typeface1,Typeface.NORMAL);
        int pris= Integer.parseInt(funModel.getPerecentage());
        holder.progress_seekbar.setProgress(pris);
        holder.progress.setText(String.valueOf(funModel.getPerecentage())+"%");
        if(pris>30){
            holder.progress_seekbar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progressgreen));
        }else {
            holder.progress_seekbar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress));
        }
        holder.progress.setTypeface(typeface1,Typeface.BOLD);
        // holder.progress.setText(funModel.getName()+" %");

        holder.main_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CourseLIstActivity.class);
                // intent.putExtra("tabs", "normal");
                new PrefManager(mContext).saveLearningGroups(String.valueOf(funModel.getLgid()));
                new PrefManager(mContext).saveNav("learn");
                new PrefManager(mContext).saveLearnNav("course_progress");
                if (mContext instanceof Activity) {
                    ((Activity) mContext).startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }
              /*  if(funModel.getContent().length()<6){
                    Toast.makeText(mContext,"No data present in a course",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(mContext, ModulesActivity.class);
                    intent.putExtra("content", funModel.getContent());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                }*/
            }
        });


    }


    @Override
    public int getItemCount() {
        return fungame.size();
    }

    public static void clickCourses(){

    }
}

