package com.globalgyan.getvalyou;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.AssessFragment;
import com.globalgyan.getvalyou.model.GameModel;
import com.globalgyan.getvalyou.simulation.SimulationStartPageActivity;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import org.androidannotations.annotations.App;

import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Globalgyan on 06-12-2017.
 */

public class TodaysGameAdapter extends RecyclerView.Adapter<TodaysGameAdapter.MyViewHolder> {

    private Context mContext;
    private static List<ToadysgameModel> todaysgamelist;
    ConnectionUtils connectionUtils;
    long mLastClickTime = 0;
    int sm_ref_id,ass_ref_id;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView certification_name,start,end,groupname,game_type,assessment_name,playedtext,category,desc_arrow,description_text;
        ImageView certilabel,certilabel1,assessment_cover;
        FrameLayout bg,certi_start_line,certi_end_line,description_button;
        RelativeLayout certificatename_linear,description_relative,played_layout,lock_layout;
        LinearLayout description_linear;


        public MyViewHolder(View view) {
            super(view);

            assessment_name = (TextView) view.findViewById(R.id.title);
            category = (TextView) view.findViewById(R.id.category);
            bg=(FrameLayout) view.findViewById(R.id.main_fr);
            playedtext=(TextView)view.findViewById(R.id.playedtext);
            lock_layout=(RelativeLayout)view.findViewById(R.id.lock_layout);
            played_layout=(RelativeLayout)view.findViewById(R.id.played_layout);
            description_text=(TextView)view.findViewById(R.id.description_text);
            description_relative=(RelativeLayout)view.findViewById(R.id.description_relative);
            groupname=(TextView)view.findViewById(R.id.groupname);
            description_linear=(LinearLayout)view.findViewById(R.id.description_linear);
            game_type=(TextView)view.findViewById(R.id.game_type);
            desc_arrow=(TextView)view.findViewById(R.id.desc_arrow);
            description_button=(FrameLayout) view.findViewById(R.id.description_button);
            assessment_cover=(ImageView)view.findViewById(R.id.assessment_cover);
            certilabel=(ImageView)view.findViewById(R.id.certilabel);
            certilabel1=(ImageView)view.findViewById(R.id.certilabel1);
            certification_name=(TextView)view.findViewById(R.id.certification_name);
            start=(TextView)view.findViewById(R.id.starttext);
            certificatename_linear=(RelativeLayout)view.findViewById(R.id.certificatename_linear);

            end=(TextView)view.findViewById(R.id.endtext);
            certi_start_line=(FrameLayout)view.findViewById(R.id.certi_start_line);
            certi_end_line=(FrameLayout)view.findViewById(R.id.certi_end_line);

        }
    }


    public TodaysGameAdapter(Context mContext, List<ToadysgameModel> todaysgamelist) {
        this.mContext = mContext;
        this.todaysgamelist = todaysgamelist;
        connectionUtils=new ConnectionUtils(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_list_items, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ToadysgameModel gameModel = todaysgamelist.get(position);
        holder.setIsRecyclable(false);

        Log.e("posis",""+position);
      //  holder.assessment_name.setSelected(true);
       // holder.category.setSelected(true);

        holder.assessment_name.setText(gameModel.getName());
        holder.category.setText(gameModel.getAss_category());
        holder.groupname.setText(gameModel.getAss_group());

       /* if(gameModel.getAss_game().equalsIgnoreCase("msq")){
            holder.game_type.setText("Type: Master-Slave Questions");

        }else if(gameModel.getAss_game().equalsIgnoreCase("Radio Tuner")){
            holder.game_type.setText("Type: Master Audio Questions");

        }else if(gameModel.getAss_game().equalsIgnoreCase("vr")){
            holder.game_type.setText("Type: Video Response Questions");

        }else {
            holder.game_type.setText("Type: Multiple Choice Questions");

        }*/
        if(gameModel.getType_is().equalsIgnoreCase("simulation")) {
            Glide.with(mContext).load(gameModel.getSm_icon())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.assessment_cover);
        }else {
            Glide.with(mContext).load(gameModel.getUrl_ic())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.assessment_cover);

        }
        holder.description_text.setText(gameModel.getAss_descgame());


        /*if(AssessFragment.desc_statuslist.get(position).equalsIgnoreCase("close")){
            holder.description_relative.setVisibility(View.GONE);
            holder.desc_arrow.setBackgroundResource(R.drawable.ic_arrow_drop_down_white_24dp);
        }else {
            holder.description_relative.setVisibility(View.VISIBLE);
            holder.desc_arrow.setBackgroundResource(R.drawable.ic_arrow_drop_up_white_24dp);

        }*/

        if(gameModel.getType_is().equalsIgnoreCase("simulation")) {
            holder.category.setText("Category: Simulation");
            holder.game_type.setText("Type: Decision Making");
            holder.groupname.setText(gameModel.getAss_group());
            holder.description_text.setText(gameModel.getSm_desc());


        }else {

        }

            Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.assessment_name.setTypeface(tf2);
        holder.category.setTypeface(tf2);

        holder.assessment_name.setTypeface(tf2);
        holder.groupname.setTypeface(tf2);
        holder.game_type.setTypeface(tf2);
     //   holder.description_button.setTypeface(tf2);
        holder.playedtext.setTypeface(tf2,Typeface.BOLD);
        holder.end.setTypeface(tf2);
        holder.start.setTypeface(tf2);
        holder.description_text.setTypeface(tf2);
        holder.certification_name.setTypeface(tf2,Typeface.BOLD);

        if (!gameModel.getStatus().equalsIgnoreCase("Played")) {

            if(gameModel.getLock_status().equalsIgnoreCase("unlocked")){
              //  holder.status_icon_lock.setVisibility(View.GONE);
              //  holder.im_status.setVisibility(View.VISIBLE);
                holder.lock_layout.setVisibility(View.GONE);
                holder.played_layout.setVisibility(View.GONE);
                holder.description_linear.setVisibility(View.VISIBLE);
                holder.bg.setAlpha(1f);
              //  holder.bg.setBackgroundResource(R.drawable.ass_ripple);
             //   holder.im_status.setImageResource(R.drawable.new_ass);
            }else{
               // holder.status_icon_lock.setVisibility(View.VISIBLE);
                holder.lock_layout.setVisibility(View.VISIBLE);
                holder.played_layout.setVisibility(View.GONE);
                holder.description_linear.setVisibility(View.VISIBLE);
            //    holder.bg.setBackgroundResource(R.drawable.ass_ripple);
               // holder.im_status.setVisibility(View.GONE);
            }


        }else {
           // holder.status_icon_lock.setVisibility(View.GONE);
          //  holder.im_status.setVisibility(View.VISIBLE);
              //  holder.bg.setAlpha(0.8f);
          //  holder.bg.setBackgroundResource(R.drawable.module_is);
          //  holder.im_status.setImageResource(R.drawable.old_ass_grey);
          //  holder.im_status.setAlpha(0.8f);
            holder.lock_layout.setVisibility(View.GONE);
            holder.played_layout.setVisibility(View.VISIBLE);
            holder.description_linear.setVisibility(View.GONE);
        }


        Log.e("pos", ""+position);
        //logic
      /*  if(!AssessFragment.list_lg_ids.get(position).equalsIgnoreCase("0")){
            //not open course ass

            if(AssessFragment.list_certi_ids.get(position).equalsIgnoreCase("0")){
                //normal course
                Log.e("posval", "course ass");


                holder.start.setText("Course");
                holder.certilabel.setVisibility(View.GONE);
                holder.certilabel1.setVisibility(View.GONE);
                //certi
                if (position == 0) {

                    if (!AssessFragment.list_course_ids.get(position).equalsIgnoreCase("0")) {
                        if (!AssessFragment.list_course_ids.get(position).equalsIgnoreCase(AssessFragment.list_course_ids.get(position + 1))) {
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                            holder.certification_name.setText(AssessFragment.course_name.get(position));
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                            Log.e("posval", "first element course ass up and down same");

                        } else {

                            if (AssessFragment.list_lg_ids.get(position).equals(AssessFragment.list_lg_ids.get(position + 1))) {
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                                holder.certification_name.setText(AssessFragment.course_name.get(position));
                                holder.certi_end_line.setVisibility(View.GONE);
                                Log.e("posval", "first element course ass down  same");

                            } else {
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                                holder.certification_name.setText(AssessFragment.course_name.get(position));
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                Log.e("posval", "first element course ass up and down same");

                            }


                        }
                    } else {
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                        Log.e("posval", "first elementnot course ass");

                    }
                }
                if (position == todaysgamelist.size() - 1) {
                    if (!AssessFragment.list_course_ids.get(position).equalsIgnoreCase("0")) {
                        if (!AssessFragment.list_course_ids.get(position).equalsIgnoreCase(AssessFragment.list_course_ids.get(position - 1))) {
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                            holder.certification_name.setText(AssessFragment.course_name.get(position));
                            Log.e("posval", "last element course ass up and down same");


                        } else {
                            //need changes
                            if (AssessFragment.list_lg_ids.get(position).equals(AssessFragment.list_lg_ids.get(position - 1))) {
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certi_start_line.setVisibility(View.GONE);
                                holder.certificatename_linear.setVisibility(View.GONE);
                                Log.e("posval", "last element course ass up same");

                            } else {
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                                Log.e("posval", "last element course ass up diff");
                            }


                        }


                    } else {
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                        Log.e("posval", "last element not course ass");

                    }
                }

                if ((position != 0) && (position != todaysgamelist.size() - 1)) {
                    if (!AssessFragment.list_course_ids.get(position).equalsIgnoreCase("0")) {
                        String A, B, C;
                        String A_lg, B_lg, C_lg;

                        A = AssessFragment.list_course_ids.get(position - 1);
                        B = AssessFragment.list_course_ids.get(position);
                        C = AssessFragment.list_course_ids.get(position + 1);

                        A_lg = AssessFragment.list_lg_ids.get(position - 1);
                        B_lg = AssessFragment.list_lg_ids.get(position);
                        C_lg = AssessFragment.list_lg_ids.get(position + 1);


                        if ((!A.equals(B)) && (!B.equals(C))) {
                            Log.e("posval", "first if up and down diff");

                            holder.certification_name.setText(AssessFragment.course_name.get(position));
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                        }

                        if ((A.equals(B)) && (B.equals(C))) {
                            Log.e("posval", "second if up and down same");
                            if ((A_lg.equals(B_lg)) && (B_lg.equals(C_lg))) {
                                holder.certi_start_line.setVisibility(View.GONE);
                                holder.certi_end_line.setVisibility(View.GONE);
                                holder.certificatename_linear.setVisibility(View.GONE);
                                Log.e("posval", "second if 2_ up and down same lg");

                            } else {
                                if (A_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.GONE);
                                    holder.certi_end_line.setVisibility(View.GONE);
                                    holder.certificatename_linear.setVisibility(View.GONE);
                                    Log.e("posval", "second if 2_up same lgid");

                                }
                                if (C_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.GONE);
                                    holder.certi_end_line.setVisibility(View.GONE);
                                    holder.certificatename_linear.setVisibility(View.GONE);
                                    Log.e("posval", "second if 2_down same lgid");

                                }
                                if (!A_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.VISIBLE);
                                    holder.certi_end_line.setVisibility(View.GONE);
                                    holder.certificatename_linear.setVisibility(View.VISIBLE);
                                    Log.e("posval", "second if 2_up not same lgid");

                                }
                                if (!C_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.GONE);
                                    holder.certi_end_line.setVisibility(View.VISIBLE);
                                    holder.certificatename_linear.setVisibility(View.GONE);
                                    Log.e("posval", "second if 2_down not same lgid");

                                }
                                if ((!A_lg.equals(B_lg)) && (!B_lg.equals(C_lg))) {
                                    holder.certi_start_line.setVisibility(View.VISIBLE);
                                    holder.certi_end_line.setVisibility(View.VISIBLE);
                                    holder.certificatename_linear.setVisibility(View.VISIBLE);
                                    Log.e("posval", "second if 2_not same lgid_both");

                                }

                            }


                        }

                        if ((!A.equals(B)) && (B.equals(C))) {
                            Log.e("posval", "tird if up diff and down same");

                            if (B_lg.equals(C_lg)) {
                                Log.e("posval", "third if 2_down same");

                                holder.certification_name.setText(AssessFragment.course_name.get(position));
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certi_end_line.setVisibility(View.GONE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                            } else {
                                Log.e("posval", "third if 2_down diff");

                                holder.certification_name.setText(AssessFragment.course_name.get(position));
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                            }


                        }

                        if ((A.equals(B)) && (!B.equals(C))) {
                            Log.e("posval", "fourth if up same and down diff");

                            // holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                            if (A_lg.equals(B_lg)) {
                                Log.e("posval", "fourth if  4_up same");

                                holder.certi_start_line.setVisibility(View.GONE);
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.GONE);
                            } else {
                                Log.e("posval", "fourth if 4_up diff");

                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                            }
                        }

                    } else {
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                        Log.e("posval", "middle no course ass");
                    }
                }
            }else {

                Log.e("posval", "certification course ass");

              //  holder.start.setText("Certification");

                holder.certilabel.setVisibility(View.VISIBLE);
                holder.certilabel1.setVisibility(View.VISIBLE);
                //certi
                if (position == 0) {
                    Log.e("pos", "0");
                    if (!AssessFragment.list_certi_ids.get(position).equalsIgnoreCase("0")) {
                        if (!AssessFragment.list_certi_ids.get(position).equalsIgnoreCase(AssessFragment.list_certi_ids.get(position + 1))) {
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                            holder.certification_name.setText(AssessFragment.certi_name.get(position));
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                        } else {

                            if (AssessFragment.list_lg_ids.get(position).equals(AssessFragment.list_lg_ids.get(position + 1))) {
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                                holder.certification_name.setText(AssessFragment.certi_name.get(position));
                                holder.certi_end_line.setVisibility(View.GONE);
                            } else {
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                                holder.certification_name.setText(AssessFragment.certi_name.get(position));
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                            }


                        }
                    } else {
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                    }
                }
                if (position == todaysgamelist.size() - 1) {
                    Log.e("pos", "1");
                    if (!AssessFragment.list_certi_ids.get(position).equalsIgnoreCase("0")) {
                        if (!AssessFragment.list_certi_ids.get(position).equalsIgnoreCase(AssessFragment.list_certi_ids.get(position - 1))) {
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                            holder.certification_name.setText(AssessFragment.certi_name.get(position));

                        } else {
                            if (AssessFragment.list_lg_ids.get(position).equals(AssessFragment.list_lg_ids.get(position - 1))) {
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certi_start_line.setVisibility(View.GONE);
                                holder.certificatename_linear.setVisibility(View.GONE);
                            } else {
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                            }


                        }


                    } else {
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                    }
                }

                if ((position != 0) && (position != todaysgamelist.size() - 1)) {
                    Log.e("pos", "other");
                    if (!AssessFragment.list_certi_ids.get(position).equalsIgnoreCase("0")) {
                        String A, B, C;
                        String A_lg, B_lg, C_lg;

                        A = AssessFragment.list_certi_ids.get(position - 1);
                        B = AssessFragment.list_certi_ids.get(position);
                        C = AssessFragment.list_certi_ids.get(position + 1);

                        A_lg = AssessFragment.list_lg_ids.get(position - 1);
                        B_lg = AssessFragment.list_lg_ids.get(position);
                        C_lg = AssessFragment.list_lg_ids.get(position + 1);


                        if ((!A.equals(B)) && (!B.equals(C))) {
                            holder.certification_name.setText(AssessFragment.certi_name.get(position));
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                        }

                        if ((A.equals(B)) && (B.equals(C))) {
                            if ((A_lg.equals(B_lg)) && (B_lg.equals(C_lg))) {
                                holder.certi_start_line.setVisibility(View.GONE);
                                holder.certi_end_line.setVisibility(View.GONE);
                                holder.certificatename_linear.setVisibility(View.GONE);
                            } else {
                                if (A_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.GONE);
                                    holder.certi_end_line.setVisibility(View.GONE);
                                    holder.certificatename_linear.setVisibility(View.GONE);
                                }
                                if (C_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.GONE);
                                    holder.certi_end_line.setVisibility(View.GONE);
                                    holder.certificatename_linear.setVisibility(View.GONE);
                                }
                                if (!A_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.VISIBLE);
                                    holder.certi_end_line.setVisibility(View.GONE);
                                    holder.certificatename_linear.setVisibility(View.VISIBLE);
                                }
                                if (!C_lg.equals(B_lg)) {
                                    holder.certi_start_line.setVisibility(View.GONE);
                                    holder.certi_end_line.setVisibility(View.VISIBLE);
                                    holder.certificatename_linear.setVisibility(View.GONE);
                                }
                                if ((!A_lg.equals(B_lg)) && (!B_lg.equals(C_lg))) {
                                    holder.certi_start_line.setVisibility(View.VISIBLE);
                                    holder.certi_end_line.setVisibility(View.VISIBLE);
                                    holder.certificatename_linear.setVisibility(View.VISIBLE);
                                }

                            }


                        }

                        if ((!A.equals(B)) && (B.equals(C))) {
                            if (B_lg.equals(C_lg)) {
                                holder.certification_name.setText(AssessFragment.certi_name.get(position));
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certi_end_line.setVisibility(View.GONE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                            } else {
                                holder.certification_name.setText(AssessFragment.certi_name.get(position));
                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                            }


                        }

                        if ((A.equals(B)) && (!B.equals(C))) {
                            // holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                            if (A_lg.equals(B_lg)) {

                                holder.certi_start_line.setVisibility(View.GONE);
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.GONE);
                            } else {

                                holder.certi_start_line.setVisibility(View.VISIBLE);
                                holder.certi_end_line.setVisibility(View.VISIBLE);
                                holder.certificatename_linear.setVisibility(View.VISIBLE);
                            }
                        }

                    } else {
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                    }
                }

//end certi
            }



        }else {
            //open course ass
            Log.e("posval", "normal ass");
        }*/


        holder.certi_start_line.setVisibility(View.GONE);
        holder.certificatename_linear.setVisibility(View.GONE);
       // holder.certification_name.setText(AssessFragment.course_name.get(position));
        holder.certi_end_line.setVisibility(View.GONE);

        holder.bg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN &&(!gameModel.getStatus().equalsIgnoreCase("Played")&&gameModel.getLock_status().equalsIgnoreCase("unlocked"))) {

                    holder.bg.setBackgroundColor(Color.parseColor("#ffffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            holder.bg.setBackgroundResource(R.drawable.rounded_corner_courselist_items);

                        }
                    }.start();

                }


                return false;
            }
        });


        holder.description_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN &&(!gameModel.getStatus().equalsIgnoreCase("Played")&&gameModel.getLock_status().equalsIgnoreCase("unlocked"))) {

                    holder.description_button.setBackgroundResource(R.drawable.play_purple_ripple);

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            holder.description_button.setBackgroundResource(R.drawable.play_purple_round);

                        }
                    }.start();

                }


                return false;
            }
        });


      /*  holder.description_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!gameModel.getStatus().equalsIgnoreCase("Played")) {
                    String status_is=AssessFragment.desc_statuslist.get(position);
                    if(status_is.equalsIgnoreCase("close")){
                        AssessFragment.desc_statuslist.set(position,"open");
                        holder.description_relative.setVisibility(View.VISIBLE);
                        holder.desc_arrow.setBackgroundResource(R.drawable.ic_arrow_drop_up_white_24dp);
                    }else {
                        AssessFragment.desc_statuslist.set(position,"close");
                        holder.description_relative.setVisibility(View.GONE);
                        holder.desc_arrow.setBackgroundResource(R.drawable.ic_arrow_drop_down_white_24dp);
                    }

                }else {

                }
            }
        });*/




        holder.bg.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                holder.bg.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        holder.bg.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {
                    if (new PrefManager(mContext).getRetryresponseString().equalsIgnoreCase("notdone")) {
                        AssessFragment.noassessmentavailableframe.setVisibility(View.VISIBLE);
                        AssessFragment.noavailabletext.setText("Video uploading is in progrss,Can't access game right now");
                        //Toast.makeText(mContext, "Video uploading is in progrss,Can't access game right now", Toast.LENGTH_SHORT).show();

                    } else {
                        if (connectionUtils.isConnectionAvailable()) {
                            if (!gameModel.getStatus().equalsIgnoreCase("Played")) {



                                try {
                                    String status_email=new PrefManager(mContext).getemailverificationString();
                                    if (status_email.equalsIgnoreCase("verified")) {

                                        if(gameModel.getType_is().equalsIgnoreCase("assessment")){

                                            if(gameModel.getLock_status().equalsIgnoreCase("unlocked")) {

                                                new PrefManager(mContext).saveTargetid(String.valueOf(gameModel.getIdis()));
                                                ass_ref_id = 0;
                                                new PrefManager(mContext).saveRatingtype("assessment_group");



                                                new PrefManager(mContext).savelgid_ass(gameModel.getLg_id());
                                                new PrefManager(mContext).savecourseid_ass(gameModel.getCourse_id());
                                                new PrefManager(mContext).savecertiid_ass(gameModel.getCertification_id());


                                                Intent it = new Intent(mContext, AssessLanding.class);
                                                it.putExtra("uidg", gameModel.getAss_uid());
                                                it.putExtra("adn", gameModel.getName());
                                                it.putExtra("ceid", gameModel.getIdis());
                                                it.putExtra("gdid", gameModel.getAss_gdid());
                                                it.putExtra("category", gameModel.getAss_category());
                                                it.putExtra("game", gameModel.getAss_game());
                                                it.putExtra("tgid", gameModel.getTg_id());
                                                it.putExtra("aid", gameModel.ass_groupid);
                                                it.putExtra("assessgroup", gameModel.getAss_group());
                                                it.putExtra("token", HomeActivity.token);
                                                it.putExtra("description_is",gameModel.getAss_descgame());
                                                it.putExtra("time_is", gameModel.getAss_gametime());
                                                it.putExtra("pic_frequency",gameModel.getPic_frequency());
                                                it.putExtra("TG_GroupId",gameModel.getAss_groupid());
                                                it.putExtra("certification_id",gameModel.getCertification_id());
                                                it.putExtra("lg_id",gameModel.getLg_id());
                                                it.putExtra("course_id",gameModel.getCourse_id());


                                                Log.e("uidg", gameModel.getAss_uid());
                                                Log.e("adn", gameModel.getName());
                                                Log.e("ceid", gameModel.getIdis());
                                                Log.e("gdid", gameModel.getAss_gdid());
                                                Log.e("category", gameModel.getAss_category());
                                                Log.e("game", gameModel.getAss_game());
                                                Log.e("tgid", gameModel.getTg_id());
                                                Log.e("aid", gameModel.ass_groupid);
                                                Log.e("assessgroup", gameModel.getAss_group());
                                                Log.e("token", HomeActivity.token);
                                                Log.e("description_is",gameModel.getAss_descgame());
                                                Log.e("time_is", gameModel.getAss_gametime());
                                                Log.e("pic_frequency",""+gameModel.getPic_frequency());
                                                Log.e("TG_GroupId",gameModel.getAss_groupid());
                                                Log.e("certification_id",gameModel.getCertification_id());
                                                Log.e("lg_id",gameModel.getLg_id());
                                                Log.e("course_id",gameModel.getCourse_id());

                                                mContext.startActivity(it);
                                                if (mContext instanceof Activity) {
                                                    ((Activity) mContext).finish();
                                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                                }


                                            }else {
                                                AssessFragment.noassessmentavailableframe.setVisibility(View.VISIBLE);
                                                AssessFragment.noavailabletext.setText("This game will be available at date:"+gameModel.getAvailable_date());
                                                //  Toast.makeText(mContext, "This game will be available at date:"+gameModel.getAvailable_date(), Toast.LENGTH_SHORT).show();


                                            }



                                        }
                                        if(gameModel.getType_is().equalsIgnoreCase("simulation")) {
                                   /* int p=todaysgamelist.size()-AssessFragment.countavailablegame;

                                    int val=p-position;
                                    val=AssessFragment.simulation_list_arraylist.size()-val;*/


                                            new PrefManager(mContext).saveTargetid(String.valueOf(gameModel.getIdis()));
                                            new PrefManager(mContext).saveRatingtype("simulation");
                                            sm_ref_id = 0;


                                            if (gameModel.getSm_type().equalsIgnoreCase("articulate")) {
                                                new PrefManager(mContext).set_sm_nav_from("ass");
                                                Intent i = new Intent(mContext, WebviewVideoActivity.class);


                                                i.putExtra("link", gameModel.getSm_articulate_url());
                                                mContext.startActivity(i);
                                                if (mContext instanceof Activity) {
                                                    ((Activity) mContext).finish();
                                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                                }
                                            } else {

                                                Intent i = new Intent(mContext, SimulationStartPageActivity.class);
                                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                i.putExtra("sm_name", gameModel.getName());
                                                i.putExtra("sm_landing_page_image", gameModel.getSm_landingpage_img());
                                                i.putExtra("sm_bg_color", gameModel.getSm_bgcolor());
                                                i.putExtra("sm_bg_image", gameModel.getSm_bg_img());
                                                i.putExtra("sm_decision_bg_color", gameModel.getSm_dc_bgcolor());
                                                i.putExtra("sm_decision_bg_image", gameModel.getSm_bg_img());
                                                i.putExtra("sm_bg_sound", gameModel.getSm_bgsound());
                                                i.putExtra("sm_icon", gameModel.getSm_icon());
                                                i.putExtra("sm_id", gameModel.getIdis());
                                                i.putExtra("sm_company", gameModel.getSm_company());
                                                i.putExtra("sm_icon_image", gameModel.getSm_ic_img());
                                                i.putExtra("gif_file", gameModel.getSm_gif());
                                                i.putExtra("sm_description", gameModel.getSm_desc());
                                                i.putExtra("sm_gif_loading_time", gameModel.getSm_giftime());
                                                i.putExtra("sm_title_color", gameModel.getSm_titlecolor());
                                                i.putExtra("sm_title_description_color", gameModel.getSm_title_desc_color());


                                                Log.e("sm_name", gameModel.getName());
                                                Log.e("sm_landing_page_image", gameModel.getSm_landingpage_img());
                                                Log.e("sm_bg_color", gameModel.getSm_bgcolor());
                                                Log.e("sm_bg_image", gameModel.getSm_bg_img());
                                                Log.e("sm_decision_bg_color", gameModel.getSm_dc_bgcolor());
                                                Log.e("sm_decision_bg_image", gameModel.getSm_bg_img());
                                                Log.e("sm_bg_sound", gameModel.getSm_bgsound());
                                                Log.e("sm_icon", gameModel.getSm_icon());
                                                Log.e("sm_id", gameModel.getIdis());
                                                Log.e("sm_company", gameModel.getSm_company());
                                                Log.e("sm_icon_image", gameModel.getSm_ic_img());
                                                Log.e("gif_file", gameModel.getSm_gif());
                                                Log.e("sm_description", gameModel.getSm_desc());
                                                Log.e("sm_gif_loading_time", gameModel.getSm_giftime());
                                                Log.e("sm_title_color", gameModel.getSm_titlecolor());
                                                Log.e("sm_title_description_color", gameModel.getSm_title_desc_color());


                                                mContext.startActivity(i);

                                                if (mContext instanceof Activity) {
                                                    ((Activity) mContext).finish();
                                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                                }
                                            }
                                        }
                                        if(gameModel.getType_is().equalsIgnoreCase("fungame")){
                                            AssessFragment.waterfall_visibility=false;
                                            Intent i = new Intent(mContext, WordGameActivity.class);
                                            mContext.startActivity(i);
                                            if (mContext instanceof Activity) {
                                                ((Activity) mContext).finish();
                                                ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                            }
                                        }

                                    } else {
                                        AppConstant appConstant=new AppConstant((Activity)mContext);
                                        appConstant.getTokenAss();

                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                    AppConstant appConstant=new AppConstant((Activity)mContext);
                                    appConstant.getTokenAss();

                                }


                            } else {
                                AssessFragment.noassessmentavailableframe.setVisibility(View.VISIBLE);
                                AssessFragment.noavailabletext.setText("This game was already played");

                            //    Toast.makeText(mContext, "This game was already played", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(mContext, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                        }

                    }
                }else {
                    Toast.makeText(mContext,"Please check your Internet connection",Toast.LENGTH_SHORT).show();

                }



            }


        });
        /*if(gameModel.getDisplayname().equalsIgnoreCase("Listening")){
            holder.bg.setBackgroundResource(R.drawable.tab_listening_min);
        }else
        if(gameModel.getDisplayname().contains("Finance")){
            holder.bg.setBackgroundResource(R.drawable.tab_business_min);
        }else
        if(gameModel.getDisplayname().equalsIgnoreCase("verbal ability")){
            holder.bg.setBackgroundResource(R.drawable.tab_brevity_min);
        }else
        if(gameModel.getDisplayname().equalsIgnoreCase("Listening skills")){
            holder.bg.setBackgroundResource(R.drawable.tab_listening_min);
        }else {
            holder.bg.setBackgroundResource(R.drawable.tab_data_min);
        }*/
        // setAnimation(holder.itemView, position);


        // loading expertCardModel cover using Glide library

        //if user select for call from profile activity

        holder.certi_start_line.setVisibility(View.GONE);
        holder.certi_end_line.setVisibility(View.GONE);

    }
    @Override
    public int getItemCount() {
        return todaysgamelist.size();
    }
}
