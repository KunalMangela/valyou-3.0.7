package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.globalgyan.getvalyou.apphelper.PrefManager;

public class CourseListListner extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().toString().equalsIgnoreCase("open_lg_group_list")) {
            Intent intentis = new Intent(context, GetLearningGroupsActivity.class);
            // intent.putExtra("tabs", "normal");
            new PrefManager(context).saveNav("learn");
            new PrefManager(context).saveLearnNav("main");
            intentis.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentis);
        }

        if (intent.getAction().toString().equalsIgnoreCase("open_course_list")) {
            Intent intentc = new Intent(context, CourseLIstActivity.class);
            // intent.putExtra("tabs", "normal");
            intentc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            intentc.putExtra("course_id_push",intent.getStringExtra("course_id_push"));
            intentc.putExtra("pushis","yes");
            context.startActivity(intentc);
        }

    }
}
