package com.globalgyan.getvalyou;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.androidhiddencamera.CameraConfig;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.customwidget.FlowLayout;
import com.globalgyan.getvalyou.listeners.PictureCapturingListener;
import com.globalgyan.getvalyou.services.APictureCapturingService;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.loopj.android.http.HttpGet;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.VerifyResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.UUID;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import tyrantgit.explosionfield.ExplosionField;

public class TempActivity extends AppCompatActivity implements PictureCapturingListener {

    private APictureCapturingService pictureService;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    Face mface = null;
    UUID faceid1, faceid2;
    boolean checkonp = true;
    CameraConfig mCameraConfig;
    boolean wtf;
    String newquid;
    Dialog dialog,dialoge;

    File fol;
    List<Face> faces;
    int audio[] = new int[10];
    String audiourl[] = new String[10];
    private CountDownTimer countDownTimer;
    private long startTime = 180 * 1000;
    private final long interval = 1 * 1000;
    private long secLeft = startTime;
    DefaultHttpClient httpClient;
    HttpURLConnection urlConnection;
    HttpPost httpPost;
    HttpGet httpGet;
    int qansr = 0;
    Bitmap storedpic;
    FontTextView timerrr, dqus, qno;
    String tres = "";
    int cqa = 0;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null;
    ArrayList<String> qidj, ansj, stj, endj;
    JSONArray questions, audios;
    static String json = "", jso = "", Qus = "", Qs = "", token = "", token_type = "", quesDis = "";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int score = 0;
    ArrayList<String> optioncard;
    ImageButton pause;
    URL ppp;
    int pauseCount = 0, qc = 0, qid = 0, tq = 0, ac = -1,cnt=0,no_of_ques_in_list=0;

    LinearLayout cl1, cl2, cl3, cl4, cl5;
    FontTextView o1, o2, o3, o4, o5;
    String uidd, adn, ceid, gdid, game, cate, tgid, sid, aid;
    private ExplosionField mExplosionField;
    String candidateId;
    RelativeLayout rel_bg_temp;
    File filepro;
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR
            = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR
            = new AccelerateInterpolator();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_game_layout);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        rel_bg_temp=(RelativeLayout)findViewById(R.id.rel_bg_temp);
//        mCameraConfig = new CameraConfig()
//                .getBuilder(this)
//                .setCameraFacing(CameraFacing.FRONT_FACING_CAMERA)
//                .setCameraResolution(CameraResolution.HIGH_RESOLUTION)
//                .setImageFormat(CameraImageFormat.FORMAT_JPEG)
//                .setImageRotation(CameraRotation.ROTATION_270)
//                .build();
//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
//
//            //Start camera preview
//            startCamera(mCameraConfig);
//        } else {
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 101);
//        }
        candidateId = PreferenceUtils.getCandidateId(TempActivity.this);
        qidj = new ArrayList<>();
        ansj = new ArrayList<>();
        stj = new ArrayList<>();
        endj = new ArrayList<>();
        try {
            uidd = getIntent().getExtras().getString("uidg");
            adn = getIntent().getExtras().getString("adn");
            ceid = getIntent().getExtras().getString("ceid");
            gdid = getIntent().getExtras().getString("gdid");
            game = getIntent().getExtras().getString("game");
            cate = getIntent().getExtras().getString("category");
            tgid = getIntent().getExtras().getString("tgid");
            aid = getIntent().getExtras().getString("aid");
            token = getIntent().getExtras().getString("token");
        } catch (Exception ex) {

        }

        cl1 = (LinearLayout) findViewById(R.id.ol1);
        cl2 = (LinearLayout) findViewById(R.id.ol2);
        cl3 = (LinearLayout) findViewById(R.id.ol3);
        cl4 = (LinearLayout) findViewById(R.id.ol4);
        cl5 = (LinearLayout) findViewById(R.id.ol5);

        o1 = (FontTextView) findViewById(R.id.opt1);
        o2 = (FontTextView) findViewById(R.id.opt2);
        o3 = (FontTextView) findViewById(R.id.opt3);
        o4 = (FontTextView) findViewById(R.id.opt4);
        o5 = (FontTextView) findViewById(R.id.opt5);

        timerrr = (FontTextView) findViewById(R.id.timerr);
        qno = (FontTextView) findViewById(R.id.qno);
        dqus = (FontTextView) findViewById(R.id.qdis);
        countDownTimer = new MyCountDownTimer(startTime, interval);
        timerrr.setText(String.valueOf(startTime / 1000));
        pause = (ImageButton) findViewById(R.id.pause);
//        pictureService = PictureCapturingServiceImpl.getInstance(this);

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pauseCount == 0) {
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(
//                            AudioGame.this, R.style.AppCompatAlertDialogStyle);
//                    builder.setTitle("ASSESSMENT PAUSED");
//                    builder.setMessage("NEXT PAUSE WILL END THIS ASSESSMENT");
//                    builder.setCancelable(false);
//                    builder.setNegativeButton("RESUME", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                            changeQues();
//                            countDownTimer = new MyCountDownTimer(secLeft, interval);
//                            countDownTimer.start();
//                        }
//                    });
//                    final AlertDialog dialog = builder.create();
//                    dialog.show();

                    countDownTimer.cancel();
                    dialog = new Dialog(TempActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.pause_popup);
                    Window window = dialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                    window.setAttributes(wlp);
                    dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                    dialog.setCancelable(false);
                    dialog.show();
                    Button pu =(Button)dialog.findViewById(R.id.resume);
                    pu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            changeQues();
                            countDownTimer = new MyCountDownTimer(secLeft, interval);
                            countDownTimer.start();
                            pauseCount++;
                        }
                    });

                } else {
                    if (qansr > 0) {
                        countDownTimer.cancel();
                        tres = formdata(qansr);
                        checkonp = false;
                        new PostAns().execute();
                        Intent it = new Intent(TempActivity.this, CheckP.class);
                        it.putExtra("qt", questions.length() + "");
                        it.putExtra("cq", cqa + "");
                        it.putExtra("po", score + "");
                        it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(it);
                        finish();
                    } else {
                        countDownTimer.cancel();
                        tres = formdata();
                        checkonp = false;
                        new PostAns().execute();
                        Intent it = new Intent(TempActivity.this, CheckP.class);
                        it.putExtra("qt", questions.length() + "");
                        it.putExtra("cq", cqa + "");
                        it.putExtra("po", score + "");
                        it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(it);
                        finish();
                    }
                }


            }
        });

        new GetQues().execute();
    }

    private class DetectionTask extends AsyncTask<InputStream, String, Face[]> {
        // Index indicates detecting in which of the two images.
        private int mIndex;
        private boolean mSucceed = true;

        DetectionTask(int index) {
            mIndex = index;
        }

        @Override
        protected Face[] doInBackground(InputStream... params) {
            // Get an instance of face service client to detect faces in image.
            FaceServiceClient faceServiceClient = new FaceServiceRestClient(getString(R.string.endpoint), getString(R.string.subscription_key));
            try {
                publishProgress("Detecting...");

                // Start detection.
                Face[] res = faceServiceClient.detect(
                        params[0],  /* Input stream of image to detect */
                        true,       /* Whether to return face ID */
                        false,       /* Whether to return face landmarks */
                        /* Which face attributes to analyze, currently we support:
                           age,gender,headPose,smile,facialHair */
                        null);
                if (res != null) {
                    wtf = true;
                } else wtf = false;

                return res;
            } catch (Exception e) {
                mSucceed = false;
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(String... progress) {
            Log.e("PROGRESS", progress[0]);

        }

        @Override
        protected void onPostExecute(Face[] result) {
            if (wtf && result.length == 1 && mIndex == 0) {
                faces = Arrays.asList(result);
                faceid1 = faces.get(0).faceId;
                PrefManager prefManager = new PrefManager(TempActivity.this);
                if (mIndex == 0) {
                    if (prefManager.getPic() != "") {
                        String enc = prefManager.getPic();

                        byte[] b = Base64.decode(enc, Base64.DEFAULT);
                        storedpic = BitmapFactory.decodeByteArray(b, 0, b.length);
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        storedpic.compress(Bitmap.CompressFormat.JPEG, 100, output);
                        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
                        new DetectionTask(1).execute(inputStream);
                    }
                }
            } else {
                if (result.length == 0 && mIndex == 0) {
                    new S3Example().execute();
                }
                if (wtf && result.length == 1 && mIndex == 1) {
                    faces = Arrays.asList(result);
                    faceid2 = faces.get(0).faceId;
                    new VerificationTask(faceid1, faceid2).execute();
                }

            }
            Log.e("FCID", result.length + "" + wtf);
        }
    }


    private class VerificationTask extends AsyncTask<Void, String, VerifyResult> {
        // The IDs of two face to verify.
        private UUID mFaceId0;
        private UUID mFaceId1;

        VerificationTask(UUID faceId0, UUID faceId1) {
            mFaceId0 = faceId0;
            mFaceId1 = faceId1;
        }

        @Override
        protected VerifyResult doInBackground(Void... params) {
            // Get an instance of face service client to detect faces in image.
            FaceServiceClient faceServiceClient = new FaceServiceRestClient(getString(R.string.endpoint), getString(R.string.subscription_key));
            try {
                publishProgress("Verifying...");

                // Start verification.
                return faceServiceClient.verify(
                        mFaceId0,      /* The first face ID to verify */
                        mFaceId1);     /* The second face ID to verify */
            } catch (Exception e) {
                publishProgress(e.getMessage());

                return null;
            }
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(VerifyResult result) {
            if (result != null) {
                if (!result.isIdentical) {
                    new S3Example().execute();
                }
            }

            // Show the result on screen when verification is done.

        }
    }

    private class S3Example extends AsyncTask<Void, Void, Void> {
        URL ppp;

        @Override
        protected Void doInBackground(Void... params) {
            String ACCESS_KEY = "AKIAJQTIZUYTX2OYTBJQ";
            String SECRET_KEY = "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";

            Random ramd = new Random();
            int innd = ramd.nextInt(400);
            AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY));

            PutObjectRequest pp = new PutObjectRequest("valyoufacerecog", "check" + innd + ".jpg", fol);
            PutObjectResult putResponse = s3Client.putObject(pp);
            String prurl = s3Client.getResourceUrl("valyoufacerecog", "check" + innd + ".jpg");
            ppp = s3Client.getUrl("valyoufacerecog", "check" + innd + ".jpg");
            Log.e("AMAZON RESPOMSE", ppp.toString());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new SExample(ppp.toString()).execute();

        }
    }

    private class SExample extends AsyncTask<Void, Void, Void> {
        String cid, url;


        SExample(String url) {
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "userId=" + candidateId + "&assessment_id=" + aid + "&FRimage=" + url);
            Request request = new Request.Builder()
                    .url("http://admin.getvalyou.com/api/postfacialRecognition/" + candidateId)
                    .put(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    String jj = response.body().toString();
                    try {
                        JSONObject jdsl = new JSONObject(jj);
                        String iddd = jdsl.getString("_id");
                        Log.e("lsbs", iddd);
                    } catch (JSONException e) {

                    }
                }

            } catch (IOException e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


        }
    }

    @Override
    public void onCaptureDone(final String pictureUrl, final byte[] pictureData) {
        if (pictureData != null && pictureUrl != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    filepro = new File(Environment.getExternalStorageDirectory() + "/" + 1 + "_pic.jpg");
                    try {
                        final OutputStream output = new FileOutputStream(filepro);
                        output.write(pictureData);
                    } catch (IOException e) {
                        Log.e("flbfpn", "Exception occurred while saving picture to external storage ", e);
                    }
                    final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
                    final int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    Matrix matrix = new Matrix();

                    matrix.postRotate(270);

                    final Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    Bitmap rotatedBitmap = Bitmap.createBitmap(scaled, 0, 0, scaled.getWidth(), scaled.getHeight(), matrix, true);

                    Matrix matri = new Matrix();

                    matrix.postRotate(180);
                    final Bitmap scale = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    Bitmap rotatedBitma = Bitmap.createBitmap(scaled, 0, 0, scaled.getWidth(), scaled.getHeight(), matrix, true);
                    filepro = new File(Environment.getExternalStorageDirectory() + "/" + 11 + "_pic.jpg");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    try {
                        final OutputStream output = new FileOutputStream(filepro);
                        output.write(pictureData);
                    } catch (IOException e) {
                        Log.e("flbfpn", "Exception occurred while saving picture to external storage ", e);
                    }
                    if (pictureUrl.contains("1_pic.jpg")) {
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
                        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
                        new DetectionTask(0).execute(inputStream);
                    }

                    // uploadFrontPhoto.setImageBitmap(scaled);

                }
            });

        }
    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {

    }


    private class GetQues extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/GetQuestions");*/

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("CE_Id", ceid)
                            .object("Game", game)
                            .object("Category", cate)
                            .object("GD_Id", gdid)
                            .build().toString();
                   /* StringEntity se = new StringEntity(jn.toString());
                    Log.e("Reqt", jn + "");
                    Log.e("Request", se + "");
                    httpPost.addHeader("Authorization", "Bearer " + token);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    Log.e("FKJ", httpResponse + "CHARAN" + is);*/


                    URL urlToRequest = new URL("http://35.154.93.176/Player/GetQuestions");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();




                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String responseString = readStream(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result1 = responseString;
                // result1="[{\"U_Id\":\"65776\",\"S_GameState\":\"InProgress\",\"Category\":\"25\",\"CE_Id\":\"430\",\"S_WebSessionId\":\"HPGUoPDsmp2PdAi6vbJvYrLPQCaAKMfeEHrQrMJs\",\"CREATED_AT\":\"2018-02-28 12:26:18\",\"S_Id\":93521},[{\"Q_Id\":9013,\"QT_Id\":3094,\"Q_Asset\":\"https://www.android-examples.com/wp-content/uploads/2016/04/Thunder-rumble.mp3\",\"Q_ChildQIds\":\"9018,9019,9020,9021\"},{\"Q_Id\":9012,\"QT_Id\":3093,\"Q_Asset\":\"https://www.android-examples.com/wp-content/uploads/2016/04/Thunder-rumble.mp3\",\"Q_ChildQIds\":\"9014,9015,9016,9017\"}],[{\"QT_Id\":3093,\"Q_Id\":9018,\"Q_Question\":\"What is the expectation of 2017 monsoon compared to the 2016 season?\",\"Q_Option1\":\"Equal\",\"Q_Option2\":\"Greater\",\"Q_Option3\":\"Lower\",\"Q_Option4\":\"Unclear\",\"Q_Option5\":\"\",\"Q_Answer\":\"B\",\"Q_MaxTime\":30},{\"QT_Id\":3093,\"Q_Id\":9019,\"Q_Question\":\"What was the agricultural growth in India during FY17?\",\"Q_Option1\":\"4.9%\",\"Q_Option2\":\"3.5%\",\"Q_Option3\":\"0.7%\",\"Q_Option4\":\"-0.2%\",\"Q_Option5\":\"\",\"Q_Answer\":\"A\",\"Q_MaxTime\":30},{\"QT_Id\":3093,\"Q_Id\":9020,\"Q_Question\":\"What were the agricultural growth rates during FY16 and FY15?\",\"Q_Option1\":\"3.5%, 0.7%\",\"Q_Option2\":\"3.5%, -0.2%\",\"Q_Option3\":\"-0.2%, 0.7%\",\"Q_Option4\":\"0.7%, -0.2%\",\"Q_Option5\":\"\",\"Q_Answer\":\"D\",\"Q_MaxTime\":30},{\"QT_Id\":3093,\"Q_Id\":9021,\"Q_Question\":\"Agricultural growth during FY18, as compared to FY17, is expected to be\",\"Q_Option1\":\"Equal\",\"Q_Option2\":\"Greater\",\"Q_Option3\":\"Lower\",\"Q_Option4\":\"Unclear\",\"Q_Option5\":\"\",\"Q_Answer\":\"C\",\"Q_MaxTime\":30},{\"QT_Id\":3093,\"Q_Id\":9014,\"Q_Question\":\"As per the latest update, what is the expected level of monsoon rainfall as a proportion of the long period average?\",\"Q_Option1\":\"94%\",\"Q_Option2\":\"96%\",\"Q_Option3\":\"98%\",\"Q_Option4\":\"100%\",\"Q_Option5\":\"\",\"Q_Answer\":\"C\",\"Q_MaxTime\":30},{\"QT_Id\":3093,\"Q_Id\":9015,\"Q_Question\":\"Compared to the earlier update, the latest update predicts the rainfall to be\",\"Q_Option1\":\"Equal\",\"Q_Option2\":\"Greater\",\"Q_Option3\":\"Lower\",\"Q_Option4\":\"Unclear\",\"Q_Option5\":\"\",\"Q_Answer\":\"B\",\"Q_MaxTime\":30},{\"QT_Id\":3093,\"Q_Id\":9016,\"Q_Question\":\"The monsoon prediction has been made by the IMD. What is the IMD?\",\"Q_Option1\":\"Indian Monsoon Department\",\"Q_Option2\":\"Indian Meteorological Department\",\"Q_Option3\":\"International Monetary Division\",\"Q_Option4\":\"International Meteorological Division\",\"Q_Option5\":\"\",\"Q_Answer\":\"B\",\"Q_MaxTime\":30},{\"QT_Id\":3093,\"Q_Id\":9017,\"Q_Question\":\"When does the south-west monsoon season typically occur in India?\",\"Q_Option1\":\"May to October\",\"Q_Option2\":\"June to September\",\"Q_Option3\":\"June to October\",\"Q_Option4\":\"May to September\",\"Q_Option5\":\"\",\"Q_Answer\":\"B\",\"Q_MaxTime\":30}]]";
                /*try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    is.close();

                    Qus = sb.toString();


                    Log.e("quesfefff", Qus);

                } catch (Exception e) {
                    e.getMessage();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }*/
                try {
                    JSONArray data = new JSONArray(result1);
                    Log.e("QUESDATA", data.length() + "");
                    // JSONArray sidd = data.getJSONArray(0);
                    final JSONObject sidobj = data.getJSONObject(0);
                    sid = sidobj.getString("S_Id");
                    Log.e("fbwwp", sid);
                    audios = data.getJSONArray(1);
                    Log.e("adddddd", audios.length() + "");
                    for (int i = 0; i < audios.length(); i++) {

                        audiourl[i] = "";
                        JSONObject aaqonj = audios.getJSONObject(i);
                        String aaq = aaqonj.getString("Q_ChildQIds");
                        audiourl[i] = aaqonj.getString("Q_Asset");
                        StringTokenizer st = new StringTokenizer(aaq, ",");
                        audio[i] = st.countTokens();

                    }
                    questions = data.getJSONArray(2);
                    Log.e("QUES", questions.length() + "");
                    tq = questions.length();

                } catch (JSONException e) {

                    Log.e("QUES", "Error parsing data " + e.toString());
                }

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialoge = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialoge.setContentView(R.layout.game_popup);
            Window window = dialoge.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
            dialoge.show();
            startTime = questions.length() * 30 * 1000;
            changeAudio(-1);

        }
    }
    public String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qus = sb.toString();


            Log.e("JSONStrr", Qus);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qus;
    }
    public String readStream1(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qs = sb.toString();


            Log.e("JSONStrr", Qs);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qs;
    }
    public void changeAudio(int acq) {
        dialoge.dismiss();
        if (acq == -1) {
            dialog = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.audio_cnfrm);
            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
            dialog.setCancelable(false);
            dialog.show();
            Button b = (Button) dialog.findViewById(R.id.audiookcnfrm);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    dialoge = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                    dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialoge.setContentView(R.layout.game_popup);
                    Window window = dialoge.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                    window.setAttributes(wlp);
                    dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                    dialoge.show();
                    changeAudio(0);
                    changeQues();
                    startTimer();
                }
            });

            return;
        }


        dialog = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.audio_play);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if(i== KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {

                    return true; // Consumed
                }
                else {
                    return false; // Not consumed
                }

            }
        });
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
        dialog.show();
        MediaPlayer mp = new MediaPlayer();

        try {
            // mp.setDataSource(AudioGame.this, Uri.parse(audiourl[acq]));
            //  mp.setDataSource(audiourl[acq]);
            mp.setDataSource(audiourl[acq]);
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.prepareAsync();
        } catch (IOException e) {

        }
        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
            }
        });
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                dialog.dismiss();
            }
        });


    }

    public void changeQues() {
        rel_bg_temp.setVisibility(View.GONE);
        cl1.setBackgroundColor(Color.parseColor("#054f89"));
        cl2.setBackgroundColor(Color.parseColor("#054f89"));
        cl3.setBackgroundColor(Color.parseColor("#054f89"));
        cl4.setBackgroundColor(Color.parseColor("#054f89"));
        cl5.setBackgroundColor(Color.parseColor("#054f89"));
        dialoge.dismiss();

        if (qc < questions.length()) {

//            if(qc==2){
//                takePicture();
//            }
            if(audio.length>1) {
                if (no_of_ques_in_list == audio[cnt]) {
                    changeAudio(cnt+1);
                    cnt++;
                    no_of_ques_in_list=0;

                }
                no_of_ques_in_list++;

            }
            final String now = Calendar.getInstance().getTime().toString();
            qno.setText(qc + 1 + "/" + questions.length());
            try {
                final JSONObject qObj = questions.getJSONObject(qc);
                optioncard = new ArrayList<>();
                optioncard.add(qObj.getString("Q_Option1"));
                optioncard.add(qObj.getString("Q_Option2"));
                optioncard.add(qObj.getString("Q_Option3"));
                optioncard.add(qObj.getString("Q_Option4"));
                if (!qObj.getString("Q_Option5").isEmpty() && !qObj.getString("Q_Option5").equals("null")) {
                    optioncard.add(qObj.getString("Q_Option5"));
                }
                String crcans = qObj.getString("Q_Answer");

                int crcopt = 0;
                switch (crcans) {
                    case "A":
                        crcopt = 0;
                        break;
                    case "B":
                        crcopt = 1;
                        break;
                    case "C":
                        crcopt = 2;
                        break;
                    case "D":
                        crcopt = 3;
                        break;
                    case "E":
                        crcopt = 4;
                        break;
                    default:
                        System.out.println("Not in 10, 20 or 30");
                }
                final int crct = crcopt;
                qid = Integer.parseInt(qObj.getString("Q_Id"));
                newquid=qObj.getString("Q_Id");

                quesDis = qObj.getString("Q_Question");

                dqus.setText(quesDis);

                switch (optioncard.size()) {
                    case 1:
                        o1.setText(optioncard.get(0));
                        cl2.setVisibility(View.GONE);
                        cl3.setVisibility(View.GONE);
                        cl4.setVisibility(View.GONE);
                        cl5.setVisibility(View.GONE);
                        break;
                    case 2:
                        o1.setText(optioncard.get(0));
                        o2.setText(optioncard.get(1));
                        cl3.setVisibility(View.GONE);
                        cl4.setVisibility(View.GONE);
                        cl5.setVisibility(View.GONE);
                        break;
                    case 3:
                        o1.setText(optioncard.get(0));
                        o2.setText(optioncard.get(1));
                        o3.setText(optioncard.get(2));
                        cl4.setVisibility(View.GONE);
                        cl5.setVisibility(View.GONE);
                        break;
                    case 4:
                        o1.setText(optioncard.get(0));
                        o2.setText(optioncard.get(1));
                        o3.setText(optioncard.get(2));
                        o4.setText(optioncard.get(3));
                        cl5.setVisibility(View.GONE);
                        break;
                    case 5:
                        o1.setText(optioncard.get(0));
                        o2.setText(optioncard.get(1));
                        o3.setText(optioncard.get(2));
                        o4.setText(optioncard.get(3));
                        o5.setText(optioncard.get(4));
                        break;
                }

                cl1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialoge = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialoge.setContentView(R.layout.game_popup);
                        Window window = dialoge.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        wlp.gravity = Gravity.CENTER;
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                        window.setAttributes(wlp);
                        dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                        dialoge.show();
                        if (crct == 0) {
                            cl1.setBackgroundColor(Color.parseColor("#00b119"));
                            cqa++;
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    score = score + 50;
                                    qc++;

                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("A");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        } else {
                            cl1.setBackgroundColor(Color.parseColor("#FF211F"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    qc++;
                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("A");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);

                        }
                    }
                });

                cl2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialoge = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialoge.setContentView(R.layout.game_popup);
                        Window window = dialoge.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        wlp.gravity = Gravity.CENTER;
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                        window.setAttributes(wlp);
                        dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                        dialoge.show();
                        if (crct == 1) {
                            cl2.setBackgroundColor(Color.parseColor("#00b119"));
                            cqa++;
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    score = score + 50;
                                    qc++;

                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("B");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        } else {
                            cl2.setBackgroundColor(Color.parseColor("#FF211F"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    qc++;
                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("B");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        }
                    }
                });
                cl3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialoge = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialoge.setContentView(R.layout.game_popup);
                        Window window = dialoge.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        wlp.gravity = Gravity.CENTER;
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                        window.setAttributes(wlp);
                        dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                        dialoge.show();
                        if (crct == 2) {
                            cqa++;
                            cl3.setBackgroundColor(Color.parseColor("#00b119"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    score = score + 50;
                                    qc++;

                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("C");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        } else {
                            cl3.setBackgroundColor(Color.parseColor("#FF211F"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    qc++;
                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("C");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        }
                    }
                });
                cl4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialoge = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialoge.setContentView(R.layout.game_popup);
                        Window window = dialoge.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        wlp.gravity = Gravity.CENTER;
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                        window.setAttributes(wlp);
                        dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                        dialoge.show();
                        if (crct == 3) {
                            cqa++;
                            cl4.setBackgroundColor(Color.parseColor("#00b119"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    score = score + 50;
                                    qc++;

                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("D");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        } else {
                            cl4.setBackgroundColor(Color.parseColor("#FF211F"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    qc++;
                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("D");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        }
                    }
                });
                cl5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialoge = new Dialog(TempActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialoge.setContentView(R.layout.game_popup);
                        Window window = dialoge.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        wlp.gravity = Gravity.CENTER;
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                        window.setAttributes(wlp);
                        dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                        dialoge.show();
                        if (crct == 4) {
                            cqa++;
                            cl5.setBackgroundColor(Color.parseColor("#00b119"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    score = score + 50;
                                    qc++;

                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("E");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);

                        } else {
                            cl5.setBackgroundColor(Color.parseColor("#FF211F"));
                            Handler han = new Handler();
                            han.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    qc++;
                                    try {
                                        qidj.add(qObj.getString("Q_Id"));
                                        ansj.add("D");
                                        stj.add(now);
                                        endj.add(Calendar.getInstance().getTime().toString());
                                        qansr++;
                                    } catch (JSONException exx) {

                                    }
                                    changeQues();
                                }
                            }, 1000);
                        }
                    }
                });

            } catch (JSONException e) {
                Log.e("CHANGE QUES", "Error parsing data " + e.toString());
            }

        } else {
            dialoge.dismiss();
            if (qansr > 0) {
                tres = formdata(qansr);
                checkonp = false;
                countDownTimer.cancel();
                new PostAns().execute();
                Intent it = new Intent(TempActivity.this, CheckP.class);
                it.putExtra("qt", questions.length() + "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                finish();
            } else {
                tres = formdata();
                checkonp = false;
                countDownTimer.cancel();
                new PostAns().execute();
                Intent it = new Intent(TempActivity.this, CheckP.class);
                it.putExtra("qt", questions.length() + "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                finish();
            }
        }

    }

    public String formdata() {
        String jn = "";
        try{

            JSONObject student1 = new JSONObject();
            student1.put("U_Id", uidd);
            student1.put("S_Id", sid);
            student1.put("Q_Id", newquid);
            student1.put("TG_Id", tgid);
            student1.put("SD_UserAnswer", "E");
            student1.put("SD_StartTime", Calendar.getInstance().getTime().toString());
            student1.put("SD_EndTime", Calendar.getInstance().getTime().toString());


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(student1);


            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn = studentsObj.toString();
        } catch(
                JSONException e)

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jn;
    }

    public String formdata(int anan){
        String jn="";

        switch(anan){
            case 1: try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));


                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);




                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;

            case 2:try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));




                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);




                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;
            case 3:try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));

                JSONObject student3 = new JSONObject();
                student3.put("U_Id", uidd);
                student3.put("S_Id", sid);
                student3.put("Q_Id", qidj.get(2));
                student3.put("TG_Id", tgid);
                student3.put("SD_UserAnswer", ansj.get(2));
                student3.put("SD_StartTime", stj.get(2));
                student3.put("SD_EndTime", endj.get(2));


                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);
                jsonArray.put(student3);




                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;
            case 4: try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));

                JSONObject student3 = new JSONObject();
                student3.put("U_Id", uidd);
                student3.put("S_Id", sid);
                student3.put("Q_Id", qidj.get(2));
                student3.put("TG_Id", tgid);
                student3.put("SD_UserAnswer", ansj.get(2));
                student3.put("SD_StartTime", stj.get(2));
                student3.put("SD_EndTime", endj.get(2));

                JSONObject student4 = new JSONObject();
                student4.put("U_Id", uidd);
                student4.put("S_Id", sid);
                student4.put("Q_Id", qidj.get(3));
                student4.put("TG_Id", tgid);
                student4.put("SD_UserAnswer", ansj.get(3));
                student4.put("SD_StartTime", stj.get(3));
                student4.put("SD_EndTime", endj.get(3));




                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);
                jsonArray.put(student3);
                jsonArray.put(student4);



                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;

            case 5: try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));

                JSONObject student3 = new JSONObject();
                student3.put("U_Id", uidd);
                student3.put("S_Id", sid);
                student3.put("Q_Id", qidj.get(2));
                student3.put("TG_Id", tgid);
                student3.put("SD_UserAnswer", ansj.get(2));
                student3.put("SD_StartTime", stj.get(2));
                student3.put("SD_EndTime", endj.get(2));

                JSONObject student4 = new JSONObject();
                student4.put("U_Id", uidd);
                student4.put("S_Id", sid);
                student4.put("Q_Id", qidj.get(3));
                student4.put("TG_Id", tgid);
                student4.put("SD_UserAnswer", ansj.get(3));
                student4.put("SD_StartTime", stj.get(3));
                student4.put("SD_EndTime", endj.get(3));

                JSONObject student5 = new JSONObject();
                student5.put("U_Id", uidd);
                student5.put("S_Id", sid);
                student5.put("Q_Id", qidj.get(4));
                student5.put("TG_Id", tgid);
                student5.put("SD_UserAnswer", ansj.get(4));
                student5.put("SD_StartTime", stj.get(4));
                student5.put("SD_EndTime", endj.get(4));




                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);
                jsonArray.put(student3);
                jsonArray.put(student4);
                jsonArray.put(student5);



                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;


            case 6: try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));

                JSONObject student3 = new JSONObject();
                student3.put("U_Id", uidd);
                student3.put("S_Id", sid);
                student3.put("Q_Id", qidj.get(2));
                student3.put("TG_Id", tgid);
                student3.put("SD_UserAnswer", ansj.get(2));
                student3.put("SD_StartTime", stj.get(2));
                student3.put("SD_EndTime", endj.get(2));

                JSONObject student4 = new JSONObject();
                student4.put("U_Id", uidd);
                student4.put("S_Id", sid);
                student4.put("Q_Id", qidj.get(3));
                student4.put("TG_Id", tgid);
                student4.put("SD_UserAnswer", ansj.get(3));
                student4.put("SD_StartTime", stj.get(3));
                student4.put("SD_EndTime", endj.get(3));

                JSONObject student5 = new JSONObject();
                student5.put("U_Id", uidd);
                student5.put("S_Id", sid);
                student5.put("Q_Id", qidj.get(4));
                student5.put("TG_Id", tgid);
                student5.put("SD_UserAnswer", ansj.get(4));
                student5.put("SD_StartTime", stj.get(4));
                student5.put("SD_EndTime", endj.get(4));

                JSONObject student6 = new JSONObject();
                student6.put("U_Id", uidd);
                student6.put("S_Id", sid);
                student6.put("Q_Id", qidj.get(5));
                student6.put("TG_Id", tgid);
                student6.put("SD_UserAnswer", ansj.get(5));
                student6.put("SD_StartTime", stj.get(5));
                student6.put("SD_EndTime", endj.get(5));




                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);
                jsonArray.put(student3);
                jsonArray.put(student4);
                jsonArray.put(student5);
                jsonArray.put(student6);



                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;

            case 7: try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));

                JSONObject student3 = new JSONObject();
                student3.put("U_Id", uidd);
                student3.put("S_Id", sid);
                student3.put("Q_Id", qidj.get(2));
                student3.put("TG_Id", tgid);
                student3.put("SD_UserAnswer", ansj.get(2));
                student3.put("SD_StartTime", stj.get(2));
                student3.put("SD_EndTime", endj.get(2));

                JSONObject student4 = new JSONObject();
                student4.put("U_Id", uidd);
                student4.put("S_Id", sid);
                student4.put("Q_Id", qidj.get(3));
                student4.put("TG_Id", tgid);
                student4.put("SD_UserAnswer", ansj.get(3));
                student4.put("SD_StartTime", stj.get(3));
                student4.put("SD_EndTime", endj.get(3));

                JSONObject student5 = new JSONObject();
                student5.put("U_Id", uidd);
                student5.put("S_Id", sid);
                student5.put("Q_Id", qidj.get(4));
                student5.put("TG_Id", tgid);
                student5.put("SD_UserAnswer", ansj.get(4));
                student5.put("SD_StartTime", stj.get(4));
                student5.put("SD_EndTime", endj.get(4));

                JSONObject student6 = new JSONObject();
                student6.put("U_Id", uidd);
                student6.put("S_Id", sid);
                student6.put("Q_Id", qidj.get(5));
                student6.put("TG_Id", tgid);
                student6.put("SD_UserAnswer", ansj.get(5));
                student6.put("SD_StartTime", stj.get(5));
                student6.put("SD_EndTime", endj.get(5));

                JSONObject student7 = new JSONObject();
                student7.put("U_Id", uidd);
                student7.put("S_Id", sid);
                student7.put("Q_Id", qidj.get(6));
                student7.put("TG_Id", tgid);
                student7.put("SD_UserAnswer", ansj.get(6));
                student7.put("SD_StartTime", stj.get(6));
                student7.put("SD_EndTime", endj.get(6));




                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);
                jsonArray.put(student3);
                jsonArray.put(student4);
                jsonArray.put(student5);
                jsonArray.put(student6);
                jsonArray.put(student7);



                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;

            case 8: try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));

                JSONObject student3 = new JSONObject();
                student3.put("U_Id", uidd);
                student3.put("S_Id", sid);
                student3.put("Q_Id", qidj.get(2));
                student3.put("TG_Id", tgid);
                student3.put("SD_UserAnswer", ansj.get(2));
                student3.put("SD_StartTime", stj.get(2));
                student3.put("SD_EndTime", endj.get(2));

                JSONObject student4 = new JSONObject();
                student4.put("U_Id", uidd);
                student4.put("S_Id", sid);
                student4.put("Q_Id", qidj.get(3));
                student4.put("TG_Id", tgid);
                student4.put("SD_UserAnswer", ansj.get(3));
                student4.put("SD_StartTime", stj.get(3));
                student4.put("SD_EndTime", endj.get(3));

                JSONObject student5 = new JSONObject();
                student5.put("U_Id", uidd);
                student5.put("S_Id", sid);
                student5.put("Q_Id", qidj.get(4));
                student5.put("TG_Id", tgid);
                student5.put("SD_UserAnswer", ansj.get(4));
                student5.put("SD_StartTime", stj.get(4));
                student5.put("SD_EndTime", endj.get(4));

                JSONObject student6 = new JSONObject();
                student6.put("U_Id", uidd);
                student6.put("S_Id", sid);
                student6.put("Q_Id", qidj.get(5));
                student6.put("TG_Id", tgid);
                student6.put("SD_UserAnswer", ansj.get(5));
                student6.put("SD_StartTime", stj.get(5));
                student6.put("SD_EndTime", endj.get(5));

                JSONObject student7 = new JSONObject();
                student7.put("U_Id", uidd);
                student7.put("S_Id", sid);
                student7.put("Q_Id", qidj.get(6));
                student7.put("TG_Id", tgid);
                student7.put("SD_UserAnswer", ansj.get(6));
                student7.put("SD_StartTime", stj.get(6));
                student7.put("SD_EndTime", endj.get(6));

                JSONObject student8 = new JSONObject();
                student8.put("U_Id", uidd);
                student8.put("S_Id", sid);
                student8.put("Q_Id", qidj.get(7));
                student8.put("TG_Id", tgid);
                student8.put("SD_UserAnswer", ansj.get(7));
                student8.put("SD_StartTime", stj.get(7));
                student8.put("SD_EndTime", endj.get(7));




                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);
                jsonArray.put(student3);
                jsonArray.put(student4);
                jsonArray.put(student5);
                jsonArray.put(student6);
                jsonArray.put(student7);
                jsonArray.put(student8);



                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;

            case 9: try {
                JSONObject student1 = new JSONObject();
                student1.put("U_Id", uidd);
                student1.put("S_Id", sid);
                student1.put("Q_Id", qidj.get(0));
                student1.put("TG_Id", tgid);
                student1.put("SD_UserAnswer", ansj.get(0));
                student1.put("SD_StartTime", stj.get(0));
                student1.put("SD_EndTime", endj.get(0));

                JSONObject student2 = new JSONObject();
                student2.put("U_Id", uidd);
                student2.put("S_Id", sid);
                student2.put("Q_Id", qidj.get(1));
                student2.put("TG_Id", tgid);
                student2.put("SD_UserAnswer", ansj.get(1));
                student2.put("SD_StartTime", stj.get(1));
                student2.put("SD_EndTime", endj.get(1));

                JSONObject student3 = new JSONObject();
                student3.put("U_Id", uidd);
                student3.put("S_Id", sid);
                student3.put("Q_Id", qidj.get(2));
                student3.put("TG_Id", tgid);
                student3.put("SD_UserAnswer", ansj.get(2));
                student3.put("SD_StartTime", stj.get(2));
                student3.put("SD_EndTime", endj.get(2));

                JSONObject student4 = new JSONObject();
                student4.put("U_Id", uidd);
                student4.put("S_Id", sid);
                student4.put("Q_Id", qidj.get(3));
                student4.put("TG_Id", tgid);
                student4.put("SD_UserAnswer", ansj.get(3));
                student4.put("SD_StartTime", stj.get(3));
                student4.put("SD_EndTime", endj.get(3));

                JSONObject student5 = new JSONObject();
                student5.put("U_Id", uidd);
                student5.put("S_Id", sid);
                student5.put("Q_Id", qidj.get(4));
                student5.put("TG_Id", tgid);
                student5.put("SD_UserAnswer", ansj.get(4));
                student5.put("SD_StartTime", stj.get(4));
                student5.put("SD_EndTime", endj.get(4));

                JSONObject student6 = new JSONObject();
                student6.put("U_Id", uidd);
                student6.put("S_Id", sid);
                student6.put("Q_Id", qidj.get(5));
                student6.put("TG_Id", tgid);
                student6.put("SD_UserAnswer", ansj.get(5));
                student6.put("SD_StartTime", stj.get(5));
                student6.put("SD_EndTime", endj.get(5));

                JSONObject student7 = new JSONObject();
                student7.put("U_Id", uidd);
                student7.put("S_Id", sid);
                student7.put("Q_Id", qidj.get(6));
                student7.put("TG_Id", tgid);
                student7.put("SD_UserAnswer", ansj.get(6));
                student7.put("SD_StartTime", stj.get(6));
                student7.put("SD_EndTime", endj.get(6));

                JSONObject student8 = new JSONObject();
                student8.put("U_Id", uidd);
                student8.put("S_Id", sid);
                student8.put("Q_Id", qidj.get(7));
                student8.put("TG_Id", tgid);
                student8.put("SD_UserAnswer", ansj.get(7));
                student8.put("SD_StartTime", stj.get(7));
                student8.put("SD_EndTime", endj.get(7));

                JSONObject student9 = new JSONObject();
                student9.put("U_Id", uidd);
                student9.put("S_Id", sid);
                student9.put("Q_Id", qidj.get(8));
                student9.put("TG_Id", tgid);
                student9.put("SD_UserAnswer", ansj.get(8));
                student9.put("SD_StartTime", stj.get(8));
                student9.put("SD_EndTime", endj.get(8));



                JSONArray jsonArray = new JSONArray();

                jsonArray.put(student1);
                jsonArray.put(student2);
                jsonArray.put(student3);
                jsonArray.put(student4);
                jsonArray.put(student5);
                jsonArray.put(student6);
                jsonArray.put(student7);
                jsonArray.put(student8);
                jsonArray.put(student9);



                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                break;


            case 10:
                try {
                    JSONObject student1 = new JSONObject();
                    student1.put("U_Id", uidd);
                    student1.put("S_Id", sid);
                    student1.put("Q_Id", qidj.get(0));
                    student1.put("TG_Id", tgid);
                    student1.put("SD_UserAnswer", ansj.get(0));
                    student1.put("SD_StartTime", stj.get(0));
                    student1.put("SD_EndTime", endj.get(0));

                    JSONObject student2 = new JSONObject();
                    student2.put("U_Id", uidd);
                    student2.put("S_Id", sid);
                    student2.put("Q_Id", qidj.get(1));
                    student2.put("TG_Id", tgid);
                    student2.put("SD_UserAnswer", ansj.get(1));
                    student2.put("SD_StartTime", stj.get(1));
                    student2.put("SD_EndTime", endj.get(1));

                    JSONObject student3 = new JSONObject();
                    student3.put("U_Id", uidd);
                    student3.put("S_Id", sid);
                    student3.put("Q_Id", qidj.get(2));
                    student3.put("TG_Id", tgid);
                    student3.put("SD_UserAnswer", ansj.get(2));
                    student3.put("SD_StartTime", stj.get(2));
                    student3.put("SD_EndTime", endj.get(2));

                    JSONObject student4 = new JSONObject();
                    student4.put("U_Id", uidd);
                    student4.put("S_Id", sid);
                    student4.put("Q_Id", qidj.get(3));
                    student4.put("TG_Id", tgid);
                    student4.put("SD_UserAnswer", ansj.get(3));
                    student4.put("SD_StartTime", stj.get(3));
                    student4.put("SD_EndTime", endj.get(3));

                    JSONObject student5 = new JSONObject();
                    student5.put("U_Id", uidd);
                    student5.put("S_Id", sid);
                    student5.put("Q_Id", qidj.get(4));
                    student5.put("TG_Id", tgid);
                    student5.put("SD_UserAnswer", ansj.get(4));
                    student5.put("SD_StartTime", stj.get(4));
                    student5.put("SD_EndTime", endj.get(4));

                    JSONObject student6 = new JSONObject();
                    student6.put("U_Id", uidd);
                    student6.put("S_Id", sid);
                    student6.put("Q_Id", qidj.get(5));
                    student6.put("TG_Id", tgid);
                    student6.put("SD_UserAnswer", ansj.get(5));
                    student6.put("SD_StartTime", stj.get(5));
                    student6.put("SD_EndTime", endj.get(5));

                    JSONObject student7 = new JSONObject();
                    student7.put("U_Id", uidd);
                    student7.put("S_Id", sid);
                    student7.put("Q_Id", qidj.get(6));
                    student7.put("TG_Id", tgid);
                    student7.put("SD_UserAnswer", ansj.get(6));
                    student7.put("SD_StartTime", stj.get(6));
                    student7.put("SD_EndTime", endj.get(6));

                    JSONObject student8 = new JSONObject();
                    student8.put("U_Id", uidd);
                    student8.put("S_Id", sid);
                    student8.put("Q_Id", qidj.get(7));
                    student8.put("TG_Id", tgid);
                    student8.put("SD_UserAnswer", ansj.get(7));
                    student8.put("SD_StartTime", stj.get(7));
                    student8.put("SD_EndTime", endj.get(7));

                    JSONObject student9 = new JSONObject();
                    student9.put("U_Id", uidd);
                    student9.put("S_Id", sid);
                    student9.put("Q_Id", qidj.get(8));
                    student9.put("TG_Id", tgid);
                    student9.put("SD_UserAnswer", ansj.get(8));
                    student9.put("SD_StartTime", stj.get(8));
                    student9.put("SD_EndTime", endj.get(8));

                    JSONObject student10 = new JSONObject();
                    student10.put("U_Id", uidd);
                    student10.put("S_Id", sid);
                    student10.put("Q_Id", qidj.get(9));
                    student10.put("TG_Id", tgid);
                    student10.put("SD_UserAnswer", ansj.get(9));
                    student10.put("SD_StartTime", stj.get(9));
                    student10.put("SD_EndTime", endj.get(9));


                    JSONArray jsonArray = new JSONArray();

                    jsonArray.put(student1);
                    jsonArray.put(student2);
                    jsonArray.put(student3);
                    jsonArray.put(student4);
                    jsonArray.put(student5);
                    jsonArray.put(student6);
                    jsonArray.put(student7);
                    jsonArray.put(student8);
                    jsonArray.put(student9);
                    jsonArray.put(student10);


                    JSONObject studentsObj = new JSONObject();
                    studentsObj.put("data", jsonArray);
                    jn=studentsObj.toString();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
        }
        return jn;
    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }


        @Override

        public void onFinish() {

            if (qansr > 0) {
                tres = formdata(qansr);
                checkonp = false;
                new PostAns().execute();
                Intent it = new Intent(TempActivity.this, CheckP.class);
                it.putExtra("qt", questions.length() + "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                finish();
            } else {
                tres = formdata();
                checkonp = false;
                new PostAns().execute();
                Intent it = new Intent(TempActivity.this, CheckP.class);
                it.putExtra("qt", questions.length() + "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                finish();
            }

        }


        @Override

        public void onTick(long millisUntilFinished) {

            timerrr.setText("" + millisUntilFinished / 1000);
            secLeft = millisUntilFinished;

        }

    }

    void startTimer() {
        countDownTimer.start();
    }


    @Override
    protected void onPause() {
        super.onPause();
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);
        if (pauseCount == 0 && checkonp) {
            countDownTimer.cancel();
            dialog = new Dialog(TempActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.pause_popup);
            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
            dialog.setCancelable(false);
            dialog.show();
            Button pu =(Button)dialog.findViewById(R.id.resume);
            pu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    changeQues();
                    countDownTimer = new MyCountDownTimer(secLeft, interval);
                    countDownTimer.start();
                    pauseCount++;
                }
            });

        } else {
            if(checkonp){
                Log.e("PAUSE",checkonp+"");
                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    countDownTimer.cancel();
                    new PostAns().execute();
                    Intent it = new Intent(TempActivity.this, CheckP.class);
                    it.putExtra("qt", questions.length() + "");
                    it.putExtra("cq", cqa + "");
                    it.putExtra("po", score + "");
                    it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(it);
                    finish();
                } else {
                    tres = formdata();
                    checkonp = false;
                    countDownTimer.cancel();
                    new PostAns().execute();
                    Intent it = new Intent(TempActivity.this, CheckP.class);
                    it.putExtra("qt", questions.length() + "");
                    it.putExtra("cq", cqa + "");
                    it.putExtra("po", score + "");
                    it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(it);
                    finish();
                }
            }else{

            }
        }
    }

    private class PostAns extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/Answers");

                    StringEntity se = new StringEntity(tres);

                    Log.e("Request", tres+ "");
                    httpPost.addHeader("Authorization", "Bearer " + token);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    Log.e("FKJ", httpResponse + "CHARAN" + is);*/
                    URL urlToRequest = new URL("http://35.154.93.176/Player/Answers");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            tres.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(tres);
                    wr.flush();
                    is = urlConnection.getInputStream();


                    String responseString = readStream(is);
                    Log.v("Response", responseString);
                    result1 = responseString;

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
               /* try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    is.close();

                    Qus = sb.toString();


                    Log.e("quesfefff", Qus);

                } catch (Exception e) {
                    e.getMessage();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }


            } */catch (Exception e) {
                    e.getMessage();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }

            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new Ends().execute();
        }
    }


    private class Ends extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {
                    /*httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/EndSession");*/

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("S_Id", sid)
                            .build().toString();
                    //StringEntity se = new StringEntity(jn.toString());

                   /* Log.e("Request", se + "");
                    httpPost.addHeader("Authorization", "Bearer " + token);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    Log.e("FKJ", httpResponse + "CHARAN" + is);*/

                    URL urlToRequest = new URL("http://35.154.93.176/Player/EndSession");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();


                    String responseString = readStream1(is);
                    Log.v("Response", responseString);
                    result1 = responseString;


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
               /* try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    is.close();

                    Qs = sb.toString();


                    Log.e("quesfefff", Qus);

                } catch (Exception e) {
                    e.getMessage();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }


            } */catch (Exception e) {
                    e.getMessage();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }

            }
            finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


        }
    }

    @Override
    public void onBackPressed() {
    }
}
