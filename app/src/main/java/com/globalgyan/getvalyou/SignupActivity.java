package com.globalgyan.getvalyou;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.globalgyan.getvalyou.cms.response.ForgetPasswordResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;

/**
 * Created by NaNi on 09/09/17.
 */

public class SignupActivity extends AppCompatActivity implements GUICallback{

    TextInputLayout userEmailWrapper;
    TextInputLayout userNameWrapper;
    TextInputLayout userPhoneWrapper = null;
    TextInputLayout userPasswordWrapper = null;
    private EditText loginusernamefield = null;
    private EditText txtPassword = null;
    private EditText txtEmail = null;
    private EditText txtPhone = null;
    Dialog dialog;
    ConnectionUtils connectionUtils;

    DefaultHttpClient httpClient;
    HttpPost httpPost;
    HttpGet httpGet;
    FontTextView user;
    int cqa=0;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null,jObjeya=null;
    JSONArray data;
    JSONObject dataJSONobject;
    static String json = "",jsoneya="",jso="",Qus="",token="",token_type="", emaileya="",dn="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();

    List<String> myList;
    ArrayAdapter<String> myAutoCompleteAdapter;

    AppCompatButton signup;
    FontTextView signin;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;

    private SimpleLocation location;
    String latlong="",phn="";
    KProgressHUD signup_progress;
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        connectionUtils=new ConnectionUtils(SignupActivity.this);
        myList=new ArrayList<String>();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        myAutoCompleteAdapter = new ArrayAdapter<String>(
                SignupActivity.this,
                R.layout.auto_layout,
                R.id.countryName,
                myList);
        signup_progress = KProgressHUD.create(SignupActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("please wait...")
                .setDimAmount(0.7f)
                .setCancellable(false);
        new GetEywa().execute();
        try {

            phn = getIntent().getExtras().getString("phn");

        } catch (Exception ex) {

        }
        signin=(FontTextView)findViewById(R.id.Login_btn);
        userNameWrapper=(TextInputLayout)findViewById(R.id.input_name_tp);
        userPasswordWrapper=(TextInputLayout)findViewById(R.id.input_password_tp);
        userEmailWrapper=(TextInputLayout)findViewById(R.id.input_email_tp);
        userPhoneWrapper=(TextInputLayout)findViewById(R.id.input_phone_tp);
        loginusernamefield=(EditText)findViewById(R.id.input_name);
        txtPassword=(EditText)findViewById(R.id.input_password);
        txtEmail=(EditText)findViewById(R.id.input_email);
        txtPhone=(EditText)findViewById(R.id.input_phone);
        signup=(AppCompatButton)findViewById(R.id.signup);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do changes
                Intent it = new Intent(SignupActivity.this,LoginActivity.class);
                startActivity(it);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (isValidationSuccess()) {
                        if(connectionUtils.isConnectionAvailable())
                        {
                            signup.setEnabled(false);
                            signup.setBackgroundColor(Color.parseColor("#ABBAC2"));

                            try {
                                new CountDownTimer(3000, 3000) {
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        signup.setEnabled(true);
                                        signup.setBackgroundColor(Color.parseColor("#2f27c9"));
                                    }
                                }.start();

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            Log.e("signup","clicked");
                            signup_progress.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(SignupActivity.this, SignupActivity.this);
                                    processor.sendOtp(txtPhone.getText().toString());
                                }
                            }.start();

                      /*  RetrofitRequestProcessor processor = new RetrofitRequestProcessor(SignupActivity.this, SignupActivity.this);
                        Log.e("FCM TOKEN", FirebaseInstanceId.getInstance().getToken()+"");
                        processor.doSignUp(txtEmail.getText().toString(), txtPassword.getText().toString(), FirebaseInstanceId.getInstance().getToken(), loginusernamefield.getText().toString(), phn,"null","0","ANDROID",getAndroidVersion(),getDevice());
*/
                        }else {
                            signup.setEnabled(true);
                            signup.setBackgroundColor(Color.parseColor("#2f27c9"));
                            Toast.makeText(SignupActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        signup.setEnabled(true);
                        signup.setBackgroundColor(Color.parseColor("#2f27c9"));
                        if((txtPassword.getText().toString().contains(" ")||txtPassword.getText().toString().length()<6)&&
                                (txtPhone.getText().toString().length()>2)&&(txtEmail.getText().toString().length()>2)&&
                                (loginusernamefield.getText().toString().length()>2)){
                            if(txtPassword.getText().toString().contains(" ")){
                                Toast.makeText(SignupActivity.this,"Blank spaces are not allowed in password",Toast.LENGTH_SHORT).show();

                            }else {
                                Toast.makeText(SignupActivity.this,"Password should be atleast 6 digits",Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            if(txtPassword.getText().toString().contains(" ")){
                                Toast.makeText(SignupActivity.this,"Blank spaces are not allowed in password",Toast.LENGTH_SHORT).show();

                            }else  if(txtPassword.getText().toString().length()<6) {
                                Toast.makeText(SignupActivity.this,"Password should be atleast 6 digits",Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(SignupActivity.this, "Enter all fields", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

            }
        });
    }
    public String getAndroidVersion() {
        String release = android.os.Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.e("RELEASE",release);
        return release;
    }

    public String getDevice() {
        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

        return deviceName;
    }

    private boolean isValidationSuccess() {
        boolean returnValue = true;
        if (!validateEmail())
            returnValue = false;
        if (!validatePhone())
            returnValue = false;
        if (!validatePassword())
            returnValue = false;
        if (!validateName())
            returnValue = false;
        return returnValue;
    }
    private boolean validateName() {
        boolean returnValue = true;
        if (!com.globalgyan.getvalyou.utils.ValidationUtils.nameValid(loginusernamefield, this)) {
            loginusernamefield.setError("name required");
            returnValue = false;

        } else {
            loginusernamefield.setError(null);
        }

        return returnValue;

    }

    private boolean validatePhone() {
        boolean returnValue = true;
        if (!com.globalgyan.getvalyou.utils.ValidationUtils.phoneValid(txtPhone, this)) {
            txtPhone.setError("phone number required");
            //phoneTxt.setBackgroundColor(Color.TRANSPARENT);

            returnValue = false;

        } else {
            //phoneTxt.setErrorEnabled(false);
            txtPhone.setError(null);
        }

        return returnValue;

    }

    private boolean validateEmail() {
        boolean returnValue = true;
        if (!com.globalgyan.getvalyou.utils.ValidationUtils.isEmailAddress(txtEmail, true, this)) {
            txtEmail.setError("Not a valid email address!");
            //  txtEmail.setBackgroundColor(Color.TRANSPARENT);
            returnValue = false;

        } else {
            txtEmail.setError(null);
        }

        return returnValue;

    }

    private boolean validatePassword() {
        boolean returnValue = true;
        if (!com.globalgyan.getvalyou.utils.ValidationUtils.passwordValid(txtPassword, this)) {

            returnValue = false;
            //txtPassword.setError("Password should be atleast 6 digits");


        } else {
            txtPassword.setError(null);
        }
        return returnValue;
    }

    private String getJSONFromRaw(int id) {
        InputStream is = getResources().openRawResource(id);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String jsonString = writer.toString();
        return jsonString;
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, GUICallback.RequestStatus status) {
        Log.e("signup","resp");
        if(signup_progress.isShowing()&&signup_progress!=null){
            signup_progress.dismiss();
        }
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {

                if (guiResponse instanceof ForgetPasswordResponse) {
                    ForgetPasswordResponse response = (ForgetPasswordResponse) guiResponse;
                    if (response.isStatus()) {

                        Intent intent = new Intent(SignupActivity.this, EnterOtpActivity.class);
                        intent.putExtra("otpf","no");
                        intent.putExtra("phn", txtPhone.getText().toString());
                        intent.putExtra("email", txtEmail.getText().toString());
                        intent.putExtra("username", loginusernamefield.getText().toString());
                        intent.putExtra("password", txtPassword.getText().toString());
                        intent.putExtra("id", response.get_id());
                        intent.putExtra("session_id", "jok");
                        intent.putExtra("from", "signup");
                        startActivity(intent);


                    } else {
                        if (!TextUtils.isEmpty(response.getMessage()))
                            Toast.makeText(SignupActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(SignupActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                    }

                } else if (guiResponse instanceof SendOtpResponse) {
                    SendOtpResponse sendOtpResponse = (SendOtpResponse) guiResponse;
                    if (sendOtpResponse.isStatus()) {
                        Intent intent = new Intent(SignupActivity.this, EnterOtpActivity.class);
                        intent.putExtra("otpf","yes");
                        intent.putExtra("phn", txtPhone.getText().toString());
                        intent.putExtra("email", txtEmail.getText().toString());
                        intent.putExtra("username", loginusernamefield.getText().toString());
                        intent.putExtra("password", txtPassword.getText().toString());
                        intent.putExtra("session_id", sendOtpResponse.getDetails());
                        intent.putExtra("from", "signup");
                        startActivity(intent);
                    }
                }
            } else {
                Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
       // signup.setEnabled(true);
        /*if (guiResponse != null) {
            if (status.equals(GUICallback.RequestStatus.SUCCESS)) {
                if (guiResponse instanceof RegisterResponse) {
                    RegisterResponse registerResponse = (RegisterResponse) guiResponse;
                    if (registerResponse != null) {
                        if (registerResponse.isStatus()) {

                            try {
                                FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                            } catch (Exception ex) {

                            }

                            DataBaseHelper dataBaseHelper = new DataBaseHelper(SignupActivity.this);
                            dataBaseHelper.deleteAllTables();
                            dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, registerResponse.getLoginModel().getDisplayName());
                            PreferenceUtils.setCandidateId(registerResponse.getLoginModel().get_id(), SignupActivity.this);


                            if (registerResponse.getLoginModel() != null) {
                                LoginModel loginModel = registerResponse.getLoginModel();
                                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                String dob = loginModel.getDateOfBirth();
                                if (!TextUtils.isEmpty(dob)) {
                                    try {
                                        //String date = DateUtils.UtcToJavaDateFormat(dob, new SimpleDateFormat("dd-MM-yyyy"));
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                    } catch (Exception ex) {

                                    }
                                }


                                //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOMAIN, loginModel.getDomain().toString());
                                //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_LOCATION, loginModel.getCurrentLocation());
                                //dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMPLOY_STATUS, loginModel.getEmploymentStatus());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());


                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());

                                PreferenceUtils.setCandidateId(loginModel.get_id(), SignupActivity.this);

                                AppConstant.OTP_FROM_SIGNUP = false;
                                String candidateId = PreferenceUtils.getCandidateId(SignupActivity.this);
                                if (!TextUtils.isEmpty(candidateId)) {
                                    UpdateMobileVerificationRequest request = new UpdateMobileVerificationRequest();
                                    request.setMobileVerified(true);
                                    // PreferenceUtils.setProfileDone(EnterOtpActivity.this);
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(SignupActivity.this, SignupActivity.this);
                                    JsonParser jsonParser = new JsonParser();
                                    JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());
                                    processor.updateMobileVerificationStatus(candidateId, gsonObject);
                                }


*//*
                            if (PreferenceUtils.isProfileDone(SignupActivity.this)) {
                                Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(SignupActivity.this, ProfileActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }*//*
                            }
                        } else {
                            Toast.makeText(this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else
                        Toast.makeText(this, "Server issuse", Toast.LENGTH_SHORT).show();
                }else if (guiResponse instanceof UpdateMobileVerificationResponse) {
                    UpdateMobileVerificationResponse response  = (UpdateMobileVerificationResponse) guiResponse;
                    if(response!=null && response.isStatus()){
                        PreferenceUtils.setVerified(SignupActivity.this);
                        // PreferenceUtils.setProfileDone(EnterOtpActivity.this);
                        Log.e("stattsssss",""+response.isStatus());
                        if (response.isStatus()) {

                            if (PreferenceUtils.isProfileDone(SignupActivity.this)) {
                                Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                                intent.putExtra("tabs","normal");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            }else {
                                Intent intent = new Intent(SignupActivity.this, Activity_Myself.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }

                    }

                }

            } else
                Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }*/
    }

    private class GetEywa extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {
            String result="",  responsestring = "";

            try {
               // httpClient = new DefaultHttpClient();
                String url = "http://admin.getvalyou.com/api/candidateMobileAppGetAllColleges";
                //httpGet = new HttpGet(url);


               /* httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
               // httpGet.setEntity(new UrlEncodedFormEntity(params));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();*/

               URL urlToRequest = new URL(url);
                HttpURLConnection urlConnection = (HttpURLConnection) urlToRequest.openConnection();

               urlConnection.setRequestMethod("GET");



               responsestring =readStream(urlConnection.getInputStream());
                result = responsestring;



            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            /*try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                jsoneya = sb.toString();


                Log.e("JSONNOYU", jsoneya);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }*/
            try {

                    JSONArray data = new JSONArray(result);
                    Log.e("QUESDATA", data.length() + "");
                    for(int i=0; i<data.length();i++){
                        JSONObject clgdata = data.getJSONObject(i);
                        myList.add(clgdata.getString("name"));
                    }
                    Log.e("aoa",myList.size()+"");

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            myAutoCompleteAdapter.notifyDataSetChanged();
        }

    }

    private String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            jsoneya = response.toString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsoneya;
    }


    @Override
    public void onBackPressed() {

    }
}
