package com.globalgyan.getvalyou;

public class CourseListUniversalmodel {
    String type,name,author,content,status_lock,lg_id,certi_id,certi_name,warn_msg,rating,desc_course,cost,url_ic,course_seen;
    int course_id,isdependent;
    public CourseListUniversalmodel() {
    }

    public String getCourse_seen() {
        return course_seen;
    }

    public void setCourse_seen(String course_seen) {
        this.course_seen = course_seen;
    }

    public String getDesc_course() {
        return desc_course;
    }

    public void setDesc_course(String desc_course) {
        this.desc_course = desc_course;
    }

    public String getUrl_ic() {
        return url_ic;
    }

    public void setUrl_ic(String url_ic) {
        this.url_ic = url_ic;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public int getIsdependent() {
        return isdependent;
    }

    public void setIsdependent(int isdependent) {
        this.isdependent = isdependent;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getWarn_msg() {
        return warn_msg;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setWarn_msg(String warn_msg) {
        this.warn_msg = warn_msg;
    }

    public String getCerti_name() {
        return certi_name;
    }

    public void setCerti_name(String certi_name) {
        this.certi_name = certi_name;
    }

    public String getCerti_id() {
        return certi_id;
    }

    public void setCerti_id(String certi_id) {
        this.certi_id = certi_id;
    }

    public String getLg_id() {
        return lg_id;
    }

    public void setLg_id(String lg_id) {
        this.lg_id = lg_id;
    }

    public String getStatus_lock() {
        return status_lock;
    }

    public void setStatus_lock(String status_lock) {
        this.status_lock = status_lock;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
