package com.globalgyan.getvalyou;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import com.globalgyan.getvalyou.SimpleGestureFilter.SimpleGestureListener;

public class GetSwipeEvents extends AppCompatActivity implements SimpleGestureListener  {
    private SimpleGestureFilter detector;
    boolean flag=false,para_touch=false;
    LinearLayout para,ques;
    RelativeLayout swipe_up;
    ScrollView paragraph_scroll,options_scroller;
    Animation animation_left,animation_right,animation_top,animation_bottom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_swipe_events);
        detector = new SimpleGestureFilter(GetSwipeEvents.this, this);
        para=(LinearLayout)findViewById(R.id.activity_fib_bg_p);
        ques=(LinearLayout)findViewById(R.id.activity_fib_bg_q);
        swipe_up=(RelativeLayout)findViewById(R.id.swipe_layout_up);
        paragraph_scroll=(ScrollView)findViewById(R.id.paragraph_scroll);
        options_scroller=(ScrollView)findViewById(R.id.options_scroller);
        para.setVisibility(View.VISIBLE);
         animation_left =
                AnimationUtils.loadAnimation(GetSwipeEvents.this,
                        R.anim.left_ques);
         animation_right =
                AnimationUtils.loadAnimation(GetSwipeEvents.this,
                        R.anim.right_ques);
        animation_top =
                AnimationUtils.loadAnimation(GetSwipeEvents.this,
                        R.anim.bottom_to_top);
        animation_bottom =
                AnimationUtils.loadAnimation(GetSwipeEvents.this,
                        R.anim.top_to_bottom);
        paragraph_scroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_DOWN==event.getActionMasked()){
                    para_touch=true;
                }
                if(MotionEvent.ACTION_UP==event.getActionMasked()){
                    new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            para_touch=false;
                        }
                    }.start();
                }
                return false;
            }
        });
        options_scroller.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_DOWN==event.getActionMasked()){
                    para_touch=true;
                }
                if(MotionEvent.ACTION_UP==event.getActionMasked()){
                    new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            para_touch=false;
                        }
                    }.start();
                }
                return false;
            }
        });
    }


    public void swipeUpEvent(View view){
        showQuestions();
    }
    public void swipeDownEvent(View view){
        showParagraph();
    }
    public void optA(View view){
        ques.startAnimation(animation_right);
        ques.setVisibility(View.VISIBLE);

        para.setVisibility(View.GONE);
    }
    public void optB(View view){
        ques.startAnimation(animation_right);
        ques.setVisibility(View.VISIBLE);
        para.setVisibility(View.GONE);
    }
    public void optC(View view){
        ques.startAnimation(animation_right);
        ques.setVisibility(View.VISIBLE);
        para.setVisibility(View.GONE);
    }
    public void optD(View view){
        ques.startAnimation(animation_right);
        ques.setVisibility(View.VISIBLE);
        para.setVisibility(View.GONE);
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {

        //Detect the swipe gestures and display toast
        if(!para_touch) {
            String showToastMessage = "";

            switch (direction) {

                case SimpleGestureFilter.SWIPE_RIGHT:
                    showToastMessage = "You have Swiped Right.";
                    break;
                case SimpleGestureFilter.SWIPE_LEFT:
                    showToastMessage = "You have Swiped Left.";
                    break;
                case SimpleGestureFilter.SWIPE_DOWN:
                    if(flag) {
                        showToastMessage = "You have Swiped Down.";
                        showParagraph();
                    }
                    break;
                case SimpleGestureFilter.SWIPE_UP:
                    if(!flag) {
                        showToastMessage = "You have Swiped Up.";
                        showQuestions();
                    }
                    break;

            }
            Toast.makeText(this, showToastMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private void showQuestions() {
        ques.startAnimation(animation_top);
        ques.setVisibility(View.VISIBLE);
        para.setVisibility(View.GONE);
        flag=true;
    }

    private void showParagraph() {
        para.startAnimation(animation_bottom);
        para.setVisibility(View.VISIBLE);
        ques.setVisibility(View.GONE);
        flag=false;
    }


    //Toast shown when double tapped on screen
    @Override
    public void onDoubleTap() {
        Toast.makeText(this, "You have Double Tapped.", Toast.LENGTH_SHORT)
                .show();
    }
}
