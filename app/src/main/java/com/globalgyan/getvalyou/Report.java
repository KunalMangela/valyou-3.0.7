package com.globalgyan.getvalyou;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 10/10/17.
 */

public class Report extends AppCompatActivity{
    ImageView backButton = null;
    Spinner spcat,spscat;
    Button resub;
    EditText reedit;
    String cat="",scat="",cid="";
    ConnectionUtils connectionUtils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report);
        connectionUtils=new ConnectionUtils(Report.this);
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        spcat = (Spinner)findViewById(R.id.spcat);
        spscat=(Spinner)findViewById(R.id.spscat);
        resub=(Button)findViewById(R.id.reports);
        reedit=(EditText)findViewById(R.id.reedit);
        cid= PreferenceUtils.getCandidateId(Report.this);
        backButton = (ImageView) findViewById(R.id.bckbtn);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
        resub.setTypeface(typeface1);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });

        List<String> categories = new ArrayList<String>();
        categories.add("Report an error");
        categories.add("Suggest an improvement");
//        categories.add("Computers");
//        categories.add("Education");
//        categories.add("Personal");
//        categories.add("Travel");

        // Creating adapter for spinner
         Report_adapter spinnerAdapter1 =  new Report_adapter(this, categories);

        // Drop down layout style - list view with radio button
      //  dataAdapter.setDropDownViewResource(R.id.items);

        // attaching data adapter to spinner
        spcat.setAdapter(spinnerAdapter1);

        spcat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        List<String> scategories = new ArrayList<String>();
        scategories.add("Question error\n");
        scategories.add("In-game error\n");
        scategories.add("Other error\n");


        // Creating adapter for spinner
     //   ArrayAdapter<String> sdataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, scategories);
        Report_adapter2 spinnerAdapter2 =  new Report_adapter2(this, scategories);
        // Drop down layout style - list view with radio button
      //  sdataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spscat.setAdapter(spinnerAdapter2);

        spscat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                scat=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        resub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(connectionUtils.isConnectionAvailable()){


                String mailTo = "mailto:" + "support@globalgyan.in" +
                        "?&subject=" + Uri.encode(cat+"\t"+scat) +
                        "&body=" + Uri.encode("CANDIDATE ID:"+cid+"\n"+reedit.getText().toString());
                Intent emailIntent = new Intent(Intent.ACTION_VIEW);
                emailIntent.setData(Uri.parse(mailTo));
                startActivity(emailIntent);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    finish();
                }else {
                    Toast.makeText(Report.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }

            }
        });
    }
    public class Report_adapter extends ArrayAdapter<String> {

        private Context ctx;
        private List<String> contentArray;

        public Report_adapter(Context context,List<String> objects) {
            super(context,  R.layout.spinner_items, R.id.items, objects);
            this.ctx = context;
            this.contentArray = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.spinner_items, parent, false);

            FontTextView textView = (FontTextView) row.findViewById(R.id.items);
            textView.setText(contentArray.get(position));



            return row;
        }
    }
    public class Report_adapter2 extends ArrayAdapter<String> {

        private Context ctx;
        private List<String> contentArray;

        public Report_adapter2(Context context,List<String> objects) {
            super(context,  R.layout.spinner_items, R.id.items, objects);
            this.ctx = context;
            this.contentArray = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.spinner_items, parent, false);

            FontTextView textView = (FontTextView) row.findViewById(R.id.items);
            textView.setText(contentArray.get(position));



            return row;
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
    }
}
