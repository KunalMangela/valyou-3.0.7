package com.globalgyan.getvalyou.simulation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class GallaryAvtivity extends AppCompatActivity {
    GridView gridView;
    GallaryAdapter gallaryAdapter;
    ArrayList<GallaryModel> gallaryAdapterList=new ArrayList<>();
    TextView img_tab,video_tab,audio_tab,no_media,heading_gallary;
    RelativeLayout rel;
    public static String folderappender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallary_avtivity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#5250B0"));
        }
        img_tab=(TextView)findViewById(R.id.image_tab);
        no_media=(TextView)findViewById(R.id.no_media);
        video_tab=(TextView)findViewById(R.id.video_tab);
        audio_tab=(TextView)findViewById(R.id.audio_tab);
        heading_gallary=(TextView)findViewById(R.id.heading_gallary);
        folderappender=getIntent().getStringExtra("sm_folder_name");
        gridView=(GridView)findViewById(R.id.gallary_grid);


        Typeface tf1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        heading_gallary.setTypeface(tf1);
        no_media.setTypeface(tf1);
        img_tab.setTypeface(tf1);
        video_tab.setTypeface(tf1);
        audio_tab.setTypeface(tf1);



        gallaryAdapterList.clear();
        try {
            getAllFiles(" ");
        }catch (Exception e){
            Toast.makeText(this,"No items available",Toast.LENGTH_SHORT).show();
        }


        ImageView im=(ImageView)findViewById(R.id.backarrow);
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Log.e("Files","loaded");

            }
        });


           }

    @Override
    protected void onResume() {
        super.onResume();
        gallaryAdapter.notifyDataSetChanged();
    }

    private void getAllFiles(String type_string) {
        File folder = new File(getFilesDir() +
                File.separator + "SimulationData"+folderappender);
        String path = folder.getPath();
        Log.e("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        Arrays.sort( files, new Comparator()
        {
            public int compare(Object o1, Object o2) {

                if (((File)o1).lastModified() > ((File)o2).lastModified()) {
                    return 1;
                } else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
                    return -1;
                } else {
                    return 0;
                }
            }

        });
        if(files.length==0){
            gridView.setVisibility(View.GONE);
            no_media.setVisibility(View.VISIBLE);
        }else {
            gridView.setVisibility(View.VISIBLE);
            no_media.setVisibility(View.GONE);
        }
        Log.e("Files", "Size: " + files.length);
        try{


        for (int i = 0; i < files.length; i++) {
            Log.e("Files", "FileName:" + files[i].getName());
            Log.e("Files", "FilePath:" + files[i].getPath());
            final int THUMBSIZE = 64;

          /*  Bitmap ThumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(files[i].getPath()),
                    THUMBSIZE, THUMBSIZE);*/
            if(type_string.equalsIgnoreCase(" ")){
                if ((files[i].getName().contains("jpg")) || (files[i].getName().contains("jpeg")) || (files[i].getName().contains("png"))
                        ||(files[i].getName().contains("JPG")) || (files[i].getName().contains("JPEG")) ||(files[i].getName().contains("gif"))
                        ||(files[i].getName().contains("Gif"))||(files[i].getName().contains("GIF"))|| (files[i].getName().contains("PNG"))) {
                    GallaryModel gallaryModel = new GallaryModel();

                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(files[i].getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);

                    gallaryModel.setName(files[i].getName());
                    gallaryModel.setPath(files[i].getPath());
                    gallaryModel.setThumbnail(bMap);

                    gallaryModel.setType("image");
                    gallaryAdapterList.add(gallaryModel);
                }


            }
            if(type_string.equalsIgnoreCase(" ")){

                  if ((files[i].getName().contains("mp4"))||(files[i].getName().contains("3gp"))  || (files[i].getName().contains("mpeg")) || (files[i].getName().contains("m4v")) || (files[i].getName().contains("mkv"))
                          ||(files[i].getName().contains("MP4"))||(files[i].getName().contains("3GP"))  || (files[i].getName().contains("MPEG")) || (files[i].getName().contains("M4V")) || (files[i].getName().contains("MKV"))) {

                      GallaryModel gallaryModel = new GallaryModel();
                      Bitmap bMap = ThumbnailUtils.createVideoThumbnail(files[i].getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);

                      gallaryModel.setName(files[i].getName());
                      gallaryModel.setPath(files[i].getPath());
                      gallaryModel.setThumbnail(bMap);

                      gallaryModel.setType("video");

                      gallaryAdapterList.add(gallaryModel);
                  }

            }
            if(type_string.equalsIgnoreCase(" ")){
                if ((files[i].getName().contains("mp3")) || (files[i].getName().contains("amr")) || (files[i].getName().contains("wav"))
                        ||(files[i].getName().contains("MP3")) || (files[i].getName().contains("AMR")) || (files[i].getName().contains("WAV"))) {
                    GallaryModel gallaryModel = new GallaryModel();

                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(files[i].getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);

                    gallaryModel.setName(files[i].getName());
                    gallaryModel.setPath(files[i].getPath());
                    gallaryModel.setThumbnail(bMap);

                    gallaryModel.setType("audio");
                    gallaryAdapterList.add(gallaryModel);

                }



            }

        }
        }catch (Exception e){
            e.printStackTrace();
        }
        gallaryAdapter=new GallaryAdapter(this,gallaryAdapterList);
        gridView.setAdapter(gallaryAdapter);
        gallaryAdapter.notifyDataSetChanged();
    }
}
