package com.globalgyan.getvalyou.simulation;

/**
 * Created by Globalgyan on 23-02-2018.
 */

public class DownloadEntryModel {
    int task_id,position;

    public DownloadEntryModel() {

    }

    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
