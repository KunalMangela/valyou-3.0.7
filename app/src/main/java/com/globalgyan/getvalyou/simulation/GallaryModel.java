package com.globalgyan.getvalyou.simulation;

import android.graphics.Bitmap;

/**
 * Created by Globalgyan on 16-02-2018.
 */

public class GallaryModel {
    String path,type,name;
    Bitmap thumbnail;

    public GallaryModel() {
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
