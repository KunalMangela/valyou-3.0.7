package com.globalgyan.getvalyou.simulation;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.CheckP;
import com.globalgyan.getvalyou.CourseLIstActivity;
import com.globalgyan.getvalyou.FeedbackActivity;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.ModulesActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.AssessFragment;
import com.globalgyan.getvalyou.simulation.core.DownloadManagerPro;
import com.globalgyan.getvalyou.simulation.report.listener.DownloadManagerListener;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.client.ClientProtocolException;
import uk.co.senab.photoview.PhotoViewAttacher;

import static com.globalgyan.getvalyou.HomeActivity.token;

public class SimulationActivity extends AppCompatActivity implements DownloadManagerListener {


    public static String comparable_username,from_where;
    static RecyclerView rc;
    static boolean resumeflg=false,is_media_download_click=false,is_video_downloaded=false,is_audio_downloaded=false;
    static ExpertCardAdapter expertCardAdapter;
    static int temp_pos=0;
    public static String tok;
    public static ArrayList<DownloadEntryModel> download_content_entry = new ArrayList<>();
    public static ArrayList<MessageModel> arrayList = new ArrayList<>();
    public static ArrayList<MessageModel> arrayListtemp = new ArrayList<>();
    public static ArrayList<String> arrayList_imgid = new ArrayList<>();
    public static ArrayList<String> arrayList_vidid = new ArrayList<>();
    public static ArrayList<String> arrayList_Audio = new ArrayList<>();
    public static ArrayList<String> arrayList_imgid_down = new ArrayList<>();
    public static ArrayList<String> arrayList_vidid_down = new ArrayList<>();
    public static ArrayList<String> arrayList_Audio_down = new ArrayList<>();
    public static ArrayList<String> arrayList_Media_in_everyload = new ArrayList<>();
    static long downloadID;
    public static String rsp,sm_id_is;
    public static int limit;
    static Context context;
    public static RecyclerView.OnItemTouchListener disabler;
    public static String url= AppConstant.Ip_url+"simulation";
    static ProgressDialog progressDialog;
    public static JSONObject finaljobj=new JSONObject();
    static int i=0;
    static boolean flag_netcheck=false,flag_first_load=false,putdata_flag=false,ispaused=false,isfocused=false,finish_simulation=false;
    static Timer timer,timer1;
    static TimerTask timertask,timertask1;
    static ArrayList<Integer> timerList=new ArrayList<>();
    static JSONArray js = null;
    static int timervlaueis;
    static RelativeLayout loading_layout;
    static ImageView gif;
    static  AlertDialog alertDialog;
    static AlertDialog.Builder alertDialogBuilder;
    static JSONArray jarray_container;
    static int width,height;
    static File folder,bg_assets;
    static boolean resumeflag_is=false,temp_finish=false;
    public static long image_id,video_id,audio_id;
    static DownloadManagerPro dm;
    public static AlertDialog alert11;
    static  MediaController mediacontroller,mediacontroller1;
    static String what_is_playing="image",background,dec_bg_color,background_music;
    static Dialog qs_progress;
    public static ArrayList<FiledownloadModel> file_list=new ArrayList<>();
    public static String downloaded_file="",downloaded_type="";
    public static LinearLayout lr;
    public static String finishobj;
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="",main_name;

    public static MediaPlayer mp,mp_click;
    public static ImageView img_bg;
    public static FrameLayout fr_img,fr_video;
    public static ImageView imgfullscreen_close,videofullscreen_close,imgfullscreen;
    public static VideoView videofullscreen;
    public static MediaController mediaController11;
    public static LinearLayoutManager linearLayoutManager;
    public static String candidateId;
    public static ImageView more,back;
    public static TextView touch_to_procced;
    public static HttpURLConnection urlConnection2;
    public static int code2;
    static InputStream is = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simulationactivity_layout);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#5250B0"));
        }
        //img_bg=(ImageView)findViewById(R.id.img_bg);
        tok=new PrefManager(SimulationActivity.this).getToken();
        from_where=new PrefManager(SimulationActivity.this).getNav();
        mediacontroller = new MediaController(
                this);
        mediacontroller1 = new MediaController(
                this);
        TextView heading=(TextView)findViewById(R.id.heading);

        heading.setText(getIntent().getStringExtra("sm_name_is"));
        main_name=getIntent().getStringExtra("sm_name_is");
       // main_name=main_name.trim();
      //  main_name = main_name.replace(" ", "");

        bg_assets= new File(Environment.getExternalStorageDirectory() +
                File.separator + "SimulationData"+main_name);
        more=(ImageView)findViewById(R.id.menu);
        back=(ImageView)findViewById(R.id.backarrow);
        back.setEnabled(false);
        fr_img=(FrameLayout)findViewById(R.id.frameimage);
        fr_video=(FrameLayout)findViewById(R.id.framevideo);
        touch_to_procced=(TextView)findViewById(R.id.btn_touch);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        touch_to_procced.setTypeface(tf2);
        heading.setTypeface(tf2);
        imgfullscreen=(ImageView)findViewById(R.id.imageview_fullview);
        imgfullscreen_close=(ImageView)findViewById(R.id.closeimage);
        videofullscreen_close=(ImageView)findViewById(R.id.closevideo);
        imgfullscreen=(ImageView)findViewById(R.id.imageview_fullview);
        videofullscreen=(VideoView)findViewById(R.id.video_fullview);
        background=getIntent().getStringExtra("bg_imageis");
        dec_bg_color=getIntent().getStringExtra("dec_bg");
        background_music=getIntent().getStringExtra("bg_soundis");
        sm_id_is=getIntent().getStringExtra("sm_id");
        mp_click=MediaPlayer.create(this,R.raw.button_click);
        candidateId = PreferenceUtils.getCandidateId(SimulationActivity.this);
        disabler = new RecyclerViewDisabler();
        context=SimulationActivity.this;
        rc = (RecyclerView) findViewById(R.id.rc);
        lr=(LinearLayout) findViewById(R.id.linear_main);
        alertDialogBuilder = new AlertDialog.Builder(this);
        loading_layout=(RelativeLayout)findViewById(R.id.loading_img);
        touch_to_procced.setEnabled(false);
        gif=(ImageView)findViewById(R.id.gif_load);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gif);
        Glide.with(this).load(R.drawable.gif).into(imageViewTarget);

        createprogress();

        Typeface tf1 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        heading.setTypeface(tf1);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        height=height-getPixel(SimulationActivity.this,120);
         linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setStackFromEnd(true);
        rc.setLayoutManager(linearLayoutManager);
        rc.setNestedScrollingEnabled(false);


        expertCardAdapter=new ExpertCardAdapter(this,arrayList);
        //   subjectAdapter = new SubjectAdapter(this, arrayList);
        rc.setAdapter(expertCardAdapter);

        // you can only pass this for context but here i want to show clearly
        createFolder();
        dm = new DownloadManagerPro(this.getApplicationContext());
        dm.init(folder.getPath(), 12, SimulationActivity.this);



        if(background.equalsIgnoreCase("no")){

        }else {
            if(background.contains("#")){
                lr.setBackgroundColor(Color.parseColor(background));
            }else {
                File bg = new File(bg_assets, background);
                Bitmap myBitmap = BitmapFactory.decodeFile(bg.getAbsolutePath());
                Drawable d = new BitmapDrawable(context.getResources(), myBitmap);
                lr.setBackground(d);
            }
        }


        if(background_music.equalsIgnoreCase("no")){
            mp=MediaPlayer.create(context,R.raw.bg_sound);
            mp.setLooping(true);

        }else {
            File bg_music = new File(bg_assets, background_music);
            mp = MediaPlayer.create(context, Uri.parse(bg_music.getPath()));
            mp.setLooping(true);

        }


        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                try {
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        touch_to_procced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(finish_simulation){
                    Log.e("clickitem","finish");

                    touch_to_procced.setEnabled(false);
                    touch_to_procced.setVisibility(View.GONE);
                    putfinaldataintoList();
                }else {
                 /*   Boolean load_flag = false;
                    int count = 0;
                    for (int i = 0; i < arrayList_Media_in_everyload.size(); i++) {

                        String[] pic = arrayList_Media_in_everyload.get(i).split("/");
                        String last = pic[pic.length - 1];
                        String[] lastname = last.split("\\.");
                        String name = lastname[0];
                        File img_is = new File(folder, last);
                        if (img_is.exists()) {
                            count++;
                        }

                    }
                    if (arrayList_Media_in_everyload.size() == count) {
                        Log.e("loaddata", "true");
                        loadMoreData();
                    } else {
                        Toast.makeText(SimulationActivity.this, "Please download media first", Toast.LENGTH_SHORT).show();
                        Log.e("loaddata", "false");
                        touch_to_procced.setEnabled(false);
                        new CountDownTimer(2000, 2000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                touch_to_procced.setEnabled(true);
                            }
                        }.start();

                    }

*/
                }
            }
        });
        if(isConnectionAvailable()){

            loadFirst();
        }else {

            Toast.makeText(SimulationActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
            //createAlertDialogue();
            back.setEnabled(true);
            internetDialogue();
        }



        rc.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                mediacontroller.hide();
                mediacontroller1.hide();
                Log.e("jj","1");
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mediacontroller.hide();
                mediacontroller1.hide();

               /* if(isfocused){
                    expertCardAdapter.notifyDataSetChanged();
                }*/
              // recyclerView.smoothScrollBy(dx, dy);
                int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                Log.e("diff",String.valueOf(temp_pos));
                Log.e("diff",String.valueOf(firstVisiblePosition));
                if((temp_pos-firstVisiblePosition>1)||(firstVisiblePosition-temp_pos>1)){
                    Log.e("diff","success");

                    if(!ispaused) {
                        if (mp.isPlaying()) {

                        } else {
                            mp.start();
                        }
                    }
                }
                temp_pos=firstVisiblePosition;

            }
        });




        videofullscreen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.setEnabled(true);
                back.setClickable(true);
                mp.start();
                try {
                    mediaController11.hide();
                }catch (Exception e){

                }
                videofullscreen.pause();
               /* videofullscreen=null;
                videofullscreen=(VideoView)findViewById(R.id.video_fullview);
              */  lr.setVisibility(View.VISIBLE);
                fr_video.setVisibility(View.GONE);
                fr_img.setVisibility(View.GONE);

            }
        });
        imgfullscreen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.setEnabled(true);
                back.setClickable(true);
                lr.setVisibility(View.VISIBLE);
                fr_video.setVisibility(View.GONE);
                fr_img.setVisibility(View.GONE);
            }
        });
    }

    public void createprogress() {
        qs_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        qs_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qs_progress.setContentView(R.layout.planet_loader);
        Window window = qs_progress.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        qs_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) qs_progress.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        qs_progress.setCancelable(false);
      //  qs_progress.show();

    }

    private void putfinaldataintoList() {
        try {
        MessageModel messageModel = new MessageModel();
            messageModel.setSm_data_id(finaljobj.getString("sm_data_id"));
        messageModel.setU_id(finaljobj.getString("u_id"));
        messageModel.setConversation_order(finaljobj.getString("coversation_order"));
        messageModel.setParent_conv_id(finaljobj.getString("parent_coversation_ids"));
        messageModel.setExpression(finaljobj.getString("expression"));
        messageModel.setUsername(finaljobj.getString("username"));
        messageModel.setConversation_text(finaljobj.getString("conversation_text"));
        messageModel.setDecision(finaljobj.getString("decision_text"));
        messageModel.setScores(finaljobj.getString("scores"));
        messageModel.setImage(finaljobj.getString("conversation_image_name"));
        messageModel.setAudio(finaljobj.getString("conversation_audio"));
        messageModel.setVideo(finaljobj.getString("conversation_video"));
        messageModel.setStatus(finaljobj.getString("sm_status"));
        messageModel.setUser_image(finaljobj.getString("user_image"));
        messageModel.setSimulation_name(finaljobj.getString("sm_name"));
        messageModel.setAlign(finaljobj.getString("aling"));
        messageModel.setIsfinished("2");
        messageModel.setScores(finaljobj.getString("scores"));
        messageModel.setCompetency_name(finaljobj.getString("competency_name"));
        messageModel.setCompetency_id(finaljobj.getString("competency_id"));
        messageModel.setSimulation_id(finaljobj.getString("sm_id"));
        messageModel.setMessageis(finaljobj.getString("message"));
        messageModel.setTotalscore(finaljobj.getString("total_score"));
        messageModel.setBg_wallpaper(finaljobj.getString("sm_bg_image"));
            messageModel.setTy_text(finaljobj.getString("sm_thankyou_text"));
            messageModel.setTy_desc(finaljobj.getString("sm_thankyou_description"));
            messageModel.setTy_percentage(finaljobj.getString("sm_percentage"));
            messageModel.setTy_img(finaljobj.getString("sm_thankyou_background_image"));

            arrayList.add(messageModel);
            try {
                expertCardAdapter.notifyDataSetChanged();
            }catch (Exception e){

            }
            rc.smoothScrollToPosition(rc.getAdapter().getItemCount() );

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //create app data folder
    private void createFolder() {
        folder = new File(/*Environment.getExternalStorageDirectory()*/getFilesDir() +
                File.separator + "SimulationData"+main_name);
        boolean success = true;
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    //first time loading of simulation
    public static void loadFirst() {
        //get data from api for the first time

        i=0;
        String params = "sm_data_id=0&u_id="+candidateId+"&simulation_id="+sm_id_is+"&scores=0";
        SendData sendData=new SendData();
        sendData.execute(url,params);

    }

    @Override
    public void OnDownloadStarted(long taskId) {

    }

    @Override
    public void OnDownloadPaused(long taskId) {

    }

    @Override
    public void onDownloadProcess(long taskId, double percent, long downloadedLength) {

    }

    @Override
    public void OnDownloadFinished(long taskId) {

        for(int i=0;i<file_list.size();i++){
            FiledownloadModel filedownloadModel=file_list.get(i);
            if(filedownloadModel.getId()==taskId){
                downloaded_file=filedownloadModel.getName();
                downloaded_type=filedownloadModel.getType();
                Log.e("typeis",downloaded_type);
                break;
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    expertCardAdapter.notifyDataSetChanged();
                    Log.e("status", "notify");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void OnDownloadRebuildStart(long taskId) {

    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {

    }

    @Override
    public void OnDownloadCompleted(long taskId) {

    }

    @Override
    public void connectionLost(long taskId) {
      //  Log.e("connectionstatus","failed");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                expertCardAdapter.notifyDataSetChanged();
                Log.e("abcd","notify");
            }
        });
        dm.delete((int) taskId,true);
        for(int i=0;i<download_content_entry.size();i++){
            DownloadEntryModel downloadEntryModel=download_content_entry.get(i);
            final int pos_temp=downloadEntryModel.getPosition();
            if(downloadEntryModel.getTask_id()==taskId){
                for(int i1=0;i1<ExpertCardAdapter.click_download_index.size();i1++){
                    if(ExpertCardAdapter.click_download_index.get(i)==pos_temp){
                        ExpertCardAdapter.click_download_index.remove(i);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                expertCardAdapter.notifyDataSetChanged();
                                Log.e("abcd","notify");
                            }
                        });


                        break;
                    }
                }
                break;
            }
        }


    }



    //on decision click simulation
    public static void sendAnswersData(final String s){
        //send answers api on button click

        if(isConnectionAvailable()){

            //qs_progress.show();
          //  showAlert();
            i=0;
            SendData sendData=new SendData();
            sendData.execute(url,s);


        }else {
            Toast.makeText(context,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
            //createAlertDialogue();
            back.setEnabled(true);
        }

    }
    public static class SendData extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  loading_layout.setVisibility(View.VISIBLE);
            showAlert();*/
        //  showAlert();
            loading_layout.setVisibility(View.VISIBLE);
            rc.addOnItemTouchListener(disabler);
            Log.e("time","start");

        }

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";
         /*   try {
                Thread.currentThread();
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
*/
            try {
                url = new URL(params[0]);
                ref_id = params[1];
                Log.v("URL", "URL: " + url);
                Log.v("postParameters",ref_id);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Authorization", "Bearer " + tok);

                /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();

                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
                try{
                   // qs_progress.dismiss();
                 //   alertDialog.dismiss();
                    back.setEnabled(true);
                }catch (Exception ex){


                }
//                loading_layout.setVisibility(View.INVISIBLE);
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
           /* if(s.equalsIgnoreCase("")||s.equalsIgnoreCase(null)){
                loading_layout.setVisibility(View.INVISIBLE);


            }*/
            rc.removeOnItemTouchListener(disabler);
            /*try{
                //  qs_progress.dismiss();
                alertDialog.dismiss();
            }catch (Exception e){

            }
            */Log.e("time","stop");

            try {
                if (s.equalsIgnoreCase("") || s.equalsIgnoreCase(null)) {

                   // back.setEnabled(true);
                    back.setEnabled(true);
                }else {
                    loadJson(s);
                }
            }catch (Exception e){

            }

        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }

    private static void loadJson(String s) {
        try {
            if (s.equalsIgnoreCase("") || s.equalsIgnoreCase(null)) {
                back.setEnabled(true);
            }else {
                mp.start();
            }
        }catch (Exception e){

        }

       /* try {
            alertDialog.dismiss();
        }catch (Exception e){

        }*/
        putdata_flag=true;
        rsp=s;
        Log.e("responseis",s);

        //crete temporary array(without images audio and video)
        try {
            JSONArray tempjsarray=new JSONArray(rsp);
            jarray_container=new JSONArray();
            for(int j1=0;j1<tempjsarray.length();j1++){
                JSONObject jsobj_container=new JSONObject();
                JSONObject jsonObject=tempjsarray.getJSONObject(j1);
                jsobj_container.put("sm_data_id",jsonObject.getString("sm_data_id"));
                jsobj_container.put("u_id",jsonObject.getString("u_id"));
                jsobj_container.put("coversation_order",jsonObject.getString("coversation_order"));
                jsobj_container.put("parent_coversation_ids",jsonObject.getString("parent_coversation_ids"));
                jsobj_container.put("expression",jsonObject.getString("expression"));
                jsobj_container.put("username",jsonObject.getString("username"));
                jsobj_container.put("conversation_text",jsonObject.getString("conversation_text"));
                jsobj_container.put("decision_text",jsonObject.getString("decision_text"));
                jsobj_container.put("scores",jsonObject.getString("scores"));
                jsobj_container.put("conversation_image_name",jsonObject.getString("conversation_image_name"));
                jsobj_container.put("conversation_audio",jsonObject.getString("conversation_audio"));
                jsobj_container.put("conversation_video",jsonObject.getString("conversation_video"));

                jsobj_container.put("sm_status",jsonObject.getString("status"));
                jsobj_container.put("user_image",jsonObject.getString("user_image"));
                jsobj_container.put("sm_name",jsonObject.getString("sm_name"));
                jsobj_container.put("aling",jsonObject.getString("aling"));
                jsobj_container.put("is_finished",jsonObject.getString("is_finished"));
                jsobj_container.put("scores",jsonObject.getString("scores"));

                jsobj_container.put("competency_name",jsonObject.getString("competency_name"));
                jsobj_container.put("sm_id",jsonObject.getString("sm_id"));
                jsobj_container.put("competency_id",jsonObject.getString("competency_id"));
                jsobj_container.put("message",jsonObject.getString("message"));
                jsobj_container.put("total_score",jsonObject.getString("total_score"));
                jsobj_container.put("sm_bg_image",jsonObject.getString("sm_bg_image"));

                jsobj_container.put("sm_thankyou_text",jsonObject.getString("sm_thankyou_text"));
                jsobj_container.put("sm_thankyou_description",jsonObject.getString("sm_thankyou_description"));
                jsobj_container.put("sm_percentage",jsonObject.getString("sm_percentage"));
                jsobj_container.put("sm_thankyou_background_image",jsonObject.getString("sm_thankyou_background_image"));

                if(Integer.parseInt(jsonObject.getString("is_finished"))==1){
                   finaljobj=jsobj_container;
                }
                jarray_container.put(jsobj_container);

                /*if(jsonObject.getString("is_finished").equalsIgnoreCase("0")){
                    if((jsonObject.getString("conversation_text").equalsIgnoreCase(""))&&
                            (jsonObject.getString("decision_text").equalsIgnoreCase(""))){

                    }else {
                        jarray_container.put(jsobj_container);
                    }

                }else {
                    jarray_container.put(jsobj_container);

                }*/

            }
        } catch (JSONException e) {
            e.printStackTrace();
            try{
                //  qs_progress.dismiss();
               // alertDialog.dismiss();
                back.setEnabled(true);
                loading_layout.setVisibility(View.GONE);
                Toast.makeText(context, "Error! while loading data", Toast.LENGTH_SHORT).show();
            }catch (Exception ex){

            }
        }


        try {
            js = new JSONArray(s);
            final Handler handler = new Handler();
            final JSONArray finalJs = js;
            final JSONArray finalJs1 = jarray_container;

            loadFirstElement();
            timertask1 = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            if(i<jarray_container.length()) {
                                loading_layout.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            };
            timer1 = new Timer();

            timer1.schedule(timertask1,0,1600);
            Log.e("timer","changed1");


            new CountDownTimer(1500, 1500) {
                @Override
                public void onTick(long millisUntilFinished)
                {

                }

                @Override
                public void onFinish() {

                    timertask = new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                public void run() {

                                    if(i< jarray_container.length()) {


                                        loading_layout.setVisibility(View.INVISIBLE);
                                        JSONObject jo = null;
                                        try {
                                            jo = jarray_container.getJSONObject(i);

                                            MessageModel messageModel = new MessageModel();
                                            messageModel.setSm_data_id(jo.getString("sm_data_id"));
                                            messageModel.setU_id(jo.getString("u_id"));
                                            messageModel.setConversation_order(jo.getString("coversation_order"));
                                            messageModel.setParent_conv_id(jo.getString("parent_coversation_ids"));
                                            messageModel.setExpression(jo.getString("expression"));
                                            if (i == 0) {
                                                comparable_username = jo.getString("username");
                                            }
                                            messageModel.setUsername(jo.getString("username"));
                                            messageModel.setConversation_text(jo.getString("conversation_text"));
                                            messageModel.setDecision(jo.getString("decision_text"));
                                            messageModel.setScores(jo.getString("scores"));
                                            messageModel.setImage(jo.getString("conversation_image_name"));
                                            messageModel.setTy_text(jo.getString("sm_thankyou_text"));
                                            messageModel.setTy_desc(jo.getString("sm_thankyou_description"));
                                            messageModel.setTy_percentage(jo.getString("sm_percentage"));
                                            messageModel.setTy_img(jo.getString("sm_thankyou_background_image"));

                                          /*  if(!jo.getString("conversation_image_name").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(2, 10000);
                                            }
                                            if(!jo.getString("conversation_video").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(10, 30000);
                                            }
                                            if(!jo.getString("conversation_audio").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(10, 20000);
                                            }*/
                                            messageModel.setAudio(jo.getString("conversation_audio"));
                                            messageModel.setVideo(jo.getString("conversation_video"));
                                            messageModel.setStatus(jo.getString("sm_status"));
                                            messageModel.setUser_image(jo.getString("user_image"));
                                            messageModel.setSimulation_name(jo.getString("sm_name"));
                                            messageModel.setAlign(jo.getString("aling"));
                                            messageModel.setIsfinished(jo.getString("is_finished"));
                                            messageModel.setScores(jo.getString("scores"));
                                            messageModel.setCompetency_name(jo.getString("competency_name"));
                                            messageModel.setCompetency_id(jo.getString("competency_id"));
                                            messageModel.setSimulation_id(jo.getString("sm_id"));
                                            messageModel.setMessageis(jo.getString("message"));
                                            messageModel.setTotalscore(jo.getString("total_score"));
                                            messageModel.setBg_wallpaper(jo.getString("sm_bg_image"));
                                            messageModel.setTy_text(jo.getString("sm_thankyou_text"));
                                            messageModel.setTy_desc(jo.getString("sm_thankyou_description"));
                                            messageModel.setTy_percentage(jo.getString("sm_percentage"));
                                            messageModel.setTy_img(jo.getString("sm_thankyou_background_image"));

                                            Log.e("data is", i + jo.getString("username"));
                                            if(!jo.getString("decision_text").equalsIgnoreCase("")/*&&
                                                    jo.getString("conversation_image_name").equalsIgnoreCase("")
                                                    &&jo.getString("conversation_audio").equalsIgnoreCase("")&&
                                                    jo.getString("conversation_video").equalsIgnoreCase("")*/){

                                                //if values are decisions
                                                Log.e("mmmm", "mmm");

                                                timer1.cancel();
                                                timer.cancel();
                                                putAnswers();
                                            }else {
                                                //if values are convesation
                                                arrayList.add(messageModel);
                                                try {
                                                    expertCardAdapter.notifyDataSetChanged();
                                                }catch (Exception e){

                                                }
                                                rc.smoothScrollToPosition(rc.getAdapter().getItemCount() );
                                                loading_layout.setVisibility(View.INVISIBLE);

                                                i++;
                                                if(jo.getString("conversation_image_name").length()>4){
                                                   arrayList_Media_in_everyload.add(jo.getString("conversation_image_name"));
                                                }
                                                if(jo.getString("conversation_audio").length()>4){
                                                    arrayList_Media_in_everyload.add(jo.getString("conversation_audio"));
                                                }
                                                if(jo.getString("conversation_video").length()>4){
                                                    arrayList_Media_in_everyload.add(jo.getString("conversation_video"));

                                                }
                                               /* if(i%3==0){
                                                    timer1.cancel();
                                                    timer.cancel();
                                                    touch_to_procced.setEnabled(true);
                                                    loading_layout.setVisibility(View.GONE);
                                                    touch_to_procced.setVisibility(View.VISIBLE);
                                                   // gif.setImageResource(R.drawable.load_ic);
                                                }*/
                                            }

                                            back.setEnabled(true);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Log.e("data is", "exception");
                                        }
                                    }else {
                                        timer1.cancel();
                                        timer.cancel();
                                        Log.e("data is", "exdiscardception");

                                    }

                                }
                            });
                        }
                    };
//rc.smoothScrollToPosition(rc.getAdapter().getItemCount() );
                    timer = new Timer();
                    // timer.schedule(timertask,0,5000);

                    timer.schedule(timertask,0,3000);
                    Log.e("timer","changed1");

                }
            }.start();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Result","Exc");

        }


    }

    private static void loadFirstElement() {
        loading_layout.setVisibility(View.INVISIBLE);
        JSONObject jo = null;
        try {
            jo = jarray_container.getJSONObject(0);

            MessageModel messageModel = new MessageModel();
            messageModel.setSm_data_id(jo.getString("sm_data_id"));
            messageModel.setU_id(jo.getString("u_id"));
            messageModel.setConversation_order(jo.getString("coversation_order"));
            messageModel.setParent_conv_id(jo.getString("parent_coversation_ids"));
            messageModel.setExpression(jo.getString("expression"));
            if (i == 0) {
                comparable_username = jo.getString("username");
            }
            messageModel.setUsername(jo.getString("username"));
            messageModel.setConversation_text(jo.getString("conversation_text"));
            messageModel.setDecision(jo.getString("decision_text"));
            messageModel.setScores(jo.getString("scores"));
            messageModel.setImage(jo.getString("conversation_image_name"));
                                          /*  if(!jo.getString("conversation_image_name").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(2, 10000);
                                            }
                                            if(!jo.getString("conversation_video").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(10, 30000);
                                            }
                                            if(!jo.getString("conversation_audio").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(10, 20000);
                                            }*/
            messageModel.setAudio(jo.getString("conversation_audio"));
            messageModel.setVideo(jo.getString("conversation_video"));
            messageModel.setStatus(jo.getString("sm_status"));
            messageModel.setUser_image(jo.getString("user_image"));
            messageModel.setSimulation_name(jo.getString("sm_name"));
            messageModel.setAlign(jo.getString("aling"));
            messageModel.setIsfinished(jo.getString("is_finished"));
            messageModel.setScores(jo.getString("scores"));
            messageModel.setCompetency_name(jo.getString("competency_name"));
            messageModel.setCompetency_id(jo.getString("competency_id"));
            messageModel.setSimulation_id(jo.getString("sm_id"));
            messageModel.setMessageis(jo.getString("message"));
            messageModel.setTotalscore(jo.getString("total_score"));
            messageModel.setBg_wallpaper(jo.getString("sm_bg_image"));
            messageModel.setTy_text(jo.getString("sm_thankyou_text"));
            messageModel.setTy_desc(jo.getString("sm_thankyou_description"));
            messageModel.setTy_percentage(jo.getString("sm_percentage"));
            messageModel.setTy_img(jo.getString("sm_thankyou_background_image"));

            Log.e("data is", i + jo.getString("username"));
            if(!jo.getString("decision_text").equalsIgnoreCase("")/*&&
                                                    jo.getString("conversation_image_name").equalsIgnoreCase("")
                                                    &&jo.getString("conversation_audio").equalsIgnoreCase("")&&
                                                    jo.getString("conversation_video").equalsIgnoreCase("")*/){

                //if values are decisions
                Log.e("mmmm", "mmm");
                timer1.cancel();
                timer.cancel();
                putAnswers();
            }else {
                //if values are convesation
                arrayList.add(messageModel);
                expertCardAdapter.notifyDataSetChanged();
                rc.smoothScrollToPosition(rc.getAdapter().getItemCount() );
                i++;
                loading_layout.setVisibility(View.INVISIBLE);
            }




        } catch (Exception e) {
            e.printStackTrace();
            Log.e("data is", "exception");
        }

    }


    public static Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    //put decisions at a time
    private static void putAnswers() {
        for (int j = i; j < jarray_container.length(); j++) {
            JSONObject jo = null;
            try {
                jo = jarray_container.getJSONObject(i);

                MessageModel messageModel = new MessageModel();
                messageModel.setSm_data_id(jo.getString("sm_data_id"));
                messageModel.setU_id(jo.getString("u_id"));
                messageModel.setConversation_order(jo.getString("coversation_order"));
                messageModel.setParent_conv_id(jo.getString("parent_coversation_ids"));
                messageModel.setExpression(jo.getString("expression"));
                if (i == 0) {
                    comparable_username = jo.getString("username");
                }
                messageModel.setUsername(jo.getString("username"));
                messageModel.setConversation_text(jo.getString("conversation_text"));
                messageModel.setDecision(jo.getString("decision_text"));
                messageModel.setScores(jo.getString("scores"));
                messageModel.setImage(jo.getString("conversation_image_name"));
                messageModel.setAudio(jo.getString("conversation_audio"));
                messageModel.setVideo(jo.getString("conversation_video"));
                messageModel.setStatus(jo.getString("sm_status"));
                messageModel.setUser_image(jo.getString("user_image"));
                messageModel.setSimulation_name(jo.getString("sm_name"));
                messageModel.setAlign(jo.getString("aling"));
                messageModel.setIsfinished(jo.getString("is_finished"));
                messageModel.setScores(jo.getString("scores"));
                messageModel.setCompetency_name(jo.getString("competency_name"));
                messageModel.setCompetency_id(jo.getString("competency_id"));
                messageModel.setSimulation_id(jo.getString("sm_id"));
                messageModel.setMessageis(jo.getString("message"));
                messageModel.setTotalscore(jo.getString("total_score"));
                messageModel.setBg_wallpaper(jo.getString("sm_bg_image"));
                messageModel.setTy_text(jo.getString("sm_thankyou_text"));
                messageModel.setTy_desc(jo.getString("sm_thankyou_description"));
                messageModel.setTy_percentage(jo.getString("sm_percentage"));
                messageModel.setTy_img(jo.getString("sm_thankyou_background_image"));

                Log.e("data is", i + jo.getString("username"));
                arrayList.add(messageModel);
                expertCardAdapter.notifyDataSetChanged();
                rc.smoothScrollToPosition(rc.getAdapter().getItemCount());
                i++;


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        putdata_flag=false;
    }

    @Override
    public void onBackPressed() {

    }


    public static void showAlert(){

        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    //back button click
    public void backarrow(View view){
        exitDialogue();
    }

    //gallary icon click
    public void menuMore(View view){

        //Toast.makeText(this,"No items available now",Toast.LENGTH_SHORT).show();
        try{
            mp.pause();

        }catch (Exception e){

        }
        Intent i = new Intent(SimulationActivity.this, GallaryAvtivity.class);
        //  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra("sm_folder_name",main_name);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


    }

    //load more icon click
    public void loadMoreData(){
        Log.e("loading","clicked");
        arrayList_Media_in_everyload.clear();
        try{

            touch_to_procced.setEnabled(false);
            touch_to_procced.setVisibility(View.GONE);
            loading_layout.setVisibility(View.VISIBLE);
            final Handler handler = new Handler();
            timertask1 = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            if(i<jarray_container.length()) {
                                loading_layout.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            };
            timer1 = new Timer();

            timer1.schedule(timertask1,0,1600);
            Log.e("timer","changed1");


            new CountDownTimer(1500, 1500) {
                @Override
                public void onTick(long millisUntilFinished)
                {

                }

                @Override
                public void onFinish() {

                    timertask = new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                public void run() {

                                    if(i< jarray_container.length()) {


                                        loading_layout.setVisibility(View.INVISIBLE);
                                        JSONObject jo = null;
                                        try {
                                            jo = jarray_container.getJSONObject(i);

                                            MessageModel messageModel = new MessageModel();
                                            messageModel.setSm_data_id(jo.getString("sm_data_id"));
                                            messageModel.setU_id(jo.getString("u_id"));
                                            messageModel.setConversation_order(jo.getString("coversation_order"));
                                            messageModel.setParent_conv_id(jo.getString("parent_coversation_ids"));
                                            messageModel.setExpression(jo.getString("expression"));
                                            if (i == 0) {
                                                comparable_username = jo.getString("username");
                                            }
                                            messageModel.setUsername(jo.getString("username"));
                                            messageModel.setConversation_text(jo.getString("conversation_text"));
                                            messageModel.setDecision(jo.getString("decision_text"));
                                            messageModel.setScores(jo.getString("scores"));
                                            messageModel.setImage(jo.getString("conversation_image_name"));
                                          /*  if(!jo.getString("conversation_image_name").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(2, 10000);
                                            }
                                            if(!jo.getString("conversation_video").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(10, 30000);
                                            }
                                            if(!jo.getString("conversation_audio").equalsIgnoreCase("")){
                                                expertCardAdapter.cr_img.setProgressWithAnimation(10, 20000);
                                            }*/
                                            messageModel.setAudio(jo.getString("conversation_audio"));
                                            messageModel.setVideo(jo.getString("conversation_video"));
                                            messageModel.setStatus(jo.getString("sm_status"));
                                            messageModel.setUser_image(jo.getString("user_image"));
                                            messageModel.setSimulation_name(jo.getString("sm_name"));
                                            messageModel.setAlign(jo.getString("aling"));
                                            messageModel.setIsfinished(jo.getString("is_finished"));
                                            messageModel.setScores(jo.getString("scores"));
                                            messageModel.setCompetency_name(jo.getString("competency_name"));
                                            messageModel.setCompetency_id(jo.getString("competency_id"));
                                            messageModel.setSimulation_id(jo.getString("sm_id"));
                                            messageModel.setMessageis(jo.getString("message"));
                                            messageModel.setTotalscore(jo.getString("total_score"));
                                            messageModel.setBg_wallpaper(jo.getString("sm_bg_image"));
                                            messageModel.setTy_text(jo.getString("sm_thankyou_text"));
                                            messageModel.setTy_desc(jo.getString("sm_thankyou_description"));
                                            messageModel.setTy_percentage(jo.getString("sm_percentage"));
                                            messageModel.setTy_img(jo.getString("sm_thankyou_background_image"));

                                            Log.e("data is", i + jo.getString("username"));
                                            if(!jo.getString("decision_text").equalsIgnoreCase("")/*&&
                                                    jo.getString("conversation_image_name").equalsIgnoreCase("")
                                                    &&jo.getString("conversation_audio").equalsIgnoreCase("")&&
                                                    jo.getString("conversation_video").equalsIgnoreCase("")*/){

                                                //if values are decisions
                                                Log.e("mmmm", "mmm");

                                                timer1.cancel();
                                                timer.cancel();
                                                putAnswers();
                                            }else {
                                                //if values are convesation
                                                arrayList.add(messageModel);
                                                try {
                                                    expertCardAdapter.notifyDataSetChanged();
                                                }catch (Exception e){

                                                }
                                                rc.smoothScrollToPosition(rc.getAdapter().getItemCount() );
                                                loading_layout.setVisibility(View.INVISIBLE);

                                                i++;
                                                if(jo.getString("conversation_image_name").length()>4){
                                                    arrayList_Media_in_everyload.add(jo.getString("conversation_image_name"));
                                                }
                                                if(jo.getString("conversation_audio").length()>4){
                                                    arrayList_Media_in_everyload.add(jo.getString("conversation_audio"));
                                                }
                                                if(jo.getString("conversation_video").length()>4){
                                                    arrayList_Media_in_everyload.add(jo.getString("conversation_video"));

                                                }
                                                if(i%3==0){
                                                    timer1.cancel();
                                                    timer.cancel();
                                                    touch_to_procced.setEnabled(true);
                                                    loading_layout.setVisibility(View.GONE);
                                                    touch_to_procced.setVisibility(View.VISIBLE);
                                                }
                                            }

                                            back.setEnabled(true);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Log.e("data is", "exception");
                                        }
                                    }else {
                                        timer1.cancel();
                                        timer.cancel();
                                        Log.e("data is", "exdiscardception");

                                    }

                                }
                            });
                        }
                    };
//rc.smoothScrollToPosition(rc.getAdapter().getItemCount() );
                    timer = new Timer();
                    // timer.schedule(timertask,0,5000);

                    timer.schedule(timertask,0,3000);
                    Log.e("timer","changed1");

                }
            }.start();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Result","Exc");

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.e("status","pause");
        //expertCardAdapter.notifyDataSetChanged();
        ispaused=true;

        try {
            mp.pause();
        }catch (Exception e){

        }
        try {
            videofullscreen.pause();
        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       // mp.start();
        resumeflag_is=true;
        ispaused=false;
            try {
                mp.start();
                rc.smoothScrollToPosition(rc.getAdapter().getItemCount() );

            }catch (Exception e){


        }
        lr.setVisibility(View.VISIBLE);
        fr_video.setVisibility(View.GONE);
        fr_img.setVisibility(View.GONE);
        expertCardAdapter.isplaying=false;
        what_is_playing="image";
         expertCardAdapter.notifyDataSetChanged();


    }

    @Override
    protected void onStop()
    {
        super.onStop();
        Log.e("status","stop");
        ispaused=true;
        mp.pause();
    }

    public int getPixel(SimulationActivity simulationActivity,int dps){
        Resources r = simulationActivity.getResources();

        int  px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }
    //download media
    public static void downloadData(String uri, String substring, String type, int position) {

        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {

            dm.startDownload(task);
            DownloadEntryModel downloadEntryModel=new DownloadEntryModel();
            downloadEntryModel.setTask_id(task);
            downloadEntryModel.setPosition(position);
            download_content_entry.add(downloadEntryModel);
            FiledownloadModel filedownloadModel=new FiledownloadModel();
            filedownloadModel.setId(task);
            filedownloadModel.setType(type);
            filedownloadModel.setName(substring);
            file_list.add(filedownloadModel);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void downloadBgwallpaper(String uri, String substring) {

        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {
            dm.startDownload(task);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public static class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }
    public static boolean isConnectionAvailable() {

        ConnectivityManager mConnectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (mConnectivity != null) {

            NetworkInfo info = mConnectivity.getActiveNetworkInfo();

            if (info != null && info.getState()== NetworkInfo.State.CONNECTED) {

                return true;

            }

        }

        return false;

    }

    public static class GetBackground extends AsyncTask<String,Void,Bitmap>{
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // qs_progress.show();
        }

        @Override
        protected void onPostExecute(Bitmap bm) {
            super.onPostExecute(bm);
            Drawable d = new BitmapDrawable(context.getResources(), bm);
            lr.setBackground(d);
            mp.start();
            try {
               // qs_progress.dismiss();
             //   alertDialog.dismiss();
            }catch (Exception e){


            }
        }

    }

    //fullscreen video
    public static void displayVideo(String path){
        if(!resumeflag_is) {
            back.setEnabled(false);
            back.setClickable(false);
            Log.e("start", "videomethod");

            mp.pause();
            expertCardAdapter.notifyDataSetChanged();
            // lr.setVisibility(View.GONE);
            fr_video.setVisibility(View.VISIBLE);
            fr_img.setVisibility(View.GONE);
            try {

                mediaController11 = new MediaController(context);
                mediaController11.setAnchorView(videofullscreen);
                // Get the URL from String VideoURL
                videofullscreen.setMediaController(mediaController11);

                Uri videouri = Uri.parse(path);

                videofullscreen.setVideoURI(videouri);

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            videofullscreen.requestFocus();
            videofullscreen.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mpis) {
                    if(!resumeflag_is) {

                        mp.pause();
                        Log.e("start", "video");
                        videofullscreen.start();
                        mediaController11.show(0);
                    }
                }
            });
            videofullscreen.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mpis) {
                    // notifyDataSetChanged();

                }
            });
        }
    }

    //fullscreen image
    public static void displayImage(String path)
    {
        back.setEnabled(false);
        back.setClickable(false);
        expertCardAdapter.notifyDataSetChanged();
        //lr.setVisibility(View.GONE);
        fr_video.setVisibility(View.GONE);
        fr_img.setVisibility(View.VISIBLE);
        if(path.contains("gif")){
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imgfullscreen);
            Glide.with(context).load(path).into(imageViewTarget);
        }else {
            Bitmap bmp = BitmapFactory.decodeFile(path);
            imgfullscreen.setImageBitmap(bmp);
            PhotoViewAttacher pAttacher;
            pAttacher = new PhotoViewAttacher(imgfullscreen);
            pAttacher.update();
        }


    }

    public void exitDialogue(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage("Do you want to quit simulation?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        try {
                            timer1.cancel();
                            timer.cancel();
                           // timer.purge();
                            timertask1.cancel();
                            timertask.cancel();
                          //  expertCardAdapter=null;

                            // expertCardAdapter.notifyDataSetChanged();
                          //  i = 0;
                        }catch (Exception e){

                        }
                        arrayList.clear();
                        ExpertCardAdapter.click_download_index.clear();
                        SimulationActivity.timer=null;
                        SimulationActivity.timertask=null;
                        if(from_where.equalsIgnoreCase("learn")){
                            ConnectionUtils connectionUtils=new ConnectionUtils(SimulationActivity.this);
                            if(connectionUtils.isConnectionAvailable()){
                               // sendProgress();
                            }
                            new PrefManager(SimulationActivity.this).isclicked("yes");

                            new PrefManager(SimulationActivity.this).isResultscreenpause("no");

                            if(new PrefManager(SimulationActivity.this).getstatusforFeedback().equalsIgnoreCase("completed")){
                                Intent i = new Intent(SimulationActivity.this, ModulesActivity.class );
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                            }else {


                                if(new PrefManager(SimulationActivity.this).getcompulsaryfeedback().equalsIgnoreCase("yes")){
                                    Intent i = new Intent(SimulationActivity.this,ModulesActivity.class);
              /*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*/

                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                    i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    finish();
                                    startActivity(i);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                }else {
                                    Intent i = new Intent(SimulationActivity.this, ModulesActivity.class );
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                    i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    finish();
                                    startActivity(i);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                }



                            }


                        }else {
                            new PrefManager(SimulationActivity.this).isclicked("yes");

                            new PrefManager(SimulationActivity.this).isResultscreenpause("no");
/*
                            if(new PrefManager(SimulationActivity.this).getcompulsaryfeedback().equalsIgnoreCase("yes")){

                                Intent i = new Intent(SimulationActivity.this,FeedbackActivity.class);
              *//*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*//*

                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                            }else {
                                Intent i = new Intent(SimulationActivity.this, ModulesActivity.class );
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                            }*/

                            Intent i = new Intent(SimulationActivity.this, HomeActivity.class );
                            Log.e("navigation","ass");
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                            i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("tabs","normal");
                            finish();
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);



                        }

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.setCancelable(false);
        alert11.show();
    }
    public void internetDialogue(){
        loading_layout.setVisibility(View.GONE);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Alert");
        builder1.setMessage("Check your internet !");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Retry",
                new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, int id) {
                        dialog.cancel();
                        if(isConnectionAvailable()){
                            loading_layout.setVisibility(View.VISIBLE);
                            back.setEnabled(false);
                            loadFirst();
                        }else {
                            loading_layout.setVisibility(View.VISIBLE);
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    loading_layout.setVisibility(View.GONE);
                                    back.setEnabled(true);
                                    internetDialogue();
                                }
                            }.start();
                        }

                            }
                });


         alert11 = builder1.create();
        alert11.setCancelable(false);
        try {
            alert11.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (alert11.isShowing()) {
                alert11.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        SendData sendData=new SendData();
        sendData.cancel(true);

        expertCardAdapter=null;

    }
    public static class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public static void displayTouchButton(){
        touch_to_procced.setEnabled(true);
        loading_layout.setVisibility(View.GONE);
        touch_to_procced.setVisibility(View.VISIBLE);
        touch_to_procced.setText("  Finish  ");
    }



    private static class SendLessonStartProgress extends AsyncTask<Integer, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        ///Authorization
        @Override
        protected String doInBackground(Integer... urlkk) {
            String result1 = "";
            try {

                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", candidateId)
                        .object("course_id", Integer.valueOf(new PrefManager(context).getCourseid()))
                        .object("lesson_id", Integer.valueOf(new PrefManager(context).getinnermoduleid()))
                        .object("assessment_id", 0)
                        .object("order_id", Integer.valueOf(new PrefManager(context).getorderid()))
                        .object("status", "completed")
                        .object("lg_id",new PrefManager(context).getlgid())
                        .object("certification_id",new PrefManager(context).getcertiid())



                        .build().toString();

                try {
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_update_user_progress");
                    urlConnection2 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection2.setDoOutput(true);
                    urlConnection2.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection2.setRequestProperty("Content-Type", "application/json");
                    urlConnection2.setRequestProperty("Authorization", "Bearer " + tok);

                    urlConnection2.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection2.getOutputStream());
                    wr.write(jn);
                    Log.e("resp_data",jn);
                    wr.flush();
                    is = urlConnection2.getInputStream();
                    code2 = urlConnection2.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                } catch (ClientProtocolException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection2 != null)
                    urlConnection2.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {
                Log.e("myvalue","postans 200");
            }
            else{
                Log.e("myvalue","postans not 200");

            }
        }
    }
    private static String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

    public static void sendProgress(){
        if(from_where.equalsIgnoreCase("learn")) {
            new SendLessonStartProgress().execute();
        }

    }
}







