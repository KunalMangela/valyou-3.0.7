package com.globalgyan.getvalyou.simulation;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.globalgyan.getvalyou.R;

import uk.co.senab.photoview.PhotoViewAttacher;

public class SimulationShowItemsActivity extends AppCompatActivity {
    ImageView im,screen_rotate;
    VideoView video_view,audio_view;
    String type,path,name;
    MediaController mediacontroller;
    RelativeLayout linear_audio;
    TextView heading;
    static float h,h_landscape;
    MediaController mediaController1;
    Boolean flag_isrotate=false,first_launch=false;
    RelativeLayout rel_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation_show_items);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#5250B0"));
        }
        im=(ImageView)findViewById(R.id.imgview);
        screen_rotate=(ImageView)findViewById(R.id.orientation_change);
        video_view=(VideoView)findViewById(R.id.videoview);
        audio_view=(VideoView)findViewById(R.id.audioview);
        rel_main=(RelativeLayout)findViewById(R.id.main_relative);
        linear_audio=(RelativeLayout)findViewById(R.id.linear_audio);
        heading=(TextView)findViewById(R.id.heading_file);
        type=getIntent().getStringExtra("type");
        path=getIntent().getStringExtra("path");
        name=getIntent().getStringExtra("name");
        Typeface tf1 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        heading.setTypeface(tf1);


        mediacontroller = new MediaController(
                this);
        if(type.equalsIgnoreCase("image")){
            video_view.setVisibility(View.GONE);
            linear_audio.setVisibility(View.GONE);
            im.setVisibility(View.VISIBLE);

            heading.setText(name);

            if(path.contains("gif")||(path.contains("GIF"))||(path.contains("Gif"))){
                GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(im);
                Glide.with(this).load(path).into(imageViewTarget);

            }else {
                im.setImageURI(Uri.parse(path));
                PhotoViewAttacher pAttacher;
                pAttacher = new PhotoViewAttacher(im);
                pAttacher.update();
            }

        }else if(type.equalsIgnoreCase("audio")) {
            video_view.setVisibility(View.GONE);
            im.setVisibility(View.GONE);
            linear_audio.setVisibility(View.VISIBLE);
            loadvideoview(audio_view);
            heading.setText(name);

        }  else {
            screen_rotate.setVisibility(View.VISIBLE);
            linear_audio.setVisibility(View.GONE);
            im.setVisibility(View.GONE);
            video_view.setVisibility(View.VISIBLE);
            loadvideoview(video_view);
            heading.setText(name);

        }

        screen_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag_isrotate){
                    screen_rotate.setImageResource(R.drawable.landscape_screen);
                    flag_isrotate=false;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


                }else {


                    screen_rotate.setImageResource(R.drawable.portrait_screen);
                    flag_isrotate=true;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(first_launch) {
            int i = getScreenOrientation();
            if(i==0){

                screen_rotate.setImageResource(R.drawable.portrait_screen);
                flag_isrotate=true;
            }
            if(i==1){

                screen_rotate.setImageResource(R.drawable.landscape_screen);
                flag_isrotate=false;

            }
        }
        first_launch=true;
    }

    private int getScreenOrientation() {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
                (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height) {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:

                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                default:

                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return orientation;
    }
    public void loadvideoview(final VideoView vw){
        try{

            mediaController1=new MediaController(this);
            mediaController1.setAnchorView(vw);
            // Get the URL from String VideoURL
            vw.setMediaController(mediaController1);
            mediaController1.setVisibility(View.VISIBLE);

            final Uri videouri = Uri.parse(path);

            new CountDownTimer(200, 200) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    vw.setVideoURI(videouri);
                }
            }.start();

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        vw.requestFocus();
        vw.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {

                new CountDownTimer(200, 200) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        vw.start();
                        mediaController1.show(0);
                    }
                }.start();


            }
        });
        vw.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // notifyDataSetChanged();

            }
        });

    }
    public void backarrowclick(View view){
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private void styleMediaController(View view) {
        if (view instanceof MediaController) {
            MediaController v = (MediaController) view;
            for(int i = 0; i < v.getChildCount(); i++) {
                styleMediaController(v.getChildAt(i));
            }
        } else
        if (view instanceof LinearLayout) {
            LinearLayout ll = (LinearLayout) view;
            for(int i = 0; i < ll.getChildCount(); i++) {
                styleMediaController(ll.getChildAt(i));
            }
        } else if (view instanceof SeekBar) {
            ((SeekBar) view).setProgressDrawable(this.getResources().getDrawable(R.drawable.blueleftt));
            ((SeekBar) view).setThumb(this.getResources().getDrawable(R.drawable.bluerightt));
        }
    }
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
}
