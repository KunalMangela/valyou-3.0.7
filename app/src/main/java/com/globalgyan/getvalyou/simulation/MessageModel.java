package com.globalgyan.getvalyou.simulation;

/**
 * Created by Globalgyan on 02-02-2018.
 */

public class MessageModel {
    String sm_data_id,u_id,conversation_order,parent_conv_id,expression,username,conversation_text,decision,scores,
    image,audio,video,status,simulation_name,
    ty_text,ty_img,ty_desc,ty_percentage,
    align,user_image,isfinished,myscore,competency_name,competency_id,simulation_id,messageis,totalscore,bg_wallpaper;

    public MessageModel() {
    }

    public String getTy_text() {
        return ty_text;
    }

    public void setTy_text(String ty_text) {
        this.ty_text = ty_text;
    }

    public String getTy_img() {
        return ty_img;
    }

    public void setTy_img(String ty_img) {
        this.ty_img = ty_img;
    }

    public String getTy_desc() {
        return ty_desc;
    }

    public void setTy_desc(String ty_desc) {
        this.ty_desc = ty_desc;
    }

    public String getTy_percentage() {
        return ty_percentage;
    }

    public void setTy_percentage(String ty_percentage) {
        this.ty_percentage = ty_percentage;
    }

    public String getCompetency_name() {
        return competency_name;
    }

    public void setCompetency_name(String competency_name) {
        this.competency_name = competency_name;
    }

    public String getBg_wallpaper() {
        return bg_wallpaper;
    }

    public void setBg_wallpaper(String bg_wallpaper) {
        this.bg_wallpaper = bg_wallpaper;
    }

    public String getCompetency_id() {
        return competency_id;
    }

    public void setCompetency_id(String competency_id) {
        this.competency_id = competency_id;
    }

    public String getSimulation_id() {
        return simulation_id;
    }

    public void setSimulation_id(String simulation_id) {
        this.simulation_id = simulation_id;
    }

    public String getMessageis() {
        return messageis;
    }

    public void setMessageis(String messageis) {
        this.messageis = messageis;
    }

    public String getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(String totalscore) {
        this.totalscore = totalscore;
    }

    public String getIsfinished() {
        return isfinished;
    }

    public void setIsfinished(String isfinished) {
        this.isfinished = isfinished;
    }

    public String getMyscore() {
        return myscore;
    }

    public void setMyscore(String myscore) {
        this.myscore = myscore;
    }

    public String getAlign() {
        return align;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public String getSm_data_id() {
        return sm_data_id;
    }

    public void setSm_data_id(String sm_data_id) {
        this.sm_data_id = sm_data_id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getConversation_order() {
        return conversation_order;
    }

    public void setConversation_order(String conversation_order) {
        this.conversation_order = conversation_order;
    }

    public String getParent_conv_id() {
        return parent_conv_id;
    }

    public void setParent_conv_id(String parent_conv_id) {
        this.parent_conv_id = parent_conv_id;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getConversation_text() {
        return conversation_text;
    }

    public void setConversation_text(String conversation_text) {
        this.conversation_text = conversation_text;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getScores() {
        return scores;
    }

    public void setScores(String scores) {
        this.scores = scores;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSimulation_name() {
        return simulation_name;
    }

    public void setSimulation_name(String simulation_name) {
        this.simulation_name = simulation_name;
    }
}
