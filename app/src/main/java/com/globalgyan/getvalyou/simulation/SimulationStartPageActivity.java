package com.globalgyan.getvalyou.simulation;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.globalgyan.getvalyou.AssessLanding;
import com.globalgyan.getvalyou.FunModel;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.MainPlanetActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.simulation.core.DownloadManagerPro;
import com.globalgyan.getvalyou.simulation.report.listener.DownloadManagerListener;

import java.io.File;
import java.io.IOException;

public class SimulationStartPageActivity extends AppCompatActivity implements DownloadManagerListener {

    int download_count=0,available_count=0;
    TextView name;
    ImageView logo,back_btn_is;
    static DownloadManagerPro dm;
    static File folder;
    String landing_page_img,bg_img,bg_sound,icon,gif_file,img_bg_name="no",music_bg_name="no",gif_name,decision_bg_color,simulation_bg_color;
    ImageView imageView,img_bg_gif;
    Button play;
    FrameLayout fr;
    static TextView waiting,desc;
    boolean playflag=false;
    long gif_load_time=0;
    String bgtitle,bgdesc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.start);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().setStatusBarColor(Color.parseColor("#ce000000"));
        }

        dm = new DownloadManagerPro(this.getApplicationContext());
        createFolder();
        dm.init(folder.getPath(), 12, SimulationStartPageActivity.this);

        fr=(FrameLayout)findViewById(R.id.frame_main);
        play=(Button)findViewById(R.id.play);
        imageView=(ImageView)findViewById(R.id.img_bg);
        back_btn_is=(ImageView)findViewById(R.id.back_btn_sm);
        img_bg_gif=(ImageView)findViewById(R.id.img_bg_gif);
        waiting=(TextView)findViewById(R.id.waiting_text);
        desc=(TextView)findViewById(R.id.desc);
        landing_page_img=getIntent().getStringExtra("sm_landing_page_image");
        bg_img=getIntent().getStringExtra("sm_bg_image");
        bg_sound=getIntent().getStringExtra("sm_bg_sound");
        icon=getIntent().getStringExtra("sm_icon_image");
        decision_bg_color=getIntent().getStringExtra("sm_decision_bg_color");
        simulation_bg_color=getIntent().getStringExtra("sm_bg_color");
        gif_file=getIntent().getStringExtra("gif_file");
        bgtitle=getIntent().getStringExtra("sm_title_color");
        bgdesc=getIntent().getStringExtra("sm_title_description_color");
        gif_load_time= Long.parseLong(getIntent().getStringExtra("sm_gif_loading_time"));
        gif_load_time=gif_load_time*1000;
        desc.setText(getIntent().getStringExtra("sm_description"));
        desc.setTextColor(Color.parseColor(bgdesc));

        name=(TextView)findViewById(R.id.name);
        name.setTextColor(Color.parseColor(bgtitle));
        logo=(ImageView)findViewById(R.id.logo);
        final Button play=(Button)findViewById(R.id.play);
        final TextView name=(TextView)findViewById(R.id.name);

        back_btn_is.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!playflag) {
                    String ss=new PrefManager(SimulationStartPageActivity.this).getNav();
                    if(ss.equalsIgnoreCase("Ass")){
                        Intent intent = new Intent(SimulationStartPageActivity.this, HomeActivity.class);
                        intent.putExtra("tabs", "normal");

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    }else {
                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                }
            }
        });
        if(landing_page_img.length()>5){
            Glide.with(this).load(landing_page_img)
                    .thumbnail(1f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

        }else {

        }


        if(gif_file.length()>5){
            String [] pic=gif_file.split("/");
            String last=pic[pic.length-1];
            String [] lastname=last.split("\\.");
            String name_is=lastname[0];
            gif_name=last;
            File bg_gif = new File(folder, gif_name);
            if(bg_gif.exists()){

            }else {
                available_count++;
                downloadData(gif_file,name_is,"gif");

            }
        }


        if(bg_img.length()>5){

            String [] pic=bg_img.split("/");
            String last=pic[pic.length-1];
            String [] lastname=last.split("\\.");
            String name_is=lastname[0];
            img_bg_name=last;

            File bg_music = new File(folder, img_bg_name);

            if(bg_music.exists()){

            }else {
                available_count++;
                downloadData(bg_img, name_is, "image");
            }
        }else  if(simulation_bg_color.contains("#")){
                img_bg_name=simulation_bg_color;
        }else {

        }
        if(bg_sound.length()>5){

            String [] pic=bg_sound.split("/");
            String last=pic[pic.length-1];
            String [] lastname=last.split("\\.");
            String name_is=lastname[0];
            music_bg_name=last;
            File bg_music = new File(folder, music_bg_name);
            if(bg_music.exists()){

            }else {

                available_count++;
                downloadData(bg_sound,name_is,"audio");

            }
        }
        if(icon.length()>5){
            Glide.with(this).load(icon)
                    .thumbnail(0.5f)
                    .placeholder(R.drawable.char_img)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(logo);

        }else {

        }

        if(download_count==available_count){
            waiting.setVisibility(View.GONE);
        }
        Typeface tf1 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        desc.setTypeface(tf1);
        waiting.setTypeface(tf1);
        play.setTypeface(tf1);
        name.setTypeface(tf1);
        name.setTypeface(tf1,Typeface.BOLD);
        name.setText(getIntent().getStringExtra("sm_name"));

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play.setEnabled(false);
                playflag=true;
                        if(available_count==download_count) {
                            File bg_gif = new File(folder, gif_name);
                            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(img_bg_gif);
                            Glide.with(SimulationStartPageActivity.this).load(bg_gif.getPath()).into(imageViewTarget);
                            img_bg_gif.setVisibility(View.VISIBLE);
                            name.setVisibility(View.GONE);
                            desc.setVisibility(View.GONE);
                            play.setVisibility(View.GONE);
                            logo.setVisibility(View.GONE);


                            new CountDownTimer(gif_load_time, gif_load_time) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    Intent intent = new Intent(SimulationStartPageActivity.this, SimulationActivity.class);
                                    intent.putExtra("bg_imageis",img_bg_name);
                                    intent.putExtra("bg_soundis",music_bg_name);
                                    intent.putExtra("dec_bg",decision_bg_color);
                                    intent.putExtra("sm_name_is",getIntent().getStringExtra("sm_name"));
                                    intent.putExtra("sm_id",getIntent().getStringExtra("sm_id"));
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    finish();

                                }
                            }.start();

                         }else {
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {

                                    play.setEnabled(true);
                                    playflag=false;

                                }
                            }.start();
                            Toast.makeText(SimulationStartPageActivity.this,"Please wait simulation is loading",Toast.LENGTH_SHORT).show();
                        }

            }
        });
    }

    public static void downloadData(String uri, String substring, String type) {
        waiting.setVisibility(View.VISIBLE);
        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {
            dm.startDownload(task);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void createFolder() {
        String main_nameis=getIntent().getStringExtra("sm_name");
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "SimulationData"+main_nameis);
        boolean success = true;
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }
/*
    public void next(View view){
        Intent intent=new Intent(SimulationStartPageActivity.this,SimulationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
*/

    @Override
    public void onBackPressed() {
        if(!playflag) {
            String ss=new PrefManager(SimulationStartPageActivity.this).getNav();
            if(ss.equalsIgnoreCase("Ass")){
                Intent intent = new Intent(SimulationStartPageActivity.this, HomeActivity.class);
                intent.putExtra("tabs", "normal");

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            }else {
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        }
    }

    @Override
    public void OnDownloadStarted(long taskId) {

    }

    @Override
    public void OnDownloadPaused(long taskId) {

    }

    @Override
    public void onDownloadProcess(long taskId, double percent, long downloadedLength) {

    }

    @Override
    public void OnDownloadFinished(long taskId) {

        download_count++;
        if(download_count==available_count){
           // play.setEnabled(true);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    play.setEnabled(true);
                    waiting.setVisibility(View.GONE);
                }
            }); }
    }

    @Override
    public void OnDownloadRebuildStart(long taskId) {

    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {

    }

    @Override
    public void OnDownloadCompleted(long taskId) {

    }

    @Override
    public void connectionLost(long taskId) {

    }

}
