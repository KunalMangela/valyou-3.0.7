package com.globalgyan.getvalyou.simulation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dragankrstic.autotypetextview.AutoTypeTextView;
import com.github.ybq.android.spinkit.SpinKitView;
import com.globalgyan.getvalyou.CourseLIstActivity;
import com.globalgyan.getvalyou.FeedbackActivity;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.ModulesActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.WebviewVideoActivity;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;

/**
 * Created by Globalgyan on 09-02-2018.
 */

public class ExpertCardAdapter extends RecyclerView.Adapter<ExpertCardAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<MessageModel> expertCardModelList;
    String flagside="left";
    public boolean mylinkstatus=false;
    ArrayList<String> click_pos=new ArrayList<>();
    ArrayList<Integer> click_position_used=new ArrayList<>();
    static ArrayList<Integer> click_download_index=new ArrayList<>();
    boolean isplaying=false;
    public SpinKitView cr_img;
    boolean flag_firstclick=false,same_pos_first=false;
    int click_latest_pos=0;
    private int lastPosition = -1;
    File image_file,video_file,audio_file;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView btn,small_btn,finsh_btn,score_label,score_text,ty_text,completed_label;
        AutoTypeTextView ques;
        ImageView left_img,right_img,img,video_btn,audio_btn,download_btn,fullscreen_btn,fullscreen_icon_img,img_icon,video,gif_icon;
        VideoView audio_view;
        RelativeLayout ques_relative,gif_layout,score_layout;
        FrameLayout btn_frame,video_frame,audio_frame,img_frame;
        LinearLayout ques_linear;
        ImageView score_layout_image;
        ImageView image_cov;


        ImageView background_color,subjectimage;

        public MyViewHolder(View view) {
            super(view);
            ques=(AutoTypeTextView) view.findViewById(R.id.ques_text);
            image_cov=(ImageView)view.findViewById(R.id.image_conv);
            video_btn=(ImageView)view.findViewById(R.id.playbtn_video);
            img_icon=(ImageView)view.findViewById(R.id.img_icon);
            ty_text=(TextView)view.findViewById(R.id
                    .ty_text);
            fullscreen_btn=(ImageView)view.findViewById(R.id.fullscreen_icon);
            audio_btn=(ImageView)view.findViewById(R.id.playbtn_audio);
            download_btn=(ImageView)view.findViewById(R.id.download_icon);
            btn=(TextView)view.findViewById(R.id.btn_dec);
            score_layout_image=(ImageView) view.findViewById(R.id.sc);
            video=(ImageView) view.findViewById(R.id.video_conv);
            gif_icon=(ImageView) view.findViewById(R.id.gif_icon);
            audio_view=(VideoView) view.findViewById(R.id.audio_conv);
            small_btn=(TextView)view.findViewById(R.id.btn_click);
            score_label=(TextView)view.findViewById(R.id.scorelabel);
            completed_label=(TextView)view.findViewById(R.id.completed);
            score_text=(TextView)view.findViewById(R.id.score_text);
            finsh_btn=(TextView)view.findViewById(R.id.finishicon);
            left_img=(ImageView)view.findViewById(R.id.img_left);
            right_img=(ImageView)view.findViewById(R.id.img_right);
            fullscreen_icon_img=(ImageView)view.findViewById(R.id.fullscreen_icon_img);
            ques_relative=(RelativeLayout)view.findViewById(R.id.ques_rel);
            score_layout=(RelativeLayout)view.findViewById(R.id.scorelayout);
           // gif_layout=(RelativeLayout)view.findViewById(R.id.rel_gif);
            ques_linear=(LinearLayout) view.findViewById(R.id.ques_linear);
            btn_frame=(FrameLayout) view.findViewById(R.id.frame_btn);
            img_frame=(FrameLayout) view.findViewById(R.id.frame_img_is);
            video_frame=(FrameLayout) view.findViewById(R.id.video_frame);
            audio_frame=(FrameLayout) view.findViewById(R.id.audio_frame);
            cr_img=(SpinKitView) view.findViewById(R.id.spin_kit);
        }
    }


    public ExpertCardAdapter(Context mContext, ArrayList<MessageModel> expertCardModelList) {
        this.mContext = mContext;
        this.expertCardModelList = expertCardModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item, parent, false);


        Log.e("createview", "yes");

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {
        final MessageModel messageModel = expertCardModelList.get(position);
     //   Log.e("misis",String.valueOf(position));
        viewHolder.setIsRecyclable(false);
        Typeface tf1 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        viewHolder.ques.setVisibility(View.VISIBLE);
        viewHolder.ques.setTypeface(tf1);
        viewHolder.score_label.setTypeface(tf1);
        viewHolder.completed_label.setTypeface(tf1);

        viewHolder.btn.setTypeface(tf1,Typeface.BOLD);
        viewHolder.small_btn.setTypeface(tf1);
        viewHolder.finsh_btn.setTypeface(tf1);
        viewHolder.ty_text.setTypeface(tf1);
        viewHolder.score_text.setTypeface(tf1);

        viewHolder.finsh_btn.setShadowLayer(1, 0, 0, Color.BLACK);

        Log.e("abcd","adapter reload");


        //when convesation text is available
        if (!messageModel.getConversation_text().equalsIgnoreCase("")) {
            viewHolder.download_btn.setVisibility(View.GONE);
            viewHolder.audio_frame.setVisibility(View.GONE);
            viewHolder.video_frame.setVisibility(View.GONE);
            viewHolder.img_frame.setVisibility(View.GONE);
            viewHolder.score_layout.setVisibility(View.GONE);
            viewHolder.btn_frame.setVisibility(View.GONE);
            viewHolder.ques.setText(messageModel.getConversation_text());
            //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
            //viewHolder.ques_relative.setVisibility(View.VISIBLE);
            if (messageModel.getAlign().equalsIgnoreCase("0")) {
                viewHolder.ques.setBackgroundResource(R.drawable.whiteleft);
                viewHolder.left_img.setVisibility(View.VISIBLE);
                viewHolder.right_img.setVisibility(View.INVISIBLE);
                viewHolder.ques_relative.setGravity(Gravity.LEFT);
                viewHolder.left_img.setImageResource(R.drawable.userone_dot);
            } else {
                viewHolder.ques.setBackgroundResource(R.drawable.lightbluerightt);
                viewHolder.left_img.setVisibility(View.INVISIBLE);
                viewHolder.right_img.setVisibility(View.VISIBLE);
                viewHolder.ques_relative.setGravity(Gravity.RIGHT);
                viewHolder.right_img.setImageResource(R.drawable.usertwo_dot);

            }
            if(messageModel.user_image.equalsIgnoreCase("")){
                viewHolder.ques.setBackgroundResource(R.drawable.whiterey);
                viewHolder.left_img.setVisibility(View.INVISIBLE);
                viewHolder.right_img.setVisibility(View.INVISIBLE);
                viewHolder.ques_relative.setGravity(Gravity.CENTER);
            }
        } else
            //when decision text is available
            if (!messageModel.getDecision().equalsIgnoreCase("")) {

                viewHolder.download_btn.setVisibility(View.GONE);
                viewHolder.audio_frame.setVisibility(View.GONE);
                viewHolder.video_frame.setVisibility(View.GONE);
                viewHolder.img_frame.setVisibility(View.GONE);
                viewHolder.score_layout.setVisibility(View.GONE);
                viewHolder.btn_frame.setVisibility(View.VISIBLE);
                viewHolder.btn.setText(messageModel.getDecision() + "                                                                                                              ");
                viewHolder.ques.setVisibility(View.GONE);
                viewHolder.left_img.setVisibility(View.INVISIBLE);
                viewHolder.right_img.setVisibility(View.INVISIBLE);
                viewHolder.ques_relative.setGravity(Gravity.LEFT);
                GradientDrawable shape =  new GradientDrawable();
                shape.setCornerRadius( 15 );
                if(SimulationActivity.dec_bg_color.contains("#")){
                    shape.setColor(Color.parseColor(SimulationActivity.dec_bg_color));
                }
                viewHolder.btn.setBackground(shape);
                if (flag_firstclick) {
                    if (position <= click_latest_pos) {
                        viewHolder.btn.setAlpha(0.5F);
                       /* viewHolder.small_btn.setBackgroundResource(R.drawable.disable_btn_small_text_corner);
                        viewHolder.small_btn.setAlpha(0.5F);
                        viewHolder.small_btn.setVisibility(View.INVISIBLE);*/
                    } else {
                        viewHolder.btn.setAlpha(1F);
                       /* viewHolder.small_btn.setBackgroundResource(R.drawable.confirm_btn_bg);
                        viewHolder.small_btn.setAlpha(1F);*/
                    }
                    for (int i = 0; i < click_pos.size(); i++) {
                        if (messageModel.getSm_data_id().equalsIgnoreCase(click_pos.get(i))) {
                            //previously clicked buttons
                            if(SimulationActivity.dec_bg_color.contains("#")){
                                shape.setColor(Color.parseColor(SimulationActivity.dec_bg_color));
                            }
                         /*   viewHolder.small_btn.setBackgroundResource(R.drawable.confirm_btn_bg);
                            viewHolder.small_btn.setVisibility(View.VISIBLE);
                            viewHolder.small_btn.setText("    Choose    ");
                         */
                         Log.e("cond", "true");
                            viewHolder.btn.setAlpha(1F);
                           /* if (position <= click_latest_pos) {
                                viewHolder.small_btn.setAlpha(1F);
                            } else {
                                viewHolder.small_btn.setAlpha(1F);
                            }*/
                            break;
                        } else {
                            //never get clicked
                            if(SimulationActivity.dec_bg_color.contains("#")){
                                shape.setColor(Color.parseColor(SimulationActivity.dec_bg_color));
                            }
                          /*  if (position <= click_latest_pos) {
                                //clicked
                                viewHolder.small_btn.setBackgroundResource(R.drawable.disable_btn_small_text_corner);
                                viewHolder.small_btn.setAlpha(0.5F);
                            } else {
                                viewHolder.small_btn.setBackgroundResource(R.drawable.confirm_btn_bg);
                                viewHolder.small_btn.setAlpha(1F);
                            }*/
                        }
                    }



                }
                if((messageModel.getDecision().equalsIgnoreCase("click to continue"))||(messageModel.getDecision().equalsIgnoreCase("touch to proceed"))||(messageModel.getDecision().equalsIgnoreCase("click to proceed"))){
                    viewHolder.download_btn.setVisibility(View.GONE);
                    if(position<click_latest_pos){
                        viewHolder.btn.setAlpha(0.5f);
                    }else {
                        viewHolder.btn.setAlpha(1f);
                    }
                    viewHolder.btn.setPadding(180,23,0,23);
                 //   viewHolder.btn.setText(Gravity.CENTER);
                    viewHolder.btn.setBackgroundResource(R.drawable.round_cancel_btn);

                    //viewHolder.btn.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL);
                  //  viewHolder.small_btn.setVisibility(View.GONE);
                }

                //when Simulation is get over
            }else if(messageModel.getImage().length()>3) {
                try {
                    for (int i = 0; i < click_download_index.size(); i++) {
                        if(position==click_download_index.get(i)){
                            viewHolder.download_btn.setVisibility(View.GONE);
                            /*if(messageModel.getImage().contains(".gif")){
                              viewHolder.gif_icon.setVisibility(View.VISIBLE);
                            }else {
                                viewHolder.gif_icon.setVisibility(View.GONE);
                            }*/
                        }else {
                            viewHolder.download_btn.setVisibility(View.VISIBLE);
                        }

                    }
                }catch (Exception e){
                    viewHolder.download_btn.setVisibility(View.VISIBLE);

                }


                if(messageModel.user_image.equalsIgnoreCase("")){
                    //viewHolder.img_frame.setBackgroundResource(R.drawable.white_patch);
                    viewHolder.left_img.setVisibility(View.INVISIBLE);
                    viewHolder.right_img.setVisibility(View.INVISIBLE);
                    viewHolder.ques_relative.setGravity(Gravity.CENTER);
                }
                else {
                    if (messageModel.getAlign().equalsIgnoreCase("0")) {
                        viewHolder.img_frame.setBackgroundResource(R.drawable.left_whiteg);
                        viewHolder.left_img.setVisibility(View.VISIBLE);
                        viewHolder.right_img.setVisibility(View.INVISIBLE);
                        viewHolder.ques_relative.setGravity(Gravity.LEFT);
                    } else {
                        viewHolder.img_frame.setBackgroundResource(R.drawable.right_blueg);
                        viewHolder.left_img.setVisibility(View.INVISIBLE);
                        viewHolder.right_img.setVisibility(View.VISIBLE);
                        viewHolder.ques_relative.setGravity(Gravity.RIGHT);
                    }
                }


                             boolean flag=false;
                for(int i = 0; i< SimulationActivity.arrayList_imgid.size(); i++){
                    if(messageModel.getSm_data_id().equalsIgnoreCase(SimulationActivity.arrayList_imgid.get(i))){
                        Log.e("statusis","forloop flg true");

                        flag=true;
                        break;

                    }
                }


                viewHolder.audio_frame.setVisibility(View.GONE);
                viewHolder.video_frame.setVisibility(View.GONE);
                viewHolder.img_frame.setVisibility(View.VISIBLE);
                viewHolder.score_layout.setVisibility(View.GONE);
                viewHolder.btn_frame.setVisibility(View.GONE);

                //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
                viewHolder.ques.setVisibility(View.GONE);
              /*  viewHolder.left_img.setVisibility(View.GONE);
                viewHolder.right_img.setVisibility(View.GONE);
              */  viewHolder.ques_relative.setGravity(Gravity.LEFT);
               /**/
                String [] pic=messageModel.getImage().split("/");
                String last=pic[pic.length-1];
                String [] lastname=last.split("\\.");
                String name=lastname[0];

                image_file = new File(SimulationActivity.folder, pic[pic.length-1]);

                if(flag){
                    //scond time adapter load
                    if (image_file.exists()) {
                        Log.e("image", "file exist");
                        //file exist
                        boolean flag_i = false;
                        //check if previously downloaded
                        try {
                            for (int i = 0; i < SimulationActivity.arrayList_imgid_down.size(); i++) {
                                if (SimulationActivity.arrayList_imgid_down.get(i).equalsIgnoreCase(messageModel.getSm_data_id())) {
                                    flag_i = true;
                                    Log.e("image", "flag true for loop");
                                    break;
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (!flag_i){

                            if (SimulationActivity.downloaded_type.equalsIgnoreCase("image")) {
                                if (SimulationActivity.downloaded_file.equalsIgnoreCase(name)) {
                                    //doanload completed
                                    Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());
                                    viewHolder.image_cov.setImageBitmap(bmp);
                                    Log.e("image", "image downloaded");
                                    viewHolder.download_btn.setVisibility(View.GONE);
                                    if(messageModel.getImage().contains(".gif")){
                                        viewHolder.gif_icon.setVisibility(View.VISIBLE);
                                    }else {
                                        viewHolder.gif_icon.setVisibility(View.GONE);
                                    }
                                    cr_img.setVisibility(View.GONE);
                                    viewHolder.fullscreen_icon_img.setVisibility(View.VISIBLE);
                                    SimulationActivity.arrayList_imgid_down.add(messageModel.getSm_data_id());
                                }else {
                                    //not complete
                                    if(messageModel.getImage().contains(".gif")){
                                        viewHolder.gif_icon.setVisibility(View.GONE);
                                    }else {
                                        viewHolder.gif_icon.setVisibility(View.GONE);
                                    }
                                    viewHolder.fullscreen_icon_img.setVisibility(View.GONE);
                                    cr_img.setVisibility(View.VISIBLE);
                                    viewHolder.download_btn.setVisibility(View.GONE);
                                   /* if(messageModel.getImage().contains(".gif")){
                                        viewHolder.gif_icon.setVisibility(View.VISIBLE);
                                    }else {
                                        viewHolder.gif_icon.setVisibility(View.GONE);
                                    }*/
                                }
                            }
                        }else {
                            Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());
                            viewHolder.image_cov.setImageBitmap(bmp);
                            Log.e("image", "image here flag cond");
                            viewHolder.fullscreen_icon_img.setVisibility(View.VISIBLE);
                            cr_img.setVisibility(View.GONE);
                            viewHolder.download_btn.setVisibility(View.GONE);
                            if(messageModel.getImage().contains(".gif")){
                                viewHolder.gif_icon.setVisibility(View.VISIBLE);
                            }else {
                                viewHolder.gif_icon.setVisibility(View.GONE);
                            }
                        }

                    }else {
                        Log.e("image", "image not exist");
                        viewHolder.fullscreen_icon_img.setVisibility(View.GONE);
                        cr_img.setVisibility(View.VISIBLE);
                        viewHolder.download_btn.setVisibility(View.GONE);
                        if(messageModel.getImage().contains(".gif")){
                            viewHolder.gif_icon.setVisibility(View.GONE);
                        }else {
                            viewHolder.gif_icon.setVisibility(View.GONE);
                        }
                      /*  if(messageModel.getImage().contains(".gif")){
                            viewHolder.gif_icon.setVisibility(View.VISIBLE);
                        }else {
                            viewHolder.gif_icon.setVisibility(View.GONE);
                        }*/
                    }
                }else {
                    //first time cell launch
                    Log.e("image", "first time");

                    cr_img.setVisibility(View.VISIBLE);

                    if (image_file.exists()) {

                            Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());
                            viewHolder.image_cov.setImageBitmap(bmp);
                            Log.e("image", "first image here flag cond");
                        viewHolder.fullscreen_icon_img.setVisibility(View.VISIBLE);
                            cr_img.setVisibility(View.GONE);
                        viewHolder.download_btn.setVisibility(View.GONE);
                        //}
                        if(messageModel.getImage().contains(".gif")){
                            viewHolder.gif_icon.setVisibility(View.VISIBLE);
                        }else {
                            viewHolder.gif_icon.setVisibility(View.GONE);
                        }
                    } else {
                        //image not here....download image now
                        viewHolder.fullscreen_icon_img.setVisibility(View.GONE);
                        cr_img.setVisibility(View.VISIBLE);
                        if(messageModel.getImage().contains(".gif")){
                            viewHolder.gif_icon.setVisibility(View.GONE);
                        }else {
                            viewHolder.gif_icon.setVisibility(View.GONE);
                        }
                        Log.e("statusis","img not exist");
                        //cr_img.setVisibility(View.VISIBLE);
                        if(SimulationActivity.isConnectionAvailable()){
                           /* SimulationActivity.downloadData(messageModel.getImage(),name,"image");
                            Glide.with(mContext).load(messageModel.getImage())
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    .placeholder(R.drawable.placeholder_img_is)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(viewHolder.image_cov);
                            SimulationActivity.arrayList_imgid.add(messageModel.getSm_data_id());
                            viewHolder.download_btn.setVisibility(View.GONE);*/
                        }else {
                            //viewHolder.download_btn.setVisibility(View.VISIBLE);
                            //net cond
                            if(messageModel.getImage().contains(".gif")){
                                viewHolder.gif_icon.setVisibility(View.GONE);
                            }else {
                                viewHolder.gif_icon.setVisibility(View.GONE);
                            }
                            cr_img.setVisibility(View.GONE);
                            viewHolder.download_btn.setVisibility(View.VISIBLE);
                        }

                   }


                }

            }
            //when image audio or video available
            else if(!messageModel.getVideo().equalsIgnoreCase("")) {
                try {
                    for (int i = 0; i < click_download_index.size(); i++) {
                        if(position==click_download_index.get(i)){
                            viewHolder.download_btn.setVisibility(View.GONE);
                        }else {
                            viewHolder.download_btn.setVisibility(View.VISIBLE);
                        }

                    }
                }catch (Exception e){
                    viewHolder.download_btn.setVisibility(View.VISIBLE);
                }
                if (messageModel.getAlign().equalsIgnoreCase("0")) {
                    viewHolder.video_frame.setBackgroundResource(R.drawable.left_whiteg);
                    viewHolder.left_img.setVisibility(View.VISIBLE);
                    viewHolder.right_img.setVisibility(View.INVISIBLE);
                    viewHolder.ques_relative.setGravity(Gravity.LEFT);
                } else {
                    viewHolder.video_frame.setBackgroundResource(R.drawable.right_blueg);
                    viewHolder.left_img.setVisibility(View.INVISIBLE);
                    viewHolder.right_img.setVisibility(View.VISIBLE);
                    viewHolder.ques_relative.setGravity(Gravity.RIGHT);
                }
                if(messageModel.user_image.equalsIgnoreCase("")){
                    viewHolder.video_frame.setBackgroundResource(R.drawable.whiterey);
                    viewHolder.left_img.setVisibility(View.INVISIBLE);
                    viewHolder.right_img.setVisibility(View.INVISIBLE);
                    viewHolder.ques_relative.setGravity(Gravity.CENTER);
                }
                viewHolder.audio_frame.setVisibility(View.GONE);
                viewHolder.video_frame.setVisibility(View.VISIBLE);
                viewHolder.img_frame.setVisibility(View.GONE);
                viewHolder.score_layout.setVisibility(View.GONE);
                viewHolder.btn_frame.setVisibility(View.GONE);

                //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
                viewHolder.ques.setVisibility(View.GONE);
               /* viewHolder.left_img.setVisibility(View.GONE);
                viewHolder.right_img.setVisibility(View.GONE);
              */  viewHolder.ques_relative.setGravity(Gravity.LEFT);

                boolean flag_vid=false;
                for(int i = 0; i< SimulationActivity.arrayList_vidid.size(); i++){
                    if(messageModel.getSm_data_id().equalsIgnoreCase(SimulationActivity.arrayList_vidid.get(i))){
                        Log.e("statusis","video forloop flg true");

                        flag_vid=true;
                        break;

                    }
                }


                String [] vid=messageModel.getVideo().split("/");
                String [] lastname=vid[vid.length-1].split("\\.");
                String name=lastname[0];
                video_file = new File(SimulationActivity.folder, vid[vid.length-1]);

                if(flag_vid){
                    //scond time adapter load
                    if (video_file.exists()) {
                        Log.e("video", "file exist");
                        //file exist
                        boolean flag_v = false;
                        //check if previously downloaded
                        try {
                            for (int i = 0; i < SimulationActivity.arrayList_vidid_down.size(); i++) {
                                if (SimulationActivity.arrayList_vidid_down.get(i).equalsIgnoreCase(messageModel.getSm_data_id())) {
                                    flag_v = true;
                                    Log.e("image", "flag true for loop");
                                    break;
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (!flag_v){

                            if (SimulationActivity.downloaded_type.equalsIgnoreCase("video")) {
                                if (SimulationActivity.downloaded_file.equalsIgnoreCase(name)) {
                                    //doanload completed

                                    Log.e("video", "video downloaded");
                                    viewHolder.fullscreen_btn.setVisibility(View.VISIBLE);
                                    cr_img.setVisibility(View.GONE);
                                    viewHolder.video_btn.setVisibility(View.VISIBLE);
                                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(video_file.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                                    viewHolder.video.setImageBitmap(bMap);
                                    SimulationActivity.arrayList_vidid_down.add(messageModel.getSm_data_id());
                                   viewHolder.download_btn.setVisibility(View.GONE);
                                }else {
                                    //not complete
                                    viewHolder.fullscreen_btn.setVisibility(View.GONE);
                                    viewHolder.video_btn.setVisibility(View.GONE);
                                    cr_img.setVisibility(View.VISIBLE);
                                    viewHolder.download_btn.setVisibility(View.GONE);
                                }
                            }
                        }else {
                            Log.e("video", "video here flag cond");
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(video_file.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                            viewHolder.video.setImageBitmap(bMap);
                            viewHolder.video_btn.setVisibility(View.VISIBLE);
                            cr_img.setVisibility(View.GONE);
                            viewHolder.fullscreen_btn.setVisibility(View.VISIBLE);
                            viewHolder.download_btn.setVisibility(View.GONE);
                        }

                    }else {
                        Log.e("video", "video not exist");
                        viewHolder.video_btn.setVisibility(View.GONE);
                        cr_img.setVisibility(View.VISIBLE);
                        viewHolder.fullscreen_btn.setVisibility(View.GONE);
                        viewHolder.download_btn.setVisibility(View.GONE);
                    }
                }else {
                    cr_img.setVisibility(View.VISIBLE);


                    //  File resolveMe = new File( Environment.getExternalStorageDirectory().getAbsolutePath() + "/Simulation/" +"test.json");
                    if (video_file.exists()) {
                        viewHolder.video_btn.setVisibility(View.VISIBLE);
                        cr_img.setVisibility(View.GONE);
                        viewHolder.fullscreen_btn.setVisibility(View.VISIBLE);
                        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(video_file.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                        viewHolder.video.setImageBitmap(bMap);
                        viewHolder.download_btn.setVisibility(View.GONE);
                       // bufferVideo(viewHolder.video,"path",video_file.getPath(),"true");
                    } else {
                        cr_img.setVisibility(View.VISIBLE);
                        viewHolder.video_btn.setVisibility(View.GONE);
                        viewHolder.fullscreen_btn.setVisibility(View.GONE);
                        Log.e("statusis","img not exist");
                      //  bufferVideo(viewHolder.video,messageModel.getVideo(),"path","false");
                        if(SimulationActivity.isConnectionAvailable()) {
                           /* SimulationActivity.downloadData(messageModel.getVideo(),name,"video");
                            SimulationActivity.arrayList_vidid.add(messageModel.getSm_data_id());
                            viewHolder.download_btn.setVisibility(View.GONE);
*/
                        }else {
                                //net cond
                         //   viewHolder.download_btn.setVisibility(View.VISIBLE);
                            viewHolder.video_btn.setVisibility(View.GONE);
                            cr_img.setVisibility(View.GONE);
                            viewHolder.download_btn.setVisibility(View.VISIBLE);
                        }

                    }


                }



            }else if(!messageModel.getAudio().equalsIgnoreCase("")) {
                Log.e("abcd","after download cancel true visible inside");
                if (messageModel.getAlign().equalsIgnoreCase("0")) {
                    viewHolder.audio_frame.setBackgroundResource(R.drawable.left_whiteg);
                    viewHolder.left_img.setVisibility(View.VISIBLE);
                    viewHolder.right_img.setVisibility(View.INVISIBLE);
                    viewHolder.ques_relative.setGravity(Gravity.LEFT);
                } else {
                    viewHolder.audio_frame.setBackgroundResource(R.drawable.right_blueg);
                    viewHolder.left_img.setVisibility(View.INVISIBLE);
                    viewHolder.right_img.setVisibility(View.VISIBLE);
                    viewHolder.ques_relative.setGravity(Gravity.RIGHT);
                }
                if(messageModel.user_image.equalsIgnoreCase("")){
                    viewHolder.audio_frame.setBackgroundResource(R.drawable.whiterey);
                    viewHolder.left_img.setVisibility(View.INVISIBLE);
                    viewHolder.right_img.setVisibility(View.INVISIBLE);
                    viewHolder.ques_relative.setGravity(Gravity.CENTER);
                }
                Log.e("statusis","audio");
                viewHolder.audio_frame.setVisibility(View.VISIBLE);
                viewHolder.img_frame.setVisibility(View.GONE);
                viewHolder.video_frame.setVisibility(View.GONE);
                viewHolder.score_layout.setVisibility(View.GONE);
                viewHolder.btn_frame.setVisibility(View.GONE);

                //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
                viewHolder.ques.setVisibility(View.GONE);
              /*  viewHolder.left_img.setVisibility(View.GONE);
                viewHolder.right_img.setVisibility(View.GONE);
             */   viewHolder.ques_relative.setGravity(Gravity.LEFT);
                boolean flag_audio=false;
                for(int i = 0; i< SimulationActivity.arrayList_Audio.size(); i++){
                    if(messageModel.getSm_data_id().equalsIgnoreCase(SimulationActivity.arrayList_Audio.get(i))){
                        Log.e("statusis","audio forloop flg true");

                        flag_audio=true;
                        break;

                    }
                }
                String [] aud=messageModel.getAudio().split("/");
                String [] lastname=aud[aud.length-1].split("\\.");
                String name=lastname[0];


                audio_file = new File(SimulationActivity.folder, aud[aud.length-1]);

                if(flag_audio){
                    //scond time adapter load
                    if (audio_file.exists()) {
                        Log.e("audio", "file exist");
                        //file exist
                        boolean flag_a = false;
                        //check if previously downloaded
                        try {
                            for (int i = 0; i < SimulationActivity.arrayList_Audio_down.size(); i++) {
                                if (SimulationActivity.arrayList_Audio_down.get(i).equalsIgnoreCase(messageModel.getSm_data_id())) {
                                    flag_a = true;
                                    Log.e("audio", "flag true for loop");
                                    break;
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (!flag_a){

                            if (SimulationActivity.downloaded_type.equalsIgnoreCase("audio")) {
                                if (SimulationActivity.downloaded_file.equalsIgnoreCase(name)) {
                                    //doanload completed

                                    Log.e("audio", "audio downloaded");
                                    viewHolder.audio_btn.setVisibility(View.VISIBLE);
                                    cr_img.setVisibility(View.GONE);
                                    viewHolder.download_btn.setVisibility(View.GONE);
                                    SimulationActivity.arrayList_Audio_down.add(messageModel.getSm_data_id());
                                }else {
                                    //not complete
                                    viewHolder.audio_btn.setVisibility(View.GONE);
                                    cr_img.setVisibility(View.VISIBLE);
                                    viewHolder.download_btn.setVisibility(View.GONE);
                                }
                            }
                        }else {

                            Log.e("audio", "audio here flag cond");
                            viewHolder.audio_btn.setVisibility(View.VISIBLE);
                            cr_img.setVisibility(View.GONE);
                            viewHolder.download_btn.setVisibility(View.GONE);

                        }

                    }else {
                        Log.e("audio", "audio not exist");
                        viewHolder.audio_btn.setVisibility(View.GONE);
                        cr_img.setVisibility(View.VISIBLE);
                        viewHolder.download_btn.setVisibility(View.GONE);
                    }
                }else {
                    cr_img.setVisibility(View.VISIBLE);
                    //  File resolveMe = new File( Environment.getExternalStorageDirectory().getAbsolutePath() + "/Simulation/" +"test.json");
                    if (audio_file.exists()) {
                        viewHolder.audio_btn.setVisibility(View.VISIBLE);
                        cr_img.setVisibility(View.GONE);
                        viewHolder.download_btn.setVisibility(View.GONE);
                        // bufferVideo(viewHolder.video,"path",video_file.getPath(),"true");
                    } else {
                        cr_img.setVisibility(View.VISIBLE);
                        Log.e("statusis","audio not exist");
                        viewHolder.audio_btn.setVisibility(View.GONE);

                        //  bufferVideo(viewHolder.video,messageModel.getVideo(),"path","false");
                        if(SimulationActivity.isConnectionAvailable()) {
/*
                            SimulationActivity.downloadData(messageModel.getAudio(),name,"audio");
                            SimulationActivity.arrayList_Audio.add(messageModel.getSm_data_id());
                            viewHolder.download_btn.setVisibility(View.GONE);
*/
                        }else {
                            //net cond
                     //       viewHolder.download_btn.setVisibility(View.VISIBLE);
                            viewHolder.audio_btn.setVisibility(View.GONE);
                            cr_img.setVisibility(View.GONE);
                            viewHolder.download_btn.setVisibility(View.VISIBLE);
                        }

                    }


                }


            }else if(messageModel.getIsfinished().equalsIgnoreCase("1")){
                viewHolder.download_btn.setVisibility(View.GONE);
                viewHolder.audio_frame.setVisibility(View.GONE);
                viewHolder.video_frame.setVisibility(View.GONE);
                viewHolder.img_frame.setVisibility(View.GONE);
                viewHolder.btn_frame.setVisibility(View.GONE);
                viewHolder.score_layout.setVisibility(View.VISIBLE);
                viewHolder.score_layout.setMinimumHeight(SimulationActivity.height);
                //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
                viewHolder.ques.setVisibility(View.GONE);
                viewHolder.left_img.setVisibility(View.GONE);
                viewHolder.right_img.setVisibility(View.GONE);
                viewHolder.ques_relative.setGravity(Gravity.LEFT);
                viewHolder.score_layout.setVisibility(View.VISIBLE);
                viewHolder.score_text.setText(messageModel.getMessageis());
                // viewHolder.score_label.setText(messageModel.getTotalscore()+"%");
            }else {
                viewHolder.download_btn.setVisibility(View.GONE);
                cr_img.setVisibility(View.GONE);
                viewHolder.audio_frame.setVisibility(View.GONE);
                viewHolder.img_frame.setVisibility(View.GONE);
                viewHolder.video_frame.setVisibility(View.GONE);
                viewHolder.score_layout.setVisibility(View.GONE);
                viewHolder.btn_frame.setVisibility(View.GONE);

                //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
                viewHolder.ques.setVisibility(View.GONE);
                viewHolder.left_img.setVisibility(View.GONE);
                viewHolder.right_img.setVisibility(View.GONE);
                viewHolder.ques_relative.setGravity(Gravity.LEFT);
                }





            //videoclick

            viewHolder.video_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SimulationActivity.resumeflag_is=false;
                    String [] pic=messageModel.getVideo().split("/");
                    String last=pic[pic.length-1];
                    String [] lastname=last.split("\\.");
                    String name=lastname[0];
                    File vid = new File(SimulationActivity.folder,last);

                    SimulationActivity.displayVideo(vid.getPath());
                    // if(!isplaying) {
                        /*viewHolder.video_btn.setVisibility(View.GONE);
                        bufferVideo(viewHolder.video,  video_file.getPath(),viewHolder.video_btn);
                    viewHolder.audio_btn.setVisibility(View.VISIBLE);*/
                  //  }
                }
            });

        viewHolder.audio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // if(!isplaying) {
                    viewHolder.audio_btn.setVisibility(View.GONE);
                    bufferAudio(viewHolder.audio_view,audio_file.getPath(),viewHolder.audio_btn);
                viewHolder.video_btn.setVisibility(View.VISIBLE);
              //  }

            }
        });

        viewHolder.gif_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if(!isplaying) {
                Log.e("img_conv","cliced");
                String [] pic=messageModel.getImage().split("/");
                String last=pic[pic.length-1];
                String [] lastname=last.split("\\.");
                String name=lastname[0];
                File img_is = new File(SimulationActivity.folder,last);

                if(img_is.exists()) {
                    SimulationActivity.displayImage(img_is.getPath());

                }else {
                    //  Toast.makeText(mContext,"Image is not available",Toast.LENGTH_SHORT).show();
                }
                //  }

            }
        });


        viewHolder.download_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!messageModel.getImage().equalsIgnoreCase("")){

                    String [] pic=messageModel.getImage().split("/");
                    String last=pic[pic.length-1];
                    String [] lastname=last.split("\\.");
                    String name=lastname[0];

                    if(SimulationActivity.isConnectionAvailable()){
                        viewHolder.download_btn.setVisibility(View.GONE);
                        cr_img.setVisibility(View.VISIBLE);
                            viewHolder.gif_icon.setVisibility(View.GONE);
                        click_download_index.add(position);
                        SimulationActivity.downloadData(messageModel.getImage(),name,"image",position);
                        Glide.with(mContext).load(messageModel.getImage())
                                .thumbnail(0.5f)
                                .crossFade()
                                .placeholder(R.drawable.placeholder_img_is)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(viewHolder.image_cov);
                        SimulationActivity.arrayList_imgid.add(messageModel.getSm_data_id());
                        notifyDataSetChanged();
                    }else {
                        Toast.makeText(mContext,"Please check your internet connection",Toast.LENGTH_SHORT).show();
                    }
                }
                if(!messageModel.getVideo().equalsIgnoreCase("")){

                    String [] pic=messageModel.getVideo().split("/");
                    String last=pic[pic.length-1];
                    String [] lastname=last.split("\\.");
                    String name=lastname[0];
                    if(SimulationActivity.isConnectionAvailable()) {
                        click_download_index.add(position);
                        viewHolder.download_btn.setVisibility(View.GONE);
                        cr_img.setVisibility(View.VISIBLE);

                        SimulationActivity.downloadData(messageModel.getVideo(),name,"video", position);
                        SimulationActivity.arrayList_vidid.add(messageModel.getSm_data_id());
                        notifyDataSetChanged();
                    }
                    else {
                        Toast.makeText(mContext,"Please check your internet connection",Toast.LENGTH_SHORT).show();
                    }
                }
                if(!messageModel.getAudio().equalsIgnoreCase("")){
                    String [] pic=messageModel.getAudio().split("/");
                    String last=pic[pic.length-1];
                    String [] lastname=last.split("\\.");
                    String name=lastname[0];
                    if(SimulationActivity.isConnectionAvailable()) {
                        click_download_index.add(position);
                        viewHolder.download_btn.setVisibility(View.GONE);
                        cr_img.setVisibility(View.VISIBLE);
                        SimulationActivity.downloadData(messageModel.getAudio(),name,"audio", position);
                        SimulationActivity.arrayList_Audio.add(messageModel.getSm_data_id());
                        notifyDataSetChanged();

                    }else {
                        Toast.makeText(mContext,"Please check your internet connection",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });






        viewHolder.image_cov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("img_conv","cliced");
                String [] pic=messageModel.getImage().split("/");
                String last=pic[pic.length-1];
                String [] lastname=last.split("\\.");
                String name=lastname[0];
                File img_is = new File(SimulationActivity.folder,last);

                if(img_is.exists()) {
                    SimulationActivity.displayImage(img_is.getPath());

                }else {
                  //  Toast.makeText(mContext,"Image is not available",Toast.LENGTH_SHORT).show();
                }


            }
        });
            //choose button click
            viewHolder.small_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                  //  notifyDataSetChanged();
                }
            });


            //click on touch to proceed
            viewHolder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if((messageModel.getDecision().equalsIgnoreCase("touch to proceed"))||(messageModel.getDecision().equalsIgnoreCase("click to proceed"))||(messageModel.getDecision().equalsIgnoreCase("click to continue"))){
                        if (position < click_latest_pos) {

                        } else {

                            if(SimulationActivity.isConnectionAvailable()){

                                SimulationActivity.mp_click.start();
                                //click first time on buttoon
                                click_pos.add(messageModel.getSm_data_id());
                                flag_firstclick = true;
                                click_latest_pos = expertCardModelList.size();

                                String params = "sm_data_id="+ messageModel.getConversation_order()+"&u_id="+ SimulationActivity.candidateId+"&simulation_id="+ SimulationActivity.sm_id_is+"&scores="+messageModel.getScores();
                                SimulationActivity.sendAnswersData(params);

                            }else {
                                Toast.makeText(mContext,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
                                //createAlertDialogue();
                            }

                        }
                    }else {

                        if (flag_firstclick) {

                            //click button not first time
                            if (position <= click_latest_pos) {

                            } else {


                                if(SimulationActivity.isConnectionAvailable()){

                                    SimulationActivity.mp_click.start();
                                    //current click position is greter than previous click
                                    Log.e("btn", "clicked");
                                    click_pos.add(messageModel.getSm_data_id());
                                    flag_firstclick = true;
                                    click_latest_pos = expertCardModelList.size();
                                    String params = "sm_data_id="+ messageModel.getConversation_order()+"&u_id="+ SimulationActivity.candidateId+"&simulation_id="+ SimulationActivity.sm_id_is+"&scores="+messageModel.getScores();
                                    SimulationActivity.sendAnswersData(params);


                                }else {
                                    Toast.makeText(mContext,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
                                    //createAlertDialogue();
                                }
                            }
                        } else {


                            if(SimulationActivity.isConnectionAvailable()){
                                //click first time on buttoon
                                SimulationActivity.mp_click.start();
                                Log.e("btn", "clicked");
                                click_pos.add(messageModel.getSm_data_id());
                                flag_firstclick = true;
                                click_latest_pos = expertCardModelList.size();
                                // String params = "coversation_order=" + messageModel.getConversation_order();
                                String params = "sm_data_id="+ messageModel.getConversation_order()+"&u_id="+ SimulationActivity.candidateId+"&simulation_id="+ SimulationActivity.sm_id_is+"&scores="+messageModel.getScores();
                                SimulationActivity.sendAnswersData(params);


                            }else {
                                Toast.makeText(mContext,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
                                //createAlertDialogue();
                            }

                        }

                    }
                }
            });

            viewHolder.fullscreen_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*  Log.e("fullscreen","cliced");
                    String [] vid=messageModel.getVideo().split("/");
                    String [] lastname=vid[vid.length-1].split("\\.");
                    String name=lastname[0];
                    File video_file_is = new File(SimulationActivity.folder, vid[vid.length-1]);
                    notifyDataSetChanged();
                    SimulationActivity.displayVideo(video_file_is.getPath());
*/

                }
            });




            //click on finish button from score layout
            viewHolder.finsh_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    ConnectionUtils connectionUtils=new ConnectionUtils(mContext);

                    if(connectionUtils.isConnectionAvailable()){
                        SimulationActivity.finish_simulation=false;
                        SimulationActivity.temp_finish=false;
                        try {
                            SimulationActivity.timer1.cancel();
                            //  SimulationActivity.timer.purge();
                            SimulationActivity.timertask1.cancel();
                            SimulationActivity.timer.cancel();
                            //  SimulationActivity.timer.purge();
                            SimulationActivity.timertask.cancel();


                            //   SimulationActivity.expertCardAdapter.notifyDataSetChanged();
                            // SimulationActivity.i = 0;
                        }catch (Exception e){

                        }
                        SimulationActivity.arrayList.clear();
                        ExpertCardAdapter.click_download_index.clear();
                        SimulationActivity.timer1=null;
                        //  SimulationActivity.timer.purge();
                        SimulationActivity.timertask1=null;
                        SimulationActivity.timer=null;
                        SimulationActivity.timertask=null;
                        if(SimulationActivity.from_where.equalsIgnoreCase("learn")){
                            if(mylinkstatus){

                            }else {
                                SimulationActivity.sendProgress();

                            }

                            new PrefManager(mContext).isclicked("yes");

                            new PrefManager(mContext).isResultscreenpause("no");

                            if(new PrefManager(mContext).getstatusforFeedback().equalsIgnoreCase("completed")){
                                Intent i = new Intent(mContext, ModulesActivity.class );
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                if (mContext instanceof Activity) {
                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    ((Activity) mContext).finish();
                                    ((Activity) mContext).startActivity(i);
                                }

                            }else {

                                if(new PrefManager(mContext).getcompulsaryfeedback().equalsIgnoreCase("yes")){
                                    Intent i = new Intent(mContext, FeedbackActivity.class );
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                    i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    if (mContext instanceof Activity) {
                                        ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                        ((Activity) mContext).finish();
                                        ((Activity) mContext).startActivity(i);
                                    }
                                }else {
                                    Intent i = new Intent(mContext, ModulesActivity.class );
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                    i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    if (mContext instanceof Activity) {
                                        ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                        ((Activity) mContext).finish();
                                        ((Activity) mContext).startActivity(i);
                                    }
                                }





                            }



                        }else {
                            new PrefManager(mContext).isclicked("yes");

                            new PrefManager(mContext).isResultscreenpause("no");

                            if(new PrefManager(mContext).getcompulsaryfeedback().equalsIgnoreCase("yes")){
                                Intent i = new Intent(mContext, HomeActivity.class );
                                Log.e("navigation","ass");
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.putExtra("tabs","normal");
                                if (mContext instanceof Activity) {
                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    ((Activity) mContext).finish();
                                    ((Activity) mContext).startActivity(i);
                                }
                            }else {
                                Intent i = new Intent(mContext, HomeActivity.class );
                                Log.e("navigation","ass");
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.putExtra("tabs","normal");
                                if (mContext instanceof Activity) {
                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    ((Activity) mContext).finish();
                                    ((Activity) mContext).startActivity(i);
                                }
                            }





                        }

                    }else {

                        Toast.makeText(mContext,"Please check your Internet connection!",Toast.LENGTH_SHORT).show();
                    }








                }
            });
            if (messageModel.getDecision().equalsIgnoreCase("")) {

                viewHolder.btn_frame.setVisibility(View.GONE);
                //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
                viewHolder.ques.setVisibility(View.VISIBLE);
                //set profile image

                try {
                    if (messageModel.getAlign().equalsIgnoreCase("0")) {

                        Glide.with(mContext).load(messageModel.getUser_image())
                                .thumbnail(0.5f)
                                .crossFade()
                                .placeholder(R.drawable.placeholder_profile)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(viewHolder.left_img);
                       // setAnimationRight(viewHolder.itemView,position);
                        // setAnimation(viewHolder.itemView,position);
                    } else {


                        Glide.with(mContext).load(messageModel.getUser_image())
                                .thumbnail(0.5f)
                                .crossFade()
                                .placeholder(R.drawable.placeholder_profile)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(viewHolder.right_img);
                      //  setAnimationLeft(viewHolder.itemView,position);
                    }
                }catch (Exception e){

                }



            }
            click_position_used.add(position);





        if(SimulationActivity.finish_simulation){
            if(messageModel.getIsfinished().equalsIgnoreCase("2")){
                Log.e("sim","finish");
                viewHolder.download_btn.setVisibility(View.GONE);
                viewHolder.audio_frame.setVisibility(View.GONE);
                viewHolder.video_frame.setVisibility(View.GONE);
                viewHolder.img_frame.setVisibility(View.GONE);
                viewHolder.btn_frame.setVisibility(View.GONE);
                viewHolder.score_layout.setVisibility(View.VISIBLE);
                viewHolder.score_layout.setMinimumHeight(SimulationActivity.height);
                Glide.with(mContext).load(messageModel.getTy_img())
                        .thumbnail(0.5f)
                        .crossFade()
                        .placeholder(R.drawable.placeholder_img_is)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.score_layout_image);
                //  viewHolder.small_btn.setVisibility(View.INVISIBLE);
                viewHolder.ty_text.setText(messageModel.getTy_text());
                viewHolder.ques.setVisibility(View.GONE);
                viewHolder.left_img.setVisibility(View.GONE);
                viewHolder.right_img.setVisibility(View.GONE);
                viewHolder.ques_relative.setGravity(Gravity.LEFT);
                viewHolder.score_layout.setVisibility(View.VISIBLE);
                viewHolder.score_text.setText(messageModel.getTy_desc());
                 viewHolder.score_label.setText(messageModel.getTy_percentage()+"%");
                SimulationActivity.touch_to_procced.setVisibility(View.GONE);
                SimulationActivity.temp_finish=true;

                if(SimulationActivity.from_where.equalsIgnoreCase("learn")) {
                    mylinkstatus=true;
                    SimulationActivity.sendProgress();
                }
            }
        }
        if(messageModel.getIsfinished().equalsIgnoreCase("1")){
            if(SimulationActivity.temp_finish){

            }else {
                SimulationActivity.displayTouchButton();
                SimulationActivity.finish_simulation = true;

            }
        }
        //setAnimationLeft(viewHolder.itemView,position);

    }

    private void playVideoFromExistingSource(VideoView video, File video_file) {

    }

    @Override
    public int getItemCount() {
        return expertCardModelList.size();
    }

    private void setAnimationRight(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.anim_chat_right);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
    private void setAnimationLeft(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }



    public void bufferVideo(final VideoView video, String video_file, final ImageView video_btn){
        SimulationActivity.mp.pause();
        try {
            // Start the MediaController

            styleMediaController(SimulationActivity.mediacontroller);
            SimulationActivity.mediacontroller.setAnchorView(video);
            // Get the URL from String VideoURL
            video.setMediaController(SimulationActivity.mediacontroller);

                Uri videouri = Uri.parse(video_file);

                video.setVideoURI(videouri);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        video.requestFocus();
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                video.start();
            }
        });
        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                   // notifyDataSetChanged();
                video_btn.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                SimulationActivity.mp.start();
            }
        });



    }


    public void bufferAudio(final VideoView video, String video_file, final ImageView audio_btn){
        SimulationActivity.mp.pause();
        try {
            // Start the MediaController

            styleMediaController(SimulationActivity.mediacontroller1);
            SimulationActivity.mediacontroller1.setAnchorView(video);
            // Get the URL from String VideoURL
            video.setMediaController(SimulationActivity.mediacontroller1);
            Uri videouri = Uri.parse(video_file);
                video.setVideoURI(videouri);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        video.requestFocus();

        video.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
              /*  if(hasFocus){
                    SimulationActivity.isfocused=true;
                    video.start();
                    Log.e("focus", "changed");
                    SimulationActivity.mp.pause();
                }else {

                    SimulationActivity.isfocused=false;
                    video.pause();
                    Log.e("focus", "changed");
                    SimulationActivity.mp.start();
                }*/


            }
        });
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                video.start();
                SimulationActivity.mediacontroller1.show(200000);
            }
        });

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                try {
                    SimulationActivity.mediaController11.hide();
                }catch (Exception ex){

                }

                audio_btn.setVisibility(View.VISIBLE);
               notifyDataSetChanged();
               SimulationActivity.mp.start();

            }
        });


    }

    private void styleMediaController(View view) {
        if (view instanceof MediaController) {
            MediaController v = (MediaController) view;
            for(int i = 0; i < v.getChildCount(); i++) {
                styleMediaController(v.getChildAt(i));
            }
        } else
        if (view instanceof LinearLayout) {
            LinearLayout ll = (LinearLayout) view;
            for(int i = 0; i < ll.getChildCount(); i++) {
                styleMediaController(ll.getChildAt(i));
            }
        } else if (view instanceof SeekBar) {
            ((SeekBar) view).setProgressDrawable(mContext.getResources().getDrawable(R.drawable.blueleftt));
            ((SeekBar) view).setThumb(mContext.getResources().getDrawable(R.drawable.bluerightt));
        }
    }
}




