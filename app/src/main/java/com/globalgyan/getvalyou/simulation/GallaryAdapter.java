package com.globalgyan.getvalyou.simulation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.globalgyan.getvalyou.R;

import java.util.ArrayList;

/**
 * Created by Globalgyan on 16-02-2018.
 */

public class GallaryAdapter extends ArrayAdapter<GallaryModel> {
    Context mcontext;
    ArrayList<GallaryModel> subjectModelArrayList;
    public GallaryAdapter(GallaryAvtivity mcontext, ArrayList<GallaryModel> subjectModelArrayList) {
        super(mcontext, R.layout.gallary_items, subjectModelArrayList);
        this.mcontext=mcontext;
        this.subjectModelArrayList=subjectModelArrayList;

    }

    private static class ViewHolder{
        TextView filename;
        ImageView thumbnail,type_ic;

    }

    @Override
    public GallaryModel getItem(int position)
    {
        if (subjectModelArrayList != null) {
            return subjectModelArrayList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final GallaryModel gallaryModel = getItem(position);

        final ViewHolder viewHolder;
        if(convertView==null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView=inflater.inflate(R.layout.gallary_items, parent, false);
            viewHolder.filename=(TextView)convertView.findViewById(R.id.file_name);
            viewHolder.thumbnail=(ImageView)convertView.findViewById(R.id.thmbnail);
            viewHolder.type_ic=(ImageView)convertView.findViewById(R.id.type_ic);

            Typeface tf1 = Typeface.createFromAsset(mcontext.getAssets(), "Gotham-Medium.otf");

            viewHolder.filename.setTypeface(tf1);

            convertView.setTag(viewHolder);
        }else{
            viewHolder=(ViewHolder)convertView.getTag();
        }

        try {
            if (gallaryModel.getType().equalsIgnoreCase("image")) {
                viewHolder.thumbnail.setImageURI(Uri.parse(gallaryModel.getPath()));
                viewHolder.filename.setText(gallaryModel.getName());
                viewHolder.type_ic.setImageResource(R.drawable.image_ic_icon);
            }
            if (gallaryModel.getType().equalsIgnoreCase("video")) {
                viewHolder.thumbnail.setImageBitmap(gallaryModel.getThumbnail());
                viewHolder.filename.setText(gallaryModel.getName());
                viewHolder.type_ic.setImageResource(R.drawable.video_ic);
            }
            if (gallaryModel.getType().equalsIgnoreCase("audio")) {
                viewHolder.thumbnail.setImageResource(R.drawable.audio_album_art);
                viewHolder.thumbnail.setBackgroundColor(Color.parseColor("#000000"));
                viewHolder.filename.setText(gallaryModel.getName());
                viewHolder.type_ic.setImageResource(R.drawable.audio_ic);
            }

            viewHolder.thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.thumbnail.setEnabled(false);
                    viewHolder.thumbnail.setClickable(false);
                    new CountDownTimer(2000, 2000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            viewHolder.thumbnail.setEnabled(true);
                            viewHolder.thumbnail.setClickable(true);
                        }
                    }.start();
                  //  Intent intent = new Intent(Intent.ACTION_VIEW);
                    if(gallaryModel.getType().equalsIgnoreCase("image")){
                        Intent i=new Intent(mcontext,SimulationShowItemsActivity.class);
                        i.putExtra("type","image");
                        i.putExtra("path",gallaryModel.getPath());
                        i.putExtra("name",gallaryModel.getName());

                        mcontext.startActivity(i);
                        if (mcontext instanceof Activity) {
                            ((Activity) mcontext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        }
                        // intent.setDataAndType(Uri.parse(gallaryModel.getPath()), "image/*");

                    }
                    if(gallaryModel.getType().equalsIgnoreCase("video")){
                        // intent.setDataAndType(Uri.parse(gallaryModel.getPath()), "video/*");
                        Intent i=new Intent(mcontext,SimulationShowItemsActivity.class);
                        i.putExtra("type","video");
                        i.putExtra("path",gallaryModel.getPath());
                        i.putExtra("name",gallaryModel.getName());
                        mcontext.startActivity(i);
                        if (mcontext instanceof Activity) {
                            ((Activity) mcontext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        }
                    }
                    if(gallaryModel.getType().equalsIgnoreCase("audio")){
                        // intent.setDataAndType(Uri.parse(gallaryModel.getPath()), "audio/x-wav");
                        Intent i=new Intent(mcontext,SimulationShowItemsActivity.class);
                        i.putExtra("type","audio");
                        i.putExtra("path",gallaryModel.getPath());
                        i.putExtra("name",gallaryModel.getName());
                        mcontext.startActivity(i);
                        if (mcontext instanceof Activity) {
                            ((Activity) mcontext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        }
                    }

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

        return convertView;

    }


}


