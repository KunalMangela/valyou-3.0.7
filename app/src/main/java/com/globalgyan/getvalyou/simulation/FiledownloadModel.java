package com.globalgyan.getvalyou.simulation;

/**
 * Created by Globalgyan on 19-02-2018.
 */

public class FiledownloadModel {
    int id;
    String type,name;

    public FiledownloadModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
