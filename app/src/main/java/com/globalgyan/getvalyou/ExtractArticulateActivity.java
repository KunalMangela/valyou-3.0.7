package com.globalgyan.getvalyou;

import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.globalgyan.getvalyou.MasterSlaveGame.MasterSlaveGame;
import com.globalgyan.getvalyou.simulation.core.DownloadManagerPro;
import com.globalgyan.getvalyou.simulation.report.listener.DownloadManagerListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ExtractArticulateActivity extends AppCompatActivity implements DownloadManagerListener {
    static DownloadManagerPro dm;
    static File folder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extract_articulate);
        createFolder();
        dm = new DownloadManagerPro(this.getApplicationContext());
        dm.init(folder.getPath(), 12, ExtractArticulateActivity.this);
        downloadData("https://valyououtputbucket.s3.ap-south-1.amazonaws.com/valyou/learning/lesson/lms_zip/Riely0rIBQ.zip","Azr");

    }
    public static void downloadData(String uri, String substring) {

        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {

            dm.startDownload(task);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void createFolder() {
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "ArticulateData");
        boolean success = true;
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }
    @Override
    public void OnDownloadStarted(long taskId) {
        Log.e("status",String.valueOf("started"));

    }

    @Override
    public void OnDownloadPaused(long taskId) {

    }

    @Override
    public void onDownloadProcess(long taskId, double percent, long downloadedLength) {
        Log.e("status",String.valueOf(percent));

    }

    @Override
    public void OnDownloadFinished(long taskId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.e("status","completed");
                File file=new File(folder,"Azr.zip");
                extractiontask(file.getPath(),"Newazr");
            }
        });

    }

    private void extractiontask(final String path, final String newazr) {

        new CountDownTimer(2000, 2000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                new ExtractFile().execute(path,newazr);

            }
        }.start();
    }

    @Override
    public void OnDownloadRebuildStart(long taskId) {

    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {

    }

    @Override
    public void OnDownloadCompleted(long taskId) {
        Log.e("status","completed");

    }

    @Override
    public void connectionLost(long taskId) {

    }



    public class ExtractFile extends AsyncTask<String,Void,String>{
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equalsIgnoreCase("true")){
                Log.e("status","true");

            }else {
                Log.e("status","false");
            }
        }

        @Override
        protected String doInBackground(String... voids) {

                InputStream is;
                ZipInputStream zis;
                try
                {
                    String filename;
                    is = new FileInputStream(voids[0]);
                    zis = new ZipInputStream(new BufferedInputStream(is));
                    ZipEntry ze;
                    byte[] buffer = new byte[1024];
                    int count;

                    while ((ze = zis.getNextEntry()) != null)
                    {
                        // zapis do souboru
                        filename = ze.getName();

                        // Need to create directories if not exists, or
                        // it will generate an Exception...
                        if (ze.isDirectory()) {
                            File fmd = new File(voids[0] + filename);
                            fmd.mkdirs();
                            continue;
                        }

                        FileOutputStream fout = new FileOutputStream(voids[0] + filename);

                        // cteni zipu a zapis
                        while ((count = zis.read(buffer)) != -1)
                        {
                            fout.write(buffer, 0, count);
                        }

                        fout.close();
                        zis.closeEntry();
                    }

                    zis.close();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                    return String.valueOf(false);
                }

                return String.valueOf(true);
                   }
    }
}
