package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Interview;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 2/1/17
 *         Module : Valyou.
 */
public class InterviewDetailsResponse extends ValYouResponse{

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private boolean statusCode= false;


    private Interview interview = null;

    public Interview getInterview() {
        return interview;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }
}
