package com.globalgyan.getvalyou.cms.response;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 31/3/17
 *         Module : Valyou.
 */

public class UpdateMobileVerificationResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return status;
    }

    @Override
    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean status = false;

    private String message = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
