package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.JobList;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 16/12/16
 *         Module : Valyou.
 */
public class JobDetailResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private JobList jobList = null;

    public JobList getJobList() {
        return jobList;
    }

    public void setJobList(JobList jobList) {
        this.jobList = jobList;
    }
}
