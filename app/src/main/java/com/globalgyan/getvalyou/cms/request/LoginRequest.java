package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/11/16
 *         Module : Valyou.
 */
public class LoginRequest extends ValYouRequest {


    private String email = null;

    private String password = null;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String fcmToken = null;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    /**
     *
     * Constructor for GUIRequest.
     *
     */
    public LoginRequest() {
        super(AppConstant.BASE_URL+REQUEST_TYPE_LOGIN, RequestTypes.POST);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        JSONObject object = new JSONObject();
        try {
            object.put("email", email);
            object.put("password", password);
            object.put("FCMToken",fcmToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
