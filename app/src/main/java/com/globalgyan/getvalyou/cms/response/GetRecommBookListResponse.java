package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.BookData;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetRecommBookListResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private boolean statusCode= false;


    public List<BookData> getArticles() {
        return articles;
    }

    public void setArticles(List<BookData> articles) {
        this.articles = articles;
    }

    private List<BookData> articles = null;

}
