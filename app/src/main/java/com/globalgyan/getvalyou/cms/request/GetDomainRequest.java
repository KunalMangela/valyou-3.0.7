package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 28/11/16
 *         Module : Valyou.
 */
public class GetDomainRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetDomainRequest() {
        super(AppConstant.BASE_URL + REQUEST_TYPE_GET_DOMAIN, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
