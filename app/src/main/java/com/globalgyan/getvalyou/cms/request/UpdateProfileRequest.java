package com.globalgyan.getvalyou.cms.request;

import android.content.Context;
import android.text.TextUtils;

import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.PriorExperience;
import com.globalgyan.getvalyou.model.Professional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 25/11/16
 *         Module : Valyou.
 */
public class UpdateProfileRequest extends ValYouRequest {

    private Context context = null;

    /**
     * Constructor for GUIRequest.
     */
    public UpdateProfileRequest(String userId, Context context) {
        super(AppConstant.BASE_URL + REQUEST_TYPE_UPDATE_PROFILE + userId + "/updateProfile", RequestTypes.POST);
        this.context = context;
    }


    private ArrayList<EducationalDetails> educationalDetails = null;

    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private ArrayList<PriorExperience> priorWorkExperience = null;


    private ArrayList<Professional> professional = null;


    private String industry = null;
    private String dateOfBirth = null;
    private String gender = null;


    private String employmentStatus = null;

    @Override
    public JSONObject getURLEncodedPostdata() {
        JSONObject object = new JSONObject();
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        try {
            object.put("industry", dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY));
            object.put("dateOfBirth", dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB));
            object.put("gender", dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER));
            object.put("profileComplete", true);
            object.put("displayName", dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME));
            object.put("mobile", dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE));
            object.put("email", dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL));
            //  object.put("description", dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DESCRIPTION));

            JSONArray currentEducation = new JSONArray();
            for (EducationalDetails educationa : dataBaseHelper.getEducationalDetails()) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("collegeName", educationa.getCollegeName());
                    obj.put("degreeName", educationa.getDegreeName());
                    String startDates = educationa.getFrom();
                    if (!TextUtils.isEmpty(startDates)) {
                        JSONObject start = new JSONObject();
                        String[] startDate = startDates.split("-");
                        if (startDate != null && startDate.length == 2) {
                            try {
                                start.put("year", Integer.parseInt(startDate[1]));
                            } catch (Exception ex) {

                            }
                            start.put("month", getMonth(startDate[0]));
                            obj.put("startDate", start);
                        }
                    }
                    String EndDates = educationa.getTo();
                    JSONObject end = new JSONObject();
                    if (!TextUtils.isEmpty(EndDates)) {

                        String[] endDate = EndDates.split("-");
                        if (endDate != null && endDate.length == 2) {
                            end.put("year", Integer.parseInt(endDate[1]));
                            end.put("month", getMonth(endDate[0]));
                            obj.put("endDate", end);
                        }
                    }


                    //  obj.put("yearOfCompletion", Integer.parseInt(educationa.getYearOfGraduation()));

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                currentEducation.put(obj);
            }

            object.put("currentEducation", currentEducation);

            //    object.put("priorWorkExperience", priorWorkExperienc);
            JSONArray professionalExperience = new JSONArray();
            for (Professional exp : dataBaseHelper.getProfessionalDetails()) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("positionHeld", exp.getPositionHeld());
                    obj.put("organisationName", exp.getOrganizationName());

                    String startDates = exp.getFrom();
                    if (!TextUtils.isEmpty(startDates)) {
                        JSONObject start = new JSONObject();
                        String[] startDate = startDates.split("-");
                        if (startDate != null && startDate.length == 2) {
                            start.put("year", Integer.parseInt(startDate[1]));
                            start.put("month", getMonth(startDate[0]));
                            obj.put("startDate", start);
                        }

                    }
                    String endDates = exp.getTo();
                    if (!TextUtils.isEmpty(endDates)) {
                        JSONObject end = new JSONObject();
                        String[] endDate = endDates.split("-");
                        if (endDate != null && endDate.length == 2) {
                            end.put("year", Integer.parseInt(endDate[1]));
                            end.put("month", getMonth(endDate[0]));
                            obj.put("endDate", end);
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                professionalExperience.put(obj);
            }

            object.put("professionalExperience", professionalExperience);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }


    private int getMonth(String month) {
        int returnValue = 1;
        int index = 1;
        for (String s : months) {
            if (s.equalsIgnoreCase(month)) {
                returnValue = index;
            }
            index++;
        }


        return returnValue;
    }
}
