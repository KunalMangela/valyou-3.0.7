package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Position;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/12/16
 *         Module : Valyou.
 */
public class AddNewPositionResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    public Position getPositionHeld() {
        return positionHeld;
    }

    public void setPositionHeld(Position positionHeld) {
        this.positionHeld = positionHeld;
    }

    private Position positionHeld = null;

}
