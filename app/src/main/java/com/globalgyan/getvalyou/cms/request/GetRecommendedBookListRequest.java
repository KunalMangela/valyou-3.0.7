package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetRecommendedBookListRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetRecommendedBookListRequest() {
        super(AppConstant.BASE_URL+REQUEST_TYPE_GET_RECOMM_BOOK_LIST, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
