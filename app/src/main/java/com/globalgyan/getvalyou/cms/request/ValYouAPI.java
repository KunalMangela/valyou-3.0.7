package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.cms.response.AddNewCollegeResponse;
import com.globalgyan.getvalyou.cms.response.AddNewDegreeResponse;
import com.globalgyan.getvalyou.cms.response.AddNewOrganisationResponse;
import com.globalgyan.getvalyou.cms.response.AddNewPositionResponse;
import com.globalgyan.getvalyou.cms.response.ChanegePasswordResponse;
import com.globalgyan.getvalyou.cms.response.ChangeEmailResponse;
import com.globalgyan.getvalyou.cms.response.ChangeMobileResponse;
import com.globalgyan.getvalyou.cms.response.ForgetPasswordResponse;
import com.globalgyan.getvalyou.cms.response.GetCountResponse;
import com.globalgyan.getvalyou.cms.response.GetValYouScoreResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.SocialRegisterResponse;
import com.globalgyan.getvalyou.cms.response.UpdateMobileExistResponse;
import com.globalgyan.getvalyou.cms.response.UpdateMobileVerificationResponse;
import com.globalgyan.getvalyou.cms.response.UpdateProfileResponse;
import com.globalgyan.getvalyou.cms.response.VerifyOtpResponse;
import com.globalgyan.getvalyou.cms.response.VideoConferenceResponse;
import com.globalgyan.getvalyou.cms.response.VideoResponseResponse;
import com.globalgyan.getvalyou.model.ArticleData;
import com.globalgyan.getvalyou.model.BookData;
import com.globalgyan.getvalyou.model.College;
import com.globalgyan.getvalyou.model.CourseData;
import com.globalgyan.getvalyou.model.Degree;
import com.globalgyan.getvalyou.model.Domain;
import com.globalgyan.getvalyou.model.GetMeetingModel;
import com.globalgyan.getvalyou.model.Industry;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.model.Organization;
import com.globalgyan.getvalyou.model.Position;
import com.globalgyan.getvalyou.model.QuestionAnswerModel;
import com.globalgyan.getvalyou.model.ReadingData;
import com.globalgyan.getvalyou.model.UserDetailsModel;
import com.globalgyan.getvalyou.model.UserJob;
import com.globalgyan.getvalyou.model.VideoData;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/3/17
 *         Module : Valyou.
 */

public interface ValYouAPI {

    @GET("candidateMobileAppGetAllFunctionalAreas")
    Call<List<Industry>>getAllIndustries();

    @GET("candidateMobileAppGetAllDegrees")
    Call<List<Degree>>getAllDegrees();


    @GET("candidateMobileAppStaticPage/VyouQuestions")
    Call<List<QuestionAnswerModel>> getQuestionAnswer();

    @GET("candidateMobileAppGetAllPositionHelds")
    Call<List<Position>>getAllPositionHelds();


    @GET("Player/GetScores/{userID}")
    Call<GetValYouScoreResponse> getValueScore(@Path("userID") String userID);

    @GET("candidateAppGetVideoResponse/{videoId}")
    Call<VideoResponseResponse> getVideoResponse(@Path("videoId") String videoId);

    @GET("candidateMobileAppGetAllColleges")
    Call<List<College>> getAllColleges();

    @GET("candidateMobileAppGetAllDomains")
    Call<List<Domain>> getAllDomain();

    @GET("candidateMobileAppGetAllOrganisations")
    Call<List<Organization>> getAllOrganisations();

    @GET("user/{userId}/type/JOB/user_collection")
    Call<List<UserJob>> getAllUserJobs(@Path("userId") String userId);

    @GET("candidateMobileAppGetCandidateDetails/{userId}")
    Call<UserDetailsModel> getCandidateDetails(@Path("userId") String userId);

    @GET("{phoneNumber}/AUTOGEN")
    Call<SendOtpResponse> sendOtpRequest(@Path("phoneNumber") String phoneNumber);

    @GET("candidateMobileAppGetVideoConferenceDetails/{meetingId}")
    Call<VideoConferenceResponse> videoConferenceDetails (@Path("meetingId") String meetingId);



    @GET("VERIFY/{session}/{code}")
    Call<VerifyOtpResponse> verifyOtp(@Path("session") String session, @Path("code") String code);

    @GET("candidateMobileAppGetMeetingDetailsForCandidate/{candidateId}")
    Call<List<GetMeetingModel>> getMeetingList(@Path("candidateId") String candidateId);

    @GET("candidateMobileAppGetMeetingDetails/{meetingId}")
    Call<GetMeetingModel> getCancelledMeeting(@Path("meetingId") String meetingId);


    @GET("candidateMobileAppGetConfig/{candidateId}")
    Call<GetCountResponse> getCount(@Path("candidateId") String candidateId);

    @GET("candidateMobileGetRecommendedArticleList/{candidateId}")
    Call<List<ArticleData>> getRecommendedArticle(@Path("candidateId") String candidateId);

    @GET("candidateMobileGetRecommendedCourseList/{candidateId}")
    Call<List<CourseData>> getRecommendedCourse(@Path("candidateId") String candidateId);

    @GET("candidateMobileGetRecommendedBookList/{candidateId}")
    Call<List<BookData>> getRecommendedBooks(@Path("candidateId") String candidateId);


    @GET("candidateMobileGetRecommendedVideoList/{candidateId}")
    Call<List<VideoData>> getRecommendedVideos(@Path("candidateId") String candidateId);

    @GET("candidateMobileGetRecommendedReadingList/{candidateId}")
    Call<List<ReadingData>> getRecommendedReading(@Path("candidateId") String candidateId);


    @FormUrlEncoded
    @POST("auth/signin")
    Call <LoginModel> doLogin(@Field("email") String email, @Field("password") String password, @Field("FCMToken") String FCMToken, @Field("location") String loc, @Field("os") String os,@Field("os_version") String osv, @Field("device_name") String devicename);

    @FormUrlEncoded
    @POST("candidateMobileAppSocialNetworkSignin")
    Call <LoginModel> doSocialSignIn(@Field("email") String providerId, @Field("FCMToken") String FCMToken);

    @POST("candidateMobileAppSocialNetworkSignup")
    Call <SocialRegisterResponse> doSocialSignUp(@Body JsonObject socialSignUp);

    @FormUrlEncoded
    @POST("auth/signup")
    Call <LoginModel> doSignUp(@Field("email") String email, @Field("password") String password, @Field("FCMToken") String FCMToken, @Field("firstName") String firstName, @Field("mobile") String mobile,@Field("collegeName") String clg, @Field("location") String loc, @Field("os") String os,@Field("os_version") String osv, @Field("device_name") String devicename);

    @FormUrlEncoded
    @POST("candidateMobileAppCreateCollege")
    Call <AddNewCollegeResponse> addCollege(@Field("name") String name);


    @FormUrlEncoded
    @POST("candidateMobileAppCreateDegree")
    Call <AddNewDegreeResponse> addDegree(@Field("name") String name);


    @FormUrlEncoded
    @POST("candidateMobileAppCreateOrganisation")
    Call <AddNewOrganisationResponse> addNewOrganization(@Field("name") String name);

    @FormUrlEncoded
    @POST("candidateMobileAppCreatePositionHeld")
    Call <AddNewPositionResponse> addNewPosition(@Field("name") String name);

    @FormUrlEncoded
    @PUT("candidateMobileAppSendUserDetailsForGivenMobileNumber")
    Call <ForgetPasswordResponse> forgotPassword(@Field("emailId") String mobileNumber);

    @FormUrlEncoded
    @PUT("candidateMobileAppSetNewPassword")
    Call <ChanegePasswordResponse> changePassword(@Field("userId") String userId, @Field("newPassword") String newPassword);


    @PUT("user/{userId}/updateProfile")
    Call <UpdateProfileResponse> updateProfile(@Path("userId") String userId, @Body JsonObject profiledata);


    @PUT("notificationRead/{notificationId}")
    Call <String> updateNotificationStatus(@Path("notificationId") String notificationId);

    @PUT("candidateMobileAppUpdateVerificationStatus/{candidateId}")
    Call <UpdateMobileVerificationResponse> updateMobileVerifyStatus(@Path("candidateId") String candidateId, @Body JsonObject data);

    @FormUrlEncoded
    @PUT("candidateMobileAppLogout")
    Call <Void> doLogOut(@Field("candidateId") String candidateId, @Field("FCMToken") String FCMToken);

    @FormUrlEncoded
    @PUT("candidateMobileAppCheckForMobileNoExistence/{userId}")
    Call <UpdateMobileExistResponse> updateMobileExistace(@Path("userId") String userId, @Field("mobile") String mobile);

    @FormUrlEncoded
    @PUT("candidateMobileAppUpdateMobileNumber/{userId}")
    Call <ChangeMobileResponse> changeMobileNumber(@Path("userId") String userId, @Field("mobile") String mobile);

    @FormUrlEncoded
    @PUT("candidateMobileAppCheckForEmailExistence/{userId}")
    Call <ChangeEmailResponse> changeEmailId(@Path("userId") String userId, @Field("email") String email);




}
