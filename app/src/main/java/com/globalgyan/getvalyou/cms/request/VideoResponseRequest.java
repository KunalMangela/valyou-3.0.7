package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/11/16
 *         Module : Valyou.
 */
public class VideoResponseRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public VideoResponseRequest(String videoId) {
        //super(AppConstant.BASE_URL + "/user/" + userId + REQUEST_TYPE_VIDEO_UPLOAD, RequestTypes.POST);
        super(AppConstant.BASE_URL + REQUEST_TYPE_VIDEO_RESPONSE+videoId, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
