package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Position;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 28/11/16
 *         Module : Valyou.
 */
public class GetPositionHeldResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }


    public List<Position> getPositionList() {
        return positionList;
    }

    public void setPositionList(List<Position> positionList) {
        this.positionList = positionList;
    }

    private List<Position> positionList = null;

}
