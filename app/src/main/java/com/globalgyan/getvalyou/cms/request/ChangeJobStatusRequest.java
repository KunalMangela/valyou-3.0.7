package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 25/11/16
 *         Module : Valyou.
 */
public class ChangeJobStatusRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public ChangeJobStatusRequest() {
        super(AppConstant.BASE_URL+ REQUEST_TYPE_CHANGE_STATUS, RequestTypes.POST);
    }

    private String user = null;

    private String job = null;

    private String status = null;

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setUser(String user) {
        this.user = user;
    }

    private String type = null;

    @Override
    public JSONObject getURLEncodedPostdata() {
        JSONObject object = new JSONObject();
        try {
            object.put("user", user);
            object.put("job", job);
            object.put("status", status);
            object.put("type", type);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
