package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Course;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetCourseDetailsResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private boolean statusCode= false;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    private Course course = null;

}
