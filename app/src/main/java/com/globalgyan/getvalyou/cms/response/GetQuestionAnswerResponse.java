package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.QuestionAnswerModel;

import java.util.List;

/**
 * Created by Jasmini Mishra. on 10/5/17.
 */

public class GetQuestionAnswerResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    public List<QuestionAnswerModel> getQuestionAnswerList() {
        return QuestionAnswerList;
    }

    public void setQuestionAnswerList(List<QuestionAnswerModel> questionAnswerList) {
        QuestionAnswerList = questionAnswerList;
    }

    private List<QuestionAnswerModel> QuestionAnswerList = null;

}