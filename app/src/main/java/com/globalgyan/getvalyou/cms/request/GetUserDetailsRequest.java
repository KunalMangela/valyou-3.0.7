package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 10/3/17
 *         Module : Valyou.
 */

public class GetUserDetailsRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetUserDetailsRequest(String userId) {
        super(AppConstant.BASE_URL+REQUEST_USER_DETAILS+userId, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
