package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 16/12/16
 *         Module : Valyou.
 */
public class InterviewDetailsRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public InterviewDetailsRequest(String meetingID) {
        super(AppConstant.BASE_URL + REQUEST_TYPE_INTERVIEW_DETAILS + meetingID, RequestTypes.GET );
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
