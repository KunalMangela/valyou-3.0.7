package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/3/17
 *         Module : Valyou.
 */

public class ChangePasswordRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public ChangePasswordRequest() {
        super(AppConstant.BASE_URL + CHANGE_PASSWORD, RequestTypes.POST);
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String userId = null;

    private String newPassword = null;

    @Override
    public JSONObject getURLEncodedPostdata() {
        JSONObject object = new JSONObject();
        try {
            object.put("userId", userId);
            object.put("newPassword", newPassword);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;

    }
}
