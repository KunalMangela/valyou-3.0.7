package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 31/3/17
 *         Module : Valyou.
 */

public class UpdateMobileVerificationRequest extends ValYouRequest {

    /**
     * Constructor for GUIRequest.
     *
     * @param url
     * @param method
     */
    public UpdateMobileVerificationRequest() {
        super(AppConstant.BASE_URL, RequestTypes.POST);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        JSONObject object = new JSONObject();
        try {
            object.put("mobileVerified", mobileVerified);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public void setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    private boolean mobileVerified = false;


}
