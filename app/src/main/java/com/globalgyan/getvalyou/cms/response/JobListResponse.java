package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.JobList;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 25/11/16
 *         Module : Valyou.
 */
public class JobListResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private List<JobList> jobLists = null;

    public List<JobList> getJobLists() {
        return jobLists;
    }

    public void setJobLists(List<JobList> jobLists) {
        this.jobLists = jobLists;
    }
}
