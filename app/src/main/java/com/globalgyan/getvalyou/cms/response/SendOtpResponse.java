package com.globalgyan.getvalyou.cms.response;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/3/17
 *         Module : Valyou.
 */

public class SendOtpResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private String Status = null;

    private String Details = null;

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
