package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 24/3/17
 *         Module : Valyou.
 */

public class GetValYouScoreRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     * @param url
     * @param method
     */
    public GetValYouScoreRequest(String userID) {
        super(AppConstant.GAME_BASE_URL + REQUEST_TYPE_GET_GAME_SCORE + userID, RequestTypes.GET );
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
