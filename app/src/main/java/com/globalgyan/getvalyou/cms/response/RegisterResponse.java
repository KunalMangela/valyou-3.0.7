package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.LoginModel;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 24/11/16
 *         Module : Valyou.
 */
public class RegisterResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private LoginModel loginModel = null;

    public LoginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }

    private String message = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
