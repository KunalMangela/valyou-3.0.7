package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.GetMeetingModel;

import java.util.List;

/**
 * Created by Goutam Kumar K. on 6/3/17.
 */

public class GetMeetingDetailsResponse extends ValYouResponse {
    private boolean status = false;
    @Override
    public boolean isStatus() {
        return status;
    }

    @Override
    public void setStatus(boolean status) {
        this.status = status;
    }

    private List<GetMeetingModel> listOfMeetings = null;

    public List<GetMeetingModel> getListOfMeetings() {
        return listOfMeetings;
    }

    public void setListOfMeetings(List<GetMeetingModel> listOfMeetings) {
        this.listOfMeetings = listOfMeetings;
    }
}
