package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Goutam Kumar K. on 17/3/17.
 */

public class SocialMediaSignInRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     */
    public SocialMediaSignInRequest() {
        super(AppConstant.BASE_URL + REQUEST_TYPE_SOCIAL_MEDIA_LOGIN, RequestTypes.POST);
    }


    private String providerId = null;

    private String fcmToken = null;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }




    @Override
    public JSONObject getURLEncodedPostdata() {

        JSONObject object = new JSONObject();
        try {
            object.put("providerId", providerId);
            object.put("FCMToken", fcmToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;

    }

/*
    {
        "email": "sample@email.com",
            "provider": {
        "providerType": "facebook",
                "providerId": "some provider id",
                "providerData": {
            "key": "value"
        }
    }
    }*/

}

