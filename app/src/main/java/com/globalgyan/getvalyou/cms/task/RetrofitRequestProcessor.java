package com.globalgyan.getvalyou.cms.task;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.widget.Toast;

import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.cms.request.ValYouAPI;
import com.globalgyan.getvalyou.cms.response.AddNewCollegeResponse;
import com.globalgyan.getvalyou.cms.response.AddNewDegreeResponse;
import com.globalgyan.getvalyou.cms.response.AddNewOrganisationResponse;
import com.globalgyan.getvalyou.cms.response.AddNewPositionResponse;
import com.globalgyan.getvalyou.cms.response.ChanegePasswordResponse;
import com.globalgyan.getvalyou.cms.response.ChangeEmailResponse;
import com.globalgyan.getvalyou.cms.response.ChangeMobileResponse;
import com.globalgyan.getvalyou.cms.response.ForgetPasswordResponse;
import com.globalgyan.getvalyou.cms.response.GetAllCollegeResponse;
import com.globalgyan.getvalyou.cms.response.GetCancelledMeetingResponse;
import com.globalgyan.getvalyou.cms.response.GetCountResponse;
import com.globalgyan.getvalyou.cms.response.GetDegreeResponse;
import com.globalgyan.getvalyou.cms.response.GetDomainResponse;
import com.globalgyan.getvalyou.cms.response.GetIndustryResponse;
import com.globalgyan.getvalyou.cms.response.GetMeetingDetailsResponse;
import com.globalgyan.getvalyou.cms.response.GetOrganizationResponse;
import com.globalgyan.getvalyou.cms.response.GetPositionHeldResponse;
import com.globalgyan.getvalyou.cms.response.GetQuestionAnswerResponse;
import com.globalgyan.getvalyou.cms.response.GetRecommArticleListResponse;
import com.globalgyan.getvalyou.cms.response.GetRecommBookListResponse;
import com.globalgyan.getvalyou.cms.response.GetRecommCourseListResponse;
import com.globalgyan.getvalyou.cms.response.GetRecommReadingListResponse;
import com.globalgyan.getvalyou.cms.response.GetRecommVideoListResponse;
import com.globalgyan.getvalyou.cms.response.GetUserDetailsResponse;
import com.globalgyan.getvalyou.cms.response.GetValYouScoreResponse;
import com.globalgyan.getvalyou.cms.response.LogOutResponse;
import com.globalgyan.getvalyou.cms.response.LoginResponse;
import com.globalgyan.getvalyou.cms.response.RegisterResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.SocialLoginResponse;
import com.globalgyan.getvalyou.cms.response.SocialRegisterResponse;
import com.globalgyan.getvalyou.cms.response.UpdateMobileExistResponse;
import com.globalgyan.getvalyou.cms.response.UpdateMobileVerificationResponse;
import com.globalgyan.getvalyou.cms.response.UpdateProfileResponse;
import com.globalgyan.getvalyou.cms.response.VerifyOtpResponse;
import com.globalgyan.getvalyou.cms.response.VideoConferenceResponse;
import com.globalgyan.getvalyou.cms.response.VideoResponseResponse;
import com.globalgyan.getvalyou.helper.RetrofitClient;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.ArticleData;
import com.globalgyan.getvalyou.model.BookData;
import com.globalgyan.getvalyou.model.College;
import com.globalgyan.getvalyou.model.CourseData;
import com.globalgyan.getvalyou.model.Degree;
import com.globalgyan.getvalyou.model.Domain;
import com.globalgyan.getvalyou.model.GetMeetingModel;
import com.globalgyan.getvalyou.model.Industry;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.model.Organization;
import com.globalgyan.getvalyou.model.Position;
import com.globalgyan.getvalyou.model.QuestionAnswerModel;
import com.globalgyan.getvalyou.model.ReadingData;
import com.globalgyan.getvalyou.model.UserDetailsModel;
import com.globalgyan.getvalyou.model.VideoData;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/3/17
 *         Module : Valyou.
 */

public class RetrofitRequestProcessor {


    private Context context = null;


    private GUICallback guiCallBack = null;

    public RetrofitRequestProcessor(Context context, GUICallback callback
    ) {
        this.context = context;
        this.guiCallBack = callback;
    }


    public void getIndustries() {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getAllIndustries().enqueue(new Callback<List<Industry>>() {
            @Override
            public void onResponse(Call<List<Industry>> call, Response<List<Industry>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetIndustryResponse response1 = new GetIndustryResponse();
                ;
                if (response.body() != null) {

                    response1.setIndustryList(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<Industry>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getDegrees() {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getAllDegrees().enqueue(new Callback<List<Degree>>() {
            @Override
            public void onResponse(Call<List<Degree>> call, Response<List<Degree>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetDegreeResponse response1 = new GetDegreeResponse();
                ;
                if (response.body() != null) {

                    response1.setDegreeList(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<Degree>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getPositionHeld() {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getAllPositionHelds().enqueue(new Callback<List<Position>>() {
            @Override
            public void onResponse(Call<List<Position>> call, Response<List<Position>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetPositionHeldResponse response1 = new GetPositionHeldResponse();
                ;
                if (response.body() != null) {

                    response1.setPositionList(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<Position>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getAllColleges() {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getAllColleges().enqueue(new Callback<List<College>>() {
            @Override
            public void onResponse(Call<List<College>> call, Response<List<College>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetAllCollegeResponse response1 = new GetAllCollegeResponse();
                if (response.body() != null) {

                    response1.setColleges(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<College>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getRecommendedArticle(String candidateId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getRecommendedArticle(candidateId).enqueue(new Callback<List<ArticleData>>() {
            @Override
            public void onResponse(Call<List<ArticleData>> call, Response<List<ArticleData>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetRecommArticleListResponse response1 = new GetRecommArticleListResponse();
                if (response.body() != null) {

                    response1.setArticles(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<ArticleData>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getRecommendedCourse(String candidateId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getRecommendedCourse(candidateId).enqueue(new Callback<List<CourseData>>() {
            @Override
            public void onResponse(Call<List<CourseData>> call, Response<List<CourseData>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetRecommCourseListResponse response1 = new GetRecommCourseListResponse();
                if (response.body() != null) {

                    response1.setArticles(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<CourseData>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getRecommendedBooks(String candidateId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getRecommendedBooks(candidateId).enqueue(new Callback<List<BookData>>() {
            @Override
            public void onResponse(Call<List<BookData>> call, Response<List<BookData>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetRecommBookListResponse response1 = new GetRecommBookListResponse();
                if (response.body() != null) {

                    response1.setArticles(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<BookData>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getRecommendedVideos(String candidateId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getRecommendedVideos(candidateId).enqueue(new Callback<List<VideoData>>() {
            @Override
            public void onResponse(Call<List<VideoData>> call, Response<List<VideoData>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetRecommVideoListResponse response1 = new GetRecommVideoListResponse();
                if (response.body() != null) {

                    response1.setVideoDatas(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<VideoData>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getRecommendedReadings(String candidateId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getRecommendedReading(candidateId).enqueue(new Callback<List<ReadingData>>() {
            @Override
            public void onResponse(Call<List<ReadingData>> call, Response<List<ReadingData>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetRecommReadingListResponse response1 = new GetRecommReadingListResponse();
                if (response.body() != null) {

                    response1.setReadingDatas(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<ReadingData>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }

    public void getAllDomains() {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getAllDomain().enqueue(new Callback<List<Domain>>() {
            @Override
            public void onResponse(Call<List<Domain>> call, Response<List<Domain>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetDomainResponse response1 = new GetDomainResponse();
                if (response.body() != null) {

                    response1.setDomainList(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<Domain>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }


    public void getAllOrganisations() {
        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getAllOrganisations().enqueue(new Callback<List<Organization>>() {
            @Override
            public void onResponse(Call<List<Organization>> call, Response<List<Organization>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetOrganizationResponse response1 = new GetOrganizationResponse();
                if (response.body() != null) {

                    response1.setOrganizations(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<Organization>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });

    }


    public void getValYouScore(String userid) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getGameClient(AppConstant.GAME_BASE_URL).create(ValYouAPI.class);

        valYouAPI.getValueScore(userid).enqueue(new Callback<GetValYouScoreResponse>() {
            @Override
            public void onResponse(Call<GetValYouScoreResponse> call, Response<GetValYouScoreResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                GetValYouScoreResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new GetValYouScoreResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<GetValYouScoreResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }


    public void getVideoResponse(String videoId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getVideoResponse(videoId).enqueue(new Callback<VideoResponseResponse>() {
            @Override
            public void onResponse(Call<VideoResponseResponse> call, Response<VideoResponseResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                VideoResponseResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new VideoResponseResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<VideoResponseResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void getCadidateDetails(String userId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getCandidateDetails(userId).enqueue(new Callback<UserDetailsModel>() {
            @Override
            public void onResponse(Call<UserDetailsModel> call, Response<UserDetailsModel> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message()+"sbo");

                GetUserDetailsResponse response1 = new GetUserDetailsResponse();
                if (response.body() != null) {
                    Log.e("BOBD",response.body()+"");
                    response1.setUserDetails(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<UserDetailsModel> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void sendOtp(String phoneNumber) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getOtpClient(AppConstant.OTP_BASE_URL).create(ValYouAPI.class);

        valYouAPI.sendOtpRequest(phoneNumber).enqueue(new Callback<SendOtpResponse>() {
            @Override
            public void onResponse(Call<SendOtpResponse> call, Response<SendOtpResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                SendOtpResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new SendOtpResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<SendOtpResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }


    public void getVideoConferenceDetails(String meetingId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getOtpClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.videoConferenceDetails(meetingId).enqueue(new Callback<VideoConferenceResponse>() {
            @Override
            public void onResponse(Call<VideoConferenceResponse> call, Response<VideoConferenceResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                VideoConferenceResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new VideoConferenceResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<VideoConferenceResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void verifyOtp(String session, String code) {
        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        try {
            dialog.show();
        }catch (Exception e){

        }
        ValYouAPI valYouAPI = RetrofitClient.getOtpClient(AppConstant.OTP_BASE_URL).create(ValYouAPI.class);

        valYouAPI.verifyOtp(session, code).enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                VerifyOtpResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new VerifyOtpResponse();
                    response1.setStatus(false);
                    if (!TextUtils.isEmpty(response.errorBody().toString())) {
                        try {
                            JSONObject jObjError = new JSONObject(convertStreamToString(response.errorBody().byteStream()));
                            response1.setDetails(jObjError.getString("Details"));
                        } catch (Exception e) {
                            response1.setDetails("OTP Expired");
                        }
                    } else {
                        response1.setDetails("OTP Expired");

                    }
                }

                if(response1!=null) {
                    if (response1.getDetails().equalsIgnoreCase("OTP matched")) {
                        guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                    } else {
                        Toast.makeText(context, "Otp mismatched", Toast.LENGTH_SHORT).show();

                        guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
                    }
                }else {
                    Toast.makeText(context, "SERVER ERROR", Toast.LENGTH_SHORT).show();

                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
                }

            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }


    public void doLogin(String email, String password, String token, String loc,String os,String osv, String devicename) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.doLogin(email, password, token,loc,os,osv,devicename).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", String.valueOf(response));

                LoginResponse response1 = new LoginResponse();
                if (response.body() != null) {
                    response1.setLoginModel(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                    if (!TextUtils.isEmpty(response.errorBody().toString())) {
                        try {
                            JSONObject jObjError = new JSONObject(convertStreamToString(response.errorBody().byteStream()));
                            response1.setMessage(jObjError.getString("message"));
                        } catch (Exception e) {
                            response1.setMessage("Invalid Username or Password");
                        }
                    } else {
                        response1.setMessage("Invalid Username or Password");

                    }


                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }


    public void doSocialSignIn(String providerID, String token) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.doSocialSignIn(providerID, token).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                SocialLoginResponse response1 = new SocialLoginResponse();

                Log.e("Response message=", "" + response.message());
                if(!response.message().equalsIgnoreCase("Bad Request")) {

                    if (response.body() != null) {
                        response1.setLoginModel(response.body());
                        response1.setStatus(true);

                    } else {
                        response1.setStatus(false);
                    }

                    if (response1 != null)
                        guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                    else
                        guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

                }
                else{
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
                }

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void doSocialSignUp(JsonObject socialSignUp) {
        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.doSocialSignUp(socialSignUp).enqueue(new Callback<SocialRegisterResponse>() {
            @Override
            public void onResponse(Call<SocialRegisterResponse> call, Response<SocialRegisterResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                SocialRegisterResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new SocialRegisterResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<SocialRegisterResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void doSignUp(String email, String password, String FCMToken, String firstName, String mobile,String clg, String loc,String os,String osv, String devicename) {
       /* final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        try {
            dialog.show();

        }catch (Exception e){
            e.printStackTrace();
        }*/
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.doSignUp(email, password, FCMToken, firstName, mobile,clg,loc,os,osv,devicename).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
               /* try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e)
                {
                    Log.e("------",e.getMessage());
                }*/
                Log.e("Response message=", "" + response.body());

                RegisterResponse response1 = new RegisterResponse();
                if (response.body() != null) {
                    response1.setLoginModel(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                    if (!TextUtils.isEmpty(response.errorBody().toString())) {
                        try {
                            JSONObject jObjError = new JSONObject(convertStreamToString(response.errorBody().byteStream()));
                            response1.setMessage(jObjError.getString("message"));
                        } catch (Exception e) {
                            response1.setMessage("Username already exists");
                        }
                    } else {
                        response1.setMessage("Username already exists");

                    }


                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
               /* try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }*/
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public String convertStreamToString(InputStream inputStream) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
        }

        String finallyError = sb.toString();
        Log.e("finallyError=", "" + finallyError);
        return finallyError;
    }

    public void addCollege(String name) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.addCollege(name).enqueue(new Callback<AddNewCollegeResponse>() {
            @Override
            public void onResponse(Call<AddNewCollegeResponse> call, Response<AddNewCollegeResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                AddNewCollegeResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new AddNewCollegeResponse();
                    response1.setStatus(false);
                }


                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<AddNewCollegeResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void addDegree(String name) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.addDegree(name).enqueue(new Callback<AddNewDegreeResponse>() {
            @Override
            public void onResponse(Call<AddNewDegreeResponse> call, Response<AddNewDegreeResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                AddNewDegreeResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new AddNewDegreeResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<AddNewDegreeResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void addOrganization(String name) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.addNewOrganization(name).enqueue(new Callback<AddNewOrganisationResponse>() {
            @Override
            public void onResponse(Call<AddNewOrganisationResponse> call, Response<AddNewOrganisationResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                AddNewOrganisationResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new AddNewOrganisationResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<AddNewOrganisationResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void addPosition(String name) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.addNewPosition(name).enqueue(new Callback<AddNewPositionResponse>() {
            @Override
            public void onResponse(Call<AddNewPositionResponse> call, Response<AddNewPositionResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                AddNewPositionResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new AddNewPositionResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<AddNewPositionResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void forgotPassword(String mobileNumber) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.forgotPassword(mobileNumber).enqueue(new Callback<ForgetPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                ForgetPasswordResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new ForgetPasswordResponse();
                    response1.setStatus(false);
                    if (!TextUtils.isEmpty(response.errorBody().toString())) {
                        try {
                            JSONObject jObjError = new JSONObject(convertStreamToString(response.errorBody().byteStream()));
                            response1.setMessage(jObjError.getString("message"));
                        } catch (Exception e) {
                            response1.setMessage("This phone number is not registered with us");
                        }
                    } else {
                        response1.setMessage("This phone number is not registered with us");

                    }
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void changePassword(String userId, String password) {
        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.changePassword(userId, password).enqueue(new Callback<ChanegePasswordResponse>() {
            @Override
            public void onResponse(Call<ChanegePasswordResponse> call, Response<ChanegePasswordResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                ChanegePasswordResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new ChanegePasswordResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<ChanegePasswordResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void getMeetingList(String candidateId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getMeetingList(candidateId).enqueue(new Callback<List<GetMeetingModel>>() {
            @Override
            public void onResponse(Call<List<GetMeetingModel>> call, Response<List<GetMeetingModel>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                GetMeetingDetailsResponse response1 = new GetMeetingDetailsResponse();
                if (response.body() != null) {
                    response1.setListOfMeetings(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<List<GetMeetingModel>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }




    public void getCancelledMeeting(String meetingId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getCancelledMeeting(meetingId).enqueue(new Callback<GetMeetingModel>() {
            @Override
            public void onResponse(Call<GetMeetingModel> call, Response<GetMeetingModel> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());

                GetCancelledMeetingResponse response1 = new GetCancelledMeetingResponse();
                if (response.body() != null) {
                    response1.setListOfMeetings(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<GetMeetingModel> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void getCount(String candidateId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getCount(candidateId).enqueue(new Callback<GetCountResponse>() {
            @Override
            public void onResponse(Call<GetCountResponse> call, Response<GetCountResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                GetCountResponse response1 = new GetCountResponse();
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<GetCountResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void updateProfile(String userId, JsonObject jsonObject) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.updateProfile(userId, jsonObject).enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                try {
                      if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                UpdateProfileResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new UpdateProfileResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }


    public void updateMobileVerificationStatus(String candidateId, JsonObject jsonObject) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.updateMobileVerifyStatus(candidateId, jsonObject).enqueue(new Callback<UpdateMobileVerificationResponse>() {
            @Override
            public void onResponse(Call<UpdateMobileVerificationResponse> call, Response<UpdateMobileVerificationResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                UpdateMobileVerificationResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new UpdateMobileVerificationResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<UpdateMobileVerificationResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void updateMobileExistance(String userId, String mobileNumber) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.updateMobileExistace(userId, mobileNumber).enqueue(new Callback<UpdateMobileExistResponse>() {
            @Override
            public void onResponse(Call<UpdateMobileExistResponse> call, Response<UpdateMobileExistResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                UpdateMobileExistResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new UpdateMobileExistResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<UpdateMobileExistResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void changeMobileNumber(String userId, String mobileNumber) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.changeMobileNumber(userId, mobileNumber).enqueue(new Callback<ChangeMobileResponse>() {
            @Override
            public void onResponse(Call<ChangeMobileResponse> call, Response<ChangeMobileResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                ChangeMobileResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new ChangeMobileResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<ChangeMobileResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void changeEmailId(String userId, String email) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.changeEmailId(userId, email).enqueue(new Callback<ChangeEmailResponse>() {
            @Override
            public void onResponse(Call<ChangeEmailResponse> call, Response<ChangeEmailResponse> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                ChangeEmailResponse response1 = null;
                if (response.body() != null) {
                    response1 = response.body();
                    response1.setStatus(true);

                } else {
                    response1 = new ChangeEmailResponse();
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<ChangeEmailResponse> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void updateNotificationStatus(String notiId) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.updateNotificationStatus(notiId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());


            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
            }
        });


    }

    public void doLogOut(String candidateId, String FCMToken) {

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.doLogOut(candidateId, FCMToken).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                Log.e("Response message=", "" + response.message());

                LogOutResponse response1 = new LogOutResponse();
                response1.setStatus(true);


                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });


    }

    public void getQuestionAnswer(){

        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        ValYouAPI valYouAPI = RetrofitClient.getClient(AppConstant.BASE_URL).create(ValYouAPI.class);

        valYouAPI.getQuestionAnswer().enqueue(new Callback<List<QuestionAnswerModel>>() {
            @Override
            public void onResponse(Call<List<QuestionAnswerModel>> call, Response<List<QuestionAnswerModel>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }

                Log.e("Response message=", "" + response.message());
                GetQuestionAnswerResponse response1 = new GetQuestionAnswerResponse();
                ;
                if (response.body() != null) {

                    response1.setQuestionAnswerList(response.body());
                    response1.setStatus(true);

                } else {
                    response1.setStatus(false);
                }

                if (response1 != null)
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.SUCCESS);
                else
                    guiCallBack.requestProcessed(response1, GUICallback.RequestStatus.FAILED);
            }

            @Override
            public void onFailure(Call<List<QuestionAnswerModel>> call, Throwable t) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                catch (Exception e){
                    Log.e("------",e.getMessage());
                }
                guiCallBack.requestProcessed(null, GUICallback.RequestStatus.FAILED);
            }
        });
    }

}
