package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.College;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/12/16
 *         Module : Valyou.
 */
public class AddNewCollegeResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private College college = null;

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }
}
