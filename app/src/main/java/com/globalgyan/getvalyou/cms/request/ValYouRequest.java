package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/11/16
 *         Module : Valyou.
 */
public abstract class ValYouRequest {

    private String url = null;

    public RequestTypes getMethod() {
        return method;
    }

    public static enum RequestTypes {POST, GET };


    private RequestTypes method = RequestTypes.GET;

    /**
     * <p>
     * This method can be used for getting the url.
     * </p>
     *
     * @return The url.
     */
    public String getUrl() {
        return url; // returning url.
    }

    /**
     * Constructor for GUIRequest.
     */
    public ValYouRequest(String url, RequestTypes method) {
        this.url = url;
        this.method = method;
    }

    public abstract JSONObject getURLEncodedPostdata();

    public static final String REQUEST_TYPE_LOGIN = "auth/signin";

    public static final String REQUEST_TYPE_VIDEO_UPLOAD = "uploadVideosToS3/";//"/profileVideoUpload";

    public static final String REQUEST_TYPE_RESUME_UPLOAD = "/candidateResumeUpload";

    public static final String REQUEST_TYPE_PROFILE_PIC_UPLOAD = "/candidateProfilePicUpload";

    public static final String REQUEST_TYPE_VIDEO_RESPONSE = "/candidateAppGetVideoResponse/";



    public static final String OTP_AUTO_GEN = "/AUTOGEN";

    public static final String OTP_VERIFY = "/VERIFY/";



    public static final String REQUEST_TYPE_REGISTER= "auth/signup";
    public static final String REQUEST_TYPE_SOCIAL_MEDIA_REGISTER= "/candidateMobileAppSocialNetworkSignup";
    public static final String REQUEST_TYPE_SOCIAL_MEDIA_LOGIN= "/candidateMobileAppSocialNetworkSignin";

    public static final String REQUEST_TYPE_JOB_LIST = "jobs";

    public static final String REQUEST_TYPE_CHANGE_STATUS = "user-collection";

    public static final String REQUEST_TYPE_UPDATE_PROFILE = "/user/";

    public static final String REQUEST_TYPE_GET_INDUSTRY = "/candidateMobileAppGetAllIndustries";

    public static final String REQUEST_TYPE_GET_DOMAIN = "/candidateMobileAppGetAllDomains";

    public static final String REQUEST_TYPE_GET_ORGANIZATION = "/candidateMobileAppGetAllOrganisations/";

    public static final String REQUEST_TYPE_GET_COLLEGES = "/candidateMobileAppGetAllColleges/";

    public static final String REQUEST_TYPE_GET_USER_LIST = "/type/JOB/user_collection/";

    public static final String REQUEST_TYPE_GET_JOB_DETAILS = "/job/";

    public static final String REQUEST_TYPE_ADD_NEW_COLLEGE = "/candidateMobileAppCreateCollege/";


    public static final String REQUEST_TYPE_ADD_NEW_INDUSTRY = "/candidateMobileAppCreateCollege/";

    public static final String REQUEST_TYPE_ADD_NEW_ORGANIZATION = "/candidateMobileAppCreateOrganisation/";

    public static final String REQUEST_TYPE_INTERVIEW_LIST= "/candidateMobileAppGetMeetingDetailsForCandidate/";

    public static final String REQUEST_TYPE_INTERVIEW_DETAILS = "/candidateMobileAppGetMeetingDetails/";

    public static final String REQUEST_TYPE_GET_MESSAGE_LIST= "/hrReadChat/";

    public static final String REQUEST_TYPE_GET_ARTICLE_LIST= "/candidateMobileGetAllArticles/";

    public static final String REQUEST_TYPE_GET_RECOMM_ARTICLE_LIST= "/candidateMobileGetRecommendedArticleList/";

    public static final String REQUEST_TYPE_GET_RECOMM_COURSE_LIST= "/candidateMobileGetRecommendedCourseList/";


    public static final String REQUEST_TYPE_GET_GAME_SCORE= "/Player/GetScores/";




    public static final String REQUEST_TYPE_GET_RECOMM_BOOK_LIST= "/candidateMobileGetRecommendedBookList/";

    public static final String REQUEST_TYPE_GET_ARTICLE_DETAILS= "/candidateMobileGetArticle/";

    public static final String REQUEST_TYPE_GET_BOOK_LIST= "/candidateMobileGetAllBooks/";

    public static final String REQUEST_TYPE_GET_BOOK_DETAILS= "/candidateMobileGetBook/";

    public static final String REQUEST_TYPE_GET_COURCE_LIST= "/candidateMobileGetAllCourses/";

    public static final String REQUEST_TYPE_GET_COURSE_DETAILS= "/candidateMobileGetCourse/";


    public static final String HOME_IMAGES = AppConstant.BASE_URL +"candidateMobileReadImage/";

    public static final String REQUEST_MEETING_DETAILS = "/candidateMobileAppGetMeetingDetailsForCandidate/";
    public static final String FORGET_PASSWORD = "/candidateMobileAppSendUserDetailsForGivenMobileNumber";
    public static final String CHANGE_PASSWORD = "/candidateMobileAppSetNewPassword";

   // public static final String REQUEST_USER_DETAILS = "/candidateMobileAppGetCandidateDetails/";

    public static final String REQUEST_USER_DETAILS = "/candidateMobileAppGetCandidateDetails/";


    public static final String SEE_VIDEO = AppConstant.BASE_URL +"/candidateMobileSeeVideoProfileFile/";



    // public static final String REQUEST_TYPE_GET_ORGANIZATION = "//";








}



