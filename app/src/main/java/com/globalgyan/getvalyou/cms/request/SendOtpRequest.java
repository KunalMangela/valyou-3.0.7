package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/3/17
 *         Module : Valyou.
 */

public class SendOtpRequest extends ValYouRequest {

    //http://2factor.in/API/V1/3834f2b3-0a20-11e7-9462-00163ef91450/SMS/

    /**
     *
     * Constructor for GUIRequest.
     *
     */
    public SendOtpRequest(String phoneNumber) {
        super(AppConstant.OTP_BASE_URL+phoneNumber+OTP_AUTO_GEN, RequestTypes.POST);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}