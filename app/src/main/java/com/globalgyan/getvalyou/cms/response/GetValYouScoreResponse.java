package com.globalgyan.getvalyou.cms.response;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 24/3/17
 *         Module : Valyou.
 */

public class GetValYouScoreResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return false;
    }

    @Override
    public void setStatus(boolean status) {

    }

    private String UGS_Id = null;

    private String GL_ID = null;

    private String UGS_ValUScore = null;


    private String UGS_ManagerialScore = null;
    private String UGS_AptitudeScore = null;

    public String getUGS_Id() {
        return UGS_Id;
    }

    public void setUGS_Id(String UGS_Id) {
        this.UGS_Id = UGS_Id;
    }

    public String getGL_ID() {
        return GL_ID;
    }

    public void setGL_ID(String GL_ID) {
        this.GL_ID = GL_ID;
    }

    public String getUGS_ValUScore() {
        return UGS_ValUScore;
    }

    public void setUGS_ValUScore(String UGS_ValUScore) {
        this.UGS_ValUScore = UGS_ValUScore;
    }

    public String getUGS_ManagerialScore() {
        return UGS_ManagerialScore;
    }

    public void setUGS_ManagerialScore(String UGS_ManagerialScore) {
        this.UGS_ManagerialScore = UGS_ManagerialScore;
    }

    public String getUGS_AptitudeScore() {
        return UGS_AptitudeScore;
    }

    public void setUGS_AptitudeScore(String UGS_AptitudeScore) {
        this.UGS_AptitudeScore = UGS_AptitudeScore;
    }
}
