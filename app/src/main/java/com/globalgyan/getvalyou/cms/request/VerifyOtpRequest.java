package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/3/17
 *         Module : Valyou.
 */

public class VerifyOtpRequest extends ValYouRequest {

    //http://2factor.in/API/V1/3834f2b3-0a20-11e7-9462-00163ef91450/SMS/

    /**
     *
     * Constructor for GUIRequest.
     *
     */
    public VerifyOtpRequest(String session, String code) {
        super(AppConstant.OTP_BASE_URL+OTP_VERIFY+session+"/"+code, RequestTypes.POST);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}