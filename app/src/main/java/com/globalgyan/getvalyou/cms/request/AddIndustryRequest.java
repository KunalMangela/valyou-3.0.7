package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 2/3/17
 *         Module : Valyou.
 */

public class AddIndustryRequest extends ValYouRequest {
    public AddIndustryRequest() {
        super(AppConstant.BASE_URL + REQUEST_TYPE_ADD_NEW_COLLEGE, RequestTypes.POST);
    }

    private String name = null;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public JSONObject getURLEncodedPostdata() {

        JSONObject object = new JSONObject();
        try {
            object.put("name", name);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
