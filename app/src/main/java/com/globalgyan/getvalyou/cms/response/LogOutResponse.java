package com.globalgyan.getvalyou.cms.response;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 10/4/17
 *         Module : Valyou.
 */

public class LogOutResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }
}
