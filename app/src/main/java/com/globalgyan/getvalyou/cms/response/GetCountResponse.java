package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Video;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 14/4/17
 *         Module : Valyou.
 */

public class GetCountResponse extends ValYouResponse {

    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }


    private int unreadNotificationCount = 0;

    private int upcomingInteractionCount = 0;

    private String valyouScore = null;

    private Video videos = null;

    public Video getVideos() {
        return videos;
    }

    public void setVideos(Video videos) {
        this.videos = videos;
    }

    public int getUnreadNotificationCount() {
        return unreadNotificationCount;
    }

    public void setUnreadNotificationCount(int unreadNotificationCount) {
        this.unreadNotificationCount = unreadNotificationCount;
    }

    public int getUpcomingInteractionCount() {
        return upcomingInteractionCount;
    }

    public void setUpcomingInteractionCount(int upcomingInteractionCount) {
        this.upcomingInteractionCount = upcomingInteractionCount;
    }

    public String getValyouScore() {
        return valyouScore;
    }

    public void setValyouScore(String valyouScore) {
        this.valyouScore = valyouScore;
    }
}
