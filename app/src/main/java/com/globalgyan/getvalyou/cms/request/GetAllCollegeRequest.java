package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/12/16
 *         Module : Valyou.
 */
public class GetAllCollegeRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetAllCollegeRequest() {
        super(AppConstant.BASE_URL+REQUEST_TYPE_GET_COLLEGES, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
