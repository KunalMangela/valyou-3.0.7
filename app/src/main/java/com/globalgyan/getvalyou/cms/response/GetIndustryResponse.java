package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Industry;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 28/11/16
 *         Module : Valyou.
 */
public class GetIndustryResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private List<Industry> industryList = null;

    public List<Industry> getIndustryList() {
        return industryList;
    }

    public void setIndustryList(List<Industry> industryList) {
        this.industryList = industryList;
    }
}
