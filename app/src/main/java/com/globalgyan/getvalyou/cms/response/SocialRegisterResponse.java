package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.LoginModel;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 24/11/16
 *         Module : Valyou.
 */
public class SocialRegisterResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private LoginModel user = null;

    public LoginModel getLoginModel() {
        return user;
    }

    public void setLoginModel(LoginModel loginModel) {
        this.user = loginModel;
    }
}
