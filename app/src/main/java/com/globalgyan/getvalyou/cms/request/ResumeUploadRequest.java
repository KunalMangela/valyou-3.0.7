package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/11/16
 *         Module : Valyou.
 */
public class ResumeUploadRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public ResumeUploadRequest(String userId) {
        super(AppConstant.BASE_URL + "/user/" + userId + REQUEST_TYPE_RESUME_UPLOAD, RequestTypes.POST);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
