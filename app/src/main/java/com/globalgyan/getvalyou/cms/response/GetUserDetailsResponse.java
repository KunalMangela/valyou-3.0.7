package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.UserDetailsModel;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 10/3/17
 *         Module : Valyou.
 */

public class GetUserDetailsResponse extends ValYouResponse {
    private boolean statusCode = false;
    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private UserDetailsModel userDetails = null;

    public UserDetailsModel getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetailsModel userDetails) {
        this.userDetails = userDetails;
    }
}


