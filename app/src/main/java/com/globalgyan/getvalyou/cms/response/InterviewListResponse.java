package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Interview;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 29/12/16
 *         Module : Valyou.
 */
public class InterviewListResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private List<Interview> interviewList = null;

    public List<Interview> getInterviewList() {
        return interviewList;
    }

    public void setInterviewList(List<Interview> interviewList) {
        this.interviewList = interviewList;
    }
}
