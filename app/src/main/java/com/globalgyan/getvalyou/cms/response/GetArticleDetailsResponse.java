package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Article;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetArticleDetailsResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private boolean statusCode= false;

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    private Article article = null;

}
