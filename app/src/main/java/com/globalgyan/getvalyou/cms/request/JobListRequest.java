package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 25/11/16
 *         Module : Valyou.
 */
public class JobListRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public JobListRequest() {
        super(AppConstant.BASE_URL+REQUEST_TYPE_JOB_LIST, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
