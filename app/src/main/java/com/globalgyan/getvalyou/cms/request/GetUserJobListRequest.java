package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 13/12/16
 *         Module : Valyou.
 */
public class GetUserJobListRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetUserJobListRequest(String userId) {
        super(AppConstant.BASE_URL+REQUEST_TYPE_UPDATE_PROFILE + userId + REQUEST_TYPE_GET_USER_LIST , RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
