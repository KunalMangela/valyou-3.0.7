package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.LoginModel;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/11/16
 *         Module : Valyou.
 */
public class SocialLoginResponse extends ValYouResponse {

    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private LoginModel loginModel = null;

    public LoginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }
}
