package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.GetMeetingModel;

/**
 * Created by Goutam Kumar K. on 6/3/17.
 */

public class GetCancelledMeetingResponse extends ValYouResponse {
    private boolean status = false;
    @Override
    public boolean isStatus() {
        return status;
    }

    @Override
    public void setStatus(boolean status) {
        this.status = status;
    }

    public GetMeetingModel getListOfMeetings() {
        return listOfMeetings;
    }

    public void setListOfMeetings(GetMeetingModel listOfMeetings) {
        this.listOfMeetings = listOfMeetings;
    }

    private GetMeetingModel listOfMeetings = null;


}
