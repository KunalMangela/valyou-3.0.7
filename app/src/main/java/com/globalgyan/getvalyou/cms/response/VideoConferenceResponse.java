package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Meeting;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 14/4/17
 *         Module : Valyou.
 */

public class VideoConferenceResponse extends ValYouResponse {

    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }


    private Meeting meeting = null;

    private String message = null;

    public Meeting getMeeting() {
        return meeting;
    }

    public void setMeeting(Meeting meeting) {
        this.meeting = meeting;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
