package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetMessageListRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     * @param url
     * @param method
     */
    public GetMessageListRequest(String chatId) {
        super(AppConstant.BASE_URL+REQUEST_TYPE_GET_MESSAGE_LIST+chatId, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
