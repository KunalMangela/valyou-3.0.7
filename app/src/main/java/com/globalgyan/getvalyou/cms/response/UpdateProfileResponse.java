package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.PriorExperience;
import com.globalgyan.getvalyou.model.Professional;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 25/11/16
 *         Module : Valyou.
 */
public class UpdateProfileResponse extends ValYouResponse {
   // private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return status;
    }

    @Override
    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean status = false;


    private String _id = null;
    private String displayName = null;
    private String username = null;
   // private String provider = null;
    private String firstName = null;
    private String email = null;
    private String mobile = null;
    private String videoProfileURL = null;
    private String currentLocation = null;
    private String dateOfBirth = null;
    private String employmentStatus = null;
    private String gender = null;
    private String industry = null;
    private boolean profileComplete = false;
    private String profileImageURL = null;

    private List<Professional> professionalExperience = null;
    private List<EducationalDetails> currentEducation = null;
    private List<PriorExperience> priorWorkExperience = null;

    private String[] domain = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getVideoProfileURL() {
        return videoProfileURL;
    }

    public void setVideoProfileURL(String videoProfileURL) {
        this.videoProfileURL = videoProfileURL;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public boolean isProfileComplete() {
        return profileComplete;
    }

    public void setProfileComplete(boolean profileComplete) {
        this.profileComplete = profileComplete;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public List<Professional> getProfessionalExperience() {
        return professionalExperience;
    }

    public void setProfessionalExperience(List<Professional> professionalExperience) {
        this.professionalExperience = professionalExperience;
    }

    public List<EducationalDetails> getCurrentEducation() {
        return currentEducation;
    }

    public void setCurrentEducation(List<EducationalDetails> currentEducation) {
        this.currentEducation = currentEducation;
    }

    public List<PriorExperience> getPriorWorkExperience() {
        return priorWorkExperience;
    }

    public void setPriorWorkExperience(List<PriorExperience> priorWorkExperience) {
        this.priorWorkExperience = priorWorkExperience;
    }

    public String[] getDomain() {
        return domain;
    }

    public void setDomain(String[] domain) {
        this.domain = domain;
    }
}
