package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.VideoData;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetRecommVideoListResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private boolean statusCode= false;



    private List<VideoData> videoDatas = null;

    public List<VideoData> getVideoDatas() {
        return videoDatas;
    }

    public void setVideoDatas(List<VideoData> videoDatas) {
        this.videoDatas = videoDatas;
    }
}
