package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetCourseListRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetCourseListRequest() {
        super(AppConstant.BASE_URL+REQUEST_TYPE_GET_COURCE_LIST, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
