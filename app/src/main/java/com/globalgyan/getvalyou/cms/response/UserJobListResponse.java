package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.UserJob;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 13/12/16
 *         Module : Valyou.
 */
public class UserJobListResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private boolean statusCode= false;

    private List<UserJob> userJobList = null;

    public List<UserJob> getUserJobList() {
        return userJobList;
    }

    public void setUserJobList(List<UserJob> userJobList) {
        this.userJobList = userJobList;
    }
}
