package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 16/12/16
 *         Module : Valyou.
 */
public class JobDetailsRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public JobDetailsRequest(String userId) {
        super(AppConstant.BASE_URL + REQUEST_TYPE_GET_JOB_DETAILS + userId, RequestTypes.GET );
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
