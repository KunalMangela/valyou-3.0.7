package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Goutam Kumar K. on 17/3/17.
 */

public class SocialMediaSignUpRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     */
    public SocialMediaSignUpRequest() {
        super(AppConstant.BASE_URL + REQUEST_TYPE_SOCIAL_MEDIA_REGISTER, RequestTypes.POST);
    }

    private String email = null;

    private String mobile = null;

    private String username = null;

    private String providerId = null;

    private String providerType = null;

    private JSONObject providerData = null;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public JSONObject getProviderData() {
        return providerData;
    }

    public void setProviderData(JSONObject providerData) {
        this.providerData = providerData;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String fcmToken = null;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public JSONObject getURLEncodedPostdata() {

        JSONObject object = new JSONObject();
        try {
            object.put("displayName", username);
            // object.put("lastName", password);
            object.put("email", email);
            object.put("mobile", mobile);
            object.put("FCMToken",fcmToken);
            // object.put("gender", password);
            //  object.put("username", username);

            JSONObject provider = new JSONObject();
            provider.put("providerType", providerType);
            provider.put("providerId", providerId);
            provider.put("providerData", providerData);

            object.put("provider",provider);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;

    }

/*
    {
        "email": "sample@email.com",
            "provider": {
        "providerType": "facebook",
                "providerId": "some provider id",
                "providerData": {
            "key": "value"
        }
    }
    }*/

}

