package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetRecommendedCourseListRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetRecommendedCourseListRequest() {
        super(AppConstant.BASE_URL+REQUEST_TYPE_GET_RECOMM_COURSE_LIST, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
