package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * Created by Goutam Kumar K. on 6/3/17.
 */

public class GetMeetingDetailsRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public GetMeetingDetailsRequest(String candidateId) {
        super(AppConstant.BASE_URL+ REQUEST_MEETING_DETAILS+candidateId, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
