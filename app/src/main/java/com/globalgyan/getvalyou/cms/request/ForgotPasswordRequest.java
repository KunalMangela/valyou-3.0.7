package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by techniche-android on 17/1/17.
 */

public class ForgotPasswordRequest extends ValYouRequest {
    private String mobile = null;

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * Constructor for GUIRequest.
     *
     */
    public ForgotPasswordRequest() {
        super(AppConstant.BASE_URL+FORGET_PASSWORD, RequestTypes.POST);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        JSONObject object = new JSONObject();
        try {
            object.put("mobileNumber", mobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
