package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Organization;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 27/12/16
 *         Module : Valyou.
 */
public class AddNewOrganisationResponse extends ValYouResponse {
    private boolean statusCode = false;

    private String message = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    public Organization getOrganization() {
        return organisation;
    }

    public void setOrganization(Organization organization) {
        this.organisation = organization;
    }

    private Organization organisation = null;

}
