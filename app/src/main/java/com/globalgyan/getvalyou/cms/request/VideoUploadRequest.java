package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 23/11/16
 *         Module : Valyou.
 */
public class VideoUploadRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public VideoUploadRequest(String userID) {
        //super(AppConstant.BASE_URL + "/user/" + userId + REQUEST_TYPE_VIDEO_UPLOAD, RequestTypes.POST);
        super(AppConstant.BASE_URL + REQUEST_TYPE_VIDEO_UPLOAD+userID, RequestTypes.POST);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
