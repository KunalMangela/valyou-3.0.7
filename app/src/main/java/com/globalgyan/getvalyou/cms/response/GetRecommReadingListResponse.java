package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.ReadingData;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 30/12/16
 *         Module : Valyou.
 */
public class GetRecommReadingListResponse extends ValYouResponse {
    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    private boolean statusCode= false;


    public List<ReadingData> getReadingDatas() {
        return readingDatas;
    }

    public void setReadingDatas(List<ReadingData> readingDatas) {
        this.readingDatas = readingDatas;
    }

    private List<ReadingData> readingDatas = null;

}
