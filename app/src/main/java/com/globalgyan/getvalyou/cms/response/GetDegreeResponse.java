package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Degree;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 28/11/16
 *         Module : Valyou.
 */
public class GetDegreeResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    public List<Degree> getDegreeList() {
        return degreeList;
    }

    public void setDegreeList(List<Degree> degreeList) {
        this.degreeList = degreeList;
    }

    private List<Degree> degreeList = null;

}
