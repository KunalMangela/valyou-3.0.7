package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 29/12/16
 *         Module : Valyou.
 */
public class InterviewListRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     *
     */
    public InterviewListRequest(String userId) {
        super(AppConstant.BASE_URL+REQUEST_TYPE_INTERVIEW_LIST + userId, RequestTypes.GET);
    }

    @Override
    public JSONObject getURLEncodedPostdata() {
        return null;
    }
}
