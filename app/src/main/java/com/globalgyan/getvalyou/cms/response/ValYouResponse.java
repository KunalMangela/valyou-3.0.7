package com.globalgyan.getvalyou.cms.response;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 18/11/16
 *         Module : Valyou.
 */
public abstract  class ValYouResponse {
    public abstract boolean isStatus();

    public abstract void setStatus(boolean status);
}
