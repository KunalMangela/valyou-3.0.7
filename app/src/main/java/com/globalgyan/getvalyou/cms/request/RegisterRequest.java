package com.globalgyan.getvalyou.cms.request;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 24/11/16
 *         Module : Valyou.
 */
public class RegisterRequest extends ValYouRequest {
    /**
     * Constructor for GUIRequest.
     */
    public RegisterRequest() {
        super(AppConstant.BASE_URL + REQUEST_TYPE_REGISTER, RequestTypes.POST);
    }

    private String email = null;

    private String password  = null;

    private String mobile = null;

    private String username = null;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String fcmToken = null;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    @Override
    public JSONObject getURLEncodedPostdata() {

        JSONObject object = new JSONObject();
        try {
            object.put("displayName", username);
           // object.put("lastName", password);
            object.put("email", email);
            object.put("password", password);
            object.put("mobile", mobile);
            object.put("FCMToken",fcmToken);
           // object.put("gender", password);
          //  object.put("username", username);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;

    }
}
