package com.globalgyan.getvalyou.cms.response;

import com.globalgyan.getvalyou.model.Organization;

import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 28/11/16
 *         Module : Valyou.
 */
public class GetOrganizationResponse extends ValYouResponse {
    private boolean statusCode = false;

    @Override
    public boolean isStatus() {
        return statusCode;
    }

    @Override
    public void setStatus(boolean status) {
        this.statusCode = status;
    }

    public List<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
    }

    private List<Organization> organizations = null;

}
