package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.cms.response.GetPositionHeldResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Position;

import java.util.ArrayList;
import java.util.List;

public class EmployerActivity extends AppCompatActivity implements GUICallback {
    private TextView txtNext = null;
    private EditText txtSearch = null;
    private PositionAdapter positionAdapter = null;
    private RecyclerView recyclerView = null;
    private String selected = null;
    List<Position> positions = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nature_of_work);
     //        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.theme_color)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
           // window.setStatusBarColor(ContextCompat.getColor(EmployerActivity.this, R.color.theme_color));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
        try {
            selected = getIntent().getExtras().getString("selected");
        }catch (Exception ex){
            selected = null;
        }

        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(this,this);
        processor.getPositionHeld();

        TextView txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMessage = new Intent();
                setResult(RESULT_CANCELED ,intentMessage);
                finish();
             //   overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });


        txtNext = (TextView) findViewById(R.id.txtNext);
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(EmployerActivity.this));



        positionAdapter = new PositionAdapter(positions,this,selected);
        recyclerView.setAdapter(positionAdapter);
        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(positionAdapter.getStringBuffer())){
                    Intent intentMessage = new Intent();
                    intentMessage.putExtra("INDUSTRIES", positionAdapter.getStringBuffer());
                    setResult(RESULT_OK, intentMessage);

                    if (getParent() == null) {
                        setResult(RESULT_OK, intentMessage);
                    } else {
                        getParent().setResult(RESULT_OK, intentMessage);
                    }
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }else{
                    Toast.makeText(EmployerActivity.this,"Please select one ", Toast.LENGTH_SHORT).show();
                }

            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
                charSequence = charSequence.toString().trim().toLowerCase();

                final List<Position> filteredList = new ArrayList<>();
                boolean itemFound = false;
                for (int i = 0; i < positions.size(); i++) {

                    final String text = positions.get(i).getName().toLowerCase();
                    if (text.contains(charSequence)) {

                        filteredList.add(positions.get(i));
                    }
                }
                if (!itemFound) {
                    Position degree = new Position();
                    degree.setName(charSequence.toString());
                    degree.set_id("new");
                    selected = charSequence.toString();
                    filteredList.add(degree);
                }
                positionAdapter = new PositionAdapter(filteredList,EmployerActivity.this,selected);
                recyclerView.setAdapter(positionAdapter);
                positionAdapter.notifyDataSetChanged();  // data set changed
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intentMessage = new Intent();

        if (getParent() == null) {
            setResult(RESULT_CANCELED, intentMessage);
        } else {
            getParent().setResult(RESULT_CANCELED, intentMessage);
        }
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {

                GetPositionHeldResponse response = (GetPositionHeldResponse) guiResponse;
                if (response != null && response.isStatus() && response.getPositionList() != null) {
                    List<Position> colleges = response.getPositionList();
                    for (int i = 0; i < colleges.size(); i++) {
                        positions.add(colleges.get(i));
                    }
                    positionAdapter = new PositionAdapter(positions, EmployerActivity.this, selected);
                    recyclerView.setAdapter(positionAdapter);
                    positionAdapter.notifyDataSetChanged();  // data set changed
                } else {
                    Toast.makeText(EmployerActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(EmployerActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }
}
