package com.globalgyan.getvalyou;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Globalgyan on 06-12-2017.
 */

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.MyViewHolder> {

    private Context mContext;
    private static List<String> user;
    private static List<String> points;

    public LeaderboardAdapter(Context mContext, List<String> user, List<String> points) {
        this.mContext=mContext;
        this.user=user;
        this.points=points;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView name,points;
        ImageView trophy;

        public MyViewHolder(View view) {
            super(view);

            name = (FontTextView) view.findViewById(R.id.name);
            points = (FontTextView) view.findViewById(R.id.points);
            trophy=(ImageView)view.findViewById(R.id.trophy_image);
        }
    }

    @Override
    public LeaderboardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leaderboard_items, parent, false);



        return new LeaderboardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LeaderboardAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(position+1+". "+user.get(position));
        holder.points.setText(points.get(position));
        if(position==0){
            holder.trophy.setVisibility(View.VISIBLE);
        }else {
            holder.trophy.setVisibility(View.INVISIBLE);

        }
    }
    @Override
    public int getItemCount() {
        return user.size();
    }
}


