package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.cms.request.UpdateProfileRequest;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by NaNi on 15/10/17.
 */

public class ListEducationActivity extends AppCompatActivity implements GUICallback {

    private RecyclerView educationalDetailsList = null;
    private EducationalDetailsListAdapter educationalDetailsListAdapter;
    private DataBaseHelper dataBaseHelper = null;
    String kya,save;
    AppCompatButton next,edumore;
    ConnectionUtils connectionUtils;
    boolean flag_netcheck=false;
    Dialog progress_profile;
    String candidateId;
    Typeface tf1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.educational_details_list_fragment);
        next=(AppCompatButton)findViewById(R.id.edulistnext);
        edumore=(AppCompatButton)findViewById(R.id.edulistmore);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
        tf1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        edumore.setTypeface(tf1);
        next.setTypeface(tf1);
        connectionUtils=new ConnectionUtils(ListEducationActivity.this);

        createProgress();
        try{
            kya=getIntent().getExtras().getString("kya");
            save=getIntent().getExtras().getString("save");
        }catch (Exception e){

        }


        try {
            if (save.equalsIgnoreCase("yes")) {
                next.setText("SAVE");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        dataBaseHelper = new DataBaseHelper(this);
        educationalDetailsList = (RecyclerView) findViewById(R.id.educationalDetails);
        educationalDetailsList.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ListEducationActivity.this, ProfileActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            }
        });

        findViewById(R.id.edulistmore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it= new Intent(ListEducationActivity.this, EducationalDetailsActivity.class);
                it.putExtra("kya",kya);
                startActivity(it);
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });

        findViewById(R.id.edulistnext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                findViewById(R.id.edulistnext).setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        findViewById(R.id.edulistnext).setEnabled(true);
                    }
                }.start();
                if (!TextUtils.isEmpty(dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS,DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT))) {
                    long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                    if (selectedid > 0) {
                        dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                    }else{
                        dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                    }

                     candidateId = PreferenceUtils.getCandidateId(ListEducationActivity.this);
                    if (!TextUtils.isEmpty(candidateId)) {
            /*UpdateProfileRequest request = new UpdateProfileRequest(candidateId, getActivity());
            new AsyncTaskExecutor<ValYouRequest, Void, ValYouResponse>().execute(
                    new RequestProcessorPut(getActivity(),
                            ProfileFragment.this, true), request);*/

                        if(connectionUtils.isConnectionAvailable()){

                            progress_profile.show();
                            final CheckInternetTask t919=new CheckInternetTask();
                            t919.execute();

                            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {

                                    t919.cancel(true);
                                    progress_profile.dismiss();
                                    if (flag_netcheck) {
                                        flag_netcheck = false;
                                        UpdateProfileRequest request = new UpdateProfileRequest(candidateId, ListEducationActivity.this);

                                        JsonParser jsonParser = new JsonParser();
                                        JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());

                                        Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ListEducationActivity.this, ListEducationActivity.this);
                                        processor.updateProfile(candidateId, gsonObject);
                                        Intent it = new Intent(ListEducationActivity.this,ProfileActivity.class);
                                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(it);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);




                                    }else {

                                        flag_netcheck=false;
                                        Toast.makeText(ListEducationActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }.start();


                        }else {
                            Toast.makeText(ListEducationActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                        }

                    }

                } else {

                    ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
                    if (professionals != null && professionals.size() > 0) {
                       Intent it= new Intent(ListEducationActivity.this, ListProfessionalDetailsActivity.class);
                        it.putExtra("save","no");
                        it.putExtra("kya",kya);
                        startActivity(it);
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }else {
                        Intent it= new Intent(ListEducationActivity.this, ProfessionalDetailsActivity.class);
                        it.putExtra("kya",kya);
                        startActivity(it);
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


                    }
                }
            }
        });


    }

    public void createProgress() {
        progress_profile = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        progress_profile.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress_profile.setContentView(R.layout.planet_loader);
        Window window = progress_profile.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progress_profile.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progress_profile.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progress_profile.setCancelable(false);
   //     progress_profile.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadEducationData();
    }

    private void loadEducationData() {
        ArrayList<EducationalDetails> arrayList = dataBaseHelper.getEducationalDetails();
        if (arrayList != null && arrayList.size() > 0) {
            educationalDetailsListAdapter = new EducationalDetailsListAdapter(this, arrayList);
            educationalDetailsList.setAdapter(educationalDetailsListAdapter);
        }else{
            Intent it=new Intent(ListEducationActivity.this, EducationalDetailsActivity.class);
            it.putExtra("kya","stud");
            startActivity(it);
            finish();
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

        }
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {

    }



    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)ListEducationActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ListEducationActivity.this, ProfileActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
    }
}
