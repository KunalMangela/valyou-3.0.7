package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.phillipcalvin.iconbutton.IconButton;
import com.sackcentury.shinebuttonlib.ShineButton;

import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.client.ClientProtocolException;

/**
 * Created by NaNi on 22/09/17.
 */

public class CheckP extends AppCompatActivity {

    TextView score,bestscoreis;
    ImageView na;
    String qi,ci,p;
    int q,c;
            float d,d1,d2;
    int percentage;
    boolean click=false;
    SimpleRatingBar s1,s2,s3;
    TextView feedback_button;

    int count=0;
    public static HttpURLConnection urlConnection2;
    public static int code2;
    static InputStream is = null;
    public static Context context;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;
    TextView textcompleted;
    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;

    RelativeLayout relative;
    BatteryManager bm;
    int batLevel;
    boolean isPowerSaveMode;
    TextView textcompletedheading;
    PowerManager pm;
    public static String  tokenis="";
    public static String json=" ";
    AppConstant appConstant;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance);
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        appConstant = new AppConstant(this);
        textcompleted=(TextView)findViewById(R.id.textcompleted);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        na=(ImageView) findViewById(R.id.nasgn);
        textcompletedheading=(TextView)findViewById(R.id.textcompletedheading);
        bestscoreis=(TextView)findViewById(R.id.bestscoreis);
        context=CheckP.this;
        qi= getIntent().getExtras().getString("qt");
        ci=getIntent().getExtras().getString("cq");
        p=getIntent().getExtras().getString("po");
        tokenis=new PrefManager(CheckP.this).getToken();
        score=(TextView)findViewById(R.id.scoreis);
        feedback_button=(TextView)findViewById(R.id.feedbcak_button);

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        Typeface tf2 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        feedback_button.setTypeface(tf2);
        score.setTypeface(tf2);
        textcompleted.setTypeface(tf2);
        bestscoreis.setTypeface(tf2);
        textcompletedheading.setTypeface(tf2);

        relative =(RelativeLayout)findViewById(R.id.relative);
        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);
        String from_where=new PrefManager(CheckP.this).getNav();
        if(from_where.equalsIgnoreCase("learn")){
         //   new SendLessonStartProgress().execute();
        }

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            startanim();
        }
        /*feedback_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PrefManager(CheckP.this).isclicked("yes");

                new PrefManager(CheckP.this).isResultscreenpause("no");

                Intent it = new Intent(CheckP.this,FeedbackActivity.class);
              *//*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*//*
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        });*/



        relative.setEnabled(false);
        relative.setClickable(false);
        c= Integer.parseInt(ci);
        q=Integer.parseInt(qi);

        percentage=c*100;
        percentage=percentage/q;

        Log.e("percentage","" +percentage);

        score.setText("SCORE: "+c+"/"+q);
        bestscoreis.setText("BEST SCORE: "+q+"/"+q);

        na.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    na.setBackgroundColor(Color.parseColor("#33ffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            na.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }*/
                return false;
            }
        });
        na.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String from_where=new PrefManager(CheckP.this).getNav();
                if(from_where.equalsIgnoreCase("learn")){
                    Log.e("navigation","learn");
                    new PrefManager(CheckP.this).isclicked("yes");

                    new PrefManager(CheckP.this).isResultscreenpause("no");

                    Intent it = new Intent(CheckP.this,ModulesActivity.class);
              /*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*/
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

                }else {
                    new PrefManager(CheckP.this).isclicked("yes");
                    Log.e("navigation","ass");
                    new PrefManager(CheckP.this).isResultscreenpause("no");

                    Intent it = new Intent(CheckP.this,HomeActivity.class);
               it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

                }


            }
        });

        feedback_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String from_where=new PrefManager(CheckP.this).getNav();
                if(from_where.equalsIgnoreCase("learn")){
                    Log.e("navigation","learn");
                    new PrefManager(CheckP.this).isclicked("yes");

                    new PrefManager(CheckP.this).isResultscreenpause("no");

                    Intent it = new Intent(CheckP.this,ModulesActivity.class);
              /*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*/
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

                }else {
                    new PrefManager(CheckP.this).isclicked("yes");
                    Log.e("navigation","ass");
                    new PrefManager(CheckP.this).isResultscreenpause("no");

                    Intent it = new Intent(CheckP.this,HomeActivity.class);
                    it.putExtra("tabs","normal");
                    it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

                }


            }
        });

        textcompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String from_where=new PrefManager(CheckP.this).getNav();
                if(from_where.equalsIgnoreCase("learn")){
                    Log.e("navigation","learn");
                    new PrefManager(CheckP.this).isclicked("yes");

                    new PrefManager(CheckP.this).isResultscreenpause("no");

                    Intent it = new Intent(CheckP.this,ModulesActivity.class);
              /*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*/
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

                }else {
                    new PrefManager(CheckP.this).isclicked("yes");
                    Log.e("navigation","ass");
                    new PrefManager(CheckP.this).isResultscreenpause("no");

                    Intent it = new Intent(CheckP.this,HomeActivity.class);
                    it.putExtra("tabs","normal");
                    it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

                }


            }
        });


        s1 = (SimpleRatingBar) findViewById(R.id.sm);
        s2 = (SimpleRatingBar) findViewById(R.id.sm1);
        s3 = (SimpleRatingBar) findViewById(R.id.sm2);

        s1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        s2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        s3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        s1.setEnabled(false);
        s2.setEnabled(false);
        s3.setEnabled(false);

        s1.setClickable(false);
        s2.setClickable(false);
        s3.setClickable(false);

        float dd = percentage*3;

        d=dd/100;
        //  sm.setRating(2.7f);
        Log.e("rating",""+d);
        if((d>0.0f)&&(d<1.01f)){
            Log.e("rating","first");
            ObjectAnimator animr = ObjectAnimator.ofFloat(s1, "rating", 0f, d);
            animr.setDuration(2000);
            animr.start();
        }
        if((d>1.0f)&&(d<2.01f)){
            Log.e("rating","second");
            ObjectAnimator animr1 = ObjectAnimator.ofFloat(s1, "rating", 0f, 1.0f);
            animr1.setDuration(2000);
            animr1.start();
            d1=d-1.0f;
            new CountDownTimer(2000, 2000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    ObjectAnimator animr2 = ObjectAnimator.ofFloat(s2, "rating", 0f, d1);
                    animr2.setDuration(2000);
                    animr2.start();
                }
            }.start();

        }
        if((d>2.0f)&&(d<3.01f)){
            Log.e("rating","third");
            ObjectAnimator animr3 = ObjectAnimator.ofFloat(s1, "rating", 0f, 1.0f);
            animr3.setDuration(2000);
            animr3.start();
            d1=d-1.0f;
            d2=d-2.0f;
            new CountDownTimer(2000, 2000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    ObjectAnimator animr4 = ObjectAnimator.ofFloat(s2, "rating", 0f, d1);
                    animr4.setDuration(2000);
                    animr4.start();

                    new CountDownTimer(2000, 2000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            ObjectAnimator animr5 = ObjectAnimator.ofFloat(s3, "rating", 0f, d2);
                            animr5.setDuration(2000);
                            animr5.start();
                        }
                    }.start();

                }
            }.start();
        }




    }


    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        Log.e("checkp","resume");
        super.onResume();

    }

    @Override
    protected void onPause() {
        Log.e("checkp","pause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.e("checkp","pause");

        super.onDestroy();
        if(new PrefManager(CheckP.this).getclick().equalsIgnoreCase("yes")){
            new PrefManager(CheckP.this).isResultscreenpause("no");
            new PrefManager(CheckP.this).isclicked("no");
        }else {
            new PrefManager(CheckP.this).isResultscreenpause("yes");
            new PrefManager(CheckP.this).isclicked("no");

        }
    }



    public void startanim()
    {
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }


    private static class SendLessonStartProgress extends AsyncTask<Integer, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        ///Authorization
        @Override
        protected String doInBackground(Integer... urlkk) {
            String result1 = "";
            try {

                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(context))
                        .object("course_id", Integer.valueOf(new PrefManager(context).getCourseid()))
                        .object("lesson_id", 0)
                        .object("assessment_id", Integer.valueOf(new PrefManager(context).getinnermoduleid()))
                        .object("order_id", Integer.valueOf(new PrefManager(context).getorderid()))
                        .object("status", "completed")
                        .object("lg_id",new PrefManager(context).getlgid())
                        .object("certification_id",new PrefManager(context).getcertiid())



                        .build().toString();

                try {
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_update_user_progress");
                    urlConnection2 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection2.setDoOutput(true);
                    urlConnection2.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection2.setRequestProperty("Content-Type", "application/json");
                    urlConnection2.setRequestProperty("Authorization", "Bearer " + tokenis);

                    urlConnection2.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection2.getOutputStream());
                    wr.write(jn);
                    Log.e("resp_data",jn);
                    wr.flush();
                    is = urlConnection2.getInputStream();
                    code2 = urlConnection2.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                } catch (ClientProtocolException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection2 != null)
                    urlConnection2.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {
                Log.e("myvalue","postans 200");
            }
            else{
                Log.e("myvalue","postans not 200");

            }
        }
    }
    private static String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

}
