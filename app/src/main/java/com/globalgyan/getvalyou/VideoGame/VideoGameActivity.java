package com.globalgyan.getvalyou.VideoGame;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialcamera.MaterialCamera;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.Activity_Fib;
import com.globalgyan.getvalyou.CheckP;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.ListeningGame.MAQ;
import com.globalgyan.getvalyou.MasterSlaveGame.MasterSlaveGame;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.customwidget.FlowLayout;
import com.globalgyan.getvalyou.fragments.Performance;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.helper.VideoCompressor;
import com.globalgyan.getvalyou.model.QuestionModel;
import com.globalgyan.getvalyou.model.Video;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.kaopiz.kprogresshud.KProgressHUD;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

import cz.msebera.android.httpclient.client.ClientProtocolException;
import de.morrox.fontinator.FontTextView;


public class VideoGameActivity extends AppCompatActivity {
    SimpleDateFormat df1;
    Calendar c;
    String path="";
    QuestionModel qsmodel;
    ArrayList<QuestionModel> qs=new ArrayList<>();
    ConnectionUtils connectionUtils;
   // KProgressHUD qs_progress,postdataprogress;

    Dialog qs_progress,postdataprogress;

    URL ppp;
    boolean loadvideo=false;
    ArrayList<String> qidj,ansj,stj,endj,test;
    String uidd,adn,ceid,gdid,game,cate,tgid,sid,aid,assessg;
    RelativeLayout ques_and_record_btn_layout,options_screen,videopreviewlayout;
    FontTextView q_no_is,ques_is,preview,upload,retry;
    ImageView record_start_btn;
    HttpURLConnection urlConnection;
    String newPath="";
    private final static int CAMERA_RQ = 6969;
    String jn="";
    static InputStream is = null;
    int code1,code2,code3,optionsize;
    static String json = "", jso = "", Qus = "",Qs="", token = "", token_type = "", quesDis = "";
    int counter_ques=0,s3_exc_count=0;
    File saveFolder;
    VideoView video_preview;
    MediaController mediaController1;
    Button exitpreview;
    NotificationManager mNotifyManager;
    private Builder build;
    int id = 1;
    long timeInMillisec;
    int progg;
    public static int pr;
    ArrayList<String> s3_paths=new ArrayList<>();
    ArrayList<String> compressed_paths=new ArrayList<>();
    ArrayList<String> startDatelist=new ArrayList<>();
    ArrayList<String> enddatelist=new ArrayList<>();

   Dialog alertDialog;

    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2, astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;
    ImageView close;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_game);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        appConstant = new AppConstant(this);
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        ques_and_record_btn_layout=(RelativeLayout)findViewById(R.id.ques_and_record_btn_layout);
        q_no_is=(FontTextView)findViewById(R.id.q_no_is);
        preview=(FontTextView)findViewById(R.id.previewbtn);
        upload=(FontTextView)findViewById(R.id.upload);
        retry=(FontTextView)findViewById(R.id.retry);
        exitpreview=(Button)findViewById(R.id.exitpreview);
        ques_and_record_btn_layout.setVisibility(View.VISIBLE);
        options_screen=(RelativeLayout)findViewById(R.id.options_screen);
        videopreviewlayout=(RelativeLayout)findViewById(R.id.videopreviewlayout);
        saveFolder = new File(Environment.getExternalStorageDirectory()+"/Movies/valYou/");
        video_preview=(VideoView)findViewById(R.id.videoview_preview);
        ques_is=(FontTextView)findViewById(R.id.ques_is);
        record_start_btn=(ImageView)findViewById(R.id.record_start_btn);
        close = (ImageView)findViewById(R.id.backbtnis_vdogame);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                        VideoGameActivity.this, R.style.DialogTheme);
                LayoutInflater inflater = LayoutInflater.from(VideoGameActivity.this);
                View dialogView = inflater.inflate(R.layout.quit_warning, null);

                dialogBuilder.setView(dialogView);


                alertDialog = dialogBuilder.create();



                alertDialog.setCancelable(false);
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

                alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                alertDialog.show();*/

                alertDialog = new Dialog(VideoGameActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialog.setContentView(R.layout.quit_warning);
                Window window = alertDialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.CENTER;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                alertDialog.setCancelable(false);

                alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

                final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
                final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                    astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                    astro_outx.setDuration(300);
                    astro_outx.setInterpolator(new LinearInterpolator());
                    astro_outx.setStartDelay(0);
                    astro_outx.start();
                    astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                    astro_outy.setDuration(300);
                    astro_outy.setInterpolator(new LinearInterpolator());
                    astro_outy.setStartDelay(0);
                    astro_outy.start();



                    alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                    alien_outx.setDuration(300);
                    alien_outx.setInterpolator(new LinearInterpolator());
                    alien_outx.setStartDelay(0);
                    alien_outx.start();
                    alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                    alien_outy.setDuration(300);
                    alien_outy.setInterpolator(new LinearInterpolator());
                    alien_outy.setStartDelay(0);
                    alien_outy.start();
                }
                final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        no.setEnabled(false);
                        yes.setEnabled(false);
                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                            astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                            astro_inx.setDuration(300);
                            astro_inx.setInterpolator(new LinearInterpolator());
                            astro_inx.setStartDelay(0);
                            astro_inx.start();
                            astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                            astro_iny.setDuration(300);
                            astro_iny.setInterpolator(new LinearInterpolator());
                            astro_iny.setStartDelay(0);
                            astro_iny.start();

                            alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                            alien_inx.setDuration(300);
                            alien_inx.setInterpolator(new LinearInterpolator());
                            alien_inx.setStartDelay(0);
                            alien_inx.start();
                            alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                            alien_iny.setDuration(300);
                            alien_iny.setInterpolator(new LinearInterpolator());
                            alien_iny.setStartDelay(0);
                            alien_iny.start();
                        }
                        new CountDownTimer(400, 400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                alertDialog.dismiss();
                            }
                        }.start();


                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        yes.setEnabled(false);

                        no.setEnabled(false);
                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                            astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                            astro_inx.setDuration(300);
                            astro_inx.setInterpolator(new LinearInterpolator());
                            astro_inx.setStartDelay(0);
                            astro_inx.start();
                            astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                            astro_iny.setDuration(300);
                            astro_iny.setInterpolator(new LinearInterpolator());
                            astro_iny.setStartDelay(0);
                            astro_iny.start();

                            alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                            alien_inx.setDuration(300);
                            alien_inx.setInterpolator(new LinearInterpolator());
                            alien_inx.setStartDelay(0);
                            alien_inx.start();
                            alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                            alien_iny.setDuration(300);
                            alien_iny.setInterpolator(new LinearInterpolator());
                            alien_iny.setStartDelay(0);
                            alien_iny.start();
                        }
                        new CountDownTimer(400, 400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                alertDialog.dismiss();

                                Intent it = new Intent(VideoGameActivity.this,HomeActivity.class);
                                it.putExtra("tabs","normal");
                                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(it);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                finish();
                            }
                        }.start();

                    }
                });


                try {
                    alertDialog.show();
                }catch (Exception e){}


            }
        });

        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);

        qsmodel=new QuestionModel();
        c = Calendar.getInstance();
        connectionUtils=new ConnectionUtils(VideoGameActivity.this);
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        qs_progress = KProgressHUD.create(VideoGameActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);



        qs_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        qs_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qs_progress.setContentView(R.layout.planet_loader);
        Window window = qs_progress.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        qs_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        qs_progress.setCancelable(false);
        GifView gifView1 = (GifView) qs_progress.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();



//        postdataprogress = KProgressHUD.create(VideoGameActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Uploading Video...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);


        postdataprogress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        postdataprogress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        postdataprogress.setContentView(R.layout.planet_loader);
        Window window2 = postdataprogress.getWindow();
        WindowManager.LayoutParams wlp2 = window2.getAttributes();
        wlp2.gravity = Gravity.CENTER;
        wlp2.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window2.setAttributes(wlp2);
        postdataprogress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        postdataprogress.setCancelable(false);
        GifView gifView3 = (GifView) postdataprogress.findViewById(R.id.loader);
        gifView3.setVisibility(View.VISIBLE);
        gifView3.play();
        gifView3.setGifResource(R.raw.loader_planet);
        gifView3.getGifResource();



        stj = new ArrayList<>();
        endj = new ArrayList<>();
        try{
            uidd= getIntent().getExtras().getString("uidg");
            adn= getIntent().getExtras().getString("adn");
            ceid= getIntent().getExtras().getString("ceid");
            gdid= getIntent().getExtras().getString("gdid");
            game= getIntent().getExtras().getString("game");
            cate= getIntent().getExtras().getString("category");
          //  startTime= Long.parseLong(getIntent().getExtras().getString("timeis"));
            tgid = getIntent().getExtras().getString("tgid");
            aid=getIntent().getExtras().getString("aid");
            assessg=getIntent().getExtras().getString("assessgroup");
            token = getIntent().getExtras().getString("token");


            /*Log.e("timeis",String.valueOf(startTime));
            if(adn.equalsIgnoreCase("finance")){
                afbg.setBackgroundResource(R.drawable.green_gradient);
                bgcolor=R.drawable.green_gradient;
            }else
            if(adn.equalsIgnoreCase("Listening")){
                afbg.setBackgroundResource(R.drawable.bluishgreen_gradient);
                bgcolor=R.drawable.bluishgreen_gradient;

            }else
            if(adn.equalsIgnoreCase("Verbal Ability")){
                afbg.setBackgroundResource(R.drawable.pinkishvoilet_gradient);
                bgcolor=R.drawable.pinkishvoilet_gradient;

            }else
            if(adn.equalsIgnoreCase("Listening skills")){
                afbg.setBackgroundResource(R.drawable.bluishgreen_gradient);
                bgcolor=R.drawable.bluishgreen_gradient;

            }else {
                afbg.setBackgroundResource(R.drawable.yellow_gradient);
                bgcolor=R.drawable.yellow_gradient;

            }*/
        }catch (Exception ex){

        }


        record_start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchCamera();
            }
        });

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preview.setEnabled(false);
                preview.setClickable(false);

                retry.setEnabled(false);
                retry.setClickable(false);

                upload.setEnabled(false);
                upload.setClickable(false);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        preview.setEnabled(true);
                        preview.setClickable(true);

                        retry.setEnabled(true);
                        retry.setClickable(true);

                        upload.setEnabled(true);
                        upload.setClickable(true);
                    }
                }.start();
                try {
                    mediaController1.hide();
                }catch (Exception e){

                }
                videopreviewlayout.setVisibility(View.VISIBLE);
                ques_and_record_btn_layout.setVisibility(View.GONE);
                options_screen.setVisibility(View.GONE);
                loadvideoview();
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                preview.setEnabled(false);
                preview.setClickable(false);

                retry.setEnabled(false);
                retry.setClickable(false);

                upload.setEnabled(false);
                upload.setClickable(false);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        preview.setEnabled(true);
                        preview.setClickable(true);

                        retry.setEnabled(true);
                        retry.setClickable(true);

                        upload.setEnabled(true);
                        upload.setClickable(true);
                    }
                }.start();

                ques_and_record_btn_layout.setVisibility(View.VISIBLE);
                options_screen.setVisibility(View.GONE);
                videopreviewlayout.setVisibility(View.GONE);
                compressed_paths.add(counter_ques,path);
                final String then = df1.format(c.getTime()).toString();
                enddatelist.add(then);
                counter_ques++;

                if(counter_ques==qs.size()){
                    //showdialogue
                    if((postdataprogress!=null)&&(!postdataprogress.isShowing())){
                        postdataprogress.show();
                    }
                    postdataprogress.show();
                    s3taskStarted();
                }else {
                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                        startPlanetanimation();
                    }
                    setQuestions();
                }
                //uploadtoS3(path);
         //       setQuestions();
                //compressiontask(path);
                //compressiontask(path);
               // onCompress(path);
            }
        });
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                preview.setEnabled(false);
                preview.setClickable(false);

                retry.setEnabled(false);
                retry.setClickable(false);

                upload.setEnabled(false);
                upload.setClickable(false);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        preview.setEnabled(true);
                        preview.setClickable(true);

                        retry.setEnabled(true);
                        retry.setClickable(true);

                        upload.setEnabled(true);
                        upload.setClickable(true);
                    }
                }.start();
                try {
                    mediaController1.hide();
                }catch (Exception e){

                }
                launchCamera();
                ques_and_record_btn_layout.setVisibility(View.GONE);
                options_screen.setVisibility(View.GONE);
                videopreviewlayout.setVisibility(View.GONE);
            }
        });

        exitpreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mediaController1.hide();
                }catch (Exception e){

                }
                video_preview=null;
                mediaController1=null;
                options_screen.setVisibility(View.VISIBLE);
                videopreviewlayout.setVisibility(View.GONE);
                ques_and_record_btn_layout.setVisibility(View.GONE);

            }
        });
        new GetQues().execute();
    }

    private void s3taskStarted() {
            if(s3_exc_count==compressed_paths.size()){
                //dismiss dialogue
                //postAnswers
                craeteJsonforPost();

            }else {
                uploadtoS3(compressed_paths.get(s3_exc_count));
        }
    }

    private void craeteJsonforPost() {



            try {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < qs.size(); i++) {
                    QuestionModel questionModel=qs.get(i);
                    JSONObject student = new JSONObject();
                    student.put("U_Id", uidd);
                    student.put("S_Id", sid);
                    student.put("Q_Id", questionModel.getQ_id());
                    student.put("TG_Id", tgid);
                    student.put("GD_Id", gdid);
                    student.put("CE_Id", ceid);
                    student.put("QT_Id", questionModel.getQT_id());
                    student.put("Ans_Url", s3_paths.get(i));
                    student.put("Start_Time", startDatelist.get(i));
                    student.put("End_Time", enddatelist.get(i));
                    jsonArray.put(student);
                }

                JSONObject studentsObj = new JSONObject();
                studentsObj.put("data", jsonArray);
                jn=studentsObj.toString();
                Log.e("jsonpostSTring",jn);
                new PostAns().execute();
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    private class PostAns extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {

                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/addvideo_answers");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code2 = urlConnection.getResponseCode();


                } catch (UnsupportedEncodingException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(postdataprogress.isShowing()){
                                postdataprogress.dismiss();
                            }
                            Toast.makeText(VideoGameActivity.this,"Error while submitting video",Toast.LENGTH_LONG).show();

                        }
                    });
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(postdataprogress.isShowing()){
                                postdataprogress.dismiss();
                            }
                            Toast.makeText(VideoGameActivity.this,"Error while submitting video",Toast.LENGTH_LONG).show();

                        }
                    });
                    e.printStackTrace();
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(postdataprogress.isShowing()){
                                postdataprogress.dismiss();
                            }
                            Toast.makeText(VideoGameActivity.this,"Error while submitting video",Toast.LENGTH_LONG).show();

                        }
                    });

                    e.printStackTrace();
                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response", responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {

                new Ends().execute();
            }
            else{
               if(postdataprogress.isShowing()){
                   postdataprogress.dismiss();
               }
               Toast.makeText(VideoGameActivity.this,"Error while submitting video",Toast.LENGTH_LONG).show();
                Intent it = new Intent(VideoGameActivity.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        }
    }
    private class Ends extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            if(new PrefManager(VideoGameActivity.this).getGetFirsttimegame().equalsIgnoreCase("first")){
                new PrefManager(VideoGameActivity.this).saveFirsttimeGame("last");
            }else if(new PrefManager(VideoGameActivity.this).getGetFirsttimegame().equalsIgnoreCase("last")){

            }else {
                new PrefManager(VideoGameActivity.this).saveFirsttimeGame("first");
            }


        }

        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/EndSession");*/

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("S_Id", sid)
                            .build().toString();
                   /* StringEntity se = new StringEntity(jn.toString());

                    Log.e("Requesttres", se + "");
                    httpPost.addHeader("Authorization", "Bearer " + token);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    code3=httpResponse.getStatusLine().getStatusCode();
                    HttpEntity httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    Log.e("FKJ", httpResponse + "CHARAN" + is);*/


                    URL urlToRequest = new URL(AppConstant.Ip_url + "Player/EndSession");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                }
                catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(postdataprogress.isShowing()){
                                postdataprogress.dismiss();
                            }
                            Toast.makeText(VideoGameActivity.this,"Error while submitting video",Toast.LENGTH_LONG).show();

                        }
                    });
                    e.printStackTrace();
                }
                if(code3==200){
                  /*  try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "iso-8859-1"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        is.close();

                        Qs = sb.toString();


                        Log.e("quesfefff", Qus);

                    } catch (Exception e) {
                        e.getMessage();
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                }



            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }*/
                    String responseString = readStream1(is);
                    Log.e("Responseendtask", responseString);
                    Log.e("Responsecodeendtask", String.valueOf(code3));
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code3!=200){
                if(postdataprogress.isShowing()){
                    postdataprogress.dismiss();
                }
                Toast.makeText(VideoGameActivity.this,"Error while submitting video",Toast.LENGTH_LONG).show();

                Intent it = new Intent(VideoGameActivity.this,HomeActivity.class);
               it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }else {
                if(postdataprogress.isShowing()){
                    postdataprogress.dismiss();
                }
                Intent it = new Intent(VideoGameActivity.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
             /*   Intent it = new Intent(Activity_Fib.this, CheckP.class);
                it.putExtra("qt", questions.length() + "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.putExtra("assessg",assessg);
                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                finish();*/
            }

        }
    }

    private String readStream1(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qs = sb.toString();


            Log.e("JSONStrr", Qs);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qs;
    }

    private void uploadtoS3(String path) {
     new S3Example().execute(path);
    }
    private class S3Example extends AsyncTask<String,Void,Void> {
        //            int iok=0;
//        S3Example(int url){
//            this.iok=url;
//        }
        @Override
        protected Void doInBackground(String... params) {


                String ACCESS_KEY ="AKIAJQTIZUYTX2OYTBJQ";
                String SECRET_KEY = "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";
            String convertpath=params[0];
            String[] mypath=convertpath.split("/");
            String [] lastnamewithextension=mypath[mypath.length-1].split("\\.");
            String uniquename=lastnamewithextension[0];
                File video = new File(params[0]);
                    Random r = new Random();
                    int i1 = r.nextInt(10005 - 30) + 28;
                    AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY,SECRET_KEY));
                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/contribution/videos/"+uniquename+i1+"-0k.mp4",video);
                    PutObjectResult putResponse = s3Client.putObject(pp);
                    String prurl= s3Client.getResourceUrl("valyouinputbucket","valyou/contribution/videos/"+uniquename+i1+"-0k.mp4");
                     ppp= s3Client.getUrl("valyouinputbucket","valyou/contribution/videos/"+uniquename+i1+"-0k.mp4");
                    Log.e("c RESPOMSE",ppp.toString());
                    return  null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            s3_paths.add(ppp.toString());
            s3_exc_count++;
            s3taskStarted();

        }
    }

    public void loadvideoview(){
        Log.e("loadview","yes");
        try{
            loadvideo=true;
            video_preview=(VideoView)findViewById(R.id.videoview_preview);
            mediaController1=new MediaController(this);
            mediaController1.setAnchorView(video_preview);
            // Get the URL from String VideoURL
            video_preview.setMediaController(mediaController1);
            mediaController1.setVisibility(View.VISIBLE);

            final Uri videouri = Uri.parse(path);

            new CountDownTimer(200, 200) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    video_preview.setVideoURI(videouri);
                }
            }.start();

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        video_preview.requestFocus();
        video_preview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {

                new CountDownTimer(200, 200) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if(loadvideo) {
                            video_preview.start();
                            mediaController1.show(0);
                        }
                    }
                }.start();


            }
        });
        video_preview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // notifyDataSetChanged();

            }
        });

    }

    @SuppressLint({"MissingPermission", "ResourceType"})
    private void launchCamera() {
        QuestionModel qs_t_model=qs.get(counter_ques);
        new MaterialCamera(this)                               // Constructor takes an Activity
                .autoSubmit(true)                                 // Whether or not user is allowed to playback videos after recording. This can affect other things, discussed in the next section.
                .saveDir(saveFolder)                               // The folder recorded videos are saved to
                .primaryColorAttr(Color.parseColor("#ceffffff"))             // The theme color used for the camera, defaults to colorPrimary of Activity in the constructor
                .showPortraitWarning(true)                         // Whether or not a warning is displayed if the user presses record in portrait orientation
                .defaultToFrontFacing(true)                       // Whether or not the camera will initially show the front facing camera
                .retryExits(false)                                 // If true, the 'Retry' button in the playback screen will exit the camera instead of going back to the recorder
                .restartTimerOnRetry(false)
                .videoEncodingBitRate(204000)
                // If true, the countdown timer is reset to 0 when the user taps 'Retry' in playback
                //.videoEncodingBitRate(224000)                     // Sets a custom bit rate for video recording.
                .continueTimerInPlayback(false)                    // If true, the countdown timer will continue to go down during playback, rather than pausing.
                //.videoEncodingBitRate(180000)                     // Sets a custom bit rate for video recording.
                .audioEncodingBitRate(50000)                       // Sets a custom bit rate for audio recording.
                .videoFrameRate(20)                                // Sets a custom frame rate (FPS) for video recording.
                .qualityProfile(MaterialCamera.QUALITY_HIGH)       // Sets a quality profile, manually setting bit rates or frame rates with other settings will overwrite individual quality profile settings
                .videoPreferredHeight(720)                         // Sets a preferred height for the recorded video output.
                .videoPreferredAspect(3f / 3f)                     // Sets a preferred aspect ratio for the recorded video output.
                .maxAllowedFileSize(1024 * 1024 * 30)               // Sets a max file size of 5MB, recording will stop if file reaches this limit. Keep in mind, the FAT file system has a file size limit of 4GB.
                .iconRecord(R.drawable.red_dot)        // Sets a custom icon for the button used to start recording
                .iconStop(R.drawable.mcam_action_stop)             // Sets a custom icon for the button used to stop recording
                .iconFrontCamera(R.drawable.mcam_camera_front)     // Sets a custom icon for the button used to switch to the front camera
                .iconRearCamera(R.drawable.mcam_camera_rear)       // Sets a custom icon for the button used to switch to the rear camera
                .iconPlay(R.drawable.evp_action_play)              // Sets a custom icon used to start playback
                //.iconPause(R.drawable.evp_action_pause)            // Sets a custom icon used to pause playback
               // .iconRestart(R.drawable.evp_action_restart)        // Sets a custom icon used to restart playback
                //.labelRetry(R.string.mcam_retry)                   // Sets a custom button label for the button used to retry recording, when available
               // .labelConfirm(R.string.mcam_use_video)             // Sets a custom button label for the button used to confirm/submit a recording
                .autoRecordWithDelaySec(3)                         // The video camera will start recording automatically after a 5 second countdown. This disables switching between the front and back camera initially.
                .autoRecordWithDelayMs(3000)                      // Same as the above, expressed with milliseconds instead of seconds.
                .audioDisabled(false)
        .autoSubmit(true).allowRetry(false)// Set to true to record video without any audio.
                .countdownSeconds(60)
                .start(CAMERA_RQ);

        /*AnncaConfiguration.Builder videoLimited = new AnncaConfiguration.Builder(VideoGameActivity.this, CAMERA_RQ);
        videoLimited.setMediaAction(AnncaConfiguration.MEDIA_ACTION_VIDEO);
        videoLimited.setMediaQuality(AnncaConfiguration.MEDIA_QUALITY_MEDIUM);
        videoLimited.setCameraFace(0);
        videoLimited.setMediaAction(0);
        videoLimited.setVideoFileSize(5 * 1024 * 1024);
        videoLimited.setMinimumVideoDuration(5 * 60 * 1000);
        new Annca(videoLimited.build()).launchCamera();
        ques_and_record_btn_layout.setVisibility(View.GONE);*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_RQ) {

            if (resultCode == RESULT_OK) {
                options_screen.setVisibility(View.VISIBLE);
                loadvideo=false;
                try {
                    video_preview=null;
                    mediaController1.hide();
                }catch (Exception e){

                }
                String[] msg = data.getDataString().split("/");
                for (int i = 0; i < msg.length; i++) {
                    Log.v("paths :" + i + "", msg[i]);
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 3; i < msg.length; i++) {
                    sb.append("/");
                    sb.append(msg[i].toString());
                }
                String sbuilder = sb.toString();
                sb.setLength(0);
                //sb.append("[");
                sb.append(sbuilder);
                path=sb.toString();
            } else if(data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }



    private void onCompress(String pathis) {
        //  VideoCompressor.
        String convertpath = pathis;
        String[] mypath = convertpath.split("/");
        String[] lastnamewithextension = mypath[mypath.length - 1].split("\\.");
        String uniquename = lastnamewithextension[0];
        Log.e("nameis", uniquename);
        newPath = pathis.replace(uniquename, "comp" + uniquename);
    }
    private class GetQues extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            qs_progress.show();
        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {
                    //httpClient = new DefaultHttpClient();
                    // httpPost = new HttpPost("http://35.154.93.176/Player/GetQuestions");

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("CE_Id", ceid)
                            .object("Game", game)
                            .object("Category", cate)
                            .object("GD_Id", gdid)
                            .build().toString();
                   /* StringEntity se = new StringEntity(jn.toString());
                    Log.e("Reqt", jn + "");
                    Log.e("Request", se + "");
                    httpPost.addHeader("Authorization", "Bearer " + token);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    code1 = httpResponse.getStatusLine().getStatusCode();
                    HttpEntity httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    Log.e("FKJ", httpResponse + "CHARAN" + is);*/


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/GetQuestions");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code1 = urlConnection.getResponseCode();


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (code1 == 200) {

                    /*try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "iso-8859-1"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        is.close();

                        Qus = sb.toString();


                        Log.e("quesfefff", Qus);

                    } catch (Exception e) {
                        e.getMessage();
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }*/

                    String responseString = readStream(urlConnection.getInputStream());
                    Log.e("Responseques", responseString);
                    result1 = responseString;

                    try {
                        JSONArray data = new JSONArray(result1);
                        Log.e("QUESDATA", data.length() + "");
                        // JSONArray sidd = data.getJSONArray(0);
                        final JSONObject sidobj = data.getJSONObject(0);
                        sid = sidobj.getString("S_Id");

                        JSONArray questions = data.getJSONArray(1);
                        for(int i=0;i<questions.length();i++){
                            JSONObject js=questions.getJSONObject(i);
                            QuestionModel s=new QuestionModel();
                            s.setQT_id(js.getString("QT_Id"));
                            s.setQ_id(js.getString("Q_Id"));
                            s.setQuestion(js.getString("Q_Question"));
                            s.setOpt1(js.optString("Q_Option1"));
                            s.setOpt2(js.optString("Q_Option2"));
                            s.setOpt3(js.optString("Q_Option3"));
                            s.setOpt4(js.optString("Q_Option4"));
                            s.setOpt5(js.optString("Q_Option5"));
                            s.setCorrectanswer(js.getString("Q_Answer"));
                            if(js.getString("Q_MaxTime").equals(null)){
                                s.setTime("60");
                            }else {
                                s.setTime(js.getString("Q_MaxTime"));
                            }
                            qs.add(s);
                        }
                        Log.e("QUES", questions.length() + "");

                    } catch (JSONException e) {

                        Log.e("QUES", "Error parsing data " + e.toString());
                    }
                }


            } catch(Exception e){
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                qs_progress.dismiss();
            }catch (Exception e){

            }
            if(code1==200){
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                    startPlanetanimation();
                }
              setQuestions();
            }else{
                Intent it = new Intent(VideoGameActivity.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }

        }
    }

    private void startPlanetanimation() {
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


    }

    private void setQuestions() {
        final String now = df1.format(c.getTime()).toString();
        startDatelist.add(now);
        QuestionModel qsmodel=qs.get(counter_ques);
        int no=counter_ques+1;
        q_no_is.setText("Question "+no);
        ques_is.setText(qsmodel.getQuestion());

    }

    public String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qus = sb.toString();


            Log.e("JSONStrr", Qus);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qus;
    }

    @Override
    public void onBackPressed() {
        Intent it = new Intent(VideoGameActivity.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(it);
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
        finish();
    }
}
