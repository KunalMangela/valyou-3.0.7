package com.globalgyan.getvalyou;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.globalgyan.getvalyou.cms.request.ValYouRequest;
import com.globalgyan.getvalyou.model.VideoData;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by NaNi on 10/10/17.
 */

public class LearningAdapter extends RecyclerView.Adapter<LearningAdapter.MyViewHolder> {
    private Context mContext;
    private List<VideoData> albumList;
    public LearningAdapter(Context mContext, List<VideoData> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }
    @Override
    public LearningAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lear_card, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LearningAdapter.MyViewHolder holder, final int position) {
        final VideoData articleData = albumList.get(position);
            holder.title.setText(albumList.get(position).getVideoName());
//        Bitmap largeIcon = BitmapFactory.decodeResource(mContext.getResources(),albumList.get(position).getCoverImage() );
//        final int nh = (int) (largeIcon.getHeight() * (512.0 / largeIcon.getWidth()));
//        final Bitmap scaled = Bitmap.createScaledBitmap(largeIcon, 512, nh, true);
//        holder.thumbnail.setImageBitmap(scaled);
        if (articleData != null && articleData.getCoverImage() != null) {
            //holder.imgElement.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(ValYouRequest.HOME_IMAGES + articleData.getCoverImage().getGridId())
                    .error(R.drawable.dummy_url)
                    .placeholder(R.drawable.dummy_url)
                    .into(holder.thumbnail);
        }else{
            if(albumList.get(position).getLink().contains("youtube")){
                StringTokenizer st = new StringTokenizer(albumList.get(position).getLink(),"=");
                String dd="";
                while (st.hasMoreTokens()) {
                    dd= st.nextToken();
                }
                Glide.with(mContext)
                        .load("https://img.youtube.com/vi/"+dd+"/0.jpg")
                        .error(R.drawable.dummy_url)
                        .placeholder(R.drawable.dummy_url)
                        .into(holder.thumbnail);
            }else{
                holder.thumbnail.setImageResource(R.drawable.dummy_url);
            }
        }
//        Picasso.with(mContext)
//                .load(Uri.parse(ValYouRequest.HOME_IMAGES +albumList.get(position).getCoverImage().getGridId()))
//                .placeholder(R.drawable.onbpic)   // optional
//                .resize(500,500)                        // optional
//                .into(holder.thumbnail);
        holder.domain.setText(albumList.get(position).getFunctionalArea());
        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videoClient = new Intent(Intent.ACTION_VIEW);
                videoClient.setData(Uri.parse(albumList.get(position).getLink()));
                mContext.startActivity(videoClient);
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count,domain;
        public ImageView thumbnail, overflow;
        public Button play;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            play=(Button)view.findViewById(R.id.lplay);
            domain=(TextView)view.findViewById(R.id.domain);
            // overflow = (ImageView) view.findViewById(R.id.overflow);

            //on item click
//            itemView.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View v){
//                    int pos = getAdapterPosition();
//                    if (pos != RecyclerView.NO_POSITION){
//                        Learning clickedDataItem = albumList.get(pos);
////                        Intent intent = new Intent(mContext, Det.class);
////                        intent.putExtra("name", albumList.get(pos).getName());
////                        intent.putExtra("lang", albumList.get(pos).getLang());
////                        intent.putExtra("thumbnail", albumList.get(pos).getThumbnail());
////                        intent.putExtra("scrn",albumList.get(pos).getScrn());
////                        intent.putExtra("ourl",albumList.get(pos).getOurl());
////                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                        mContext.startActivity(intent);
//
//                        Intent videoClient = new Intent(Intent.ACTION_VIEW);
//                        videoClient.setData(Uri.parse(albumList.get(pos).getUrl()));
//                       // videoClient.setClassName("com.google.android.youtube", "com.google.android.youtube.WatchActivity");
//                        mContext.startActivity(videoClient);
//                        // Toast.makeText(v.getContext(), "You clicked " + clickedDataItem.getName(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
        }
    }
}
