package com.globalgyan.getvalyou.customwidget;

import android.content.Context;
import android.graphics.Color;
import android.widget.ScrollView;

/**
 * Created by NaNi on 13/09/17.
 */

public class CustomScrollView extends ScrollView {
    public CustomScrollView(Context context) {
        super(context);
    }
    @Override
    public int getSolidColor() {
        return Color.rgb(255, 234, 230);
    }
}
