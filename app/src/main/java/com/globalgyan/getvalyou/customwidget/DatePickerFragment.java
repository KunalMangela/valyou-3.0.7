package com.globalgyan.getvalyou.customwidget;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.globalgyan.getvalyou.interfaces.CalendarPicker;
import com.globalgyan.getvalyou.utils.FontUtils;

import java.util.Calendar;

public class    DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {


    public void setCalendarPicker(CalendarPicker calendarPicker) {
        this.calendarPicker = calendarPicker;
    }

    private CalendarPicker calendarPicker = null;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        if (selectedDate != null) {
            year = selectedDate.get(Calendar.YEAR);
            month = selectedDate.get(Calendar.MONTH);
            day = selectedDate.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);

        // Create a TextView programmatically.
        TextView tv = new TextView(getActivity());

        // Create a TextView programmatically
        LayoutParams lp = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, // Width of TextView
                LayoutParams.WRAP_CONTENT); // Height of TextView
        tv.setLayoutParams(lp);
        tv.setPadding(10, 20, 10, 20);
        tv.setTypeface(FontUtils.getInstance(getActivity()).getNexaTypeFace());
        tv.setGravity(Gravity.CENTER);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        if (TextUtils.isEmpty(title))
            tv.setText("This is a custom title.");
        else
            tv.setText(title);
        tv.setTextColor(Color.parseColor("#ffffff"));
        tv.setBackgroundColor(Color.parseColor("#02BFF3"));

        // Set the newly created TextView as a custom tile of DatePickerDialog
        dpd.setCustomTitle(tv);

        // Or you can simply set a tile for DatePickerDialog
            /*
                setTitle(CharSequence title)
                    Set the title text for this dialog's window.
            */
        //dpd.setTitle("This is a simple title."); // Uncomment this line to activate it

        // Return the DatePickerDialog


        if (startDate != null) {
            dpd.getDatePicker().setMinDate(startDate.getTimeInMillis());
        }

        if (endDate != null) {
            dpd.getDatePicker().setMaxDate(endDate.getTimeInMillis());
        }


        if (!showDate) {
            dpd.getDatePicker().setSpinnersShown(true);
            dpd.getDatePicker().setCalendarViewShown(false);

            LinearLayout pickerParentLayout = (LinearLayout) dpd.getDatePicker().getChildAt(0);

            LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);


            int count = pickerSpinnersHolder.getChildCount();
            if (count == 3) {
                View firstView = pickerSpinnersHolder.getChildAt(0);

                NumberPicker firstNumber = (NumberPicker) firstView;

                if (firstNumber != null && firstNumber.getMaxValue()>12 && firstNumber.getMaxValue() < 32) {
                    pickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
                } else {
                    View secondView = pickerSpinnersHolder.getChildAt(1);
                    NumberPicker secondNumber = (NumberPicker) secondView;
                    if (secondNumber != null && secondNumber.getMaxValue()>12 && secondNumber.getMaxValue() < 32) {
                        pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                    }
                }
            }



        }

        if (!showMonth) {
            dpd.getDatePicker().setSpinnersShown(true);
            dpd.getDatePicker().setCalendarViewShown(false);

            LinearLayout pickerParentLayout = (LinearLayout) dpd.getDatePicker().getChildAt(0);

            LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);


            int count = pickerSpinnersHolder.getChildCount();
            if (count == 3) {
                View firstView = pickerSpinnersHolder.getChildAt(0);
                NumberPicker firstNumber = (NumberPicker) firstView;
                if (firstNumber != null && firstNumber.getMaxValue()<12 ) {
                    pickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
                } else {
                    View secondView = pickerSpinnersHolder.getChildAt(1);
                    NumberPicker secondNumber = (NumberPicker) secondView;
                    if (secondNumber != null && secondNumber.getMaxValue()<12 ) {
                        pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                    }
                }
            }



        }

        return dpd;
    }

    private boolean showYearOnly = false;
    private boolean showDate = false;

    public void setShowMonth(boolean showMonth) {
        this.showMonth = showMonth;
    }

    private boolean showMonth = false;


    public void setShowDate(boolean showDate) {
        this.showDate = showDate;
    }

    private String title = null;

    private Calendar startDate = null;

    private Calendar endDate = null;

    public void setSelectedDate(Calendar selectedDate) {
        this.selectedDate = selectedDate;
    }

    private Calendar selectedDate = null;

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the chosen date
        calendarPicker.onDateSelected(year, month + 1, day);

        Log.e("Dateeeee", "year=" + year + " month=" + month + " day" + day);
    }
}