package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.MasterSlaveGame.MSQ;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;


public class LanguageActivity extends AppCompatActivity {
    private PrefManager prefManager;
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int responseCode,responseCod,responseCo;
    HttpURLConnection urlConnection, urlConnection1;
    static InputStream is = null;
    static String json = "",jso="";
    static ArrayList<String> lang_array = new ArrayList<>();
    Handler handler = new Handler();
    boolean flag_lang_selected = false;
    static String token="", lang_name;
   static String selected_lang ="";
    Dialog loading_popup;

    FontTextView tv1,tv2,tv3;
    static String intro1_title1_string,intro1_title2_string,intro2_title1_string,intro2_desc1_string,intro2_desc2_string,intro2_desc3_string,
            intro2_desc4_string,intro3_title1_string,intro3_desc1_string,intro3_desc2_string,intro3_desc3_string,intro3_desc4_string, skip_txt,login_txt;
    static Typeface tf2;
    static  Spannable colored_str_in_intro2, colored_str_in_intro3;

    ImageView uparrow, spinner_downarrow;
    FontTextView spinner_uparrow;
    LinearLayout lang;
    ListView lang_list;
    SpinnerAdapter spinnerAdapter;
    ArrayList<String > textArray = new ArrayList<>();
    ArrayList<Integer> imageArray=new ArrayList<>();
    ImageView earth;
    ObjectAnimator earthotate;
    FontTextView selct_lang;
    Dialog alertDialog;
    ObjectAnimator  astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;
    ConnectionUtils connectionUtils;

    ImageView red_planet,yellow_planet,purple_planet,moon, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            moon_anim_1, moon_anim_2, orange_planet_anim_1,orange_planet_anim_2;
    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        prefManager = new PrefManager(this);

        appConstant = new AppConstant(this);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
     /*  if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#291C5A"));
        }

        tf2 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");

        connectionUtils = new ConnectionUtils(this);

        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        Log.e("battery", ""+batLevel);

        imageArray.add(R.drawable.span_flag);
        imageArray.add(R.drawable.chin_flag);
        imageArray.add(R.drawable.eng_flag);

        spinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_lang_content, textArray, imageArray);


        selct_lang = (FontTextView)findViewById(R.id.select_lang);
        selct_lang.setTypeface(tf2);

        lang = (LinearLayout)findViewById(R.id.slct_lang);
        lang_list = (ListView)findViewById(R.id.lang_list) ;
        uparrow = (ImageView) findViewById(R.id.up_arrow) ;
        spinner_uparrow = (FontTextView) findViewById(R.id.go) ;
        spinner_downarrow = (ImageView) findViewById(R.id.spinner_downarrow) ;
        earth = (ImageView) findViewById(R.id.earth) ;


        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        moon = (ImageView)findViewById(R.id.moon);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);


        earth.setLayerType(View.LAYER_TYPE_HARDWARE, null);

         earthotate = ObjectAnimator.ofFloat(earth,
                                "rotation", 0f, 360f);
        earthotate.setDuration(150000); // miliseconds
        earthotate.setStartDelay(0);
        earthotate.setRepeatCount(ValueAnimator.INFINITE);
        earthotate.setInterpolator(new LinearInterpolator());

        spinner_downarrow.setVisibility(View.GONE);



        tv1 = (FontTextView) findViewById(R.id.spanish);
        tv2 = (FontTextView) findViewById(R.id.Chinese);
        tv3 = (FontTextView) findViewById(R.id.English);
//        loading_popup = KProgressHUD.create(LanguageActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.1f)
//                .setCancellable(false);


        loading_popup = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        loading_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading_popup.setContentView(R.layout.planet_loader);
        Window window1 = loading_popup.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        loading_popup.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        loading_popup.setCancelable(false);
        GifView gifView2 = (GifView) loading_popup.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();

        tv1.setTypeface(tf2);
        tv2.setTypeface(tf2);
        tv3.setTypeface(tf2);


        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv1.setTextColor(Color.parseColor("#d200ff"));

                boolean isSelectedAfterClick = !v.isActivated();
                v.setActivated(isSelectedAfterClick);
                selected_lang = tv1.getText().toString();
                new GetIntroInfo().execute();
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv2.setTextColor(Color.parseColor("#d200ff"));

                boolean isSelectedAfterClick = !v.isActivated();
                v.setActivated(isSelectedAfterClick);
                selected_lang = tv2.getText().toString();
                new GetIntroInfo().execute();
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv3.setTextColor(Color.parseColor("#d200ff"));

                boolean isSelectedAfterClick = !v.isActivated();
                v.setActivated(isSelectedAfterClick);
                selected_lang = tv3.getText().toString();
                new GetIntroInfo().execute();
            }
        });
//
//        lang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                uparrow.setVisibility(uparrow.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
//                lang_list.setVisibility(lang_list.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
//                spinner_downarrow.setVisibility(spinner_downarrow.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
//                spinner_uparrow.setVisibility(spinner_uparrow.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
//
//             //   uparrow.setVisibility(View.VISIBLE);
//
//              //  lang_list.setVisibility(View.VISIBLE);
//            }
//        });

        lang_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
              /*  selected_lang = lang_array.get(position);
                new GetIntroInfo().execute();*/
            }
        });







        if(connectionUtils.isConnectionAvailable()){

            getData();

        }
        else {

            showREtryDialogue("Error while getting languages");
            Toast.makeText(LanguageActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
        }


    }

    public void startPlanetanimation(){
        earthotate.start();
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  moon animation
        moon_anim_1 = ObjectAnimator.ofFloat(moon, "translationX", 0f,1150f);
        moon_anim_1.setDuration(90000);
        moon_anim_1.setInterpolator(new LinearInterpolator());
        moon_anim_1.setStartDelay(0);
        moon_anim_1.start();

        moon_anim_2 = ObjectAnimator.ofFloat(moon, "translationX", -400f,0f);
        moon_anim_2.setDuration(55000);
        moon_anim_2.setInterpolator(new LinearInterpolator());
        moon_anim_2.setStartDelay(0);

        moon_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                moon_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        moon_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                moon_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


    }


    private void getData() {
        new GetToken().execute();
    }

    private class GetToken extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {
            loading_popup.show();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream1(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading_popup.dismiss();
                        showREtryDialogue("Error while getting languages");
                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading_popup.dismiss();
                        showREtryDialogue("Error while getting languages");
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading_popup.dismiss();
                        showREtryDialogue("Error while getting languages");
                    }
                });
            }
            try {
                JSONObject jObj = new JSONObject(result1);
                token=jObj.getString("access_token");
                String token_type=jObj.getString("token_type");
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(responseCod != urlConnection.HTTP_OK){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading_popup.dismiss();
                        showREtryDialogue("Error while getting languages");

                    }
                });
                Toast.makeText(getApplicationContext(),"Error while loading user data",Toast.LENGTH_SHORT).show();
            }else
            {


                new GetLanguages().execute();


            }

        }
    }

    private class GetLanguages extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            String result1 = "";
            try {


                try {



                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/get_languages");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");



                    responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.v("Response", responseString);
                    result1 = responseString;


                    JSONArray lang_data = new JSONArray(result1);

                    for (int i = 0; i < lang_data.length(); i++) {

                        JSONObject jo = lang_data.getJSONObject(i);

                        // Storing each json item in variable
                        int l_id = jo.getInt("l_id");
                        lang_name = jo.getString("language_name");
                        String country = jo.getString("country");
                        String status = jo.getString("status");


                        // show the values in our logcat
                        Log.e("api lang data ", "l_id: " + l_id
                                + ", language_name: " + lang_name
                                + ", country: " + country + ", status: " + status);

                        lang_array.add(lang_name);

                        textArray.add(lang_name);

                    }





//                    myPagerAdapter.notifyDataSetChanged();

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loading_popup.dismiss();
                            showREtryDialogue("Error while getting languages");

                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loading_popup.dismiss();
                            showREtryDialogue("Error while getting languages");

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loading_popup.dismiss();
                            showREtryDialogue("Error while getting languages");

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loading_popup.dismiss();
                            showREtryDialogue("Error while getting languages");

                        }
                    });
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        lang_list.setAdapter(spinnerAdapter);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading_popup.dismiss();
                }
            });
            // mViewPager.setOffscreenPageLimit(5);
//            if(gotologingpage.equals("loginpage")&&!prefManager.isFirstTimeLaunch()){
//
//                mViewPager.setCurrentItem(page);
//                mViewPager.setAdapter(myPagerAdapter);
//
//                loading_popup.dismiss();
//            }
//
//            else {


            new CountDownTimer(500, 500) {
                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    loading_popup.dismiss();

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        startPlanetanimation();

                    }
                    else {


                    }





                }
            }.start();
            try {
                tv1.setText(lang_array.get(0));
                tv2.setText(lang_array.get(1));
                tv3.setText(lang_array.get(2));

            }catch (Exception e){
                e.printStackTrace();
            }
            //  }
            // myPagerAdapter.notifyDataSetChanged();

            try {
                new CountDownTimer(200, 200) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (connectionUtils.isConnectionAvailable()) {
                            selected_lang = lang_array.get(2);
                            new PrefManager(LanguageActivity.this).set_lang(selected_lang);
                            new GetIntroInfo().execute();
                        } else {
                            Toast.makeText(LanguageActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                            ;
                        }
                    }
                }.start();
            }catch (Exception e){
                e.printStackTrace();
            }
        }



    }

    private class GetIntroInfo extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            loading_popup.show();
            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {
                    // httpClient = new DefaultHttpClient();
                    // httpPost = new HttpPost("http://35.154.93.176/Player/TodayGames");


                    String jon = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", "")
                            .object("language_name", ""+selected_lang)
                            .build().toString();



                    URL urlToRequest = new URL(AppConstant.Ip_url+"GameSettings");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jon.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jon);
                    wr.flush();

                    responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.v("Response", responseString);
                    result1 = responseString;



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {



                JSONObject jsonObject1=new JSONObject(s);

                String frkey=jsonObject1.getString("FR_Key");

                new PrefManager(LanguageActivity.this).set_frkey(frkey);
                String jsonObj = jsonObject1.getString("language_json");
                JSONObject jsonObject=new JSONObject(jsonObj);
                JSONArray jsonArrayIntro1_title1=jsonObject.getJSONArray("intropageTitles_1");
                intro1_title1_string=jsonArrayIntro1_title1.get(0).toString();
                intro1_title2_string=jsonArrayIntro1_title1.get(1).toString();
                JSONArray jsonArrayIntro2_title1=jsonObject.getJSONArray("intropageTitles_2");
                intro2_title1_string=jsonArrayIntro2_title1.get(0).toString();
                JSONArray jsonArrayIntro2_Desc=jsonObject.getJSONArray("intropageDescription_2");
                intro2_desc1_string=jsonArrayIntro2_Desc.get(0).toString();
                intro2_desc2_string=jsonArrayIntro2_Desc.get(1).toString();
                intro2_desc3_string=jsonArrayIntro2_Desc.get(2).toString();
                intro2_desc4_string=jsonArrayIntro2_Desc.get(3).toString();
                JSONArray jsonArrayIntro3_title=jsonObject.getJSONArray("intropageTitles_3");
                intro3_title1_string=jsonArrayIntro3_title.get(0).toString();
                JSONArray jsonArrayIntro3_Desc=jsonObject.getJSONArray("intropageDescription_3");
                intro3_desc1_string=jsonArrayIntro3_Desc.get(0).toString();
                intro3_desc2_string=jsonArrayIntro3_Desc.get(1).toString();
                intro3_desc3_string=jsonArrayIntro3_Desc.get(2).toString();
                intro3_desc4_string=jsonArrayIntro3_Desc.get(3).toString();
                JSONArray skip_login_txt=jsonObject.getJSONArray("intropageskipbtn");
                skip_txt = skip_login_txt.getString(0).toString();
                login_txt = skip_login_txt.getString(2).toString();





                SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(LanguageActivity.this).edit();
                prefEditor.putString("lang", selected_lang);

                prefEditor.putString("intro1_title1_string", intro1_title1_string);
                prefEditor.putString("intro1_title2_string", intro1_title2_string);
                prefEditor.putString("colored_str_in_intro2", intro2_title1_string);
                prefEditor.putString("intro2_desc1_string", intro2_desc1_string);
                prefEditor.putString("intro2_desc2_string", intro2_desc2_string);
                prefEditor.putString("intro2_desc3_string", intro2_desc3_string);
                prefEditor.putString("intro2_desc4_string", intro2_desc4_string);
                prefEditor.putString("colored_str_in_intro3", intro3_title1_string);
                prefEditor.putString("intro3_desc1_string", intro3_desc1_string);
                prefEditor.putString("intro3_desc2_string", intro3_desc2_string);
                prefEditor.putString("intro3_desc3_string", intro3_desc3_string);
                prefEditor.putString("intro3_desc4_string", intro3_desc4_string);
                prefEditor.putString("skip", skip_txt);
                prefEditor.putString("login", login_txt);

                prefEditor.apply();





                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long l) {

                    }

                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onFinish() {
                        loading_popup.dismiss();

                        Intent i = new Intent(LanguageActivity.this, Welcome.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                        finish();
                    }
                }.start();

//                SpannableStringBuilder SS1 = new SpannableStringBuilder(intro1_title1_string);
//                SS1.setSpan(new CustomTypefaceSpan("", tf2), 0, intro1_title1_string.length()-1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
    private String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {

                sb.append(line);
            }
            is.close();

            jso = sb.toString();


            Log.e("JSONStrr", jso);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return jso;
    }
    private String readStream1(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @Override
    public void onBackPressed() {

    }



    public void showREtryDialogue(String errormsg) {
        Log.e("respmsg","dialogue");

        /*final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                LanguageActivity.this, R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(LanguageActivity.this);
        View dialogView = inflater.inflate(R.layout.retry_anim_dialogue, null);

        dialogBuilder.setView(dialogView);


        alertDialog = dialogBuilder.create();


//                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

        alertDialog.show();*/
        alertDialog = new Dialog(LanguageActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText(errormsg);

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();

                if(connectionUtils.isConnectionAvailable()) {
                    yes.setEnabled(false);
                    if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode) || batLevel == 0) {

                        astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                        astro_inx.setDuration(300);
                        astro_inx.setInterpolator(new LinearInterpolator());
                        astro_inx.setStartDelay(0);
                        astro_inx.start();
                        astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                        astro_iny.setDuration(300);
                        astro_iny.setInterpolator(new LinearInterpolator());
                        astro_iny.setStartDelay(0);
                        astro_iny.start();

                        alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                        alien_inx.setDuration(300);
                        alien_inx.setInterpolator(new LinearInterpolator());
                        alien_inx.setStartDelay(0);
                        alien_inx.start();
                        alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                        alien_iny.setDuration(300);
                        alien_iny.setInterpolator(new LinearInterpolator());
                        alien_iny.setStartDelay(0);
                        alien_iny.start();
                    }
                    new CountDownTimer(400, 400) {


                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                                    alertDialog.dismiss();


                            getData();


                        }
                    }.start();



                }else {
                    Toast.makeText(LanguageActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });

        try {
            if(!alertDialog.isShowing())
            alertDialog.show();
             }
             catch (Exception e){

             }
        }
    }


