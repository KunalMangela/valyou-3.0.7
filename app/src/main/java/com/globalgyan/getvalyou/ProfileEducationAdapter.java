package com.globalgyan.getvalyou;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.globalgyan.getvalyou.model.EducationalDetails;

import java.util.ArrayList;

/**
 * Created by techniche-android on 10/1/17.
 */

public class ProfileEducationAdapter extends RecyclerView.Adapter<ProfileEducationAdapter.ViewHolder>{
    Context context = null;

    public String getStringBuffer() {
        return stringBuffer;
    }

    private String stringBuffer = null;
    private ArrayList<EducationalDetails> educationalDetailses = null;
    private boolean canEdit = false;

    public ProfileEducationAdapter(Context context, ArrayList<EducationalDetails> educationalDetailses, boolean canEdit) {
        this.context = context;
        this.educationalDetailses = educationalDetailses;
        this.canEdit = canEdit;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_educational_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EducationalDetails educationalDetails = educationalDetailses.get(position);
        holder.txtDegreeName.setText(educationalDetails.getDegreeName());
        holder.txtCollegeName.setText(educationalDetails.getCollegeName());
        String yrs = educationalDetails.getFrom();
        String toDate = educationalDetails.getTo();
        if(!TextUtils.isEmpty(toDate) && toDate.length()>2) {
            holder.txtEducationTime.setText(yrs +" to "+toDate);
        }else{
            holder.txtEducationTime.setText(yrs +" to current");
        }
    }

    @Override
    public int getItemCount() {
        return educationalDetailses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtDegreeName;
        TextView txtCollegeName;
        TextView txtEducationTime;
        public ViewHolder(View itemView) {
            super(itemView);

            txtDegreeName = (TextView) itemView.findViewById(R.id.degree);
            txtCollegeName = (TextView) itemView.findViewById(R.id.clgname);
            txtEducationTime = (TextView) itemView.findViewById(R.id.yearsclg);
        }
    }
}
