package com.globalgyan.getvalyou;


import android.app.Activity;
import android.graphics.Color;
import android.icu.util.JapaneseCalendar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.globalgyan.getvalyou.fragments.LoginFragment;

import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.Welcome.intro1_title1_string;
import static com.globalgyan.getvalyou.Welcome.intro1_title2_string;
import static com.globalgyan.getvalyou.Welcome.lang_array;
import static com.globalgyan.getvalyou.Welcome.tf2;


/**
 * A simple {@link Fragment} subclass.
 */
public class IntroLanguageFragment extends Fragment {
    lang_spanish clicked_spanish;

    lang_chinese clicked_chinese;

    lang_english clicked_english;

    static String txt1, txt2, txt3;

    public interface lang_spanish {
        void clicked_span_lang(View v);
    }

    public interface lang_chinese {
        void clicked_chi_lang(View v);
    }

    public interface lang_english {
        void clicked_eng_lang(View v);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        clicked_spanish = (lang_spanish) activity;
        clicked_chinese = (lang_chinese) activity;
        clicked_english = (lang_english) activity;

    }


    public IntroLanguageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_intro_language, container, false);

        final FontTextView tv = (FontTextView) v.findViewById(R.id.Japanese);


        tv.setText(lang_array.get(0));
        final FontTextView tv2 = (FontTextView) v.findViewById(R.id.Chinese);

        tv2.setText(lang_array.get(1));

        final FontTextView tv3 = (FontTextView) v.findViewById(R.id.English);

        tv3.setText(lang_array.get(2));


        tv.setTypeface(tf2);
        tv2.setTypeface(tf2);
        tv3.setTypeface(tf2);

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv.setTextColor(Color.parseColor("#d200ff"));

                boolean isSelectedAfterClick = !v.isActivated();
                v.setActivated(isSelectedAfterClick);

                if(isSelectedAfterClick) {
                    clicked_spanish.clicked_span_lang(v);
                    txt1 = tv.getText().toString();
                }
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv2.setTextColor(Color.parseColor("#d200ff"));

                clicked_chinese.clicked_chi_lang(v);
                txt2 = tv2.getText().toString();
                boolean isSelectedAfterClick = !v.isSelected();


                v.setSelected(isSelectedAfterClick);
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv3.setTextColor(Color.parseColor("#d200ff"));

                boolean isSelectedAfterClick = !v.isSelected();
                v.setSelected(isSelectedAfterClick);
                clicked_english.clicked_eng_lang(v);
                txt3 = tv3.getText().toString();

            }
        });

        return v;
    }

}
