package com.globalgyan.getvalyou;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.ArrayList;

public class LearningGroupAdapter extends RecyclerView.Adapter<LearningGroupAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<LearningGroupModel> fungame;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,progress;

        public MyViewHolder(View view) {
            super(view);

            name=(TextView)view.findViewById(R.id.txtname);


        }
    }


    public LearningGroupAdapter(Context mContext, ArrayList<LearningGroupModel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.learning_grp_items, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final LearningGroupModel learningGroupModel=fungame.get(position);
        holder.name.setText(learningGroupModel.getName());
        holder.name.setSelected(true);

        Typeface typeface1 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.name.setTypeface(typeface1);
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionUtils connectionUtils=new ConnectionUtils(mContext);
                if(connectionUtils.isConnectionAvailable()) {
                    ActiveCoursesActivity.click=true;
                    Log.e("idis", String.valueOf(learningGroupModel.getId()));
                    ActiveCoursesActivity.getProgress(learningGroupModel.getId());
                }else {
                    Toast.makeText(mContext,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return fungame.size();
    }

    public static void clickCourses(){

    }
}

