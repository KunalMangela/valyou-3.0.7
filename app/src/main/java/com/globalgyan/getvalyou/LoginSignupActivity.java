package com.globalgyan.getvalyou;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.globalgyan.getvalyou.MasterSlaveGame.MasterSlaveGame;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.cms.response.ForgetPasswordResponse;
import com.globalgyan.getvalyou.cms.response.LoginResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.SocialLoginResponse;
import com.globalgyan.getvalyou.cms.response.SocialRegisterResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Dates;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import im.delight.android.location.SimpleLocation;
import it.sephiroth.android.library.easing.Back;
import it.sephiroth.android.library.easing.EasingManager;

import static com.thefinestartist.utils.content.ContextUtil.getSystemService;

public class LoginSignupActivity extends AppCompatActivity implements GUICallback,ActivityCompat.OnRequestPermissionsResultCallback{
    private ViewGroup rootLayout;
    TextView login_tv,signup_tv,forgotpassis;
    FormEditText email,password,emailS, passwordS,phone, username;
    boolean checkpas=false,checkphone=false;
    String type,currentscreen="loginscreen";
    ImageView logincard,signupcard;
    boolean ispassvisible=false,ispassvisiblelogin=false;
    //login
    ImageView pass_toggle_signup,pass_toggle_login;
    private static final int RC_SIGN_IN = 007;
    private String providerTypeStr = "";
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    private SimpleLocation location;
    KProgressHUD login_progrss,signup_progress;
    ConnectionUtils connectionUtils;
    //signup
    boolean ispause=false;
    AppConstant appConstant;
    boolean flag_netcheck=false;
    RelativeLayout loginlayout,signuplayout;
    Animation rightanim,leftanim, rightanim2, leftanim2;
    ImageView img_logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);

        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        img_logo=(ImageView)findViewById(R.id.img_logo);
        loginlayout=(RelativeLayout)findViewById(R.id.login_layout);
        signuplayout=(RelativeLayout)findViewById(R.id.signup_layout);

         email = (FormEditText) findViewById(R.id.email);
         password = (FormEditText) findViewById(R.id.password);

         emailS = (FormEditText) findViewById(R.id.email_singup);
         passwordS = (FormEditText) findViewById(R.id.password_singup);
         phone = (FormEditText) findViewById(R.id.phone);
         username = (FormEditText) findViewById(R.id.username);
       /*  pass_toggle_login=(ImageView) findViewById(R.id.pass_toggle_login);
        pass_toggle_signup=(ImageView) findViewById(R.id.pass_toggle_signup);*/
        login_tv=(TextView)findViewById(R.id.login_tv);
        forgotpassis=(TextView)findViewById(R.id.forgot_pass_tv);
        signup_tv=(TextView)findViewById(R.id.singup_big_tv);

        signupcard=(ImageView)findViewById(R.id.singup_button);
        logincard=(ImageView)findViewById(R.id.login_button);
      /*  email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    img_logo.setVisibility(View.GONE);
                }else {
                    img_logo.setVisibility(View.VISIBLE);
                }
            }
        });
        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    img_logo.setVisibility(View.GONE);
                }else {
                    img_logo.setVisibility(View.VISIBLE);
                }
            }
        });*/

        rightanim =
                AnimationUtils.loadAnimation(LoginSignupActivity.this,
                        R.anim.login_right_anim);
        leftanim =
                AnimationUtils.loadAnimation(LoginSignupActivity.this,
                        R.anim.login_left_anim);

        rightanim2 =
                AnimationUtils.loadAnimation(LoginSignupActivity.this,
                        R.anim.on_left_swipe_anim);
        leftanim2 =
                AnimationUtils.loadAnimation(LoginSignupActivity.this,
                        R.anim.on_swipe_left_anim_two);
        //login
        connectionUtils=new ConnectionUtils(LoginSignupActivity.this);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //progress dialogue
        login_progrss = KProgressHUD.create(LoginSignupActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Logging in")
                .setDimAmount(0.7f)
                .setCancellable(false);
        //signup
        signup_progress = KProgressHUD.create(LoginSignupActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("please wait...")
                .setDimAmount(0.7f)
                .setCancellable(false);



        loginlayout.setOnTouchListener(new OnSwipeTouchListener(LoginSignupActivity.this) {
            public void onSwipeTop() {
              //  Toast.makeText(LoginSignupActivity.this, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
//                @SuppressLint("ObjectAnimatorBinding") ObjectAnimator animation = ObjectAnimator.ofFloat(signuplayout, "xFraction", 0.0f, 0.25f);
//                animation.setDuration(3600);
//
//                animation.setInterpolator(new AccelerateDecelerateInterpolator());
//                animation.start();

                signuplayout.startAnimation(leftanim);
                loginlayout.startAnimation(rightanim);
                signupcard.performClick();
                //Toast.makeText(LoginSignupActivity.this, "right", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeLeft() {
              //  Toast.makeText(LoginSignupActivity.this, "left", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeBottom() {
              //  Toast.makeText(LoginSignupActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });

        signuplayout.setOnTouchListener(new OnSwipeTouchListener(LoginSignupActivity.this) {
            public void onSwipeTop() {
              //  Toast.makeText(LoginSignupActivity.this, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
              //  Toast.makeText(LoginSignupActivity.this, "right", Toast.LENGTH_SHORT).show();


            }
            public void onSwipeLeft() {

                signuplayout.startAnimation(rightanim2);
                loginlayout.startAnimation(leftanim2);
                logincard.performClick();
              //  Toast.makeText(LoginSignupActivity.this, "left", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeBottom() {
                //Toast.makeText(LoginSignupActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });

        forgotpassis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //change param
                Intent forgot = new Intent(LoginSignupActivity.this, ForgotActivity.class);
                forgot.putExtra("SIGNUP","NO");
                startActivity(forgot);
            }
        });
        logincard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentscreen="loginscreen";
                loginlayout.setVisibility(View.VISIBLE);
                signuplayout.setVisibility(View.GONE);
              //  showLogIn(view);
            }
        });
        loginlayout.setVisibility(View.VISIBLE);
        signupcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentscreen="signupscreen";
                signuplayout.setVisibility(View.VISIBLE);
                loginlayout.setVisibility(View.GONE);

                //  showSingUp(view);
            }
        });

      /*  pass_toggle_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ispassvisible) {
                    ispassvisible = false;
                    pass_toggle_signup.setImageResource(R.drawable.pass_visible);
                    passwordS.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    ispassvisible = true;
                    pass_toggle_signup.setImageResource(R.drawable.pass_invisible);
                    passwordS.setTransformationMethod(null);
                }
            }
        });*/
/*
        pass_toggle_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ispassvisiblelogin) {
                    ispassvisiblelogin = false;
                    pass_toggle_login.setImageResource(R.drawable.pass_visible);
                    password.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    ispassvisiblelogin = true;
                    pass_toggle_login.setImageResource(R.drawable.pass_invisible);
                    password.setTransformationMethod(null);
                }
            }
        });*/
  passwordS.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
//                showkeyboard(view);
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(passwordS.getText().length()>0) {
                        if (event.getRawX() >= (passwordS.getRight())) {
                            hideKeyboard();
                            if (ispassvisible) {
                                ispassvisible = false;
                                passwordS.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visible, 0);
                                passwordS.setTransformationMethod(new PasswordTransformationMethod());
                            } else {
                                ispassvisible = true;
                                passwordS.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisible, 0);
                                passwordS.setTransformationMethod(null);
                            }
                            // your action here
                            Log.e("pass", "clicked");
                            return true;
                        }
                    }
                }else {
                   // showkeyboard(view);
                }
                return false;
            }
        });

        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
//                showkeyboard(view);
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(password.getText().length()>0) {

                        if (event.getRawX() >= (password.getRight())) {
                            hideKeyboard();
                            if (ispassvisiblelogin) {
                                ispassvisiblelogin = false;
                                password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visible, 0);
                                password.setTransformationMethod(new PasswordTransformationMethod());
                            } else {
                                ispassvisiblelogin = true;
                                password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisible, 0);
                                password.setTransformationMethod(null);
                            }
                            // your action here
                            Log.e("pass", "clicked");
                            return true;
                        }
                    }
                }else {
                    // showkeyboard(view);
                }
                return false;
            }
        });


        login_tv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
        login_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                type="login";
                Log .e("email",email.getText().toString());

                Log .e("pass",password.getText().toString());
                login_tv.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        login_tv.setEnabled(true);
                    }
                }.start();

                FormEditText[] allFields	= { email,password};


                boolean allValid = true;
                for (FormEditText field: allFields) {
                    allValid = field.testValidity() && allValid;
                }

                if (allValid) {
                    if(password.getText().toString().contains(" ")) {
                        Toast.makeText(LoginSignupActivity.this,"Blank spaces are not allowed in password",Toast.LENGTH_SHORT).show();
                    }else if(password.getText().toString().length()<5) {
                        Toast.makeText(LoginSignupActivity.this,"Password length must be of minimum 6 characters",Toast.LENGTH_SHORT).show();


                    }else {
                      //do task
                        dologin();
                    }
                        // YAY
                    Log.e("status","valid");
                } else {
                    // EditText are going to appear with an exclamation mark and an explicative message.
                    Log.e("status","invalid");

                }
            }
        });

   password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
           /* if(editable.length()>1){
                if (!ispassvisiblelogin) {
                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visible, 0);
                    password.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisible, 0);
                    password.setTransformationMethod(null);
                }
            }*/
            }
        });

        signup_tv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
        signup_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                signup_tv.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        signup_tv.setEnabled(true);
                    }
                }.start();

                type="signup";

                Log .e("email",emailS.getText().toString());

                Log .e("username",username.getText().toString());

                Log .e("passis",passwordS.getText().toString());
                Log .e("phone",phone.getText().toString());


                checkpas=false;
                checkphone=false;
                FormEditText[] allFields	= { emailS,passwordS,username,phone};


                boolean allValid = true;
                for (FormEditText field: allFields) {
                    allValid = field.testValidity() && allValid;
                }

                if (allValid) {

                    if((phone.getText().toString().length()==10)){
                        checkphone=true;

                    }else {
                        checkphone=false;
                    }
                    Log.e("pass",passwordS.getText().toString());
                    if(passwordS.getText().toString().contains(" ")){
                        checkpas=false;
                    }else if(passwordS.getText().toString().length()<5) {
                        checkpas=false;
                    }else
                     {
                        checkpas=true;
                    }
                    if(checkphone && checkpas){

                        dosignup();
                        //do task
                    }else {
                        if(!checkpas){
                            if(passwordS.getText().toString().contains(" ")){
                                Toast.makeText(LoginSignupActivity.this,"Blank spaces are not allowed in password",Toast.LENGTH_SHORT).show();

                            }else {
                                Toast.makeText(LoginSignupActivity.this,"Password length must be of minimum 6 characters",Toast.LENGTH_SHORT).show();

                            }
                        }else {
                            Toast.makeText(LoginSignupActivity.this,"Phone number length must be of 10 digits",Toast.LENGTH_SHORT).show();

                        }
                    }
                    // YAY
                    Log.e("status","valid");
                } else {
                    // EditText are going to appear with an exclamation mark and an explicative message.
                    Log.e("status","invalid");

                }





            }
        });




    }

    private void showkeyboard(View view) {
        InputMethodManager inputMethodManager =
                (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                view.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

    private void dosignup() {
        hideKeyboard();

        if(connectionUtils.isConnectionAvailable()){

            signup_progress.show();

            final CheckInternetTask t911=new CheckInternetTask();
            t911.execute();

            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    t911.cancel(true);

                    if (flag_netcheck) {
                        flag_netcheck = false;
                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(LoginSignupActivity.this, LoginSignupActivity.this);
                        processor.sendOtp(phone.getText().toString());
                    }else {
                        signup_progress.dismiss();
                        flag_netcheck=false;
                        Toast.makeText(LoginSignupActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    }
                }
            }.start();

new CountDownTimer(2000, 2000) {
                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {




                }
            }.start();


        }else {
            Toast.makeText(LoginSignupActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
        }
    }





    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(type.equalsIgnoreCase("login")){


            Log.e("signin","resp");
            try {
                if (login_progrss.isShowing() && login_progrss != null) {
                    login_progrss.dismiss();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            if (guiResponse != null) {
                if (status.equals(RequestStatus.SUCCESS)) {

                    if (guiResponse instanceof LoginResponse) {

                        LoginResponse loginResponse = (LoginResponse) guiResponse;
                        if (loginResponse != null) {
                            if (loginResponse.isStatus()) {

                                try {
                                    FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                    FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                                } catch (Exception ex) {

                                }

                                DataBaseHelper dataBaseHelper = new DataBaseHelper(LoginSignupActivity.this);
                                dataBaseHelper.deleteAllTables();
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, loginResponse.getLoginModel().getDisplayName());

                                if (loginResponse.getLoginModel() != null && loginResponse.getLoginModel().isProfileComplete())
                                    PreferenceUtils.setProfileDone(LoginSignupActivity.this);
                                if (loginResponse.getLoginModel() != null) {
                                    LoginModel loginModel = loginResponse.getLoginModel();
                                    long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                    String dob = loginModel.getDateOfBirth();
                                    if (!TextUtils.isEmpty(dob)) {
                                        try {
                                            //String date = DateUtils.UtcToJavaDateFormat(dob, new SimpleDateFormat("dd-MM-yyyy"));
                                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                        } catch (Exception ex) {

                                        }
                                    }
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                    if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());

                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());

                                    PreferenceUtils.setCandidateId(loginModel.get_id(), LoginSignupActivity.this);

                                  //  List<EducationalDetails> educationalDetailses = loginResponse.getLoginModel().getCurrentEducation();
                                    List<Professional> professionals = loginResponse.getLoginModel().getProfessionalExperience();
                                  /*  if (educationalDetailses != null) {
                                        for (EducationalDetails details : educationalDetailses) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }

                                            details.setFrom(date);
                                            Dates end = details.getEndDate();

                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createEducation(details);
                                        }
                                    }*/
                                    if (professionals != null) {
                                        for (Professional details : professionals) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }
                                            details.setFrom(date);

                                            Dates end = details.getEndDate();
                                            try {
                                                if (end != null && end.getMonth() < 13) {
                                                    int index = end.getMonth();
                                                    index--;
                                                    date = months[index] + "-" + String.valueOf(end.getYear());
                                                }
                                            } catch (Exception ex) {

                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createProfessionlDetails(details);
                                        }
                                    }

                                    if (loginModel.isMobileVerified()) {
                                        PreferenceUtils.setVerified(LoginSignupActivity.this);
                                        Log.e("ibbbp",loginModel.isMobileVerified()+""+loginModel.isProfileComplete());
                                        if (PreferenceUtils.isProfileDone(LoginSignupActivity.this)) {
                                            Intent intent = new Intent(LoginSignupActivity.this, HomeActivity.class);
                                            intent.putExtra("tabs","normal");
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();

                                        }else {
                                            Intent intent = new Intent(LoginSignupActivity.this, Activity_Myself.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.putExtra("from","signup");
                                            startActivity(intent);
                                            finish();
                                        }
                                    } else {

                                        AppConstant.OTP_FROM_SIGNUP = true;
                                        Intent intent = new Intent(LoginSignupActivity.this, EnterOtpActivity.class);
                                        intent.putExtra("phn", loginModel.getMobile());
                                        intent.putExtra("id", loginModel.get_id());
                                        intent.putExtra("session_id", " ");
                                        intent.putExtra("otpf","no");
                                        startActivity(intent);
                                        finish();

                                    }


                                }

                            } else {
                                Toast.makeText(this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else
                            Toast.makeText(this, "INVALID USER DETAILS", Toast.LENGTH_SHORT).show();
                    } else if (guiResponse instanceof SocialRegisterResponse) {
                        SocialRegisterResponse registerResponse = (SocialRegisterResponse) guiResponse;
                        if (registerResponse != null) {
                            if (registerResponse.isStatus()) {

                                try {
                                    FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                    FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                                } catch (Exception ex) {

                                }

                                DataBaseHelper dataBaseHelper = new DataBaseHelper(LoginSignupActivity.this);
                                dataBaseHelper.deleteAllTables();

                                if (registerResponse.getLoginModel() != null && registerResponse.getLoginModel().isProfileComplete())
                                    PreferenceUtils.setProfileDone(LoginSignupActivity.this);

                                String displayName = registerResponse.getLoginModel().getDisplayName();
                                if (TextUtils.isEmpty(displayName))
                                    displayName = registerResponse.getLoginModel().getFirstName();
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, displayName);
                                PreferenceUtils.setCandidateId(registerResponse.getLoginModel().get_id(), LoginSignupActivity.this);
                                if (registerResponse.getLoginModel() != null) {
                                    LoginModel loginModel = registerResponse.getLoginModel();
                                    long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                    String dob = loginModel.getDateOfBirth();
                                    if (!TextUtils.isEmpty(dob)) {
                                        try {
                                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                        } catch (Exception ex) {

                                        }
                                    }
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                    if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());


                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());


                                  //  List<EducationalDetails> educationalDetailses = registerResponse.getLoginModel().getCurrentEducation();
                                    List<Professional> professionals = registerResponse.getLoginModel().getProfessionalExperience();
                                  /*  if (educationalDetailses != null) {
                                        for (EducationalDetails details : educationalDetailses) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }

                                            details.setFrom(date);
                                            Dates end = details.getEndDate();

                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createEducation(details);
                                        }
                                    }*/
                                    if (professionals != null) {
                                        for (Professional details : professionals) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }
                                            details.setFrom(date);

                                            Dates end = details.getEndDate();
                                            try {
                                                if (end != null && end.getMonth() < 13) {
                                                    int index = end.getMonth();
                                                    index--;
                                                    date = months[index] + "-" + String.valueOf(end.getYear());
                                                }
                                            } catch (Exception ex) {

                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createProfessionlDetails(details);
                                        }
                                    }
                                }

                                if (PreferenceUtils.isProfileDone(LoginSignupActivity.this)) {
                                    Intent intent = new Intent(LoginSignupActivity.this, HomeActivity.class);
                                    intent.putExtra("tabs","normal");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                Toast.makeText(this, "Username already exists", Toast.LENGTH_SHORT).show();
                            }
                        } else
                            Toast.makeText(this, "Server issuse", Toast.LENGTH_SHORT).show();


                    } else if (guiResponse instanceof SocialLoginResponse) {
                        SocialLoginResponse loginResponse = (SocialLoginResponse) guiResponse;
                        if (loginResponse != null) {
                            if (loginResponse.isStatus()) {

                                try {
                                    FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                    FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                                } catch (Exception ex) {

                                }

                                DataBaseHelper dataBaseHelper = new DataBaseHelper(LoginSignupActivity.this);
                                dataBaseHelper.deleteAllTables();

                                if (loginResponse.getLoginModel() != null && loginResponse.getLoginModel().isProfileComplete())
                                    PreferenceUtils.setProfileDone(LoginSignupActivity.this);


                                String displayName = loginResponse.getLoginModel().getDisplayName();
                                if (TextUtils.isEmpty(displayName))
                                    displayName = loginResponse.getLoginModel().getFirstName();
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, displayName);
                                PreferenceUtils.setCandidateId(loginResponse.getLoginModel().get_id(), LoginSignupActivity.this);

                                if (loginResponse.getLoginModel() != null) {
                                    PreferenceUtils.setVerified(LoginSignupActivity.this);
                                    LoginModel loginModel = loginResponse.getLoginModel();
                                    long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                    String dob = loginModel.getDateOfBirth();
                                    if (!TextUtils.isEmpty(dob)) {
                                        try {

                                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                        } catch (Exception ex) {

                                        }
                                    }


                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());
                                    if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());

                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());


                              //      List<EducationalDetails> educationalDetailses = loginResponse.getLoginModel().getCurrentEducation();
                                    List<Professional> professionals = loginResponse.getLoginModel().getProfessionalExperience();
                                 /*   if (educationalDetailses != null) {
                                        for (EducationalDetails details : educationalDetailses) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }

                                            details.setFrom(date);
                                            Dates end = details.getEndDate();

                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createEducation(details);
                                        }
                                    }
                                 */   if (professionals != null) {
                                        for (Professional details : professionals) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }
                                            details.setFrom(date);

                                            Dates end = details.getEndDate();
                                            try {
                                                if (end != null && end.getMonth() < 13) {
                                                    int index = end.getMonth();
                                                    index--;
                                                    date = months[index] + "-" + String.valueOf(end.getYear());
                                                }
                                            } catch (Exception ex) {

                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createProfessionlDetails(details);
                                        }
                                    }
                                }

                                if (PreferenceUtils.isProfileDone(LoginSignupActivity.this)) {
                                    Intent intent = new Intent(LoginSignupActivity.this, HomeActivity.class);
                                    intent.putExtra("tabs","normal");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                Intent intent = new Intent(LoginSignupActivity.this, SocialsignupActivity.class);
                                intent.putExtra("providertype", providerTypeStr);
                                startActivity(intent);
                                //  finish();
                            }
                        } else
                            Toast.makeText(this, "Server Pissue", Toast.LENGTH_SHORT).show();


                    }

                } else lodin();
                //Toast.makeText(this, "Server PPissue", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "NETWORK ISSUE", Toast.LENGTH_SHORT).show();

        }else {
            //signup proceed
            Log.e("signup","resp");
            if(signup_progress.isShowing()&&signup_progress!=null){
                signup_progress.dismiss();
            }
            if(guiResponse!=null) {
                if (status.equals(RequestStatus.SUCCESS)) {

                    if (guiResponse instanceof ForgetPasswordResponse) {
                        ForgetPasswordResponse response = (ForgetPasswordResponse) guiResponse;
                        if (response.isStatus()) {

                            Intent intent = new Intent(LoginSignupActivity.this, LoginSignupActivity.class);
                            intent.putExtra("otpf","no");
                            intent.putExtra("phn", phone.getText().toString());
                            intent.putExtra("email", emailS.getText().toString());
                            intent.putExtra("username", username.getText().toString());
                            intent.putExtra("password", passwordS.getText().toString());
                            intent.putExtra("id", response.get_id());
                            intent.putExtra("session_id", "jok");
                            intent.putExtra("from", "signup");
                            startActivity(intent);


                        } else {
                            if (!TextUtils.isEmpty(response.getMessage()))
                                Toast.makeText(LoginSignupActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(LoginSignupActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                        }

                    } else if (guiResponse instanceof SendOtpResponse) {
                        SendOtpResponse sendOtpResponse = (SendOtpResponse) guiResponse;
                        if (sendOtpResponse.isStatus()) {
                            Intent intent = new Intent(LoginSignupActivity.this, EnterOtpActivity.class);
                            intent.putExtra("otpf","yes");
                            intent.putExtra("phn", phone.getText().toString());
                            intent.putExtra("email", emailS.getText().toString());
                            intent.putExtra("username", username.getText().toString());
                            intent.putExtra("password", passwordS.getText().toString());
                            intent.putExtra("session_id", sendOtpResponse.getDetails());
                            intent.putExtra("from", "signup");
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //login
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }
    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    public String getAndroidVersion() {
        String release = android.os.Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.e("RELEASE",release);
        return release;
    }

    public String getDevice() {
        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

        return deviceName;
    }
    @Override
    public void onBackPressed() {

    }
    private void dologin() {
        hideKeyboard();
        if(connectionUtils.isConnectionAvailable()){

            login_progrss.show();

            final CheckInternetTask t912=new CheckInternetTask();
            t912.execute();

            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    t912.cancel(true);

                    if (flag_netcheck) {
                        flag_netcheck = false;
                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(LoginSignupActivity.this, LoginSignupActivity.this);
                        processor.doLogin(email.getText().toString(), password.getText().toString(), FirebaseInstanceId.getInstance().getToken(), "0", "ANDROID", getAndroidVersion(), getDevice());

                    }else {
                        login_progrss.dismiss();
                        flag_netcheck=false;
                        Toast.makeText(LoginSignupActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    }
                }
            }.start();

        }else {
            Toast.makeText(LoginSignupActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
        }

    }

    private void lodin(){
        Intent intent = new Intent(LoginSignupActivity.this, SocialsignupActivity.class);
        intent.putExtra("type","social");
        intent.putExtra("providertype", providerTypeStr);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ispause) {
            if (currentscreen.equalsIgnoreCase("loginscreen")) {
                logincard.performClick();

            } else {
                signupcard.performClick();
            }
        }
        ispause=false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        ispause=true;
    }


    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)LoginSignupActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

}

