package com.globalgyan.getvalyou;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.globalgyan.getvalyou.interfaces.GUICallback;

import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by techniche-android on 17/1/17.
 */

public class IntAdapter extends RecyclerView.Adapter<IntAdapter.ViewHolder> {
    private Context context;
    private List<String> notificationses=null;
    private List<String> ll = null;
    private List<String> lls = null;
    private FragmentManager fragmentManager = null;
    private GUICallback guiCallback = null;



    public IntAdapter(Context context, List<String> articleList,List<String> message) {
        this.context = context;
        this.notificationses = articleList;
        this.ll=message;
       // this.lls=messag;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.game_below_list, null);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //final Notifications notifications = notificationses.get(position);

        holder.title.setText(notificationses.get(position));
        holder.description.setText(ll.get(position));
        holder.intc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HomeActivity().mTabLayout.getTabAt(1).select();

            }
        });



//        if ( notifications.getNotificationType().equalsIgnoreCase("Interaction") ) {
//            holder.notificationImage.setImageResource(R.drawable.rocket_ship);
//        } else if ( notifications.getNotificationType().equalsIgnoreCase("Video Response") ) {
//            holder.notificationImage.setImageResource(R.drawable.rocket_ship);
//        } else {
//            holder.notificationImage.setImageResource(R.drawable.rocket_ship);
//        }



       /* holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( !TextUtils.isEmpty(notifications.getStatus()) && notifications.getStatus().equalsIgnoreCase("Notified") ) {
                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(context, guiCallback);
                    processor.updateNotificationStatus(notifications.get_id());
                }
                if ( !TextUtils.isEmpty(notifications.getMessage()) && notifications.getMessage().contains("cancelled") ) {
                    Intent intent = new Intent(context, InteractionsActivity.class);
                    intent.putExtra("objectId", notifications.getObjectId());
                    intent.putExtra("status", "cancelled");
                    int ncount= PreferenceUtils.getNotificationCount(context);
                    ncount=--ncount;
                    PreferenceUtils.setNotification(context,ncount);
                    context.startActivity(intent);
                } else {
                    if ( notifications.getNotificationType().equalsIgnoreCase("Video Response") ) {
                        Intent intent = new Intent(context, VideoPerformanceActivity.class);
                        intent.putExtra("objectId", notifications.getObjectId());
                        int ncount= PreferenceUtils.getNotificationCount(context);
                        ncount=--ncount;
                        PreferenceUtils.setNotification(context,ncount);
                        context.startActivity(intent);
                    } else if ( notifications.getNotificationType().equalsIgnoreCase("Interaction") ) {
                        Intent intent = new Intent(context, InteractionsActivity.class);
                        intent.putExtra("objectId", notifications.getObjectId());
                        int ncount= PreferenceUtils.getNotificationCount(context);
                        ncount=--ncount;
                        PreferenceUtils.setNotification(context,ncount);
                        context.startActivity(intent);

                    } else if ( notifications.getNotificationType().equalsIgnoreCase("Learning") ) {
                        Log.e("----notificationMsg",notifications.getMessage()+"");

                        if ( !TextUtils.isEmpty(notifications.getMessage()) && notifications.getMessage().contains("Articles") ) {
                            int ncount= PreferenceUtils.getNotificationCount(context);
                            ncount=--ncount;
                            PreferenceUtils.setNotification(context,ncount);

                        } else if ( !TextUtils.isEmpty(notifications.getMessage()) && notifications.getMessage().contains("Books") ) {
                            int ncount= PreferenceUtils.getNotificationCount(context);
                            ncount=--ncount;
                            PreferenceUtils.setNotification(context,ncount);

                        } else {
                            int ncount= PreferenceUtils.getNotificationCount(context);
                            Log.i("COUNT",ncount+"");
                            ncount=--ncount;
                            Log.i("COUNT",ncount+"");
                            PreferenceUtils.setNotification(context,ncount);

                        }


                    } else if ( notifications.getNotificationType().equalsIgnoreCase("Game") ) {
                        int ncount= PreferenceUtils.getNotificationCount(context);
                        Log.i("COUNT",ncount+"");
                        ncount=ncount-1;
                        Log.i("COUNT",ncount+"");
                        PreferenceUtils.setNotification(context,ncount);
                        int nncount= PreferenceUtils.getNotificationCount(context);
                        Log.i("COUNT",nncount+"");
                    }
                }


            }
        });
*/
    }


    @Override
    public int getItemCount() {
        return notificationses == null ? 0 : notificationses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FontTextView title;
        FontTextView description;
        ImageView notificationImage;
        LinearLayout intc;


        public ViewHolder(View itemView) {
            super(itemView);
            title = (FontTextView) itemView.findViewById(R.id.notititle);
            description = (FontTextView) itemView.findViewById(R.id.notidesc);
            notificationImage = (ImageView) itemView.findViewById(R.id.notiimage);
            intc=(LinearLayout)itemView.findViewById(R.id.intclick);
        }
    }
}
