package com.globalgyan.getvalyou;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.globalgyan.getvalyou.ListeningGame.MAQ;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.Activity_Fib.blinking_anim;


public class Fib_adapter extends RecyclerView.Adapter<Fib_adapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<String> options_array;
    Typeface typeface1;
    static boolean clicked = false;
    long mLastClickTime = 0;
    String check="";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView option;
        ImageView options;
        RelativeLayout audio_options;
        LinearLayout maq_options;

        public MyViewHolder(View view) {
            super(view);

            option = (FontTextView) view.findViewById(R.id.option_is_clickable);
            options=(ImageView)view.findViewById(R.id.a_opt);
            audio_options=(RelativeLayout)view.findViewById(R.id.audio_options);
            maq_options=(LinearLayout) view.findViewById(R.id.ol1);

        }
    }



    public Fib_adapter(Context mContext, ArrayList<String> options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.audio_option_clickable, parent, false);



        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Typeface typeface3 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");

        holder.option.setTypeface(typeface3);
        holder.option.setText(options_array.get(position));

       /* if(position==0){
            holder.option.setTextSize(17);

        }else {
            holder.option.setTextSize(16);
        }*/



        if(position==0){
            holder.options.setVisibility(View.GONE);

        }
        else if(position == 1){
            holder.options.setImageResource(R.drawable.option_a);

        }
        else if(position == 2){
            holder.options.setImageResource(R.drawable.option_b);

        }
        else if(position == 3){
            holder.options.setImageResource(R.drawable.option_c);

        }
        else if(position == 4){
            holder.options.setImageResource(R.drawable.option_d);

        }
        else if(position == 5){
            holder.options.setImageResource(R.drawable.opt_e);

        }


        holder.maq_options.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(position==0){
                    return true;

                }
                if(clicked){
                    return true;
                }

                else {
                    return false;
                }
            }


        });
        holder.maq_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                clicked = true;
               /* try{
                    blinking_anim.cancel();
                }
                catch (Exception e){
                    e.printStackTrace();
                }*/
                Activity_Fib.mp_click.start();
                holder.maq_options.setClickable(false);
                holder.maq_options.setEnabled(false);
//                                                 holder.option.setEnabled(false);
//                                                 holder.option.setClickable(false);

                //  MAQ.rc_options_non_clickable.addOnItemTouchListener(MAQ.disabler);

                new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        clicked=false;

                        //   MAQ.rc_options_non_clickable.removeOnItemTouchListener(MAQ.disabler);

                        holder.maq_options.setEnabled(true);
                        holder.maq_options.setClickable(true);
//                                                         holder.option.setEnabled(true);
//                                                         holder.option.setClickable(true);
                    }
                }.start();
                int pos = position-1;
                if (Activity_Fib.crct == pos) {
                    holder.options.setBackgroundResource(R.drawable.round_ques_ans_green);

                    Log.e("ans", "correct"+ Activity_Fib.crct);
                    Log.e("ans", "correct"+ pos);
                } else {

                    holder.options.setBackgroundResource(R.drawable.round_ques_ans_red);

                    Log.e("ans", "wrong" + Activity_Fib.crct);
                    Log.e("ans", "wrong" +pos);
                }
                Intent brodcast_receiver_screen=new Intent("Show_screen_fib");
                brodcast_receiver_screen.putExtra("pos", String.valueOf(pos));
                Log.e("broad", "msg" +pos);

                mContext.sendBroadcast(brodcast_receiver_screen);
            }
        });



    }

    @Override
    public int getItemCount() {
        return options_array.size();
    }
}

