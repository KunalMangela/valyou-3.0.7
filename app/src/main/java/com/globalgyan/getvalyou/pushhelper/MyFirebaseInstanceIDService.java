package com.globalgyan.getvalyou.pushhelper;

import android.util.Log;

import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{  
    @Override
    public void onTokenRefresh() {  
        // Get updated InstanceID token.  
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("Token", "Refreshed token: " + refreshedToken);
        // TODO: Implement this method to send any registration to your app's servers.  
        /// sendRegistrationToServer(refreshedToken);
        PreferenceUtils.setFcmToken(this,refreshedToken);
    }
}  