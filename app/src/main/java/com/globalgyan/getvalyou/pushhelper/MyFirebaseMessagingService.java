package com.globalgyan.getvalyou.pushhelper;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.globalgyan.getvalyou.AssessmentGroupsActivity;
import com.globalgyan.getvalyou.CourseLIstActivity;
import com.globalgyan.getvalyou.GetLearningGroupsActivity;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.MainPlanetActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.model.PushMessage;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String INTENT_FILTER = "VALYOU_INTENT_FILTER";
    private static final int NOTIFICATION_ID = 111;
    public static String navigationFrom="No_Notification";
    public static String ispause_delay="on";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.  
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
     //   Log.e("Message", "From: " + remoteMessage.getFrom());
        Log.e("Message", "Fromdata: " + remoteMessage.getData());

        if (remoteMessage.getData().size() > 0) {

            if(remoteMessage.getData().toString().contains("videoResponse")){

                Log.e("Push message", "VR");

                String [] msgarray=remoteMessage.getData().toString().split("=");
                String jsonableString=msgarray[1].substring(0, msgarray[1].length() - 1);
                Log.e("jsonableMessage", jsonableString);
                try {
                    if (AppConstant.ishome_active) {
                        ispause_delay="not_pause";
                    } else {
                        ispause_delay="pause";                }
                }catch (Exception e){
                    e.printStackTrace();
                    ispause_delay="pause";
                }
                String type,msg,courseid,certiid,coursename,courseauthor,lgid,keyvalue;

                try {
                    JSONObject jsonObjectis = new JSONObject(jsonableString);
                    JSONObject jsonObject = jsonObjectis.getJSONObject("data");
                    sendNotification(this, jsonObject.getString("Msg"));


                }catch (Exception e) {
                    e.printStackTrace();
                }

                  //  sendNotificationasslist(this,"view","200");


                /*    if(pushMessage.getCount()>0) {

                        int count = PreferenceUtils.getNotificationCount(this);
                        if (count > 0){
                            count += pushMessage.getCount();
                        }else
                            count = pushMessage.getCount();

                        Log.i("FCMCOUTN",count+"");
                        PreferenceUtils.setNotification(this,count);
                        Intent intent = new Intent(INTENT_FILTER);
                        intent.putExtra("count", String.valueOf(pushMessage.getCount()));
                        sendBroadcast(intent);
                    }*/




            }else {
                Log.e("Push message", "not VR");
                Log.e("insideMessage", remoteMessage.getData().toString());
                String [] msgarray=remoteMessage.getData().toString().split("=");
                String jsonableString=msgarray[1].substring(0, msgarray[1].length() - 1);
                Log.e("jsonableMessage", jsonableString);
                try {
                    if (AppConstant.ishome_active) {
                        ispause_delay="not_pause";
                    } else {
                        ispause_delay="pause";                }
                }catch (Exception e){
                    e.printStackTrace();
                    ispause_delay="pause";
                }
                 String type,msg,courseid,certiid,coursename,courseauthor,lgid,keyvalue;

                try {
                    JSONObject jsonObjectis=new JSONObject(jsonableString);
                    JSONObject jsonObject=jsonObjectis.getJSONObject("data");
                    type=jsonObject.getString("type");

                    //ass group
                    if(type.equalsIgnoreCase("assessmentgroup")){
                        sendNotificationassgroup(this,jsonObject.getString("Msg"));
                    }
                    //ass
                    if(type.equalsIgnoreCase("assessment")){
                        sendNotificationasslist(this,jsonObject.getString("Msg"),jsonObject.getString("id"));
                    }
                    //lg group
                    if(type.equalsIgnoreCase("learninggroup")){
                        sendNotificationlgGroup(this,jsonObject.getString("Msg"));

                    }
                    //course
                    if(type.equalsIgnoreCase("course")){
                        sendNotificationcourselist(this,jsonObject.getString("Msg"),jsonObject.getString("lg_id")
                        ,jsonObject.getString("course_author"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

             /*   keyvalue="course";

                if(keyvalue.equalsIgnoreCase("assessmentgroup")){
                    sendNotificationassgroup(this,"new ass group");
                }
                if(keyvalue.equalsIgnoreCase("assessment")){
                    sendNotificationasslist(this,"new ass","825");
                }
                if(keyvalue.equalsIgnoreCase("lggroup")){
                    sendNotificationlgGroup(this,"new lg group");
                }
                if(keyvalue.equalsIgnoreCase("course")){
                    sendNotificationcourselist(this,"new course","10");
                }
*/
            }

        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    //vr ntf
    public static void sendNotification(Context context, String message) {
        Log.e("message","push received");
        if(new PrefManager(context).getRetryresponseString().equalsIgnoreCase("notdone")){

        }else {
            Log.e("message","push received");
            try {
                Intent brodcast_receiver_screen=new Intent("refresh List");
                context.sendBroadcast(brodcast_receiver_screen);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        Intent intent = new Intent("click_notification");
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);


        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.e("androidversion","O");
            Intent notificationIntent = new Intent(context, HomeActivity.class);
            notificationIntent.putExtra("tabs","ntf_tab");
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent intentis = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);

            navigationFrom="Notification";

            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("my_channel_01",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
                mNotificationManager.createNotificationChannel(channel);

            Notification notification = new Notification.Builder(context, "my_channel_01")
                    .setContentTitle(context.getString(R.string.app_name))
                    .setColor(Color.parseColor("#5250B0"))
                    .setContentText("You have a new video response request.Good Luck!")
                    .setSmallIcon(R.drawable.ic_val)
                    .setChannelId("my_channel_01")
                    .setAutoCancel(true)
                    .setContentIntent(intentis)
                    .setSound(defaultSoundUri)
                    .build();

            try {
                if (AppConstant.ishome_active) {

                } else {
                    mNotificationManager.notify(1 , notification);                }
            }catch (Exception e){
                e.printStackTrace();
                mNotificationManager.notify(1 , notification);
            }



        }else {
            navigationFrom="No_Notification";

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.e("androidversion","Lollipop or above");
                notificationBuilder.setSmallIcon(R.drawable.ic_val)
                        .setColor(Color.parseColor("#5250B0"))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText("You have a new video response request.Good Luck!")
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            } else {
                Log.e("androidversion","Lollipop below");
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText("You have a new video response request.Good Luck!")
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }


            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


            try {
                if (AppConstant.ishome_active) {

                } else
                    {
                        try {

                            notificationManager.cancel(NOTIFICATION_ID);
                        } catch (Exception ex) {
                            Log.e("notification Exce", "" + ex.getMessage());
                        }
                        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

                    }
            }catch (Exception e){
                e.printStackTrace();
                try {

                    notificationManager.cancel(NOTIFICATION_ID);
                } catch (Exception ex) {
                    Log.e("notification Exce", "" + ex.getMessage());
                }
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

            }



        }
    }



    //ass group
    public static void sendNotificationassgroup(Context context, String pushMessage) {
        Log.e("message","other than vr push received");

        Intent intent = new Intent("open_assessment_group_list");
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);



        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.e("androidversion","O");
            Intent notificationIntent = new Intent(context, AssessmentGroupsActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent intentis = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);


            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("my_channel_01",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);

            Notification notification = new Notification.Builder(context, "my_channel_01")
                    .setContentTitle(context.getString(R.string.app_name))
                    .setColor(Color.parseColor("#5250B0"))
                    .setContentText(pushMessage)
                    .setSmallIcon(R.drawable.ic_val)
                    .setChannelId("my_channel_01")
                    .setAutoCancel(true)
                    .setContentIntent(intentis)
                    .setSound(defaultSoundUri)
                    .build();

            mNotificationManager.notify(1 , notification);



        }else {


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.e("androidversion","Lollipop or above");
                notificationBuilder.setSmallIcon(R.drawable.ic_val)
                        .setColor(Color.parseColor("#5250B0"))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            } else {
                Log.e("androidversion","Lollipop below");
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }


            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            try {
                notificationManager.cancel(NOTIFICATION_ID);
            } catch (Exception ex) {
                Log.e("notification Exce", "" + ex.getMessage());
            }
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());




        }
    }

    //ass list
    public static void sendNotificationasslist(Context context, String pushMessage, String id) {
        Log.e("message","other than vr push received");

        Intent intent = new Intent("open_assessment_list");
        intent.putExtra("ass_ce_id",id);
        intent.putExtra("pushis", "yes");

        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);



        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.e("androidversion","O");
            Intent notificationIntent = new Intent(context, HomeActivity.class);
            notificationIntent.putExtra("tabs", "normal");
            notificationIntent.putExtra("pushis", "yes");
            notificationIntent.putExtra("ass_ce_id", id);




            // intent.putExtra("navigatefrom", "mainplanet");

            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (context instanceof Activity) {
                ((Activity) context).finish();
                ((Activity) context).startActivity(notificationIntent);

            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent intentis = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);


            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("my_channel_01",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);

            Notification notification = new Notification.Builder(context, "my_channel_01")
                    .setContentTitle(context.getString(R.string.app_name))
                    .setColor(Color.parseColor("#5250B0"))
                    .setContentText(pushMessage)
                    .setSmallIcon(R.drawable.ic_val)
                    .setChannelId("my_channel_01")
                    .setAutoCancel(true)
                    .setContentIntent(intentis)
                    .setSound(defaultSoundUri)
                    .build();

            mNotificationManager.notify(1 , notification);



        }else {


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.e("androidversion","Lollipop or above");
                notificationBuilder.setSmallIcon(R.drawable.ic_val)
                        .setColor(Color.parseColor("#5250B0"))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            } else {
                Log.e("androidversion","Lollipop below");
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }


            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            try {

                notificationManager.cancel(NOTIFICATION_ID);
            } catch (Exception ex) {
                Log.e("notification Exce", "" + ex.getMessage());
            }
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());




        }
    }

    //lg group
    public static void sendNotificationlgGroup(Context context, String pushMessage) {
        Log.e("message","other than vr push received");

        Intent intent = new Intent("open_lg_group_list");
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);



        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.e("androidversion","O");
            Intent notificationIntent = new Intent(context, GetLearningGroupsActivity.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent intentis = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);


            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("my_channel_01",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);

            Notification notification = new Notification.Builder(context, "my_channel_01")
                    .setContentTitle(context.getString(R.string.app_name))
                    .setColor(Color.parseColor("#5250B0"))
                    .setContentText(pushMessage)
                    .setSmallIcon(R.drawable.ic_val)
                    .setChannelId("my_channel_01")
                    .setAutoCancel(true)
                    .setContentIntent(intentis)
                    .setSound(defaultSoundUri)
                    .build();

            mNotificationManager.notify(1 , notification);



        }else {


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.e("androidversion","Lollipop or above");
                notificationBuilder.setSmallIcon(R.drawable.ic_val)
                        .setColor(Color.parseColor("#5250B0"))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            } else {
                Log.e("androidversion","Lollipop below");
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }


            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            try {

                notificationManager.cancel(NOTIFICATION_ID);
            } catch (Exception ex) {
                Log.e("notification Exce", "" + ex.getMessage());
            }
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());




        }
    }

    //course
    public static void sendNotificationcourselist(Context context, String pushMessage, String id,String author) {
        Log.e("message","other than vr push received");

        Intent intent = new Intent("open_course_list");
        intent.putExtra("course_id_push",id);
        intent.putExtra("pushis", "yes");

        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);



       // new PrefManager(context).saveLearningGroups(String.valueOf("11"));

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.e("androidversion","O");
            Intent notificationIntent = new Intent(context, CourseLIstActivity.class);
            notificationIntent.putExtra("course_id_push",id);
            notificationIntent.putExtra("pushis","yes");

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent intentis = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);


            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("my_channel_01",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);

            Notification notification = new Notification.Builder(context, "my_channel_01")
                    .setContentTitle(context.getString(R.string.app_name))
                    .setColor(Color.parseColor("#5250B0"))
                    .setContentText(pushMessage)
                    .setSmallIcon(R.drawable.ic_val)
                    .setChannelId("my_channel_01")
                    .setAutoCancel(true)
                    .setContentIntent(intentis)
                    .setSound(defaultSoundUri)
                    .build();

            mNotificationManager.notify(1 , notification);



        }else {


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.e("androidversion","Lollipop or above");
                notificationBuilder.setSmallIcon(R.drawable.ic_val)
                        .setColor(Color.parseColor("#5250B0"))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            } else {
                Log.e("androidversion","Lollipop below");
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(pushMessage)
                        // .setContentText("Test")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }


            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            try {

                notificationManager.cancel(NOTIFICATION_ID);
            } catch (Exception ex) {
                Log.e("notification Exce", "" + ex.getMessage());
            }
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());




        }
    }

}  