package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.ListeningGame.MAQ;
import com.globalgyan.getvalyou.MasterSlaveGame.MSQ;
import com.globalgyan.getvalyou.MasterSlaveGame.New_MSQ;
import com.globalgyan.getvalyou.VideoGame.VideoGameActivity;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 27/09/17.
 */

public class AssessLanding extends AppCompatActivity {

    ImageView play;
    FontTextView title,assessdescription,owl_status;
    String uid,adn,ceid,gdid,game,cate,tgid,token,aid,descripton,time,grp,tg_groupid,certifcation_id,course_id,lg_id;
    ImageView assessicon;
    FrameLayout main_rl_bg;
    ImageView back,image_astro;
    FrameLayout main_ass_landing_banner;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;


    Dialog audio_check;
    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2 ,astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;
    Bitmap bitmap, decodedByte;
    FaceServiceClient faceServiceClient;
    PrefManager prefManager;
    String profileImageUrl;
    private DataBaseHelper dataBaseHelper = null;
    Dialog alertDialogis;
    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    private AudioManager mAudioManager;
    int media_current_volume;
    int media_max_volume;

    int pic_frequency;
    MediaPlayer mp;
    SeekBar volume_seek;

    Dialog alertDialog;
    Dialog verification_progress;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assess_landing2);

        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        appConstant = new AppConstant(this);
        prefManager = new PrefManager(AssessLanding.this);
        dataBaseHelper = new DataBaseHelper(this);

        faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(AssessLanding.this).get_frkey());

        //checkIfProfilePicAvailable();


        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        mp = new MediaPlayer();


        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

//        media_current_volume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//
//         media_max_volume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        int mid_volume = media_max_volume/2;


        if(media_current_volume<mid_volume){



            //  Toast.makeText(getApplicationContext(),"Please turn up the volume",Toast.LENGTH_LONG).show();
        }

        verification_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        verification_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verification_progress.setContentView(R.layout.planet_loader);
        Window window1 = verification_progress.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        verification_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        verification_progress.setCancelable(false);
        GifView gifView2 = (GifView) verification_progress.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();



        title=(FontTextView)findViewById(R.id.assesstitle);
      //  owl_status=(FontTextView)findViewById(R.id.owl_status);
        main_ass_landing_banner=(FrameLayout)findViewById(R.id.main_ass_landing_banner);
        assessdescription=(FontTextView)findViewById(R.id.assessdescription);
        play=(ImageView)findViewById(R.id.assessplay);
        image_astro=(ImageView)findViewById(R.id.image_astro);
        //assessicon=(ImageView)findViewById(R.id.assessicon);
        back=(ImageView)findViewById(R.id.backbtnis);
        back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    back.setBackgroundColor(Color.parseColor("#33ffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            back.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }*/
                return false;
            }
        });
        main_rl_bg=(FrameLayout)findViewById(R.id.relative_bg);

        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);

     //   play.setEnabled(false);

        Animation animation_fade1 =
                AnimationUtils.loadAnimation(AssessLanding.this,
                        R.anim.fadeing_out_animation_planet);
        main_ass_landing_banner.startAnimation(animation_fade1);
        image_astro.startAnimation(animation_fade1);
        back.startAnimation(animation_fade1);
        new CountDownTimer(1000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                main_ass_landing_banner.setVisibility(View.VISIBLE);
                image_astro.setVisibility(View.VISIBLE);
                back.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {

            }
        }.start();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PrefManager(AssessLanding.this).saveAsstype("Ass");
                Intent intent = new Intent(AssessLanding.this, HomeActivity.class);
                intent.putExtra("tabs", "normal");

                new PrefManager(AssessLanding.this).set_back_nav_from("ass_land");
                 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });
        try{
             uid= getIntent().getExtras().getString("uidg");
             adn= getIntent().getExtras().getString("adn");
             ceid= getIntent().getExtras().getString("ceid");
             gdid= getIntent().getExtras().getString("gdid");
             game= getIntent().getExtras().getString("game");
             cate= getIntent().getExtras().getString("category");
             tgid = getIntent().getExtras().getString("tgid");
             token = getIntent().getExtras().getString("token");
             aid= getIntent().getExtras().getString("aid");
             grp=getIntent().getStringExtra("assessgroup");
             descripton=getIntent().getExtras().getString("description_is");
             time=getIntent().getExtras().getString("time_is");

            tg_groupid = getIntent().getExtras().getString("TG_GroupId");
            certifcation_id = getIntent().getExtras().getString("certification_id");
            course_id = getIntent().getExtras().getString("course_id");
            lg_id = getIntent().getExtras().getString("lg_id");
            pic_frequency=getIntent().getIntExtra("pic_frequency",3);

            title.setText(adn);
        }catch (Exception ex){

        }
        assessdescription.setText(descripton);
        Log.e("Desc",descripton);
        new PrefManager(AssessLanding.this).saveTargetid(String.valueOf(ceid));
       /* if(descripton.contains("the Game")){
            String ss=descripton.replace("the Game",adn);
            assessdescription.setText(ss);
        }else {
            assessdescription.setText(descripton);
        }*/

        /*if(adn.equalsIgnoreCase("finance")){
            assessicon.setImageResource(R.drawable.business_awareness_icon);
            main_rl_bg.setBackgroundR esource(R.drawable.green_gradient);
            play.setTextColor(Color.parseColor("#00BB61"));
           // owl_status.setTextColor(Color.parseColor("#00BB61"));
        }else
        if(adn.equalsIgnoreCase("Listening")){
            assessicon.setImageResource(R.drawable.listening_skills_icon);
            main_rl_bg.setBackgroundResource(R.drawable.bluishgreen_gradient);
            play.setTextColor(Color.parseColor("#01D2FF"));
           // owl_status.setTextColor(Color.parseColor("#01D2FF"));
        }else
        if(adn.equalsIgnoreCase("Verbal Ability")){
            assessicon.setImageResource(R.drawable.writing_brevity_icon);
            main_rl_bg.setBackgroundResource(R.drawable.pinkishvoilet_gradient);
            play.setTextColor(Color.parseColor("#BD08E3"));
          //  owl_status.setTextColor(Color.parseColor("#BD08E3"));
        }else
        if(adn.equalsIgnoreCase("Listening skills")){
            assessicon.setImageResource(R.drawable.listening_skills_icon);
            main_rl_bg.setBackgroundResource(R.drawable.bluishgreen_gradient);
            play.setTextColor(Color.parseColor("#01D2FF"));
          //  owl_status.setTextColor(Color.parseColor("#01D2FF"));
        }else {
            assessicon.setImageResource(R.drawable.data_interpretation_icon);
            main_rl_bg.setBackgroundResource(R.drawable.yellow_gradient);
            play.setTextColor(Color.parseColor("#FECB00"));
           // owl_status.setTextColor(Color.parseColor("#FECB00"));
        }*/
     /*   GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {0xFFFF0000,0xFFFFFFFF});
        gd.setCornerRadius(0f);
        main_rl_bg.setBackground(gd);*/
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                play.setEnabled(true);
                    }
                }.start();
                ConnectionUtils connectionUtils=new ConnectionUtils(AssessLanding.this);
                if(connectionUtils.isConnectionAvailable()){

                    try{
                        new PrefManager(AssessLanding.this).saveTargetid(String.valueOf(ceid));
                   /* String uid= getIntent().getExtras().getString("uidg");
                    String adn= getIntent().getExtras().getString("adn");
                    String ceid= getIntent().getExtras().getString("ceid");
                    String gdid= getIntent().getExtras().getString("gdid");
                    String game= getIntent().getExtras().getString("game");
                    String cate= getIntent().getExtras().getString("category");
                    String tgid = getIntent().getExtras().getString("tgid");
                    String token = getIntent().getExtras().getString("token");
                    String aid= getIntent().getExtras().getString("aid");
*/
                        Log.e("RADIO",game);


                        if(game.equalsIgnoreCase("Radio Tuner")){

                            media_current_volume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

                            media_max_volume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

                            Log.e("Game",game+cate);
                            Log.e("RADIO",game);
                            final Intent it = new Intent(AssessLanding.this,MAQ.class);
                            it.putExtra("uidg", uid);
                            it.putExtra("adn", adn);
                            it.putExtra("ceid", ceid);
                            it.putExtra("gdid", gdid);
                            it.putExtra("category", cate);
                            it.putExtra("game", game);
                            it.putExtra("tgid",tgid);
                            it.putExtra("token",token);
                            it.putExtra("aid",aid);
                            it.putExtra("timeis",time);

                            it.putExtra("TG_GroupId",tg_groupid);
                            it.putExtra("certification_id",certifcation_id);
                            it.putExtra("course_id",course_id);
                            it.putExtra("lg_id",lg_id);


                            it.putExtra("pic_frequency",pic_frequency);

                            it.putExtra("assessgroup",getIntent().getExtras().getString("assessgroup"));
                            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                                Log.e("volume level", ""+media_current_volume);

                                try {

                                    audio_check = new Dialog(AssessLanding.this, android.R.style.Theme_Translucent_NoTitleBar);
                                    audio_check.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    audio_check.setContentView(R.layout.increase_volume);
                                    Window window = audio_check.getWindow();
                                    WindowManager.LayoutParams wlp = window.getAttributes();

                                    wlp.gravity = Gravity.CENTER;
                                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                                    window.setAttributes(wlp);
                                    audio_check.setCancelable(false);


                                    audio_check.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);




                                    volume_seek = (SeekBar) audio_check.findViewById(R.id.mySeekBar);

                                    volume_seek.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));


                                    ImageView close=(ImageView)audio_check.findViewById(R.id.closeb);
                                    close.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            audio_check.dismiss();
                                            new PrefManager(AssessLanding.this).saveAsstype("Ass");
                                            Intent intent = new Intent(AssessLanding.this, HomeActivity.class);
                                            intent.putExtra("tabs", "normal");

                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);

                                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                        }
                                    });
                                    volume_seek.setProgress(media_current_volume);
                                    audio_check.setOnKeyListener(new DialogInterface.OnKeyListener() {
                                        @Override
                                        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                            switch (i) {
                                                case KeyEvent.KEYCODE_VOLUME_UP:
                                                    mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                                                            AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
                                                    int volume_index = volume_seek.getProgress();


                                                    volume_seek.setProgress(volume_index + 1);
                                                    Log.e("volume up", "up");
                                                    return true;
                                                case KeyEvent.KEYCODE_VOLUME_DOWN:
                                                    mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                                                            AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);

                                                    int volume_index1 = volume_seek.getProgress();


                                                    volume_seek.setProgress(volume_index1 - 1);

                                                    Log.e("volume down", "down");

                                                    return true;
                                             /*   default:
                                                    // return false;
                                                    // Update based on @Rene comment below:
                                                    return super.*/
                                            }
                                            return false;
                                        }
                                    });

                                    volume_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                        @Override
                                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                                            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
                                        }

                                        @Override
                                        public void onStartTrackingTouch(SeekBar seekBar) {

                                        }

                                        @Override
                                        public void onStopTrackingTouch(SeekBar seekBar) {

                                        }
                                    });




                                    final ImageView astro = (ImageView) audio_check.findViewById(R.id.astro);
                                    final ImageView alien = (ImageView) audio_check.findViewById(R.id.alien);

                                    if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);

                                        astro_outx.setDuration(300);
                                        astro_outx.setInterpolator(new LinearInterpolator());
                                        astro_outx.setStartDelay(0);
                                        astro_outx.start();
                                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                                        astro_outy.setDuration(300);
                                        astro_outy.setInterpolator(new LinearInterpolator());
                                        astro_outy.setStartDelay(0);
                                        astro_outy.start();


                                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                                        alien_outx.setDuration(300);
                                        alien_outx.setInterpolator(new LinearInterpolator());
                                        alien_outx.setStartDelay(0);
                                        alien_outx.start();
                                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                                        alien_outy.setDuration(300);
                                        alien_outy.setInterpolator(new LinearInterpolator());
                                        alien_outy.setStartDelay(0);
                                        alien_outy.start();

                                    }
                                    final ImageView yes = (ImageView) audio_check.findViewById(R.id.yes_quit);


                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            yes.setEnabled(false);
                                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                                astro_inx.setDuration(300);
                                                astro_inx.setInterpolator(new LinearInterpolator());
                                                astro_inx.setStartDelay(0);
                                                astro_inx.start();
                                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                                astro_iny.setDuration(300);
                                                astro_iny.setInterpolator(new LinearInterpolator());
                                                astro_iny.setStartDelay(0);
                                                astro_iny.start();

                                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                                alien_inx.setDuration(300);
                                                alien_inx.setInterpolator(new LinearInterpolator());
                                                alien_inx.setStartDelay(0);
                                                alien_inx.start();
                                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                                alien_iny.setDuration(300);
                                                alien_iny.setInterpolator(new LinearInterpolator());
                                                alien_iny.setStartDelay(0);
                                                alien_iny.start();
                                            }
                                            new CountDownTimer(400, 400) {


                                                @Override
                                                public void onTick(long l) {

                                                }

                                                @Override
                                                public void onFinish() {

                                               audio_check.dismiss();
                                                    startActivity(it);
                                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                                    finish();
                                                }
                                            }.start();


                                        }
                                    });
                                    final FontTextView test_audio = (FontTextView)audio_check.findViewById(R.id.test_audio);

                                    test_audio.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            yes.setEnabled(false);
                                            test_audio.setEnabled(false);

                                            mp = MediaPlayer.create(getBaseContext(), R.raw.test_audio_track);

                                                        mp.start();

                                                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                            @Override
                                                            public void onCompletion(MediaPlayer mediaPlayer) {
                                                                mp.release();
                                                                yes.setEnabled(true);
                                                                test_audio.setEnabled(true);
                                                            }
                                                        });

                                        }
                                    });
                                    try {
                                        audio_check.show();

                                    } catch (Exception e) {
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                    /*startActivity(it);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                finish();*/


                        }else if(game.equalsIgnoreCase("msq")){
                            Log.e("Game",game+cate);
                            Log.e("RADIO",game);
                            Intent it = new Intent(AssessLanding.this,New_MSQ.class);
                            it.putExtra("uidg", uid);
                            it.putExtra("adn", adn);
                            it.putExtra("ceid", ceid);
                            it.putExtra("gdid", gdid);
                            it.putExtra("category", cate);
                            it.putExtra("game", game);
                            it.putExtra("tgid",tgid);
                            it.putExtra("token",token);
                            it.putExtra("aid",aid);
                            it.putExtra("timeis",time);
                            it.putExtra("TG_GroupId",tg_groupid);

                            it.putExtra("certification_id",certifcation_id);
                            it.putExtra("course_id",course_id);
                            it.putExtra("lg_id",lg_id);
                            it.putExtra("pic_frequency",pic_frequency);

                            it.putExtra("assessgroup",getIntent().getExtras().getString("assessgroup"));
                            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(it);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            finish();
                        }else if(game.equalsIgnoreCase("vr")){
                            new PrefManager(AssessLanding.this).setvideoFrom("as a game");
                            Log.e("Game",game+cate);
                            Log.e("RADIO",game);
                            Intent it = new Intent(AssessLanding.this,VideoGameActivity.class);
                            it.putExtra("uidg", uid);
                            it.putExtra("adn", adn);
                            it.putExtra("ceid", ceid);
                            it.putExtra("gdid", gdid);
                            it.putExtra("category", cate);
                            it.putExtra("game", game);
                            it.putExtra("tgid",tgid);
                            it.putExtra("token",token);
                            it.putExtra("aid",aid);
                            it.putExtra("timeis",time);
                            it.putExtra("TG_GroupId",tg_groupid);
                            it.putExtra("certification_id",certifcation_id);
                            it.putExtra("course_id",course_id);
                            it.putExtra("lg_id",lg_id);
                            it.putExtra("assessgroup",getIntent().getExtras().getString("assessgroup"));
                            it.putExtra("objectId", "as a game");
                            it.putExtra("pic_frequency",pic_frequency);

                            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(it);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            finish();
                        }else{
                            Log.e("Game",game+cate);
                            Intent it = new Intent(AssessLanding.this,Activity_Fib.class);
                            it.putExtra("uidg", uid);
                            it.putExtra("timeis",time);
                            it.putExtra("adn", adn);
                            it.putExtra("ceid", ceid);
                            it.putExtra("gdid", gdid);
                            it.putExtra("category", cate);
                            it.putExtra("game", game);
                            it.putExtra("tgid",tgid);
                            it.putExtra("token",token);
                            it.putExtra("aid",aid);
                            it.putExtra("TG_GroupId",tg_groupid);
                            it.putExtra("certification_id",certifcation_id);
                            it.putExtra("course_id",course_id);
                            it.putExtra("lg_id",lg_id);
                            it.putExtra("assessgroup",getIntent().getExtras().getString("assessgroup"));
                            it.putExtra("pic_frequency",pic_frequency);
                            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(it);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            finish();
                        }
//                        Intent it = new Intent(AssessLanding.this,Check.class);
//                        it.putExtra("uidg", uid);
//                        it.putExtra("adn", adn);
//                        it.putExtra("ceid", ceid);
//                        it.putExtra("gdid", gdid);
//                        it.putExtra("aid",aid);
//                        it.putExtra("category", cate);

//                        it.putExtra("game", game);
//                        it.putExtra("tgid",tgid);
//                        it.putExtra("token",token);
//                        startActivity(it);
//                        finish();

                    }catch (Exception ex){

                    }

                }else {
                    Toast.makeText(AssessLanding.this,"please check your internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            startanimation();
        }


    }
    public void checkIfProfilePicAvailable(){
        try {
            profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
        }catch (Exception e){
            e.printStackTrace();
            //   profileImageUrl="";
        }
        try {
            if (prefManager.getPic() == " ") {

                if((profileImageUrl!="")||(profileImageUrl!=null)){


                    new DownloadImage().execute(AppConstant.BASE_URL+"candidateMobileReadImage/"+profileImageUrl);
                    //not present
                    //showProfilePicDialogue();
                }else {
                    //not present in shared data
                    // showProfilePicDialogue();


                }
            }else if(!prefManager.is_detection_done()){
                byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                detectpic(decodedByte);
            } else {
                        //present
              /*  byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                detectpic(decodedByte);*/
            }
        }catch (Exception e){
            e.printStackTrace();
            //    showProfilePicDialogue();
        }


    }
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];


            try {
                // Download Image from URL
                InputStream input = new URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if(result!=null) {
                // Set the bitmap into ImageView
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                result.compress(Bitmap.CompressFormat.PNG, 0, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                Log.e("ENCID", encodedImage);
                prefManager.savePic(encodedImage);
                Log.e("EN", prefManager.getPic());
                byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                detectpic(decodedByte);
            }
            else{

                showProfilePicDialogue();

            }
            // dialog.cancel();
        }
    }
    private void detectpic(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {

                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;


                        }


                        catch (Exception e) {
                            publishProgress("Detection failed");

                            Log.e("detection failed","Detection failed");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (verification_progress.isShowing()) {
                                            verification_progress.dismiss();
                                        }
                                    }catch (Exception e){

                                    }
                                    try {
                                        if (alertDialog.isShowing()) {

                                        } else {
                                            showGetQuesAgainDialogue();
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        showGetQuesAgainDialogue();
                                    }


                                }
                            });
                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                        super.onPreExecute();
                        try {
                            if(verification_progress!=null && !verification_progress.isShowing())
                            verification_progress.show();
                        }catch (Exception e){
                            showGetQuesAgainDialogue();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                        try {
                        if (result != null && result.length == 0) {
                            //    Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                            Log.e("mainplanetactivity","No face detected in mainplanetactivity");
                            verification_progress.dismiss();
                            //   sl.setVisibility(View.VISIBLE);

                            //show dialog again if no face is detected in available profile pic
                            showProfilePicDialogue();
                        }

                        else if(result != null && result.length>1){

                            Log.e("mainplanetactivity","No face detected in mainplanetactivity");
                            verification_progress.dismiss();
                            //   sl.setVisibility(View.VISIBLE);

                            //show dialog again if no face is detected in available profile pic
                            showProfilePicDialogue();

                        }else {
                            //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("mainplanetactivity","face detected in mainplanetactivity");
                            Log.e("result null?", ""+result);


                                verification_progress.dismiss();
                                List<Face> faces;


                                assert result != null;
                                faces = Arrays.asList(result);
                                for (Face face : faces) {
                                    UUID mFaceId1 = face.faceId;
                                    String faceid = mFaceId1.toString();


                                    prefManager.detectionDone(true);
                                    prefManager.setprofilefaceid(faceid);
                                    //txt.setText(faceid);
                                    // Log.e("fid",mFaceId1+"  "+mface);
                                    play.setEnabled(true);
                                }
                            }

                        }catch (Exception e){
                            try {
                                if (verification_progress.isShowing()) {
                                    verification_progress.dismiss();
                                }
                            }catch (Exception ex){

                            }
                            try {
                                if (alertDialog.isShowing()&&alertDialog!=null) {

                                } else {
                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception exx){
                                exx.printStackTrace();
                                showGetQuesAgainDialogue();
                            }

                        }


                    }
                };
        detectTask.execute(inputStream);
    }
    public void showProfilePicDialogue(){
        alertDialogis = new Dialog(AssessLanding.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialogis.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogis.setContentView(R.layout.compulsary_profile_pic);
        Window window = alertDialogis.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialogis.setCancelable(false);

        alertDialogis.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        Typeface tf1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        TextView tv = (TextView) alertDialogis.findViewById(R.id.text_profile_compulsary);
        final ImageView im_pic=(ImageView)alertDialogis.findViewById(R.id.profile_pic_compulsary);
        ImageView close = (ImageView) alertDialogis.findViewById(R.id.close);
        tv.setTypeface(tf1,Typeface.BOLD);
        try{
            alertDialogis.show();
        }
        catch (Exception e)
        {}        im_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                im_pic.setEnabled(false);

                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        im_pic.setEnabled(true);
                    }
                }.start();
               //alertDialogis.dismiss();
                Intent it = new Intent(AssessLanding.this,ProPic.class);
                it.putExtra("fromwhere","assesslanding");
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PrefManager(AssessLanding.this).saveAsstype("Ass");
                Intent intent = new Intent(AssessLanding.this, HomeActivity.class);
                intent.putExtra("tabs", "normal");

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });

    }

    public void startanimation(){
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }
    @Override
    public void onBackPressed() {

    }
   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
                 volume_index = volume_seek.getProgress();

                 volume_increase = volume_index +1;
                volume_seek.setProgress(volume_index + 1);
                Log.e("volume up", "up");
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);

                 volume_index = volume_seek.getProgress();


                volume_seek.setProgress(volume_index - 1);

                Log.e("volume down", "down");

                return true;
            default:
                // return false;
                // Update based on @Rene comment below:
                return super.onKeyDown(keyCode, event);
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
        try{
            if(alertDialogis!=null && alertDialogis.isShowing()){

                alertDialogis.dismiss();
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        checkIfProfilePicAvailable();
    }

    public void showGetQuesAgainDialogue() {


      /*  final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                MSQ.this, R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(MSQ.this);
        View dialogView = inflater.inflate(R.layout.retry_anim_dialogue, null);

        dialogBuilder.setView(dialogView);


        alertDialog = dialogBuilder.create();


//                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

        alertDialog.show();*/
        alertDialog = new Dialog(AssessLanding.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();

                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(AssessLanding.this);
                        if (connectionUtils.isConnectionAvailable()) {
                            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            detectpic(decodedByte);
                        } else {
                            verification_progress.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (verification_progress.isShowing() && verification_progress != null) {
                                        verification_progress.dismiss();


                                    }
                                    showGetQuesAgainDialogue();
                                }
                            }.start();
                        }

                    }
                }.start();




            }
        });

        try {
            if(!alertDialog.isShowing())
                alertDialog.show();
        }
        catch (Exception e){

        }
    }


}
