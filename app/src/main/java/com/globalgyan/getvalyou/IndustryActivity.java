package com.globalgyan.getvalyou;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.cms.response.GetIndustryResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Industry;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.ArrayList;
import java.util.List;

import de.morrox.fontinator.FontEditText;
import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 15/10/17.
 */

public class IndustryActivity extends AppCompatActivity implements GUICallback {
    private FontTextView txtNext = null;
    private FontEditText txtSearch = null;
    private IndustryAdapter industryAdapter = null;
    private RecyclerView recyclerView = null;
    private List<Industry> industries = null;
    private String selected = null;
    ConnectionUtils connectionUtils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer);
        connectionUtils=new ConnectionUtils(IndustryActivity.this);
        txtNext = (FontTextView) findViewById(R.id.txtNext);
        txtSearch = (FontEditText) findViewById(R.id.txtSearch);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(IndustryActivity.this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }

        TextView txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMessage = new Intent();
                setResult(RESULT_CANCELED ,intentMessage);
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });

        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(IndustryActivity.this,IndustryActivity.this);
        processor.getIndustries();


        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(industryAdapter.getStringBuffer())) {
                    Intent intentMessage = new Intent();
                    intentMessage.putExtra("INDUSTRIES", industryAdapter.getStringBuffer());
                    setResult(RESULT_OK, intentMessage);

                    if (getParent() == null) {
                        setResult(RESULT_OK, intentMessage);
                    } else {
                        getParent().setResult(RESULT_OK, intentMessage);
                    }
                    finish();
                } else {
                    Toast.makeText(IndustryActivity.this, "Please select one ", Toast.LENGTH_SHORT).show();
                }

            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
                charSequence = charSequence.toString().trim().toLowerCase();
                if (industries != null ) {
                    final List<Industry> filteredList = new ArrayList<>();
                    boolean itemFound = false;
                    for (int i = 0; i < industries.size(); i++) {

                        final String text = industries.get(i).getName().toLowerCase();
                        if (text.contains(charSequence)) {
                            itemFound = true;
                            filteredList.add(industries.get(i));
                        }
                    }
                  /*  if (!itemFound) {
                        Industry college = new Industry();
                        college.setName(charSequence.toString());
                        college.setId("new");
                        selected = charSequence.toString();
                        filteredList.add(college);
                    }*/
                    industryAdapter = new IndustryAdapter(filteredList, IndustryActivity.this,selected);
                    recyclerView.setAdapter(industryAdapter);
                    industryAdapter.notifyDataSetChanged();  // data set changed
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {

        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {
                if (guiResponse.isStatus()) {
                    GetIndustryResponse response = (GetIndustryResponse) guiResponse;
                    if (response != null && response.getIndustryList() != null) {
                        industries = response.getIndustryList();

                        industryAdapter = new IndustryAdapter(industries, this, selected);
                        recyclerView.setAdapter(industryAdapter);
                    }
                }
            } else {
                Toast.makeText(IndustryActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(IndustryActivity.this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    public void createAlertDialogue(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(IndustryActivity.this);
        builder1.setTitle("Warning");
        builder1.setMessage("Your phone has no internet connection!");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
