package com.globalgyan.getvalyou;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.transition.Fade;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.globalgyan.getvalyou.ListeningGame.MAQ;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.bottomnavigation.CustomViewPager;
import com.globalgyan.getvalyou.cms.request.ProfilePicUploadRequest;
import com.globalgyan.getvalyou.cms.request.VideoUploadRequest;
import com.globalgyan.getvalyou.cms.response.GetCountResponse;
import com.globalgyan.getvalyou.cms.response.GetUserDetailsResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.fragments.AssessFragment;
import com.globalgyan.getvalyou.fragments.MoreFragment;
import com.globalgyan.getvalyou.fragments.NotificationFragment;
import com.globalgyan.getvalyou.fragments.Performance;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.helper.VideoCompressor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.interfaces.LogoutHandler;
import com.globalgyan.getvalyou.interfaces.UpdateProfileVideo;
import com.globalgyan.getvalyou.interfaces.VideoReziser;
import com.globalgyan.getvalyou.model.UploadResponse;
import com.globalgyan.getvalyou.pushhelper.MyFirebaseMessagingService;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.thefinestartist.Base;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;

import static android.os.Environment.getExternalStoragePublicDirectory;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

/**
 * Created by NaNi on 08/09/17.
 */

public class HomeActivity extends AppCompatActivity implements GUICallback,LogoutHandler,ActivityCompat.OnRequestPermissionsResultCallback,UploadStatusDelegate,VideoReziser {
    public final static int REQUEST_CHECK_SETTINGS = 2000;

    Location mLastloc;
    String latitude,longitude;
    GoogleApiClient mGoogleApiClient;

    public static String from_navigate="";
   public static String lat_long;
   String value_api="";
   public static boolean assess=false,notf=false,more=false;

    static public BatteryManager bm;
   static public int batLevel;
    DataReceivedListener listener;

   static public AppConstant appConstant;

    public static boolean isPowerSaveMode;
    public static PowerManager pm;
    public interface DataReceivedListener {
        void onReceived();
        void loadlist();
    }
    public void setDataReceivedListener(DataReceivedListener listener) {
        this.listener = listener;
    }

    private static final String USER_AGENT = "ValYou/" + BuildConfig.VERSION_NAME;
    private static final String IMAGE_DIRECTORY_NAME = "valyou";
    String now;
    String newPath = AppConstant.mediaStorageDir.getPath() + File.separator + "new0.mp4";

    private UpdateProfileVideo updateProfileVideo = null;
    Dialog dialog;
    int progg;
    URL ppp;
    int percentage=10;
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {


        @Override
        public void onReceive(Context context, Intent intent) {
            String count = intent.getExtras().getString("count");
            if (!TextUtils.isEmpty(count)) {
                try {
                    int notiCount = Integer.parseInt(count);
                    if (notiCount > 0) {
                        notificationCount.setVisibility(View.VISIBLE);
                        int notcount = PreferenceUtils.getNotificationCount(HomeActivity.this);
                        count=count+notcount;
                        notificationCount.setText(count);
                    }
                } catch (Exception e) {

                }

            }
        }
    };

    private BroadcastReceiver myReceiverntf = new BroadcastReceiver() {


        @Override
        public void onReceive(Context context, Intent intent) {
            new NotificationFragment().changentfcount();
        }
    };
    ConnectionUtils connectionUtils;
    NotificationManager mNotifyManager;
    private Builder build;
    int id = 1;
    String emailokk;
    long timeInMillisec;
    String profileImageUrl;
    AlertDialog alertDialogis;
    DefaultHttpClient httpClient;
    HttpPost httpPost;
    HttpGet httpGet;
    public static String tg_group="";
    FontTextView timerrr, dqus,qno;
    public static Handler mHandler = new Handler();
    int cqa=0;
    String frompush="no";
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    int prevcount;
    static JSONObject jObj = null,jObjeya=null;
    JSONArray data;
    public boolean showProfileDialogue=false;
    JSONObject dataJSONobject;
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    public  static FontTextView badge;

    Bitmap bitmap, decodedByte;


    PrefManager prefManager;

   public static String ipaddress;


    FaceServiceClient faceServiceClient;
    public static boolean apiloading=false;
    public static int pr;
    String uploadFile;
    File filepro;
    String candid;
    Bitmap rotatedBitmp = null;
    boolean wtf=false;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    ImageView icon;
    int wel=0;
    public static boolean comp_flag=false;
    FontTextView title;
    Bitmap profilepic;
    private DrawerLayout drawer = null;
    public static TabLayout mTabLayout;
    private static int REQUEST_CODE_NOTICATION = 555;
    private LinearLayout footer = null;
    private int learningTabId;
    private RelativeLayout headerLayout = null;
    private Fragment currentFragment = null;
    private DataBaseHelper dataBaseHelper = null;
    private TextView notificationCount = null;
    public static String token="";
    public static String token_is="";
    private int[] mTabsIcons = {
            R.drawable.tab_a,
            R.drawable.tab_v,
            R.drawable.tab_n,
            R.drawable.tab_m};
    Uri imageUri;
    private Uri mUriPhotoTaken;
    File flepro;

    public static String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.e("RELEASE",release);
        return release;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CODE_NOTICATION) {
                int index = data.getIntExtra("index", 0);
                int learningIndex = data.getIntExtra("learningIndex", 0);
                Log.e("----LearningIndex", learningIndex + "");

            }
            if(requestCode==0){
                imageUri = mUriPhotoTaken;
                try {
                    profilepic = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    try{
//
//                        File out = new File(getFilesDir(), "newImage.jpg");
//                        if(!out.exists()) {
//
//                            Toast.makeText(getBaseContext(),
//
//                                    "Error while capturing image", Toast.LENGTH_LONG)
//
//                                    .show();
//
//                            return;
//
//                        }

                        ExifInterface ei = new ExifInterface(filepro.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED);


                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotatedBitmp = rotateImage(profilepic, 90);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotatedBitmp = rotateImage(profilepic, 180);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotatedBitmp = rotateImage(profilepic, 270);
                                break;

                            case ExifInterface.ORIENTATION_NORMAL:
                            default:
                                rotatedBitmp = profilepic;
                        }
//                        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//                        flepro = File.createTempFile("IMG_11", ".jpg", storageDir);
//
//                        OutputStream os;
//                        try {
//                            os = new FileOutputStream(flepro);
//                            profilepic.compress(Bitmap.CompressFormat.JPEG, 0, os);
//                            os.flush();
//                            os.close();
//                        } catch (Exception e) {
//                            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
//                        }
                        final int nh = (int) (profilepic.getHeight() * (512.0 / profilepic.getWidth()));
                        final Bitmap scaled = Bitmap.createScaledBitmap(profilepic, 512, nh, true);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();
                        PrefManager prefManager = new PrefManager(HomeActivity.this);
                        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                        Log.e("ENCID",encodedImage);
                        prefManager.savePic(encodedImage);

                    }catch (IOException e) {

                    }
                    try {
                        String candidateid= PreferenceUtils.getCandidateId(HomeActivity.this);
                        Log.e("CANKALBLB",candidateid);
                        MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, "http://admin.getvalyou.com/api/user/"+candidateid+"/candidateProfilePicUpload");
                        request.addFileToUpload(filepro.getAbsolutePath(), "profilePic");

                        request.setCustomUserAgent(USER_AGENT);
                        request.setUtf8Charset();
                        request.setMethod("POST");
                        request.setAutoDeleteFilesAfterSuccessfulUpload(false);
                        request.setUsesFixedLengthStreamingMode(true);
                        request.setMaxRetries(3);
                        request.startUpload();
                        Log.e("lcbslbs","");

                    } catch (FileNotFoundException exc) {

                    } catch (IllegalArgumentException exc) {

                    } catch (MalformedURLException exc) {

                    }

                }catch(IOException e){

                 }
//                ByteArrayOutputStream output = new ByteArrayOutputStream();
//                rotatedBitmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
//                ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
//                new DetectionTask().execute(inputStream);
            }
        }else{
            takePic();
        }
    }

    private class DetectionTask extends AsyncTask<InputStream, String, Face[]> {
        // Index indicates detecting in which of the two images.
        private int mIndex;
        private boolean mSucceed = true;



        @Override
        protected Face[] doInBackground(InputStream... params) {
            // Get an instance of face service client to detect faces in image.
            FaceServiceClient faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points,new PrefManager(HomeActivity.this).get_frkey());
            try{
                publishProgress("Detecting...");

                // Start detection.
                Face[] res= faceServiceClient.detect(
                        params[0],  /* Input stream of image to detect */
                        true,       /* Whether to return face ID */
                        false,       /* Whether to return face landmarks */
                        /* Which face attributes to analyze, currently we support:
                           age,gender,headPose,smile,facialHair */
                        null);
                if(res!=null){
                    wtf=true;
                }else wtf=false;

                return res;
            }  catch (Exception e) {
                mSucceed = false;
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(String... progress) {
            Log.e("PROGRESS",progress[0]);

        }

        @Override
        protected void onPostExecute(Face[] result) {
            Log.e("WTF",wtf+"");
            Log.e("FCID",result.length+""+wtf);
            if(result.length==1){

            }else{
                takePic();
            }
        }
    }

    public  File createDir() {
        File DIR=null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
            DIR=new File(Environment.getExternalStorageDirectory()+"valyou2");
        else
            DIR=this.getCacheDir();
        if(!DIR.exists())
            DIR.mkdirs();
        return DIR;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

        public void takePic() {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getPackageManager()) != null) {
                // Save the photo taken to a temporary file.
                ContextWrapper cw = new ContextWrapper(HomeActivity.this);
              File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
              // File storageDir = createDir();
                try {
                 final String hnow = Calendar.getInstance().getTime().toString();
                   filepro = File.createTempFile("IMG_"+hnow, ".jpg", storageDir);
                    if ( filepro.exists() && filepro.canWrite() )
                        filepro.delete();
                    String ffffff= filepro.getAbsolutePath();
                   mUriPhotoTaken = Uri.fromFile(filepro);

                   // mUriPhotoTaken=getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,mUriPhotoTaken);
                    startActivityForResult(intent, 0);
                } catch (IOException e) {

                }
            }


//            try {
//                File f = new File(
//                        Environment.getExternalStorageDirectory(),
//                        tempfileName);
//                if ( f.exists() && f.canWrite() )
//                    f.delete();
//                Uri imageUri = Uri.fromFile(f);
//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.putExtra(MediaStore.EXTRA_OUTPUT,
//                        imageUri);
//                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
//
//                startActivityForResult(intent, CAPTURE_PICTURE_REQUESTCODE);
//
//
//            } catch (ActivityNotFoundException e) {
//            } catch (Exception e) {
//            }
        }

    /*
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WAKE_LOCK ,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION

        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }
    PrefManager pf;
    Camera camera;
    String valyouu,tab_value="";

   public static String versionName;


/*
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .enableAutoManage(this */
/* FragmentActivity *//*
,
                        this */
/* OnConnectionFailedListener *//*
)

                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        @SuppressLint("RestrictedApi") LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
// All location settings are satisfied. The client can initialize location requests here

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
// Show the dialog by calling startResolutionForResult(),
// and check the result in onActivityResult().
                            status.startResolutionForResult(HomeActivity.this, REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
// Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });


    }
*/
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    protected void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        Log.e("Androidversion",value_api);
                        Fade slide = new Fade();
                        slide.setDuration(500);
                        getWindow().setEnterTransition(slide);
                        setContentView(R.layout.activity_home);
                        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                        isPowerSaveMode = pm.isPowerSaveMode();
                        appConstant = new AppConstant(this);
                        Base.initialize(this);
                   //  buildGoogleApiClient();
                        try {
                            value_api = getIntent().getStringExtra("tabs");
                            Log.e("crashfrom-",value_api);

                            if (value_api.equalsIgnoreCase("more_tab")) {
                                more = true;
                                notf = false;
                                assess = false;
                            }
                            if (value_api.equalsIgnoreCase("ntf_tab")) {
                                more = false;
                                notf = true;
                                assess = false;
                            }
                            if (value_api.equalsIgnoreCase("normal")) {
                                more = false;
                                notf = false;
                                assess = true;

                                frompush=getIntent().getStringExtra("pushis");
                                try{
                                    Log.e("val",frompush);
                                }catch (Exception e){
                                    e.printStackTrace();
                                    frompush="no";
                                }


                                try {
                                    if (frompush.equalsIgnoreCase("yes")) {
                                        new PrefManager(HomeActivity.this).saveRatingtype("assessment_group");
                                        new PrefManager(HomeActivity.this).saveAsstype("Ass");
                                        new PrefManager(HomeActivity.this).saveNav("Ass");
                                        Log.e("valid",getIntent().getStringExtra("ass_ce_id"));
                                        new PrefManager(HomeActivity.this).saveAssgroupId(getIntent().getStringExtra("ass_ce_id"));

                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                                try{
                                    Log.e("frompushHome",frompush);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            }

                        }catch(Exception e){
                            e.printStackTrace();
                            Log.e("crashfrom-","homeexc");

                        }

                        //      Toast.makeText(getApplicationContext(),""+lat_long,Toast.LENGTH_LONG).show();

//                        if (mGoogleApiClient == null) {
//                            mGoogleApiClient = new GoogleApiClient.Builder(this)
//                                    .addConnectionCallbacks(this)
//                                    .addOnConnectionFailedListener(this)
//                                    .addApi(LocationServices.API)
//                                    .build();
//                        }
//                        mGoogleApiClient.connect();
                        prefManager = new PrefManager(HomeActivity.this);
                        faceServiceClient =
                                new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(HomeActivity.this).get_frkey());

                         versionName = BuildConfig.VERSION_NAME;
                        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                        ipaddress = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

                        AppConstant.ishome_active=true;
                        badge=(FontTextView)findViewById(R.id.badge);
                        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
                        int width = metrics.widthPixels;
                        int height = metrics.heightPixels;
                        final FrameLayout.LayoutParams layoutparams = (FrameLayout.LayoutParams)badge.getLayoutParams();
                        layoutparams.setMarginStart((int) (width*0.63));
                        badge.setLayoutParams(layoutparams);
                        connectionUtils=new ConnectionUtils(HomeActivity.this);
                        if(Build.VERSION.SDK_INT>=23) {
                            checkPermissions();
                        }
/*
                        try {
                            Log.e("HOMEISS",MyFirebaseMessagingService.navigationFrom);
                            valyouu = getIntent().getStringExtra("tabs");
                            if(valyouu.equalsIgnoreCase("ntf_tab")){
                                tab_value="ntf_tab";
                            }

                            if(valyouu.equalsIgnoreCase("more_tab")){
                                tab_value="more_tab";
                            }

                            if(new PrefManager(HomeActivity.this).getGetFirsttimegame().equalsIgnoreCase("first")){

                            }else {
                                if (MyFirebaseMessagingService.ispause_delay.equalsIgnoreCase("pause")) {
                                    valyouu = "not_normal";
                                } else {
                                    valyouu = "normal";
                                }
                            }
                            Log.e("HOMEISS","normal");
                        }catch (Exception e){
                            e.printStackTrace();
                            if(MyFirebaseMessagingService.navigationFrom.equalsIgnoreCase("Notification")){
                                valyouu="not_normal";
                            }else {
                                valyouu="normal";
                            }

                            Log.e("HOMEIS","exc");
                        }
*/

                        try{
                            String ososbos= getIntent().getStringExtra("assessg");
                            Log.e("HOME",ososbos);
                           /* AssessFragment assessFragmentf= (AssessFragment)getSupportFragmentManager().findFragmentById(R.layout.frag_assess);
                            assessFragmentf.updateA(ososbos);*/


                        }catch (Exception e){

                        }

                       // new GetEywa().execute();
                        pf= new PrefManager(HomeActivity.this);
                //        ValYouApplication application = (ValYouApplication) getApplication();
                        if(Build.VERSION.SDK_INT>=24){
                            try{
                                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                                m.invoke(null);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        candid= PreferenceUtils.getCandidateId(HomeActivity.this);
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,9);
                        calendar.set(Calendar.MINUTE,00);
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                        notificationIntent.addCategory("android.intent.category.DEFAULT");

                        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,broadcast);



                        registerReceiver(myReceiver, new IntentFilter(MyFirebaseMessagingService.INTENT_FILTER));
                        registerReceiver(myReceiverntf, new IntentFilter("ntf count"));
                        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                       PrefManager prefManager = new PrefManager(HomeActivity.this);

                //            if(prefManager.getPic()==""){
                //                final AlertDialog.Builder builder = new AlertDialog.Builder(
                //                        HomeActivity.this, R.style.AppCompatAlertDialogStyle);
                //                builder.setTitle("PROFILE PICTURE");
                //                builder.setMessage("Upload a profile picture to Continue..");
                //                builder.setCancelable(false);
                //                builder.setPositiveButton("OK",
                //                        new DialogInterface.OnClickListener() {
                //                            public void onClick(DialogInterface dialog,
                //                                                int which) {
                //                               takePic();
                //                            }
                //                        });
                //                final AlertDialog dialog = builder.create();
                //                dialog.show();
                //            }

                        Log.e("DATAA",date);
                        if (getIntent().getBooleanExtra("LOGOUT", false)) {
                            //do chnanges
                            Intent intent = new Intent(HomeActivity.this, Welcome.class);
                            intent.putExtra("gotologinpage","loginpage");

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            prefManager.savePic("");
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                            finish();
                        }
                        dataBaseHelper = new DataBaseHelper(    this);

                        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        headerLayout = (RelativeLayout) findViewById(R.id.headerLayout);
                        CustomViewPager viewPager = (CustomViewPager) findViewById(R.id.viewpagerhome);

                        viewPager.setOffscreenPageLimit(4);
                        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

                        if (viewPager != null) {
                            viewPager.setPagingEnabled(true);
                            viewPager.setAdapter(pagerAdapter);
                        }
                        footer = (LinearLayout) findViewById(R.id.footer);
                        try {
                            emailokk = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);

                        if (mTabLayout != null) {
                            mTabLayout.setupWithViewPager(viewPager);

                            for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                                if (tab != null) {
                                    tab.setCustomView(pagerAdapter.getTabView(i));

                                }
                            }
                            try {
                                value_api = getIntent().getStringExtra("tabs");
                                Log.e("androidversion",value_api);

                                if (value_api.equalsIgnoreCase("more_tab")) {
                                    more = true;
                                    notf = false;
                                    assess = false;
                                    mTabLayout.getTabAt(3).select();
                                    Log.e("androidversion","more");
                                }
                                if (value_api.equalsIgnoreCase("ntf_tab")) {
                                    more = false;
                                    notf = true;
                                    assess = false;
                                    mTabLayout.getTabAt(2).select();
                                    Log.e("androidversion","noti");
                                }
                                if (value_api.equalsIgnoreCase("normal")) {
                                    more = false;
                                    notf = false;
                                    assess = true;
                                    mTabLayout.getTabAt(0).select();
                                    Log.e("androidversion","assess");
                                    frompush=getIntent().getStringExtra("pushis");
                                    try{
                                        Log.e("val",frompush);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        frompush="no";
                                    }


                                    try {
                                        if (frompush.equalsIgnoreCase("yes")) {
                                            new PrefManager(HomeActivity.this).saveRatingtype("assessment_group");
                                            new PrefManager(HomeActivity.this).saveAsstype("Ass");
                                            new PrefManager(HomeActivity.this).saveNav("Ass");

                                            new PrefManager(HomeActivity.this).saveAssgroupId(getIntent().getStringExtra("ass_ce_id"));

                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    try{
                                        Log.e("frompushHome",frompush);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                }

                            }catch(Exception e){
                                e.printStackTrace();
                                Log.e("androidversion","exc");
                                more = false;
                                notf = true;
                                assess = false;
                                mTabLayout.getTabAt(2).select();

                            }

                            MyFirebaseMessagingService.navigationFrom="No_Notification";
                            MyFirebaseMessagingService.ispause_delay="on";
                        }
                        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                //tab.getIcon().setColorFilter(getResources().getColor(R.color.textinputotp), PorterDuff.Mode.SRC_IN);
                                if(tab.getPosition()==0){
                                    if(wel!=0){
                                        currentFragment = Performance.newInstance();
                                    }
                                    else
                                        wel++;
                                }
                                if(tab.getPosition()==2){

                                    try {
                                        if (AppConstant.ishome_active) {
                                            new PrefManager(HomeActivity.this).saventfcount(0);
                                            HomeActivity.badge.setVisibility(View.INVISIBLE);
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }

                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {
                                //tab.getIcon().clearColorFilter();
                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });
                        mTabLayout.setTabTextColors(R.color.coral_blue, R.color.dark_blue);

                        //checkIfProfilePicAvailable();


                    }
    public int getBackCameraResolutionInMp()
    {
        int noOfCameras = Camera.getNumberOfCameras();
        float maxResolution = -1;
        long pixelCount = -1;
        for (int i = 0;i < noOfCameras;i++)
        {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
            {
                Camera camera = Camera.open(i);;
                Camera.Parameters cameraParams = camera.getParameters();
                for (int j = 0;j < cameraParams.getSupportedPictureSizes().size();j++)
                {
                    long pixelCountTemp = cameraParams.getSupportedPictureSizes().get(j).width * cameraParams.getSupportedPictureSizes().get(j).height; // Just changed i to j in this loop
                    if (pixelCountTemp > pixelCount)
                    {
                        pixelCount = pixelCountTemp;
                        maxResolution = ((float)pixelCountTemp) / (1024000.0f);
                    }
                }

                camera.release();
            }
        }
        int k=(int) Math.ceil(maxResolution);
        return k;
    }

    @Override
    public void onLoggedOut() {
        mTabLayout.getTabAt(3).select();
    }


    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public final int PAGE_COUNT = 4;

        private final String[] mTabsTitle = {"Games","Contribute","Notification","More"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public View getTabView(int position) {
            View view = LayoutInflater.from(HomeActivity.this).inflate(R.layout.custom_tab, null);
            Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
            title = (FontTextView) view.findViewById(R.id.title);
            title.setTypeface(typeface);
            Log.e("tab txt", mTabsTitle[position] + "");
            title.setText(mTabsTitle[position]);
            icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(mTabsIcons[position]);
            return view;
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
//                    icon.setImageResource(R.drawable.v_copy);
//                    title.setTextColor(getResources().getColor(R.color.textinputotp));
                    currentFragment = AssessFragment.newInstance();
                    return currentFragment;

              /*  case 1:
                    currentFragment = LearningFragment.newInstance();
                    return currentFragment;*/

//                    icon.setImageResource(R.drawable.a_copy);
//                    title.setTextColor(getResources().getColor(R.color.textinputotp));

                case 1:
//                    icon.setImageResource(R.drawable.l_copy);
//                    title.setTextColor(getResources().getColor(R.color.textinputotp));

                currentFragment= Performance.newInstance();
                return currentFragment;

                case 2:
//                    icon.setImageResource(R.drawable.shape_copy_2);
//                    title.setTextColor(getResources().getColor(R.color.textinputotp));
                   //new PrefManager(HomeActivity.this).saventfcount(0);
                   /* Intent intent=new Intent("ntf count");
                    sendBroadcast(intent);*/
                   /*try {
                       if (AppConstant.ishome_active) {
                           new PrefManager(HomeActivity.this).saventfcount(0);
                           HomeActivity.badge.setVisibility(View.INVISIBLE);
                       }
                   }catch (Exception e){
                       e.printStackTrace();
                   }*/
                    currentFragment = NotificationFragment.newInstance();
                  //  new NotificationFragment().currentcount=0;
                    return currentFragment;

                case 3:
//                    icon.setImageResource(R.drawable.noun_1082098_cc_copy);
//                    title.setTextColor(getResources().getColor(R.color.textinputotp));
                    currentFragment = MoreFragment.newInstance();
                    return currentFragment;


            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }
    }
    @Override
    protected void onDestroy() {

                        if(pr>9){
                            new PrefManager(HomeActivity.this).saveRetryreponseString("done");
                            Log.e("mycondition", "done");
                        }
                        mHandler=null;
        AppConstant.ishome_active=false;
        Log.e("--upload", "ondestroy");
        try {
            unregisterReceiver(myReceiver);
            unregisterReceiver(myReceiverntf);
        } catch (Exception ex) {
            Log.e("Exp unregisterReceiver", "" + ex.getMessage());
        }

        try {
            Thread.currentThread();
            Thread.sleep(500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onDestroy();
    }
    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if (guiResponse != null) {
            if (status.equals(RequestStatus.SUCCESS)) {
                if (guiResponse instanceof GetUserDetailsResponse) {

                    GetUserDetailsResponse userDetailsResponse = (GetUserDetailsResponse) guiResponse;
                    if (userDetailsResponse != null) {
                        // listOfnotification = userDetailsResponse.getUserDetails().getNotificationList();
                       /* if (listOfnotification != null) {
                            if (listOfnotification.size() > 0) {
                                notificationCount.setVisibility(View.VISIBLE);
                                int count = 0;
                                for (Notifications notifications : listOfnotification) {

                                    if (notifications.getStatus().equalsIgnoreCase("Notified")) {
                                        count++;
                                    }
                                }

                                Log.e("count", count + "g");
                                if (count > 0) {
                                    notificationCount.setVisibility(View.VISIBLE);
                                    notificationCount.setText(String.valueOf(String.valueOf(count)));
                                } else {
                                    notificationCount.setVisibility(View.GONE);
                                }

                            } else {

                                notificationCount.setVisibility(View.GONE);
                            }
                        } else {
                            notificationCount.setVisibility(View.GONE);
                        }*/
                    }
                } else if (guiResponse instanceof GetCountResponse) {
                    GetCountResponse detailsResponse = (GetCountResponse) guiResponse;
                    if (detailsResponse != null) {
                        // PreferenceUtils.setNotification(HomeActivity.this, detailsResponse.getUnreadNotificationCount());
                       /* Intent intent = new Intent(MyFirebaseMessagingService.INTENT_FILTER);
                        intent.putExtra("count", String.valueOf(detailsResponse.getUnreadNotificationCount()));
                        sendBroadcast(intent);*/

                        if (detailsResponse.getUnreadNotificationCount() > 0) {
                            //notificationCount.setVisibility(View.VISIBLE);
                            //notificationCount.setText(String.valueOf(detailsResponse.getUnreadNotificationCount()));
                        } else
                            //notificationCount.setVisibility(View.GONE);

                            PreferenceUtils.setInteractionCount(HomeActivity.this, detailsResponse.getUpcomingInteractionCount());

                        if (!TextUtils.isEmpty(detailsResponse.getValyouScore())) {

                            PreferenceUtils.setValueInSharedPreference(HomeActivity.this, PreferenceUtils.PREFERNCE_KEY_VALYOU_SCORE, detailsResponse.getValyouScore());
//                            if (scoreUIUpdator != null)
//                                scoreUIUpdator.onScoreUpdated();

                            headerLayout.setBackgroundColor(ContextCompat.getColor(HomeActivity.this, R.color.action_bar_new));
                        } else {
                            PreferenceUtils.setValueInSharedPreference(HomeActivity.this, PreferenceUtils.PREFERNCE_KEY_VALYOU_SCORE, "0");
//                            if (scoreUIUpdator != null)
//                                scoreUIUpdator.onScoreUpdated();

                        }
                        String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
                        DataBaseHelper dataBaseHelper = new DataBaseHelper(HomeActivity.this);
                        if (detailsResponse.getVideos() != null && !TextUtils.isEmpty(detailsResponse.getVideos().getS3valyouinputbucket()))
                            dataBaseHelper.updateTableDetails(String.valueOf(candidateId), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, detailsResponse.getVideos().getS3valyouinputbucket());



                        /*GameAdapter gameAdapter = new GameAdapter(gameInfoList, gameUIUpdator, fragmentManager, getActivity(), detailsResponse.getUpcomingInteractionCount());
                        gamesRecycler.setAdapter(gameAdapter);*/

                    }
                }

            } else {
               // Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            //Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }
    public void setUpdateProfileVideo(UpdateProfileVideo updateProfileVideo) {
        this.updateProfileVideo = updateProfileVideo;
    }



    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {

        Log.e("---uploadInfoProgress", "" + uploadInfo.getProgressPercent());
    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, Exception exception) {
        Log.e("---onError", "" + exception.getMessage());
        Log.e("uploadInfo", "" + uploadInfo.getUploadId());
        Log.e("uploadInfo", "" + uploadInfo.toString());


        dataBaseHelper.updateVideoResponseStatus(uploadInfo.getUploadId());

    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
        Log.e("mytask","video response uploaded");
        pr=10;
        percentage=10;
        new CountDownTimer(3000, 300) {
            @Override
            public void onTick(long millisUntilFinished) {
                pr=percentage;
                build.setProgress(100,percentage, false);
                if (listener != null) {

                    listener.onReceived();
   /* ConnectionUtils connectionUtils=new ConnectionUtils(HomeActivity.this);
    if(connectionUtils.isConnectionAvailable()){
        listener.loadlist();
    }*/
                }else {
                    Log.e("refresh","error");
                }

                percentage=percentage+10;
            }
            @Override
            public void onFinish() {
                mNotifyManager.cancelAll();
                new PrefManager(HomeActivity.this).saveRetryreponseString("done");
                if (listener != null) {

                    listener.onReceived();
   /* ConnectionUtils connectionUtils=new ConnectionUtils(HomeActivity.this);
    if(connectionUtils.isConnectionAvailable()){
        listener.loadlist();
    }*/
                }else {
                    Log.e("refresh","error");
                }
            }
        }.start();

        Log.e("completed", String.format(Locale.getDefault(),
                "ID %1$s: completed in %2$ds at %3$.2f Kbit/s. Response code: %4$d, body:[%5$s]",
                uploadInfo.getUploadId(), uploadInfo.getElapsedTime() / 1000,
                uploadInfo.getUploadRate(), serverResponse.getHttpCode(),
                serverResponse.getBodyAsString()));
        try {
            UploadResponse uploadResponse = new Gson().fromJson(serverResponse.getBodyAsString(), UploadResponse.class);
            if (uploadResponse != null) {
                DataBaseHelper dataBaseHelper = new DataBaseHelper(HomeActivity.this);
                long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                if (id > 0) {
                    if (!TextUtils.isEmpty(uploadResponse.getVideoProfileId()))
                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, uploadResponse.getVideoProfileId());
                    if (!TextUtils.isEmpty(uploadResponse.getProfilePicId()))
                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, uploadResponse.getProfilePicId());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {

    }

    @Override
    public void onVideoResized(String filePath, int currentIndex, boolean queue) {

        Log.e("onVideoResizes", "filePath=" + filePath);
        Log.e("onVideoResizes", "currentIndex=" + currentIndex);
        Log.e("onVideoResizes", "queue=" + queue);


        if (queue) {

            //VIDEO RESPONSE
            List<String> videoQusts = dataBaseHelper.getVideoResponseQuestions();
            List<String> videoFiles = dataBaseHelper.getVideoResponseFiles();
            if (videoQusts != null && videoFiles != null && videoQusts.size() == videoFiles.size() && videoQusts.size() != 0) {
                if (currentIndex < videoFiles.size()) {
                    videoFiles.set(currentIndex, filePath);
                    currentIndex++;
                    if (currentIndex < videoFiles.size()) {
                        String newPath = AppConstant.mediaStorageDir.getPath() + File.separator + "new" + currentIndex + ".mp4";
                        new VideoCompressor(videoFiles.get(currentIndex), HomeActivity.this, newPath, currentIndex, true).execute();
                    } else {
                        String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
                        if (!TextUtils.isEmpty(candidateId)) {
                            String uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);
                            String url = AppConstant.BASE_URL + "candidateAppUpoadAnswers/" + uploadFile;
                            Log.e("video Upload ", "url" + url);
                            try {

//                                android.hardware.Camera.Parameters parameters = camera.getParameters();
//                                android.hardware.Camera.Size size = parameters.getPictureSize();
//
//
//                                int height = size.height;
//                                int width = size.width;
                                MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, url)

                                       // .setNotificationConfig(getNotificationConfig("Video Response"))
                                        .setCustomUserAgent(USER_AGENT)
                                        .setMethod("PUT")
                                        .setAutoDeleteFilesAfterSuccessfulUpload(false)
                                        .setUsesFixedLengthStreamingMode(true)
                                        .setMaxRetries(3);
                                for (int i = 0; i < videoFiles.size(); i++) {
                                    Log.e("Video path=", "" + videoFiles.get(i));
                                    request.addFileToUpload(videoFiles.get(i), videoQusts.get(i));
                                }
                                request.setUtf8Charset();
                                String uploadIDs = request.setDelegate(this).startUpload();
                                dataBaseHelper.updateUploadVideoId(uploadIDs, uploadFile);
                                Log.e("uploadIDsAudio-----", uploadIDs);
                            } catch (FileNotFoundException exc) {
                                showToast(exc.getMessage());
                            } catch (IllegalArgumentException exc) {
                                showToast("Missing some arguments. " + exc.getMessage());
                            } catch (MalformedURLException exc) {
                                showToast(exc.getMessage());
                            }


                        }

                        // AppConstant.videoResponseObjectId = null;
                        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                        if (id > 0) {
                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, "");
                        }
                    }
                }
            }


        } else {
            //VIDEO PROFILE
            //TODO hide loading bar

            PreferenceUtils.setProgress(false, HomeActivity.this);
            long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
            if (id > 0) {
                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_UPLOAD_VIDEO_PROFILE, "");
            }

            String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
            VideoUploadRequest videoUploadRequest = new VideoUploadRequest(candidateId);

            try {

                MultipartUploadRequest req = new MultipartUploadRequest(HomeActivity.this, videoUploadRequest.getUrl())
                        .addFileToUpload("/storage/emulated/0/Movies/valYou/videoProfile.mp4", "file")
                       // .setNotificationConfig(getNotificationConfig(getFilename("/storage/emulated/0/Movies/valYou/videoProfile.mp4")))
                        .setCustomUserAgent(USER_AGENT)
                        .setMethod("POST")
                        .setAutoDeleteFilesAfterSuccessfulUpload(false)
                        .setUsesFixedLengthStreamingMode(true)
                        .setMaxRetries(3);
                req.setUtf8Charset();
                String uploadID = req.setDelegate(this).startUpload();
                Log.e("uploadIDVideo-----", uploadID);
                DataBaseHelper dataBaseHelper = new DataBaseHelper(HomeActivity.this);
                String profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
                if (TextUtils.isEmpty(profileImageUrl)) {
                    if (FileUtils.isFileExistsSDCard(AppConstant.stillFile)) {
                        ProfilePicUploadRequest profilePicUploadRequest = new ProfilePicUploadRequest(candidateId);
                        MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, profilePicUploadRequest.getUrl())
                                .addFileToUpload("/storage/emulated/0/Movies/valYou/profile.png", "profilePic")
                               // .setNotificationConfig(getNotificationConfig(getFilename("/storage/emulated/0/Movies/valYou/profile.png")))
                                .setCustomUserAgent(USER_AGENT)
                                .setMethod("POST")
                                .setAutoDeleteFilesAfterSuccessfulUpload(false)
                                .setUsesFixedLengthStreamingMode(true)
                                .setMaxRetries(3);

                        request.setUtf8Charset();
                        String uploadIDs = request.setDelegate(this).startUpload();
                        Log.e("uploadIDsAudio-----", uploadIDs);
                    }
                }


            } catch (FileNotFoundException exc) {
                showToast(exc.getMessage());
            } catch (IllegalArgumentException exc) {
                showToast("Missing some arguments. " + exc.getMessage());
            } catch (MalformedURLException exc) {
                showToast(exc.getMessage());
            }
        }
    }


//    @Override
//    public void onVideoResized(String filePath, int currentIndex, boolean queue) {
//
//        Log.e("onVideoResizes", "filePath=" + filePath);
//        Log.e("onVideoResizes", "currentIndex=" + currentIndex);
//        Log.e("onVideoResizes", "queue=" + queue);
//
//
//        if (queue) {
//
//            //VIDEO RESPONSE
//            List<String> videoQusts = dataBaseHelper.getVideoResponseQuestions();
//            List<String> videoFiles = dataBaseHelper.getVideoResponseFiles();
//            if (videoQusts != null && videoFiles != null && videoQusts.size() == videoFiles.size() && videoQusts.size() != 0) {
//                if (currentIndex < videoFiles.size()) {
//                    videoFiles.set(currentIndex, filePath);
//                    currentIndex++;
//                    if (currentIndex < videoFiles.size()) {
//                        String newPath = AppConstant.mediaStorageDir.getPath() + File.separator + "new" + currentIndex + ".mp4";
//                       // new VideoCompressor(videoFiles.get(currentIndex), HomeActivity.this, newPath, currentIndex, true).execute();
//                    } else {
//                        String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
//                        if (!TextUtils.isEmpty(candidateId)) {
//
//
//
//                            String uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);
//                            String url = AppConstant.BASE_URL + "candidateAppUpoadAnswers/" + uploadFile;
//                            Log.e("video Upload ", "url" + url);
//                            try {
//                                MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, url)
//
//                                        .setNotificationConfig(getNotificationConfig("Video Response"))
//                                        .setCustomUserAgent(USER_AGENT)
//                                        .setMethod("PUT")
//                                        .setAutoDeleteFilesAfterSuccessfulUpload(false)
//                                        .setUsesFixedLengthStreamingMode(true)
//                                        .setMaxRetries(3);
//                                for (int i = 0; i < videoFiles.size(); i++) {
//                                    Log.e("Video path=", "" + videoFiles.get(i));
//                                    request.addFileToUpload(AppConstant.mediaStorageDir.getPath() + File.separator + "new" + currentIndex + ".mp4", videoQusts.get(i));
//                                }
//                                request.setUtf8Charset();
//                                String uploadIDs = request.setDelegate(this).startUpload();
//                                dataBaseHelper.updateUploadVideoId(uploadIDs, uploadFile);
//                                Log.e("uploadIDsAudio-----", uploadIDs);
//                            } catch (FileNotFoundException exc) {
//
//                            } catch (IllegalArgumentException exc) {
//
//                            } catch (MalformedURLException exc) {
//
//                            }
//
//
//                        }
//
//                        // AppConstant.videoResponseObjectId = null;
//                        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
//                        if (id > 0) {
//                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, "");
//                        }
//                    }
//                }
//            }
//
//
//        } else {
//            //VIDEO PROFILE
//            //TODO hide loading bar
//
//            PreferenceUtils.setProgress(false, HomeActivity.this);
//            long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
//            if (id > 0) {
//                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_UPLOAD_VIDEO_PROFILE, "");
//            }
//
//            String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
//            VideoUploadRequest videoUploadRequest = new VideoUploadRequest(candidateId);
//
//            try {
//
//                MultipartUploadRequest req = new MultipartUploadRequest(HomeActivity.this, videoUploadRequest.getUrl())
//                        .addFileToUpload("/storage/emulated/0/Movies/valYou/videoProfile.mp4", "file")
//                        .setNotificationConfig(getNotificationConfig(getFilename("/storage/emulated/0/Movies/valYou/videoProfile.mp4")))
//                        .setCustomUserAgent(USER_AGENT)
//                        .setMethod("POST")
//                        .setAutoDeleteFilesAfterSuccessfulUpload(false)
//                        .setUsesFixedLengthStreamingMode(true)
//                        .setMaxRetries(3);
//                req.setUtf8Charset();
//                String uploadID = req.setDelegate(this).startUpload();
//                Log.e("uploadIDVideo-----", uploadID);
//                DataBaseHelper dataBaseHelper = new DataBaseHelper(HomeActivity.this);
//                String profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
//                if (TextUtils.isEmpty(profileImageUrl)) {
//                    if (FileUtils.isFileExistsSDCard(AppConstant.stillFile)) {
//                        ProfilePicUploadRequest profilePicUploadRequest = new ProfilePicUploadRequest(candidateId);
//                        MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, profilePicUploadRequest.getUrl())
//                                .addFileToUpload("/storage/emulated/0/Movies/valYou/profile.png", "profilePic")
//                                .setNotificationConfig(getNotificationConfig(getFilename("/storage/emulated/0/Movies/valYou/profile.png")))
//                                .setCustomUserAgent(USER_AGENT)
//                                .setMethod("POST")
//                                .setAutoDeleteFilesAfterSuccessfulUpload(false)
//                                .setUsesFixedLengthStreamingMode(true)
//                                .setMaxRetries(3);
//
//                        request.setUtf8Charset();
//                        String uploadIDs = request.setDelegate(this).startUpload();
//                        Log.e("uploadIDsAudio-----", uploadIDs);
//                    }
//                }
//
//
//            } catch (FileNotFoundException exc) {
//
//            } catch (IllegalArgumentException exc) {
//
//            } catch (MalformedURLException exc) {
//
//            }
//        }
//    }


    public void uploadVideoResponse() {
        Toast.makeText(HomeActivity.this, "Sit back and relax, we will upload your videos", Toast.LENGTH_LONG).show();

        new PrefManager(HomeActivity.this).saveRetryreponseString("notdone");
       /* if (listener != null) {
            listener.onReceived();
        }else {
            Log.e("refresh","error");
        }*/


        if(new PrefManager(HomeActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {
            mTabLayout.getTabAt(1).select();


        }else {
            mTabLayout.getTabAt(2).select();

        }
        List<String> videoQusts = dataBaseHelper.getVideoResponseQuestions();
        List<String> videoFiles = dataBaseHelper.getVideoResponseFiles();
        if (videoQusts != null && videoFiles != null && videoQusts.size() == videoFiles.size() && videoQusts.size() != 0) {
            try {
                Performance.inVisiblebars();
                mHandler.removeCallbacks(Performance.myTask);
            }catch (Exception e){
                e.printStackTrace();
            }
            if(connectionUtils.isConnectionAvailable()){
                new S3Example().execute();
            }else {
                createDialogue();
            }
        }

    }

    private void createDialogue() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeActivity.this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.internetcheck_uploadtime, null);
        dialogBuilder.setView(dialogView);

        TextView tv = (TextView) dialogView.findViewById(R.id.tv);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if(connectionUtils.isConnectionAvailable()){
                    new S3Example().execute();
                }else {
                    new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            createDialogue();
                        }
                    }.start();
                }
            }
        });
        /*AlertDialog.Builder builder1 = new AlertDialog.Builder(HomeActivity.this);
        builder1.setTitle("Error");
        builder1.setMessage("Unable to upload video response,Please check your Internet Connection...!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Retry",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if(connectionUtils.isConnectionAvailable()){
                            new S3Example().execute();
                        }else {
                            new CountDownTimer(1000, 1000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    createDialogue();
                                }
                            }.start();
                        }
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }


    public void postVideoResponse(String urll){




        Log.e("mytask","postvideostarted");

        List<String> videoQusts = dataBaseHelper.getVideoResponseQuestions();
        List<String> videoFiles = dataBaseHelper.getVideoResponseFiles();
        String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
        if (!TextUtils.isEmpty(candidateId)) {

            if(new PrefManager(HomeActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {
                String desc_video_contri=new PrefManager(HomeActivity.this).getDescriptionVideo();
                String params="U_Id="+candidateId+"&cont_video_link="+urll+"&video_desc="+desc_video_contri;
                VideoSend videoSend=new VideoSend();
                videoSend.execute(AppConstant.Ip_url + "UserVCData/update_vc",params);
                /*String desc_video_contri=new PrefManager(HomeActivity.this).getDescriptionVideo();
                //String uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);
                String url = AppConstant.Ip_url + "UserVCData/update_vc";
                Log.e("video Upload ", "url" + url);
                try {

                    MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, url)

                            .setNotificationConfig(getNotificationConfig("Video Response"))
                            .setCustomUserAgent(USER_AGENT)
                            .setMethod("POST")
                            .setAutoDeleteFilesAfterSuccessfulUpload(false)
                            .setUsesFixedLengthStreamingMode(true)
                            .setMaxRetries(3);
                    request.addParameter("cont_video_link",urll);
                    request.addParameter("video_desc",desc_video_contri);
                    request.addParameter("U_Id",candidateId);
                    request.addHeader("Authorization ","Bearer " + HomeActivity.token);
                    request.setUtf8Charset();
                    String uploadIDs = request.setDelegate(this).startUpload();
                    dataBaseHelper.updateUploadVideoId(uploadIDs, uploadFile);
                    dataBaseHelper.putUploadedVideoId(uploadFile);

                    if (listener != null) {

                        listener.onReceived();
   *//* ConnectionUtils connectionUtils=new ConnectionUtils(HomeActivity.this);
    if(connectionUtils.isConnectionAvailable()){
        listener.loadlist();
    }*//*
                    }else {
                        Log.e("refresh","error");
                    }
                    Log.e("uploadIDsAudio-----", uploadIDs);
                } catch (Exception exc) {
                    showToast(exc.getMessage());
                }

*/

            }else {
                String uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);
                String url = AppConstant.BASE_URL + "candidateAppUpoadAnswersnew/" + uploadFile;
                Log.e("video Upload ", "url" + url);
                try {

                    MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, url)
                            //.setNotificationConfig(getNotificationConfig("Video Response"))
                            .setCustomUserAgent(USER_AGENT)
                            .setMethod("PUT")
                            .setAutoDeleteFilesAfterSuccessfulUpload(false)
                            .setUsesFixedLengthStreamingMode(true)
                            .setMaxRetries(3);
                    for (int i = 0; i < videoFiles.size(); i++) {
                        Log.e("Video path=", "" + videoFiles.get(i));
                        File file = new File(newPath);
                        String le=null;
                        long length = file.length()/1024;
                        if(length>=1024) {
                            le = length / 1024 +"."+length%1024 +"MB";
                        }else{
                            le=length+"KB";
                        }
                        Log.e("LENGTH",le);
                        Log.e("Camera",getBackCameraResolutionInMp()+"MP");

                        //request.addFileToUpload(newPath, videoQusts.get(i));
                        request.addParameter("vr_url",urll);
                        request.addParameter("camera_resolution",getBackCameraResolutionInMp()+"MP");
                        request.addParameter("video_size_app",le);
                    }
                    request.setUtf8Charset();
                    String uploadIDs = request.setDelegate(this).startUpload();
                    dataBaseHelper.updateUploadVideoId(uploadIDs, uploadFile);
                    dataBaseHelper.putUploadedVideoId(uploadFile);

                    if (listener != null) {

                        listener.onReceived();
   /* ConnectionUtils connectionUtils=new ConnectionUtils(HomeActivity.this);
    if(connectionUtils.isConnectionAvailable()){
        listener.loadlist();
    }*/
                    }else {
                        Log.e("refresh","error");
                    }
                    Log.e("uploadIDsAudio-----", uploadIDs);
                } catch (Exception exc) {
                    showToast(exc.getMessage());
                }


            }








        }

        // AppConstant.videoResponseObjectId = null;
        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
        if (id > 0) {
            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, "");
        }






    }


    public class VideoSend extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";

            try {
                url = new URL(params[0]);
                ref_id =params[1];
                Log.v("URL", "URL: " + url);
                Log.v("postParameters",ref_id);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Authorization", "Bearer " + HomeActivity.token);

                /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();
     /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/


                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.v("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            Log.e("mytask",s);
            pr=10;
            percentage=10;
            if(s.contains("succesfully")){

                new CountDownTimer(3000, 300) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        Log.e("mytask",String.valueOf(percentage));
                        build.setProgress(100,percentage, false);
                        mNotifyManager.notify(id, build.build());
                        percentage=percentage+10;
                    }
                    @Override
                    public void onFinish() {
                        mNotifyManager.cancelAll();
                        new PrefManager(HomeActivity.this).saveRetryreponseString("done");
                    }
                }.start();
                if(s.length()>3){


                }


            }    else {
                Toast.makeText(HomeActivity.this,"Error while uploading video !",Toast.LENGTH_SHORT).show();
            }


        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }


    //    private void uploadVideoResponse() {
//
//        List<String> videoQusts = dataBaseHelper.getVideoResponseQuestions();
//        List<String> videoFiles = dataBaseHelper.getVideoResponseFiles();
//        if (videoQusts != null && videoFiles != null && videoQusts.size() == videoFiles.size() && videoQusts.size() != 0) {
//
//            String newPath = AppConstant.mediaStorageDir.getPath() + File.separator + "new0.mp4";
//           // new VideoCompressor(videoFiles.get(0), HomeActivity.this, newPath, 0, true).execute();
////            for(int i=0;i<videoFiles.size();i++){
////                new S3Example(i).execute();
////            }
//
////            String status = dataBaseHelper.getVideoFromTableWithId(DataBaseHelper.TABLE_UPLOAD_VIDEO_RESPONSE, DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_STATUS, 0+"");
////            if(!status.equalsIgnoreCase("uploading")){
////
////            }
//
//           // new S3Example().execute();
//
////fuck();
//        }
//
//    }
private boolean parsingSuccessful = true;

    private class S3Example extends AsyncTask<Void,Void,Void> {
//            int iok=0;
//        S3Example(int url){
//            this.iok=url;
//        }
        @Override
        protected Void doInBackground(Void... params) {
            Log.e("mytask",new PrefManager(HomeActivity.this).getVideoResponseDataPath());
            Log.e("mytask","s3taskstrted");
            mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            build = new Builder(HomeActivity.this);
            build.setContentTitle("Uploading Video...")
                    .setContentText("upload in progress")
                    .setSmallIcon(R.drawable.ic_upload);


            pr=0;
            build.setProgress(100,0, false);
            mNotifyManager.notify(id, build.build());


            if(new PrefManager(HomeActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {


                String ACCESS_KEY ="AKIAJQTIZUYTX2OYTBJQ";
                String SECRET_KEY = "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";
                uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);

                File photo = new File(new PrefManager(HomeActivity.this).getVideoResponseDataPath());
                if(photo.exists()){
                    Random r = new Random();
                    int i1 = r.nextInt(10005 - 30) + 28;
                    parsingSuccessful=true;
                    AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY,SECRET_KEY));
                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/contribution/videos/"+candid+"-"+uploadFile+i1+"-0k.mp4",photo);
                    PutObjectResult putResponse = s3Client.putObject(pp);
                    String prurl= s3Client.getResourceUrl("valyouinputbucket","valyou/contribution/videos/"+candid+"-"+uploadFile+i1+"-0k.mp4");
                    ppp= s3Client.getUrl("valyouinputbucket","valyou/contribution/videos/"+candid+"-"+uploadFile+i1+"-0k.mp4");
                    Log.e("AMAZON RESPOMSE",ppp.toString());
                    dataBaseHelper.updateUploadVideoId(""+0, uploadFile+i1);
                    return  null;
                }else{
                    parsingSuccessful=false;

                    return  null;
                }



            }else {


                String ACCESS_KEY ="AKIAJQTIZUYTX2OYTBJQ";
                String SECRET_KEY = "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";
                uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);
                if (listener != null) {

                    listener.onReceived();
   /* ConnectionUtils connectionUtils=new ConnectionUtils(HomeActivity.this);
    if(connectionUtils.isConnectionAvailable()){
        listener.loadlist();
    }*/
                }else {
                    Log.e("refresh","error");
                }

                File photo = new File(new PrefManager(HomeActivity.this).getVideoResponseDataPath());
                if(photo.exists()){
                    parsingSuccessful=true;
                    AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY,SECRET_KEY));
                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "videos/"+candid+"-"+uploadFile+"-0k.mp4",photo);
                    PutObjectResult putResponse = s3Client.putObject(pp);
                    String prurl= s3Client.getResourceUrl("valyouinputbucket","videos/"+candid+"-"+uploadFile+"-0k.mp4");
                    ppp= s3Client.getUrl("valyouinputbucket","videos/"+candid+"-"+uploadFile+"-0k.mp4");
                    Log.e("AMAZON RESPOMSE",ppp.toString());
                    dataBaseHelper.updateUploadVideoId(""+0, uploadFile);
                    return  null;
                }else{
                    parsingSuccessful=false;

                    return  null;
                }



            }





        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(parsingSuccessful){
                Log.e("AMAZON RESPOMSE",ppp.toString());
//            long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
//            if (id > 0) {
//                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, "");
//            }
                // new SExample(ppp.toString(),uploadFile).execute();

                pr=0;
                build.setProgress(100,0, false);
                if(new PrefManager(HomeActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {

                }else {
                    if (listener != null) {

                        listener.onReceived();
   /* ConnectionUtils connectionUtils=new ConnectionUtils(HomeActivity.this);
    if(connectionUtils.isConnectionAvailable()){
        listener.loadlist();
    }*/
                    }else {
                        Log.e("refresh","error");
                    }
                }

//                mNotifyManager.cancelAll();

                Log.e("mytask",ppp.toString());
                Log.e("mytask","s3taskended");
                postVideoResponse(ppp.toString());
            }else{


               /* build.setContentTitle("Uploaded")
                        .setContentText("upload done")
                        .setSmallIcon(R.drawable.donentf2);*/

               uploadVideoResponse();
            }

        }
    }
//
//    private class SExample extends AsyncTask<Void,Void,Void> {
//        String cid,url;
//
//
//        SExample(String url,String cid){
//            this.url=url;
//            this.cid=cid;
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            OkHttpClient client = new OkHttpClient();
//
//            MediaType mediaType = MediaType.parse("application/json");
//            RequestBody body = RequestBody.create(mediaType, "{\"objectid\":\""+cid+"\",\"candidateId\": \""+candid+"\",\"recruiterId\": \""+pf.getPicRID()+"\",\"answers\": [{\"question\": \""+pf.getUUID()+"\",\"videoUrl\": {\"s3valyouinputbucket\": \""+url+"\",\"s3valyououtputbucket\": [\""+url+"\",\""+url+"\"],\"cloudFront\": [\""+url+"\",\""+url+"\"]}}]}");
//            Request request = new Request.Builder()
//                    .url("http://admin.getvalyou.com/api/candidateAppUpoadAnswers/"+cid)
//                    .put(body)
//                    .addHeader("content-type", "application/json")
//                    .addHeader("cache-control", "no-cache")
//                    .build();
//
//            try {
//                Response response = client.newCall(request).execute();
//                Log.e("AMAZON RESPOMSE",url.toString());
//                if(response.isSuccessful()){
//                    String jj =response.body().toString();
//                    try {
//                        JSONObject jdsl = new JSONObject(jj);
//                        Log.e("lsbs","fick");
//                    }catch (JSONException e){
//
//                    }
//                }
//                else
//                    Log.e("lsbs",response.body().toString());
//
//            }catch (IOException e){
//
//            }
//            return  null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//
//
//        }
//    }
//
//    public void fuck(){
//        String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
//        if (!TextUtils.isEmpty(candidateId)) {
//
//            List<String> videoQusts = dataBaseHelper.getVideoResponseQuestions();
//            List<String> videoFiles = dataBaseHelper.getVideoResponseFiles();
//
//            String uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);
//            String url = AppConstant.BASE_URL + "candidateAppUpoadAnswers/" + uploadFile;
//            Log.e("video Upload ", "url" + url);
//            try {
//                MultipartUploadRequest request = new MultipartUploadRequest(HomeActivity.this, url)
//
//                        .setNotificationConfig(getNotificationConfig("Video Response"))
//                        .setCustomUserAgent(USER_AGENT)
//                        .setMethod("PUT")
//                        .setAutoDeleteFilesAfterSuccessfulUpload(false)
//                        .setUsesFixedLengthStreamingMode(false)
//                        .setMaxRetries(10);
//                for (int i = 0; i < videoFiles.size(); i++) {
//                    Log.e("Video path=", "" + videoFiles.get(i));
//                    request.addFileToUpload(AppConstant.mediaStorageDir.getPath() + File.separator+0+".mp4", videoQusts.get(i));
//                }
//                request.setUtf8Charset();
//                String uploadIDs = request.setDelegate(this).startUpload();
//                dataBaseHelper.updateUploadVideoId(uploadIDs, uploadFile);
//                Log.e("uploadIDsAudio-----", uploadIDs);
//            } catch (FileNotFoundException exc) {
//
//            } catch (IllegalArgumentException exc) {
//
//            } catch (MalformedURLException exc) {
//
//            }
//
//
//        }
//
//        // AppConstant.videoResponseObjectId = null;
//        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
//        if (id > 0) {
//            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, "");
//        }
//    }


    void uploadFilesToServer() {
        String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
        if (!TextUtils.isEmpty(candidateId)) {

            if (FileUtils.isFileExistsSDCard(AppConstant.tempFile)) {
//TODO show loading

                PreferenceUtils.setProgress(true, HomeActivity.this);

                new VideoCompressor(AppConstant.tempFile, HomeActivity.this, AppConstant.outputFile, 0, false).execute();


            }


        }


    }


    private void showToast(String message) {
        Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
    }

    private UploadNotificationConfig getNotificationConfig(String filename) {


        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_upload)
                .setCompletedIcon(R.drawable.ic_upload_success)
                .setErrorIcon(R.drawable.ic_upload_error)
                .setTitle(filename)
                .setInProgressMessage("uploading...")
                .setCompletedMessage("success")
                .setErrorMessage("error")
                .setAutoClearOnSuccess(false)
                .setClearOnAction(true)
                .setRingToneEnabled(true);
    }


    private String getFilename(String filepath) {
        if (filepath == null)
            return null;

        final String[] filepathParts = filepath.split("/");

        return filepathParts[filepathParts.length - 1];
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppConstant.ishome_active=false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppConstant.ishome_active=false;
    }

    // Resume Unity
    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume", "----");
        AppConstant.isalive=false;
        AppConstant.ishome_active=true;
        try {
            Log.e("onResume", getIntent().getStringExtra("from"));
        }catch (Exception e){
            Log.e("onResume", "exc");

        }



      /*  Performance performance=new Performance();
       performance.getScore();*/
      //  new PrefManager(HomeActivity.this).setIsCapturing("no");

        // shareData();

        //int count = PreferenceUtils.getNotificationCount(this);
//        if (count > 0) {
//            notificationCount.setVisibility(View.VISIBLE);
//            notificationCount.setText(String.valueOf(count));
//        } else
//            notificationCount.setVisibility(View.GONE);

        /*if(new PrefManager(HomeActivity.this).getResultscreenpause().equalsIgnoreCase("yes")){
            new PrefManager(HomeActivity.this).isResultscreenpause("no");


        }else {*/
         //   new PrefManager(HomeActivity.this).isResultscreenpause("no");

        if(new PrefManager(HomeActivity.this).getResultscreenpause().equalsIgnoreCase("yes")){
            new PrefManager(HomeActivity.this).isResultscreenpause("no");
            Intent it = new Intent(HomeActivity.this,HomeActivity.class);
             it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra("tabs","normal");
            it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
            startActivity(it);
           // finish();

        }else {
            new PrefManager(HomeActivity.this).isResultscreenpause("no");

            String candidateId = PreferenceUtils.getCandidateId(HomeActivity.this);
            Log.e("onResume", "candidateId" + candidateId);
            if (!TextUtils.isEmpty(candidateId)) {

                if (!PreferenceUtils.isVerified(HomeActivity.this)) {
                    AppConstant.OTP_FROM_SIGNUP = true;
                    Intent intent = new Intent(HomeActivity.this, EnterOtpActivity.class);
                    //DataBaseHelper dataBaseHelper = new DataBaseHelper(HomeActivity.this);
                    String phone = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE);
                    if (!TextUtils.isEmpty(phone))
                        intent.putExtra("phoneNumber", phone);
                    intent.putExtra("_id", PreferenceUtils.getCandidateId(HomeActivity.this));
                    startActivity(intent);
                    finish();
                } else {

                    String uploadFile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_UPLOAD_VIDEO_PROFILE);
                    Log.e("--Upload file", "" + uploadFile);
                    if (!TextUtils.isEmpty(uploadFile)) {
                        uploadFilesToServer();

                        if (updateProfileVideo != null)
                            updateProfileVideo.onUpdateProfileData();
                    }

                    String videoResponseObjectId = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID);

                    if (!TextUtils.isEmpty(videoResponseObjectId)) {

                        List<String> videoQusts = dataBaseHelper.getVideoResponseQuestions();
                        List<String> videoFiles = dataBaseHelper.getVideoResponseFiles();
                        Log.e("onResume", "AppConstant.videoResponseObjectId" + videoResponseObjectId + "   " + videoFiles.size());
                        if (videoQusts != null && videoFiles != null && videoQusts.size() == videoFiles.size() && videoQusts.size() != 0) {
                            if ((new PrefManager(HomeActivity.this).getRetryresponseString().equalsIgnoreCase("trying"))||(new PrefManager(HomeActivity.this).getRetryresponseString().equalsIgnoreCase("notdone"))) {
                                uploadVideoResponse();
                            }
                        }

                    }


                    String candId = PreferenceUtils.getCandidateId(HomeActivity.this);
                    if (!TextUtils.isEmpty(candId)) {
                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(HomeActivity.this, HomeActivity.this);
                        processor.getCount(candId);

                    }

                }
            } else {
                //do changes
                Intent intent = new Intent(HomeActivity.this, Welcome.class);
                intent.putExtra("gotologinpage","loginpage");

                startActivity(intent);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }

        }

       /* try {
            String uploadFiles = getIntent().getExtras().getString("uploadFiles");
            if (!TextUtils.isEmpty(uploadFiles) && uploadFiles.equalsIgnoreCase("uploadFiles")) {

            }
        } catch (Exception e) {

        }*/
    }


    private class GetEywa extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                httpClient = new DefaultHttpClient();
                String url = "http://admin.getvalyou.com/api/getCandidateDetails/" + PreferenceUtils.getCandidateId(HomeActivity.this);
                // String url = "http://admin.getvalyou.com/api/getCandidateDetails/59de53e90f43165f3afdded0";
//navya
                httpGet = new HttpGet(url);


                httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
                httpGet.setEntity(new UrlEncodedFormEntity(params));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                jsoneya = sb.toString();


                Log.e("JSONNOYU", jsoneya);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            try {
                jObjeya = new JSONObject(jsoneya);






//                dn= jObjeya.getString("displayName");
//                emaileya=jObjeya.getString("email");

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            if(jObjeya.has("profileImageURL")){

                try{
                    Log.e("PROFILE IMAE",jObjeya.getString("profileImageURL"));
                    long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, jObjeya.getString("profileImageURL"));
                }catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }


            }else{
                Intent it = new Intent(HomeActivity.this,ProPic.class);
                it.putExtra("fromwhere","home");
                startActivity(it);
                finish();
            }
        }

    }

    @Override
    public void onBackPressed() {
       /* try {
            Log.e("count", String.valueOf(getFragmentManager().getBackStackEntryCount()));
            getSupportFragmentManager().popBackStack();
        }catch (Exception e){
            e.printStackTrace();
        }
*/

       /* Intent intent = new Intent(HomeActivity.this, AssesmentSelectionActivity.class);
     //   intent.putExtra("tabs", "normal");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/

       if(HomeActivity.apiloading){

       }else {
           if (new PrefManager(HomeActivity.this).getNav().equalsIgnoreCase("learn")) {
               Intent intent = new Intent(HomeActivity.this, ModulesActivity.class);
               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               startActivity(intent);

               overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
               finish();
           } else {
               if(new PrefManager(HomeActivity.this).getgetback_status().equalsIgnoreCase("Assgroup")){
                   Intent intent = new Intent(HomeActivity.this, AssessmentGroupsActivity.class);
                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                   startActivity(intent);

                   overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                   finish();

               }else {
                   Intent intent = new Intent(HomeActivity.this, MainPlanetActivity.class);
                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                   startActivity(intent);

                   overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                   finish();

               }

           }
       }



    }



  /*  class UploadTask extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... strings) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    uploadVideoResponse();

                }
            });
            return null;
        }
    }*/

/*
  public static void updateBadge(){
      try{
          prevcount=new PrefManager(context).getntfcount();

      }catch (Exception e){
          prevcount=0;
      }
      int currentcount=prevcount+1;
      HomeActivity.badge.setText(currentcount+"");
      new PrefManager(context).saventfcount(currentcount);
  }
*/
/*
public void checkIfProfilePicAvailable(){
    try {
        profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
    }catch (Exception e){
        e.printStackTrace();
     //   profileImageUrl="";
    }
   try {
       if (prefManager.getPic() == " ") {

           if((profileImageUrl!="")||(profileImageUrl!=null)){


               new DownloadImage().execute(AppConstant.BASE_URL+"candidateMobileReadImage/"+profileImageUrl);
               //not present
               //showProfilePicDialogue();
           }else {
               //not present in shared data
              // showProfilePicDialogue();


           }
       } else {
//present
       }
   }catch (Exception e){
       e.printStackTrace();
   //    showProfilePicDialogue();
   }


}
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];


            try {
                // Download Image from URL
                InputStream input = new URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if(result!=null) {
                // Set the bitmap into ImageView
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                result.compress(Bitmap.CompressFormat.PNG, 0, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                Log.e("ENCID", encodedImage);
                prefManager.savePic(encodedImage);
                Log.e("EN", prefManager.getPic());
                byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                detectpic(decodedByte);
            }
            else{

                showProfilePicDialogue();

            }
            // dialog.cancel();
        }
    }
    private void detectpic(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            //    Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                            // Log.e("facedetection","No face detected");

                            //   sl.setVisibility(View.VISIBLE);
                        }else {
                            //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","face detected");


                            List<Face> faces;

                            assert result != null;
                            faces = Arrays.asList(result);
                            for (Face face: faces) {
                               UUID mFaceId1 = face.faceId;
                                String faceid=mFaceId1.toString();

                                prefManager.setprofilefaceid(faceid);
                                //txt.setText(faceid);
                                // Log.e("fid",mFaceId1+"  "+mface);
                            }

                        }



                    }
                };
        detectTask.execute(inputStream);
    }
public void showProfilePicDialogue(){
    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeActivity.this);
// ...Irrelevant code for customizing the buttons and title
    LayoutInflater inflater = this.getLayoutInflater();
    View dialogView = inflater.inflate(R.layout.compulsary_profile_pic_layout, null);
    dialogBuilder.setView(dialogView);
    Typeface tf1 = Typeface.createFromAsset(this.getAssets(), "Roboto-Regular.ttf");
    TextView tv = (TextView) dialogView.findViewById(R.id.text_profile_compulsary);
    ImageView im_pic=(ImageView)dialogView.findViewById(R.id.profile_pic_compulsary);
    tv.setTypeface(tf1,Typeface.BOLD);
    alertDialogis = dialogBuilder.create();
    alertDialogis.setCancelable(false);
    alertDialogis.show();
    im_pic.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            alertDialogis.dismiss();
            Intent it = new Intent(HomeActivity.this,ProPic.class);
            it.putExtra("fromwhere","home");
            startActivity(it);
        }
    });

}*/
}
