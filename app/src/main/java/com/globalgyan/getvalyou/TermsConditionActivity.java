package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.fragments.MoreFragment;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import de.morrox.fontinator.FontTextView;

public class TermsConditionActivity extends AppCompatActivity {
    ImageView backButton = null;
    ConnectionUtils connectionUtils;
    private Dialog dialog = null;
    private MyWebChromeClient mWebChromeClient = null;
    private View mCustomView;
    private RelativeLayout mContentView;
    private FrameLayout mCustomViewContainer;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    boolean flag_netcheck=false;
    private WebView myWebView;
    Dialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        myWebView = (WebView) findViewById(R.id.webView);

        backButton = (ImageView) findViewById(R.id.bckbtn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
      createProgress();
        FontTextView tt = (FontTextView) findViewById(R.id.title);
        tt.setText("TERMS AND CONDITIONS");
        connectionUtils = new ConnectionUtils(this);
        if (connectionUtils.isConnectionAvailable()) {

            progress.show();
            final CheckInternetTask t915=new CheckInternetTask();
            t915.execute();

            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    t915.cancel(true);

                    if (flag_netcheck) {
                        flag_netcheck = false;
                        mWebChromeClient = new MyWebChromeClient();
                        myWebView.setWebChromeClient(mWebChromeClient);
                        myWebView.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                return false;
                            }
                        });
                        WebSettings webSettings = myWebView.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        myWebView.getSettings().setAllowFileAccess(true);
                        myWebView.getSettings().setDomStorageEnabled(true);
                        myWebView.getSettings().setAppCachePath(String.valueOf(getApplicationContext()));
                        myWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
                        myWebView.getSettings().setAppCacheEnabled(true);
                        myWebView.getSettings().setAllowFileAccess(true);
                        myWebView.getSettings().setDatabaseEnabled(true);
                        myWebView.getSettings().setBuiltInZoomControls(true);
                        myWebView.getSettings().setLoadWithOverviewMode(true);
                        myWebView.getSettings().setUseWideViewPort(true);
                        myWebView.getSettings().setSupportZoom(true);
                        myWebView.loadUrl(AppConstant.BASE_URL + "candidateMobileAppStaticPage/TermsAndConditions");

                        progress.dismiss();
                    }else {
                        progress.dismiss();
                        flag_netcheck=false;
                        Toast.makeText(TermsConditionActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    }
                }
            }.start();


        } else {
            Log.e("Not Connected", "to internet");
            Toast.makeText(TermsConditionActivity.this, "There is no internet.Please connect internet.", Toast.LENGTH_SHORT).show();
        }

    }

    private void createProgress() {
        progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.setContentView(R.layout.planet_loader);
        Window window = progress.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progress.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progress.setCancelable(false);
        progress.show();

    }


    public class MyWebChromeClient extends WebChromeClient {

        FrameLayout.LayoutParams LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mContentView = (RelativeLayout) findViewById(R.id.main_rel);
            mContentView.setVisibility(View.GONE);
            mCustomViewContainer = new FrameLayout(TermsConditionActivity.this);
            mCustomViewContainer.setLayoutParams(LayoutParameters);
            mCustomViewContainer.setBackgroundResource(android.R.color.black);
            view.setLayoutParams(LayoutParameters);
            mCustomViewContainer.addView(view);
            mCustomView = view;
            mCustomViewCallback = callback;
            mCustomViewContainer.setVisibility(View.VISIBLE);
            setContentView(mCustomViewContainer);
        }

        @Override
        public void onHideCustomView() {
            if (mCustomView == null) {
                return;
            } else {
                // Hide the custom view.
                mCustomView.setVisibility(View.GONE);
                // Remove the custom view from its container.
                mCustomViewContainer.removeView(mCustomView);
                mCustomView = null;
                mCustomViewContainer.setVisibility(View.GONE);
                mCustomViewCallback.onCustomViewHidden();
                // Show the content view.
                mContentView.setVisibility(View.VISIBLE);
                setContentView(mContentView);
            }
        }
    }

    @Override
    public void onBackPressed() {
        /*if (mCustomViewContainer != null)
            mWebChromeClient.onHideCustomView();
        else */
        if (myWebView.canGoBack()) {
            myWebView.goBack();
        } else {
            finish();
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
        }
    }
    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)TermsConditionActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

}