package com.globalgyan.getvalyou;

/**
 * Created by Globalgyan on 14-12-2017.
 */

public abstract class SwipeControllerActions {
    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}
}
