package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.Topic_data_model;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import cz.msebera.android.httpclient.client.ClientProtocolException;

public class ModulesActivity extends AppCompatActivity {

    public static ModulesAdapter modulesAdapter;
    public static ArrayList<ModulesModel> module_names=new ArrayList<>();
    public static ArrayList<TopicModule> topic_names=new ArrayList<>();
    public static RecyclerView module_name_list;
    public static String resp_array,childarray,flag_resp_array="parent";
    ImageView lock, modulebg;
    public static String c_name;
    public static TopicAdapter topicAdapter;
    public static FrameLayout frame_main_banner;
    public static Animation animation_fade;
    public static Context mcont;
    public static ArrayList<String> open_topic_status=new ArrayList<>();
    public static ArrayList<String> open_desc_status=new ArrayList<>();
    public static MaterialRefreshLayout swipemodule;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;
    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;
    public static Context context;
    public static String  tokenis="";
    public static HttpURLConnection urlConnection2;
    public static int code2;
    static InputStream is = null;
    public static String json=" ";
    public static TextView no_module_text;
    public static String sub="",lmstype="";
    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    int batLevel;
    TextView certification_name;
    AppConstant appConstant;
    public static FrameLayout load_frame;
    public static ImageView load_img;
    public static FrameLayout nocourseavailableframe;
    public static TextView noavailabletext;
    public static ImageView okbtn;
    public static boolean flag_loader=false;
    public static ObjectAnimator imageViewObjectAnimator1;
    LinearLayout certificatename_linear;
    boolean isPowerSaveMode;
    public static ImageView backbtnis_lessonis;
    public static TextView backbtnis_lessonis_text;
    PowerManager pm;
    TextView textlessonsheading;
    public static FrameLayout banneris;
    static long mLastClickTime = 0;
    static ArrayList<Topic_data_model> topic_ids = new ArrayList<>();
   static Topic_data_model topic_data_model ;
    static Topic_data_model topic_data_model1 ;
  static boolean flag_update_lesson = false;



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modules);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        mcont=ModulesActivity.this;
        appConstant = new AppConstant(this);
        backbtnis_lessonis=(ImageView)findViewById(R.id.backbtnis_lessonis);
        backbtnis_lessonis_text=(TextView)findViewById(R.id.textlessons);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        textlessonsheading=(TextView)findViewById(R.id.textlessonsheading);
        certificatename_linear=(LinearLayout)findViewById(R.id.certificatename_linear);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        certification_name=(TextView)findViewById(R.id.certification_name);
        frame_main_banner = (FrameLayout) findViewById(R.id.frame_main_banner);
        no_module_text = (TextView) findViewById(R.id.no_module_text);
        module_name_list = (RecyclerView) findViewById(R.id.modules);
        module_name_list.setHasFixedSize(true);
        topicAdapter=new TopicAdapter(this,topic_names);
        banneris=(FrameLayout)findViewById(R.id.banneris);
        context = ModulesActivity.this;
        load_frame=(FrameLayout)findViewById(R.id.load_frame_module);
        load_img=(ImageView)findViewById(R.id.load_icmodule);
        red_planet = (ImageView) findViewById(R.id.red_planet);
        c_name=new PrefManager(ModulesActivity.this).getcertiname();

        yellow_planet = (ImageView) findViewById(R.id.yellow_planet);
        purple_planet = (ImageView) findViewById(R.id.purple_planet);
        nocourseavailableframe=(FrameLayout)findViewById(R.id.nocourseavailableframem);
        noavailabletext=(TextView)findViewById(R.id.noavailabletextm);
        okbtn=(ImageView)findViewById(R.id.okbtnm);

        earth = (ImageView) findViewById(R.id.earth);
        orange_planet = (ImageView) findViewById(R.id.orange_planet);
        swipemodule = (MaterialRefreshLayout) findViewById(R.id.swipemodule);

        topic_data_model = new Topic_data_model();
        topic_data_model1 = new Topic_data_model();
        tokenis = new PrefManager(ModulesActivity.this).getToken();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        module_name_list.setLayoutManager(linearLayoutManager);
        animation_fade =
                AnimationUtils.loadAnimation(ModulesActivity.this,
                        R.anim.fade_in);

        //   resp_array=getIntent().getStringExtra("content");
        //Log.e("Moduleresp",resp_array);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        certification_name.setTypeface(tf2,Typeface.BOLD);
        textlessonsheading.setTypeface(tf2);
        backbtnis_lessonis_text.setTypeface(tf2);

        noavailabletext.setTypeface(tf2,Typeface.BOLD);
        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nocourseavailableframe.setVisibility(View.GONE);
            }
        });

        nocourseavailableframe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nocourseavailableframe.setVisibility(View.GONE);
            }
        });


       /* if(new PrefManager(ModulesActivity.this).getcertiname().equalsIgnoreCase("")){
            certificatename_linear.setVisibility(View.GONE);
        }else {
            certificatename_linear.setVisibility(View.VISIBLE);
            certification_name.setText(c_name);

        }*/
        modulesAdapter = new ModulesAdapter(this, module_names);

        module_name_list.setAdapter(modulesAdapter);


        Animation animation_fade1 =
                AnimationUtils.loadAnimation(ModulesActivity.this,
                        R.anim.fadeing_out_animation_planet);
        // frame_main_banner.startAnimation(animation_fade1);

        imageViewObjectAnimator1 = ObjectAnimator.ofFloat(load_img,
                "rotation", 0f, 360f);
        imageViewObjectAnimator1.setDuration(500); // miliseconds
        imageViewObjectAnimator1.setStartDelay(100);
        imageViewObjectAnimator1.setRepeatCount(600);


        new CountDownTimer(100, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
                frame_main_banner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                swipemodule.autoRefresh();
            }
        }.start();
        // addDatatoModule();


      //  module_name_list.setLayoutFrozen(true);

        module_name_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });




        swipemodule.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {


                ConnectionUtils connectionUtils = new ConnectionUtils(ModulesActivity.this);
                if (connectionUtils.isConnectionAvailable()) {
                    String retry_str = " ";
                    try{
                        retry_str = new PrefManager(ModulesActivity.this).getRetryresponseString();
                    }catch (Exception e){
                        retry_str = " ";
                        e.printStackTrace();
                    }
                    if (retry_str.equalsIgnoreCase("notdone")) {
                        Toast.makeText(ModulesActivity.this, "Video uploading is in progrss,Can't access latest notifications now", Toast.LENGTH_LONG).show();
                        swipemodule.finishRefresh();
                    } else {


                        new CountDownTimer(100, 100) {
                            @Override
                            public void onTick(long millisUntilFinished)
                            {

                            }

                            @Override
                            public void onFinish()
                            {
                                module_names.clear();
                                topic_names.clear();
                                open_desc_status.clear();
                                topic_ids.clear();
                                flag_update_lesson = true;
                                modulesAdapter.notifyDataSetChanged();
                                new GetModules().execute();

                                //  swipemodule.finishRefresh();
                            }
                        }.start();



                       /* int positionView = ((LinearLayoutManager)recyclerGen.getLayoutManager()).findFirstVisibleItemPosition();
                        Log.e("pos",String.valueOf(positionView));
                        if(positionView==0) {
                            swipeRefreshLayout.setEnabled(true);


                        }else {

                        }
*/
                    }
                } else {
                    swipemodule.finishRefresh();
                    Toast.makeText(ModulesActivity.this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
     /*   module_names.add("01. Financial Statements Accounting Principles");
        module_names.add("02. Financial Analysis");
        module_names.add("03. Value of Money");
        module_names.add("04. Decision Making");
        module_names.add("05. Marketing Skills");
        module_names.add("03. Value of Money");
        module_names.add("04. Decision Making");
        module_names.add("05. Marketing Skills");
        module_names.add("03. Value of Money");
        module_names.add("04. Decision Making");
        module_names.add("05. Marketing Skills");
*/


        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            //  red planet animation
            red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f, 700f);
            red_planet_anim_1.setDuration(130000);
            red_planet_anim_1.setInterpolator(new LinearInterpolator());
            red_planet_anim_1.setStartDelay(0);
            red_planet_anim_1.start();

            red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700, 0f);
            red_planet_anim_2.setDuration(130000);
            red_planet_anim_2.setInterpolator(new LinearInterpolator());
            red_planet_anim_2.setStartDelay(0);

            red_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            red_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  yellow planet animation
            yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f, 1100f);
            yellow_planet_anim_1.setDuration(220000);
            yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_1.setStartDelay(0);
            yellow_planet_anim_1.start();

            yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f, 0f);
            yellow_planet_anim_2.setDuration(22000);
            yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_2.setStartDelay(0);

            yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  purple planet animation
            purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f, 100f);
            purple_planet_anim_1.setDuration(9500);
            purple_planet_anim_1.setInterpolator(new LinearInterpolator());
            purple_planet_anim_1.setStartDelay(0);
            purple_planet_anim_1.start();

            purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f, 0f);
            purple_planet_anim_2.setDuration(100000);
            purple_planet_anim_2.setInterpolator(new LinearInterpolator());
            purple_planet_anim_2.setStartDelay(0);


            purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            //  earth animation
            earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f, 1150f);
            earth_anim_1.setDuration(90000);
            earth_anim_1.setInterpolator(new LinearInterpolator());
            earth_anim_1.setStartDelay(0);
            earth_anim_1.start();

            earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f, 0f);
            earth_anim_2.setDuration(55000);
            earth_anim_2.setInterpolator(new LinearInterpolator());
            earth_anim_2.setStartDelay(0);

            earth_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            earth_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  orange planet animation
            orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f, 300f);
            orange_planet_anim_1.setDuration(56000);
            orange_planet_anim_1.setInterpolator(new LinearInterpolator());
            orange_planet_anim_1.setStartDelay(0);
            orange_planet_anim_1.start();

            orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f, 0f);
            orange_planet_anim_2.setDuration(75000);
            orange_planet_anim_2.setInterpolator(new LinearInterpolator());
            orange_planet_anim_2.setStartDelay(0);

            orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


        }



        backbtnis_lessonis_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime  = SystemClock.elapsedRealtime();
                Intent intent = new Intent(ModulesActivity.this, MainPlanetActivity.class);
                // intent.putExtra("tabs", "normal");
                try {
                    new PrefManager(ModulesActivity.this).saveNav("learn");
                }catch (Exception e){
                    e.printStackTrace();
                }
                startActivity(intent);
                overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);
                finish();
            }
        });


        backbtnis_lessonis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backModuleclickis();
            }
        });
    }




    @Override
    protected void onResume() {
        super.onResume();
       // flag_update_lesson = false;

        modulesAdapter.notifyDataSetChanged();
    }

    public static void addDatatoModule(String resp_array_is) {
        //banneris.setBackgroundResource(R.drawable.lessons_banner);
        module_names.clear();
        open_desc_status.clear();
        try {
            JSONArray jsonArray=new JSONArray(resp_array_is);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject=(JSONObject)jsonArray.get(i);
                ModulesModel modulesModel=new ModulesModel();
                modulesModel.setId(jsonObject.getInt("id"));
                modulesModel.setType(jsonObject.getString("type"));
                modulesModel.setObj_name(jsonObject.getString("object_name"));

                try{
                    modulesModel.setWarn_text(jsonObject.getString("lock_message"));
                }catch (Exception e){
                    modulesModel.setWarn_text("");
                }
                if(jsonObject.getString("type").equalsIgnoreCase("topic")){
                    modulesModel.setLms_url(jsonObject.getString("lms_url"));
                    modulesModel.setSm_id(jsonObject.getInt("sm_id"));

                    modulesModel.setOrderid(jsonObject.getInt("order_id"));
                    try{
                        modulesModel.setAuthor(jsonObject.getString("author_name"));

                    }catch (Exception e){
                        modulesModel.setAuthor("");

                    }
                }else {
                    if(jsonObject.getString("type").equalsIgnoreCase("assessment")){
                        modulesModel.setOrderid(jsonObject.getInt("order_id"));

                    }else {
                        modulesModel.setOrderid(0);

                    }
                    modulesModel.setAuthor("");
                    modulesModel.setLms_url("");
                    modulesModel.setSm_id(0);
                }

                if(jsonObject.getString("type").equalsIgnoreCase("lesson")){
                    modulesModel.setChildrens(String.valueOf(jsonObject.getJSONArray("lesson_content")));
                    modulesModel.setIs_locked(jsonObject.getString("is_locked"));
                    modulesModel.setDesc(jsonObject.getString("description"));
                    modulesModel.setPrice(jsonObject.getString("price"));
                    modulesModel.setRating(jsonObject.getString("avg_rating"));

                }else  if(jsonObject.getString("type").equalsIgnoreCase("topic")){
                    if(jsonObject.getString("lms_url").length()>5){
                        modulesModel.setChildrens("");
                    }else {
                        modulesModel.setChildrens(String.valueOf(jsonObject.getJSONArray("topic_content")));

                        JSONArray jsonObject1 = new JSONArray(modulesModel.getChildrens());

                        for (int in = 0; in < jsonObject1.length(); in++) {
                            JSONObject sim_model = jsonObject1.getJSONObject(in);

                            modulesModel.setSm_type(sim_model.getString("sm_type"));
                            modulesModel.setSm_articulate_url(sim_model.getString("sm_articulate_url"));
                        }
                    }
                    modulesModel.setIs_locked(jsonObject.getString("is_locked"));

                }else {
                    modulesModel.setIs_locked(jsonObject.getString("is_locked"));
                    modulesModel.setChildrens("");
                }

                module_names.add(modulesModel);
                Log.e("status","moduleparsed");

            }


            for(int i=0;i< module_names.size();i++){
                open_desc_status.add(i,"close");
            }
            modulesAdapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addDatatoModuleChild(String children, int position) {



      //  banneris.setBackgroundResource(R.drawable.banner);

        topic_names.clear();
        open_topic_status.clear();

        childarray=children;
        try {
            JSONArray jsonArray=new JSONArray(childarray);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject=(JSONObject)jsonArray.get(i);
                TopicModule modulesModel=new TopicModule();
                modulesModel.setId(jsonObject.getInt("id"));
                modulesModel.setType(jsonObject.getString("type"));
                modulesModel.setObj_name(jsonObject.getString("object_name"));
                try{
                    modulesModel.setWarn_text(jsonObject.getString("lock_message"));
                }catch (Exception e){
                    modulesModel.setWarn_text("");
                }
                if(jsonObject.getString("type").equalsIgnoreCase("topic")){
                    modulesModel.setLms_url(jsonObject.getString("lms_url"));
                    modulesModel.setSm_id(jsonObject.getInt("sm_id"));

                    modulesModel.setDesc(jsonObject.getString("description"));
                    modulesModel.setOrderid(jsonObject.getInt("order_id"));
                    try{
                        modulesModel.setAuthor(jsonObject.getString("author_name"));

                    }catch (Exception e){
                        modulesModel.setAuthor("");

                    }
                }else {
                    modulesModel.setLms_url("");
                    modulesModel.setSm_id(0);
                    if(jsonObject.getString("type").equalsIgnoreCase("assessment")){
                        modulesModel.setOrderid(jsonObject.getInt("order_id"));

                    }else {
                        modulesModel.setOrderid(0);

                    }
                    modulesModel.setAuthor("");
                }

                if(jsonObject.getString("type").equalsIgnoreCase("lesson")){
                    modulesModel.setChildrens(String.valueOf(jsonObject.getJSONArray("lesson_content")));
                    modulesModel.setIs_locked(jsonObject.getString("is_locked"));

                }else  if(jsonObject.getString("type").equalsIgnoreCase("topic")){
                    if(jsonObject.getString("lms_url").length()>5){
                        Log.e("typelms","lms");
                        modulesModel.setChildrens("");
                        modulesModel.setTypelms(jsonObject.getString("topic_type"));
                    }else {
                        Log.e("typelms","sm");
                        modulesModel.setTypelms(" ");
                        modulesModel.setChildrens(String.valueOf(jsonObject.getJSONArray("topic_content")));
                        JSONArray jsonObject1 = new JSONArray(modulesModel.getChildrens());

                        for (int in = 0; in < jsonObject1.length(); in++) {
                            JSONObject sim_model = jsonObject1.getJSONObject(in);

                            modulesModel.setSm_type(sim_model.getString("sm_type"));
                            modulesModel.setSm_articulate_url(sim_model.getString("sm_articulate_url"));
                        }

                    }
                    modulesModel.setIs_locked(jsonObject.getString("is_locked"));

                }else {
                    Log.e("typelms","ass");
                    modulesModel.setTypelms(" ");
                    modulesModel.setIs_locked(jsonObject.getString("is_locked"));
                    modulesModel.setChildrens("");
                }
                if(i==jsonArray.length()-1){
                    modulesModel.setIsfeedback("yes");
                }else {
                    modulesModel.setIsfeedback("no");
                }


                topic_names.add(modulesModel);
                Log.e("status","childmoduleparsed");
            }

            for(int i=0;i< topic_names.size();i++){
                open_topic_status.add(i,"close");
            }
            modulesAdapter.notifyDataSetChanged();
           // topicAdapter.notifyItemChanged(position);
           // topicAdapter.notifyDataSetChanged();

            Log.e("status","childmoduleparsed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime  = SystemClock.elapsedRealtime();
        new PrefManager(ModulesActivity.this).set_back_nav_from_course("modules");
        Intent intent = new Intent(ModulesActivity.this, CourseLIstActivity.class);
         intent.putExtra("tabs", "normal");
        try {
            new PrefManager(ModulesActivity.this).saveNav("learn");
        }catch (Exception e){
            e.printStackTrace();
        }
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);

    }

    public static void clickelement(String childrens, int position){
       addDatatoModuleChild(childrens,position);
    }

    public void backModuleclickis(){
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
      mLastClickTime  = SystemClock.elapsedRealtime();

        new PrefManager(ModulesActivity.this).set_back_nav_from_course("modules");
        Intent intent = new Intent(ModulesActivity.this, CourseLIstActivity.class);
        // intent.putExtra("tabs", "normal");
        try {
            new PrefManager(ModulesActivity.this).saveNav("learn");
        }catch (Exception e){
            e.printStackTrace();
        }
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);

    }
    public static void sendLessonsStartData(int id, int order, String type,String sub_type,String lmstypeis){

        sub=sub_type;
        lmstype=lmstypeis;
        if(type.equalsIgnoreCase("topic")){
            try {
                new SendLessonStartProgress().execute(Integer.valueOf(new PrefManager(context).getCourseid()), id, 0, order);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }else {
            try {
                new SendLessonStartProgress().execute(Integer.valueOf(new PrefManager(context).getCourseid()), 0, id, order);
                //  new SendLessonStartProgress().execute(1,2,0,1);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }
    private static class SendLessonStartProgress extends AsyncTask<Integer, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        ///Authorization
        @Override
        protected String doInBackground(Integer... urlkk) {
            String result1 = "" , cand_id = " ", lg_id = " ", cert_id = " ";
            try {

                try{
                    cand_id = PreferenceUtils.getCandidateId(context);
                    lg_id = new PrefManager(context).getlgid();
                    cert_id = new PrefManager(context).getcertiid();
                }
                catch (Exception e){
                    cand_id = " ";
                    lg_id = " ";
                    cert_id = " ";

                    e.printStackTrace();
                }
                String pr="";
                if(sub.equals("lms")){
                    if(lmstype.equalsIgnoreCase("Articulate Zip")){
                        pr="inprogress";
                    }else {
                        pr="completed";
                    }

                }else {
                    pr="inprogress";
                }
                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", cand_id)
                        .object("course_id", urlkk[0])
                        .object("lesson_id", urlkk[1])
                        .object("assessment_id", urlkk[2])
                        .object("order_id", urlkk[3])
                        .object("status", pr)
                        .object("lg_id",lg_id)
                        .object("certification_id",cert_id)


                        .build().toString();

                try {
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_update_user_progress");
                    urlConnection2 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection2.setDoOutput(true);
                    urlConnection2.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection2.setRequestProperty("Content-Type", "application/json");
                    urlConnection2.setRequestProperty("Authorization", "Bearer " + tokenis);

                    urlConnection2.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection2.getOutputStream());
                    wr.write(jn);
                    Log.e("resp_data",jn);
                    wr.flush();
                    is = urlConnection2.getInputStream();
                    code2 = urlConnection2.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                } catch (ClientProtocolException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;

                }
            }finally {
                if (urlConnection2 != null)
                    urlConnection2.disconnect();
            }

            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {
                Log.e("myvalue","postans 200");
            }
            else{
                Log.e("myvalue","postans not 200");

            }
        }
    }
    private static String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }


    private class GetModules extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            if(flag_loader) {
                load_frame.setVisibility(View.VISIBLE);
                imageViewObjectAnimator1.start();
            }else {
                load_frame.setVisibility(View.GONE);

            }
            //changed
            no_module_text.setText("No lessons available");
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "", cand_id = " ", lg_id = " ", course_id=" ", cert_id=" ";
            HttpURLConnection urlConnection1 = null;
            try {

                try {
                    lg_id = new PrefManager(ModulesActivity.this).getlgid();
                    course_id = new PrefManager(ModulesActivity.this).getCourseid();
                    cert_id = new PrefManager(ModulesActivity.this).getcertiid();
                    cand_id = PreferenceUtils.getCandidateId(ModulesActivity.this);
                }catch (Exception e){
                    lg_id = " ";
                    course_id = " ";
                    cert_id = " ";

                    cand_id = " ";
                    e.printStackTrace();
                }

                DataBaseHelper dataBaseHelper=new DataBaseHelper(ModulesActivity.this);
                String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);

                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("course_id",course_id)
                        .object("U_Id",cand_id )
                        .object("email", email)
                        .object("lg_id",lg_id)
                        .object("certification_id",cert_id)


                        .build().toString();

                try {


                    // httpClient = new DefaultHttpClient();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_courses_details");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + tokenis);
                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    int responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.e("Course", responseString);
                    result1 = responseString;



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipemodule.finishRefresh();
                                no_module_text.setVisibility(View.VISIBLE);

                            }catch (Exception e){

                            }
                            if(flag_loader) {
                                imageViewObjectAnimator1.cancel();
                                load_frame.setVisibility(View.GONE);
                            }

                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipemodule.finishRefresh();
                                no_module_text.setVisibility(View.VISIBLE);

                            }catch (Exception e){

                            }
                            if(flag_loader) {
                                imageViewObjectAnimator1.cancel();
                                load_frame.setVisibility(View.GONE);
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipemodule.finishRefresh();
                                no_module_text.setVisibility(View.VISIBLE);


                            }catch (Exception e){

                            }
                            if(flag_loader) {
                                imageViewObjectAnimator1.cancel();
                                load_frame.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try{
                swipemodule.finishRefresh();
            }catch (Exception e){

            }
            if(flag_loader) {
                imageViewObjectAnimator1.cancel();
                load_frame.setVisibility(View.GONE);
            }
            if(s.equalsIgnoreCase("")){
                //  rc_course.setVisibility(View.GONE);
                no_module_text.setVisibility(View.VISIBLE);
            }else {
                //  rc_course.setVisibility(View.VISIBLE);
                no_module_text.setVisibility(View.GONE);
            }

            String ss=" ";

            try {
                ss = new PrefManager(ModulesActivity.this).getAuthToken().toString();
            }
            catch (Exception e){
                ss=" ";
                e.printStackTrace();
            }
            if(s.equals("")){

            }else {

                try {
                  JSONObject jsonObject=new JSONObject(s);
                  JSONArray js=jsonObject.getJSONArray("course_details_list");

                    addDatatoModule(js.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("respmsg","noerror");

            }

            flag_loader=false;
        }
    }


    public static class GetLessons extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            if(flag_loader) {
                load_frame.setVisibility(View.VISIBLE);
                imageViewObjectAnimator1.start();
            }else {
                load_frame.setVisibility(View.GONE);

            }
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "",cand_id = " ", lg_id = " ", course_id=" ", cert_id=" ", module_id=" ";
            HttpURLConnection urlConnection1 = null;
            try {
                try{
                    try {
                        module_id = new PrefManager(mcont).getModuleid();
                        lg_id = new PrefManager(mcont).getlgid();
                        course_id = new PrefManager(mcont).getCourseid();
                        cert_id = new PrefManager(mcont).getcertiid();
                        cand_id = PreferenceUtils.getCandidateId(mcont);
                    }catch (Exception e){
                        lg_id = " ";
                        course_id = " ";
                        cert_id = " ";
                        module_id=" ";
                        cand_id = " ";
                        e.printStackTrace();
                    }
                }catch (Exception e){

                }

                DataBaseHelper dataBaseHelper=new DataBaseHelper(mcont);
                String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);

                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("course_id",course_id)

                        .object("module_id",module_id)

                        .object("U_Id", cand_id)
                        .object("email", email)
                        .object("lg_id",lg_id)
                        .object("certification_id",cert_id)

                        .build().toString();

                try {

                    // httpClient = new DefaultHttpClient();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_module_details");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + tokenis);
                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    int responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.e("Course", responseString);
                    result1 = responseString;



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();


                    if (mcont instanceof Activity) {
                        ((Activity) mcont).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipemodule.finishRefresh();
                                    no_module_text.setVisibility(View.VISIBLE);

                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                            }
                        });
                    }


                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    if (mcont instanceof Activity) {
                        ((Activity) mcont).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipemodule.finishRefresh();
                                    no_module_text.setVisibility(View.VISIBLE);

                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                            }
                        });
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    if (mcont instanceof Activity) {
                        ((Activity) mcont).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipemodule.finishRefresh();
                                    no_module_text.setVisibility(View.VISIBLE);

                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                            }
                        });
                    }

                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try{
                swipemodule.finishRefresh();
            }catch (Exception e){

            }
            if(flag_loader) {
                imageViewObjectAnimator1.cancel();
                load_frame.setVisibility(View.GONE);
            }
            if(s.equalsIgnoreCase("")){
                //  rc_course.setVisibility(View.GONE);
                no_module_text.setVisibility(View.VISIBLE);
            }else {
                //  rc_course.setVisibility(View.VISIBLE);
                no_module_text.setVisibility(View.GONE);
            }

            String ss=" ";
            try {
                ss=new PrefManager(mcont).getAuthToken().toString();

            }
            catch (Exception e){
                ss=" ";
                e.printStackTrace();
            }
            if(s.equals("")){

            }else {

                try {

               //     addDatatoModuleChild(s, position);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("respmsg","noerror");

            }
            flag_loader=false;
        }
    }


}
