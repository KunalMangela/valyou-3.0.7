package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.cms.response.GetDegreeResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Degree;

import java.util.ArrayList;
import java.util.List;

public class DegreeActivity extends AppCompatActivity implements GUICallback {
    private TextView txtNext = null;
    private DegreeAdapter degreeAdapter = null;
    private RecyclerView recyclerView = null;
    private EditText txtSearch = null;
    private String selected = null;
    private List<Degree> degreeList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_degree);
        txtNext = (TextView) findViewById(R.id.txtNext);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(DegreeActivity.this));
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        TextView txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
        txtPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMessage = new Intent();
                setResult(RESULT_CANCELED ,intentMessage);

                finish();
               // overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });

        try {
            selected = getIntent().getExtras().getString("selected");
        }catch (Exception ex){
            selected = null;
        }
        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(this,this);
        processor.getDegrees();

        degreeAdapter = new DegreeAdapter(degreeList,this,selected);
        recyclerView.setAdapter(degreeAdapter);
        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentMessage = new Intent();
                setResult(RESULT_OK, intentMessage);

                if (getParent() == null) {
                    setResult(RESULT_OK, intentMessage);
                } else {
                    getParent().setResult(RESULT_OK, intentMessage);
                }
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


            }
        });
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
                charSequence = charSequence.toString().trim().toLowerCase();

                final List<Degree> filteredList = new ArrayList<>();
                boolean itemFound = false;
                for (int i = 0; i < degreeList.size(); i++) {

                    final String text = degreeList.get(i).getName().toLowerCase();
                    if (text.contains(charSequence)) {
                        itemFound = true;
                        filteredList.add(degreeList.get(i));
                    }
                }
                if (!itemFound) {
                    Degree degree = new Degree();
                    degree.setName(charSequence.toString());
                    degree.set_id("new");
                    selected = charSequence.toString();
                    filteredList.add(degree);
                }
                degreeAdapter = new DegreeAdapter(filteredList,DegreeActivity.this,selected);
                recyclerView.setAdapter(degreeAdapter);
                degreeAdapter.notifyDataSetChanged();  // data set changed
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intentMessage = new Intent();

        if (getParent() == null) {
            setResult(RESULT_CANCELED, intentMessage);
        } else {
            getParent().setResult(RESULT_CANCELED, intentMessage);
        }
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {

                GetDegreeResponse response = (GetDegreeResponse) guiResponse;
                if (response != null && response.isStatus() && response.getDegreeList() != null) {
                    List<Degree> colleges = response.getDegreeList();
                    for (int i = 0; i < colleges.size(); i++) {
                        degreeList.add(colleges.get(i));
                    }
                    degreeAdapter = new DegreeAdapter(degreeList, DegreeActivity.this, selected);
                    recyclerView.setAdapter(degreeAdapter);
                    degreeAdapter.notifyDataSetChanged();  // data set changed
                } else {
                    Toast.makeText(DegreeActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(DegreeActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }
}
