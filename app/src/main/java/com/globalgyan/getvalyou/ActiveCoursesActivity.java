package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.HomeActivity.appConstant;
import static com.globalgyan.getvalyou.HomeActivity.batLevel;

public class ActiveCoursesActivity extends AppCompatActivity {


    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;


    public static Animation animation_fade;

    static FontTextView no_learning_groups;

    public static RecyclerView rc_course;
    public static int lg_id_is;
    public static MaterialRefreshLayout swipe;
    ArrayList<LearningGroupModel> arrayListlg=new ArrayList<>();
    public static ArrayList<LearningGroupCourseProgressModel> arrayListpr=new ArrayList<>();
    public static CourseProgressAdapter courseAdapter;
    public static LearningGroupAdapter learningGroupAdapter;
    public static TextView myprogress,learningbytes;
    KProgressHUD assess_progressis;
    static InputStream is = null;
    String result1 ="";
    int first=0;
    static JSONObject jObj = null,jObjeya=null;
    HttpURLConnection urlConnection;
    public static String token="";
    public static boolean flag_loader=false;
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="";
    int responseCode,responseCod,responseCo;
    public static Dialog alertDialog;
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    public static ImageView backbtnis_course,leftarrowis,rightarrowis;
    public static Context mcontext;
    public static boolean click=false;
    boolean isPowerSaveMode;
    public static SnapHelper mSnapHelper;
    public static FrameLayout load_frame;
    public static ImageView load_img;
    public static ObjectAnimator imageViewObjectAnimator1;
    PowerManager pm;
    public static TextView textprogress;
    static FrameLayout frame;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_courses);
        rc_course = (RecyclerView) findViewById(R.id.course_list);
        mcontext=ActiveCoursesActivity.this;
         mSnapHelper = new PagerSnapHelper();
        red_planet = (ImageView) findViewById(R.id.red_planet);
        yellow_planet = (ImageView) findViewById(R.id.yellow_planet);
        purple_planet = (ImageView) findViewById(R.id.purple_planet);
        earth = (ImageView) findViewById(R.id.earth);
        orange_planet = (ImageView) findViewById(R.id.orange_planet);
        swipe=(MaterialRefreshLayout)findViewById(R.id.swipe);
        textprogress=(TextView)findViewById(R.id.textprogress);
        load_frame=(FrameLayout)findViewById(R.id.load_frame);
        load_img=(ImageView)findViewById(R.id.load_ic);
        no_learning_groups=(FontTextView)findViewById(R.id.no_learnings_groups_text);
        frame = (FrameLayout)findViewById(R.id.frame);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        leftarrowis=(ImageView)findViewById(R.id.leftarrow);
        rightarrowis=(ImageView)findViewById(R.id.rightarrow);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        animation_fade =
                AnimationUtils.loadAnimation(ActiveCoursesActivity.this,
                        R.anim.fade_in);

      //  GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 3, LinearLayoutManager.HORIZONTAL, false); // MAX NUMBER OF SPACES

        rc_course.setLayoutManager(linearLayoutManager);
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        textprogress.setTypeface(typeface1);
        myprogress = (TextView) findViewById(R.id.myprogress);
        learningbytes = (TextView) findViewById(R.id.learnngbytes);
        myprogress.setTypeface(typeface1, Typeface.NORMAL);
        learningbytes.setTypeface(typeface1, Typeface.NORMAL);
       /* courseAdapter = new CourseProgressAdapter(this, arrayListpr);
        rc_course.setAdapter(courseAdapter);*/

       imageViewObjectAnimator1 = ObjectAnimator.ofFloat(load_img,
                "rotation", 0f, 360f);
        imageViewObjectAnimator1.setDuration(500); // miliseconds
        imageViewObjectAnimator1.setStartDelay(100);
        imageViewObjectAnimator1.setRepeatCount(600);


        learningGroupAdapter=new LearningGroupAdapter(this,arrayListlg);
        rc_course.setAdapter(learningGroupAdapter);
        rc_course.setVerticalScrollBarEnabled(true);
        assess_progressis = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setDimAmount(0.7f)
                .setCancellable(false);
        ImageView back = (ImageView) findViewById(R.id.backbtnis_course);
        leftarrowis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=first-3;
                if(val<0){

                }else {
                    rc_course.smoothScrollToPosition(val);
                }

            }
        });

        rightarrowis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val2=first+3;
                if(val2>arrayListpr.size()-1){

                }else {
                    rc_course.smoothScrollToPosition(val2);
                }
            }
        });

        rc_course.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                first = layoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        textprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(click){
                    click=false;
                    //
                    //  frame.setVisibility(View.INVISIBLE);


                    ConnectionUtils connectionUtils=new ConnectionUtils(ActiveCoursesActivity.this);
                    if(connectionUtils.isConnectionAvailable()){
                        flag_loader=true;
                        LinearLayoutManager linearLayoutManager =
                                new LinearLayoutManager(ActiveCoursesActivity.this, LinearLayoutManager.VERTICAL, false);
                        rc_course.setLayoutManager(linearLayoutManager);


                        rc_course.setVerticalScrollBarEnabled(true);

                        myprogress.setText("LEARNING GROUPS");

                        leftarrowis.setVisibility(View.INVISIBLE);
                        rightarrowis.setVisibility(View.INVISIBLE);
//                        rc_course.setAdapter(learningGroupAdapter);
//                        learningGroupAdapter.notifyDataSetChanged();
                        new CountDownTimer(100, 50) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                frame.setVisibility(View.VISIBLE);

                            }

                            @Override
                            public void onFinish() {
                                swipe.autoRefresh();

                            }
                        }.start();
                        rc_course.setAdapter(learningGroupAdapter);
                        learningGroupAdapter.notifyDataSetChanged();
                    }else {
                        myprogress.setText("LEARNING GROUPS");
                        LinearLayoutManager linearLayoutManager =
                                new LinearLayoutManager(ActiveCoursesActivity.this, LinearLayoutManager.VERTICAL, false);
                        rc_course.setLayoutManager(linearLayoutManager);

                        rc_course.setAdapter(learningGroupAdapter);
                        rc_course.setVerticalScrollBarEnabled(true);

                        leftarrowis.setVisibility(View.INVISIBLE);
                        rightarrowis.setVisibility(View.INVISIBLE);
                        learningGroupAdapter.notifyDataSetChanged();
                    }


                }else {
                    try {
                        new GetContacts().cancel(true);
                    } catch (Exception e) {

                    }
                    try {
                        new GetCourseProgress().cancel(true);
                    } catch (Exception e) {

                    }
                    Intent i=new Intent(ActiveCoursesActivity.this,JourneyMainScreenActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }


            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(click){
                    click=false;
                //
                    //  frame.setVisibility(View.INVISIBLE);


                    ConnectionUtils connectionUtils=new ConnectionUtils(ActiveCoursesActivity.this);
                    if(connectionUtils.isConnectionAvailable()){
                        flag_loader=true;
                        LinearLayoutManager linearLayoutManager =
                                new LinearLayoutManager(ActiveCoursesActivity.this, LinearLayoutManager.VERTICAL, false);
                        rc_course.setLayoutManager(linearLayoutManager);


                        rc_course.setVerticalScrollBarEnabled(true);

                        myprogress.setText("LEARNING GROUPS");

                        leftarrowis.setVisibility(View.INVISIBLE);
                        rightarrowis.setVisibility(View.INVISIBLE);
//                        rc_course.setAdapter(learningGroupAdapter);
//                        learningGroupAdapter.notifyDataSetChanged();
                        new CountDownTimer(100, 50) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            frame.setVisibility(View.VISIBLE);

                            }

                            @Override
                            public void onFinish() {
                                swipe.autoRefresh();

                            }
                        }.start();
                        rc_course.setAdapter(learningGroupAdapter);
                        learningGroupAdapter.notifyDataSetChanged();
                    }else {
                        myprogress.setText("LEARNING GROUPS");
                        LinearLayoutManager linearLayoutManager =
                                new LinearLayoutManager(ActiveCoursesActivity.this, LinearLayoutManager.VERTICAL, false);
                        rc_course.setLayoutManager(linearLayoutManager);

                        rc_course.setAdapter(learningGroupAdapter);
                        rc_course.setVerticalScrollBarEnabled(true);

                        leftarrowis.setVisibility(View.INVISIBLE);
                        rightarrowis.setVisibility(View.INVISIBLE);
                        learningGroupAdapter.notifyDataSetChanged();
                    }


                }else {
                    try {
                        new GetContacts().cancel(true);
                    } catch (Exception e) {

                    }
                    try {
                        new GetCourseProgress().cancel(true);
                    } catch (Exception e) {

                    }
                    Intent i=new Intent(ActiveCoursesActivity.this,JourneyMainScreenActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }


            }
        });
        alertDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.planet_loader);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);


        GifView gifView1 = (GifView) alertDialog.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        alertDialog.setCancelable(false);

       /* arrayList.add(new CourseModel("PHP",10));
        arrayList.add(new CourseModel("MS-CIT",40));
        arrayList.add(new CourseModel("ACCOUNTS",20));
        arrayList.add(new CourseModel("BIO",100));
        arrayList.add(new CourseModel("FRENCH",90));
        arrayList.add(new CourseModel("ANDROID",60));
        arrayList.add(new CourseModel("IOS",20));
        arrayList.add(new CourseModel("ACCOUNTS",20));
        arrayList.add(new CourseModel("BIO",100));
        arrayList.add(new CourseModel("FRENCH",90));
        arrayList.add(new CourseModel("ANDROID",60));
        arrayList.add(new CourseModel("IOS",20));
        courseAdapter.notifyDataSetChanged();*/
        ImageView imageview1 = (ImageView) findViewById(R.id.wheel1);
        ImageView imageview2 = (ImageView) findViewById(R.id.wheel2);
        ImageView imageview3 = (ImageView) findViewById(R.id.wheel3);
        ImageView imageview4 = (ImageView) findViewById(R.id.moon);
        ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(imageview1,
                "rotation", 0f, 360f);
        imageViewObjectAnimator.setDuration(6000); // miliseconds
        imageViewObjectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        imageViewObjectAnimator.setStartDelay(0);
        imageViewObjectAnimator.setInterpolator(new LinearInterpolator());

        if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode) || batLevel == 0) {

            imageViewObjectAnimator.start();
        }


        ObjectAnimator imageViewObjectAnimator1 = ObjectAnimator.ofFloat(imageview2,
                "rotation", 0f, 360f);
        imageViewObjectAnimator1.setDuration(6000); // miliseconds
        imageViewObjectAnimator1.setRepeatCount(ValueAnimator.INFINITE);
        imageViewObjectAnimator1.setStartDelay(0);
        imageViewObjectAnimator1.setInterpolator(new LinearInterpolator());

        if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode) || batLevel == 0) {

            imageViewObjectAnimator1.start();
        }


        ObjectAnimator imageViewObjectAnimator2 = ObjectAnimator.ofFloat(imageview3,
                "rotation", 0f, 360f);
        imageViewObjectAnimator2.setDuration(6000); // miliseconds
        imageViewObjectAnimator2.setRepeatCount(ValueAnimator.INFINITE);
        imageViewObjectAnimator2.setStartDelay(0);
        imageViewObjectAnimator2.setInterpolator(new LinearInterpolator());

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {

            imageViewObjectAnimator2.start();

        }

        ObjectAnimator imageViewObjectAnimator3 = ObjectAnimator.ofFloat(imageview4,
                "rotation", 360f, 0f);
        imageViewObjectAnimator3.setDuration(300000); // miliseconds
        imageViewObjectAnimator3.setRepeatCount(ValueAnimator.INFINITE);
        imageViewObjectAnimator3.setStartDelay(0);
        imageViewObjectAnimator3.setInterpolator(new LinearInterpolator());


        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {
            imageViewObjectAnimator3.start();
        }


        FrameLayout fr = (FrameLayout) findViewById(R.id.car_frame);

        ObjectAnimator imageViewObjectAnimator4 = ObjectAnimator.ofFloat(fr, "translationY", 2, 1, -2, 3, -1, 0, -2, 1, -4, 0);
        imageViewObjectAnimator4.setDuration(2000); // miliseconds
        imageViewObjectAnimator4.setRepeatCount(ValueAnimator.INFINITE);
        imageViewObjectAnimator4.setStartDelay(100);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {

            imageViewObjectAnimator4.start();

        }


        swipe.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override

            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //refreshing...
                ConnectionUtils connectionUtils=new ConnectionUtils(ActiveCoursesActivity.this);
                if(connectionUtils.isConnectionAvailable()){

                    if(!click){
                        getCourses();
                    }else {
                        new GetCourseProgress().execute();
                    }
                }else {
                    swipe.finishRefresh();
                    Toast.makeText(ActiveCoursesActivity.this,"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }

            }

            @Override

            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //load more refreshing...

            }
        });

        new CountDownTimer(100, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
                frame.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFinish() {
                swipe.autoRefresh();

            }
        }.start();

//        swipe.autoRefresh();
        //  red planet animation


        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {
            red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f, 700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);

        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700, 0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        leftarrowis.setVisibility(View.INVISIBLE);
        rightarrowis.setVisibility(View.INVISIBLE);

        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f, 1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f, 0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f, 100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f, 0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f, 1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f, 0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f, 300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f, 0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


    }




    }

    private void getCourses() {
        new GetContacts().execute();
    }

    private class GetLearningGroups extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {


            no_learning_groups.setVisibility(View.GONE);

            LinearLayoutManager linearLayoutManager =
                    new LinearLayoutManager(ActiveCoursesActivity.this, LinearLayoutManager.VERTICAL, false);
            rc_course.setLayoutManager(linearLayoutManager);

            rc_course.setAdapter(learningGroupAdapter);
            rc_course.setVerticalScrollBarEnabled(true);

            leftarrowis.setVisibility(View.INVISIBLE);
            rightarrowis.setVisibility(View.INVISIBLE);
            myprogress.setText("LEARNING GROUPS");
            arrayListpr.clear();
            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            HttpURLConnection urlConnection1 = null;
            try {


                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(ActiveCoursesActivity.this))

                        .build().toString();

                try {

                    // httpClient = new DefaultHttpClient();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/user_learning_groups");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    int responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.e("Course", responseString);
                    result1 = responseString;





                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipe.finishRefresh();
                            }catch (Exception e){

                            }
                            if(flag_loader) {
                                imageViewObjectAnimator1.cancel();
                                load_frame.setVisibility(View.GONE);
                            }
                            no_learning_groups.setText("No Learning Group Available");
                            no_learning_groups.setVisibility(View.VISIBLE);
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipe.finishRefresh();
                            }catch (Exception e){

                            }
                            if(flag_loader) {
                                imageViewObjectAnimator1.cancel();
                                load_frame.setVisibility(View.GONE);
                            }
                            no_learning_groups.setText("No Learning Group Available");
                            no_learning_groups.setVisibility(View.VISIBLE);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                  runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipe.finishRefresh();
                            }catch (Exception e){

                            }
                            if(flag_loader) {
                                imageViewObjectAnimator1.cancel();
                                load_frame.setVisibility(View.GONE);
                            }
                            no_learning_groups.setText("No Learning Group Available");
                            no_learning_groups.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            arrayListlg.clear();
            Log.e("respmsg",s);
            try{
                swipe.finishRefresh();
            }catch (Exception e){

            }
            if(flag_loader) {
                imageViewObjectAnimator1.cancel();
                load_frame.setVisibility(View.GONE);
            }


            String ss=new PrefManager(ActiveCoursesActivity.this).getAuthToken().toString();


            if(s.contains("error")){
                Log.e("respmsg","error");
                // swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONObject js=new JSONObject(s);
                    String errormsg=js.getString("error");
                    AppConstant appConstant=new AppConstant(ActiveCoursesActivity.this);
                    appConstant.showLogoutDialogue(errormsg);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("respmsg","errorexc");
                }

            }else {
                no_learning_groups.setVisibility(View.INVISIBLE);

                String resp=s;
                Log.e("respmsg","noerror");
                try {
                    JSONArray jsonArray=new JSONArray(s);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);
                        Log.e("respmsgis",String.valueOf(jsonObject));
                        LearningGroupModel learningGroupModel=new LearningGroupModel();
                        learningGroupModel.setId(jsonObject.getInt("lg_id"));

                        learningGroupModel.setIslock(jsonObject.getString("is_locked"));
                        learningGroupModel.setName(jsonObject.getString("learning_group_name"));
                        learningGroupModel.setStartdate(jsonObject.getString("start_date"));
                        learningGroupModel.setEnddate(jsonObject.getString("end_date"));
                        learningGroupModel.setType(jsonObject.getString("type"));
                        arrayListlg.add(learningGroupModel);
                        Log.e("respmsg","success");
                        learningGroupAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            if(arrayListlg.size()<1){
                no_learning_groups.setText("No Learning Group Available");
                no_learning_groups.setVisibility(View.VISIBLE);
            }else {
                no_learning_groups.setText("No Learning Group Available");
                no_learning_groups.setVisibility(View.GONE);
            }
            flag_loader=false;
        }
    }
    private String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

             json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }
    public static String readStreamis(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

    private class GetContacts extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {
            if(flag_loader) {
                load_frame.setVisibility(View.VISIBLE);
                imageViewObjectAnimator1.start();
            }else {
                load_frame.setVisibility(View.GONE);

            }
            no_learning_groups.setVisibility(View.GONE);
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                        swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        if(flag_loader) {
                            imageViewObjectAnimator1.cancel();
                            load_frame.setVisibility(View.GONE);
                        }
                        no_learning_groups.setText("No Learning Group Available");
                        no_learning_groups.setVisibility(View.VISIBLE);
                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        if(flag_loader) {
                            imageViewObjectAnimator1.cancel();
                            load_frame.setVisibility(View.GONE);
                        }
                        no_learning_groups.setText("No Learning Group Available");
                        no_learning_groups.setVisibility(View.VISIBLE);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
               runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        if(flag_loader) {
                            imageViewObjectAnimator1.cancel();
                            load_frame.setVisibility(View.GONE);
                        }
                        no_learning_groups.setText("No Learning Group Available");
                        no_learning_groups.setVisibility(View.VISIBLE);
                    }
                });
            }
            try {
                jObj = new JSONObject(result1);
                token=jObj.getString("access_token");
                token_type=jObj.getString("token_type");
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(responseCod != urlConnection.HTTP_OK){

                try{
                    swipe.finishRefresh();
                }catch (Exception e){

                } if(flag_loader) {
                    imageViewObjectAnimator1.cancel();
                    load_frame.setVisibility(View.GONE);
                }
                no_learning_groups.setText("No Learning Group Available");
                no_learning_groups.setVisibility(View.VISIBLE);
                  Toast.makeText(ActiveCoursesActivity.this,"Error while loading user data",Toast.LENGTH_SHORT).show();
                flag_loader=false;
            }else {


                new GetLearningGroups().execute();
            }
        }
    }
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    public void backJourney(View view){
        finish();
    }

    @Override
    public void onBackPressed() {

        if(click){
            click=false;

           // frame.setVisibility(View.INVISIBLE);

            ConnectionUtils connectionUtils=new ConnectionUtils(ActiveCoursesActivity.this);
            if(connectionUtils.isConnectionAvailable()){
                flag_loader=true;
                LinearLayoutManager linearLayoutManager =
                        new LinearLayoutManager(ActiveCoursesActivity.this, LinearLayoutManager.VERTICAL, false);
                rc_course.setLayoutManager(linearLayoutManager);

                rc_course.setAdapter(learningGroupAdapter);
                rc_course.setVerticalScrollBarEnabled(true);

                myprogress.setText("LEARNING GROUPS");

                leftarrowis.setVisibility(View.INVISIBLE);
                rightarrowis.setVisibility(View.INVISIBLE);
             learningGroupAdapter.notifyDataSetChanged();
                new CountDownTimer(100, 50) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                     //   frame.setVisibility(View.VISIBLE);


                    }

                    @Override
                    public void onFinish() {
                        swipe.autoRefresh();

                    }
                }.start();
            }else {
                myprogress.setText("LEARNING GROUPS");
                LinearLayoutManager linearLayoutManager =
                        new LinearLayoutManager(ActiveCoursesActivity.this, LinearLayoutManager.VERTICAL, false);
                rc_course.setLayoutManager(linearLayoutManager);

                rc_course.setAdapter(learningGroupAdapter);
                rc_course.setVerticalScrollBarEnabled(true);

                leftarrowis.setVisibility(View.INVISIBLE);
                rightarrowis.setVisibility(View.INVISIBLE);
                learningGroupAdapter.notifyDataSetChanged();
            }


        }else {
            try {
                new GetContacts().cancel(true);
            } catch (Exception e) {

            }
            try {
                new GetCourseProgress().cancel(true);
            } catch (Exception e) {

            }
            Intent i=new Intent(ActiveCoursesActivity.this,JourneyMainScreenActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

        }


    }

    public static void getProgress(int lgid){
        GridLayoutManager linearLayoutManager = new GridLayoutManager(mcontext, 3, LinearLayoutManager.HORIZONTAL, false); // MAX NUMBER OF SPACES
        rc_course.setLayoutManager(linearLayoutManager);
        courseAdapter = new CourseProgressAdapter(mcontext, arrayListpr);
        rc_course.setAdapter(courseAdapter);

        mSnapHelper.attachToRecyclerView(rc_course);

        rc_course.setVerticalScrollBarEnabled(false);
        rc_course.setOverScrollMode(View.OVER_SCROLL_NEVER);
        lg_id_is=lgid;
        flag_loader=true;
        new CountDownTimer(100, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
              //  frame.startAnimation(animation_fade);

            }

            @Override
            public void onFinish() {
                swipe.autoRefresh();

            }
        }.start();

    }
    public static class GetCourseProgress extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            if(flag_loader) {
                load_frame.setVisibility(View.VISIBLE);
                imageViewObjectAnimator1.start();
            }else {
                load_frame.setVisibility(View.GONE);
            }
            no_learning_groups.setVisibility(View.GONE);
            myprogress.setText("COURSES");
            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            HttpURLConnection urlConnection1 = null;
            try {


                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(mcontext))
                        .object("lg_id",lg_id_is)

                        .build().toString();

                try {

                    // httpClient = new DefaultHttpClient();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/user_course_progress");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    int responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStreamis(is);
                    Log.e("Course", responseString);
                    result1 = responseString;





                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (mcontext instanceof Activity) {
                        ((Activity) mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_learning_groups.setText("No Course Progress Available");
                                no_learning_groups.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                } catch (ClientProtocolException e) { e.printStackTrace();
                    if (mcontext instanceof Activity) {
                        ((Activity) mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }

                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_learning_groups.setText("No Course Progress Available");
                                no_learning_groups.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (mcontext instanceof Activity) {
                        ((Activity) mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    swipe.finishRefresh();
                                }catch (Exception e){

                                }
                                if(flag_loader) {
                                    imageViewObjectAnimator1.cancel();
                                    load_frame.setVisibility(View.GONE);
                                }
                                no_learning_groups.setText("No Course Progress Available");
                                no_learning_groups.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("respmsg",s);
            arrayListpr.clear();
            try{
                swipe.finishRefresh();
            }catch (Exception e){

            } if(flag_loader) {
                imageViewObjectAnimator1.cancel();
                load_frame.setVisibility(View.GONE);
            }
            String ss=new PrefManager(mcontext).getAuthToken().toString();

            if(s.contains("error")){
                Log.e("respmsg","error");
                // swipeRefreshLayout.setRefreshing(false);


            }else {
                no_learning_groups.setVisibility(View.INVISIBLE);

                String resp=s;
                Log.e("respmsg","noerror");
                try {
                    JSONArray jsonArray=new JSONArray(s);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);
                        Log.e("respmsgis",String.valueOf(jsonObject));
                        LearningGroupCourseProgressModel learningGroupCourseProgressModel=new LearningGroupCourseProgressModel();
                        learningGroupCourseProgressModel.setLgid(jsonObject.getInt("lg_id"));
                        learningGroupCourseProgressModel.setCourseid(jsonObject.getInt("course_id"));
                        learningGroupCourseProgressModel.setPerecentage(jsonObject.getString("percentage"));

                        learningGroupCourseProgressModel.setCoursename(jsonObject.getString("course_name"));
                        learningGroupCourseProgressModel.setIslock(jsonObject.getString("is_locked"));
                        learningGroupCourseProgressModel.setLgname(jsonObject.getString("learning_group_name"));
                        learningGroupCourseProgressModel.setCourse_dependent(jsonObject.getString("is_dependent"));

                        arrayListpr.add(learningGroupCourseProgressModel);
                        Log.e("respmsg","success");
                        courseAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(arrayListpr.size()>3){
                    leftarrowis.setVisibility(View.VISIBLE);
                    rightarrowis.setVisibility(View.VISIBLE);
                }else {
                    leftarrowis.setVisibility(View.INVISIBLE);
                    rightarrowis.setVisibility(View.INVISIBLE);
                }

            }

            if(arrayListpr.size()<1){

                no_learning_groups.setText("No Course Progress Available");
                no_learning_groups.setVisibility(View.VISIBLE);
            }else {
                no_learning_groups.setText("No Course Progress Available");
                no_learning_groups.setVisibility(View.GONE);
            }


            flag_loader=false;
        }
    }

    @Override
    protected void onResume() {
        new PrefManager(ActiveCoursesActivity.this).saveCourseProgressalive("yes");
        super.onResume();
    }

    @Override
    protected void onPause() {
        new PrefManager(ActiveCoursesActivity.this).saveCourseProgressalive("yes");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        new PrefManager(ActiveCoursesActivity.this).saveCourseProgressalive("no");
       click=false;

        super.onDestroy();
    }
}
