package com.globalgyan.getvalyou.helper;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit retrofit = null;

    private static Retrofit gameRetrofit = null;

    private static Retrofit otpRetrofit = null;


    public static Retrofit getClient(String baseUrl) {


        if (retrofit == null) {
/*
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient();
// add your other interceptors …

// add logging as last interceptor
            httpCliooooolooent.interceptors().add(logging);  // <-- this is the important line!*/
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor).build();
            Gson gson = new GsonBuilder().create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getGameClient(String baseUrl) {


        if (gameRetrofit == null) {
            Gson gson = new GsonBuilder().create();
            gameRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    //.client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return gameRetrofit;
    }

    public static Retrofit getOtpClient(String baseUrl) {


        if (otpRetrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor).build();
            Gson gson = new GsonBuilder().create();
            otpRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return otpRetrofit;
    }

    static  class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();

            long t1 = System.nanoTime();
            Log.e("Sending request",
                    "" + request.url());

            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            Log.e("Received response for ", "" + (t2 - t1) / 1e6d);


            final String responseString = new String(response.body().bytes());

            Log.e("Response: ", "" + responseString);

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), responseString))
                    .build();
        }
    }
}