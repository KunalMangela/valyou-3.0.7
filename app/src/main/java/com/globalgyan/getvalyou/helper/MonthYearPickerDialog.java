package com.globalgyan.getvalyou.helper;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import com.globalgyan.getvalyou.R;

import java.util.Calendar;

public class MonthYearPickerDialog extends DialogFragment {

  private static final int MAX_YEAR = 2016;
  private DatePickerDialog.OnDateSetListener listener;
  private boolean showMonth = false;

  public void setListener(DatePickerDialog.OnDateSetListener listener, boolean showMonth) {
    this.listener = listener;
    this.showMonth = showMonth;
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    // Get the layout inflater
    LayoutInflater inflater = getActivity().getLayoutInflater();

    Calendar cal = Calendar.getInstance();

    View dialog = inflater.inflate(R.layout.date_picker_dialog, null);
    final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
    final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);
    if(showMonth)
      monthPicker.setVisibility(View.VISIBLE);
    else
      monthPicker.setVisibility(View.GONE);

    Button btnOkay = (Button) dialog.findViewById(R.id.btnOkay);
    btnOkay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), 0);
        MonthYearPickerDialog.this.getDialog().cancel();
      }
    });

    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
    btnCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        MonthYearPickerDialog.this.getDialog().cancel();
      }
    });
    monthPicker.setMinValue(1);
    monthPicker.setMaxValue(12);
    monthPicker.setValue(cal.get(Calendar.MONTH) + 1);

    int year = cal.get(Calendar.YEAR);
    yearPicker.setMinValue(1970);
    yearPicker.setMaxValue(MAX_YEAR);
    yearPicker.setValue(1970);

    builder.setView(dialog)
        // Add action buttons
        /*.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int id) {

          }
        })
        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {

          }
        })*/;
    return builder.create();
  }
}