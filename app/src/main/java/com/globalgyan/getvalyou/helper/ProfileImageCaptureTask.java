package com.globalgyan.getvalyou.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.view.WindowManager;

import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.interfaces.ImageSaveCallback;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 14/11/16
 *         Module : Valyou.
 */
public class ProfileImageCaptureTask extends AsyncTask<Void, Void, Bitmap> {



    private ProgressDialog alertDialog = null;
    private Context context = null;
    private ImageSaveCallback callback = null;

    public ProfileImageCaptureTask(Context context, ImageSaveCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        alertDialog = new ProgressDialog(context);
        alertDialog.setTitle("saving video, please wait");
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.show();
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {

        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

        mediaMetadataRetriever.setDataSource(AppConstant.tempFile);
        Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(5000000); //unit in microsecond
        return bmFrame;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        alertDialog.dismiss();
      //  dialog.dismiss();
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(AppConstant.stillFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        callback.imageCreated(AppConstant.stillFile);

    }
}
