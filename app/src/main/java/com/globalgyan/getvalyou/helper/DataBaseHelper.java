package com.globalgyan.getvalyou.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.PriorExperience;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.model.ProfileDetails;
import com.globalgyan.getvalyou.model.QuestionAnswerModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 24/11/16
 *         Module : Valyou.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    // Contacts table name
    public static final String TABLE_PROFILE = "profile";
    public static final String TABLE_COLLEGE = "college";
    public static final String TABLE_PROFESSION = "profession";
    public static final String TABLE_PRIOR_WORK = "prior_work";
    //public static final String TABLE_TEMP = "temp";
    public static final String TABLE_CALENDAR = "calendar";
    public static final String TABLE_SCRIPT = "script";
    public static final String TABLE_STRING_CONTANTS = "string_constants";
    public static final String TABLE_VIDEO_RESPONSE = "video_response";
    public static final String TABLE_VIDEO_RESPONSE_ID = "video_response_id";
    public static final String TABLE_UPLOAD_VIDEO_RESPONSE = "upload_video_response";
    public static final String KEY_ID = "id";
    public static final String KEY_STRING_UPLOAD_VIDEO_PROFILE = "upload_video_profile";
    public static final String KEY_STRING_VIDEO_RESPONSE_OBJ_ID = "upload_video_response_id";
    public static final String KEY_STRING_SELCTED_EDUCATON = "selected_education";
    public static final String KEY_STRING_SELCTED_PROFESSION = "selected_profession";
    public static final String KEY_STRING_IS_OPEN_FOR_EDIT = "is_open_for_edit";
    public static final String KEY_VIDEO_RESPONSE_QUST = "video_response_qst";
    public static final String KEY_VIDEO_RESPONSE_FILES = "video_response_files";
    public static final String KEY_CANDIDATE_ID = "candidate_id";
    public static final String KEY_PROFILE_NAME = "name";
    public static final String KEY_PROFILE_GENDER = "gender";
    public static final String KEY_PROFILE_DOB = "dob";
    public static final String KEY_PROFILE_MOBILE = "mobile";
    public static final String KEY_PROFILE_EMAIL = "email";
    public static final String KEY_PROFILE_EMPLOY_STATUS = "employ_status";
    public static final String KEY_PROFILE_INDUSTRY = "industry";
    public static final String KEY_PROFILE_DOMAIN = "domain";
    public static final String KEY_PROFILE_LOCATION = "location";
    public static final String KEY_PROFILE_DESCRIPTION = "description";
    public static final String KEY_PROFILE_IMAGE_URL = "image_url";
    public static final String KEY_PROFILE_VIDEO_URL = "video_url";
    public static final String KEY_SCRIPT_QUESTION = "script_question";
    public static final String KEY_SCRIPT_ANSWER = "script_answer";
    public static final String KEY_SCRIPT_RIGHT_ANSWER = "script_right_answer";
    public static final String KEY_COLLEGE_NAME = "college";
    public static final String KEY_COLLEGE_DEGREE = "degree";
    public static final String KEY_COLLEGE_YEAR_FROM = "college_from";
    public static final String KEY_COLLEGE_YEAR_TO = "college_to";
    public static final String KEY_PRIOR_NATURE_WORK = "nature";
    public static final String KEY_PRIOR_ORG_NAME = "org_name";
    public static final String KEY_PRIOR_TIME_PERIOD = "time";
    public static final String KEY_CALENDAR_ID = "calendar_id";
    public static final String KEY_CALENDER_STATUS = "status";
    public static final String KEY_PROFESSION_POSITION_HELD = "position_held";
    public static final String KEY_PROFESSION_ORG_NAME = "profesion_org_name";
    public static final String KEY_PROFESSION_TIME_PERIOD_FROM = "profession_time_from";
    public static final String KEY_PROFESSION_TIME_PERIOD_TO = "profession_time_to";
    public static final String KEY_UPLOAD_VIDEO_RESPONSE_STATUS = "upload_video_response_status";
    public static final String KEY_UPLOAD_VIDEO_RESPONSE_OBJECT_ID = "upload_video_response_object_id";
    public static final String KEY_UPLOAD_VIDEO_ID = "upload_video_id";
    public static final String KEY_UPLOADED_VIDEO_ID = "uploaded_video_id";


    private static final String Val_ass_fungame_word = "wordwaterfall";
    private static final String wordid = "id";
    private static final String words = "Name";


    private static final String Val_ass_fungame_score = "wordwaterfall_score";
    private static final String scoreid = "id";
    private static final String Game_Name = " game_name";
    private static final String Game_score = "score";
    private static final String Attribute = "attribute";
    private static final String Date_and_Time = "date_and_time";
    private static final String Game_Cycle = " game_cycle";


    // All Static variables
    // Database Version
    public static final int DATABASE_VERSION =4;
    // Database Name
    private static final String DATABASE_NAME = "ValYou";

    public DataBaseHelper(Context context) {
       /* super(context, Environment.getExternalStorageDirectory()
                + File.separator  + DATABASE_NAME, null, DATABASE_VERSION);*/
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PROFILE_TABLE = "CREATE TABLE " + TABLE_PROFILE + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_PROFILE_NAME + " TEXT,"
                + KEY_PROFILE_GENDER + " TEXT,"
                + KEY_PROFILE_DOB + " TEXT,"
                + KEY_PROFILE_MOBILE + " TEXT,"
                + KEY_PROFILE_EMAIL + " TEXT,"
                + KEY_PROFILE_EMPLOY_STATUS + " TEXT,"
                + KEY_PROFILE_INDUSTRY + " TEXT,"
                + KEY_PROFILE_LOCATION + " TEXT,"
                + KEY_PROFILE_DESCRIPTION + " TEXT,"
                + KEY_PROFILE_DOMAIN + " TEXT,"
                + KEY_PROFILE_IMAGE_URL + " TEXT,"
                + KEY_PROFILE_VIDEO_URL + " TEXT" + ")";

        String CREATE_COLLEGE_TABLE = "CREATE TABLE " + TABLE_COLLEGE + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_COLLEGE_NAME + " TEXT,"
                + KEY_COLLEGE_DEGREE + " TEXT,"
                + KEY_COLLEGE_YEAR_FROM + " TEXT,"
                + KEY_COLLEGE_YEAR_TO + " TEXT" + ")";
        String CREATE_PROFESSION_TABLE = "CREATE TABLE " + TABLE_PROFESSION + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_PROFESSION_POSITION_HELD + " TEXT,"
                + KEY_PROFESSION_ORG_NAME + " TEXT,"
                + KEY_PROFESSION_TIME_PERIOD_FROM + " TEXT,"
                + KEY_PROFESSION_TIME_PERIOD_TO + " TEXT" + ")";

        String CREATE_PRIOR_WORK_TABLE = "CREATE TABLE " + TABLE_PRIOR_WORK + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_PRIOR_NATURE_WORK + " TEXT,"
                + KEY_PRIOR_ORG_NAME + " TEXT,"
                + KEY_PRIOR_TIME_PERIOD + " TEXT" + ")";
       /* String CREATE_TEMP_TABLE = "CREATE TABLE " + TABLE_TEMP + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_COLLEGE_NAME + " TEXT,"
                + KEY_COLLEGE_DEGREE + " TEXT,"
                + KEY_COLLEGE_YEAR + " TEXT,"
                + KEY_PROFESSION_POSITION_HELD + " TEXT,"
                + KEY_PROFESSION_ORG_NAME + " TEXT,"
                + KEY_PROFESSION_TIME_PERIOD + " TEXT,"
                + KEY_PRIOR_NATURE_WORK + " TEXT,"
                + KEY_PRIOR_ORG_NAME + " TEXT,"
                + KEY_PRIOR_TIME_PERIOD + " TEXT" + ")";*/


        String CREATE_SCRIPT_TABLE = "CREATE TABLE " + TABLE_SCRIPT + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_SCRIPT_QUESTION + " TEXT,"
                + KEY_SCRIPT_ANSWER + " TEXT,"
                + KEY_SCRIPT_RIGHT_ANSWER + " TEXT" + ")";

        String CREATE_CALENDAR_TABLE = "CREATE TABLE " + TABLE_CALENDAR + "("
                + KEY_CALENDAR_ID + " TEXT,"
                + KEY_CALENDER_STATUS + " TEXT" + ")";

        String CREATE_STRING_CONSTANTS_TABLE = "CREATE TABLE " + TABLE_STRING_CONTANTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_STRING_UPLOAD_VIDEO_PROFILE + " TEXT,"
                + KEY_STRING_VIDEO_RESPONSE_OBJ_ID + " TEXT,"
                + KEY_STRING_SELCTED_EDUCATON + " TEXT,"
                + KEY_STRING_SELCTED_PROFESSION + " TEXT,"
                + KEY_STRING_IS_OPEN_FOR_EDIT + " TEXT" + ")";

        String CREATE_VIDEO_RESPONSE_TABLE = "CREATE TABLE " + TABLE_VIDEO_RESPONSE + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_VIDEO_RESPONSE_QUST + " TEXT,"
                + KEY_VIDEO_RESPONSE_FILES + " TEXT" + ")";

        String CREATE_UPLOAD_VIDEO_RESPONSE_TABLE = "CREATE TABLE " + TABLE_UPLOAD_VIDEO_RESPONSE + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_UPLOAD_VIDEO_RESPONSE_OBJECT_ID + " TEXT,"
                + KEY_UPLOAD_VIDEO_RESPONSE_STATUS + " TEXT ,"
                + KEY_UPLOAD_VIDEO_ID + " TEXT"+")";








        db.execSQL(CREATE_PROFILE_TABLE);
        db.execSQL(CREATE_COLLEGE_TABLE);
        db.execSQL(CREATE_PROFESSION_TABLE);
        db.execSQL(CREATE_PRIOR_WORK_TABLE);
        db.execSQL(CREATE_CALENDAR_TABLE);
        db.execSQL(CREATE_SCRIPT_TABLE);
        db.execSQL(CREATE_STRING_CONSTANTS_TABLE);
        db.execSQL(CREATE_VIDEO_RESPONSE_TABLE);
        db.execSQL(CREATE_UPLOAD_VIDEO_RESPONSE_TABLE);

        if(isTableExists(TABLE_VIDEO_RESPONSE_ID,db)){
            Log.e("table"," present");
        }else {
            Log.e("table"," not present");
            String CREATE_UPLOADED_VIDEO_RESPONSE_TABLEIS = "CREATE TABLE " + TABLE_VIDEO_RESPONSE_ID + "("
                    + KEY_ID + " INTEGER PRIMARY KEY,"
                    + KEY_UPLOADED_VIDEO_ID + " TEXT"+")";
            db.execSQL(CREATE_UPLOADED_VIDEO_RESPONSE_TABLEIS);
        }

        if(isTableExists(Val_ass_fungame_word,db)){
            Log.e("table"," present");
        }else {
            Log.e("table"," not present");
            String CREATE_WORDWATERFALL_TABLE = "CREATE TABLE "+Val_ass_fungame_word+
                    " ("+wordid+" INTEGER PRIMARY KEY AUTOINCREMENT, "+words+" VARCHAR(255));";

            db.execSQL(CREATE_WORDWATERFALL_TABLE);
        }

        if(isTableExists(Val_ass_fungame_score,db)){
            Log.e("table"," present");
        }else {
            Log.e("table"," not present");
            String CREATE_WORDWATERFALL_SCORE_TABLE = "CREATE TABLE "+Val_ass_fungame_score+
                    " ("+scoreid+" INTEGER PRIMARY KEY AUTOINCREMENT,  "+Game_Name+" VARCHAR(255), "+Game_score+" VARCHAR(255), "+Attribute+" VARCHAR(255), "+Date_and_Time+" VARCHAR(255), "+Game_Cycle+" INTEGER);";


            db.execSQL(CREATE_WORDWATERFALL_SCORE_TABLE);
        }
      /*  try {
            if (TABLE_VIDEO_RESPONSE_ID.length() > 0) {
                Log.e("table"," present");
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e("table"," exception");

        }
*/
    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed

        if(isTableExists(TABLE_VIDEO_RESPONSE_ID,db)){
            Log.e("table"," present");
        }else {
            Log.e("table"," not present");
            String CREATE_UPLOADED_VIDEO_RESPONSE_TABLEIS = "CREATE TABLE " + TABLE_VIDEO_RESPONSE_ID + "("
                    + KEY_ID + " INTEGER PRIMARY KEY,"
                    + KEY_UPLOADED_VIDEO_ID + " TEXT"+")";
            db.execSQL(CREATE_UPLOADED_VIDEO_RESPONSE_TABLEIS);
        }


        if(isTableExists(Val_ass_fungame_word,db)){
            Log.e("table"," present");
        }else {
            Log.e("table"," not present");
            String CREATE_WORDWATERFALL_TABLE = "CREATE TABLE "+Val_ass_fungame_word+
                    " ("+wordid+" INTEGER PRIMARY KEY AUTOINCREMENT, "+words+" VARCHAR(255));";

            db.execSQL(CREATE_WORDWATERFALL_TABLE);
        }

        if(isTableExists(Val_ass_fungame_score,db)){
            Log.e("table"," present");
        }else {
            Log.e("table"," not present");
            String CREATE_WORDWATERFALL_SCORE_TABLE = "CREATE TABLE "+Val_ass_fungame_score+
                    " ("+scoreid+" INTEGER PRIMARY KEY AUTOINCREMENT,  "+Game_Name+" VARCHAR(255), "+Game_score+" VARCHAR(255), "+Attribute+" VARCHAR(255), "+Date_and_Time+" VARCHAR(255), "+Game_Cycle+" INTEGER);";


            db.execSQL(CREATE_WORDWATERFALL_SCORE_TABLE);
        }
    }

    public long insertInWordWaterfall(String word){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(words,word);

        long id = sqLiteDatabase.insert(Val_ass_fungame_word,null,contentValues);
        return id;
    }
    public  boolean CheckIsWordAlreadyInDBorNot(String fieldValue) {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        String [] columns = {words};

        Cursor cursor = sqldb.query(Val_ass_fungame_word,columns, words+" = '"+fieldValue+"'",null,null,null,null);
        //String Query = "Select * from " + DBinfo.tableName + " where " + DBinfo.names + " = " + fieldValue;
        // Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public long insertInWordWaterfallscore(String gamename, String score, String attribute, String datentime, String gamecycles){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Game_Name,gamename);
        contentValues.put(Game_score,score);
        contentValues.put(Attribute,attribute);
        contentValues.put(Date_and_Time, datentime);
        contentValues.put(Game_Cycle, gamecycles);

        long id = sqLiteDatabase.insert(Val_ass_fungame_score,null,contentValues);
        return id;
    }


    public void deleteAllTables() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_PROFILE);
        db.execSQL("delete from " + TABLE_COLLEGE);
        db.execSQL("delete from " + TABLE_PROFESSION);
        db.execSQL("delete from " + TABLE_PRIOR_WORK);
        db.execSQL("delete from " + TABLE_SCRIPT);

        db.execSQL("delete from " + TABLE_UPLOAD_VIDEO_RESPONSE);
        db.close();
    }

    public void deleteScriptTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_SCRIPT);
        db.close();
    }

    public void deleteUploadVideoResponseTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_UPLOAD_VIDEO_RESPONSE);
        db.close();
    }


    public long getMaxId(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT MAX(id) AS max_id FROM " + table;
        Cursor cursor = db.rawQuery(query, null);

        int id = 0;
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return id;
    }

    public long createTable(String table, ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        long id = db.insert(table, null, values);
        db.close();
        return id;
    }

    public long createTable(String table, String key, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(key, value);
        long id = db.insert(table, null, values);
        db.close();
        return id;
    }


    public long createCalenderEvents(String id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CALENDAR_ID, id);
        values.put(KEY_CALENDER_STATUS, status);

        long ids = db.insert(TABLE_CALENDAR, null, values);
        db.close();
        return ids;
    }

    public long createProfessionlDetails(Professional professional) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROFESSION_POSITION_HELD, professional.getPositionHeld());
        values.put(KEY_PROFESSION_ORG_NAME, professional.getOrganizationName());
        values.put(KEY_PROFESSION_TIME_PERIOD_FROM, professional.getFrom());
        values.put(KEY_PROFESSION_TIME_PERIOD_TO, professional.getTo());

        long id = db.insert(TABLE_PROFESSION, null, values);
        db.close();
        return id;
    }

    public void updateProfession(String id, Professional professional) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROFESSION_POSITION_HELD, professional.getPositionHeld());
        values.put(KEY_PROFESSION_ORG_NAME, professional.getOrganizationName());
        values.put(KEY_PROFESSION_TIME_PERIOD_FROM, professional.getFrom());
        values.put(KEY_PROFESSION_TIME_PERIOD_TO, professional.getTo());
        try {
            db.update(TABLE_PROFESSION, values,
                    KEY_ID + " = ?",
                    new String[]{id});
        } catch (Exception ex) {
            Logger.error("Exception", ex.getMessage());
        }

    }

   /* public long createPrior(PriorExperience professional) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PRIOR_NATURE_WORK, professional.getNatureOfWork());
        values.put(KEY_PRIOR_ORG_NAME, professional.getOrganizationName());
        values.put(KEY_PRIOR_TIME_PERIOD, professional.getTimePeriod());

        long id = db.insert(TABLE_PRIOR_WORK, null, values);
        db.close();
        return id;
    }

    public void updatePriorWork(String id, PriorExperience professional) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PRIOR_NATURE_WORK, professional.getNatureOfWork());
        values.put(KEY_PRIOR_ORG_NAME, professional.getOrganizationName());
        values.put(KEY_PRIOR_TIME_PERIOD, professional.getTimePeriod());
        try {
            db.update(TABLE_PRIOR_WORK, values,
                    KEY_ID + " = ?",
                    new String[]{id});
        } catch (Exception ex) {
            Logger.error("Exception", ex.getMessage());
        }

    }*/

    public long createEducation(EducationalDetails professional) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_COLLEGE_NAME, professional.getCollegeName());
        values.put(KEY_COLLEGE_DEGREE, professional.getDegreeName());
        values.put(KEY_COLLEGE_YEAR_FROM, professional.getFrom());
        values.put(KEY_COLLEGE_YEAR_TO, professional.getTo());

        long id = db.insert(TABLE_COLLEGE, null, values);
        db.close();
        return id;
    }

    public long createVideoResponse(String question, String files) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_VIDEO_RESPONSE_QUST, question);
        values.put(KEY_VIDEO_RESPONSE_FILES, files);

        long id = db.insert(TABLE_VIDEO_RESPONSE, null, values);
        db.close();
        return id;
    }

    // Adding new uploaded video id
    public void putUploadedVideoId(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_UPLOADED_VIDEO_ID, id);

        // Inserting Row
        db.insert(TABLE_VIDEO_RESPONSE_ID, null, values);
        db.close(); // Closing database connection
    }

    public List<String> getAllContacts() {
        List<String> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_VIDEO_RESPONSE_ID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                contactList.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    public String getUploadedVideoID(String id_is) {
        // Select All Query
        String status="false";
        String selectQuery = "SELECT  * FROM " + TABLE_VIDEO_RESPONSE_ID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                if(cursor.getString(1).equalsIgnoreCase(id_is)){
                    status="true";
                }
            } while (cursor.moveToNext());
        }

        // return contact list
        return status;
    }

    public void updateEducation(String id, EducationalDetails professional) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_COLLEGE_NAME, professional.getCollegeName());
        values.put(KEY_COLLEGE_DEGREE, professional.getDegreeName());
        values.put(KEY_COLLEGE_YEAR_FROM, professional.getFrom());
        values.put(KEY_COLLEGE_YEAR_TO, professional.getTo());
        try {
            db.update(TABLE_COLLEGE, values,
                    KEY_ID + " = ?",
                    new String[]{id});
        } catch (Exception ex) {
            Logger.error("Exception", ex.getMessage());
        }

    }

    public void updateTableDetails(String id, String table, String key, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(key, value);
        try {
            db.update(table, values,
                    KEY_ID + " = ?",
                    new String[]{id});
        } catch (Exception ex) {
            Logger.error("Exception", ex.getMessage());
        }

    }


    public String getDataFromTable(String tableName, String key) {
        String retunString = null;
        String selectQuery = "SELECT  " + key + " FROM " + tableName;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                retunString = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        db.close();
        return retunString;
    }

    public String getDataFromTableWithId(String tableName, String key, String id) {
        String retunString = null;
        String selectQuery = "SELECT  " + key + " FROM " + tableName + " where id=" + id;
        Log.e("---Querry", "" + selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                retunString = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        db.close();
        return retunString;
    }

    public String getVideoFromTableWithId(String tableName, String key, String id) {
        String retunString = null;
        String selectQuery = "SELECT  " + key + " FROM " + tableName + " where " + KEY_UPLOAD_VIDEO_RESPONSE_OBJECT_ID + " = '" + id + "'";
        Log.e("---Querry", "" + selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                retunString = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        db.close();
        return retunString;
    }

    public void updateVideoResponseStatus(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_UPLOAD_VIDEO_RESPONSE_STATUS, "error");

        try {
          int sqlQuery= db.update(TABLE_UPLOAD_VIDEO_RESPONSE, values,
                    KEY_UPLOAD_VIDEO_ID + " = ?",
                    new String[]{id});
            Log.e("QUERY---",sqlQuery+"");
        } catch (Exception ex) {
            Logger.error("Exception", ex.getMessage());
        }

    }
    public void updateUploadVideoId(String id, String objectId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_UPLOAD_VIDEO_ID, id);

        try {
            int sqlQuery= db.update(TABLE_UPLOAD_VIDEO_RESPONSE, values,
                    KEY_UPLOAD_VIDEO_RESPONSE_OBJECT_ID + " = ?",
                    new String[]{objectId});
            Log.e("QUERY---",sqlQuery+"");
        } catch (Exception ex) {
            Logger.error("Exception", ex.getMessage());
        }

    }

    /*public String getProfileScript() {
        String returnString = null;
        String selectQuery = "SELECT  * FROM " + TABLE_SCRIPT;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                returnString = cursor.getString(1);
                if (TextUtils.isEmpty(returnString))
                    returnString = cursor.getString(2);
                else
                    returnString += " " + cursor.getString(2);
                if (TextUtils.isEmpty(returnString))
                    returnString = cursor.getString(3);
                else
                    returnString += " " + cursor.getString(3);
                if (TextUtils.isEmpty(returnString))
                    returnString = cursor.getString(4);
                else
                    returnString += " " + cursor.getString(4);
               *//* if (TextUtils.isEmpty(returnString))
                    returnString = cursor.getString(5);
                else
                    returnString += cursor.getString(5);*//*
                if (TextUtils.isEmpty(returnString))
                    returnString = cursor.getString(6);


            } while (cursor.moveToNext());
        }

        db.close();

        return returnString;
    }
*/
    public ProfileDetails getProfileDetails() {
        String selectQuery = "SELECT  * FROM " + TABLE_PROFILE;
        ProfileDetails details = new ProfileDetails();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                details.setName(cursor.getString(1));
                details.setGender(cursor.getString(2));
                details.setDob(cursor.getString(3));
                details.setEmploymentStatus(cursor.getString(4));
                details.setIndustries(cursor.getString(5));
                details.setLocation(cursor.getString(6));

                details.setDomanSelected(null);


            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return details;
    }

    public void deleteEducation(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_COLLEGE + " where " + KEY_ID + "=" + id);
        db.close();
    }

    public void deletePriorWork(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_PRIOR_WORK + " where " + KEY_ID + "=" + id);
        db.close();
    }

    public void deleteVideoResponses() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_VIDEO_RESPONSE);
        db.close();
    }

    public void deleteProfessional(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_PROFESSION + " where " + KEY_ID + "=" + id);
        db.close();
    }

    public EducationalDetails getEducationFromId(String id) {
        String selectQuery = "SELECT  * FROM " + TABLE_COLLEGE + " where id = " + id;
        EducationalDetails details = null;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                details = new EducationalDetails();
                details.setId(String.valueOf(cursor.getInt(0)));
                details.setCollegeName(cursor.getString(1));
                details.setDegreeName(cursor.getString(2));
                details.setFrom(cursor.getString(3));
                details.setTo(cursor.getString(4));

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return details;
    }

    public ArrayList<EducationalDetails> getEducationalDetails() {
        String selectQuery = "SELECT  * FROM " + TABLE_COLLEGE;
        ArrayList<EducationalDetails> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                EducationalDetails details = new EducationalDetails();
                details.setId(String.valueOf(cursor.getInt(0)));
                details.setCollegeName(cursor.getString(1));
                details.setDegreeName(cursor.getString(2));
                details.setFrom(cursor.getString(3));
                details.setTo(cursor.getString(4));
                arrayList.add(details);

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return arrayList;
    }

    public ArrayList<QuestionAnswerModel> getAllRightScripAnswers() {
        String selectQuery = "SELECT  * FROM " + TABLE_SCRIPT;
        ArrayList<QuestionAnswerModel> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionAnswerModel details = new QuestionAnswerModel();
                details.setId(String.valueOf(cursor.getInt(0)));
                details.setQuestion(cursor.getString(1));
                details.setAnswer(cursor.getString(2));
                details.setRightAnswer(cursor.getString(3));
                arrayList.add(details);

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return arrayList;
    }

    public List<String> getVideoResponseQuestions() {
        String selectQuery = "SELECT  * FROM " + TABLE_VIDEO_RESPONSE;
        List<String> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                arrayList.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return arrayList;
    }

    public List<String> getVideoResponseFiles() {
        String selectQuery = "SELECT  * FROM " + TABLE_VIDEO_RESPONSE;
        List<String> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                arrayList.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return arrayList;
    }


    public Professional getProfessionDetailsFromId(String id) {
        String selectQuery = "SELECT  * FROM " + TABLE_PROFESSION + " where id = " + id;
        Professional details = null;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                details = new Professional();
                details.setId(String.valueOf(cursor.getInt(0)));
                details.setPositionHeld(cursor.getString(1));
                details.setOrganizationName(cursor.getString(2));

                details.setFrom(cursor.getString(3));
                details.setTo(cursor.getString(4));

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return details;
    }

    public ArrayList<Professional> getProfessionalDetails() {
        String selectQuery = "SELECT  * FROM " + TABLE_PROFESSION;
        ArrayList<Professional> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Professional details = new Professional();
                details.setId(String.valueOf(cursor.getInt(0)));
                details.setPositionHeld(cursor.getString(1));
                details.setOrganizationName(cursor.getString(2));
                details.setFrom(cursor.getString(3));
                details.setTo(cursor.getString(4));
                arrayList.add(details);

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return arrayList;
    }

    public ArrayList<PriorExperience> getPriorWorkExp() {
        String selectQuery = "SELECT  * FROM " + TABLE_PRIOR_WORK;
        ArrayList<PriorExperience> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                PriorExperience details = new PriorExperience();
                details.setId(String.valueOf(cursor.getInt(0)));
                details.setOrganizationName(cursor.getString(1));
                details.setNatureOfWork(cursor.getString(2));
                details.setTimePeriod(cursor.getString(3));
                arrayList.add(details);

            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return arrayList;
    }


    public boolean calenderEventExists(String id) {


        String selectQuery = "SELECT  * FROM " + TABLE_CALENDAR + " where " + KEY_CALENDAR_ID + "='" + id + "'";
        ArrayList<PriorExperience> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }
    public boolean isTableExists(String tableName, SQLiteDatabase db) {
        /*if(db.isOpen()) {
            if(db == null || !db.isOpen()) {
                db = getReadableDatabase();
            }

            if(!db.isReadOnly()) {
                db.close();
                db = getReadableDatabase();
            }
        }*/
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

}

