package com.globalgyan.getvalyou.helper;

import android.os.AsyncTask;
import android.util.Log;

import com.globalgyan.getvalyou.interfaces.VideoReziser;
import com.globalgyan.getvalyou.videocompression.MediaController;

public class VideoCompressor extends AsyncTask<Void, Void, Boolean> {

    private String filePath = null;
    private String outPath = null;
    private VideoReziser videoReziser = null;
    private int currentIndex = 0;
    private boolean queue = false;

    public VideoCompressor(String filePath, VideoReziser videoReziser, String outPath, int currentIndex, boolean queue) {
        this.filePath = filePath;
        this.videoReziser = videoReziser;
        this.outPath = outPath;
        this.currentIndex = currentIndex;
        this.queue = queue;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        MediaController.outputPath = outPath;
        return MediaController.getInstance().convertVideo(filePath);
    }

    @Override
    protected void onPostExecute(Boolean compressed) {
        super.onPostExecute(compressed);
        if (compressed) {
            Log.e("Compression", "Compression successfully!");
            Log.e("Compressed File Path", "" + MediaController.cachedFile.getPath());
            videoReziser.onVideoResized(MediaController.cachedFile.getPath(), currentIndex, queue);

        }

    }
}