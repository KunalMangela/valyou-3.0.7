package com.globalgyan.getvalyou;

public class LearningGroupModel {
    String name,islock,startdate,enddate,type,certi_id,desc,learning_group_icon;
    int id;

    public String getName() {
        return name;
    }

    public String getCerti_id() {
        return certi_id;
    }

    public void setCerti_id(String certi_id) {
        this.certi_id = certi_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLearning_group_icon() {
        return learning_group_icon;
    }

    public void setLearning_group_icon(String learning_group_icon) {
        this.learning_group_icon = learning_group_icon;
    }

    public String getIslock() {
        return islock;
    }

    public void setIslock(String islock) {
        this.islock = islock;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
