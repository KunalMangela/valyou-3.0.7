package com.globalgyan.getvalyou;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.Professional;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

/**
 * Created by sagar on 28/2/17.
 */

public class ProfessionalDetailsListAdapter extends RecyclerView.Adapter<ProfessionalDetailsListAdapter.ViewHolder> {
    Context context = null;

    private Dialog dialog = null;


    private ArrayList<Professional> professionalDetailses = null;



    public ProfessionalDetailsListAdapter(Context context, ArrayList<Professional> professionalDetailses) {
        this.context = context;
        this.professionalDetailses = professionalDetailses;

    }

    @Override
    public ProfessionalDetailsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.professional_details_list_item, parent, false);
        ProfessionalDetailsListAdapter.ViewHolder viewHolder = new ProfessionalDetailsListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProfessionalDetailsListAdapter.ViewHolder holder, int position) {
        final Professional ProfessionalDetails = professionalDetailses.get(position);

        holder.txtOrganisationName.setText(ProfessionalDetails.getOrganizationName());
        holder.txtPositionName.setText(ProfessionalDetails.getPositionHeld());
        //holder.txtDate.setText(ProfessionalDetails.getFrom()+" to "+ProfessionalDetails.getTo());

        if(!TextUtils.isEmpty(ProfessionalDetails.getTo()) && ProfessionalDetails.getTo().length()>2) {
            holder.txtDate.setText(ProfessionalDetails.getFrom() +" to "+ProfessionalDetails.getTo());
        }else{
            holder.txtDate.setText(ProfessionalDetails.getFrom() +" to current");
        }

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
                long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                Log.e("----SELECTION ID",selectedid+"");
                if (selectedid > 0) {
                    dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_PROFESSION, ProfessionalDetails.getId());
                } else {
                    dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_PROFESSION, ProfessionalDetails.getId());
                }
                Activity activity = (Activity) context;
                Intent it= new Intent(context, ProfessionalDetailsActivity.class);
                it.putExtra("kya","prof");
               // context.startActivity(it);
                if (context instanceof Activity) {
                    ((Activity) context).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    ((Activity) context).finish();
                    ((Activity) context).startActivity(it);
                }

            }
        });
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(ProfessionalDetails.getId(),ProfessionalDetails,ProfessionalDetails.getTo());

            }
        });

    }

    @Override
    public int getItemCount() {
        return professionalDetailses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtOrganisationName;
        TextView txtPositionName;
        TextView txtDate;
        LinearLayout editButton;
        LinearLayout deleteButton;

        public ViewHolder(View itemView) {
            super(itemView);

            txtOrganisationName = (TextView) itemView.findViewById(R.id.degreeName);
            txtPositionName = (TextView) itemView.findViewById(R.id.collegeName);
            txtDate = (TextView) itemView.findViewById(R.id.dates);
            editButton = (LinearLayout) itemView.findViewById(R.id.editButton);
            deleteButton = (LinearLayout) itemView.findViewById(R.id.deleteButton);

        }
    }

    public void openDialog(final String id, final Professional professional, final String toDate) {

        dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_video_view);
        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        FontTextView infoText = (FontTextView) dialog.findViewById(R.id.dialog_info);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        Button btnyes = (Button) dialog.findViewById(R.id.btnYes);
        infoText.setText("This will delete your professional details, Are you sure?");
        btnNo.setText("No");
      btnyes.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(!TextUtils.isEmpty(toDate) && toDate.length()>2) {

              }else{
                  new PrefManager(context).saveCurrentWork("no");

              }

              Handler handler = new Handler();
              handler.postDelayed(new Runnable() {
                  @Override
                  public void run() {
                      dialog.dismiss();
                      DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
                      dataBaseHelper.deleteProfessional(id);
                      professionalDetailses.remove(professional);
                      notifyDataSetChanged();
                  }
              }, 10);
          }
      });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 10);

            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

}

