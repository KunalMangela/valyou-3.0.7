package com.globalgyan.getvalyou;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.nineoldandroids.view.ViewHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Parallax transformer for ViewPagers that let you set different parallax 
 * effects for each view in your Fragments.
 * 
 * Created by Marcos Trujillo (#^.^#) on 1/10/14.
 * Recreate by Guihgo :{) on 20/02/16.
 */
public class ParallaxPageTransformer implements ViewPager.PageTransformer {

    private List<ParallaxTransformInformation> mViewsToParallax
            = new ArrayList<ParallaxTransformInformation>();
    
    public ParallaxPageTransformer() {
    }

    public ParallaxPageTransformer(List<ParallaxTransformInformation> viewsToParallax) {
        mViewsToParallax = viewsToParallax;
    }

    public ParallaxPageTransformer addViewToParallax(ParallaxTransformInformation viewInfo) {
        if (mViewsToParallax != null) {
            mViewsToParallax.add(viewInfo);
        }
        return this;
    }

    @Override
    public void transformPage(View view, float position) {
    	
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (position < -1) {
            // This page is way off-screen to the left.
        	ViewHelper.setAlpha(view, 1);

        } else if (position <= 1 && mViewsToParallax != null) { // [-1,1]
        	
            for (ParallaxTransformInformation parallaxTransformInformation : mViewsToParallax) {
                applyParallaxEffect(view, position, pageHeight, pageWidth, parallaxTransformInformation,
                        position > 0);
//                applyParallaxEffect2(view, position, pageWidth, parallaxTransformInformation,
//                        position > 0);
            }

        } else {
            // This page is way off-screen to the right.
        	ViewHelper.setAlpha(view, 1);
        }
    }

    private void applyParallaxEffect(View view, float position, int pageHeight, int pageWidth, ParallaxTransformInformation information, boolean isEnter) {
        if(information.resource == R.id.rocket_intro1) {
            if (information.isValid() && view.findViewById(information.resource) != null) {
                if (isEnter && !information.isEnterDefault()) {
                 //   ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxEnterEffect)));

                    ViewHelper.setTranslationY(view.findViewById(information.resource), (position * (pageHeight / information.parallaxEnterEffect)));
                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxEnterEffect)));

                } else if (!isEnter && !information.isExitDefault()) {
             //       ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxEnterEffect)));

                    ViewHelper.setTranslationY(view.findViewById(information.resource), (position * (pageHeight / information.parallaxExitEffect)));
                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxExitEffect)));

                }
            }
        }
        if(information.resource == R.id.rocket_intro3) {
            if (information.isValid() && view.findViewById(information.resource) != null) {
                if (isEnter && !information.isEnterDefault()) {

                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxEnterEffect)));

                } else if (!isEnter && !information.isExitDefault()) {

                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxExitEffect)));

                }
            }
        }
        if(information.resource == R.id.rocket_intro2) {
            if (information.isValid() && view.findViewById(information.resource) != null) {
                if (isEnter && !information.isEnterDefault()) {

                  //  ViewHelper.setRotation(view.findViewById(information.resource), (position * (pageWidth / information.parallaxEnterEffect)));
                    ViewHelper.setScaleX(view.findViewById(information.resource), (position * (  information.parallaxEnterEffect)));
                  //  ViewHelper.setScaleY(view.findViewById(information.resource), (position * ( information.parallaxEnterEffect)));
                } else if (!isEnter && !information.isExitDefault()) {

                 //   ViewHelper.setRotation(view.findViewById(information.resource), (position * (pageWidth / information.parallaxExitEffect)));
                    ViewHelper.setScaleX(view.findViewById(information.resource), (position * ( information.parallaxExitEffect)));
                //   ViewHelper.setScaleY(view.findViewById(information.resource), (position * (  information.parallaxExitEffect)));
                }
            }
        }
        if(information.resource == R.id.singup_button) {
            if (information.isValid() && view.findViewById(information.resource) != null) {
                if (isEnter && !information.isEnterDefault()) {

                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxEnterEffect)));
                } else if (!isEnter && !information.isExitDefault()) {

                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxExitEffect)));
                }
            }
        }
        if(information.resource == R.id.login_button) {
            if (information.isValid() && view.findViewById(information.resource) != null) {
                if (isEnter && !information.isEnterDefault()) {

                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxEnterEffect)));
                } else if (!isEnter && !information.isExitDefault()) {

                    ViewHelper.setTranslationX(view.findViewById(information.resource), (position * (pageWidth / information.parallaxExitEffect)));
                }
            }
        }
    }



    /**
     * Information to make the parallax effect in a concrete view.
     *
     * parallaxEffect positive values reduces the speed of the view in the translation
     * ParallaxEffect negative values increase the speed of the view in the translation
     * Try values to see the different effects. I recommend 2, 0.75 and 0.5
     */
    public static class ParallaxTransformInformation {

        public static final float PARALLAX_EFFECT_DEFAULT = -101.1986f;

        int resource = -1;
        float parallaxEnterEffect = 2f;
        float parallaxExitEffect = -2f;

        public ParallaxTransformInformation(int resource, float parallaxEnterEffect,
                float parallaxExitEffect) {
            this.resource = resource;
            this.parallaxEnterEffect = parallaxEnterEffect;
            this.parallaxExitEffect = parallaxExitEffect;
        }

        public boolean isValid() {
            return parallaxEnterEffect != 0 && parallaxExitEffect != 0 && resource != -1;
        }

        public boolean isEnterDefault() {
            return parallaxEnterEffect == PARALLAX_EFFECT_DEFAULT;
        }

        public boolean isExitDefault() {
            return parallaxExitEffect == PARALLAX_EFFECT_DEFAULT;
        }
    }
}