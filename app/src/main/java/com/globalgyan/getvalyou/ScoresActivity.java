package com.globalgyan.getvalyou;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.WindowManager;
import android.widget.TextView;

import com.sackcentury.shinebuttonlib.ShineButton;

public class ScoresActivity extends Activity {

    ShineButton s1,s2,s3;
    TextView feedback_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);
        s1 = (ShineButton) findViewById(R.id.first_star);
        s2 = (ShineButton) findViewById(R.id.second_star);
        s3 = (ShineButton) findViewById(R.id.third_star);
        feedback_button=(TextView)findViewById(R.id.feedbcak_button);

        Typeface tf2 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        feedback_button.setTypeface(tf2);
        new CountDownTimer(1500, 1500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                s1.init(ScoresActivity.this);
                s1.setBtnFillColor(Color.parseColor("#F0DA2E"));
                s1.performClick();
                s1.setEnabled(false);
                s1.setClickable(false);
            }
        }.start();
        new CountDownTimer(2500, 1500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                s2.init(ScoresActivity.this);
                s2.setBtnFillColor(Color.parseColor("#F0DA2E"));
                s2.performClick();

                s2.setEnabled(false);
                s2.setClickable(false);

            }
        }.start();
        new CountDownTimer(3500, 1500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                s3.init(ScoresActivity.this);
                s3.performClick();

                s3.setEnabled(false);
                s3.setClickable(false);
            }
        }.start();


    }
}
