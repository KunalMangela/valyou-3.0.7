package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import de.morrox.fontinator.FontTextView;

import static android.content.Context.BATTERY_SERVICE;
import static com.globalgyan.getvalyou.Welcome.appConstant;
import static com.globalgyan.getvalyou.Welcome.batLevel;
import static com.globalgyan.getvalyou.Welcome.isPowerSaveMode;
import static com.globalgyan.getvalyou.Welcome.tf2;


public class IntroOne extends Fragment {
    View v;
    static ObjectAnimator astro1_outx, astro1_outy;

    static boolean flag_anim_done1=false;
    BatteryManager bm;

    static ImageView astro1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         v = inflater.inflate(R.layout.intro_one, container, false);


        setRetainInstance(true);
        astro1 = (ImageView)v.findViewById(R.id.rocket_intro1);

        astro1_outx = ObjectAnimator.ofFloat(astro1, "translationX", -400f,0f);
        astro1_outx.setDuration(300);
        astro1_outx.setInterpolator(new LinearInterpolator());
        astro1_outx.setStartDelay(0);

        astro1_outy = ObjectAnimator.ofFloat(astro1, "translationY", 700f,0f);
        astro1_outy.setDuration(300);
        astro1_outy.setInterpolator(new LinearInterpolator());
        astro1_outy.setStartDelay(0);
        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            if (!flag_anim_done1) {
                astro1_outx.start();
                astro1_outy.start();
            }
            astro1_outx.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    flag_anim_done1 = true;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            astro1_outy.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    flag_anim_done1 = true;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


        }

        return v;
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FontTextView tv = (FontTextView) v.findViewById(R.id.title_text1_intro1);
        setRetainInstance(true);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.otf");


        appConstant = new AppConstant((getActivity()));
        String intro1_title1_string = prefs.getString("intro1_title1_string", "intro1_title1_string");
        String intro1_title2_string = prefs.getString("intro1_title2_string", "intro1_title2_string");


        tv.setText(intro1_title1_string);
        FontTextView tv2 = (FontTextView) v.findViewById(R.id.title_text2_intro1);

        tv2.setText(intro1_title2_string);

        tv.setTypeface(tf2);
        tv2.setTypeface(tf2);

//          astro1 = (ImageView)v.findViewById(R.id.rocket_intro1);

       //  astro1.setVisibility(View.INVISIBLE);

//        astro1_outx = ObjectAnimator.ofFloat(astro1, "translationX", -400f,0f);
//        astro1_outx.setDuration(300);
//        astro1_outx.setInterpolator(new LinearInterpolator());
//        astro1_outx.setStartDelay(0);
//
//        astro1_outy = ObjectAnimator.ofFloat(astro1, "translationY", 700f,0f);
//        astro1_outy.setDuration(300);
//        astro1_outy.setInterpolator(new LinearInterpolator());
//        astro1_outy.setStartDelay(0);
        bm = (BatteryManager)getActivity().getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        Log.e("battery", ""+batLevel);

       /* if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus"))||batLevel==0){
            if (!flag_anim_done1) {
                astro1_outx.start();
                astro1_outy.start();
            }
            astro1_outx.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    flag_anim_done1 = true;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            astro1_outy.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    flag_anim_done1 = true;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


        }*/

    }
    public static IntroOne newInstance(String text) {

        IntroOne f = new IntroOne();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }


}
