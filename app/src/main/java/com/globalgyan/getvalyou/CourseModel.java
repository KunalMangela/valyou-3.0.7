package com.globalgyan.getvalyou;

public class CourseModel {
    String name,desc,hashtags,content,author,status_lock,lg_id,certi_id,certi_name,warn_msg,rating,desc_course,cost,url_ic,course_seen;
    int progress,id,days,isdependent;

    public CourseModel(String name, int progress) {
        this.name = name;
        this.progress = progress;
    }

    public String getCourse_seen() {
        return course_seen;
    }

    public void setCourse_seen(String course_seen) {
        this.course_seen = course_seen;
    }

    public String getDesc_course() {
        return desc_course;
    }

    public String getUrl_ic() {
        return url_ic;
    }

    public void setUrl_ic(String url_ic) {
        this.url_ic = url_ic;
    }

    public void setDesc_course(String desc_course) {
        this.desc_course = desc_course;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getWarn_msg() {
        return warn_msg;
    }

    public void setWarn_msg(String warn_msg) {
        this.warn_msg = warn_msg;
    }

    public String getCerti_name() {
        return certi_name;
    }

    public void setCerti_name(String certi_name) {
        this.certi_name = certi_name;
    }

    public String getCerti_id() {
        return certi_id;
    }

    public void setCerti_id(String certi_id) {
        this.certi_id = certi_id;
    }

    public String getLg_id() {
        return lg_id;
    }

    public void setLg_id(String lg_id) {
        this.lg_id = lg_id;
    }

    public String getStatus_lock() {
        return status_lock;
    }

    public void setStatus_lock(String status_lock) {
        this.status_lock = status_lock;
    }

    public int getIsdependent() {
        return isdependent;
    }

    public void setIsdependent(int isdependent) {
        this.isdependent = isdependent;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getHashtags() {
        return hashtags;
    }

    public void setHashtags(String hashtags) {
        this.hashtags = hashtags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public CourseModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
