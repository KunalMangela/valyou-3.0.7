package com.globalgyan.getvalyou.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.Activity_Myself;
import com.globalgyan.getvalyou.EnterOtpActivity;
import com.globalgyan.getvalyou.ForgotActivity;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.LanguageActivity;
import com.globalgyan.getvalyou.LoginSignupActivity;
import com.globalgyan.getvalyou.MainPlanetActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.SocialsignupActivity;
import com.globalgyan.getvalyou.Welcome;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.response.ForgetPasswordResponse;
import com.globalgyan.getvalyou.cms.response.LoginResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.SocialLoginResponse;
import com.globalgyan.getvalyou.cms.response.SocialRegisterResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Dates;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import butterknife.ButterKnife;
import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;

import static android.app.Activity.RESULT_OK;
import static com.globalgyan.getvalyou.Welcome.mViewPager;
import static com.globalgyan.getvalyou.Welcome.myPagerAdapter;
import static com.thefinestartist.utils.service.ServiceUtil.getSystemService;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements GUICallback,ActivityCompat.OnRequestPermissionsResultCallback {
    private ViewGroup rootLayout;
    FontTextView login_tv, signup_tv, forgotpassis;
    FormEditText email, password, emailS, passwordS, phone, username;
    boolean checkpas = false, checkphone = false;
    String type, currentscreen = "loginscreen";
    ImageView logincard, signupcard,eye;
    boolean ispassvisible = false, ispassvisiblelogin = false;
    //login
    ImageView pass_toggle_signup, pass_toggle_login;
    private static final int RC_SIGN_IN = 007;
    private String providerTypeStr = "";
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    Dialog login_progrss, signup_progress;
    ConnectionUtils connectionUtils;
    //signup
    boolean ispause = false;
    AppConstant appConstant;
    boolean flag_netcheck = false;
    RelativeLayout loginlayout, signuplayout;
    Animation rightanim, leftanim, rightanim2, leftanim2;
    ImageView img_logo;
    Context context;
    buttonClick click;
    View v;
    private SimpleLocation location;

    public interface buttonClick {
        void buttonClicked1(View v);
    }
    public interface buttonClick3 {
        void buttonClicked1(View v);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        click = (buttonClick) activity;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        email = (FormEditText) v.findViewById(R.id.email);
        password = (FormEditText) v.findViewById(R.id.password);
        login_tv=(FontTextView)v.findViewById(R.id.login_tv);
        forgotpassis=(FontTextView)v.findViewById(R.id.forgot_pass_tv);
        eye=(ImageView) v.findViewById(R.id.eye);

        connectionUtils = new ConnectionUtils(getActivity());
        signupcard=(ImageView)v.findViewById(R.id.singup_button);
        location = new SimpleLocation(getActivity());
        ButterKnife.bind(getActivity());

        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.otf");

        email.setTypeface(typeface1);
        password.setTypeface(typeface1);
        login_tv.setTypeface(typeface1);
        forgotpassis.setTypeface(typeface1);

        //progress dialogue
       createProgress();
        forgotpassis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //change param
                forgotpassis.setEnabled(false);
                forgotpassis.setClickable(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        forgotpassis.setEnabled(true);
                        forgotpassis.setClickable(true);
                    }
                }.start();
                Intent forgot = new Intent(getActivity(), ForgotActivity.class);
                forgot.putExtra("SIGNUP","NO");
                startActivity(forgot);
                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });


        signupcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signupcard.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        signupcard.setEnabled(true);
                    }
                }.start();
                click.buttonClicked1(view);

            }
        });
//        login_tv.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                switch (motionEvent.getAction()) {
//                    case MotionEvent.ACTION_DOWN: {
//                        view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
//                        view.invalidate();
//                        break;
//                    }
//                    case MotionEvent.ACTION_UP: {
//                        view.getBackground().clearColorFilter();
//                        view.invalidate();
//                        break;
//                    }
//                }
//                return false;
//            }
//        });
        login_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                type = "login";
                Log.e("email", email.getText().toString());

                Log.e("pass", password.getText().toString());
                login_tv.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        login_tv.setEnabled(true);
                    }
                }.start();

                FormEditText[] allFields = {email, password};


                boolean allValid = true;
                for (FormEditText field : allFields) {
                    allValid = field.testValidity() && allValid;
                }

                if (allValid) {
                    if (password.getText().toString().contains(" ")) {
                        Toast.makeText(getActivity(), "Blank spaces are not allowed in password", Toast.LENGTH_SHORT).show();
                    } else if (password.getText().toString().length() < 5) {
                        Toast.makeText(getActivity(), "Password length must be of minimum 6 characters", Toast.LENGTH_SHORT).show();


                    } else {
                        //do task
                        dologin();
                    }
                    // YAY
                    Log.e("status", "valid");
                } else {
                    // EditText are going to appear with an exclamation mark and an explicative message.
                    Log.e("status", "invalid");

                }
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
           /* if(editable.length()>1){
                if (!ispassvisiblelogin) {
                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visible, 0);
                    password.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisible, 0);
                    password.setTransformationMethod(null);
                }
            }*/
            }
        });





       /* password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    String hint = String.valueOf(password.getHint());
                    if(password.getText().length()>0) {
                        if (event.getRawX() >= (password.getRight())) {

                            float x = event.getX();
                            float y = event.getY();


                            Log.e("touch point raw", ""+event.getRawX());
                            Log.e("touch point<450", ""+x);
                            if (event.getRawX() >750 || (x>320&&x<400)) {
                                Log.e("touch point", ""+event.getRawX());
                                hideKeyboard();
                                if (ispassvisible) {



                                    ispassvisible = false;
                                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visi, 0);
                                    password.setTransformationMethod(new PasswordTransformationMethod());
                                    password.setSelection(password.getText().length());
                                } else {
                                    ispassvisible = true;
                                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisi, 0);
                                    password.setTransformationMethod(null);
                                    password.setSelection(password.getText().length());
                                }
                                // your action here
                                Log.e("pass", "clicked");
                                return true;
                            }
                            else {
                                InputMethodManager imm = (InputMethodManager)
                                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.showSoftInput(password,
                                        InputMethodManager.SHOW_IMPLICIT);
                            }
                        }

                        else{

                            InputMethodManager imm = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(password,
                                    InputMethodManager.SHOW_IMPLICIT);


                               *//*                         InputMethodManager keyboard = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            keyboard.showSoftInput(passwordS, 0);*//*
                        }
                    }
                }else {

                }
                return false;
            }
        });*/

        eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(password.getText().length()>0) {


                            hideKeyboard();
                            if (ispassvisible) {



                                ispassvisible = false;
                                password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visi, 0);
                                password.setTransformationMethod(new PasswordTransformationMethod());
                                password.setSelection(password.getText().length());
                            } else {
                                ispassvisible = true;
                                password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisi, 0);
                                password.setTransformationMethod(null);
                                password.setSelection(password.getText().length());
                            }
                            // your action here
                            Log.e("pass", "clicked");

                        }
                        else {
                            InputMethodManager imm = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(password,
                                    InputMethodManager.SHOW_IMPLICIT);
                        }
                    }





        });
    }



    private void showkeyboard() {




    }

    public void createProgress() {
        login_progrss = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        login_progrss.requestWindowFeature(Window.FEATURE_NO_TITLE);
        login_progrss.setContentView(R.layout.planet_loader);
        Window window = login_progrss.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        login_progrss.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) login_progrss.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        login_progrss.setCancelable(false);
        //login_progrss.show();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       // mViewPager.setBackgroundResource(R.drawable.big_intro_bg);


        v = inflater.inflate(R.layout.fragment_login, container, false);

        new PrefManager(getActivity()).setFirstTimeLaunch(false);

        return v;
    }
    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    private void dologin() {
        hideKeyboard();
        if (connectionUtils.isConnectionAvailable()) {

            login_progrss.show();

            final CheckInternetTask t912 = new CheckInternetTask();
            t912.execute();

            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    t912.cancel(true);

                    if (flag_netcheck) {
                        flag_netcheck = false;
                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(getActivity(),LoginFragment.this);

                        processor.doLogin(email.getText().toString(), password.getText().toString(), FirebaseInstanceId.getInstance().getToken(), location.getLatitude()+","+location.getLongitude(), "ANDROID", getAndroidVersion(), getDevice());
                    } else {
                        login_progrss.dismiss();
                        flag_netcheck = false;
                        Toast.makeText(getActivity(), "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    }
                }
            }.start();

        } else {
            Toast.makeText(getActivity(), "Please check your internet connection !", Toast.LENGTH_SHORT).show();
        }

    }
    public String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.e("RELEASE",release);
        return release;
    }

    public String getDevice() {
        String deviceName = Build.MANUFACTURER + " " + Build.MODEL;

        return deviceName;
    }
    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }
        public boolean isInternetAvailable() {
            ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(type.equalsIgnoreCase("login")){


            Log.e("signin","resp");
            try {
                if (login_progrss.isShowing() && login_progrss != null) {
                    login_progrss.dismiss();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            login_tv.setEnabled(false);
            new CountDownTimer(2000, 2000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    login_tv.setEnabled(true);

                }
            }.start();
            if (guiResponse != null) {
                if (status.equals(RequestStatus.SUCCESS)) {

                    if (guiResponse instanceof LoginResponse) {

                        LoginResponse loginResponse = (LoginResponse) guiResponse;
                        if (loginResponse != null) {
                            try {
                                LoginModel loginModelis = loginResponse.getLoginModel();
                                Log.e("wayis", loginModelis.getAuth_token());
                                new PrefManager(getActivity()).saveAuthToken(loginModelis.getAuth_token());
                            }catch (Exception e){
                                Log.e("error","token inot found");
                            }
                            if (loginResponse.isStatus()) {

                                try {
                                    FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                    FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                                } catch (Exception ex) {

                                }

                                DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
                                dataBaseHelper.deleteAllTables();
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, loginResponse.getLoginModel().getDisplayName());

                                if (loginResponse.getLoginModel() != null && loginResponse.getLoginModel().isProfileComplete())
                                    PreferenceUtils.setProfileDone(getActivity());
                                if (loginResponse.getLoginModel() != null) {
                                    LoginModel loginModel = loginResponse.getLoginModel();
                                    long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                    String dob = loginModel.getDateOfBirth();
                                    if (!TextUtils.isEmpty(dob)) {
                                        try {
                                            //String date = DateUtils.UtcToJavaDateFormat(dob, new SimpleDateFormat("dd-MM-yyyy"));
                                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                        } catch (Exception ex) {

                                        }
                                    }
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                    if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());

                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());

                                    PreferenceUtils.setCandidateId(loginModel.get_id(), getActivity());

                                    List<EducationalDetails> educationalDetailses = loginResponse.getLoginModel().getCurrentEducation();
                                    List<Professional> professionals = loginResponse.getLoginModel().getProfessionalExperience();
                                    if (educationalDetailses != null) {
                                        for (EducationalDetails details : educationalDetailses) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }

                                            details.setFrom(date);
                                            Dates end = details.getEndDate();

                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createEducation(details);
                                        }
                                    }
                                    if (professionals != null) {
                                        for (Professional details : professionals) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }
                                            details.setFrom(date);

                                            Dates end = details.getEndDate();
                                            try {
                                                if (end != null && end.getMonth() < 13) {
                                                    int index = end.getMonth();
                                                    index--;
                                                    date = months[index] + "-" + String.valueOf(end.getYear());
                                                }
                                            } catch (Exception ex) {

                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createProfessionlDetails(details);
                                        }
                                    }

                                    if (loginModel.isMobileVerified()) {
                                        PreferenceUtils.setVerified(getActivity());
                                        Log.e("ibbbp",loginModel.isMobileVerified()+""+loginModel.isProfileComplete());
                                        if (PreferenceUtils.isProfileDone(getActivity())) {
                                            Intent intent = new Intent(getActivity(), MainPlanetActivity.class);

                                            SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                                            prefEditor.putString("login_success", "login_success");
                                            prefEditor.apply();

                                            intent.putExtra("tabs","normal");
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            getActivity().                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                            getActivity().finish();

                                        }else {
                                            Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                            SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                                            prefEditor.putString("login_success", "login_success");
                                            prefEditor.apply();

                                            intent.putExtra("tabs","normal");
                                            intent.putExtra("from","signup");
                                            if (getActivity().getParent() == null) {
                                                getActivity().setResult(RESULT_OK, intent);
                                            } else {
                                                getActivity().getParent().setResult(RESULT_OK, intent);
                                            }
                                            startActivity(intent);
                                            getActivity().                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                            getActivity().finish();
                                        }
                                    } else {

                                        AppConstant.OTP_FROM_SIGNUP = true;
                                        Intent intent = new Intent(getActivity(), EnterOtpActivity.class);
                                        intent.putExtra("phn", loginModel.getMobile());
                                        intent.putExtra("id", loginModel.get_id());
                                        intent.putExtra("session_id", " ");
                                        intent.putExtra("otpf","no");
                                        startActivity(intent);
                                        getActivity().finish();

                                    }


                                }

                            } else {
                                Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else
                            Toast.makeText(getActivity(), "INVALID USER DETAILS", Toast.LENGTH_SHORT).show();
                    } else if (guiResponse instanceof SocialRegisterResponse) {
                        SocialRegisterResponse registerResponse = (SocialRegisterResponse) guiResponse;
                        if (registerResponse != null) {
                            if (registerResponse.isStatus()) {

                                try {
                                    FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                    FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                                } catch (Exception ex) {

                                }

                                DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
                                dataBaseHelper.deleteAllTables();

                                if (registerResponse.getLoginModel() != null && registerResponse.getLoginModel().isProfileComplete())
                                    PreferenceUtils.setProfileDone(getActivity());

                                String displayName = registerResponse.getLoginModel().getDisplayName();
                                if (TextUtils.isEmpty(displayName))
                                    displayName = registerResponse.getLoginModel().getFirstName();
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, displayName);
                                PreferenceUtils.setCandidateId(registerResponse.getLoginModel().get_id(), getActivity());
                                if (registerResponse.getLoginModel() != null) {
                                    LoginModel loginModel = registerResponse.getLoginModel();
                                    long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                    String dob = loginModel.getDateOfBirth();
                                    if (!TextUtils.isEmpty(dob)) {
                                        try {
                                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                        } catch (Exception ex) {

                                        }
                                    }
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());

                                    if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());


                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());


                                    List<EducationalDetails> educationalDetailses = registerResponse.getLoginModel().getCurrentEducation();
                                    List<Professional> professionals = registerResponse.getLoginModel().getProfessionalExperience();
                                    if (educationalDetailses != null) {
                                        for (EducationalDetails details : educationalDetailses) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }

                                            details.setFrom(date);
                                            Dates end = details.getEndDate();

                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createEducation(details);
                                        }
                                    }
                                    if (professionals != null) {
                                        for (Professional details : professionals) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }
                                            details.setFrom(date);

                                            Dates end = details.getEndDate();
                                            try {
                                                if (end != null && end.getMonth() < 13) {
                                                    int index = end.getMonth();
                                                    index--;
                                                    date = months[index] + "-" + String.valueOf(end.getYear());
                                                }
                                            } catch (Exception ex) {

                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createProfessionlDetails(details);
                                        }
                                    }
                                }

                                if (PreferenceUtils.isProfileDone(getActivity())) {
                                    Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                                    intent.putExtra("tabs","normal");

                                    SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                                    prefEditor.putString("login_success", "login_success");
                                    prefEditor.apply();

                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    getActivity().                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                    getActivity().finish();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Username already exists", Toast.LENGTH_SHORT).show();
                            }
                        } else
                            Toast.makeText(getActivity(), "Server issuse", Toast.LENGTH_SHORT).show();


                    } else if (guiResponse instanceof SocialLoginResponse) {
                        SocialLoginResponse loginResponse = (SocialLoginResponse) guiResponse;
                        if (loginResponse != null) {
                            if (loginResponse.isStatus()) {

                                try {
                                    FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                                    FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                                } catch (Exception ex) {

                                }

                                DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
                                dataBaseHelper.deleteAllTables();

                                if (loginResponse.getLoginModel() != null && loginResponse.getLoginModel().isProfileComplete())
                                    PreferenceUtils.setProfileDone(getActivity());


                                String displayName = loginResponse.getLoginModel().getDisplayName();
                                if (TextUtils.isEmpty(displayName))
                                    displayName = loginResponse.getLoginModel().getFirstName();
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME, displayName);
                                PreferenceUtils.setCandidateId(loginResponse.getLoginModel().get_id(), getActivity());

                                if (loginResponse.getLoginModel() != null) {
                                    PreferenceUtils.setVerified(getActivity());
                                    LoginModel loginModel = loginResponse.getLoginModel();
                                    long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);

                                    String dob = loginModel.getDateOfBirth();
                                    if (!TextUtils.isEmpty(dob)) {
                                        try {

                                            dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB, dob);
                                        } catch (Exception ex) {

                                        }
                                    }


                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, loginModel.getGender());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY, loginModel.getIndustry());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, loginModel.getProfileImageURL());
                                    if (loginModel.getVideos() != null && !TextUtils.isEmpty(loginModel.getVideos().getS3valyouinputbucket()))
                                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_VIDEO_URL, loginModel.getVideos().getS3valyouinputbucket());

                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL, loginModel.getEmail());
                                    dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE, loginModel.getMobile());


                                    List<EducationalDetails> educationalDetailses = loginResponse.getLoginModel().getCurrentEducation();
                                    List<Professional> professionals = loginResponse.getLoginModel().getProfessionalExperience();
                                    if (educationalDetailses != null) {
                                        for (EducationalDetails details : educationalDetailses) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }

                                            details.setFrom(date);
                                            Dates end = details.getEndDate();

                                            if (end != null && end.getMonth() < 13) {
                                                int index = end.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(end.getYear());
                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createEducation(details);
                                        }
                                    }
                                    if (professionals != null) {
                                        for (Professional details : professionals) {
                                            String date = "";
                                            Dates start = details.getStartDate();

                                            if (start != null && start.getMonth() < 13) {
                                                int index = start.getMonth();
                                                index--;
                                                date = months[index] + "-" + String.valueOf(start.getYear());
                                            }
                                            details.setFrom(date);

                                            Dates end = details.getEndDate();
                                            try {
                                                if (end != null && end.getMonth() < 13) {
                                                    int index = end.getMonth();
                                                    index--;
                                                    date = months[index] + "-" + String.valueOf(end.getYear());
                                                }
                                            } catch (Exception ex) {

                                            }
                                            details.setTo(date);
                                            dataBaseHelper.createProfessionlDetails(details);
                                        }
                                    }
                                }

                                if (PreferenceUtils.isProfileDone(getActivity())) {
                                    Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                                    intent.putExtra("tabs","normal");

                                    SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                                    prefEditor.putString("login_success", "login_success");
                                    prefEditor.apply();

                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    getActivity().                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


                                    getActivity().finish();
                                }
                            } else {
                                Intent intent = new Intent(getActivity(), SocialsignupActivity.class);
                                intent.putExtra("providertype", providerTypeStr);
                                startActivity(intent);
                                //  finish();
                            }
                        } else
                            Toast.makeText(getActivity(), "Server Pissue", Toast.LENGTH_SHORT).show();


                    }

                } else lodin();
                //Toast.makeText(this, "Server PPissue", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(getActivity(), "NETWORK ISSUE", Toast.LENGTH_SHORT).show();

        }
    }
    private void lodin(){
        Intent intent = new Intent(getActivity(), SocialsignupActivity.class);
        intent.putExtra("type","social");
        intent.putExtra("providertype", providerTypeStr);
        startActivity(intent);
    }

}

