package com.globalgyan.getvalyou.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.VideoPerformanceActivity;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.HomeActivity.comp_flag;
import static com.globalgyan.getvalyou.HomeActivity.mHandler;
import static com.globalgyan.getvalyou.HomeActivity.mTabLayout;

//import com.facebook.shimmer.ShimmerFrameLayout;

public class Performance extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public static Context context;
    boolean flag_i=false;
    FontTextView game_stats,instruction, scoreanim,scorenoanim;
    FrameLayout contributton;
    View view;
    FrameLayout no_anim,valyou_score;
    RippleBackground anim;
    String candidateId;
    static RoundCornerProgressBar bar_r;
    static FontTextView bar_progress;
    static FontTextView uploading_status;
    TextView text_contri,desc_textview,no_contri;
    EditText desc_edittext;
    RelativeLayout main_linear;
    public static Performance newInstance() {

        return new Performance();
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Performance.
     */
    // TODO: Rename and change types and number of parameters
    public static Performance newInstance(String param1, String param2) {
        Performance fragment = new Performance();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=  inflater.inflate(R.layout.fragment_performance,container,false);
        return view;    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        game_stats=(FontTextView)view.findViewById(R.id.game_stats);
        instruction=(FontTextView)view.findViewById(R.id.instructions);
        anim=(RippleBackground)view.findViewById(R.id.frame_anim);
        contributton=(FrameLayout)view.findViewById(R.id.frame_is);
        no_anim=(FrameLayout)view.findViewById(R.id.frame_no_anim);
        scoreanim=(FontTextView)view.findViewById(R.id.points_performance);
        scorenoanim=(FontTextView)view.findViewById(R.id.points_performance_no_anim);
        text_contri=(TextView)view.findViewById(R.id.text_contri);
        no_contri=(TextView)view.findViewById(R.id.no_contri_vaialble_text);
        main_linear=(RelativeLayout)view.findViewById(R.id.main_frame_contri);
        desc_textview=(TextView)view.findViewById(R.id.desc);
        bar_progress=(FontTextView)view.findViewById(R.id.pr_value1);
        uploading_status=(FontTextView)view.findViewById(R.id.uploading_status);
        context=getActivity();
        bar_r=(RoundCornerProgressBar)view.findViewById(R.id.progress_11);
        valyou_score=(FrameLayout)view.findViewById(R.id.value_score);
        desc_edittext=(EditText)view.findViewById(R.id.desc_edit);
        candidateId = PreferenceUtils.getCandidateId(getActivity());
        Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Gotham-Medium.otf");
        text_contri.setTypeface(tf2,Typeface.BOLD);
        desc_textview.setTypeface(tf2);
        desc_edittext.setTypeface(tf2);
        no_contri.setTypeface(tf2,Typeface.BOLD);
       /* new ContributionPermission().execute();
        new GetTotalScore().execute();*/
        if(getDevice().contains("moto")||getDevice().contains("Moto")||getDevice().contains("MOTO")){
            anim.setVisibility(View.GONE);
            no_anim.setVisibility(View.VISIBLE);
        }else {
            no_anim.setVisibility(View.GONE);
            anim.setVisibility(View.VISIBLE);
            anim.startRippleAnimation();
            scoreanim.setText("0");
        }
        /*ShimmerFrameLayout container =
                (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);
        container.startShimmerAnimation();*/
        game_stats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, new Leaderboard());
                fragmentTransaction.addToBackStack("Leaderboard");
                fragmentTransaction.commit();
            }
        });
        instruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, new Instructon());
                fragmentTransaction.addToBackStack("instructon");
                fragmentTransaction.commit();
            }
        });


        contributton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contributton.setEnabled(false);
                contributton.setClickable(false);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        contributton.setEnabled(true);
                        contributton.setClickable(true);

                    }
                }.start();
                if(desc_edittext.getText().length()>1){
                    if (new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")) {

                    }else {
                        HomeActivity.pr=0;
                        new PrefManager(getActivity()).setvideoFrom("contribution");
                        new PrefManager(getActivity()).setDescriptionVideo(desc_edittext.getText().toString());
                        desc_edittext.setText("");
                        mHandler.postDelayed(myTask, 0);
                        Intent intent = new Intent(getActivity(), VideoPerformanceActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("objectId", "contri");
                        startActivity(intent);
                    }
                }else {
                    Toast.makeText(getActivity(), "Please enter video description", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }




    public String getDevice() {
        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

        return deviceName;
    }

    public class GetTotalScore extends AsyncTask<String ,String,String > {


        //ProgressDialog progressDialog= new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection httpURLConnection=null;
            String result="",responcestring="";

            try {
                url=new URL(AppConstant.Ip_url+"reports/get_valyou_score/"+candidateId);
                httpURLConnection=(HttpURLConnection)url.openConnection();

                responcestring=readStream(httpURLConnection.getInputStream());
                result=responcestring;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(httpURLConnection!=null){
                    httpURLConnection.disconnect();
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            Log.e("GetScore json", s);
            try {
                JSONObject jsonObject=new JSONObject(s);
                if(jsonObject.getString("valyou_score").length()>4){
                   scoreanim.setTextSize(17);
                    scorenoanim.setTextSize(17);
                }
                scorenoanim.setText(jsonObject.getString("valyou_score"));
                scoreanim.setText(jsonObject.getString("valyou_score"));

            } catch (JSONException e) {
                e.printStackTrace();
            }



        }
    }


  /*  public void getScore(){
        new GetTotalScore().execute();
    }
*/


    private class ContributionPermission extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";

            try {
                url = new URL(AppConstant.Ip_url+"GameSettings");
                ref_id = "U_id="+candidateId;
                Log.v("URL", "URL: " + url);
                Log.v("postParameters",ref_id);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Authorization", "Bearer " + HomeActivity.token);

                /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();
     /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/


                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.v("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        main_linear.setVisibility(View.GONE);
                        valyou_score.setVisibility(View.VISIBLE);
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            try {
                JSONObject js=new JSONObject(s);
                if(js.getString("Contribution").equalsIgnoreCase("1")){

                    main_linear.setVisibility(View.VISIBLE);
                   valyou_score.setVisibility(View.GONE);
                   startbarProgress();
                 /*  bar_progress.setVisibility(View.GONE);
                   bar_r.setVisibility(View.GONE);
                    uploading_status.setVisibility(View.GONE);*/
                }else {

                    main_linear.setVisibility(View.GONE);
                  valyou_score.setVisibility(View.VISIBLE);

                }
            } catch (JSONException e) {
                e.printStackTrace();
                main_linear.setVisibility(View.GONE);
                valyou_score.setVisibility(View.VISIBLE);
            }


        }

    }

    private void startbarProgress() {
      /*  bar_r.setProgressBackgroundColor(Color.parseColor("#000e36"));
        bar_r.setProgressColor(Color.parseColor("#ffffff"));*/

       /* mHandler.postDelayed(myTask, 0);*/

    }

    public static Runnable myTask = new Runnable() {
        public void run() {
         /*   if(new PrefManager(context).getvideoFrom().equalsIgnoreCase("contribution")) {

                if (HomeActivity.pr > 1) {

                    bar_progress.setVisibility(View.VISIBLE);
                    bar_r.setVisibility(View.VISIBLE);
                    uploading_status.setVisibility(View.GONE);
                }

                bar_r.setProgress(HomeActivity.pr);
                bar_progress.setText(HomeActivity.pr+"%");
                mHandler.postDelayed(this, 10); // Running this thread again after 5000 milliseconds        }


            }else {

            }*/

        }
    };

    public static void inVisiblebars(){

      /*  bar_progress.setVisibility(View.GONE);
        bar_r.setVisibility(View.GONE);
        uploading_status.setVisibility(View.GONE);*/
    }
    public static void visiblebars(){

     /*   bar_progress.setVisibility(View.VISIBLE);
        bar_r.setVisibility(View.VISIBLE);
        uploading_status.setVisibility(View.GONE);*/
    }
    public String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

}
