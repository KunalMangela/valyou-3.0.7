package com.globalgyan.getvalyou.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.AboutUsActivity;

import com.globalgyan.getvalyou.MainPlanetActivity;
import com.globalgyan.getvalyou.MasterSlaveGame.MSQ;
import com.globalgyan.getvalyou.ProfileActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.Report;
import com.globalgyan.getvalyou.TermsConditionActivity;
import com.globalgyan.getvalyou.Welcome;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.response.LogOutResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.interfaces.LogoutHandler;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.Context.BATTERY_SERVICE;
import static com.globalgyan.getvalyou.HomeActivity.appConstant;
import static com.globalgyan.getvalyou.HomeActivity.batLevel;
import static com.globalgyan.getvalyou.HomeActivity.isPowerSaveMode;
import static com.globalgyan.getvalyou.apphelper.BatteryReceiver.flag_play_anim;
import static com.thefinestartist.utils.content.ContextUtil.getFilesDir;

//import com.facebook.AccessToken;
//import com.facebook.login.LoginManager;

/**
 * Created by NaNi on 18/09/17.
 */

public class MoreFragment extends android.support.v4.app.Fragment implements GUICallback{
    CardView p,st,su,ru,tc,lo;
    ConnectionUtils connectionUtils;
    Dialog progress,progress_profile;
    private LogoutHandler logoutHandler = null;
    boolean flag_netcheck=false;
    LinearLayout linear_main;

   Dialog alertDialog;

   TextView textmore;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet,close_btn;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2, astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;
    ImageView imageView;
    View dialogView;
    BatteryReceiver mBatInfoReceiver;

    public static MoreFragment newInstance() {
        return new MoreFragment();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=  inflater.inflate(R.layout.frag_more,container,false);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        logoutHandler =(LogoutHandler)getActivity();
        connectionUtils=new ConnectionUtils(getActivity());


        Log.e("battery", ""+batLevel);

        getActivity().registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        p=(CardView)getActivity().findViewById(R.id.profile);
        st=(CardView)getActivity().findViewById(R.id.settings);
        su=(CardView)getActivity().findViewById(R.id.support);
        ru=(CardView)getActivity().findViewById(R.id.rateus);
        tc=(CardView)getActivity().findViewById(R.id.tac);
        lo=(CardView)getActivity().findViewById(R.id.logout);
        textmore=(TextView)getActivity().findViewById(R.id.textmore);
        red_planet = (ImageView)getActivity().findViewById(R.id.red_planet);
        yellow_planet = (ImageView)getActivity().findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)getActivity().findViewById(R.id.purple_planet);
        earth = (ImageView)getActivity().findViewById(R.id.earth);
        orange_planet = (ImageView)getActivity().findViewById(R.id.orange_planet);
        close_btn=(ImageView)getActivity().findViewById(R.id.close_btn);

        LayoutInflater inflater = LayoutInflater.from(getActivity());

//         dialogView = inflater.inflate(R.layout.planet_loader, null);
//         imageView = (ImageView) getActivity().findViewById(R.id.loader);
//
//        imageView.setImageResource(R.mipmap.ic_launcher);
//
//        progress=  KProgressHUD.create(getActivity())
//                .setCustomView(imageView)
//                .setDimAmount(0.7f)
//                .setCancellable(false);
        createprogress();
        createProgressnormal();
        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.otf");

        textmore.setTypeface(typeface1);
        textmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                getActivity().finish();
            }
        });
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                getActivity().finish();
            }
        });
        linear_main=(LinearLayout)getActivity().findViewById(R.id.linear_main);
        Animation animation_fade1 =
                AnimationUtils.loadAnimation(getActivity(),
                        R.anim.fadeing_out_animation_planet);
      //  linear_main.setVisibility(View.VISIBLE);

      /*  linear_main.startAnimation(animation_fade1);
        new CountDownTimer(1000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                linear_main.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {

            }
        }.start();*/
        p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setEnabled(false);
                st.setEnabled(false);
                su.setEnabled(false);
                ru.setEnabled(false);
                tc.setEnabled(false);
                lo.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        p.setEnabled(true);
                        st.setEnabled(true);
                        su.setEnabled(true);
                        ru.setEnabled(true);
                        tc.setEnabled(true);
                        lo.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){
                    if(new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")){
                        Toast.makeText(getActivity(),"Video uploading is in progrss,please try after uploading get over", Toast.LENGTH_LONG).show();

                    }else {
                        Intent intent = new Intent(getActivity(), ProfileActivity.class);
                        intent.putExtra("nav_to","more");
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    }
                }else {
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

        st.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setEnabled(false);
                st.setEnabled(false);
                su.setEnabled(false);
                ru.setEnabled(false);
                tc.setEnabled(false);
                lo.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        p.setEnabled(true);
                        st.setEnabled(true);
                        su.setEnabled(true);
                        ru.setEnabled(true);
                        tc.setEnabled(true);
                        lo.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {
                    if (new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")) {
                        Toast.makeText(getActivity(), "Video uploading is in progrss,please try after uploading get over", Toast.LENGTH_LONG).show();

                    } else {
                        Intent intent = new Intent(getActivity(), Report.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                }else {
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });

        su.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setEnabled(false);
                st.setEnabled(false);
                su.setEnabled(false);
                ru.setEnabled(false);
                tc.setEnabled(false);
                lo.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        p.setEnabled(true);
                        st.setEnabled(true);
                        su.setEnabled(true);
                        ru.setEnabled(true);
                        tc.setEnabled(true);
                        lo.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {
                    if (new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")) {
                        Toast.makeText(getActivity(), "Video uploading is in progrss,please try after uploading get over", Toast.LENGTH_LONG).show();

                    } else {
                        Intent intent = new Intent(getActivity(), AboutUsActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                }else {
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });

        ru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setEnabled(false);
                st.setEnabled(false);
                su.setEnabled(false);
                ru.setEnabled(false);
                tc.setEnabled(false);
                lo.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        p.setEnabled(true);
                        st.setEnabled(true);
                        su.setEnabled(true);
                        ru.setEnabled(true);
                        tc.setEnabled(true);
                        lo.setEnabled(true);                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {
                    final String appPackageName = getActivity().getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }else {
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });

        tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setEnabled(false);
                st.setEnabled(false);
                su.setEnabled(false);
                ru.setEnabled(false);
                tc.setEnabled(false);
                lo.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        p.setEnabled(true);
                        st.setEnabled(true);
                        su.setEnabled(true);
                        ru.setEnabled(true);
                        tc.setEnabled(true);
                        lo.setEnabled(true);                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {
                    if (new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")) {
                        Toast.makeText(getActivity(), "Video uploading is in progrss,please try after uploading get over", Toast.LENGTH_LONG).show();

                    } else {
                        Intent intent = new Intent(getActivity(), TermsConditionActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                }else {
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });

        lo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setEnabled(false);
                st.setEnabled(false);
                su.setEnabled(false);
                ru.setEnabled(false);
                tc.setEnabled(false);
                lo.setEnabled(false);

                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        p.setEnabled(true);
                        st.setEnabled(true);
                        su.setEnabled(true);
                        ru.setEnabled(true);
                        tc.setEnabled(true);
                        lo.setEnabled(true);                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {
                    if (new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")) {
                        Toast.makeText(getActivity(), "Video uploading is in progrss,Can't do logout right now", Toast.LENGTH_LONG).show();

                    } else {
                        if (connectionUtils.isConnectionAvailable()) {
//                            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
//                                    getActivity(), R.style.DialogTheme);
//                            LayoutInflater inflater = LayoutInflater.from(getActivity());
//                            View dialogView = inflater.inflate(R.layout.do_you_want_to_logout, null);
//
//                            dialogBuilder.setView(dialogView);
//
//
//                            alertDialog = dialogBuilder.create();
//
//
////                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
//
//                            alertDialog.setCancelable(false);
//                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//
//                            alertDialog.show();
                            alertDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
                            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            alertDialog.setContentView(R.layout.do_you_want_to_logout);
                            Window window = alertDialog.getWindow();
                            WindowManager.LayoutParams wlp = window.getAttributes();

                            wlp.gravity = Gravity.CENTER;
                            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                            window.setAttributes(wlp);
                            alertDialog.setCancelable(false);

                            alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                            final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
                            final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                                astro_outx.setDuration(300);
                                astro_outx.setInterpolator(new LinearInterpolator());
                                astro_outx.setStartDelay(0);
                                astro_outx.start();
                                astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                                astro_outy.setDuration(300);
                                astro_outy.setInterpolator(new LinearInterpolator());
                                astro_outy.setStartDelay(0);
                                astro_outy.start();



                                alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                                alien_outx.setDuration(300);
                                alien_outx.setInterpolator(new LinearInterpolator());
                                alien_outx.setStartDelay(0);
                                alien_outx.start();
                                alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                                alien_outy.setDuration(300);
                                alien_outy.setInterpolator(new LinearInterpolator());
                                alien_outy.setStartDelay(0);
                                alien_outy.start();
                            }
                            final ImageView no = (ImageView)alertDialog.findViewById(R.id.no_quit);
                            final ImageView yes = (ImageView)alertDialog.findViewById(R.id.yes_quit);

                            no.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {



                                    no.setEnabled(false);
                                    yes.setEnabled(false);

                                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                                        astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                        astro_inx.setDuration(300);
                                        astro_inx.setInterpolator(new LinearInterpolator());
                                        astro_inx.setStartDelay(0);
                                        astro_inx.start();
                                        astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                        astro_iny.setDuration(300);
                                        astro_iny.setInterpolator(new LinearInterpolator());
                                        astro_iny.setStartDelay(0);
                                        astro_iny.start();

                                        alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                        alien_inx.setDuration(300);
                                        alien_inx.setInterpolator(new LinearInterpolator());
                                        alien_inx.setStartDelay(0);
                                        alien_inx.start();
                                        alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                        alien_iny.setDuration(300);
                                        alien_iny.setInterpolator(new LinearInterpolator());
                                        alien_iny.setStartDelay(0);
                                        alien_iny.start();
                                    }
                                    new CountDownTimer(400,400) {


                                        @Override
                                        public void onTick(long l) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            alertDialog.dismiss();
                                        }
                                    }.start();




                                }
                            });

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View view) {
                                    if(connectionUtils.isConnectionAvailable()){

                                        yes.setEnabled(false);
                                        no.setEnabled(false);

                                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                                            astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                            astro_inx.setDuration(300);
                                            astro_inx.setInterpolator(new LinearInterpolator());
                                            astro_inx.setStartDelay(0);
                                            astro_inx.start();
                                            astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                            astro_iny.setDuration(300);
                                            astro_iny.setInterpolator(new LinearInterpolator());
                                            astro_iny.setStartDelay(0);
                                            astro_iny.start();

                                            alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                            alien_inx.setDuration(300);
                                            alien_inx.setInterpolator(new LinearInterpolator());
                                            alien_inx.setStartDelay(0);
                                            alien_inx.start();
                                            alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                            alien_iny.setDuration(300);
                                            alien_iny.setInterpolator(new LinearInterpolator());
                                            alien_iny.setStartDelay(0);
                                            alien_iny.start();

                                        }
                                        new CountDownTimer(400,400) {


                                            @Override
                                            public void onTick(long l) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                alertDialog.dismiss();
                                                progress.show();
//                                                imageView.setVisibility(View.VISIBLE);
//                                                GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
//                                                Glide.with(getActivity()).load(R.raw.planet_loader).into(imageViewTarget);
                                            }
                                        }.start();

                                        final CheckInternetTask t914=new CheckInternetTask();
                                        t914.execute();

                                        new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {

                                            }

                                            @Override
                                            public void onFinish() {

                                                t914.cancel(true);

                                                if (flag_netcheck) {
                                                    flag_netcheck = false;
                                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(getActivity(), MoreFragment.this);
                                                    processor.doLogOut(PreferenceUtils.getCandidateId(getActivity()), PreferenceUtils.getFcmToken(getActivity()));

                                                }else {
                                                    progress.dismiss();
                                                    flag_netcheck=false;
                                                    Toast.makeText(getActivity(), "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }.start();
                                    }else {
                                        Toast.makeText(getActivity(), "Please check your internet connection !", Toast.LENGTH_SHORT).show();

                                    }


                                }
                            });

                            try{
                                alertDialog.show();
                            }catch (Exception e){

                            }



                        } else {
                            Toast.makeText(getActivity(), "Please check your internet connection !", Toast.LENGTH_SHORT).show();

                        }

                    }

                }else {
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();

                }


            }


        });


        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            startanimation();
        }

        else {

        }

    }

    public void createProgressnormal() {
        progress_profile = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        progress_profile.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress_profile.setContentView(R.layout.planet_loader);
        Window window = progress_profile.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progress_profile.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progress_profile.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progress_profile.setCancelable(false);
        //progress_profile.show();

    }

    private void createprogress() {
        progress = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.setContentView(R.layout.planet_loader);
        Window window = progress.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progress.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progress.setCancelable(false);
        //progress.show();

    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        try {

            if (progress.isShowing() && progress != null) {
                progress.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if (status.equals(RequestStatus.SUCCESS)) {

            if (guiResponse instanceof LogOutResponse) {
                LogOutResponse response = (LogOutResponse) guiResponse;
                if (response != null && response.isStatus()) {
                    try {

                        PreferenceUtils.clearAll(getActivity());
                        FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                        FileUtils.deleteFileFromSdCard(AppConstant.stillFile);

                    } catch (Exception ex) {

                    }

                    DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
                    dataBaseHelper.deleteAllTables();
//                    logoutHandler.onLoggedOut();
//                    try {
//                        if (AccessToken.getCurrentAccessToken() != null) {
//                            LoginManager.getInstance().logOut();
//                        }
//                    } catch (Exception ex) {
//
//                    }

                    try {
                        File folder = new File(/*Environment.getExternalStorageDirectory()*/getFilesDir() +
                                File.separator + "SimulationData");
                        if (folder.exists()) {
                            deleteDirectory(folder);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        File folder_outside = new File(Environment.getExternalStorageDirectory() +
                                File.separator + "SimulationData");
                        if (folder_outside.exists()) {
                            deleteDirectory(folder_outside);

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                    try{
                        NotificationManager notificationManager =
                                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancelAll();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    new PrefManager(getActivity()).saveemailverificationString("notverified");
                    new PrefManager(getActivity()).savePic(" ");
                    PrefManager prefManager = new PrefManager(getActivity());
                    prefManager.clearPref();
                    prefManager.setFirstTimeLaunch(false);

                    SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                    prefEditor.remove("login_success");
                    prefEditor.apply();
                    //do changes
                    Intent intent = new Intent(getActivity(), Welcome.class);
                    intent.putExtra("gotologinpage","loginpage");
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    getActivity().finishAffinity();

                } else {
                    Toast.makeText(getActivity(), "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }

            } else
                Toast.makeText(getActivity(), "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }


    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }
    public static void deleteDirectory( File dir )
    {

        if ( dir.isDirectory() )
        {
            String [] children = dir.list();
            for ( int i = 0 ; i < children.length ; i ++ )
            {
                File child =    new File( dir , children[i] );
                if(child.isDirectory()){
                    deleteDirectory( child );
                    child.delete();
                }else{
                    child.delete();

                }
            }
            dir.delete();
        }
    }
    public void startanimation(){

        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }
}
