package com.globalgyan.getvalyou.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.globalgyan.getvalyou.AnswerAdapter;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.MainPlanetActivity;
import com.globalgyan.getvalyou.NotificationAdapter;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.SwipeController;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.model.NotificationListModel;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.HomeActivity.appConstant;
import static com.globalgyan.getvalyou.HomeActivity.batLevel;
import static com.globalgyan.getvalyou.HomeActivity.isPowerSaveMode;

//import com.dinuscxj.refresh.RecyclerRefreshLayout;

/**
 * Created by NaNi on 18/09/17.
 */

public class NotificationFragment extends android.support.v4.app.Fragment implements HomeActivity.DataReceivedListener {

    DefaultHttpClient httpClient;
    HttpURLConnection urlConnection;
    HttpPost httpPost;
    HttpGet httpGet;
    String candidateId;
    Boolean firstlaunch=false;
    FontTextView timerrr, dqus,qno,ntf_unread_msgs;
    int cqa=0;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null;
    JSONArray questions;
    static String json = "",jso="",Qus="",token="",token_type="", quesDis="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int score=0;
    ArrayList<String> optioncard;
    AppCompatButton ques,options;
    FrameLayout ansDis;
    Dialog dialog;
    ImageButton pause;
    AnswerAdapter answerAdapter;
    ImageView up,down;
    int pauseCount=0,qc=0,qid=0,prevcount;
    public int currentcount=0;

    List<NotificationListModel> intNOID=new ArrayList<>();
    NotificationAdapter genad;
    RelativeLayout clear_relative;
    private static View view;
    public static RecyclerView recyclerGen;
    MaterialRefreshLayout swipeRefreshLayout;
    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }
    IntentFilter intentFilter;
    SwipeController swipeController = null;
    ImageView backbtnis_ntf;
    TextView textnotifications;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_noti, container, false);
         candidateId = PreferenceUtils.getCandidateId(getActivity());
        ((HomeActivity) getActivity()).setDataReceivedListener(this);
        Log.e("KIDD","ME");
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        textnotifications=(TextView)view.findViewById(R.id.textnotifications);
        recyclerGen = (RecyclerView) view.findViewById(R.id.internoti);
        recyclerGen.setLayoutManager(new LinearLayoutManager(getActivity()));
        clear_relative=(RelativeLayout)view.findViewById(R.id.no_list_bg);
        up=(ImageView)view.findViewById(R.id.up);
        down=(ImageView)view.findViewById(R.id.down);

        red_planet = (ImageView)getActivity().findViewById(R.id.red_planet);
        yellow_planet = (ImageView)getActivity().findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)getActivity().findViewById(R.id.purple_planet);
        earth = (ImageView)getActivity().findViewById(R.id.earth);
        orange_planet = (ImageView)getActivity().findViewById(R.id.orange_planet);


        backbtnis_ntf=(ImageView)view.findViewById(R.id.backbtnis_ntf);
          swipeRefreshLayout=(MaterialRefreshLayout) view.findViewById(R.id.swipe);
          ntf_unread_msgs=(FontTextView)view.findViewById(R.id.unreadmsg);
         // int[] colors={Color.BLACK,Color.BLACK/*Integer.parseInt("#00000000"),Integer.parseInt("#ff000000")*/};
         // swipeRefreshLayout.setProgressColors(colors);
        Log.e("KIDD","ME");
        intentFilter = new IntentFilter("refresh List");
        getActivity().registerReceiver(mReceiver, intentFilter);
        candidateId = PreferenceUtils.getCandidateId(getActivity());
        recyclerGen.addItemDecoration(new VerticalSpaceItemDecoration(100));
        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.otf");

        textnotifications.setTypeface(typeface1);
        textnotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(HomeActivity.apiloading){

                }else {
                    Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    getActivity().finish();
                }
            }
        });

        backbtnis_ntf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(HomeActivity.apiloading){

                }else {
                    Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    getActivity().finish();
                }
            }
        });
       // swipeRefreshLayout.setClickable(false);
       // swipeRefreshLayout.setDrawingCacheBackgroundColor(Color.parseColor("#ffffff"));
       /* swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                genad.removeItem(position);
                genad.notifyItemRemoved(position);
                genad.notifyItemRangeChanged(position, genad.getItemCount());
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerGen);

        recyclerGen.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });*/


        recyclerGen.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (firstVisibleItemPosition==0){
                    down.setVisibility(View.VISIBLE);
                    up.setVisibility(View.GONE);
                } else {
                    up.setVisibility(View.VISIBLE);
                    down.setVisibility(View.GONE);
                }
                if(intNOID.size()>0){

                }else {
                    up.setVisibility(View.GONE);
                    down.setVisibility(View.GONE);
                }
                new PrefManager(getActivity()).saventfcount(0);
                currentcount=new PrefManager(getActivity()).getntfcount();
                if(currentcount==0){
                    HomeActivity.badge.setVisibility(View.INVISIBLE);

                }else {
                    HomeActivity.badge.setText(currentcount+"");
                }

               /* if(intNOID.size()>5){
                    up.setVisibility(View.VISIBLE);
                    down.setVisibility(View.VISIBLE);
                }else {
                    up.setVisibility(View.GONE);
                    down.setVisibility(View.GONE);
                }*/
                Log.e("Logis","scrollstateF"+String.valueOf(firstVisibleItemPosition));
                Log.e("Logis","scrollstateL"+String.valueOf(lastVisibleItemPosition));
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerGen.smoothScrollToPosition(0);
               // recyclerGen.getLayoutManager().scrollToPosition(0);
                up.setVisibility(View.GONE);
                down.setVisibility(View.VISIBLE);
            }
        });
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerGen.smoothScrollToPosition(genad.getItemCount()-1);
                //recyclerGen.getLayoutManager().scrollToPosition(genad.getItemCount()-1);
                up.setVisibility(View.VISIBLE);
                down.setVisibility(View.GONE);
            }
        });
        if(HomeActivity.notf) {
            new GetContacts().execute();
        }
        if(new PrefManager(getActivity()).getntfFirstlaunch().equalsIgnoreCase("yes")){

        }else {

        }
        swipeRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override

            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                                                                 //refreshing...
                ConnectionUtils connectionUtils=new ConnectionUtils(getActivity());
                if(connectionUtils.isConnectionAvailable()){
                    if(new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")){
                        Toast.makeText(getActivity(),"Video uploading is in progrss,Can't access latest notifications now",Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.finishRefresh();
                    }else {
                        intNOID.clear();
                        new PrefManager(getActivity()).saventfcount(0);
                        new GetContacts().execute();
                       /* int positionView = ((LinearLayoutManager)recyclerGen.getLayoutManager()).findFirstVisibleItemPosition();
                        Log.e("pos",String.valueOf(positionView));
                        if(positionView==0) {
                            swipeRefreshLayout.setEnabled(true);


                        }else {

                        }
*/
                    }
                }else {
                    swipeRefreshLayout.finishRefresh();
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }

            }

            @Override

            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                                                                 //load more refreshing...

            }
        });
       /* swipeRefreshLayout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ConnectionUtils connectionUtils=new ConnectionUtils(getActivity());
                if(connectionUtils.isConnectionAvailable()){
                    if(new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")){
                        Toast.makeText(getActivity(),"Video uploading is in progrss,Can't access latest notifications now",Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }else {
                        intNOID.clear();
                        new GetContacts().execute();
                    }
                }else {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
*/       /* swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ConnectionUtils connectionUtils=new ConnectionUtils(getActivity());
                if(connectionUtils.isConnectionAvailable()){
                    if(new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")){
                        Toast.makeText(getActivity(),"Video uploading is in progrss,Can't access latest notifications now",Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }else {
                        intNOID.clear();
                        new GetContacts().execute();
                    }
                }else {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });
*/
        genad= new NotificationAdapter(getActivity(),intNOID);


        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            //  red planet animation
            red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f, 700f);
            red_planet_anim_1.setDuration(130000);
            red_planet_anim_1.setInterpolator(new LinearInterpolator());
            red_planet_anim_1.setStartDelay(0);
            red_planet_anim_1.start();

            red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700, 0f);
            red_planet_anim_2.setDuration(130000);
            red_planet_anim_2.setInterpolator(new LinearInterpolator());
            red_planet_anim_2.setStartDelay(0);

            red_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            red_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  yellow planet animation
            yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f, 1100f);
            yellow_planet_anim_1.setDuration(220000);
            yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_1.setStartDelay(0);
            yellow_planet_anim_1.start();

            yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f, 0f);
            yellow_planet_anim_2.setDuration(22000);
            yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_2.setStartDelay(0);

            yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  purple planet animation
            purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f, 100f);
            purple_planet_anim_1.setDuration(9500);
            purple_planet_anim_1.setInterpolator(new LinearInterpolator());
            purple_planet_anim_1.setStartDelay(0);
            purple_planet_anim_1.start();

            purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f, 0f);
            purple_planet_anim_2.setDuration(100000);
            purple_planet_anim_2.setInterpolator(new LinearInterpolator());
            purple_planet_anim_2.setStartDelay(0);


            purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            //  earth animation
            earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f, 1150f);
            earth_anim_1.setDuration(90000);
            earth_anim_1.setInterpolator(new LinearInterpolator());
            earth_anim_1.setStartDelay(0);
            earth_anim_1.start();

            earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f, 0f);
            earth_anim_2.setDuration(55000);
            earth_anim_2.setInterpolator(new LinearInterpolator());
            earth_anim_2.setStartDelay(0);

            earth_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            earth_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  orange planet animation
            orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f, 300f);
            orange_planet_anim_1.setDuration(56000);
            orange_planet_anim_1.setInterpolator(new LinearInterpolator());
            orange_planet_anim_1.setStartDelay(0);
            orange_planet_anim_1.start();

            orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f, 0f);
            orange_planet_anim_2.setDuration(75000);
            orange_planet_anim_2.setInterpolator(new LinearInterpolator());
            orange_planet_anim_2.setStartDelay(0);

            orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


        }


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.e("KIDD","ME");
        candidateId = PreferenceUtils.getCandidateId(getActivity());
//        Log.e("KIDD",candidateId);

    }

    @Override
    public void onReceived() {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    genad.notifyDataSetChanged();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void loadlist() {

    }


    public void getNtf(){
        new GetContacts().execute();
    }
    public class GetContacts extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            Log.e("androidversion","notificationpre");
            HomeActivity.apiloading=true;
            super.onPreExecute();
            new PrefManager(getActivity()).saventfFirstlaunch("yes");

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{
              //  httpClient = new DefaultHttpClient();
                String url= AppConstant.BASE_URL+"candidateMobileAppGetCandidateDetails/"+candidateId;
               // httpGet = new HttpGet(url);


               // httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
               // httpGet.setEntity(new UrlEncodedFormEntity(params));
               // HttpResponse httpResponse = httpClient.execute(httpGet);
               // HttpEntity httpEntity = httpResponse.getEntity();
               // is = httpEntity.getContent();

                URL obj = new URL(url);

                urlConnection = (HttpURLConnection) obj.openConnection();


                is = urlConnection.getInputStream();


            } catch (UnsupportedEncodingException e) {
                HomeActivity.apiloading=false;
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                HomeActivity.apiloading=false;
                e.printStackTrace();
            } catch (IOException e) {
                HomeActivity.apiloading=false;
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();


                Log.e("androidversion", json);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            try {
                jObj = new JSONObject(json);

                intNOID.clear();
                JSONArray data= (JSONArray)jObj.get("notificationList");
                for(int i=0;i<data.length();i++){
                    JSONObject dataJSONobject = data.getJSONObject(i);
                    NotificationListModel notificationListModel=new NotificationListModel();
                    if(dataJSONobject.getString("notificationType").equalsIgnoreCase("Video Response")){
                        notificationListModel.setObj_id(dataJSONobject.getString("objectId"));
                        notificationListModel.setType(dataJSONobject.getString("notificationType"));
                        notificationListModel.setId(dataJSONobject.getString("_id"));
                        notificationListModel.setCreated_at(dataJSONobject.getString("createdAt"));
                        notificationListModel.setMessage(dataJSONobject.getString("message"));
                        notificationListModel.setStatus(dataJSONobject.getString("status"));
                        intNOID.add(notificationListModel);

                    }

                }


               // adapterint = new IntAdapter(getActivity(), intN,listOfnotification);


            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("androidversion","notificationpost");
            HomeActivity.apiloading=false;
            try {
                swipeRefreshLayout.finishRefresh();
            }catch (Exception e){
                e.printStackTrace();
            }
          //  firstlaunch=true;
            if(intNOID.size()==0){
                try{
                    genad.notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }
               // recyclerGen.setVisibility(View.GONE);
                swipeRefreshLayout.setEnabled(true);
                up.setVisibility(View.GONE);
                down.setVisibility(View.GONE);
                ntf_unread_msgs.setText("No Unread");
                //HomeActivity.badge.setVisibility(View.GONE);
                if(firstlaunch){
                    HomeActivity.badge.setVisibility(View.INVISIBLE);
                }else {
                    HomeActivity.badge.setText("!");

                }
                clear_relative.setVisibility(View.VISIBLE);

            }else{
                Animation animation_fade1 =
                        AnimationUtils.loadAnimation(getActivity(),
                                R.anim.fadeing_out_animation_planet);
                recyclerGen.setVisibility(View.VISIBLE);

               /* recyclerGen.startAnimation(animation_fade1);
                new CountDownTimer(200, 50) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        recyclerGen.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFinish() {

                    }
                }.start();
*/
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                //HomeActivity.badge.setVisibility(View.VISIBLE);
                try {
                    currentcount = new PrefManager(getActivity()).getntfcount();
                }catch (Exception e){
                    e.printStackTrace();
                    currentcount=0;
                }
                if(currentcount==0){
                    if(firstlaunch){
                        HomeActivity.badge.setVisibility(View.INVISIBLE);
                    }else {
                        HomeActivity.badge.setText("!");

                    }
                }else {

                   // HomeActivity.badge.setVisibility(View.VISIBLE);

                    HomeActivity.badge.setText(currentcount+"");
                }


                up.setVisibility(View.GONE);
                down.setVisibility(View.VISIBLE);
                recyclerGen.setAdapter(genad);
                ntf_unread_msgs.setText(intNOID.size()+" Unread");
                clear_relative.setVisibility(View.GONE);
                genad.notifyDataSetChanged();

                if(intNOID.size()>5){
                    up.setVisibility(View.VISIBLE);
                    down.setVisibility(View.VISIBLE);
                }else {
                    up.setVisibility(View.GONE);
                    down.setVisibility(View.GONE);
                }
            }



          /*  if(intNOID.size()>6){
                down.setVisibility(View.VISIBLE);
                up.setVisibility(View.VISIBLE);
            }else {

                down.setVisibility(View.INVISIBLE);
                up.setVisibility(View.INVISIBLE);
            }*/
            if(!firstlaunch){
                firstlaunch=true;
            }
        }
    }

    private final BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v("receive broadcast","receive");
            new GetContacts().execute();
            try{
                prevcount=new PrefManager(context).getntfcount();

            }catch (Exception e){
                prevcount=0;
            }
            currentcount=prevcount+1;
            HomeActivity.badge.setText(currentcount+"");
            new PrefManager(context).saventfcount(currentcount);


        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mReceiver);
    }


    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int verticalSpaceHeight=50;

        public VerticalSpaceItemDecoration(int verticalSpaceHeight) {
            this.verticalSpaceHeight = verticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = verticalSpaceHeight;
        }
    }


    public void changentfcount(){
        new PrefManager(getActivity()).saventfcount(0);
        currentcount=new PrefManager(getActivity()).getntfcount();
        if(currentcount==0){
            if(firstlaunch){
                HomeActivity.badge.setVisibility(View.INVISIBLE);
            }else {
                HomeActivity.badge.setText("!");

            }
        }else {
            HomeActivity.badge.setText(currentcount+"");
        }
    }


}
