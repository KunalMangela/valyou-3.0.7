package com.globalgyan.getvalyou.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.VideoPerformanceUpdator;

import static android.content.Context.BATTERY_SERVICE;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 4/3/17
 *         Module : Valyou.
 */

public class VideoPerformanceStarter extends Fragment {
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2, astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;


    AlertDialog alertDialog;
    public static VideoPerformanceStarter newInstance() {
        return new VideoPerformanceStarter();
    }

    private String displayName = null;

    Dialog dialog2;

    public void setAlreadyReorded(boolean alreadyReorded) {
        this.alreadyReorded = alreadyReorded;
    }

    private boolean alreadyReorded = false;

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    private View view = null;
    private VideoPerformanceUpdator videoPerformanceUpdator = null;
    private DataBaseHelper dataBaseHelper = null;
    ImageView bc;
    BatteryManager bm;
    ImageView vid_ic;
    int batLevel;
    BatteryReceiver mBatInfoReceiver;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.video_performance_starter, container, false);



        return view;
    }

    private void startanimation() {
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        super.onActivityCreated(savedInstanceState);
        getActivity().registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        appConstant = new AppConstant(getActivity());
        pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        bm = (BatteryManager)getActivity().getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        dataBaseHelper  = new DataBaseHelper(getActivity());;
        videoPerformanceUpdator = (VideoPerformanceUpdator) getActivity();
        vid_ic=(ImageView)view.findViewById(R.id.vid_ic);
        videoPerformanceUpdator.showHideToolbar(false);
        // TextView nameLayout = (TextView) view.findViewById(R.id.nameLayout);
        //  TextView descTextView = (TextView) view.findViewById(R.id.desc);
        red_planet = (ImageView)view.findViewById(R.id.red_planet);
        yellow_planet = (ImageView)view.findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)view.findViewById(R.id.purple_planet);
        earth = (ImageView)view.findViewById(R.id.earth);
        orange_planet = (ImageView)view.findViewById(R.id.orange_planet);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            startanimation();
        }

        else {

        }
        bc=(ImageView)view.findViewById(R.id.backimg);
        final Button buildNowView = (Button) view.findViewById(R.id.btnContinue);
        String status = dataBaseHelper.getUploadedVideoID(videoPerformanceUpdator.getId());
        Log.e("STATUS",status+"");
        try {
            if (!TextUtils.isEmpty(status)) {
                if (status.equalsIgnoreCase("true")) {
                    alreadyReorded = true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        bc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bc.setEnabled(false);
                dialog2 = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
                dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog2.setContentView(R.layout.quit_warning);
                Window window = dialog2.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                dialog2.setCancelable(false);
                wlp.gravity = Gravity.CENTER;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                dialog2.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                final ImageView astro = (ImageView) dialog2.findViewById(R.id.astro);
                final ImageView alien = (ImageView) dialog2.findViewById(R.id.alien);

                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                    astro_outx.setDuration(300);
                    astro_outx.setInterpolator(new LinearInterpolator());
                    astro_outx.setStartDelay(0);
                    astro_outx.start();
                    astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                    astro_outy.setDuration(300);
                    astro_outy.setInterpolator(new LinearInterpolator());
                    astro_outy.setStartDelay(0);
                    astro_outy.start();



                    alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                    alien_outx.setDuration(300);
                    alien_outx.setInterpolator(new LinearInterpolator());
                    alien_outx.setStartDelay(0);
                    alien_outx.start();
                    alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                    alien_outy.setDuration(300);
                    alien_outy.setInterpolator(new LinearInterpolator());
                    alien_outy.setStartDelay(0);
                    alien_outy.start();
                }
                final ImageView no = (ImageView) dialog2.findViewById(R.id.no_quit);
                final ImageView yes = (ImageView) dialog2.findViewById(R.id.yes_quit);

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        no.setEnabled(false);
                        yes.setEnabled(false);
                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                            astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                            astro_inx.setDuration(300);
                            astro_inx.setInterpolator(new LinearInterpolator());
                            astro_inx.setStartDelay(0);
                            astro_inx.start();
                            astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                            astro_iny.setDuration(300);
                            astro_iny.setInterpolator(new LinearInterpolator());
                            astro_iny.setStartDelay(0);
                            astro_iny.start();

                            alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                            alien_inx.setDuration(300);
                            alien_inx.setInterpolator(new LinearInterpolator());
                            alien_inx.setStartDelay(0);
                            alien_inx.start();
                            alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                            alien_iny.setDuration(300);
                            alien_iny.setInterpolator(new LinearInterpolator());
                            alien_iny.setStartDelay(0);
                            alien_iny.start();
                        }
                        new CountDownTimer(400, 400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                dialog2.dismiss();
                                bc.setEnabled(true);
                            }
                        }.start();


                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {

                        yes.setEnabled(false);
                        no.setEnabled(false);
                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                            astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                            astro_inx.setDuration(300);
                            astro_inx.setInterpolator(new LinearInterpolator());
                            astro_inx.setStartDelay(0);
                            astro_inx.start();
                            astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                            astro_iny.setDuration(300);
                            astro_iny.setInterpolator(new LinearInterpolator());
                            astro_iny.setStartDelay(0);
                            astro_iny.start();

                            alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                            alien_inx.setDuration(300);
                            alien_inx.setInterpolator(new LinearInterpolator());
                            alien_inx.setStartDelay(0);
                            alien_inx.start();
                            alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                            alien_iny.setDuration(300);
                            alien_iny.setInterpolator(new LinearInterpolator());
                            alien_iny.setStartDelay(0);
                            alien_iny.start();
                        }
                        new CountDownTimer(400, 400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                dialog2.dismiss();

                                bc.setEnabled(true);
                                getActivity().finish();
                                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            }
                        }.start();

                    }
                });

                try {
                    dialog2.show();
                }
                catch (Exception e){
                    e.printStackTrace();
                }


            }
        });
        if(alreadyReorded){
            //already recorded video
            //nameLayout.setText("You have already recorded this video");
            buildNowView.setVisibility(View.GONE);
            // descTextView.setVisibility(View.GONE);
        }else{
            //not yet recorded
            String name = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);
            if (!TextUtils.isEmpty(name) && name.length() > 2) {
                name = String.valueOf(name.charAt(0)).toUpperCase() + name.subSequence(1, name.length());
                // nameLayout.setText(name + ", create your video response");
            }
            //   descTextView.setText(displayName + " has requested video response for  questions");

            vid_ic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buildNowView.performClick();
                }
            });


            buildNowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildNowView.setEnabled(false);
                    new CountDownTimer(1500, 1500) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            buildNowView.setEnabled(true);
                        }
                    }.start();
                    Fragment fragment = VideoPerformanceQuestion.newInstance();
                    if (fragment != null) {
                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        fragmentTransaction.add(R.id.containerLayout, fragment);
                        fragmentTransaction.addToBackStack("VideoPerformanceQuestion");
                        fragmentTransaction.commit();

                    }
                }
            });
        }




    }






}
