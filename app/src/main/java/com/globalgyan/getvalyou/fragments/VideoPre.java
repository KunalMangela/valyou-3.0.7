package com.globalgyan.getvalyou.fragments;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.VideoPerformanceActivity;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.interfaces.VideoPerformanceUpdator;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

/**
 * Created by NaNi on 15/11/17.
 */

public class VideoPre extends Fragment {

    public static VideoPre newInstance() {
        return new VideoPre();
    }

    private View view = null;
    private VideoPerformanceUpdator videoPerformanceUpdator = null;
    LinearLayout llff;
    RelativeLayout previewwin;
    Button bckbtn,sub,pre,retry;
    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;
    View mBottomLayout;
    View mVideoLayout;
    private static final String TAG =VideoPre.class.getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.video_pre_btns, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Log.e(TAG, "onCreateView: ViewPreView");
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mVideoView = (UniversalVideoView) view.findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) view.findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);

        videoPerformanceUpdator = (VideoPerformanceUpdator) getActivity();
        bckbtn=(Button)view.findViewById(R.id.backbtn);
        bckbtn.setVisibility(View.VISIBLE);
        bckbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VideoPerformanceActivity.ispreview=false;
                bckbtn.setVisibility(View.GONE);
                getActivity().getSupportFragmentManager().popBackStack();
                videoPerformanceUpdator.updateQuestionIndex();

            }
        });
        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
            @Override
            public void onScaleChange(boolean isFullscreen) {
                // this.isFullscreen = isFullscreen;
                if (isFullscreen) {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

//                    ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                    layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                    mVideoLayout.setLayoutParams(layoutParams);
//                    //GONE the unconcerned views to leave room for video and controller
//                    mBottomLayout.setVisibility(View.GONE);
                } else {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

//                    ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                    layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                    mVideoLayout.setLayoutParams(layoutParams);
//                    mBottomLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPause(MediaPlayer mediaPlayer) { // Video pause
                Log.d("LIBAR", "onPause UniversalVideoView callback");
            }

            @Override
            public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                Log.d("LIBBB", "onStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                Log.d("OPN", "onBufferingStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                Log.d("fobfp", "onBufferingEnd UniversalVideoView callback");
//                llff.setVisibility(View.VISIBLE);
//                mMediaController.setVisibility(View.GONE);
            }

        });
//mVideoView.setFullscreen(false);
        //mVideoView.setFullscreen(true,0);
        if(new PrefManager(getActivity()).get_typevr().equalsIgnoreCase("trail")) {
            mVideoView.setVideoPath(new PrefManager(getActivity()).get_trailvrpath());

        }else {
            mVideoView.setVideoPath(new PrefManager(getActivity()).getVideoResponseDataPath());

        }

//            videoView.setClickable(false);
//            videoView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    previ.performClick();
//                }
//            });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                VideoPerformanceActivity.ispreview=false;
                bckbtn.setVisibility(View.GONE);
                getActivity().getSupportFragmentManager().popBackStack();
                videoPerformanceUpdator.updateQuestionIndex();
            }
        });

        mVideoView.start();
        mVideoView.performClick();
    }
}
