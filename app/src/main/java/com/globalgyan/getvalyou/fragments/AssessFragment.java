package com.globalgyan.getvalyou.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.AssessmentGroupsActivity;
import com.globalgyan.getvalyou.CheckP;
import com.globalgyan.getvalyou.CourseLIstActivity;
import com.globalgyan.getvalyou.FeedbackActivity;
import com.globalgyan.getvalyou.FunGameModel;
import com.globalgyan.getvalyou.FunGamesAdapter;
import com.globalgyan.getvalyou.FunModel;
import com.globalgyan.getvalyou.MainPlanetActivity;
import com.globalgyan.getvalyou.ModulesActivity;
import com.globalgyan.getvalyou.SimulationAdapter;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.ToadysgameModel;
import com.globalgyan.getvalyou.TodaysGameAdapter;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.GameModel;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.HttpGet;
import com.pluscubed.recyclerfastscroll.RecyclerFastScroller;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;

import static android.content.Context.BATTERY_SERVICE;
import static com.globalgyan.getvalyou.HomeActivity.appConstant;
import static com.globalgyan.getvalyou.HomeActivity.batLevel;
import static com.globalgyan.getvalyou.HomeActivity.isPowerSaveMode;
import static com.globalgyan.getvalyou.HomeActivity.token;
import static com.globalgyan.getvalyou.apphelper.BatteryReceiver.flag_play_anim;

//import com.alexfu.countdownview.CountDownView;

/**
 * Created by NaNi on 18/09/17.
 */

public class AssessFragment extends android.support.v4.app.Fragment  {
    private ProgressDialog dialog;
    RelativeLayout rl_left,rl_right;
    View view;
    String assessg;
    CardView c1,c2,c3,c4,c5,c6;
    ConnectionUtils connectionUtils;
    DefaultHttpClient httpClient;
    RecyclerFastScroller recyclerFastScroller, recyclerFastScroller1,recyclerFastScroller2;
    private SimpleLocation location;
    HttpPost httpPost;
    HttpGet httpGet;
    public static boolean waterfall_visibility=false;
    HttpURLConnection urlConnection, urlConnection1;
    String game_tip_is;
    FontTextView timerrr, dqus,qno;
    public static int cqa=0,countavailablegame=0;
    Dialog dialog_is;
    String game_resp;
    ImageView heading;
    TextView textassessment;

    //    PhotoViewAttacher photoAttacher;
    public static ArrayList<GameModel> list_games=new ArrayList<>();
    static InputStream is = null;
    static JSONObject jObj = null,jObjeya=null;
    JSONArray data;
    JSONObject dataJSONobject;
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    DataBaseHelper dataBaseHelper;
    int responseCode,responseCod,responseCo;
    public static ArrayList<String> list_lg_ids=new ArrayList<String>();
    public static ArrayList<String> list_certi_ids=new ArrayList<String>();
    public static ArrayList<String> list_course_ids=new ArrayList<String>();
    public static ArrayList<String> certi_name=new ArrayList<String>();
    public static ArrayList<String> course_name=new ArrayList<String>();
    public static ArrayList<String> Extra_ids=new ArrayList<String>();
    public static ArrayList<String> desc_statuslist=new ArrayList<>();
    public static int played_status_from_learning=0;
    public static Boolean isvalue=false;
    public static RecyclerView today_game_list;
    TodaysGameAdapter todaysGameAdapter;
    TextView textassessmentheading;
//    SimulationAdapter simulationAdapter;
//    FunGamesAdapter funGamesAdapter;
    FunGameModel funGameModel;
    String uid;
    ProgressDialog pr;
    RecyclerView.OnItemTouchListener disabler;
    public static HttpURLConnection urlConnection2;
    public static int code2;

    CountdownView endtime;
    Dialog assess_progress;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView no_sim,no_ass,no_games;
    LinearLayout ln;
    Dialog alertDialog;
    public static ArrayList<ToadysgameModel> mainList=new ArrayList<>();
    FrameLayout fr_list;
    public static FrameLayout noassessmentavailableframe;
    public static TextView noavailabletext;
    MaterialRefreshLayout swipeass;
    public static ImageView okbtn;
    ImageView refresh_btn;
    public static ArrayList<FunModel> simulation_list_arraylist;
    FrameLayout ass;
    ArrayList<FunGameModel> fungame_list;

    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;

    BatteryReceiver mBatInfoReceiver;


    public static AssessFragment newInstance() {
        return new AssessFragment();
    }
    LocationManager locationManager;

    ImageView ref_ic;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=  inflater.inflate(R.layout.frag_assess,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        connectionUtils=new ConnectionUtils(getActivity());
        dataBaseHelper = new DataBaseHelper(getActivity());

        getActivity().registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        location = new SimpleLocation(getActivity());
        today_game_list=(RecyclerView)view.findViewById(R.id.todaysgame_list);
        today_game_list.setHasFixedSize(true);
        textassessment=(TextView)view.findViewById(R.id.textassessment);
        okbtn=(ImageView)view.findViewById(R.id.okbtn);
        noavailabletext=(TextView)view.findViewById(R.id.noavailabletext);
        noassessmentavailableframe=(FrameLayout)view.findViewById(R.id.noassessmentavailableframe);
//        simulation_list=(RecyclerView)view.findViewById(R.id.simulation_list);
//        funGamerecyclerList=(RecyclerView)view.findViewById(R.id.fungames_list);
  /*      pacmantext=(FontTextView)view.findViewById(R.id.pacmantext);
  */     // sim=(FrameLayout)view.findViewById(R.id.sim_frame);
        ass=(FrameLayout)view.findViewById(R.id.ass_frame);
   //     game=(FrameLayout)view.findViewById(R.id.fungame_frame);
        //heading=(ImageView)view.findViewById(R.id.heading);
        ref_ic=(ImageView)view.findViewById(R.id.ref_ic);
    /*    endtime=(CountdownView) view.findViewById(R.id.timetext);
    */    no_ass=(TextView)view.findViewById(R.id.no_ass_text);
//        no_games=(TextView)view.findViewById(R.id.no_games_text);
//        no_sim=(TextView)view.findViewById(R.id.no_sim_text);
      //  recyclerFastScroller=(RecyclerFastScroller) view.findViewById(R.id.fastscroll);
//        recyclerFastScroller1=(RecyclerFastScroller) view.findViewById(R.id.fastscroll1);
//        recyclerFastScroller2=(RecyclerFastScroller) view.findViewById(R.id.fastscroll2);

        textassessmentheading=(TextView)view.findViewById(R.id.textassessmentheading);
        swipeass=(MaterialRefreshLayout) view.findViewById(R.id.swipeass);
        ref_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshList();
            }
        });
        final ImageView backis=(ImageView)view.findViewById(R.id.backbtnis_ass);


        backis.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    backis.setBackgroundColor(Color.parseColor("#33ffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            backis.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }*/
                return false;
            }
        });
        noassessmentavailableframe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noassessmentavailableframe.setVisibility(View.GONE);
            }
        });


        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noassessmentavailableframe.setVisibility(View.GONE);
            }
        });

        swipeass.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                ConnectionUtils connectionUtils=new ConnectionUtils(getActivity());
                if(connectionUtils.isConnectionAvailable()){

                    if(new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")){
                        Toast.makeText(getActivity(),"Video uploading is in progrss,Can't access latest notifications now",Toast.LENGTH_LONG).show();
                        swipeass.finishRefresh();
                    }else {
                        mainList.clear();

                        todaysGameAdapter.notifyDataSetChanged();
                       refreshList();
                       /* int positionView = ((LinearLayoutManager)recyclerGen.getLayoutManager()).findFirstVisibleItemPosition();
                        Log.e("pos",String.valueOf(positionView));
                        if(positionView==0) {
                            swipeRefreshLayout.setEnabled(true);


                        }else {

                        }
*/
                    }
                }else {
                    swipeass.finishRefresh();
                    Toast.makeText(getActivity(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });


        backis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(HomeActivity.apiloading){

                    backis.setEnabled(false);
                    try{
                        new GetContacts().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new GetContact().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    new CountDownTimer(500, 500) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            String get_nav = " ";

                            try{
                                get_nav =new PrefManager(getActivity()).getNav();
                            }
                            catch (Exception e){
                                get_nav = " ";
                                e.printStackTrace();
                            }
                            if (get_nav.equalsIgnoreCase("learn")) {
                                Intent intent = new Intent(getActivity(), ModulesActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                getActivity().finish();
                            } else {
                                Intent intent = new Intent(getActivity(), AssessmentGroupsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                getActivity().finish();
                            }
                            backis.setEnabled(true);
                        }
                    }.start();
                }else {
                    String get_nav = " ";

                    try{
                        get_nav =new PrefManager(getActivity()).getNav();
                    }
                    catch (Exception e){
                        get_nav = " ";
                        e.printStackTrace();
                    }
                    if (get_nav.equalsIgnoreCase("learn")) {
                        Intent intent = new Intent(getActivity(), ModulesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        getActivity().finish();
                    } else {
                        Intent intent = new Intent(getActivity(), AssessmentGroupsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        getActivity().finish();
                    }
                    backis.setEnabled(true);

                }
            }
        });



        textassessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(HomeActivity.apiloading){

                    backis.setEnabled(false);
                    try{
                        new GetContacts().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new GetContact().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    new CountDownTimer(500, 500) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            String get_nav = " ";

                            try{
                                get_nav =new PrefManager(getActivity()).getNav();
                            }
                            catch (Exception e){
                                get_nav = " ";
                                e.printStackTrace();
                            }
                            if (get_nav.equalsIgnoreCase("learn")) {
                                Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                getActivity().finish();
                            } else {
                                Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                                getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                getActivity().finish();
                            }
                            backis.setEnabled(true);
                        }
                    }.start();
                }else {
                    String get_nav = " ";

                    try{
                        get_nav =new PrefManager(getActivity()).getNav();
                    }
                    catch (Exception e){
                        get_nav = " ";
                        e.printStackTrace();
                    }
                    if (get_nav.equalsIgnoreCase("learn")) {
                        Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        getActivity().finish();
                    } else {
                        Intent intent = new Intent(getActivity(), MainPlanetActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        getActivity().finish();
                    }
                    backis.setEnabled(true);

                }
            }
        });


      /*  game_tip=(FontTextView)view.findViewById(R.id.game_tip);
        ln=(LinearLayout)view.findViewById(R.id.mainlin);
        fr_list=(FrameLayout)view.findViewById(R.id.framelist);
        refresh_btn=(ImageView)view.findViewById(R.id.refresh_btn);
      */  final Calendar today = Calendar.getInstance();
        today.set(Calendar.SECOND, 59);
        today.set(Calendar.MINUTE, 59);
        today.set(Calendar.HOUR_OF_DAY, 23
        );
        disabler = new RecyclerViewDisabler();
        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.otf");
        no_ass.setTypeface(typeface1);
        noavailabletext.setTypeface(typeface1);
        textassessment.setTypeface(typeface1);
        textassessmentheading.setTypeface(typeface1);
/*





        //  rl_left=(RelativeLayout)view.findViewById(R.id.rel_left);

        long offset = today.get(Calendar.ZONE_OFFSET) +
                today.get(Calendar.DST_OFFSET);
        long sinceMid = (today.getTimeInMillis()) %
                (24 * 60 * 60 * 1000);
        Log.e("sectoday",String.valueOf(sinceMid));
        Calendar todayis = Calendar.getInstance();
        long availablesec=(System.currentTimeMillis()) %
                (24 * 60 * 60 * 1000);
        Log.e("secnow",String.valueOf(availablesec));

        long countdowntime=Long.parseLong(String.valueOf(Integer.parseInt(String.valueOf(sinceMid))-Integer.parseInt(String.valueOf(availablesec))));
        endtime.start(countdowntime);*/

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        today_game_list.setLayoutManager(linearLayoutManager);
//        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        simulation_list.setLayoutManager(linearLayoutManager1);
//        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        funGamerecyclerList.setLayoutManager(linearLayoutManager2);
        todaysGameAdapter=new TodaysGameAdapter(getActivity(),mainList);
        simulation_list_arraylist=new ArrayList<>();
        fungame_list=new ArrayList<>();
//        simulationAdapter=new SimulationAdapter(getActivity(),simulation_list_arraylist);
//        funGamesAdapter=new FunGamesAdapter(getActivity(),fungame_list);

        fungame_list.add(new FunGameModel("World Wterfall","Fun Game"));

        today_game_list.setAdapter(todaysGameAdapter);
//        simulation_list.setAdapter(simulationAdapter);
//        funGamerecyclerList.setAdapter(funGamesAdapter);

//
//        recyclerFastScroller1.attachRecyclerView(funGamerecyclerList);
//        recyclerFastScroller2.attachRecyclerView(simulation_list);
       // recyclerFastScroller.attachRecyclerView(today_game_list);

      //  recyclerFastScroller.attachAdapter(todaysGameAdapter);
//        recyclerFastScroller2.attachAdapter(simulationAdapter);
//        recyclerFastScroller1.attachAdapter(funGamesAdapter);

       createProgress();
        String myName = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);
        if ( !TextUtils.isEmpty(myName) ) {
            dn=myName;
        } else {
            dn="";
        }

        String emailok = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);
        if ( !TextUtils.isEmpty(emailok) ) {
            emaileya=emailok;
        } else {
            emaileya=" ";
        }
        String type_ass = " ";

        try
        {
            type_ass=new PrefManager(getActivity()).getAsstype();
        }
        catch (Exception e)
        {
            type_ass = " ";
            e.printStackTrace();
        }


        if(type_ass.equalsIgnoreCase("Ass")){
        //    ass.setVisibility(View.VISIBLE);

            new CountDownTimer(100, 50) {
                @Override
                public void onTick(long millisUntilFinished) {
                    ass.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish() {
                    if(HomeActivity.assess) {

                        swipeass.autoRefresh();
                        // swipeass.setShowArrow(true);

                    }

                    String get_nav = " ";

                    try{
                        get_nav=new PrefManager(getActivity()).getNav();
                    }
                    catch (Exception e){
                        get_nav = " ";
                        e.printStackTrace();
                    }

                    if(get_nav.equalsIgnoreCase("learn")){

                        swipeass.autoRefresh();
                        //   swipeass.setShowArrow(true);


                    }                }
            }.start();
//            game.setVisibility(View.GONE);
//            sim.setVisibility(View.GONE);
          // heading.setImageResource(R.drawable.ass_text);

        }
        if(type_ass.equalsIgnoreCase("sim")){


            ass.setVisibility(View.GONE);
//            game.setVisibility(View.GONE);
//            sim.setVisibility(View.VISIBLE);
          //  heading.setImageResource(R.drawable.sim_text);
            if(HomeActivity.assess) {
                new GetContacts().execute();
            }

            String get_nav = " ";

            try{
                get_nav=new PrefManager(getActivity()).getNav();
            }
            catch (Exception e){
                get_nav = " ";
                e.printStackTrace();
            }
            if(get_nav.equalsIgnoreCase("learn")){
                new GetContacts().execute();

            }
        }
        if(type_ass.equalsIgnoreCase("fun")){

            ass.setVisibility(View.GONE);
//            game.setVisibility(View.VISIBLE);
//            sim.setVisibility(View.GONE);
        //    heading.setImageResource(R.drawable.fungame_text);
            if(HomeActivity.assess) {
                new GetContacts().execute();
            }
            String get_nav = " ";

            try{
                get_nav=new PrefManager(getActivity()).getNav();
            }
            catch (Exception e){
                get_nav = " ";
                e.printStackTrace();
            }
            if(get_nav.equalsIgnoreCase("learn")){
                new GetContacts().execute();

            }
        }


        red_planet = (ImageView)view.findViewById(R.id.red_planet);
        yellow_planet = (ImageView)view.findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)view.findViewById(R.id.purple_planet);
        earth = (ImageView)view.findViewById(R.id.earth);
        orange_planet = (ImageView)view.findViewById(R.id.orange_planet);



        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            startanimation();
        }



    }

    public void startanimation(){
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void createProgress() {
        assess_progress = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        assess_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assess_progress.setContentView(R.layout.planet_loader);
        Window window = assess_progress.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        assess_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) assess_progress.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        assess_progress.setCancelable(false);
       // assess_progress.show();

    }


    private class GetContacts extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {

            HomeActivity.apiloading=true;
            /*try {
                assess_progress.show();
            }catch (Exception e){

            }*/
            super.onPreExecute();
           /* try {
                if (dialog_is.isShowing()) {

                } else {
                    showDialogue();
                }
            }catch (Exception e){
                showDialogue();
            }*/
           // swipeRefreshLayout.setRefreshing(true);
          //  swipeRefreshLayout.setDistanceToTriggerSync(999999);
            list_games.clear();
            simulation_list_arraylist.clear();

            mainList.clear();
            todaysGameAdapter.notifyDataSetChanged();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream1(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                HomeActivity.apiloading=false;
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                //assess_progress.dismiss();
                                swipeass.finishRefresh();
                                exceptionOccurs();
                            } catch (Exception e) {

                            }
                        }
                    });
                }
                catch (Exception ex){
                    e.printStackTrace();
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                HomeActivity.apiloading=false;
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                swipeass.finishRefresh();

                                //assess_progress.dismiss();
                                exceptionOccurs();
                            } catch (Exception e) {

                            }
                        }
                    });
                }catch (Exception ex){
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                HomeActivity.apiloading=false;
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                swipeass.finishRefresh();

                                // assess_progress.dismiss();
                                exceptionOccurs();
                            } catch (Exception e) {

                            }
                        }
                    });
                }catch (Exception ex){
                    e.printStackTrace();
                }
            }
          /*  try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();


                Log.e("JSONStr", json);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }*/
            try {
                jObj = new JSONObject(result1);
                token=jObj.getString("access_token");
                try {
                    new PrefManager(getActivity()).savetoken(token);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                token_type=jObj.getString("token_type");
            } catch (Exception e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if (responseCod != urlConnection.HTTP_OK) {

                    //  simulation_list.setVisibility(View.INVISIBLE);
                    no_ass.setVisibility(View.VISIBLE);
                    //  no_fun_games.setVisibility(View.VISIBLE);
                    //  swipeRefreshLayout.setRefreshing(false);
                    // swipeRefreshLayout.setDistanceToTriggerSync(0);
                    //  dialog_is.dismiss();
                    HomeActivity.apiloading = false;
                    try {
                        swipeass.finishRefresh();

                        // assess_progress.dismiss();
                    } catch (Exception e) {

                    }
                    //   countavailablegame=0;
                    today_game_list.setClickable(true);
                    today_game_list.setNestedScrollingEnabled(true);
          /*      try {
                    if (assess_progress.isShowing() && assess_progress != null) {
                        assess_progress.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
          */
                    Toast.makeText(getActivity(), "Error while loading user data", Toast.LENGTH_SHORT).show();
                } else {




                    String ass_type = " ";

                    try{
                        ass_type = new PrefManager(getActivity()).getAsstype();
                    }
                    catch (Exception e){
                        ass_type = " ";
                        e.printStackTrace();
                    }
                    if (ass_type.equalsIgnoreCase("ass")) {
                        new GetContact().execute();

                    } else {
                        String candidateId = " ";
                        try {
                            candidateId  = PreferenceUtils.getCandidateId(getActivity());
                        }
                        catch (Exception e){
                             candidateId = " ";
                                e.printStackTrace();
                        }
                        GetSImulation getSImulation = new GetSImulation();
                        String params = "sm_id=1&u_id=" + candidateId;
                        getSImulation.execute(AppConstant.Ip_url + "simulation/list", params);

                    }


                }

            }catch (Exception e){

            }
        }
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private class GetEywa extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {

            String cand_id = " ";
            try {
                try{
                    cand_id = PreferenceUtils.getCandidateId(getActivity());
                }
                catch (Exception e){
                    cand_id = " ";
                    e.printStackTrace();
                }
                httpClient = new DefaultHttpClient();
                String url = AppConstant.BASE_URL+"getCandidateDetails/" + cand_id;
                // String url = "http://admin.getvalyou.com/api/getCandidateDetails/59de53e90f43165f3afdded0";
//navya
                httpGet = new HttpGet(url);


                httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
                httpGet.setEntity(new UrlEncodedFormEntity(params));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                responseCo = httpResponse.getStatusLine().getStatusCode();
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                jsoneya = sb.toString();


                Log.e("JSONNOYU", jsoneya);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            try {
                jObjeya = new JSONObject(jsoneya);
                dn= jObjeya.getString("displayName");
                emaileya=jObjeya.getString("email");

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(responseCo!=200){

            }else
                new GetContact().execute();
        }

    }
    private String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            jso = sb.toString();


            Log.e("JSONStrr", jso);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return jso;
    }

    private String readStream1(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

    private class GetContact extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            countavailablegame=0;
            mainList.clear();
            list_lg_ids.clear();
            certi_name.clear();
            list_certi_ids.clear();
            course_name.clear();
            list_course_ids.clear();

            todaysGameAdapter.notifyDataSetChanged();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "", cand_id = " ", auth_token = " ";
          //  String tok=new PrefManager(getActivity()).getToken();
            try {


                try {
                    // httpClient = new DefaultHttpClient();
                    // httpPost = new HttpPost("http://35.154.93.176/Player/TodayGames");

                    try{

                        auth_token = new PrefManager(getActivity()).getAuthToken();
                        cand_id = PreferenceUtils.getCandidateId(getActivity());

                    }catch (Exception e){
                        e.printStackTrace();
                        auth_token = " ";
                        cand_id = " ";
                    }

                    String jon = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_First", dn)
                            .object("email", emaileya)
                            .object("U_Last", "")
                            .object("U_Job", "")
                            .object("U_Other", "")
                            .object("U_Id",cand_id)
                            .build().toString();


//                    String jon = new JsonBuilder(new GsonAdapter())
//                            .object("data")
//                            .object("U_First", "Sandesh Jadhav")
//                            .object("email", "sandesh@fortress.co.in")
//                            .object("U_Last", "")
//                            .object("U_Job", "")
//                            .object("U_Other", "")
//                            .object("U_Id", "59b7f1c33a81703f6da159ba")
//                            .build().toString();


                    //  StringEntity se = new StringEntity(jon);
                    // Log.e("Reqt",jon+"");
                    // Log.e("Request",se+"");
                    Log.e("datais","ip: "+AppConstant.getLocalIpAddressApp()+" os: "+AppConstant.getAndroidVersionApp()
                            +" version: "+AppConstant.getversionofApp()+" loc: "+location.getLatitude()+","+location.getLongitude());
                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/TodayGames");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jon.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + HomeActivity.token);
                    urlConnection1.setRequestProperty("auth_token",auth_token);

                    urlConnection1.setRequestProperty("app_version",AppConstant.getversionofApp());
                    urlConnection1.setRequestProperty("app_os",AppConstant.getAndroidVersionApp());
                    urlConnection1.setRequestProperty("app_ip",AppConstant.getLocalIpAddressApp());
                    urlConnection1.setRequestProperty("app_latlong",location.getLatitude()+","+location.getLongitude());

                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());

                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jon);
                    wr.flush();

                    responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.e("TdaysgameResponse", responseString);
                    result1 = responseString;



                 /*  httpPost.addHeader("Authorization","Bearer "+HomeActivity.token);
                  httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                     responseCode = httpResponse.getStatusLine().getStatusCode();
                   HttpEntity httpEntity = httpResponse.getEntity();
                   is = httpEntity.getContent();
                    Log.e("Fivvovbobo",httpResponse+"CHARAN"+responseCode);*/


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    HomeActivity.apiloading=false;
                    try {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    swipeass.finishRefresh();

                                    //assess_progress.dismiss();
                                    exceptionOccurs();
                                } catch (Exception e) {

                                }
                            }
                        });
                    }catch (Exception ex){
                        e.printStackTrace();
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    HomeActivity.apiloading=false;
                    try {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    swipeass.finishRefresh();

                                    // assess_progress.dismiss();
                                    exceptionOccurs();
                                } catch (Exception e) {

                                }
                            }
                        });
                    }catch (Exception ex){
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    HomeActivity.apiloading=false;
                    try {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    swipeass.finishRefresh();

                                    // assess_progress.dismiss();
                                    exceptionOccurs();
                                } catch (Exception e) {

                                }
                            }
                        });
                    }catch (Exception ex){
                        e.printStackTrace();
                    }
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("veri","today");
            // Log.e("auth_log_token",new PrefManager(getActivity()).getAuthToken());
            // Log.e("want",new PrefManager(getActivity()).getAuthToken());
            //   dialog_is.dismiss();
            list_games.clear();
            fungame_list.clear();
            mainList.clear();

            todaysGameAdapter.notifyDataSetChanged();

            HomeActivity.apiloading=false;
            try{
                swipeass.finishRefresh();

                //  assess_progress.dismiss();
            }catch (Exception e)
            {

            }
          //  String ss=new PrefManager(getActivity()).getAuthToken().toString();
            if(s.contains("error")){
                Log.e("respmsg","error");
                // swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONObject js=new JSONObject(s);
                    String errormsg=js.getString("error");
                    AppConstant appConstant=new AppConstant(getActivity());
                    appConstant.showLogoutDialogue(errormsg);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("respmsg","errorexc");
                }

            }else {
                Log.e("respmsg","noerror");

                try {

                    game_resp = s;

                    String from_where=" ";
                    try {
                        from_where = new PrefManager(getActivity()).getNav();
                    }
                    catch (Exception e){
                        from_where = " ";
                        e.printStackTrace();
                    }

                    if (from_where.equalsIgnoreCase("learn")) {
                        desc_statuslist.clear();
                        loadGamesLearning(game_resp);

                    } else {
                        desc_statuslist.clear();
                        loadGames(game_resp);

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
            /*  try {
                if (assess_progress.isShowing() && assess_progress != null) {
                    assess_progress.dismiss();
                }
            }catch (Exception e){
                e.printStackTrace();
            }*/


        }
    }


    public void loadGames(String s){
        HomeActivity.apiloading=false;
        new PrefManager(getActivity()).saveAsstype("Ass");
        new PrefManager(getActivity()).saveRatingtype("assessment_group");
        new PrefManager(getActivity()).saveNav("Ass");
       // swipeRefreshLayout.setDistanceToTriggerSync(0);

        try {
            jObj = new JSONObject(s);
            uid = jObj.getString("U_Id");
            game_tip_is=jObj.getString("Game_tip");

            data= (JSONArray)jObj.get("data");
            Log.e("adtaa",data.length()+"");
            for (int i = 0; i < data.length(); i++) {
                dataJSONobject = data.getJSONObject(i);
                GameModel gameModel=new GameModel();
                ToadysgameModel toadysgameModel=new ToadysgameModel();
                gameModel.setGdid(dataJSONobject.getString("GD_Id"));
                gameModel.setCeid(dataJSONobject.getString("CE_Id"));
                try{
                    gameModel.setPic_frequency(dataJSONobject.getInt("CE_PICFrequency"));
                    toadysgameModel.setPic_frequency(dataJSONobject.getInt("CE_PICFrequency"));
                }catch (Exception e){
                    gameModel.setPic_frequency(0);
                    toadysgameModel.setPic_frequency(0);
                    e.printStackTrace();
                }


                gameModel.setUid(jObj.getString("U_Id"));
                gameModel.setTgid(dataJSONobject.getString("TG_Id"));
                toadysgameModel.setTg_id(dataJSONobject.getString("TG_Id"));
                gameModel.setGame(dataJSONobject.getString("Game"));
                gameModel.setCategory(dataJSONobject.getString("Category"));
                gameModel.setUrl_ic(dataJSONobject.getString("GD_Imgurl"));
                toadysgameModel.setLock_status(dataJSONobject.getString("is_locked"));
                toadysgameModel.setAvailable_date(dataJSONobject.getString("CED_Date"));
                toadysgameModel.setUrl_ic(dataJSONobject.getString("GD_Imgurl"));
                toadysgameModel.setAss_uid(jObj.getString("U_Id"));
                toadysgameModel.setAss_gdid(dataJSONobject.getString("GD_Id"));
                toadysgameModel.setAss_category(dataJSONobject.getString("Category"));
                toadysgameModel.setAss_game(dataJSONobject.getString("Game"));
                toadysgameModel.setAss_groupid(dataJSONobject.getString("TG_GroupId"));
                toadysgameModel.setAss_group(dataJSONobject.getString("TG_Group"));
                toadysgameModel.setAss_gametime(dataJSONobject.getString("GD_Time"));
                toadysgameModel.setAss_descgame(dataJSONobject.getString("GD_Description"));
                toadysgameModel.setSm_landingpage_img(" ");
                toadysgameModel.setSm_bgcolor(" ");
                toadysgameModel.setSm_bg_img(" ");
                toadysgameModel.setSm_bgsound(" ");
                toadysgameModel.setSm_icon(" ");
                toadysgameModel.setSm_dc_bgcolor(" ");
                toadysgameModel.setSm_dc_bgimg(" ");
                toadysgameModel.setSm_company(" ");
                toadysgameModel.setSm_ic_img(" ");
                toadysgameModel.setSm_desc(" ");
                toadysgameModel.setSm_gif(" ");
                toadysgameModel.setSm_giftime(" ");
                toadysgameModel.setSm_titlecolor(" ");
                toadysgameModel.setSm_title_desc_color(" ");

                //new params
                try{
                    gameModel.setLg_id(String.valueOf(dataJSONobject.getInt("lg_id")));
                    toadysgameModel.setLg_id(String.valueOf(dataJSONobject.getInt("lg_id")));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setLg_id(String.valueOf(0));
                    toadysgameModel.setLg_id(String.valueOf(0));

                }

                try{
                    gameModel.setCourse_id(String.valueOf(dataJSONobject.getInt("course_id")));
                    toadysgameModel.setCourse_id(String.valueOf(dataJSONobject.getInt("course_id")));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setCourse_id(String.valueOf(0));
                    toadysgameModel.setCourse_id(String.valueOf(0));

                }

                try{
                    gameModel.setCertification_id(String.valueOf(dataJSONobject.getInt("certification_id")));
                    toadysgameModel.setCertification_id(String.valueOf(dataJSONobject.getInt("certification_id")));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setCertification_id(String.valueOf(0));
                    toadysgameModel.setCertification_id(String.valueOf(0));

                }

                try{

                    gameModel.setLearning_group_name(dataJSONobject.getString("learning_group_name"));
                    toadysgameModel.setLearning_group_name(dataJSONobject.getString("learning_group_name"));

                }catch (Exception e){
                    e.printStackTrace();

                    gameModel.setLearning_group_name("");
                    toadysgameModel.setLearning_group_name("");

                }

                try{

                    gameModel.setCourse_name(dataJSONobject.getString("course_name"));
                    toadysgameModel.setCourse_name(dataJSONobject.getString("course_name"));

                }catch (Exception e){
                    e.printStackTrace();

                    gameModel.setCourse_name("");
                    toadysgameModel.setCourse_name("");

                }

                try{
                    gameModel.setCertification_name(dataJSONobject.getString("certification_name"));
                    toadysgameModel.setCertification_name(dataJSONobject.getString("certification_name"));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setCertification_name("");
                    toadysgameModel.setCertification_name("");

                }


                gameModel.setDisplayname(dataJSONobject.getString("Display_Name"));
                toadysgameModel.setName(dataJSONobject.getString("Display_Name"));
                gameModel.setStatus(dataJSONobject.getString("TG_Status"));
                toadysgameModel.setStatus(dataJSONobject.getString("TG_Status"));
                toadysgameModel.setType_is("assessment");
                toadysgameModel.setIdis(dataJSONobject.getString("CE_Id"));


                int val;
                try {
                    val = dataJSONobject.getInt("lg_id");
                    Log.e("lgid",""+val);
                }catch (Exception e){
                    e.printStackTrace();
                    val=0;
                    Log.e("lgid",""+val);

                }
                if(val==0){
                    if(!dataJSONobject.getString("TG_Status").equalsIgnoreCase("Yet to Play")){
                        countavailablegame++;
                        gameModel.setTemp_status("played");
                        toadysgameModel.setTemp_status("played");
                        gameModel.setStatus_id(1);
                        toadysgameModel.setStatus_id(1);
                        gameModel.setExtra_param("o");
                        toadysgameModel.setExtra_param("o");
                    }else {
                        gameModel.setStatus_id(3);
                        toadysgameModel.setStatus_id(3);
                        gameModel.setExtra_param("obc");
                        toadysgameModel.setExtra_param("obc");
                        gameModel.setTemp_status("yet to play");
                        toadysgameModel.setTemp_status("yet to play");
                    }
                }else {
                    gameModel.setStatus_id(2);
                    toadysgameModel.setStatus_id(2);
                    gameModel.setExtra_param("ob");
                    toadysgameModel.setExtra_param("ob");
                    gameModel.setTemp_status("yet play");
                    toadysgameModel.setTemp_status("yet play");
                }


                gameModel.setGroupid(dataJSONobject.getString("TG_GroupId"));
                gameModel.setGroup(dataJSONobject.getString("TG_Group"));
                toadysgameModel.setSubname(dataJSONobject.getString("TG_Group"));
                HomeActivity.tg_group=dataJSONobject.getString("TG_Group");
                gameModel.setCe_picavailable(dataJSONobject.getString("CE_TakeProfilePIC"));

                try{
                    gameModel.setCe_pic_frequency(dataJSONobject.getString("CE_PICFrequency"));

                }catch (Exception e){
                    gameModel.setCe_pic_frequency("0");

                    e.printStackTrace();
                }

                gameModel.setCe_status(dataJSONobject.getString("CE_Status"));
                gameModel.setGame_time(dataJSONobject.getString("GD_Time"));
                gameModel.setDescription_game(dataJSONobject.getString("GD_Description"));

                String lg_val;
                try{
                   lg_val=String.valueOf(dataJSONobject.getInt("lg_id"));

                }catch (Exception e){
                    e.printStackTrace();
                    lg_val="0";

                }

                if(!lg_val.equalsIgnoreCase("0")){

                }else {
                    String assgroupid_sharedpref=new PrefManager(getActivity()).getAssgroupId();
                    String current_ce_id=String.valueOf(dataJSONobject.getString("CE_Id"));
                    if(assgroupid_sharedpref.equalsIgnoreCase(current_ce_id)) {
                        list_games.add(gameModel);
                        mainList.add(toadysgameModel);
                    }
                }

                todaysGameAdapter.notifyDataSetChanged();
                Log.e("eror","forass"+i);
            }




            try {
                JSONArray js=jObj.getJSONArray("sm_list");
                for(int i=0;i<js.length();i++){
                    JSONObject jobj=js.getJSONObject(i);
                    FunModel funModel=new FunModel();
                    ToadysgameModel toadysgameModel=new ToadysgameModel();
                    funModel.setSmname(jobj.getString("sm_name"));
                    toadysgameModel.setName(jobj.getString("sm_name"));
                    toadysgameModel.setTg_id("17688363849");
                    funModel.setLandingpage_img(jobj.getString("sm_landing_page_image"));
                    funModel.setBgcolor(jobj.getString("sm_bg_color"));
                    funModel.setBgimg(jobj.getString("sm_bg_image"));
                    funModel.setDc_bg_color(jobj.getString("sm_decision_bg_color"));
                    funModel.setDc_bg_img(jobj.getString("sm_decision_bg_image"));
                    funModel.setBgsound(jobj.getString("sm_bg_sound"));
                    funModel.setId(jobj.getString("sm_id"));
                    funModel.setGroup(jobj.getString("TG_Group"));
                    funModel.setSm_type(jobj.getString("sm_type"));
                    funModel.setSm_articulate_url(jobj.getString("sm_articulate_url"));
                    funModel.setSm_icon(jobj.getString("sm_icon"));
                    funModel.setCompanyname(jobj.getString("sm_company"));
                    toadysgameModel.setSubname(jobj.getString("sm_company"));
                    toadysgameModel.setStatus("Yet to Play");
                    toadysgameModel.setType_is("simulation");
                    toadysgameModel.setAvailable_date("");
                    toadysgameModel.setTemp_status("Yet to Play");
                    toadysgameModel.setIdis(jobj.getString("sm_id"));
                    toadysgameModel.setSm_type(jobj.getString("sm_type"));
                    toadysgameModel.setSm_articulate_url(jobj.getString("sm_articulate_url"));
                    funModel.setGif(jobj.getString("sm_bg_gif"));
                    funModel.setDesc(jobj.getString("sm_description"));
                    toadysgameModel.setLock_status("unlocked");
                    funModel.setGif_load_time(jobj.getString("sm_gif_loading_time"));
                    funModel.setTitlecolor(jobj.getString("sm_title_color"));
                    funModel.setTitledescriptioncolor(jobj.getString("sm_title_description_color"));





                    toadysgameModel.setAss_uid(" ");
                    toadysgameModel.setAss_gdid(" ");
                    toadysgameModel.setAss_category(" ");
                    toadysgameModel.setAss_game(" ");
                    toadysgameModel.setAss_groupid(" ");
                    toadysgameModel.setAss_group(jobj.getString("TG_Group"));
                    toadysgameModel.setAss_gametime(" ");
                    toadysgameModel.setAss_descgame(" ");
                    toadysgameModel.setPic_frequency(0);

                    toadysgameModel.setSm_landingpage_img(jobj.getString("sm_landing_page_image"));
                    toadysgameModel.setSm_bgcolor(jobj.getString("sm_bg_color"));
                    toadysgameModel.setSm_bg_img(jobj.getString("sm_bg_image"));
                    toadysgameModel.setSm_bgsound(jobj.getString("sm_bg_sound"));
                    toadysgameModel.setSm_icon(jobj.getString("sm_icon"));
                    toadysgameModel.setSm_dc_bgcolor(jobj.getString("sm_decision_bg_color"));
                    toadysgameModel.setSm_dc_bgimg(jobj.getString("sm_decision_bg_image"));
                    toadysgameModel.setSm_company(jobj.getString("sm_company"));
                    toadysgameModel.setSm_ic_img(jobj.getString("sm_icon_image"));
                    toadysgameModel.setSm_desc(jobj.getString("sm_description"));
                    toadysgameModel.setSm_gif(jobj.getString("sm_bg_gif"));
                    toadysgameModel.setSm_giftime(jobj.getString("sm_gif_loading_time"));
                    toadysgameModel.setSm_titlecolor(jobj.getString("sm_title_color"));
                    toadysgameModel.setSm_title_desc_color(jobj.getString("sm_title_description_color"));
                    toadysgameModel.setUrl_ic("");


                    //new params
                    try{
                        funModel.setLg_id(String.valueOf(jobj.getInt("lg_id")));
                        toadysgameModel.setLg_id(String.valueOf(jobj.getInt("lg_id")));

                    }catch (Exception e){
                        e.printStackTrace();
                        funModel.setLg_id(String.valueOf(0));
                        toadysgameModel.setLg_id(String.valueOf(0));

                    }

                    if(jobj.getInt("lg_id")==0){
                        funModel.setStatus(3);
                        toadysgameModel.setStatus_id(3);
                        funModel.setExtra_param("obc");
                        toadysgameModel.setExtra_param("obc");
                    }else {
                        funModel.setStatus(2);
                        toadysgameModel.setStatus_id(2);
                        funModel.setExtra_param("ob");
                        toadysgameModel.setExtra_param("ob");
                    }


                    try{
                        funModel.setCourse_id(String.valueOf(jobj.getInt("course_id")));
                        toadysgameModel.setCourse_id(String.valueOf(jobj.getInt("course_id")));

                    }catch (Exception e){
                        e.printStackTrace();
                        funModel.setCourse_id(String.valueOf(0));
                        toadysgameModel.setCourse_id(String.valueOf(0));

                    }

                    try{
                        funModel.setCertification_id(String.valueOf(jobj.getInt("certification_id")));
                        toadysgameModel.setCertification_id(String.valueOf(jobj.getInt("certification_id")));

                    }catch (Exception e){
                        e.printStackTrace();
                        funModel.setCertification_id(String.valueOf(0));
                        toadysgameModel.setCertification_id(String.valueOf(0));

                    }

                    try{

                        funModel.setLearning_group_name(jobj.getString("learning_group_name"));
                        toadysgameModel.setLearning_group_name(jobj.getString("learning_group_name"));

                    }catch (Exception e){
                        e.printStackTrace();

                        funModel.setLearning_group_name("");
                        toadysgameModel.setLearning_group_name("");

                    }

                    try{

                        funModel.setCourse_name(jobj.getString("course_name"));
                        toadysgameModel.setCourse_name(jobj.getString("course_name"));

                    }catch (Exception e){
                        e.printStackTrace();

                        funModel.setCourse_name("");
                        toadysgameModel.setCourse_name("");

                    }

                    try{
                        funModel.setCertification_name(jobj.getString("certification_name"));
                        toadysgameModel.setCertification_name(jobj.getString("certification_name"));

                    }catch (Exception e){
                        e.printStackTrace();
                        funModel.setCertification_name("");
                        toadysgameModel.setCertification_name("");

                    }


                    if(jobj.getString("sm_waterfall_isvisible").equalsIgnoreCase("1")){
                        //   if(new PrefManager(getActivity()).getAsstype().equalsIgnoreCase("fun")){
                        waterfall_visibility=true;
                        // }

                    }
                    funModel.setSm_icon_img(jobj.getString("sm_icon_image"));

                    String lg_val;
                    try{
                        lg_val=String.valueOf(jobj.getInt("lg_id"));

                    }catch (Exception e){
                        e.printStackTrace();
                        lg_val="0";

                    }

                    if(!lg_val.equalsIgnoreCase("0")){

                    }else {

                        String assgroupid_sharedpref=new PrefManager(getActivity()).getAssgroupId();
                        String current_ce_id=String.valueOf(jobj.getString("CE_Id"));
                        if(assgroupid_sharedpref.equalsIgnoreCase(current_ce_id)) {
                            simulation_list_arraylist.add(funModel);
                            mainList.add(toadysgameModel);
                        }



                    }

                    todaysGameAdapter.notifyDataSetChanged();
                    Log.e("eror","forsim"+i);

                }


                if(waterfall_visibility){
                  //  displayWorldwaterFallGame();
                }
//                simulationAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //need
            today_game_list.getRecycledViewPool().clear();
            todaysGameAdapter.notifyDataSetChanged();


        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }


        try{
            swipeass.finishRefresh();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(responseCode!=200){
            // rl_left.setVisibility(View.GONE);
            //  rl_right.setVisibility(View.GONE);
          //  countavailablegame=0;
            Log.e("eror","!200");
            // simulation_list.setVisibility(View.INVISIBLE);
            no_ass.setVisibility(View.VISIBLE);

            swipeass.setVisibility(View.VISIBLE);
            // no_fun_games.setVisibility(View.VISIBLE);
        }else {

          /*  Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    return rhs.getCourse_id().compareTo(lhs.getCourse_id());
                }
            });*/
            Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    String r1=rhs.getExtra_param();
                    String r2=lhs.getExtra_param();
                    return r1.compareTo(r2);
                }
            });


            Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    String r1=rhs.getCourse_id();
                    String r2=lhs.getCourse_id();
                    return r1.compareTo(r2);
                }
            });

            /*Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    String r1=rhs.getName();
                    String r2=lhs.getName();
                    return r1.compareToIgnoreCase(r2);
                }
            });
*/

            Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    String r1=rhs.getExtra_param();
                    String r2=lhs.getExtra_param();
                    return r1.compareTo(r2);
                }
            });


            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                Extra_ids.add(i,toadysgameModel.getExtra_param());

            }

            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                list_lg_ids.add(i,toadysgameModel.getLg_id());

            }
            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                list_certi_ids.add(i,toadysgameModel.getCertification_id());

            }
            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                list_course_ids.add(i,toadysgameModel.getCourse_id());

            }
            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                if(toadysgameModel.getCourse_name().equalsIgnoreCase("null")){
                    course_name.add(i,"");
                }else {
                    course_name.add(i,toadysgameModel.getCourse_name());
                }


            }

            for(int i=0;i<mainList.size();i++){

                    desc_statuslist.add(i,"close");

            }





            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                if(toadysgameModel.getCertification_name().equalsIgnoreCase("null")){
                    certi_name.add(i,"");
                }else {
                    certi_name.add(i,toadysgameModel.getCertification_name());
                }

            }
            todaysGameAdapter.notifyDataSetChanged();
            //simulationAdapter.notifyDataSetChanged();
            String retry_str=" ";
            try {
                retry_str =new PrefManager(getActivity().getApplication()).getRetryresponseString();
            }
            catch (Exception e){
                retry_str = " ";
                e.printStackTrace();
            }

            if(retry_str.equalsIgnoreCase("notdone")){
                // new HomeActivity().uploadVideoResponse();
                ((HomeActivity)getActivity()).uploadVideoResponse();
            }



            Log.e("eror",""+mainList.size());

            new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    if(mainList.size()>0){
                        Log.e("eror",">0");
                        visibleelements();
                        today_game_list.setVisibility(View.VISIBLE);
                        no_ass.setVisibility(View.GONE);

                    }else {
                        Log.e("eror","excoccures");

                        exceptionOccurs();
                        swipeass.setVisibility(View.VISIBLE);
                   /* rl_right.setVisibility(View.GONE);
                    rl_left.setVisibility(View.GONE);*/
                        //  countavailablegame=0;
                        // simulation_list.setVisibility(View.INVISIBLE);
                        no_ass.setVisibility(View.VISIBLE);


                        //   no_fun_games.setVisibility(View.INVISIBLE);
                    }
                }
            }.start();
        }
      //  dialog_is.dismiss();
        todaysGameAdapter.notifyDataSetChanged();
        today_game_list.setClickable(true);
        today_game_list.setNestedScrollingEnabled(true);

        today_game_list.setVisibility(View.VISIBLE);

    }

    public void loadGamesLearning(String s){
        played_status_from_learning=0;
        isvalue=false;
        HomeActivity.apiloading=false;
        // swipeRefreshLayout.setDistanceToTriggerSync(0);

        try {
            jObj = new JSONObject(s);
            uid = jObj.getString("U_Id");
            game_tip_is=jObj.getString("Game_tip");

            data= (JSONArray)jObj.get("data");
            Log.e("adtaa",data.length()+"");
            for (int i = 0; i < data.length(); i++) {


                dataJSONobject = data.getJSONObject(i);
                GameModel gameModel=new GameModel();
                ToadysgameModel toadysgameModel=new ToadysgameModel();
                gameModel.setGdid(dataJSONobject.getString("GD_Id"));
                gameModel.setCeid(dataJSONobject.getString("CE_Id"));
                gameModel.setUid(jObj.getString("U_Id"));
                gameModel.setTgid(dataJSONobject.getString("TG_Id"));
                toadysgameModel.setTg_id(dataJSONobject.getString("TG_Id"));
                try{
                    toadysgameModel.setPic_frequency(dataJSONobject.getInt("CE_PICFrequency"));
                    gameModel.setPic_frequency(dataJSONobject.getInt("CE_PICFrequency"));

                }catch (Exception e){
                    toadysgameModel.setPic_frequency(0);
                    gameModel.setPic_frequency(0);

                    e.printStackTrace();
                }

                gameModel.setGame(dataJSONobject.getString("Game"));
                gameModel.setCategory(dataJSONobject.getString("Category"));
                toadysgameModel.setLock_status(dataJSONobject.getString("is_locked"));
                toadysgameModel.setAvailable_date(dataJSONobject.getString("CED_Date"));
                gameModel.setUrl_ic(dataJSONobject.getString("GD_Imgurl"));
                toadysgameModel.setUrl_ic(dataJSONobject.getString("GD_Imgurl"));
                toadysgameModel.setAss_uid(jObj.getString("U_Id"));
                toadysgameModel.setAss_gdid(dataJSONobject.getString("GD_Id"));
                toadysgameModel.setAss_category(dataJSONobject.getString("Category"));
                toadysgameModel.setAss_game(dataJSONobject.getString("Game"));
                toadysgameModel.setAss_groupid(dataJSONobject.getString("TG_GroupId"));
                toadysgameModel.setAss_group(dataJSONobject.getString("TG_Group"));
                toadysgameModel.setAss_gametime(dataJSONobject.getString("GD_Time"));
                toadysgameModel.setAss_descgame(dataJSONobject.getString("GD_Description"));
                toadysgameModel.setSm_landingpage_img(" ");
                toadysgameModel.setSm_bgcolor(" ");
                toadysgameModel.setSm_bg_img(" ");
                toadysgameModel.setSm_bgsound(" ");
                toadysgameModel.setSm_icon(" ");
                toadysgameModel.setSm_dc_bgcolor(" ");
                toadysgameModel.setSm_dc_bgimg(" ");
                toadysgameModel.setSm_company(" ");
                toadysgameModel.setSm_ic_img(" ");
                toadysgameModel.setSm_desc(" ");
                toadysgameModel.setSm_gif(" ");
                toadysgameModel.setSm_giftime(" ");
                toadysgameModel.setSm_titlecolor(" ");
                toadysgameModel.setSm_title_desc_color(" ");

                //new params
                try{
                    gameModel.setLg_id(String.valueOf(dataJSONobject.getInt("lg_id")));
                    toadysgameModel.setLg_id(String.valueOf(dataJSONobject.getInt("lg_id")));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setLg_id(String.valueOf(0));
                    toadysgameModel.setLg_id(String.valueOf(0));

                }

                try{
                    gameModel.setCourse_id(String.valueOf(dataJSONobject.getInt("course_id")));
                    toadysgameModel.setCourse_id(String.valueOf(dataJSONobject.getInt("course_id")));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setCourse_id(String.valueOf(0));
                    toadysgameModel.setCourse_id(String.valueOf(0));

                }

                try{
                    gameModel.setCertification_id(String.valueOf(dataJSONobject.getInt("certification_id")));
                    toadysgameModel.setCertification_id(String.valueOf(dataJSONobject.getInt("certification_id")));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setCertification_id(String.valueOf(0));
                    toadysgameModel.setCertification_id(String.valueOf(0));

                }

                try{

                    gameModel.setLearning_group_name(dataJSONobject.getString("learning_group_name"));
                    toadysgameModel.setLearning_group_name(dataJSONobject.getString("learning_group_name"));

                }catch (Exception e){
                    e.printStackTrace();

                    gameModel.setLearning_group_name("");
                    toadysgameModel.setLearning_group_name("");

                }

                try{

                    gameModel.setCourse_name(dataJSONobject.getString("course_name"));
                    toadysgameModel.setCourse_name(dataJSONobject.getString("course_name"));

                }catch (Exception e){
                    e.printStackTrace();

                    gameModel.setCourse_name("");
                    toadysgameModel.setCourse_name("");

                }

                try{
                    gameModel.setCertification_name(dataJSONobject.getString("certification_name"));
                    toadysgameModel.setCertification_name(dataJSONobject.getString("certification_name"));

                }catch (Exception e){
                    e.printStackTrace();
                    gameModel.setCertification_name("");
                    toadysgameModel.setCertification_name("");

                }


                gameModel.setDisplayname(dataJSONobject.getString("Display_Name"));
                toadysgameModel.setName(dataJSONobject.getString("Display_Name"));
                gameModel.setStatus(dataJSONobject.getString("TG_Status"));
                toadysgameModel.setStatus(dataJSONobject.getString("TG_Status"));
                toadysgameModel.setType_is("assessment");
                toadysgameModel.setIdis(dataJSONobject.getString("CE_Id"));


                int val;
                try {
                    val = dataJSONobject.getInt("lg_id");
                    Log.e("lgid",""+val);
                }catch (Exception e){
                    e.printStackTrace();
                    val=0;
                    Log.e("lgid",""+val);

                }
                if(val==0){
                    if(!dataJSONobject.getString("TG_Status").equalsIgnoreCase("Yet to Play")){
                        countavailablegame++;
                        gameModel.setTemp_status("played");
                        toadysgameModel.setTemp_status("played");
                        gameModel.setStatus_id(1);
                        toadysgameModel.setStatus_id(1);
                        gameModel.setExtra_param("o");
                        toadysgameModel.setExtra_param("o");
                    }else {
                        gameModel.setStatus_id(3);
                        toadysgameModel.setStatus_id(3);
                        gameModel.setExtra_param("obc");
                        toadysgameModel.setExtra_param("obc");
                        gameModel.setTemp_status("yet to play");
                        toadysgameModel.setTemp_status("yet to play");
                    }
                }else {
                    gameModel.setStatus_id(2);
                    toadysgameModel.setStatus_id(2);
                    gameModel.setExtra_param("ob");
                    toadysgameModel.setExtra_param("ob");
                    gameModel.setTemp_status("yet play");
                    toadysgameModel.setTemp_status("yet play");
                }


                gameModel.setGroupid(dataJSONobject.getString("TG_GroupId"));
                gameModel.setGroup(dataJSONobject.getString("TG_Group"));
                toadysgameModel.setSubname(dataJSONobject.getString("TG_Group"));
                HomeActivity.tg_group=dataJSONobject.getString("TG_Group");
                gameModel.setCe_picavailable(dataJSONobject.getString("CE_TakeProfilePIC"));
                try{
                    gameModel.setCe_pic_frequency(dataJSONobject.getString("CE_PICFrequency"));

                }catch (Exception e){
                    gameModel.setCe_pic_frequency("0");

                    e.printStackTrace();
                }

                gameModel.setCe_status(dataJSONobject.getString("CE_Status"));
                gameModel.setGame_time(dataJSONobject.getString("GD_Time"));
                gameModel.setDescription_game(dataJSONobject.getString("GD_Description"));

                String t_ce_id=" ";
                try {
                    t_ce_id=new PrefManager(getActivity()).getTargetid();
                }
                catch (Exception e){
                    t_ce_id = " ";
                    e.printStackTrace();
                }


                if(t_ce_id.equalsIgnoreCase(String.valueOf(dataJSONobject.getString("CE_Id")))){



                    String course_id=" ";
                    try {
                        course_id=new PrefManager(getActivity()).getCourseid();
                    }
                    catch (Exception e){
                        course_id = " ";
                        e.printStackTrace();
                    }

                    if(course_id.equals((String.valueOf(dataJSONobject.getInt("course_id"))))) {

                        String certi_main,certi_pref;

                        try{
                            certi_main=String.valueOf(dataJSONobject.getInt("certification_id"));
                        }catch (Exception e){
                            e.printStackTrace();
                           certi_main="0";

                        }
                        try{
                            certi_pref=new PrefManager(getActivity()).getcertiid();
                        }catch (Exception e){
                            e.printStackTrace();
                            certi_pref="0";

                        }

                        if(certi_main.equalsIgnoreCase(certi_pref)) {
                            if (!dataJSONobject.getString("TG_Status").equalsIgnoreCase("Yet to Play")) {
                                countavailablegame++;
                                played_status_from_learning++;
                            } else {


                            }
                            Log.e("valyous", "cont");
                            String lg_val;
                            try {
                                lg_val = String.valueOf(dataJSONobject.getInt("lg_id"));

                            } catch (Exception e) {
                                e.printStackTrace();
                                lg_val = "0";

                            }

                            if (lg_val.equalsIgnoreCase("0")) {

                            } else {
                                list_games.add(gameModel);
                                mainList.add(toadysgameModel);

                            }


                        }

                    }

                    }
                todaysGameAdapter.notifyDataSetChanged();
            }

            if(played_status_from_learning==list_games.size()){
                isvalue=true;
            }



/*
            try {
                JSONArray js=jObj.getJSONArray("sm_list");
                for(int i=0;i<js.length();i++){
                    JSONObject jobj=js.getJSONObject(i);
                    FunModel funModel=new FunModel();
                    ToadysgameModel toadysgameModel=new ToadysgameModel();
                    funModel.setSmname(jobj.getString("sm_name"));
                    toadysgameModel.setName(jobj.getString("sm_name"));

                    funModel.setLandingpage_img(jobj.getString("sm_landing_page_image"));
                    funModel.setBgcolor(jobj.getString("sm_bg_color"));
                    funModel.setBgimg(jobj.getString("sm_bg_image"));
                    funModel.setDc_bg_color(jobj.getString("sm_decision_bg_color"));
                    funModel.setDc_bg_img(jobj.getString("sm_decision_bg_image"));
                    funModel.setBgsound(jobj.getString("sm_bg_sound"));
                    funModel.setId(jobj.getString("sm_id"));
                    funModel.setSm_icon(jobj.getString("sm_icon"));
                    funModel.setCompanyname(jobj.getString("sm_company"));
                    toadysgameModel.setSubname(jobj.getString("sm_company"));
                    toadysgameModel.setStatus("Yet to Play");
                    toadysgameModel.setType_is("simulation");
                    toadysgameModel.setIdis(jobj.getString("sm_id"));
                    funModel.setGif(jobj.getString("sm_bg_gif"));
                    funModel.setDesc(jobj.getString("sm_description"));
                    funModel.setGif_load_time(jobj.getString("sm_gif_loading_time"));
                    funModel.setTitlecolor(jobj.getString("sm_title_color"));
                    funModel.setTitledescriptioncolor(jobj.getString("sm_title_description_color"));


                    if(jobj.getString("sm_waterfall_isvisible").equalsIgnoreCase("1")){
                        //   if(new PrefManager(getActivity()).getAsstype().equalsIgnoreCase("fun")){
                        waterfall_visibility=true;
                        // }

                    }
                    funModel.setSm_icon_img(jobj.getString("sm_icon_image"));
                    simulation_list_arraylist.add(funModel);
                    mainList.add(toadysgameModel);
                    todaysGameAdapter.notifyDataSetChanged();
                }


                if(waterfall_visibility){
                    displayWorldwaterFallGame();
                }
//                simulationAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            //need
            today_game_list.getRecycledViewPool().clear();
            todaysGameAdapter.notifyDataSetChanged();


        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }


        try{
            swipeass.finishRefresh();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(responseCode!=200){
            // rl_left.setVisibility(View.GONE);
            //  rl_right.setVisibility(View.GONE);
            //  countavailablegame=0;
            // simulation_list.setVisibility(View.INVISIBLE);
            today_game_list.setVisibility(View.INVISIBLE);
            swipeass.setVisibility(View.VISIBLE);
            // no_fun_games.setVisibility(View.VISIBLE);
        }else {
            if(mainList.size()>0){
                visibleelements();
                today_game_list.setVisibility(View.VISIBLE);
                no_ass.setVisibility(View.GONE);

            }else {
                exceptionOccurs();
                swipeass.setVisibility(View.VISIBLE);
                   /* rl_right.setVisibility(View.GONE);
                    rl_left.setVisibility(View.GONE);*/
                //  countavailablegame=0;
                // simulation_list.setVisibility(View.INVISIBLE);
                no_ass.setVisibility(View.VISIBLE);

                //   no_fun_games.setVisibility(View.INVISIBLE);
            }


            Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    String r1=rhs.getExtra_param();
                    String r2=lhs.getExtra_param();
                    return r1.compareTo(r2);
                }
            });

            Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    String r1=rhs.getCourse_id();
                    String r2=lhs.getCourse_id();
                    return r1.compareTo(r2);
                }
            });

            Collections.sort(mainList, new Comparator<ToadysgameModel>() {
                @Override
                public int compare(ToadysgameModel lhs, ToadysgameModel rhs) {
                    String r1=rhs.getExtra_param();
                    String r2=lhs.getExtra_param();
                    return r1.compareTo(r2);
                }
            });

            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                Extra_ids.add(i,toadysgameModel.getExtra_param());

            }

            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                list_lg_ids.add(i,toadysgameModel.getLg_id());

            }
            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                list_certi_ids.add(i,toadysgameModel.getCertification_id());

            }
            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                list_course_ids.add(i,toadysgameModel.getCourse_id());

            }
            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                if(toadysgameModel.getCourse_name().equalsIgnoreCase("null")){
                    course_name.add(i,"");
                }else {
                    course_name.add(i,toadysgameModel.getCourse_name());
                }


            }

            for(int i=0;i<mainList.size();i++){

                desc_statuslist.add(i,"close");

            }

            for(int i=0;i<mainList.size();i++){
                ToadysgameModel toadysgameModel=mainList.get(i);
                if(toadysgameModel.getCertification_name().equalsIgnoreCase("null")){
                    certi_name.add(i,"");
                }else {
                    certi_name.add(i,toadysgameModel.getCertification_name());
                }

            }
            todaysGameAdapter.notifyDataSetChanged();
            //simulationAdapter.notifyDataSetChanged();

            String retry_str=" ";
            try {
                retry_str=new PrefManager(getActivity().getApplication()).getRetryresponseString();
            }
            catch (Exception e){
                retry_str = " ";
                e.printStackTrace();
            }
            if(retry_str.equalsIgnoreCase("notdone")){
                // new HomeActivity().uploadVideoResponse();
                ((HomeActivity)getActivity()).uploadVideoResponse();
            }
        }
        //  dialog_is.dismiss();
        today_game_list.setClickable(true);
        today_game_list.setNestedScrollingEnabled(true);

        today_game_list.setVisibility(View.VISIBLE);



        if(isvalue){
            Log.e("humvalis","true");
            //assessments are not  available
            new SendLessonStartProgress().execute();

        }else {
            //assessments are available

        }
    }



    public  class SendLessonStartProgress extends AsyncTask<Integer, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        ///Authorization
        @Override
        protected String doInBackground(Integer... urlkk) {
            String result1 = "", cand_id=" ",lg_id=" ",cert_id=" ";
            int course_id = 0,order_id= 0,inner_module_id=0;
            try {

                try{
                    cand_id = PreferenceUtils.getCandidateId(getActivity());
                    course_id = Integer.valueOf(new PrefManager(getActivity()).getCourseid());
                    order_id =Integer.valueOf(new PrefManager(getActivity()).getorderid());
                    inner_module_id = Integer.valueOf(new PrefManager(getActivity()).getinnermoduleid());
                    lg_id = new PrefManager(getActivity()).getlgid();
                    cert_id = new PrefManager(getActivity()).getcertiid();
                }
                catch (Exception e){
                    cand_id = " ";
                    course_id = 0;
                    inner_module_id = 0;
                    order_id =0;
                    lg_id = " ";
                    cert_id = " ";
                    e.printStackTrace();
                }

                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", cand_id)
                        .object("course_id",course_id )
                        .object("lesson_id", 0)
                        .object("assessment_id",inner_module_id)
                        .object("order_id", order_id)
                        .object("status", "completed")
                        .object("lg_id",lg_id)
                        .object("certification_id",cert_id)



                        .build().toString();

                try {
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_update_user_progress");
                    urlConnection2 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection2.setDoOutput(true);
                    urlConnection2.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection2.setRequestProperty("Content-Type", "application/json");
                    urlConnection2.setRequestProperty("Authorization", "Bearer " + HomeActivity.token);

                    urlConnection2.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection2.getOutputStream());
                    wr.write(jn);
                    Log.e("resp_data",jn);
                    wr.flush();
                    is = urlConnection2.getInputStream();
                    code2 = urlConnection2.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                } catch (ClientProtocolException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                if (code2 == 200) {

                    String responseString = readStreamresp(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection2 != null)
                    urlConnection2.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {
                Log.e("myvalue","postans 200");
            }
            else{
                Log.e("myvalue","postans not 200");

            }
        }
    }
    private static String readStreamresp(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }


    public void showDialogue(){
        dialog_is=new Dialog(getActivity());
        dialog_is.setContentView(R.layout.loading_dialogue_overlay);
        dialog_is.setCancelable(false);
        try {
            dialog_is.show();
        }catch (Exception e){

        }


    }


    public static class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    private class GetSImulation extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="", auth_token = " ";

            try {

                try{
                    auth_token = new PrefManager(getActivity()).getAuthToken();
                }catch (Exception e){
                    auth_token = " ";
                    e.printStackTrace();
                }

                url = new URL(params[0]);
                ref_id = params[1];
                Log.v("URL", "URL: " + url);
                Log.v("postParameters",ref_id);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Authorization", "Bearer " +HomeActivity.token);
                urlConnection.setRequestProperty("auth_token", auth_token);
                urlConnection.setRequestMethod("POST");
                /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();

                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.e("SMResponse", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                swipeass.finishRefresh();

                                // assess_progress.dismiss();
                                exceptionOccurs();
                            } catch (Exception e) {

                            }
                        }
                    });
                }catch (Exception ex){
                    e.printStackTrace();
                }
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            try{
                swipeass.finishRefresh();

                // assess_progress.dismiss();
            }catch (Exception e){

            }
         //   swipeRefreshLayout.setRefreshing(false);

            try {
                JSONArray js=new JSONArray(s);
                for(int i=0;i<js.length();i++){
                    JSONObject jobj=js.getJSONObject(i);
                    FunModel funModel=new FunModel();
                    funModel.setSmname(jobj.getString("sm_name"));
                    funModel.setLandingpage_img(jobj.getString("sm_landing_page_image"));
                    funModel.setBgcolor(jobj.getString("sm_bg_color"));
                    funModel.setBgimg(jobj.getString("sm_bg_image"));
                    funModel.setDc_bg_color(jobj.getString("sm_decision_bg_color"));
                    funModel.setDc_bg_img(jobj.getString("sm_decision_bg_image"));
                    funModel.setBgsound(jobj.getString("sm_bg_sound"));
                    funModel.setId(jobj.getString("sm_id"));
                    funModel.setSm_type(jobj.getString("sm_type"));
                    funModel.setSm_articulate_url(jobj.getString("sm_articulate_url"));
                    funModel.setSm_icon(jobj.getString("sm_icon"));
                    funModel.setCompanyname(jobj.getString("sm_company"));
                    funModel.setGif(jobj.getString("sm_bg_gif"));
                    funModel.setDesc(jobj.getString("sm_description"));
                    funModel.setGif_load_time(jobj.getString("sm_gif_loading_time"));
                    funModel.setTitlecolor(jobj.getString("sm_title_color"));
                    funModel.setTitledescriptioncolor(jobj.getString("sm_title_description_color"));

                    if(jobj.getString("sm_waterfall_isvisible").equalsIgnoreCase("1")){
                     //   if(new PrefManager(getActivity()).getAsstype().equalsIgnoreCase("fun")){
                            waterfall_visibility=true;
                       // }

                    }
                    funModel.setSm_icon_img(jobj.getString("sm_icon_image"));
                    simulation_list_arraylist.add(funModel);
                }
                //simulationAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String ass_type = " ";

            try{
                ass_type = new PrefManager(getActivity()).getAsstype();
            }
            catch (Exception e){
                ass_type = " ";
                e.printStackTrace();
            }
            if(ass_type.equalsIgnoreCase("fun")){
                if(waterfall_visibility){
                    //getWorldwaterFallGame();
                   visibleelements();
                    waterfall_visibility=false;
                }else {
                  //  funGamerecyclerList.setVisibility(View.GONE);
                    no_games.setVisibility(View.VISIBLE);
                    waterfall_visibility=false;
                }
            }else {
                if(simulation_list_arraylist.size()>0){
                visibleelements();
                }else {
                    exceptionOccurs();
                }
            }


        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }

    private void getWorldwaterFallGame() {
        FunModel funModel=new FunModel();
        funModel.setSmname("World Waterfall");
        funModel.setLandingpage_img("");
        funModel.setBgcolor("");
        funModel.setBgimg("");
        funModel.setDc_bg_color("");
        funModel.setDc_bg_img("");
        funModel.setBgsound("");
        funModel.setId("");
        funModel.setSm_icon("");
        funModel.setCompanyname("Fun game");
        funModel.setSm_icon_img("");
        funModel.setGif("");
        funModel.setDesc("");
        funModel.setGif_load_time("");
        funModel.setTitlecolor("");
        funModel.setTitledescriptioncolor("");

        simulation_list_arraylist.add(funModel);


    }


    private void displayWorldwaterFallGame() {
        FunModel funModel=new FunModel();
        funModel.setSmname("World Waterfall");
        funModel.setLandingpage_img("");
        funModel.setBgcolor("");
        funModel.setBgimg("");
        funModel.setDc_bg_color("");
        funModel.setDc_bg_img("");
        funModel.setBgsound("");
        funModel.setId("");
        funModel.setSm_icon("");
        funModel.setCompanyname("Fun game");
        funModel.setSm_icon_img("");
        funModel.setGif("");
        funModel.setDesc("");
        funModel.setGif_load_time("");
        funModel.setTitlecolor("");
        funModel.setTitledescriptioncolor("");

        simulation_list_arraylist.add(funModel);

        ToadysgameModel toadysgameModel=new ToadysgameModel();
        toadysgameModel.setName("Word Waterfall");
        toadysgameModel.setSubname("Fun Game");
        toadysgameModel.setStatus("Yet to Play");
        toadysgameModel.setType_is("fungame");

        mainList.add(toadysgameModel);

        todaysGameAdapter.notifyDataSetChanged();
    }



    public void refreshList(){
      //  showDialogue();

        if(connectionUtils.isConnectionAvailable()){
            String retry_str=" ";
            try {
                retry_str=new PrefManager(getActivity()).getRetryresponseString();
            }
            catch (Exception e){
                retry_str = " ";
                e.printStackTrace();
            }
            if(retry_str.equalsIgnoreCase("notdone")) {
                Toast.makeText(getActivity(), "Video uploading is in progrss,Can't access latest notifications now", Toast.LENGTH_LONG).show();
              //  dialog_is.dismiss();
              //  swipeRefreshLayout.setRefreshing(false);
            }else {
                 //   swipeRefreshLayout.setRefreshing(false);

                String type_ass=" ";

                try {
                    type_ass = new PrefManager(getActivity()).getAsstype();
                }catch (Exception e){
                    type_ass = " ";
                    e.printStackTrace();
                }
                if(type_ass.equalsIgnoreCase("Ass")){
                    ass.setVisibility(View.VISIBLE);
//                    game.setVisibility(View.GONE);
//                    sim.setVisibility(View.GONE);
                    new GetContacts().execute();

                }
                if(type_ass.equalsIgnoreCase("sim")){


                    ass.setVisibility(View.GONE);
//                    game.setVisibility(View.GONE);
//                    sim.setVisibility(View.VISIBLE);
                    new GetContacts().execute();

                }
                if(type_ass.equalsIgnoreCase("fun")){

                    ass.setVisibility(View.GONE);
//                    game.setVisibility(View.VISIBLE);
//                    sim.setVisibility(View.GONE);
                    new GetContacts().execute();

                }


            }
        }else {
           // dialog_is.dismiss();
          //  swipeRefreshLayout.setRefreshing(false);
             Toast.makeText(getActivity(), "please check your internet connection !", Toast.LENGTH_SHORT).show();
        }
    }


    public void exceptionOccurs(){

        String type_ass=" ";

        try {
            type_ass = new PrefManager(getActivity()).getAsstype();
        }
        catch (Exception e){
            type_ass = " ";
            e.printStackTrace();
        }
        if(type_ass.equalsIgnoreCase("Ass")){
            no_ass.setVisibility(View.VISIBLE);

        }
        if(type_ass.equalsIgnoreCase("sim")){
//            no_sim.setVisibility(View.VISIBLE);
//            simulation_list.setVisibility(View.GONE);

        }
        if(type_ass.equalsIgnoreCase("fun")){

//            no_games.setVisibility(View.VISIBLE);
//            funGamerecyclerList.setVisibility(View.GONE);


        }

    }



    public void visibleelements(){
        String type_ass=" ";
        try {
       type_ass =new PrefManager(getActivity()).getAsstype();
        }
        catch (Exception e){
            type_ass = " ";
            e.printStackTrace();
        }

        if(type_ass.equalsIgnoreCase("Ass")){
            ass.setVisibility(View.VISIBLE);
//            game.setVisibility(View.GONE);
//            sim.setVisibility(View.GONE);
            no_ass.setVisibility(View.GONE);
            today_game_list.setVisibility(View.VISIBLE);
          //  new GetContacts().execute();

        }
        if(type_ass.equalsIgnoreCase("sim")){

            //simulation_list.setVisibility(View.VISIBLE);
            ass.setVisibility(View.GONE);
//            game.setVisibility(View.GONE);
//            sim.setVisibility(View.VISIBLE);
          //  new GetContacts().execute();

        }
        if(type_ass.equalsIgnoreCase("fun")){

            ass.setVisibility(View.GONE);
//            game.setVisibility(View.VISIBLE);
//            sim.setVisibility(View.GONE);
        //    funGamerecyclerList.setVisibility(View.VISIBLE);
           // new GetContacts().execute();

        }

    }



}
