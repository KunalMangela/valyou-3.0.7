package com.globalgyan.getvalyou.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.globalgyan.getvalyou.LeaderboardAdapter;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class Leaderboard extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private DataBaseHelper dataBaseHelper = null;
    PrefManager prefManager;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ProgressDialog dialog;
    Bitmap bitmap;
    RecyclerView recycler_list;
    LeaderboardAdapter leaderboardAdapter;
    View view;
    ImageView back_btn;
    CircleImageView circleImageView;
    public Leaderboard() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Leaderboard.
     */
    // TODO: Rename and change types and number of parameters
    public static Leaderboard newInstance(String param1, String param2) {
        Leaderboard fragment = new Leaderboard();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=  inflater.inflate(R.layout.fragment_leaderboard,container,false);
        try {
            if (prefManager.getPic() == " ") {

            } else {
                byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                circleImageView.setImageBitmap(decodedByte);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recycler_list=(RecyclerView)view.findViewById(R.id.user_list);
        back_btn=(ImageView)view.findViewById(R.id.bckbtn);
        circleImageView=(CircleImageView)view.findViewById(R.id.profile_image);
        List<String> user=new ArrayList<>();
        user.add(0,"kunal");
        user.add(1,"omkar");
        user.add(2,"cr7");
        user.add(3,"messi");
        user.add(4,"greizmann");
        user.add(5,"roberto");
        user.add(6,"persi");
        user.add(7,"aguero");
        user.add(8,"keylor");
        user.add(9,"me");

        List<String> points=new ArrayList<>();
        points.add(0,"1000");
        points.add(1,"900");
        points.add(2,"800");
        points.add(3,"700");
        points.add(4,"650");
        points.add(5,"500");
        points.add(6,"400");
        points.add(7,"300");
        points.add(8,"200");
        points.add(9,"100");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_list.setLayoutManager(linearLayoutManager);
        leaderboardAdapter=new LeaderboardAdapter(getActivity(),user,points);
        recycler_list.setAdapter(leaderboardAdapter);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        dataBaseHelper = new DataBaseHelper(getActivity());

        prefManager = new PrefManager(getActivity());
        getpic();

    }

    private void getpic() {
        String profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
        prefManager = new PrefManager(getActivity());
        if(prefManager.getPic()==" "){
            if(profileImageUrl==null){
                Log.e("lnowndnwpmdpw","lfnpnfpnnpewnpfpnfpnfpewnfewpffpnfpewnfpwnfnfp"+profileImageUrl);
                loadImag();
                // new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);

                // loadImage(profileImageUrl);
            }else{
              /*  final ProgressDialog progressDialog=new ProgressDialog(ProfileActivity.this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setTitle("Please wait...");
                progressDialog.show();
                String url="http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl;
                ImageRequest imgRequest = new ImageRequest(url,
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {
                                userimage.setImageBitmap(response);
                                progressDialog.cancel();
                            }
                        }, 0, 0, ImageView.ScaleType.FIT_XY, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        userimage.setBackgroundColor(Color.parseColor("#ff0000"));
                        error.printStackTrace();
                        progressDialog.cancel();
                    }
                });

                Volley.newRequestQueue(this).add(imgRequest);
              */
                if(new PrefManager(getActivity()).getRetryresponseString().equalsIgnoreCase("notdone")){

                }else {
                    new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/" + profileImageUrl);
                    //   new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/59fc25b66217bda954ad8dad");
                }
            }
        }else{
            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            circleImageView.setImageBitmap(decodedByte);
        }

    }

    public void loadImag(){
        Picasso.with(getActivity())
                .load("https://admin.getvalyou.com/api/candidateMobileReadImage/59e9869961b7db216dac6d08")
                .placeholder(R.drawable.onbpic)   // optional
                .resize(500,500)                        // optional
                .into(circleImageView);
    }
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Please wait...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];


            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            result.compress(Bitmap.CompressFormat.PNG, 0, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            Log.e("ENCID",encodedImage);
            prefManager.savePic(encodedImage);
            Log.e("EN",prefManager.getPic());
            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            circleImageView.setImageBitmap(decodedByte);
            if(dialog.isShowing()){

                dialog.dismiss();
            }
            // dialog.cancel();
        }
    }

}
