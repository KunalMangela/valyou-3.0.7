package com.globalgyan.getvalyou.fragments;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.WhatsNewAdapter;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 23/09/17.
 */

public class FirstTime extends Fragment {
    private static View view;
    String candidateId;

    ArrayList<String> agrup = new ArrayList<>();
    ArrayList<String> glist = new ArrayList<>();
    ArrayList<String> clist = new ArrayList<>();
    ArrayList<String> ilist = new ArrayList<>();

    DefaultHttpClient httpClient;
    HttpURLConnection urlConnection;
    HttpPost httpPost;
    HttpGet httpGet;
    FontTextView user;
    int cqa=0;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null,jObjeya=null;
    JSONArray data;
    JSONObject dataJSONobject;
    static String json = "",jsoneya="",jso="",Qus="",token="",token_type="", emaileya="",dn="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();

    RecyclerView recyclerGen;
    WhatsNewAdapter wadapter;

    public static FirstTime newInstance() {

        return  new FirstTime();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(cqa==0){
            view = inflater.inflate(R.layout.frag_whatsnew, container, false);
            candidateId = PreferenceUtils.getCandidateId(getActivity());
            cqa++;
        }else{
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new Performance())
                    .commit();
        }


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        user=(FontTextView)view.findViewById(R.id.user_wel);
        recyclerGen = (RecyclerView) view.findViewById(R.id.welrcv);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerGen.setLayoutManager(mLayoutManager);
        recyclerGen.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerGen.setItemAnimator(new DefaultItemAnimator());
        new GetContacts().execute();
    }

    private class GetContacts extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{
               // httpClient = new DefaultHttpClient();
               // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));


                URL urlToRequest = new URL("http://35.154.93.176/oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(params));

                writer.flush();
                writer.close();
                os.close();

                is = urlConnection.getInputStream();




                //httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
               // httpPost.setEntity(new UrlEncodedFormEntity(params));
               // HttpResponse httpResponse = httpClient.execute(httpPost);
               // HttpEntity httpEntity = httpResponse.getEntity();
               // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();


                Log.e("JSONWWWW", json);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            try {
                jObj = new JSONObject(json);
                token=jObj.getString("access_token");
                token_type=jObj.getString("token_type");
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new GetEywa().execute();

        }
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private class GetEywa extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {
            String result="",  responsestring = "";

            try {
             //   httpClient = new DefaultHttpClient();
                String url = "http://admin.getvalyou.com/api/getCandidateDetails/" + candidateId;
              //  httpGet = new HttpGet(url);


              //  httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
               // httpGet.setEntity(new UrlEncodedFormEntity(params));
               // HttpResponse httpResponse = httpClient.execute(httpGet);
               // HttpEntity httpEntity = httpResponse.getEntity();
               // is = httpEntity.getContent();
                URL obj = new URL(url);

                urlConnection = (HttpURLConnection) obj.openConnection();
                urlConnection.setRequestMethod("Get");



                responsestring =readStream1(urlConnection.getInputStream());
                result = responsestring;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
           /* try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                jsoneya = sb.toString();


                Log.e("JSONNOYU", jsoneya);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }*/
            try {
                jObjeya = new JSONObject(result);
                dn= jObjeya.getString("displayName");
                emaileya=jObjeya.getString("email");

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            user.setText("Hi "+dn+"!!");
            new GetContact().execute();
        }

    }

    private String readStream1(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            jsoneya = response.toString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsoneya;
    }
    private class GetContact extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 ="";
            try {


                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/TodayGames");*/

                    String jon = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_First", dn)
                            .object("email", emaileya)
                            .object("U_Last", "")
                            .object("U_Job", "")
                            .object("U_Other", "")
                            .object("U_Id", candidateId)
                            .build().toString();
                   /* StringEntity se = new StringEntity(jon.toString());
                    Log.e("Reqt",jon+"");
                    Log.e("Request",se+"");
                    httpPost.addHeader("Authorization","Bearer "+token);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    Log.e("FKJ",httpResponse+"CHARAN"+is);*/

                    URL urlToRequest = new URL("http://35.154.93.176/Player/TodayGames");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jon.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jon);
                    wr.flush();


                    String responseString = readStream(urlConnection.getInputStream());
                    Log.v("Response", responseString);
                    result1 = responseString;


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }



               /* try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    is.close();

                    jso = sb.toString();


                    Log.e("JSONStrr", jso);

                } catch (Exception e) {
                    e.getMessage();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }*/
                try {
                    jObj = new JSONObject(result1);

                    data= (JSONArray)jObj.get("data");
                    Log.e("adtaa",data.length()+"");
                    for (int i = 0; i < data.length(); i++) {
                        dataJSONobject = data.getJSONObject(i);
                        if(i==0){
                            agrup.add(dataJSONobject.getString("TG_Group"));
                        }else{
                            if(!agrup.contains(dataJSONobject.getString("TG_Group")))
                                agrup.add(dataJSONobject.getString("TG_Group"));

                        }
                    }
                    for (int i = 0; i < agrup.size(); i++) {
                        int k=0;
                        for (int j = 0; j < data.length(); j++) {
                            dataJSONobject = data.getJSONObject(j);
                            if(agrup.get(i).equals(dataJSONobject.getString("TG_Group"))){
                                k++;
                            }
                        }
                        glist.add(k+" Games");
                        Log.e("GAMES",glist.get(i));
                    }
                } catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            Log.e("WW",agrup.size()+"");
            Log.e("WWW",glist.size()+"");
            wadapter= new WhatsNewAdapter(getActivity(),agrup,glist);
            return null;

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            recyclerGen.setAdapter(wadapter);
        }
    }

    private String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            jso = sb.toString();


            Log.e("JSONStrr", jso);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return jso;
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
