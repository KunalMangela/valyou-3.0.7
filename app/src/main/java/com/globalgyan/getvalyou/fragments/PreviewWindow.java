package com.globalgyan.getvalyou.fragments;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.interfaces.VideoPerformanceUpdator;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

/**
 * Created by NaNi on 15/11/17.
 */

public class PreviewWindow extends Fragment {
    public static PreviewWindow newInstance() {
        return new PreviewWindow();
    }
    private View view = null;
    private VideoPerformanceUpdator videoPerformanceUpdator = null;
    LinearLayout llff;
    RelativeLayout previewwin;
    Button bckbtn,sub,pre,retry;
    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;
    View mBottomLayout;
    View mVideoLayout;
    private static final String TAG = PreviewWindow.class.getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView: PreviewWindow Called" );
        view = inflater.inflate(R.layout.video_pre, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        mVideoLayout = view.findViewById(R.id.video_layout);
//        mBottomLayout = view.findViewById(R.id.bottom_layout);
        llff=(LinearLayout)view.findViewById(R.id.footernkl);
        previewwin=(RelativeLayout)view.findViewById(R.id.previewvideo);
        bckbtn=(Button)view.findViewById(R.id.backbtn);
        sub=(Button)view.findViewById(R.id.submitvr);
        pre=(Button)view.findViewById(R.id.preview);
        retry=(Button)view.findViewById(R.id.recordagain);
        videoPerformanceUpdator = (VideoPerformanceUpdator) getActivity();
        mVideoView = (UniversalVideoView) view.findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) view.findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);

        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
            @Override
            public void onScaleChange(boolean isFullscreen) {
                // this.isFullscreen = isFullscreen;
                if (isFullscreen) {
//                    ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                    layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                    mVideoLayout.setLayoutParams(layoutParams);
//                    //GONE the unconcerned views to leave room for video and controller
//                    mBottomLayout.setVisibility(View.GONE);
                } else {
//                    ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                    layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                    mVideoLayout.setLayoutParams(layoutParams);
//                    mBottomLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPause(MediaPlayer mediaPlayer) { // Video pause
                Log.d("LIBAR", "onPause UniversalVideoView callback");
            }

            @Override
            public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                Log.d("LIBBB", "onStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                Log.d("OPN", "onBufferingStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                Log.d("fobfp", "onBufferingEnd UniversalVideoView callback");
                llff.setVisibility(View.VISIBLE);
                mMediaController.setVisibility(View.GONE);
            }

        });

        mVideoView.setVideoPath("/storage/emulated/0/Movies/valYou/0.mp4");
//            videoView.setClickable(false);
//            videoView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    previ.performClick();
//                }
//            });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.d("fONNONONNONNONONONONO", "onBufferingEnd UniversalVideoView callback");
                llff.setVisibility(View.VISIBLE);
                previewwin.setVisibility(View.GONE);
               // cover.setVisibility(View.GONE);
                bckbtn.setVisibility(View.GONE);
                mMediaController.setVisibility(View.GONE);
                mVideoView.setFullscreen(false);
            }
        });
        bckbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llff.setVisibility(View.VISIBLE);
                previewwin.setVisibility(View.GONE);
             //   cover.setVisibility(View.GONE);
                mVideoView.pause();
                bckbtn.setVisibility(View.GONE);
                mMediaController.setVisibility(View.GONE);
            }
        });
        pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("NEOSNDNS","fofb");

                llff.setVisibility(View.GONE);
               // cover.setVisibility(View.VISIBLE);
                previewwin.setVisibility(View.VISIBLE);
                bckbtn.setVisibility(View.VISIBLE);
                mVideoView.setFullscreen(false);
                mVideoView.seekTo(0);
                mVideoView.start();
                mVideoView.requestFocus();
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoPerformanceUpdator.findis();
            }
        });

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoPerformanceUpdator.startRecordingPageAgain();
            }
        });
    }
}
