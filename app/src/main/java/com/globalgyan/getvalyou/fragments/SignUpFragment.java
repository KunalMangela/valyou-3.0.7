package com.globalgyan.getvalyou.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.Activity_Myself;
import com.globalgyan.getvalyou.EnterOtpActivity;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.LoginSignupActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.SocialsignupActivity;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.response.ForgetPasswordResponse;
import com.globalgyan.getvalyou.cms.response.LoginResponse;
import com.globalgyan.getvalyou.cms.response.SendOtpResponse;
import com.globalgyan.getvalyou.cms.response.SocialLoginResponse;
import com.globalgyan.getvalyou.cms.response.SocialRegisterResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Dates;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.LoginModel;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import im.delight.android.location.SimpleLocation;

import static com.globalgyan.getvalyou.Welcome.mViewPager;
import static com.globalgyan.getvalyou.Welcome.myPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment implements GUICallback,ActivityCompat.OnRequestPermissionsResultCallback {

    private ViewGroup rootLayout;
    TextView login_tv, signup_tv, forgotpassis;
    FormEditText email, password, emailS, passwordS, phone, username;
    boolean checkpas = false, checkphone = false;
    String type, currentscreen = "loginscreen";
    ImageView logincard, signupcard,eye;
    boolean ispassvisible = false, ispassvisiblelogin = false;
    //login
    ImageView pass_toggle_signup, pass_toggle_login;
    private static final int RC_SIGN_IN = 007;
    private String providerTypeStr = "";
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    private SimpleLocation location;
    //  KProgressHUD login_progrss, signup_progress;

    Dialog signup_progress;
    ConnectionUtils connectionUtils;
    //signup
    boolean ispause = false;
    AppConstant appConstant;
    boolean flag_netcheck = false;
    RelativeLayout loginlayout, signuplayout;
    Animation rightanim, leftanim, rightanim2, leftanim2;
    ImageView img_logo;
    Context context;
    View v;
    buttonClick2 click;

    public interface buttonClick2 {
        void buttonClicked2(View v);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        click = (buttonClick2) activity;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_sign_up, container, false);
        // mViewPager.setBackgroundResource(R.drawable.big_intro_bg);


        new PrefManager(getActivity()).setFirstTimeLaunch(false);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        emailS = (FormEditText) v.findViewById(R.id.email_singup);
        passwordS = (FormEditText) v.findViewById(R.id.password_singup);
        phone = (FormEditText) v.findViewById(R.id.phone);
        username = (FormEditText) v.findViewById(R.id.username);
        signup_tv=(TextView)v.findViewById(R.id.singup_big_tv);
        logincard=(ImageView)v.findViewById(R.id.login_button);
        eye=(ImageView)v.findViewById(R.id.eye);

        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.otf");

        emailS.setTypeface(typeface1);
        passwordS.setTypeface(typeface1);
        phone.setTypeface(typeface1);
        username.setTypeface(typeface1);
        signup_tv.setTypeface(typeface1);

        connectionUtils=new ConnectionUtils(getActivity());

//        signup_progress = KProgressHUD.create(getActivity())
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);


        signup_progress = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        signup_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        signup_progress.setContentView(R.layout.planet_loader);
        Window window2 = signup_progress.getWindow();
        WindowManager.LayoutParams wlp2 = window2.getAttributes();
        wlp2.gravity = Gravity.CENTER;
        wlp2.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window2.setAttributes(wlp2);
        signup_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        signup_progress.setCancelable(false);
        GifView gifView3 = (GifView) signup_progress.findViewById(R.id.loader);
        gifView3.setVisibility(View.VISIBLE);
        gifView3.play();
        gifView3.setGifResource(R.raw.loader_planet);
        gifView3.getGifResource();


        logincard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logincard.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        logincard.setEnabled(true);
                    }
                }.start();
                click.buttonClicked2(view);

            }
        });

      /*  passwordS.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
//                showkeyboard(view);
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (passwordS.getText().length() > 0) {
                        if (event.getRawX() >= (passwordS.getRight())) {
                            float x = event.getX();
                            float y = event.getY();
                            if (event.getRawX() >750|| (x>320&&x<400)) {
                                Log.e("touch point", ""+event.getRawX());

                                hideKeyboard();
                                if (ispassvisible) {
                                    ispassvisible = false;
                                    passwordS.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visi, 0);
                                    passwordS.setTransformationMethod(new PasswordTransformationMethod());
                                    passwordS.setSelection(passwordS.getText().length());
                                } else {
                                    ispassvisible = true;
                                    passwordS.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisi, 0);
                                    passwordS.setTransformationMethod(null);
                                    passwordS.setSelection(passwordS.getText().length());
                                }
                                // your action here
                                Log.e("pass", "clicked");
                                return true;
                            } else {
                                InputMethodManager imm = (InputMethodManager)
                                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.showSoftInput(passwordS,
                                        InputMethodManager.SHOW_IMPLICIT);



                               *//*                         InputMethodManager keyboard = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            keyboard.showSoftInput(passwordS, 0);*//*
                            }
                        } else {
                            InputMethodManager imm = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(passwordS,
                                    InputMethodManager.SHOW_IMPLICIT);

                               *//*                         InputMethodManager keyboard = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            keyboard.showSoftInput(passwordS, 0);*//*
                        }
                    }
                }else {
                    // showkeyboard(view);
                }
                return false;
            }
        });
        */
        eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordS.getText().length() > 0) {



                            hideKeyboard();
                            if (ispassvisible) {
                                ispassvisible = false;
                                passwordS.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_visi, 0);
                                passwordS.setTransformationMethod(new PasswordTransformationMethod());
                                passwordS.setSelection(passwordS.getText().length());
                            } else {
                                ispassvisible = true;
                                passwordS.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pass_invisi, 0);
                                passwordS.setTransformationMethod(null);
                                passwordS.setSelection(passwordS.getText().length());
                            }
                            // your action here
                            Log.e("pass", "clicked");

                        } else {
                            InputMethodManager imm = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(passwordS,
                                    InputMethodManager.SHOW_IMPLICIT);



                               /*                         InputMethodManager keyboard = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            keyboard.showSoftInput(passwordS, 0);*/
                        }
            }
        });

        signup_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                signup_tv.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        signup_tv.setEnabled(true);
                    }
                }.start();

                type="signup";

                Log .e("email",emailS.getText().toString());

                Log .e("username",username.getText().toString());

                Log .e("passis",passwordS.getText().toString());
                Log .e("phone",phone.getText().toString());


                checkpas=false;
                checkphone=false;
                FormEditText[] allFields	= { emailS,passwordS,username,phone};


                boolean allValid = true;
                for (FormEditText field: allFields) {
                    allValid = field.testValidity() && allValid;
                }

                if (allValid) {

                    if((phone.getText().toString().length()==10)){
                        checkphone=true;

                    }else {
                        checkphone=false;
                    }
                    Log.e("pass",passwordS.getText().toString());
                    if(passwordS.getText().toString().contains(" ")){
                        checkpas=false;
                    }else if(passwordS.getText().toString().length()<5) {
                        checkpas=false;
                    }else
                    {
                        checkpas=true;
                    }
                    if(checkphone && checkpas){

                        dosignup();
                        //do task
                    }else {
                        if(!checkpas){
                            if(passwordS.getText().toString().contains(" ")){
                                Toast.makeText(getActivity(),"Blank spaces are not allowed in password",Toast.LENGTH_SHORT).show();

                            }else {
                                passwordS.setError("error");
                                Toast.makeText(getActivity(),"Password length must be of minimum 6 characters",Toast.LENGTH_SHORT).show();
                                //   passwordS.setError("Password length must be of minimum 6 characters");
                            }
                        }else {
                          //  Toast.makeText(getActivity(),"Phone number length must be between 8 to 12 digits",Toast.LENGTH_SHORT).show();

                            phone.setError("Phone number length must be between 8 to 12 digits");
                        }
                    }
                    // YAY
                    Log.e("status","valid");
                } else {
                    // EditText are going to appear with an exclamation mark and an explicative message.
                    Log.e("status","invalid");

                }





            }
        });
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    private void dosignup() {

        hideKeyboard();

        if(connectionUtils.isConnectionAvailable()){

            signup_progress.show();

            final CheckInternetTask t911=new CheckInternetTask();
            t911.execute();

            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    t911.cancel(true);

                    if (flag_netcheck) {
                        flag_netcheck = false;
                        new SendEmail().execute();
                    }else {
                        signup_progress.dismiss();
                        flag_netcheck=false;
                        Toast.makeText(getActivity(), "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    }
                }
            }.start();

            new CountDownTimer(2000, 2000) {
                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {




                }
            }.start();


        }else {
            Toast.makeText(getActivity(), "Please check your internet connection !", Toast.LENGTH_SHORT).show();
        }
    }
    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(type.equalsIgnoreCase("signup")){

            //signup proceed
            Log.e("signup","resp");
            if(signup_progress.isShowing()&&signup_progress!=null){
                signup_progress.dismiss();
            }
            if(guiResponse!=null) {
                if (status.equals(RequestStatus.SUCCESS)) {

                    if (guiResponse instanceof ForgetPasswordResponse) {
                        ForgetPasswordResponse response = (ForgetPasswordResponse) guiResponse;
                        if (response.isStatus()) {

                            Intent intent = new Intent(getContext(), SignUpFragment.class);
                            intent.putExtra("otpf","no");
                            intent.putExtra("phn", phone.getText().toString());
                            intent.putExtra("email", emailS.getText().toString());
                            intent.putExtra("username", username.getText().toString());
                            intent.putExtra("password", passwordS.getText().toString());
                            intent.putExtra("id", response.get_id());
                            intent.putExtra("session_id", "jok");
                            intent.putExtra("from", "signup");
                            startActivity(intent);


                        } else {
                            if (!TextUtils.isEmpty(response.getMessage()))
                                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                        }

                    } else if (guiResponse instanceof SendOtpResponse) {
                        SendOtpResponse sendOtpResponse = (SendOtpResponse) guiResponse;
                        if (sendOtpResponse.isStatus()) {
                            Intent intent = new Intent(getContext(), EnterOtpActivity.class);
                            intent.putExtra("otpf","yes");
                            intent.putExtra("phn", phone.getText().toString());
                            intent.putExtra("email", emailS.getText().toString());
                            intent.putExtra("username", username.getText().toString());
                            intent.putExtra("password", passwordS.getText().toString());
                            intent.putExtra("session_id", sendOtpResponse.getDetails());
                            intent.putExtra("from", "signup");
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "NETWORK ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private class SendEmail extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String emailtext=emailS.getText().toString();
            String responseString="";
            String jon = new JsonBuilder(new GsonAdapter())
                    .object("email", emailtext.toLowerCase())
                    .build().toString();

            try {
                url = new URL(AppConstant.BASE_URL+"auth/verifyemail");

                Log.v("URL", "URL: " + url);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type",
                        "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        jon.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(jon);
                wr.flush();

                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.v("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(signup_progress.isShowing()&&signup_progress!=null){
                            signup_progress.dismiss();
                        }
                        Toast.makeText(getActivity(),"Error while signing up!",Toast.LENGTH_SHORT).show();
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            if(s.equals("Email Is Not Registered")){
                RetrofitRequestProcessor processor = new RetrofitRequestProcessor(getActivity(), SignUpFragment.this);
                processor.sendOtp(phone.getText().toString());
            }else {
                if(signup_progress.isShowing()&&signup_progress!=null){
                    signup_progress.dismiss();
                }
                Toast.makeText(getActivity(),"This email id is already registered",Toast.LENGTH_SHORT).show();
            }
            /*
            RetrofitRequestProcessor processor = new RetrofitRequestProcessor(getActivity(), SignUpFragment.this);
            processor.sendOtp(phone.getText().toString());*/
        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }


}
