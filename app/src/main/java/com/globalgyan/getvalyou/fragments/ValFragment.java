package com.globalgyan.getvalyou.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.globalgyan.getvalyou.R;

/**
 * Created by NaNi on 18/09/17.
 */

public class ValFragment extends android.support.v4.app.Fragment {
    private static View view;
    FrameLayout fr;

    public static ValFragment newInstance() {

        return new ValFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_v, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fr=(FrameLayout)view.findViewById(R.id.frame_instruction);


       android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_instruction, new Performance());
        fragmentTransaction.addToBackStack("instructon");
        fragmentTransaction.commit();
    }
}
