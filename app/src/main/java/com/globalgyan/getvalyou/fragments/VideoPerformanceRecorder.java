/*
 * Copyright (c) 2016 Techniche E-commerce Solutions Pvt Ltd
 * No.14, 6th Floor,
 * Orchid Techscape, STPI Campus,
 * Cyber Park, Electronics City Phase1,
 * Bangalore-560100.
 *
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Techniche E-commerce
 * Solutions Pvt Ltd. You shall not disclose such Confidential Information and shall use it
 * only in accordance with the terms of the license agreement you entered into with
 * Techniche E-commerce Solutions Pvt Ltd.
 */

package com.globalgyan.getvalyou.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.andexert.library.RippleView;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.ValYouApplication;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.helper.FileReadyObserver;
import com.globalgyan.getvalyou.helper.HoloCircularProgressBar;
import com.globalgyan.getvalyou.helper.camera.CameraGLView;
import com.globalgyan.getvalyou.helper.camera.encoder.MediaAudioEncoder;
import com.globalgyan.getvalyou.helper.camera.encoder.MediaEncoder;
import com.globalgyan.getvalyou.helper.camera.encoder.MediaMuxerWrapper;
import com.globalgyan.getvalyou.helper.camera.encoder.MediaVideoEncoder;
import com.globalgyan.getvalyou.interfaces.FileReadyListener;
import com.globalgyan.getvalyou.interfaces.MergeUpdator;
import com.globalgyan.getvalyou.interfaces.RecordTimerUpdator;
import com.globalgyan.getvalyou.interfaces.RecorderOptionListner;
import com.globalgyan.getvalyou.interfaces.VideoPerformanceUpdator;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.MergeVideo;
import com.wooplr.spotlight.SpotlightView;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.morrox.fontinator.FontTextView;

import static android.app.Activity.RESULT_OK;

public class VideoPerformanceRecorder extends Fragment {
    private final static int CAMERA_RQ = 6969;
    private View view = null;
    private float currentProgress = 1;
    private TextView dummyDetails = null;
    File saveFolder;
    private FileReadyObserver fileObserver = null;
    private RecordTimerUpdator recordTimerUpdator = null;
    private HoloCircularProgressBar recordBufferIndicator = null;
    private Timer recordingTimer = null;
    private ImageButton cameraImage = null;
    private VideoPerformanceUpdator videoPerformanceUpdator = null;
    private String outputFile = null;
    private List<String> filePath = new ArrayList<>();
    Dialog dialog;
    Dialog dialogis;
    private CameraGLView mCameraView;
    /**
     * muxer for audio/video recording
     */
    private MediaMuxerWrapper mMuxer;

    int time_value=0;

    private int currentSpeed = 25;

    private boolean videoPaused = false;

    private ImageView pauseVideo = null;

    private String profileScript = null;
    private static boolean isOpenedFirstTime = true;

    private RelativeLayout alphaLayout = null, overlayLayout = null;
    RippleView continueView = null;

    private final static String TAG = VideoPerformanceRecorder.class.getName();


    public static VideoPerformanceRecorder newInstance() {
        Log.e(TAG, "newInstance: VideoPerformance instance created");
        return new VideoPerformanceRecorder();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_camera_old, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        return view;
    }


    @Override
    public void onViewCreated(final View views, Bundle savedInstanceState) {


    }

    @SuppressLint("ResourceType")
    private void launchCamera() {
        new MaterialCamera(this)                               // Constructor takes an Activity
                .autoSubmit(true)                                 // Whether or not user is allowed to playback videos after recording. This can affect other things, discussed in the next section.
                .saveDir(saveFolder)                               // The folder recorded videos are saved to
                .primaryColorAttr(Color.parseColor("#ceffffff"))             // The theme color used for the camera, defaults to colorPrimary of Activity in the constructor
                .showPortraitWarning(true)                         // Whether or not a warning is displayed if the user presses record in portrait orientation
                .defaultToFrontFacing(true)                       // Whether or not the camera will initially show the front facing camera
                .retryExits(false)                                 // If true, the 'Retry' button in the playback screen will exit the camera instead of going back to the recorder
                .restartTimerOnRetry(false)                        // If true, the countdown timer is reset to 0 when the user taps 'Retry' in playback
                .videoEncodingBitRate(224000)                     // Sets a custom bit rate for video recording.
                .continueTimerInPlayback(false)                    // If true, the countdown timer will continue to go down during playback, rather than pausing.
                //.videoEncodingBitRate(180000)                     // Sets a custom bit rate for video recording.
                .audioEncodingBitRate(50000)                       // Sets a custom bit rate for audio recording.
                .videoFrameRate(20)                                // Sets a custom frame rate (FPS) for video recording.
                .qualityProfile(MaterialCamera.QUALITY_HIGH)       // Sets a quality profile, manually setting bit rates or frame rates with other settings will overwrite individual quality profile settings
                .videoPreferredHeight(720)                         // Sets a preferred height for the recorded video output.
                .videoPreferredAspect(3f / 3f)                     // Sets a preferred aspect ratio for the recorded video output.
                .maxAllowedFileSize(1024 * 1024 * 30)               // Sets a max file size of 5MB, recording will stop if file reaches this limit. Keep in mind, the FAT file system has a file size limit of 4GB.
                .iconRecord(R.drawable.red_dot)        // Sets a custom icon for the button used to start recording
                .iconStop(R.drawable.mcam_action_stop)             // Sets a custom icon for the button used to stop recording
                .iconFrontCamera(R.drawable.mcam_camera_front)     // Sets a custom icon for the button used to switch to the front camera
                .iconRearCamera(R.drawable.mcam_camera_rear)       // Sets a custom icon for the button used to switch to the rear camera
                .iconPlay(R.drawable.evp_action_play)              // Sets a custom icon used to start playback
                //.iconPause(R.drawable.evp_action_pause)            // Sets a custom icon used to pause playback
                // .iconRestart(R.drawable.evp_action_restart)        // Sets a custom icon used to restart playback
                //.labelRetry(R.string.mcam_retry)                   // Sets a custom button label for the button used to retry recording, when available
                // .labelConfirm(R.string.mcam_use_video)             // Sets a custom button label for the button used to confirm/submit a recording
                .autoRecordWithDelaySec(3)                         // The video camera will start recording automatically after a 5 second countdown. This disables switching between the front and back camera initially.
                .autoRecordWithDelayMs(3000)                      // Same as the above, expressed with milliseconds instead of seconds.
                .audioDisabled(false)
                .autoSubmit(true).allowRetry(false)// Set to true to record video without any audio.
                .countdownSeconds(time_value)
                .start(CAMERA_RQ);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_RQ) {

            if (resultCode == RESULT_OK) {

                String[] msg = data.getDataString().split("/");
                for (int i = 0; i < msg.length; i++) {
                    Log.v("paths :" + i + "", msg[i]);
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 3; i < msg.length; i++) {
                    sb.append("/");
                    sb.append(msg[i].toString());
                }
                String sbuilder = sb.toString();
                sb.setLength(0);
                //sb.append("[");
                sb.append(sbuilder);
                new PrefManager(getActivity()).saveVideoResponseDataPath(sb.toString());
            } else if(data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                e.printStackTrace();
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }



    @Override
    public void onDestroy() {
        Log.e("----", "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("-----", "onpause");
    }



    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
