package com.globalgyan.getvalyou.fragments;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.globalgyan.getvalyou.Learning;
import com.globalgyan.getvalyou.LearningAdapter;
import com.globalgyan.getvalyou.LearningMainCourseAdapter;
import com.globalgyan.getvalyou.LearningRecyclerAdapter;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.cms.response.GetRecommVideoListResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.GameModel;
import com.globalgyan.getvalyou.model.VideoData;

import java.util.ArrayList;
import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 18/09/17.
 */

public class LearningFragment extends android.support.v4.app.Fragment implements GUICallback{
    private RecyclerView recyclerGen;
    private LearningAdapter adapter;
    private List<Learning> albumList;
    FontTextView empty;
    private List<VideoData> articleList = new ArrayList<>();
    List<GameModel> list_games=new ArrayList<>();
   // private LearningRecyclerVideoAdapter articleAdapter = null;
    View view;
    RecyclerView learning_list,maincourse_list;
    LearningRecyclerAdapter learningRecyclerAdapter;
    LearningMainCourseAdapter learningMainCourseAdapter;
    String uid;
    ProgressDialog pr;
    FontTextView no_maincourse,no_learning;
    public static LearningFragment newInstance() {

        return new LearningFragment();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.frag_learning, container, false);

        return view;

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<String> list_title=new ArrayList<>();
        list_title.add(0,"Operations");
        list_title.add(1,"Marketing");

        List<String> list_company=new ArrayList<>();
        list_company.add(0,"TATA COMMUNICATION");
        list_company.add(1,"TATA COMMUNICATION");
        list_company.add(2,"TATA COMMUNICATION");
        list_company.add(3,"TATA COMMUNICATION");
        learning_list=(RecyclerView)view.findViewById(R.id.learning_list);
        maincourse_list=(RecyclerView)view.findViewById(R.id.maincourse_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        learning_list.setLayoutManager(linearLayoutManager);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        maincourse_list.setLayoutManager(linearLayoutManager1);
        learningRecyclerAdapter=new LearningRecyclerAdapter(getActivity(),list_title,list_company);
        learningMainCourseAdapter=new LearningMainCourseAdapter(getActivity(),list_title,list_title);
        learning_list.setAdapter(learningMainCourseAdapter);
        maincourse_list.setAdapter(learningMainCourseAdapter);
        no_maincourse=(FontTextView)view.findViewById(R.id.no_courses);
        no_learning=(FontTextView)view.findViewById(R.id.no_learnings);

    }

//    private void prepareAlbums() {
//        int[] covers = new int[]{
//                R.drawable.one,
//                R.drawable.a2a,
//                R.drawable.a3a,
//                R.drawable.a4a,
//                R.drawable.a5a,
//                R.drawable.a6a};
//
//        Learning a = new Learning("Where does the Internet come from?",covers[1],"https://www.youtube.com/watch?v=jKA5hz3dV-g");
//        albumList.add(a);
//
//        a = new Learning("Digital Transformation in F1 - a Speed To Lead story",covers[0],"https://www.youtube.com/watch?v=kl2Hh5BW9ng");
//        albumList.add(a);
//
//        a = new Learning("A network for tomorrow, ready today",covers[2],"https://www.youtube.com/watch?v=AH4mUMPosNY");
//        albumList.add(a);
//
//        a = new Learning("Tata Communications: Doing Whatever It Takes",covers[3],"https://www.youtube.com/watch?v=sGTdzgiKGkc");
//        albumList.add(a);
//
//        a = new Learning("Giving MERCEDES AMG PETRONAS the Speed To Lead(TM)",covers[4],"https://www.youtube.com/watch?v=bUZv0dR7LHk");
//        albumList.add(a);
//
//        a = new Learning("Vinod Kumar on disruption and innovation",covers[5],"https://www.youtube.com/watch?v=oJETT9Jkp_U");
//        albumList.add(a);
//
//        recyclerGen.setAdapter(adapter);
//        //adapter.notifyDataSetChanged();
//    }


    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {
                if (guiResponse != null) {

                    GetRecommVideoListResponse getRecommArticleListResponse = (GetRecommVideoListResponse) guiResponse;
                    if (getRecommArticleListResponse != null) {
                        if (articleList != null)
                            articleList.clear();
                        articleList = getRecommArticleListResponse.getVideoDatas();
                        if (articleList != null && articleList.size()>0) {
                            adapter = new LearningAdapter(getActivity(), articleList);
                            recyclerGen.setAdapter(adapter);
                            empty.setVisibility(View.GONE);
                            recyclerGen.setVisibility(View.VISIBLE);
                        }else{
                            empty.setText("No video recommendations available");
                            empty.setVisibility(View.VISIBLE);
                            recyclerGen.setVisibility(View.GONE);
                        }
                    }


                } else {
                    Toast.makeText(getActivity(), "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getActivity(), "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            //Toast.makeText(getActivity(), "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

/*
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerGen = (RecyclerView) view.findViewById(R.id.learnig);
        empty=(FontTextView)view.findViewById(R.id.empty);
        albumList = new ArrayList<>();
        adapter = new LearningAdapter(getActivity(), articleList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerGen.setLayoutManager(mLayoutManager);
        recyclerGen.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerGen.setItemAnimator(new DefaultItemAnimator());

      //  prepareAlbums();
        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(getActivity(),LearningFragment.this);
        processor.getRecommendedVideos(PreferenceUtils.getCandidateId(getActivity()));
    }
*/

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
