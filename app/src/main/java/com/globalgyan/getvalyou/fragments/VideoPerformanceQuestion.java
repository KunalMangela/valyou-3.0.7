package com.globalgyan.getvalyou.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.VideoPerformanceActivity;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.VideoPerformanceUpdator;

import de.morrox.fontinator.FontTextView;

import static android.content.Context.BATTERY_SERVICE;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 4/3/17
 *         Module : Valyou.
 */

public class VideoPerformanceQuestion extends Fragment {

    private DataBaseHelper dataBaseHelper = null;

    public static CountDownTimer vdo_performance_ques_timer;
    private  long startTime = 60 * 1000;
    private final long interval = 1 * 1000;
    private long secLeft = startTime;
    public static VideoPerformanceQuestion newInstance() {
        return new VideoPerformanceQuestion();
    }
    Button buildNowView,skip;
    private View view = null;
    Dialog dialog;
    AlertDialog alert11is;
    FontTextView questionTxt;
    Dialog alertDialogis;
    boolean check=true;
    private VideoPerformanceUpdator videoPerformanceUpdator = null;
    FontTextView timer;
    ImageView bck;

    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2,astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;
    BatteryReceiver mBatInfoReceiver;

    Dialog dialog2;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    public static  boolean is_ques_timer_running = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.video_performance_question, container, false);

        return view;
    }

    private void startanimation() {
        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        appConstant = new AppConstant(getActivity());
        videoPerformanceUpdator = (VideoPerformanceUpdator) getActivity();
        videoPerformanceUpdator.showHideToolbar(false);
        bm = (BatteryManager)getActivity().getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        yellow_planet = (ImageView)view.findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)view.findViewById(R.id.purple_planet);
        earth = (ImageView)view.findViewById(R.id.earth);
        orange_planet = (ImageView)view.findViewById(R.id.orange_planet);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            startanimation();
        }

        else {

        }



        bck=(ImageView)view.findViewById(R.id.backimgq);
        bck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bck.setEnabled(false);
                openDialogue();
            }
        });


       /* view.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  getActivity().getSupportFragmentManager().popBackStack();
                openDialogue();
            }
        });*/
        timer=(FontTextView)view.findViewById(R.id.timer);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
      //  vdo_performance_ques_timer = new MyCountDownTimer(startTime, interval);
        // TextView txtTitle = (TextView) view.findViewById(R.id.txtTitle);


        // txtTitle.setText("Create Video Response");

        //  TextView desc = (TextView) view.findViewById(R.id.desc);
       /* desc.setText(getString(R.string.question, String.valueOf(videoPerformanceUpdator.currentQuestionIndex()+1), String.valueOf(
                videoPerformanceUpdator.totalQuestions()
        )));
*/
        questionTxt = (FontTextView) view.findViewById(R.id.questionTxt);

        dataBaseHelper = new DataBaseHelper(getActivity());
        String name = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);
        if(!TextUtils.isEmpty(name) && name.length()>2){
            name = String.valueOf(name.charAt(0)).toUpperCase() + name.subSequence(1, name.length());
            questionTxt.setText(name + ", create your video profile");
        }
        questionTxt.setText(videoPerformanceUpdator.getQuestion()+"" );
        if(check) {
        //    vdo_performance_ques_timer.start();
        }
        PrefManager pf= new PrefManager(getActivity());
        pf.saveUU(videoPerformanceUpdator.getQuestion());
        Log.e("OBJEJBEOHOEHOEOBOEB",videoPerformanceUpdator.getId());

        buildNowView = (Button) view.findViewById(R.id.btnContinue);
        skip = (Button) view.findViewById(R.id.btnSkip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //recording started
                VideoPerformanceActivity.isrecordstarted=true;
                check=false;
              //  vdo_performance_ques_timer.cancel();
                skip.setEnabled(false);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        skip.setEnabled(true);
                    }
                }.start();
                try {
                    if (VideoPerformanceActivity.alert11.isShowing() && VideoPerformanceActivity.alert11 != null) {
                        VideoPerformanceActivity.alert11.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                try {
                    if (alert11is.isShowing() && alert11is != null) {
                        alert11is.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                showDialogueVrtrail();
               /* Fragment fragment = VideoPerformanceRecorder.newInstance();
                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.containerLayout, fragment);
                    fragmentTransaction.addToBackStack("VideoPerformanceRecorder");
                    fragmentTransaction.commit();
                }*/
            }
        });
        buildNowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //recording started
                buildNowView.setEnabled(false);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        buildNowView.setEnabled(true);
                    }
                }.start();
                VideoPerformanceActivity.isrecordstarted=true;
                try {
                    if (VideoPerformanceActivity.alert11.isShowing() && VideoPerformanceActivity.alert11 != null) {
                        VideoPerformanceActivity.alert11.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                try {
                    if (alert11is.isShowing() && alert11is != null) {
                        alert11is.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                showDialogueVrtrail();

            }
        });





    }

    public void showDialogueVrtrail(){

        alertDialogis = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        alertDialogis.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogis.setContentView(R.layout.trail_vr);
        Window window = alertDialogis.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        //alertDialogis.setCancelable(false);
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        //alertDialogis.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView trail = (FontTextView)alertDialogis.findViewById(R.id.trailvr);
        final FontTextView mainvr = (FontTextView)alertDialogis.findViewById(R.id.startvr);


        trail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialogis.dismiss();
                new PrefManager(getActivity()).set_typevr("trail");
                VideoPerformanceActivity.startCameraResponse();

            }
        });

        mainvr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogis.dismiss();
                new PrefManager(getActivity()).set_typevr("main");
                VideoPerformanceActivity.startCameraResponse();


            }
        });

        mainvr.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    mainvr.setBackgroundResource(R.drawable.skip_click);
                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            mainvr.setBackgroundResource(R.drawable.skip_profile);
                        }
                    }.start();

                }


                return false;
            }
        });

        trail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    trail.setBackgroundResource(R.drawable.skip_click);
                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            trail.setBackgroundResource(R.drawable.skip_profile);
                        }
                    }.start();

                }


                return false;
            }
        });




        try{
            alertDialogis.show();
        }
        catch (Exception e)
        {}
    }
    public  void stsarst(){

    }

    private void openDialogue(){

//        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(),R.style.ThemeDialogCustom);
//        builder1.setMessage("This action will cancel the video recording and will navigate to home page. Do you want to continue?");
//        //  builder1.setInverseBackgroundForced(true);
//        builder1.setCancelable(false);
//
//        builder1.setPositiveButton(
//                "Yes",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                        getActivity().finish();
//                    }
//                });
//
//        builder1.setNegativeButton(
//                "No",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
//
//        alert11is = builder1.create();
//        alert11is.show();
//
//        new CountDownTimer(3000, 3000) {
//            @Override
//            public void onTick(long l) {
//
//            }
//
//            @Override
//            public void onFinish() {
//                try {
//                    if (alert11is.isShowing()) {
//                        alert11is.dismiss();
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//            }
//        }.start();





        dialog2 = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.quit_warning);
        Window window = dialog2.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        dialog2.setCancelable(false);
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog2.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final ImageView astro = (ImageView) dialog2.findViewById(R.id.astro);
        final ImageView alien = (ImageView) dialog2.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView no = (ImageView) dialog2.findViewById(R.id.no_quit);
        final ImageView yes = (ImageView) dialog2.findViewById(R.id.yes_quit);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                no.setEnabled(false);
                yes.setEnabled(false);
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        dialog2.dismiss();
                        bck.setEnabled(true);
                    }
                }.start();


            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);
                no.setEnabled(false);
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        dialog2.dismiss();
                     //   vdo_performance_ques_timer.cancel();

                        bck.setEnabled(true);
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                }.start();

            }
        });

        try {
            dialog2.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }





    }


    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }


        @Override
        public void onFinish() {
           // vdo_performance_ques_timer.cancel();
            is_ques_timer_running = false;
            Log.e("FINFI","FINFOONSFOSFBSBFBBSPNPSNP");
            if(check){
                try {
                    buildNowView.performClick();
                    check = false;
                }catch (Exception e){

                }
            }

        }


        @Override
        public void onTick(long millisUntilFinished) {
            is_ques_timer_running = true;
            secLeft = millisUntilFinished;
            secLeft=secLeft/1000;
            Log.e("TICK",""+secLeft);
            timer.setText(""+secLeft);
            questionTxt.setText( videoPerformanceUpdator.getQuestion());

        }

    }

    void startTimer() {
        vdo_performance_ques_timer.start();
    }



}
