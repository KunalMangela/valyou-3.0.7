package com.globalgyan.getvalyou;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

/**
 * Created by NaNi on 09/10/17.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {/*
        Intent notificationIntent = new Intent(context, HomeActivity.class);
        notificationIntent.putExtra("tabs","normal");*/
        Intent intent1 = new Intent(context, HomeActivity.class);
        intent.putExtra("tabs","normal");
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent1,intent.FILL_IN_ACTION);


       /* TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(notificationIntent);*/

       // PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        NotificationCompat.Builder notification = builder.setContentTitle("Your Games are ready!!");

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notification.setContentText("Checkout the new games and play them.")
                    .setTicker("New Message Alert!")
                    .setSmallIcon(R.drawable.ic_val)
                    .setAutoCancel(true)
                    .setColor(Color.parseColor("#5250B0"))
                    .setContentIntent(pendingIntent).build();
        }else {
            notification.setContentText("Checkout the new games and play them.")
                    .setTicker("New Message Alert!")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent).build();
        }


        //NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //notificationManager.notify(0, notification.build());
    }
}