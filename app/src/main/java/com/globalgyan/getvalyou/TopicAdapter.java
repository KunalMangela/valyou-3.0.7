package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.model.Topic_data_model;
import com.globalgyan.getvalyou.simulation.SimulationStartPageActivity;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.globalgyan.getvalyou.ModulesActivity.flag_update_lesson;
import static com.globalgyan.getvalyou.ModulesActivity.topic_data_model;
import static com.globalgyan.getvalyou.ModulesActivity.topic_ids;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<TopicModule> fungame;
    ConnectionUtils connectionUtils;
    ArrayList<Topic_data_model> s ;
    boolean flag_locked = false;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView module_names,author,price_module,description_button_module,progress_topic,desc_arrow_module,topics,description_text_module,topic_plus_sign,topics_text;
        FrameLayout main_fr;
        ImageView module_cover,bullet_img;
        SimpleRatingBar ratingbar_module;
        LinearLayout description_linear_module;
        RelativeLayout description_relative_module,lock_layout_module,played_layout_module;
        LinearLayout view_topics_linear_module;
        RelativeLayout topic_list_relative;
        ArrayList<String> s ;
        public MyViewHolder(View view) {
            super(view);
            module_names=(TextView)view.findViewById(R.id.moduleename_is);
            author=(TextView)view.findViewById(R.id.author_module_is);
            module_cover=(ImageView)view.findViewById(R.id.module_cover_is);
            progress_topic=(TextView)view.findViewById(R.id.progress_topic);
            main_fr=(FrameLayout)view.findViewById(R.id.main_fr_module_is);
            price_module=(TextView)view.findViewById(R.id.price_module_is);
            bullet_img=(ImageView)view.findViewById(R.id.bullet_img);
            ratingbar_module=(SimpleRatingBar)view.findViewById(R.id.ratingbar_module_is);
            description_linear_module=(LinearLayout)view.findViewById(R.id.description_linear_module_is);
            description_button_module=(TextView)view.findViewById(R.id.description_button_module_is);
            description_relative_module=(RelativeLayout)view.findViewById(R.id.description_relative_module_is);
            desc_arrow_module=(TextView)view.findViewById(R.id.desc_arrow_module_is);
            //topic click
            view_topics_linear_module=(LinearLayout) view.findViewById(R.id.view_topics_linear_module_is);
            topic_plus_sign=(TextView)view.findViewById(R.id.topic_plus_sign_is);
            topics_text=(TextView)view.findViewById(R.id.topics_text_is);

            description_text_module=(TextView)view.findViewById(R.id.description_text_module_is);
            lock_layout_module=(RelativeLayout)view.findViewById(R.id.lock_layout_module_is);
            played_layout_module=(RelativeLayout)view.findViewById(R.id.played_layout_module_is);
        }
    }


    public TopicAdapter(Context mContext, ArrayList<TopicModule> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;
        connectionUtils=new ConnectionUtils(mContext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.topic_items, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.setIsRecyclable(false);

        Log.e("whenpossible","todays-"+position);
        final TopicModule modulesModel=fungame.get(position);
        holder.module_names.setSelected(true);
        holder.author.setSelected(true);

        if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")){

            flag_locked = false;
           // holder.played_layout_module.setVisibility(View.GONE);
          //  holder.lock_layout_module.setVisibility(View.GONE);
            //    holder.main_fr.setAlpha(1f);

            //   holder.main_fr.setBackgroundResource(R.drawable.ass_ripple);
        }else {

            flag_locked = true;
            holder.module_names.setAlpha(0.4f);
            holder.bullet_img.setAlpha(0.4f);
           // holder.played_layout_module.setVisibility(View.GONE);
          //  holder.lock_layout_module.setVisibility(View.VISIBLE);
            // holder.main_fr.setAlpha(0.6f);
            // holder.main_fr.setBackgroundResource(R.drawable.module_is);

        }

        if(modulesModel.getType().equalsIgnoreCase("lesson")){
            holder.author.setText("Lesson");
        }else if(modulesModel.getType().equalsIgnoreCase("assessment")){
            holder.author.setText("Assessment");
        }else {
            if(modulesModel.getAuthor().equals("null")){
                holder.author.setText(new PrefManager(mContext).getCourseauthor());
            }else {
                holder.author.setText("Author: "+modulesModel.getAuthor());

            }
        }

      /*  if(ModulesActivity.open_topic_status.get(position).equalsIgnoreCase("close")){
            holder.description_relative_module.setVisibility(View.GONE);
            holder.desc_arrow_module.setBackgroundResource(R.drawable.ic_arrow_drop_down_white_24dp);
        }else {
            holder.description_relative_module.setVisibility(View.VISIBLE);
            holder.desc_arrow_module.setBackgroundResource(R.drawable.ic_arrow_drop_up_white_24dp);

        }*/



        holder.price_module.setText("Price: 2300");
        holder.played_layout_module.setVisibility(View.GONE);


        if(flag_update_lesson) {
            if (modulesModel.getType().equalsIgnoreCase("topic")) {
                if (!modulesModel.getIs_locked().equalsIgnoreCase("locked")) {
                    flag_locked = false;
                    if (modulesModel.getIs_locked().equalsIgnoreCase("completed")) {

                        topic_data_model.setType(modulesModel.getType());
                        topic_data_model.setLock(modulesModel.getIs_locked());

                        topic_ids.add(topic_data_model);

                        new PrefManager(mContext).save_topic_ids(topic_ids, "ids");

                        holder.progress_topic.setVisibility(View.VISIBLE);
                        holder.progress_topic.setText("Completed");

                        // holder.played_layout_module.setVisibility(View.VISIBLE);
                        holder.lock_layout_module.setVisibility(View.GONE);
                    } else if (modulesModel.getIs_locked().equalsIgnoreCase("inprogress")) {

                        topic_data_model.setType(modulesModel.getType());
                        topic_data_model.setLock(modulesModel.getIs_locked());

                        topic_ids.add(topic_data_model);

                        new PrefManager(mContext).save_topic_ids(topic_ids, "ids");
                        // holder.progress_topic.setText("Inprogress");
                        holder.progress_topic.setVisibility(View.VISIBLE);
                        holder.progress_topic.setText("Inprogress");
                        // holder.played_layout_module.setVisibility(View.VISIBLE);
                        holder.lock_layout_module.setVisibility(View.GONE);
                    } else {

                        topic_data_model.setType(modulesModel.getType());
                        topic_data_model.setLock(modulesModel.getIs_locked());

                        topic_ids.add(topic_data_model);

                        new PrefManager(mContext).save_topic_ids(topic_ids, "ids");

                         holder.progress_topic.setText("New");
                        holder.progress_topic.setVisibility(View.VISIBLE);

                        //  holder.played_layout_module.setVisibility(View.GONE);
                        holder.lock_layout_module.setVisibility(View.GONE);

                    }

                } else {
                    topic_data_model.setType(modulesModel.getType());
                    topic_data_model.setLock(modulesModel.getIs_locked());

                    topic_ids.add(topic_data_model);

                    new PrefManager(mContext).save_topic_ids(topic_ids, "ids");
                    flag_locked = true;
                    holder.progress_topic.setText("Locked");
                    holder.progress_topic.setVisibility(View.VISIBLE);

                    holder.module_names.setAlpha(0.4f);
                    holder.bullet_img.setAlpha(0.4f);
                    // holder.played_layout_module.setVisibility(View.GONE);
                    //  holder.lock_layout_module.setVisibility(View.VISIBLE);
                }
            }

        }
        else {
            if (!flag_update_lesson) {
                s = new PrefManager(mContext).get_topic_array_ids("ids");

                s.add(topic_data_model);

                if (topic_data_model.getType().equalsIgnoreCase("topic")) {
                    if (!topic_data_model.getLock().equalsIgnoreCase("locked")) {
                        Log.e("topic",""+topic_data_model.getLock());

                        flag_locked = false;
                        if (topic_data_model.getLock().equalsIgnoreCase("completed")) {


                            Log.e("topic",""+topic_data_model.getLock());
                            holder.progress_topic.setVisibility(View.VISIBLE);
                            holder.progress_topic.setText("Completed");

                            // holder.played_layout_module.setVisibility(View.VISIBLE);
                            holder.lock_layout_module.setVisibility(View.GONE);
                        } else if (topic_data_model.getLock().equalsIgnoreCase("inprogress")) {


                             holder.progress_topic.setText("Inprogress");
                            holder.progress_topic.setVisibility(View.VISIBLE);

                            // holder.played_layout_module.setVisibility(View.VISIBLE);
                            holder.lock_layout_module.setVisibility(View.GONE);
                        } else {

                            Log.e("topic",""+topic_data_model.getLock());
                             holder.progress_topic.setText("New");
                            holder.progress_topic.setVisibility(View.VISIBLE);

                            //  holder.played_layout_module.setVisibility(View.GONE);
                            holder.lock_layout_module.setVisibility(View.GONE);

                        }

                    } else {

                        flag_locked = true;
                        holder.progress_topic.setText("Locked");
                        holder.progress_topic.setVisibility(View.VISIBLE);

                        holder.module_names.setAlpha(0.4f);
                        holder.bullet_img.setAlpha(0.4f);
                        // holder.played_layout_module.setVisibility(View.GONE);
                        //  holder.lock_layout_module.setVisibility(View.VISIBLE);
                    }
                }

            }

        }
        if(modulesModel.getType().equalsIgnoreCase("assessment")){
            if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")){
                flag_locked = false;
                if(modulesModel.getIs_locked().equalsIgnoreCase("completed")){
                    holder.progress_topic.setText("Completed");
                    holder.progress_topic.setVisibility(View.VISIBLE);

                    // holder.played_layout_module.setVisibility(View.VISIBLE);
                    holder.lock_layout_module.setVisibility(View.GONE);
                }else  if(modulesModel.getIs_locked().equalsIgnoreCase("inprogress")){
                    holder.progress_topic.setText("Inprogress");
                    holder.progress_topic.setVisibility(View.VISIBLE);
                   //   holder.played_layout_module.setVisibility(View.VISIBLE);
                    holder.lock_layout_module.setVisibility(View.GONE);
                }else {
                    holder.progress_topic.setText("New");
                    holder.progress_topic.setVisibility(View.VISIBLE);
                    // holder.played_layout_module.setVisibility(View.GONE);
                    holder.lock_layout_module.setVisibility(View.GONE);

                }

            }else {
                flag_locked = true;
                holder.progress_topic.setText("Locked");
                holder.progress_topic.setVisibility(View.VISIBLE);
                holder.module_names.setAlpha(0.4f);
                holder.bullet_img.setAlpha(0.4f);
                //  holder.played_layout_module.setVisibility(View.GONE);
             //   holder.lock_layout_module.setVisibility(View.VISIBLE);
            }
        }



        if(modulesModel.getType().equalsIgnoreCase("lesson")){
            if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")){
                flag_locked = false;
                if(modulesModel.getIs_locked().equalsIgnoreCase("completed")){
                    holder.played_layout_module.setVisibility(View.VISIBLE);
                    holder.lock_layout_module.setVisibility(View.GONE);
                }else {
                    holder.played_layout_module.setVisibility(View.GONE);
                    holder.lock_layout_module.setVisibility(View.GONE);

                }

            }else {
                flag_locked = true;
                holder.played_layout_module.setVisibility(View.GONE);
                holder.lock_layout_module.setVisibility(View.VISIBLE);
            }
        }



        holder.module_names.setText(modulesModel.getObj_name());
        Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.module_names.setTypeface(tf2);
        holder.topics_text.setTypeface(tf2);
        holder.author.setTypeface(tf2);
        holder.description_text_module.setTypeface(tf2);
        holder.price_module.setTypeface(tf2);
        holder.progress_topic.setTypeface(tf2,Typeface.BOLD);
        holder.description_button_module.setTypeface(tf2);
        holder.description_text_module.setText(modulesModel.getDesc());
      //  holder.topics.setTypeface(tf2);


       /* holder.description_linear_module.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")) {
                    String status_is=ModulesActivity.open_topic_status.get(position);
                    if(status_is.equalsIgnoreCase("close")){
                        ModulesActivity.open_topic_status.add(position,"open");
                        holder.description_relative_module.setVisibility(View.VISIBLE);
                        holder.desc_arrow_module.setBackgroundResource(R.drawable.ic_arrow_drop_up_white_24dp);
                    }else {
                        ModulesActivity.open_topic_status.add(position,"close");
                        holder.description_relative_module.setVisibility(View.GONE);
                        holder.desc_arrow_module.setBackgroundResource(R.drawable.ic_arrow_drop_down_white_24dp);
                    }

                }else {

                }
            }
        });
*/




        holder.main_fr.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN  &&(!modulesModel.getIs_locked().equalsIgnoreCase("locked") || (modulesModel.getType().equalsIgnoreCase("topic") && !modulesModel.getIs_locked().equalsIgnoreCase("locked")) ||(modulesModel.getType().equalsIgnoreCase("lesson")&&!modulesModel.getIs_locked().equalsIgnoreCase("locked"))) ) {




                    holder.main_fr.setBackgroundColor(Color.parseColor("#c5c9d3"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            holder.main_fr.setBackgroundColor(Color.parseColor("#cee2e4e9"));


                        }
                    }.start();


                }




                return false;

            }



        });
        holder.main_fr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - ModulesActivity.mLastClickTime < 1000) {
                    return;
                }
                ModulesActivity.mLastClickTime  = SystemClock.elapsedRealtime();
                if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")) {
                    new PrefManager(mContext).saveTargetid(String.valueOf(modulesModel.getId()));
                    if (connectionUtils.isConnectionAvailable()) {
                        if (modulesModel.getType().equalsIgnoreCase("lesson")) {
                            //module
                            //ModulesActivity.click_to=true;


                        }else {

                            if(modulesModel.getIsfeedback().equalsIgnoreCase("yes")){
                                new PrefManager(mContext).savecompulsaryfeedback("yes");
                            }else {
                                new PrefManager(mContext).savecompulsaryfeedback("no");
                            }
                            if (modulesModel.getType().equalsIgnoreCase("assessment")) {
                                //assessment




                                topic_data_model.setType(modulesModel.getType());
                                topic_data_model.setLock(modulesModel.getIs_locked());

                                topic_ids.add(topic_data_model);

                                new PrefManager(mContext).save_topic_ids(topic_ids,"ids");


                                ModulesActivity.sendLessonsStartData(modulesModel.getId(), modulesModel.getOrderid(), "assess", "ass",modulesModel.getTypelms());
                                new PrefManager(mContext).saveinnermoduleid(String.valueOf(modulesModel.getId()));
                                new PrefManager(mContext).saveorderid(String.valueOf(modulesModel.getOrderid()));
                                Log.e("orderidis",""+modulesModel.getOrderid());
                                new PrefManager(mContext).saveRatingtype("assessment_group");
                                new PrefManager(mContext).saveAsstype("Ass");
                                Intent intent = new Intent(mContext, HomeActivity.class);
                                intent.putExtra("tabs", "normal");

                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                mContext.startActivity(intent);

                                if (mContext instanceof Activity) {
                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    //((Activity) mContext).finish();
                                }

                            } else {

                                if (!modulesModel.getIs_locked().equalsIgnoreCase("locked")) {


                                    topic_data_model.setType(modulesModel.getType());
                                    topic_data_model.setLock("completed");

                                    topic_ids.add(topic_data_model);

                                    new PrefManager(mContext).save_topic_ids(topic_ids,"ids");

                                    new PrefManager(mContext).saveinnermoduleid(String.valueOf(modulesModel.getId()));
                                    new PrefManager(mContext).saveorderid(String.valueOf(modulesModel.getOrderid()));
                                    if (modulesModel.getLms_url().length() > 5) {
                                        //lms

                                        ModulesActivity.sendLessonsStartData(modulesModel.getId(), modulesModel.getOrderid(), "topic", "lms",modulesModel.getTypelms());
                                        new PrefManager(mContext).saveRatingtype(modulesModel.getType());
                                        new PrefManager(mContext).savestatusforFeedback(modulesModel.getIs_locked());

                                        Log.e("status", "last lesson");
                                        new PrefManager(mContext).set_sm_nav_from("learn");
                                        Intent intent = new Intent(mContext, WebviewVideoActivity.class);

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("link", modulesModel.getLms_url());
                                        mContext.startActivity(intent);

                                    } else {
                                        /*topic_data_model.setType(modulesModel.getType());
                                        topic_data_model.setLock(modulesModel.getIs_locked());

                                        topic_ids.add(topic_data_model);

                                        new PrefManager(mContext).save_topic_ids(topic_ids,"ids");*/

                                        //simulation
                                        ModulesActivity.sendLessonsStartData(modulesModel.getId(), modulesModel.getOrderid(), "topic", "sm",modulesModel.getTypelms());

                                        new PrefManager(mContext).saveRatingtype(modulesModel.getType());
                                        new PrefManager(mContext).savestatusforFeedback(modulesModel.getIs_locked());
                                        Log.e("status", "last lesson");
                                        Log.e("status", modulesModel.getChildrens());

                                        if(modulesModel.getSm_type().equalsIgnoreCase("articulate")){

                                            Log.e("sm_articulate","sm_articulate");
                                            new PrefManager(mContext).set_sm_nav_from("learn");


                                            Intent i = new Intent(mContext, WebviewVideoActivity.class);

                                            i.putExtra("link", modulesModel.getSm_articulate_url());
                                            mContext.startActivity(i);
                                            if (mContext instanceof Activity) {
//                                                ((Activity) mContext).finish();
                                                ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                            }

                                        }
                                    else {
                                            Log.e("sm_normal","sm_normal");
                                            Intent i = new Intent(mContext, SimulationStartPageActivity.class);
                                            try {
                                                JSONArray jsonObject = new JSONArray(modulesModel.getChildrens());
                                                for (int in = 0; in < jsonObject.length(); in++) {
                                                    JSONObject sim_model = jsonObject.getJSONObject(in);

                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    i.putExtra("sm_name", sim_model.getString("sm_name"));
                                                    i.putExtra("sm_landing_page_image", sim_model.getString("sm_landing_page_image"));
                                                    i.putExtra("sm_bg_color", sim_model.getString("sm_bg_color"));
                                                    i.putExtra("sm_bg_image", sim_model.getString("sm_bg_image"));
                                                    i.putExtra("sm_decision_bg_color", sim_model.getString("sm_decision_bg_color"));
                                                    i.putExtra("sm_decision_bg_image", sim_model.getString("sm_decision_bg_image"));
                                                    i.putExtra("sm_bg_sound", sim_model.getString("sm_bg_sound"));
                                                    i.putExtra("sm_icon", sim_model.getString("sm_icon"));
                                                    i.putExtra("sm_id", sim_model.getString("sm_id"));
                                                    i.putExtra("sm_company", sim_model.getString("sm_company"));
                                                    i.putExtra("sm_icon_image", sim_model.getString("sm_icon_image"));
                                                    i.putExtra("gif_file", sim_model.getString("sm_bg_gif"));
                                                    i.putExtra("sm_description", sim_model.getString("sm_description"));
                                                    i.putExtra("sm_gif_loading_time", sim_model.getString("sm_gif_loading_time"));
                                                    i.putExtra("sm_title_color", sim_model.getString("sm_title_color"));
                                                    i.putExtra("sm_title_description_color", sim_model.getString("sm_title_description_color"));

                                                }
                                                mContext.startActivity(i);
                                                if (mContext instanceof Activity) {
                                                    // ((Activity) mContext).finish();
                                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                } else {
                                    ModulesActivity.nocourseavailableframe.setVisibility(View.VISIBLE);
                                    ModulesActivity.noavailabletext.setText(modulesModel.getWarn_text());
                                    //  Toast.makeText(mContext, "Currently this lesson is unavailable", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } else {
                        Toast.makeText(mContext, "Please check your internet connection", Toast.LENGTH_SHORT).show();

                    }


                }else {
                    ModulesActivity.nocourseavailableframe.setVisibility(View.VISIBLE);
                    ModulesActivity.noavailabletext.setText(modulesModel.getWarn_text());
                    //  Toast.makeText(mContext, "Currently this lesson is unavailable", Toast.LENGTH_SHORT).show();

                }





            }


        });

    }


    @Override
    public int getItemCount() {
        return fungame.size();

    }
}



