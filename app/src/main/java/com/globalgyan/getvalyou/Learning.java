package com.globalgyan.getvalyou;

/**
 * Created by NaNi on 10/10/17.
 */

public class Learning {

    private String name;
    private int thumbnail;
    private String url;

    public Learning() {
    }

    public Learning(String name,int thumbnail,String url) {
        this.name = name;
        this.url=url;
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
