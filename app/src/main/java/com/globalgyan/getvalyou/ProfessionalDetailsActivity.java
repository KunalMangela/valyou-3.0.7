package com.globalgyan.getvalyou;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.request.UpdateProfileRequest;
import com.globalgyan.getvalyou.cms.response.AddNewOrganisationResponse;
import com.globalgyan.getvalyou.cms.response.AddNewPositionResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.CalendarPicker;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 15/10/17.
 */

public class ProfessionalDetailsActivity extends AppCompatActivity implements GUICallback,CalendarPicker{

    String startDateString = "", endDateString = "";
    int count = 0,count_picker=0;
    LinearLayout select_linear = null;
    private FontTextView organisationName = null;
    private FontTextView natureOfWork = null;
    private FontTextView startDate = null;
    private FontTextView endDate = null;
    //
    private int ORGANIZATION_PICKER_REQUEST_CODE = 111;
    private int POSITION_HELD_PICKER_REQUEST_CODE = 222;
    private DataBaseHelper dataBaseHelper = null;
    private int dateSelection = 0;
    private Button btnDone = null;
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    //date picker
    ConnectionUtils connectionUtils;
    Calendar calendarE,calendar;
    Boolean currentwork=false;
    private DatePicker datePicker = null;
    private DatePicker enddatePicker = null;
    private Button saveButton = null;
    private CardView cardView = null;
    private FontTextView pickerTitle = null, cancel = null;
    private AppCompatButton saveView = null;
    private ImageView nextArrow = null, radio_button = null;
    private Professional professional = null;
    String kya;
    public boolean org_flag=false,nature_flag=false,date_flag=false,isradioclicked=true;
    String dateclicked="";
    YearMonthPickerDialog yearMonthPickerDialog;
    public  boolean isorgcorrect=false,isnaturecorrect=false;
    Typeface  tf1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_professional_details);
        connectionUtils=new ConnectionUtils(ProfessionalDetailsActivity.this);
           try{
            kya=getIntent().getExtras().getString("kya");
        }catch (Exception e){

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }

        yearMonthPickerDialog = new YearMonthPickerDialog(this, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month) {
                if(dateclicked.equalsIgnoreCase("start")){
                     calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");

                    startDate.setText(dateFormat.format(calendar.getTime()));
                    count_picker=1;
                    startDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
                    if(startDate.getText().toString().equalsIgnoreCase("Month/Year")||endDate.getText().toString().equalsIgnoreCase("Month/Year")){

                    }else {
                        calendarE= Calendar.getInstance();
                        if(calendarE.compareTo(calendar)==-1){
                            Toast.makeText(ProfessionalDetailsActivity.this,"Please choose valid dates",Toast.LENGTH_SHORT).show();
                            btnDone.setEnabled(false);
                            btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));

                        }else {
                            btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                            btnDone.setEnabled(true);
                        }
                    }

                }else {
                    radio_button.setImageResource(R.drawable.ic_radio_button_unchecked_black_24dp);

                    calendarE= Calendar.getInstance();
                    calendarE.set(Calendar.YEAR, year);
                    calendarE.set(Calendar.MONTH, month);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");

                    endDate.setText(dateFormat.format(calendarE.getTime()));
                    date_flag=true;
                    endDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
                    if(startDate.getText().toString().equalsIgnoreCase("Month/Year")||endDate.getText().toString().equalsIgnoreCase("Month/Year")){

                    }else {
                        calendar = Calendar.getInstance();
                        if(calendarE.compareTo(calendar)==-1){
                            Toast.makeText(ProfessionalDetailsActivity.this,"Please choose valid dates",Toast.LENGTH_SHORT).show();
                            btnDone.setEnabled(false);
                            btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));

                        }else {
                            btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                            btnDone.setEnabled(true);
                        }
                    }

                }
                //yearMonth.setText(dateFormat.format(calendar.getTime()));
            }
        });

        Calendar calendar3 = Calendar.getInstance();

        calendar3.set(Calendar.YEAR, 1990);
        calendar3.set(Calendar.MONTH, 1);
        calendar3.set(Calendar.DAY_OF_MONTH, 1);


        final Calendar selectedCalendar = Calendar.getInstance();
        selectedCalendar.set(Calendar.YEAR, 1995);
        selectedCalendar.set(Calendar.MONTH, 11);
        selectedCalendar.set(Calendar.DAY_OF_MONTH, 15);


        findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        btnDone = (Button) findViewById(R.id.btnDone);

        btnDone.setTypeface(tf1);
        if(!kya.equalsIgnoreCase("prof")){
            btnDone.setText("SKIP");
        }else{
            btnDone.setText("DONE");
        }
        // btnDone.setTypeface(FontUtils.getInstance(this).getNexaTypeFace());
        // btnDone = (AppCompatButton) findViewById(R.id.doneButtonView);

        // new date picker
        datePicker = (DatePicker) findViewById(R.id.datePicker);
        datePicker.setMinDate(calendar3.getTime().getTime());
        datePicker.setMaxDate(Calendar.getInstance().getTime().getTime());


        enddatePicker = (DatePicker) findViewById(R.id.endDatePicker);
        saveButton = (Button) findViewById(R.id.saveButton);
        cardView = (CardView) findViewById(R.id.dateSelect_cardview);
        pickerTitle = (FontTextView) findViewById(R.id.pickerTitle);
        saveView = (AppCompatButton) findViewById(R.id.saveButton);

          tf1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        saveView.setTypeface(tf1);
        //  nextArrow = (ImageView) findViewById(R.id.nextArrow);
        radio_button = (ImageView) findViewById(R.id.radio_button);
        cancel = (FontTextView) findViewById(R.id.cancel);
        select_linear = (LinearLayout) findViewById(R.id.select_linear);
        //  radioButton=(RadioButton)findViewById(R.id.radioButton);

        dataBaseHelper = new DataBaseHelper(this);
        organisationName = (FontTextView) findViewById(R.id.collegeName);
        findViewById(R.id.organisationLabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.organisationLabel).setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        findViewById(R.id.organisationLabel).setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){
                    Intent intent = new Intent(ProfessionalDetailsActivity.this, OrganizationActivity.class);
                    startActivityForResult(intent, ORGANIZATION_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    org_flag=true;
                }else {
                    Toast.makeText(ProfessionalDetailsActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });
        organisationName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                organisationName.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        organisationName.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){
                    Intent intent = new Intent(ProfessionalDetailsActivity.this, OrganizationActivity.class);
                    startActivityForResult(intent, ORGANIZATION_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    org_flag=true;
                }else {
                    Toast.makeText(ProfessionalDetailsActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });
        findViewById(R.id.natureOfWorklabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.natureOfWorklabel).setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        findViewById(R.id.natureOfWorklabel).setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){
                    Intent intent = new Intent(ProfessionalDetailsActivity.this, EmployerActivity.class);
                    startActivityForResult(intent, POSITION_HELD_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    nature_flag=true;
                }else {
                    Toast.makeText(ProfessionalDetailsActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });
        natureOfWork = (FontTextView) findViewById(R.id.natureOfWork);
        natureOfWork.setTypeface(tf1);
        natureOfWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                natureOfWork.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        natureOfWork.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){
                    Intent intent = new Intent(ProfessionalDetailsActivity.this, EmployerActivity.class);
                    startActivityForResult(intent, POSITION_HELD_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    nature_flag=true;
                }else {
                    Toast.makeText(ProfessionalDetailsActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });

        //added
        radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radio_button.setEnabled(false);
                new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                    radio_button.setEnabled(true);
                    }
                }.start();
                if(new PrefManager(ProfessionalDetailsActivity.this).getCurrentWork().equalsIgnoreCase("yes")){
                    Toast.makeText(ProfessionalDetailsActivity.this,"Employee is already working in other organization",Toast.LENGTH_SHORT).show();
                }else {
                    btnDone.setBackground(getResources().getDrawable(R.drawable.email_button));
                    btnDone.setEnabled(true);
                    if (count_picker == 1) {
                        if (isradioclicked) {
                            date_flag = true;
                            radio_button.setImageResource(R.drawable.ic_radio_button_checked_black_24dp);
                            // count = 1;
                            saveButton.setText("SAVE");
                            endDate.setText(null);
                            isradioclicked = false;
                            //  nextArrow.setVisibility(View.GONE);
                        } else {
                            radio_button.setImageResource(R.drawable.ic_radio_button_unchecked_black_24dp);
                            // count =
                            date_flag = false;

                            saveButton.setText("NEXT");
                            isradioclicked = true;
                            //nextArrow.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(ProfessionalDetailsActivity.this, "please enter start date", Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });


        findViewById(R.id.startDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openStartDatePicker();

            }
        });
        startDate = (FontTextView) findViewById(R.id.startDateText);
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openStartDatePicker();

            }
        });
        endDate = (FontTextView) findViewById(R.id.endDateText);
        endDate.setText("Month/Year");
        findViewById(R.id.endDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEndDatePicker();


            }
        });


        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEndDatePicker();

            }
        });

        //For start date
        LinearLayout pickerParentLayout = (LinearLayout) datePicker.getChildAt(0);

        LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);


        int datePickerCount = pickerSpinnersHolder.getChildCount();
        if ( datePickerCount == 3 ) {
            View firstView = pickerSpinnersHolder.getChildAt(0);

            NumberPicker firstNumber = (NumberPicker) firstView;

            if ( firstNumber != null && firstNumber.getMaxValue() > 12 && firstNumber.getMaxValue() < 32 ) {
                pickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
            } else {
                View secondView = pickerSpinnersHolder.getChildAt(1);
                NumberPicker secondNumber = (NumberPicker) secondView;
                if ( secondNumber != null && secondNumber.getMaxValue() > 12 && secondNumber.getMaxValue() < 32 ) {
                    pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                }
            }
        }


        //for end date


        LinearLayout endDatePickerParentLayout = (LinearLayout) enddatePicker.getChildAt(0);

        LinearLayout endDatePickerSpinnersHolder = (LinearLayout) endDatePickerParentLayout.getChildAt(0);


        int endDatePickerCount = endDatePickerSpinnersHolder.getChildCount();
        if ( endDatePickerCount == 3 ) {
            View firstView = endDatePickerSpinnersHolder.getChildAt(0);

            NumberPicker firstNumber = (NumberPicker) firstView;

            if ( firstNumber != null && firstNumber.getMaxValue() > 12 && firstNumber.getMaxValue() < 32 ) {
                endDatePickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
            } else {
                View secondView = endDatePickerSpinnersHolder.getChildAt(1);
                NumberPicker secondNumber = (NumberPicker) secondView;
                if ( secondNumber != null && secondNumber.getMaxValue() > 12 && secondNumber.getMaxValue() < 32 ) {
                    endDatePickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                }
            }
        }

        saveView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( saveButton.getText().toString().equalsIgnoreCase("NEXT") ) {
                    cardView.setVisibility(View.VISIBLE);

                    if ( pickerTitle.getText().toString().equalsIgnoreCase("START DATE") ) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                        int month = datePicker.getMonth();
                        String mon = months[month];
                        startDateString = mon + "-" + datePicker.getYear();
                        startDate.setText(startDateString);

                        startDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
                    }
                    saveButton.setText("SAVE");
                    //   nextArrow.setVisibility(View.GONE);
                    pickerTitle.setText("END DATE");
                    pickerTitle.setTextColor(Color.parseColor("#183989"));
                    datePicker.setVisibility(View.GONE);
                    enddatePicker.setVisibility(View.VISIBLE);
                    // select_linear.setVisibility(View.GONE);


                    Calendar startDate = Calendar.getInstance();
                    startDate.set(Calendar.YEAR, datePicker.getYear());

                    startDate.set(Calendar.MONTH, datePicker.getMonth());
                    startDate.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                    enddatePicker.setMinDate(startDate.getTime().getTime());
                    enddatePicker.setMaxDate(Calendar.getInstance().getTime().getTime());


                } else {
                    cardView.setVisibility(View.GONE);
                    btnDone.setEnabled(true);
                    startDate.setEnabled(true);
                    endDate.setEnabled(true);
                    findViewById(R.id.startDate).setEnabled(true);
                    findViewById(R.id.endDate).setEnabled(true);

                    if ( pickerTitle.getText().toString().equalsIgnoreCase("END DATE") ) {
                        int month = enddatePicker.getMonth();
                        String mon = months[month];
                        endDateString = mon + "-" + enddatePicker.getYear();

                        Calendar startDate = Calendar.getInstance();

                        startDate.set(Calendar.YEAR, enddatePicker.getYear());
                        startDate.set(Calendar.MONTH, enddatePicker.getMonth());
                        startDate.set(Calendar.DAY_OF_MONTH, enddatePicker.getDayOfMonth());

                        datePicker.setMaxDate(startDate.getTime().getTime());

                        endDate.setText(endDateString);
                        endDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));


                    }

                    if ( count == 1 ) {
                        int month = datePicker.getMonth();
                        String mon = months[month];
                        startDateString = mon + "-" + datePicker.getYear();
                        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                        int mnth = calendar.get(Calendar.MONTH);
                        String newmon = months[mnth];
                        endDateString = mon + "-" + enddatePicker.getYear();//newmon + "-" + calendar.get(Calendar.YEAR);
                        startDate.setText(startDateString);
                        endDate.setText("");
                        select_linear.setVisibility(View.GONE);
                        startDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
                        endDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));

                    }
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardView.setVisibility(View.GONE);
                btnDone.setEnabled(true);
                startDate.setEnabled(true);
                endDate.setEnabled(true);
                findViewById(R.id.startDate).setEnabled(true);
                findViewById(R.id.endDate).setEnabled(true);

            }
        });
        String selectedEducationId = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_PROFESSION);
        Log.e("----ID", selectedEducationId + "");
        if ( !TextUtils.isEmpty(selectedEducationId) ) {
            professional = dataBaseHelper.getProfessionDetailsFromId(selectedEducationId);

        }

        if ( professional != null ) {
            organisationName.setText(professional.getOrganizationName());
            natureOfWork.setText(professional.getPositionHeld());
            startDate.setText(professional.getFrom());
            endDate.setText(professional.getTo());
            calendar=stringToCalendar(professional.getFrom(),"MMM-yyyy");
            calendarE=stringToCalendar(professional.getFrom(),"MMM-yyyy");

            if(new PrefManager(ProfessionalDetailsActivity.this).getCurrentWork().equalsIgnoreCase("yes")){
                date_flag = true;
                radio_button.setImageResource(R.drawable.ic_radio_button_checked_black_24dp);
                // count = 1;
                endDate.setText(null);
                isradioclicked = false;
                currentwork=true;
            }else {
                currentwork=false;
            }
            if ( !TextUtils.isEmpty(professional.getFrom()) ) {
                String[] dates = professional.getFrom().split("-");
                if ( dates != null && dates.length == 2 ) {
                    Calendar startDate = Calendar.getInstance();
                    int index = -1;
                    for (int i = 0; i < months.length; i++) {
                        if ( months[i].equals(dates[0]) ) {
                            index = i;
                            break;
                        }
                    }
                    if ( index != -1 ) {
                        startDate.set(Calendar.YEAR, Integer.parseInt(dates[1]));
                        startDate.set(Calendar.MONTH, index);
                        startDate.set(Calendar.DAY_OF_MONTH, 1);

                        enddatePicker.setMinDate(startDate.getTime().getTime());
                        enddatePicker.setMaxDate(Calendar.getInstance().getTime().getTime());
                    }
                }

            }

            if ( !TextUtils.isEmpty(professional.getTo()) ) {
                String[] dates = professional.getTo().split("-");
                if ( dates != null && dates.length == 2 ) {
                    Calendar startDate = Calendar.getInstance();
                    int index = -1;
                    for (int i = 0; i < months.length; i++) {
                        if ( months[i].equals(dates[0]) ) {
                            index = i;
                            break;
                        }
                    }
                    if ( index != -1 ) {
                        startDate.set(Calendar.YEAR, Integer.parseInt(dates[1]));
                        startDate.set(Calendar.MONTH, index);
                        startDate.set(Calendar.DAY_OF_MONTH, 1);

                        datePicker.setMaxDate(Calendar.getInstance().getTime().getTime());
                    }
                }

            }


            if ( isValidateFields() ) {
                btnDone.setBackground(getResources().getDrawable(R.drawable.reounded_corner_button));
                btnDone.setText("DONE");
            } else {
                if(!kya.equalsIgnoreCase("prof")) {
                    btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));
                    btnDone.setText("SKIP");
                }
            }
            organisationName.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
            natureOfWork.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
            startDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
            endDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
        } else {
            Log.e("----Prof", "is empty");
        }

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDone.setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        btnDone.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){
                    if (btnDone.getText().toString().equalsIgnoreCase("SKIP") && !kya.equalsIgnoreCase("prof")) {
                        if (!TextUtils.isEmpty(dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT))) {
                            long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                            if (selectedid > 0) {
                                dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                            } else {
                                dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                            }
                            Intent intentMessage = new Intent();

                            if (getParent() == null) {
                                setResult(Activity.RESULT_OK, intentMessage);
                            } else {
                                getParent().setResult(RESULT_OK, intentMessage);
                            }
                            finish();

                        } else {

                            ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
                            if (professionals != null && professionals.size() > 0) {
                                Intent intent = new Intent(ProfessionalDetailsActivity.this, ListProfessionalDetailsActivity.class);
                                intent.putExtra("save","no");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                finish();
                            } else {
                                String candidateId = PreferenceUtils.getCandidateId(ProfessionalDetailsActivity.this);
                                if ( !TextUtils.isEmpty(candidateId) ) {

                                    UpdateProfileRequest request = new UpdateProfileRequest(candidateId, ProfessionalDetailsActivity.this);

                                    JsonParser jsonParser = new JsonParser();
                                    JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());

                                    Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ProfessionalDetailsActivity.this, ProfessionalDetailsActivity.this);
                                    processor.updateProfile(candidateId, gsonObject);
                                    PreferenceUtils.setProfileDone(ProfessionalDetailsActivity.this);
                                    finish();
                                }
                                Intent it = new Intent(ProfessionalDetailsActivity.this,MainPlanetActivity.class);
                                it.putExtra("tabs","normal");
                                startActivity(it);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                finish();
                            }
                        }

                    }
                    if ((isValidateFields()||(professional!=null))&& isorgcorrect && isnaturecorrect) {
                        if (!TextUtils.isEmpty(organisationName.getText().toString()) && !TextUtils.isEmpty(natureOfWork.getText().toString()) && !TextUtils.isEmpty(startDate.getText().toString()) /*&& !TextUtils.isEmpty(endDate.getText().toString())*/) {
                            Professional educationalDetails = new Professional();
                            educationalDetails.setOrganizationName(organisationName.getText().toString());
                            educationalDetails.setPositionHeld(natureOfWork.getText().toString());
                            educationalDetails.setFrom(startDate.getText().toString());
                            educationalDetails.setTo(endDate.getText().toString());
                            if(!TextUtils.isEmpty(endDate.getText().toString()) && endDate.getText().toString().length()>2) {

                                if(currentwork){
                                    new PrefManager(ProfessionalDetailsActivity.this).saveCurrentWork("no");
                                }
                            }else{
                                new PrefManager(ProfessionalDetailsActivity.this).saveCurrentWork("yes");
                            }
                            if (professional == null) {
                                dataBaseHelper.createProfessionlDetails(educationalDetails);
                            } else {
                                dataBaseHelper.updateProfession(professional.getId(), educationalDetails);
                                long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                                if (selectedid > 0) {
                                    dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_PROFESSION, "");
                                }
                            }

                        }

                        Intent intent = new Intent(ProfessionalDetailsActivity.this, ListProfessionalDetailsActivity.class);
                        intent.putExtra("save","no");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                        finish();

                    }else {
                        if(btnDone.getText().toString().equalsIgnoreCase("skip")){

                        }else {
                            if(!isorgcorrect && !isnaturecorrect){
                                Toast.makeText(ProfessionalDetailsActivity.this, "Please enter correct organisation and nature of work details", Toast.LENGTH_SHORT).show();
                            }else if(!isorgcorrect){
                                Toast.makeText(ProfessionalDetailsActivity.this, "Please enter correct organisation details", Toast.LENGTH_SHORT).show();

                            }else if(!isnaturecorrect){
                                Toast.makeText(ProfessionalDetailsActivity.this, "Please enter correct nature of work details", Toast.LENGTH_SHORT).show();

                            }else {
                                Toast.makeText(ProfessionalDetailsActivity.this, "Please enter all the details", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }



                }else {
                    Toast.makeText(ProfessionalDetailsActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private void openStartDatePicker() {
        dateclicked="start";
        yearMonthPickerDialog.show();

    }

    private void openEndDatePicker() {
        dateclicked="end";
        yearMonthPickerDialog.show();
    }

    @Override
    public void onDateSelected(int year, int month, int day) {
        if ( dateSelection == 0 ) {
            int monthIndex = month - 1;

           /* if (ISEDIT) {
                if (AppConstant.selectedProfessional != null) {
                    String id = AppConstant.selectedProfessional.getId();
                    if (!TextUtils.isEmpty(id)) {
                        dataBaseHelper.updateTableDetails(id, DataBaseHelper.TABLE_PROFESSION, DataBaseHelper.KEY_PROFESSION_TIME_PERIOD_FROM, months[monthIndex] + "-" + year);
                        dataBaseHelper.updateTableDetails(id, DataBaseHelper.TABLE_PROFESSION, DataBaseHelper.KEY_PROFESSION_TIME_PERIOD_TO, "");
                    }
                    AppConstant.selectedProfessional = null;
                }

            }*/
            startDate.setText(months[monthIndex] + "-" + year);
            startDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
            Calendar startDate = Calendar.getInstance();

            startDate.set(Calendar.YEAR, year);
            startDate.set(Calendar.MONTH, monthIndex);
            startDate.set(Calendar.DAY_OF_MONTH, 1);

            Calendar calendar = Calendar.getInstance();

            calendar.set(Calendar.YEAR, 1990);
            calendar.set(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);


            endDate.setText("Month/Year");
            if ( isValidateFields() ) {
                btnDone.setBackground(getResources().getDrawable(R.drawable.reounded_corner_button));
                btnDone.setText("DONE");
            } else {
                btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));
                btnDone.setText("SKIP");
            }

        } else {
            int monthIndex = month - 1;
            // timePeriod = txtStart.getText().toString() + " to " + months[monthIndex]  + "-" + year;

            /*if (ISEDIT) {
                if (AppConstant.selectedProfessional != null) {
                    String id = AppConstant.selectedProfessional.getId();
                    if (!TextUtils.isEmpty(id))
                        dataBaseHelper.updateTableDetails(id, DataBaseHelper.TABLE_PROFESSION, DataBaseHelper.KEY_PROFESSION_TIME_PERIOD_TO, months[monthIndex] + "-" + year);
                    AppConstant.selectedProfessional = null;
                }
            }*/
            endDate.setText(months[monthIndex] + "-" + year);
            endDate.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
            if ( isValidateFields() ) {
                btnDone.setBackground(getResources().getDrawable(R.drawable.reounded_corner_button));
                btnDone.setText("DONE");
            } else {
                btnDone.setBackground(getResources().getDrawable(R.drawable.rounded_corner_grey_button));
                btnDone.setText("SKIP");
            }

            Calendar startDate = Calendar.getInstance();

            startDate.set(Calendar.YEAR, year);
            startDate.set(Calendar.MONTH, monthIndex);
            startDate.set(Calendar.DAY_OF_MONTH, 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ( requestCode == ORGANIZATION_PICKER_REQUEST_CODE ) {
            if ( resultCode == RESULT_OK ) {
                String phoneNumber = data.getStringExtra("INDUSTRIES");
                String collegeId = data.getStringExtra("id");
                if ( !TextUtils.isEmpty(phoneNumber) ) {
                    Log.e("industry--", phoneNumber);

                    if ( !TextUtils.isEmpty(collegeId) && !collegeId.equalsIgnoreCase("new") ) {
                        organisationName.setText(phoneNumber);
                        organisationName.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));

                    } else {


                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ProfessionalDetailsActivity.this, ProfessionalDetailsActivity.this);
                        processor.addOrganization(phoneNumber);
                    }
                }
            }
        } else if ( requestCode == POSITION_HELD_PICKER_REQUEST_CODE ) {
            if ( resultCode == RESULT_OK ) {
                String college = data.getStringExtra("INDUSTRIES");
                String collegeId = data.getStringExtra("id");
                if ( !TextUtils.isEmpty(college) && !collegeId.equalsIgnoreCase("new") ) {

                    natureOfWork.setText(college);
                    natureOfWork.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));


                } else {
                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ProfessionalDetailsActivity.this, ProfessionalDetailsActivity.this);
                    processor.addPosition(college);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {


        if ( professional != null ) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                            if ( selectedid > 0 ) {
                                dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_PROFESSION, "");
                            }

                            finish();
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(ProfessionalDetailsActivity.this);
            if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
                builder = new AlertDialog.Builder(ProfessionalDetailsActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(ProfessionalDetailsActivity.this);
            }

            builder.setMessage("This action will cancel editing data. Do you want to continue?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        } else

        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if ( guiResponse != null ) {
            if ( status.equals(RequestStatus.SUCCESS) ) {
                if ( guiResponse instanceof AddNewOrganisationResponse) {
                    AddNewOrganisationResponse getAllCollegeResponse = (AddNewOrganisationResponse) guiResponse;
                    if ( getAllCollegeResponse != null && getAllCollegeResponse.getOrganization() != null ) {
                        organisationName.setText(getAllCollegeResponse.getOrganization().getName());
                        organisationName.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
                        org_flag=true;
                        Log.e("click","org");
                    } else {
                        Toast.makeText(ProfessionalDetailsActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                    }
                } else if ( guiResponse instanceof AddNewPositionResponse) {
                    AddNewPositionResponse getAllCollegeResponse = (AddNewPositionResponse) guiResponse;
                    if ( getAllCollegeResponse != null && getAllCollegeResponse.getPositionHeld() != null ) {
                        natureOfWork.setText(getAllCollegeResponse.getPositionHeld().getName());
                        natureOfWork.setTextColor(ContextCompat.getColor(ProfessionalDetailsActivity.this, R.color.green_profile));
                        nature_flag=true;
                        Log.e("click","nature");
                    } else {
                        Toast.makeText(ProfessionalDetailsActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                    }
                }

            } else {
                Toast.makeText(ProfessionalDetailsActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }



    private boolean isValidateFields() {
        int c=0;
        Log.e("values",organisationName.getText().toString());
        Log.e("values",natureOfWork.getText().toString());
        Log.e("values",startDate.getText().toString());
        Log.e("values",endDate.getText().toString());


        boolean isValidate = true;

        isorgcorrect=false;
        if(organisationName.getText().toString().length()>1){
            isorgcorrect=true;
        }else {
            isorgcorrect=false;
        }
        isnaturecorrect=false;
        if(natureOfWork.getText().toString().length()>1){
            isnaturecorrect=true;
        }else {
            isnaturecorrect=false;
        }


        if(org_flag/*organisationName.getText().toString().equalsIgnoreCase("Enter your organization name")*/){
            c++;
        }
        if(nature_flag/*natureOfWork.getText().toString().equalsIgnoreCase("Enter your nature of work")*/){
            c++;
        }
        if(date_flag/*startDate.getText().toString().equalsIgnoreCase("month/year")*/){
            c++;
        }


        Log.e("count",String.valueOf(c));
        if(c==3){
            isValidate=true;
        }else {
            isValidate=false;
        }
        return isValidate;
    }
    public static Calendar stringToCalendar(String stringDate, String datePattern) {
        if (stringDate == null) {
            return null;
        }
        Calendar calendar = new GregorianCalendar();
        try {
            Timestamp newDate = Timestamp.valueOf(stringDate);
            calendar.setTime(newDate);
        }
        catch (Exception e) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
            try {
                calendar.setTime(simpleDateFormat.parse(stringDate));
            }
            catch (ParseException pe) {
                calendar = null;
            }
        }
        return calendar;
    }

}
