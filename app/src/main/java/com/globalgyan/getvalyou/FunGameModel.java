package com.globalgyan.getvalyou;

public class FunGameModel {

    String gamename,subname;

    public FunGameModel() {
    }

    public FunGameModel(String gamename, String subname) {
        this.gamename = gamename;
        this.subname = subname;
    }

    public String getGamename() {
        return gamename;
    }

    public void setGamename(String gamename) {
        this.gamename = gamename;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }
}
