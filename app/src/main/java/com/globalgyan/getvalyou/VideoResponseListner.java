package com.globalgyan.getvalyou;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;

import com.globalgyan.getvalyou.apphelper.AppConstant;

/**
 * Created by Globalgyan on 11-12-2017.
 */

public class VideoResponseListner extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().toString().equalsIgnoreCase("click_notification")) {
            Log.e("message", "enter");
            try {
                if (AppConstant.ishome_active) {
                    new HomeActivity().mTabLayout.getTabAt(2).select();
                    Log.e("receiver", "done");
                } else {
                    Intent intent1 = new Intent(context, HomeActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent1);
                    new HomeActivity().mTabLayout.getTabAt(2).select();

                    new CountDownTimer(500, 500) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                        }
                    }.start();
                }
            } catch (Exception e) {
                Intent intent1 = new Intent(context, HomeActivity.class);
                //intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(intent1);
                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        new HomeActivity().mTabLayout.getTabAt(2).select();

                    }
                }.start();
            }
        }
    }
}
