package com.globalgyan.getvalyou;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.VideoGame.VideoGameActivity;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.response.VideoResponseResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.fragments.Performance;
import com.globalgyan.getvalyou.fragments.VideoPerformanceQuestion;

import com.globalgyan.getvalyou.fragments.VideoPerformanceStarter;
import com.globalgyan.getvalyou.fragments.VideoPre;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.interfaces.RecordTimerUpdator;
import com.globalgyan.getvalyou.interfaces.VideoPerformanceUpdator;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.ValYouApplication.context;
import static com.globalgyan.getvalyou.apphelper.BatteryReceiver.flag_play_anim;
import static com.globalgyan.getvalyou.fragments.VideoPerformanceQuestion.is_ques_timer_running;
import static com.globalgyan.getvalyou.fragments.VideoPerformanceQuestion.vdo_performance_ques_timer;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 4/3/17
 *         Module : Valyou.
 */

public class VideoPerformanceActivity extends AppCompatActivity implements VideoPerformanceUpdator, RecordTimerUpdator, GUICallback, BetterVideoCallback {

    private List<String> questions = null;
    private CountDownTimer countDownTimer;
    private long startTime = 60 * 1000;
    public static int time_value;
    private final long interval = 1 * 1000;
    private static final String USER_AGENT = "ValYou/" + BuildConfig.VERSION_NAME;
    private DataBaseHelper dataBaseHelper = null;
    FontTextView timerrr, dqus, qno;
    static String record_status="";
    public static File saveFolder;
    private int currentIndex = 0;
    private TextView timer = null;
    Dialog dialog, dialog2;
    public static boolean ispreview = false, isrecordstarted = false;
    private RelativeLayout topPanel = null;
    private String objectId = null;
    FontTextView previ, novr, yesvr;
    FontTextView trail_prev, trail_retry,trial_start_mainvr;

    Button bckbtn;
    private final static int CAMERA_RQ = 6969;
    public static AlertDialog alert11;
    boolean checkkk, backkk = false;
    int stopp;
    RelativeLayout mainlayout,optionslayout,videopreviewlayout;
    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;
    View mBottomLayout;
    View mVideoLayout;
    String pathis="",isprev="";
    public static String d_visible="no";
    AlertDialog.Builder builder;
    private BetterVideoPlayer player;
    LinearLayout llff,trail_linear, cover;
    RelativeLayout video;
    String val_contri="";
    // private String id=null;
    ConnectionUtils connectionUtils;
    private static final String TAG = VideoPerformanceActivity.class.getName();
    boolean isactive = true;
    public static Context mcont;
    ImageView close1, close2;
    AlertDialog alertDialog;
    ObjectAnimator astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;

    public Dialog progressDialog;
    boolean flag_pre_close_clicked = false;
    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    int batLevel;
    boolean isPowerSaveMode;
    PowerManager pm;

    AppConstant appConstant;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_performance);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.progressswipe));
        }
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        Log.e("battery", ""+batLevel);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        appConstant = new AppConstant(this);
      /*  if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
*/
        mcont=VideoPerformanceActivity.this;
        Log.e(TAG, "onCreate:");
        timer = (TextView) findViewById(R.id.timer);
        mainlayout=(RelativeLayout)findViewById(R.id.main_layout);
        optionslayout=(RelativeLayout)findViewById(R.id.options_screenis);
        videopreviewlayout=(RelativeLayout)findViewById(R.id.videopreviewlayoutis);
        connectionUtils = new ConnectionUtils(VideoPerformanceActivity.this);
        topPanel = (RelativeLayout) findViewById(R.id.toolbar);
        countDownTimer = new MyCountDownTimer(startTime, interval);
        saveFolder = new File(Environment.getExternalStorageDirectory()+"/Movies/valYou/");

        try {
            objectId = getIntent().getExtras().getString("objectId");
        } catch (Exception ex) {
            Log.e("------", "" + ex.getMessage());
        }
        dataBaseHelper = new DataBaseHelper(this);
        Log.e("video performance", "started");
        /*VideoResponseRequest socialMediaSignInRequest = new VideoResponseRequest(objectId);
        new AsyncTaskExecutor<ValYouRequest, Void, ValYouResponse>().execute(
                new RequestProcessorGet(VideoPerformanceActivity.this,
                        VideoPerformanceActivity.this, true), socialMediaSignInRequest);*/


        ImageButton closeButton = (ImageButton) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialogue();
            }
        });

        if (objectId.equalsIgnoreCase("contri")) {
            val_contri="Contri";
            questions = new ArrayList<>();
            questions.add(0, "contribution");
            //want
            if(new PrefManager(VideoPerformanceActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {
                time_value=6000;


            }else {
                time_value=60;

            }

            launchCamera();

           /* Fragment fragment = VideoPerformanceRecorder.newInstance();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.containerLayout, fragment);
            fragmentTransaction.addToBackStack("VideoPerformanceRecorder");
            fragmentTransaction.commit();*/

        } else if (objectId.equalsIgnoreCase("as a game")) {

        } else {
            ConnectionUtils connectionUtils=new ConnectionUtils(VideoPerformanceActivity.this);
            if(connectionUtils.isConnectionAvailable()) {
                createProgress();
                RetrofitRequestProcessor processor = new RetrofitRequestProcessor(VideoPerformanceActivity.this, VideoPerformanceActivity.this);
                processor.getVideoResponse(objectId);
                registerReceiver();
            }else {
                Toast.makeText(VideoPerformanceActivity.this,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void createProgress() {
        progressDialog = new Dialog(VideoPerformanceActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.planet_loader);
        Window window = progressDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progressDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progressDialog.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progressDialog.setCancelable(false);
        progressDialog.show();

    }


    private void registerReceiver() {
        // TELEPHONY MANAGER class object to register one listner
        context = this;
        TelephonyManager tmgr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);

        //Create Listner
        MyPhoneStateListener PhoneListener = new MyPhoneStateListener();

        // Register listener for LISTEN_CALL_STATE
        tmgr.listen(PhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    private class MyPhoneStateListener extends PhoneStateListener {

        public void onCallStateChanged(int state, String incomingNumber) {

            // Log.d("MyPhoneListener",state+"   incoming no:"+incomingNumber);

            // state = 1 means when phone is ringing
            if (state == 1) {
                try {
                    mVideoView.pause();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class IncomingCall extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {

            try {
                // TELEPHONY MANAGER class object to register one listner
                TelephonyManager tmgr = (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE);

                //Create Listner
                MyPhoneStateListener PhoneListener = new MyPhoneStateListener();

                // Register listener for LISTEN_CALL_STATE
                tmgr.listen(PhoneListener, PhoneStateListener.LISTEN_CALL_STATE);

            } catch (Exception e) {
                Log.e("Phone Receive Error", " " + e);
            }

        }
    }


    @Override
    public String getQuestion() {
        if (currentIndex < questions.size()) {
            return questions.get(currentIndex);
        }
        return null;
    }

    @Override
    public int totalQuestions() {
        return questions.size();
    }

    @Override
    public int currentQuestionIndex() {
        Log.e("++++", "" + currentIndex);
        return currentIndex;
    }

    @Override
    public String getFilePath() {
       /* if(new PrefManager(VideoPerformanceActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {

        }else {*/
        if (currentIndex < questions.size()) {
            return AppConstant.mediaStorageDir.getPath() + File.separator + String.valueOf(currentQuestionIndex()) + ".mp4";
        }

        // }
        return null;
    }

    @Override
    public void updateQuestionIndex() {
        if (currentIndex + 1 < questions.size()) {
            currentIndex++;


            int count = getSupportFragmentManager().getBackStackEntryCount();
            Log.e("count---", "" + count);
            for (int i = count - 1; i > 1; i--) {
                getSupportFragmentManager().popBackStack();
            }

            Fragment fragment = VideoPerformanceQuestion.newInstance();
            if (fragment != null) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.containerLayout, fragment);
                fragmentTransaction.addToBackStack("VideoPerformanceQuestion");
                fragmentTransaction.commit();
            }
        } else {
            dataBaseHelper.deleteVideoResponses();

            ContentValues values = new ContentValues();
            values.put(DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_OBJECT_ID, objectId);
            values.put(DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_STATUS, "uploading");
            values.put(DataBaseHelper.KEY_UPLOAD_VIDEO_ID, "");
            dataBaseHelper.createTable(DataBaseHelper.TABLE_UPLOAD_VIDEO_RESPONSE, values);

            // dataBaseHelper.createTable(DataBaseHelper.TABLE_UPLOAD_VIDEO_RESPONSE, DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_STATUS, "uploading");

            //AppConstant.videoResponseFiles.clear();
            for (int i = 0; i < questions.size(); i++) {
                Log.e("count---", "" + AppConstant.mediaStorageDir.getPath() + File.separator + String.valueOf(i) + ".mp4");
                dataBaseHelper.createVideoResponse(questions.get(i), AppConstant.mediaStorageDir.getPath() + File.separator + String.valueOf(i) + ".mp4");
                // AppConstant.videoResponseFiles.add(AppConstant.mediaStorageDir.getPath() + File.separator + String.valueOf(i) + ".mp4");
            }
            //uploadFile();

            long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
            if (id > 0) {
                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, objectId);
            } else {
                dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, objectId);
            }

            dialog = new Dialog(VideoPerformanceActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.video_pre);
            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            mVideoView = (UniversalVideoView) dialog.findViewById(R.id.videoView);
            mMediaController = (UniversalMediaController) dialog.findViewById(R.id.media_controller);
            mVideoView.setMediaController(mMediaController);
//            mVideoView.setFullscreen(false);
            // mVideoView.start();
            video = (RelativeLayout) dialog.findViewById(R.id.previewvideo);
            cover = (LinearLayout) dialog.findViewById(R.id.cover);
            llff = (LinearLayout) dialog.findViewById(R.id.main_vr_btns);
            trail_linear = (LinearLayout) dialog.findViewById(R.id.trailvr_btns);
            if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                trail_linear.setVisibility(View.VISIBLE);
                llff.setVisibility(View.GONE);
            }else {
                llff.setVisibility(View.VISIBLE);
                trail_linear.setVisibility(View.GONE);

            }
            bckbtn = (Button) dialog.findViewById(R.id.backbtn);
            bckbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    openDialogue();
                    if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                        trail_linear.setVisibility(View.VISIBLE);
                        llff.setVisibility(View.GONE);
                    }else {
                        llff.setVisibility(View.VISIBLE);
                        trail_linear.setVisibility(View.GONE);

                    }

                    video.setVisibility(View.GONE);
                    cover.setVisibility(View.GONE);
                    mVideoView.pause();
                    bckbtn.setVisibility(View.GONE);
                    mMediaController.setVisibility(View.GONE);
                }
            });

            mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                @Override
                public void onScaleChange(boolean isFullscreen) {
                    // this.isFullscreen = isFullscreen;
                    if (isFullscreen) {
//                        ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                        mVideoLayout.setLayoutParams(layoutParams);
//                        //GONE the unconcerned views to leave room for video and controller
//                        mBottomLayout.setVisibility(View.GONE);
                    } else {
//                        ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                        mVideoLayout.setLayoutParams(layoutParams);
//                        mBottomLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onPause(MediaPlayer mediaPlayer) { // Video pause
                    Log.d("LIBAR", "onPause UniversalVideoView callback");
                }

                @Override
                public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                    Log.d("LIBBB", "onStart UniversalVideoView callback");
                }

                @Override
                public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                    Log.d("OPN", "onBufferingStart UniversalVideoView callback");
                }

                @Override
                public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                    Log.d("fobfp", "onBufferingEnd UniversalVideoView callback");
                    if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                        trail_linear.setVisibility(View.VISIBLE);
                        llff.setVisibility(View.GONE);
                    }else {
                        llff.setVisibility(View.VISIBLE);
                        trail_linear.setVisibility(View.GONE);

                    }
                    mMediaController.setVisibility(View.GONE);
                }

            });

            mVideoView.setVideoPath(pathis);
//            videoView.setClickable(false);
//            videoView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    previ.performClick();
//                }
//            });
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    Log.d("fONNONONNONNONONONONO", "onBufferingEnd UniversalVideoView callback");

                    new CountDownTimer(300, 300) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                                trail_linear.setVisibility(View.VISIBLE);
                                llff.setVisibility(View.GONE);
                            }else {
                                llff.setVisibility(View.VISIBLE);
                                trail_linear.setVisibility(View.GONE);

                            }
                            video.setVisibility(View.GONE);
                            cover.setVisibility(View.GONE);
                            bckbtn.setVisibility(View.GONE);
                            mMediaController.setVisibility(View.GONE);
                        }
                    }.start();
                }
            });

            backkk = true;
//            plpa=(Button)dialog.findViewById(R.id.play_pause);
//             checkkk= false;
//            stopp=0;
//            plpa.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(checkkk){
//                      //  mVideoView.pause();
//                        stopp=mVideoView.getCurrentPosition();
//                        Log.e("PAUSED VIDEO",stopp+"");
//                        checkkk=false;
//                    }else{
//                        checkkk=true;
//                        mVideoView.seekTo(stopp);
//                        mVideoView.start();
//                        Log.e("CUTTTE",mVideoView.getCurrentPosition()+"");
//                    }
//
//                }
//            });
//            plpa.setVisibility(View.GONE);
            //main vr
            previ = (FontTextView) dialog.findViewById(R.id.preview);
            novr = (FontTextView) dialog.findViewById(R.id.recordagain);
            yesvr = (FontTextView) dialog.findViewById(R.id.submitvr);
            //trail vr
            trail_prev = (FontTextView) dialog.findViewById(R.id.preview_trail);
            trail_retry = (FontTextView) dialog.findViewById(R.id.retry_trail);
            trial_start_mainvr = (FontTextView) dialog.findViewById(R.id.startrealvr);


            trail_prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trail_prev.setEnabled(false);
                    trail_retry.setEnabled(false);
                    trial_start_mainvr.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            trail_prev.setEnabled(true);
                            trail_retry.setEnabled(true);
                            trial_start_mainvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();
                    ispreview = true;
                    Fragment fragment = VideoPre.newInstance();
                    if (fragment != null) {
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.containerLayout, fragment);
                        fragmentTransaction.addToBackStack("VideoPre");
                        fragmentTransaction.commit();
                    }

                    new CountDownTimer(300, 300) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();


                }
            });



                 trail_retry.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    backkk = false;
                                    trail_retry.setEnabled(false);
                                    trail_prev.setEnabled(false);
                                    trial_start_mainvr.setEnabled(false);
                                    close1.setEnabled(false);
                                    new CountDownTimer(5000, 5000) {
                                        @Override
                                        public void onTick(long l) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            trail_retry.setEnabled(true);
                                            trail_prev.setEnabled(true);
                                            trial_start_mainvr.setEnabled(true);
                                            close1.setEnabled(true);
                                        }
                                    }.start();

                                    startRecordingPageAgain();
                                    new CountDownTimer(200, 200) {
                                        @Override
                                        public void onTick(long l) {

                                        }

                                        @Override
                                        public void onFinish() {

                                            dialog.dismiss();
                        }
                    }.start();

                }
            });


            trial_start_mainvr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backkk = false;
                    trial_start_mainvr.setEnabled(false);
                    trail_prev.setEnabled(false);
                    trail_retry.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            trail_retry.setEnabled(true);
                            trail_prev.setEnabled(true);
                            trial_start_mainvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();
                    new PrefManager(mcont).set_typevr("main");

                    startRecordingPageAgain();
                    new CountDownTimer(200, 200) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();

                }
            });


            close1 = (ImageView) dialog.findViewById(R.id.close);
            close1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    flag_pre_close_clicked = true;
                    close1.setEnabled(false);
                    openDialogue();
//                    novr.setEnabled(false);
//                    yesvr.setEnabled(false);
//                    previ.setEnabled(false);
                    //  finish();

                }
            });
            previ.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    previ.setEnabled(false);
                    novr.setEnabled(false);
                    yesvr.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            previ.setEnabled(true);
                            novr.setEnabled(true);
                            yesvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();
                    ispreview = true;
                    Fragment fragment = VideoPre.newInstance();
                    if (fragment != null) {
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.containerLayout, fragment);
                        fragmentTransaction.addToBackStack("VideoPre");
                        fragmentTransaction.commit();
                    }

                    new CountDownTimer(300, 300) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();


//                    player.setVisibility(View.VISIBLE);
//                    player.start();
//                    llff.setVisibility(View.GONE);

                }
            });
//            if(!videoView.isPlaying()){
//                previ.setText("PREVIEW");
//            }
            yesvr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isprev="no";
                    if (connectionUtils.isConnectionAvailable()) {
                        yesvr.setEnabled(false);
                        novr.setEnabled(false);
                        previ.setEnabled(false);
                        close1.setEnabled(false);
                        new CountDownTimer(5000, 5000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                yesvr.setEnabled(true);
                                novr.setEnabled(true);
                                previ.setEnabled(true);
                                close1.setEnabled(true);
                            }
                        }.start();

                        backkk = false;
                        //dialog.dismiss();
                        new PrefManager(VideoPerformanceActivity.this).saveRetryreponseString("trying");

                        Performance.visiblebars();
                        // fuckme();
                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    } else {
                        Toast.makeText(VideoPerformanceActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                        yesvr.setEnabled(false);
                        novr.setEnabled(false);
                        previ.setEnabled(false);
                        close1.setEnabled(false);
                        new CountDownTimer(3000, 3000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                yesvr.setEnabled(true);
                                novr.setEnabled(true);
                                previ.setEnabled(true);
                                close1.setEnabled(true);
                            }
                        }.start();
                    }

                }
            });
            novr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backkk = false;
                    novr.setEnabled(false);
                    previ.setEnabled(false);
                    yesvr.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            novr.setEnabled(true);
                            previ.setEnabled(true);
                            yesvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();

                    startRecordingPageAgain();
                    new CountDownTimer(200, 200) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();

                }
            });
            dialog.setCancelable(false);
            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            isrecordstarted = false;


            /*Intent int
            ent = new Intent(VideoPerformanceActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/

            //finish();
        }

    }

    public void openDialogue() {
//        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
//                VideoPerformanceActivity.this, R.style.DialogTheme);
//        LayoutInflater inflater = LayoutInflater.from(VideoPerformanceActivity.this);
//        View dialogView = inflater.inflate(R.layout.vdo_quit_warning, null);
//
//        dialogBuilder.setView(dialogView);
//
//
//        alertDialog = dialogBuilder.create();
//
//
//
//        alertDialog.setCancelable(false);
//        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//
//        alertDialog.show();

        dialog2 = new Dialog(VideoPerformanceActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.quit_warning);
        Window window = dialog2.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog2.setCancelable(false);
        dialog2.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final ImageView astro = (ImageView) dialog2.findViewById(R.id.astro);
        final ImageView alien = (ImageView) dialog2.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView no = (ImageView) dialog2.findViewById(R.id.no_quit);
        final ImageView yes = (ImageView) dialog2.findViewById(R.id.yes_quit);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                no.setEnabled(false);
                yes.setEnabled(false);
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        if(flag_pre_close_clicked){
                            close1.setEnabled(true);

                        }
                        else{

                        }

                        dialog2.dismiss();

                        flag_pre_close_clicked = false;
                    }
                }.start();


            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                no.setEnabled(false);
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        if(flag_pre_close_clicked){
                            close1.setEnabled(true);

                        }
                        else{

                        }
                        flag_pre_close_clicked = false;

                        if(is_ques_timer_running)
                        {
                            vdo_performance_ques_timer.cancel();

                        }
                        dialog2.dismiss();



                        isprev="no";

                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                }.start();

            }
        });

        try {
            dialog2.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }

//        new CountDownTimer(3000, 3000) {
//            @Override
//            public void onTick(long l) {
//
//            }
//
//            @Override
//            public void onFinish() {
//                try {
//                    if (alertDialog.isShowing()) {
//                        astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
//                        astro_inx.setDuration(300);
//                        astro_inx.setInterpolator(new LinearInterpolator());
//                        astro_inx.setStartDelay(0);
//                        astro_inx.start();
//                        astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
//                        astro_iny.setDuration(300);
//                        astro_iny.setInterpolator(new LinearInterpolator());
//                        astro_iny.setStartDelay(0);
//                        astro_iny.start();
//
//                        alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
//                        alien_inx.setDuration(300);
//                        alien_inx.setInterpolator(new LinearInterpolator());
//                        alien_inx.setStartDelay(0);
//                        alien_inx.start();
//                        alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
//                        alien_iny.setDuration(300);
//                        alien_iny.setInterpolator(new LinearInterpolator());
//                        alien_iny.setStartDelay(0);
//                        alien_iny.start();
//
//                        new CountDownTimer(400, 400) {
//
//
//                            @Override
//                            public void onTick(long l) {
//
//                            }
//
//                            @Override
//                            public void onFinish() {
//                                alertDialog.dismiss();
//
//
//                            }
//                        }.start();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();


    }


    @Override
    public void onBackPressed() {


        if (new PrefManager(VideoPerformanceActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {
            if(isprev.equalsIgnoreCase("yes")){

            }else {
                openDialogue();
            }
        } else {

            if (backkk) {

            } else {
                int count = getSupportFragmentManager().getBackStackEntryCount();
                if (count == 1) {
                    Log.e("crashfrom","back");
                    openDialogue();
                }
                else {

                    openDialogue();
                }
            }

        }



    }

    @Override
    public void showHideToolbar(boolean status) {
        if (status) {
            topPanel.setVisibility(View.VISIBLE);
        } else {
            topPanel.setVisibility(View.GONE);
        }
    }

    @Override
    public void startRecordingPageAgain() {
        //recording started
        //retake changesr
        isrecordstarted = true;
        if(new PrefManager(VideoPerformanceActivity.this).getvideoFrom().equalsIgnoreCase("contribution")) {
            time_value=6000;


        }else {
            time_value=60;

        }

        launchCamera();

//want
       /* Fragment fragment = VideoPerformanceRecorder.newInstance();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.containerLayout, fragment);
            fragmentTransaction.addToBackStack("VideoPerformanceRecorder");
            fragmentTransaction.commit();
        }*/
    }

    @Override
    public void findis() {
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }

    @Override
    public String getId() {

        return objectId;
    }


    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        try {
            if ((progressDialog.isShowing()) && (progressDialog != null)) {
                progressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if (guiResponse != null) {

            if (status.equals(RequestStatus.SUCCESS)) {
                if (guiResponse != null) {

                    if (guiResponse instanceof VideoResponseResponse) {
                        VideoResponseResponse videoResponseResponse = (VideoResponseResponse) guiResponse;
                        if (videoResponseResponse != null && videoResponseResponse.getQuestions() != null) {
                            questions = videoResponseResponse.getQuestions();
                            for (int i = 0; i < questions.size(); i++) {
                                Log.e("respQues", questions.get(i).toString());
                            }


                            //AppConstant.videoResponseQuestions = questions;
                            VideoPerformanceStarter fragment = VideoPerformanceStarter.newInstance();
                            if (fragment != null) {
                                if (videoResponseResponse.getRecruiter() != null) {
                                    fragment.setDisplayName(videoResponseResponse.getRecruiter().getDisplayName());
                                    Log.e("REC", videoResponseResponse.getRecruiter().get_id());
                                    PrefManager pf = new PrefManager(VideoPerformanceActivity.this);
                                    pf.savePiRID(videoResponseResponse.getRecruiter().get_id());
                                }
                                if (videoResponseResponse.getAnswers() != null && videoResponseResponse.getAnswers().size() > 0)
                                    fragment.setAlreadyReorded(true);
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.containerLayout, fragment);
                                fragmentTransaction.addToBackStack("VideoPerformanceStarter");
                                fragmentTransaction.commitAllowingStateLoss();
                            }
                        }

                    }
                } else
                    Toast.makeText(this, "Server issuse", Toast.LENGTH_SHORT).show();


            } else
                Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpdateTime(String text, float progress) {
        if (!TextUtils.isEmpty(text)) {
            timer.setText(text);
        }
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {
        if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
            trail_linear.setVisibility(View.VISIBLE);
            llff.setVisibility(View.GONE);
        }else {
            llff.setVisibility(View.VISIBLE);
            trail_linear.setVisibility(View.GONE);

        }
        player.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }


        @Override

        public void onFinish() {


        }


        @Override

        public void onTick(long millisUntilFinished) {

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("video performance", "started");

        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void stopProcess() {
        Intent it = new Intent(context, HomeActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        it.putExtra("tabs", "ntf_tab");
        startActivity(it);

        finishAffinity();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if ((dialog != null && dialog.isShowing()) || (!isrecordstarted)) {
            Log.e("Dialogue", "visible");
        } else {
            Log.e("Dialogue", "invisible");
            try {

                if (new PrefManager(VideoPerformanceActivity.this).getRetryresponseString().equalsIgnoreCase("trying") || new PrefManager(VideoPerformanceActivity.this).getRetryresponseString().equalsIgnoreCase("notdone")) {

                    //video is uploading
                } else {
                  /*  Intent it = new Intent(VideoPerformanceActivity.this, HomeActivity.class);
                    it.putExtra("tabs", "notf");
                    finish();
                    startActivity(it);*/


                    //video is not uploading
                    //user click homebutton while recording response
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void actPauseMethod() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(val_contri.equalsIgnoreCase("contri")){
            Log.e("valcontri","yes");
            new CountDownTimer(100, 100) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    val_contri="";
                }
            }.start();
        }else {

            if(record_status.equalsIgnoreCase("startRecording")){

                if(new PrefManager(mcont).getvideoFrom().equalsIgnoreCase("contribution")) {
                    Log.e("valcontri","yes finish");

                    record_status="";
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }else {
                    Log.e("valcontri","no finish");
                    Intent it = new Intent(VideoPerformanceActivity.this, HomeActivity.class);
                    Log.e("tasks","resume");
                    it.putExtra("tabs", "ntf_tab");
                    finish();
                    startActivity(it);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    record_status="";
                }

            }
        }

       /* if(new PrefManager(VideoPerformanceActivity.this).getIsCapturing().equalsIgnoreCase("yes")){
            new PrefManager(VideoPerformanceActivity.this).setIsCapturing("no");
            Intent it = new Intent(context,HomeActivity.class);
            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            it.putExtra("tabs","notf");
            context.startActivity(it);
            finish();
        }*/

    }

    public void previewTasks() {
        if (currentIndex + 1 < questions.size()) {
            currentIndex++;


            int count = getSupportFragmentManager().getBackStackEntryCount();
            Log.e("count---", "" + count);
            for (int i = count - 1; i > 1; i--) {
                getSupportFragmentManager().popBackStack();
            }

            Fragment fragment = VideoPerformanceQuestion.newInstance();
            if (fragment != null) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.containerLayout, fragment);
                fragmentTransaction.addToBackStack("VideoPerformanceQuestion");
                fragmentTransaction.commit();
            }
        } else {
            isprev="yes";
            dataBaseHelper.deleteVideoResponses();

            ContentValues values = new ContentValues();
            values.put(DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_OBJECT_ID, objectId);
            values.put(DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_STATUS, "uploading");
            values.put(DataBaseHelper.KEY_UPLOAD_VIDEO_ID, "");
            dataBaseHelper.createTable(DataBaseHelper.TABLE_UPLOAD_VIDEO_RESPONSE, values);

            // dataBaseHelper.createTable(DataBaseHelper.TABLE_UPLOAD_VIDEO_RESPONSE, DataBaseHelper.KEY_UPLOAD_VIDEO_RESPONSE_STATUS, "uploading");

            //AppConstant.videoResponseFiles.clear();
            for (int i = 0; i < questions.size(); i++) {
                Log.e("count---", "" + AppConstant.mediaStorageDir.getPath() + File.separator + String.valueOf(i) + ".mp4");
                dataBaseHelper.createVideoResponse(questions.get(i), AppConstant.mediaStorageDir.getPath() + File.separator + String.valueOf(i) + ".mp4");
                // AppConstant.videoResponseFiles.add(AppConstant.mediaStorageDir.getPath() + File.separator + String.valueOf(i) + ".mp4");
            }
            //uploadFile();

            long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
            if (id > 0) {
                dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, objectId);
            } else {
                dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_VIDEO_RESPONSE_OBJ_ID, objectId);
            }

            dialog = new Dialog(VideoPerformanceActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.video_pre);
            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            mVideoView = (UniversalVideoView) dialog.findViewById(R.id.videoView);
            mMediaController = (UniversalMediaController) dialog.findViewById(R.id.media_controller);
            mVideoView.setMediaController(mMediaController);
//            mVideoView.setFullscreen(false);
            // mVideoView.start();
            video = (RelativeLayout) dialog.findViewById(R.id.previewvideo);
            cover = (LinearLayout) dialog.findViewById(R.id.cover);
            llff = (LinearLayout) dialog.findViewById(R.id.main_vr_btns);
            trail_linear=(LinearLayout)dialog.findViewById(R.id.trailvr_btns);
            bckbtn = (Button) dialog.findViewById(R.id.backbtn);
            if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                trail_linear.setVisibility(View.VISIBLE);
                llff.setVisibility(View.GONE);
            }else {
                llff.setVisibility(View.VISIBLE);
                trail_linear.setVisibility(View.GONE);

            }
            bckbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                        trail_linear.setVisibility(View.VISIBLE);
                        llff.setVisibility(View.GONE);
                    }else {
                        llff.setVisibility(View.VISIBLE);
                        trail_linear.setVisibility(View.GONE);

                    }
                    video.setVisibility(View.GONE);
                    cover.setVisibility(View.GONE);
                    mVideoView.pause();
                    bckbtn.setVisibility(View.GONE);
                    mMediaController.setVisibility(View.GONE);
                }
            });

            mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                @Override
                public void onScaleChange(boolean isFullscreen) {
                    // this.isFullscreen = isFullscreen;
                    if (isFullscreen) {
//                        ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                        mVideoLayout.setLayoutParams(layoutParams);
//                        //GONE the unconcerned views to leave room for video and controller
//                        mBottomLayout.setVisibility(View.GONE);
                    } else {
//                        ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//                        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                        mVideoLayout.setLayoutParams(layoutParams);
//                        mBottomLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onPause(MediaPlayer mediaPlayer) { // Video pause
                    Log.d("LIBAR", "onPause UniversalVideoView callback");
                }

                @Override
                public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                    Log.d("LIBBB", "onStart UniversalVideoView callback");
                }

                @Override
                public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                    Log.d("OPN", "onBufferingStart UniversalVideoView callback");
                }

                @Override
                public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                    Log.d("fobfp", "onBufferingEnd UniversalVideoView callback");
                    if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                        trail_linear.setVisibility(View.VISIBLE);
                        llff.setVisibility(View.GONE);
                    }else {
                        llff.setVisibility(View.VISIBLE);
                        trail_linear.setVisibility(View.GONE);

                    }
                    mMediaController.setVisibility(View.GONE);
                }

            });

            mVideoView.setVideoPath(pathis);
//            videoView.setClickable(false);
//            videoView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    previ.performClick();
//                }
//            });
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    Log.d("fONNONONNONNONONONONO", "onBufferingEnd UniversalVideoView callback");
                    new CountDownTimer(300, 300) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                                trail_linear.setVisibility(View.VISIBLE);
                                llff.setVisibility(View.GONE);
                            }else {
                                llff.setVisibility(View.VISIBLE);
                                trail_linear.setVisibility(View.GONE);

                            }
                            video.setVisibility(View.GONE);
                            cover.setVisibility(View.GONE);
                            bckbtn.setVisibility(View.GONE);
                            mMediaController.setVisibility(View.GONE);
                        }
                    }.start();
                }
            });

            backkk = true;
//            plpa=(Button)dialog.findViewById(R.id.play_pause);
//             checkkk= false;
//            stopp=0;
//            plpa.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(checkkk){
//                      //  mVideoView.pause();
//                        stopp=mVideoView.getCurrentPosition();
//                        Log.e("PAUSED VIDEO",stopp+"");
//                        checkkk=false;
//                    }else{
//                        checkkk=true;
//                        mVideoView.seekTo(stopp);
//                        mVideoView.start();
//                        Log.e("CUTTTE",mVideoView.getCurrentPosition()+"");
//                    }
//
//                }
//            });
//            plpa.setVisibility(View.GONE);
            previ = (FontTextView) dialog.findViewById(R.id.preview);
            novr = (FontTextView) dialog.findViewById(R.id.recordagain);
            yesvr = (FontTextView) dialog.findViewById(R.id.submitvr);

            //trail vr
            trail_prev = (FontTextView) dialog.findViewById(R.id.preview_trail);
            trail_retry = (FontTextView) dialog.findViewById(R.id.retry_trail);
            trial_start_mainvr = (FontTextView) dialog.findViewById(R.id.startrealvr);


            trail_prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trail_prev.setEnabled(false);
                    trail_retry.setEnabled(false);
                    trial_start_mainvr.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            trail_prev.setEnabled(true);
                            trail_retry.setEnabled(true);
                            trial_start_mainvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();
                    ispreview = true;
                    Fragment fragment = VideoPre.newInstance();
                    if (fragment != null) {
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.containerLayout, fragment);
                        fragmentTransaction.addToBackStack("VideoPre");
                        fragmentTransaction.commit();
                    }

                    new CountDownTimer(300, 300) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();


                }
            });



            trail_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backkk = false;
                    trail_retry.setEnabled(false);
                    trail_prev.setEnabled(false);
                    trial_start_mainvr.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            trail_retry.setEnabled(true);
                            trail_prev.setEnabled(true);
                            trial_start_mainvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();

                    startRecordingPageAgain();
                    new CountDownTimer(200, 200) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();

                }
            });


            trial_start_mainvr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backkk = false;
                    trial_start_mainvr.setEnabled(false);
                    trail_prev.setEnabled(false);
                    trail_retry.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            trail_retry.setEnabled(true);
                            trail_prev.setEnabled(true);
                            trial_start_mainvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();
                    new PrefManager(mcont).set_typevr("main");

                    startRecordingPageAgain();
                    new CountDownTimer(200, 200) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();

                }
            });




            close1 = (ImageView) dialog.findViewById(R.id.close);
            close1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    flag_pre_close_clicked = true;
                    close1.setEnabled(false);
                    openDialogue();
                    //  isprev="no";
//                    novr.setEnabled(false);
//                    yesvr.setEnabled(false);
//                    previ.setEnabled(false);
                    //    finish();

                }
            });
            previ.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int count = getSupportFragmentManager().getBackStackEntryCount();
                    Log.e("count---is", "" + count);

                    isprev="yes";
                    previ.setEnabled(false);
                    novr.setEnabled(false);
                    yesvr.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            previ.setEnabled(true);
                            novr.setEnabled(true);
                            yesvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();
                    ispreview = true;
                    Fragment fragment = VideoPre.newInstance();
                    if (fragment != null) {
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.containerLayout, fragment);
                        fragmentTransaction.addToBackStack("VideoPre");
                        fragmentTransaction.commit();
                    }

                    new CountDownTimer(300, 300) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();


//                    player.setVisibility(View.VISIBLE);
//                    player.start();
//                    llff.setVisibility(View.GONE);

                }
            });
//            if(!videoView.isPlaying()){
//                previ.setText("PREVIEW");
//            }
            yesvr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (connectionUtils.isConnectionAvailable()) {
                        yesvr.setEnabled(false);
                        novr.setEnabled(false);
                        previ.setEnabled(false);
                        close1.setEnabled(false);
                        new CountDownTimer(5000, 5000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                yesvr.setEnabled(true);
                                novr.setEnabled(true);
                                previ.setEnabled(true);
                                close1.setEnabled(true);
                            }
                        }.start();

                        backkk = false;
                        //dialog.dismiss();
                        new PrefManager(VideoPerformanceActivity.this).saveRetryreponseString("trying");

                        Performance.visiblebars();
                        // fuckme();
                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    } else {
                        Toast.makeText(VideoPerformanceActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                        yesvr.setEnabled(false);
                        novr.setEnabled(false);
                        previ.setEnabled(false);
                        close1.setEnabled(false);
                        new CountDownTimer(3000, 3000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                yesvr.setEnabled(true);
                                novr.setEnabled(true);
                                previ.setEnabled(true);
                                close1.setEnabled(true);
                            }
                        }.start();
                    }

                }
            });
            novr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isprev="no";
                    backkk = false;
                    novr.setEnabled(false);
                    previ.setEnabled(false);
                    yesvr.setEnabled(false);
                    close1.setEnabled(false);
                    new CountDownTimer(5000, 5000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            novr.setEnabled(true);
                            previ.setEnabled(true);
                            yesvr.setEnabled(true);
                            close1.setEnabled(true);
                        }
                    }.start();

                    startRecordingPageAgain();
                    new CountDownTimer(200, 200) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            dialog.dismiss();
                        }
                    }.start();

                }
            });
            dialog.setCancelable(false);
            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            isrecordstarted = false;


        }
    }
    @SuppressLint("ResourceType")
    private static void launchCamera() {

        record_status="startRecording";
        if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")){
            new MaterialCamera((Activity) mcont)                               // Constructor takes an Activity
                    .autoSubmit(true)                                 // Whether or not user is allowed to playback videos after recording. This can affect other things, discussed in the next section.
                    .saveDir(saveFolder)                               // The folder recorded videos are saved to
                    .primaryColorAttr(Color.parseColor("#ceffffff"))             // The theme color used for the camera, defaults to colorPrimary of Activity in the constructor
                    .showPortraitWarning(true)                         // Whether or not a warning is displayed if the user presses record in portrait orientation
                    .defaultToFrontFacing(true)                       // Whether or not the camera will initially show the front facing camera
                    .retryExits(false)                                 // If true, the 'Retry' button in the playback screen will exit the camera instead of going back to the recorder
                    .restartTimerOnRetry(false)                        // If true, the countdown timer is reset to 0 when the user taps 'Retry' in playback
                    .videoEncodingBitRate(104000)                     // Sets a custom bit rate for video recording.
                    .continueTimerInPlayback(false)                    // If true, the countdown timer will continue to go down during playback, rather than pausing.
                    //.videoEncodingBitRate(180000)                     // Sets a custom bit rate for video recording.
                    .audioEncodingBitRate(50000)                       // Sets a custom bit rate for audio recording.
                    .videoFrameRate(20)                                // Sets a custom frame rate (FPS) for video recording.
                    .qualityProfile(MaterialCamera.QUALITY_HIGH)       // Sets a quality profile, manually setting bit rates or frame rates with other settings will overwrite individual quality profile settings
                    .videoPreferredHeight(720)                         // Sets a preferred height for the recorded video output.
                    .videoPreferredAspect(3f / 3f)                     // Sets a preferred aspect ratio for the recorded video output.
                    .maxAllowedFileSize(1024 * 1024 * 30)               // Sets a max file size of 5MB, recording will stop if file reaches this limit. Keep in mind, the FAT file system has a file size limit of 4GB.
                    .iconRecord(R.drawable.red_dot)        // Sets a custom icon for the button used to start recording
                    .iconStop(R.drawable.mcam_action_stop)             // Sets a custom icon for the button used to stop recording
                    .iconFrontCamera(R.drawable.mcam_camera_front)     // Sets a custom icon for the button used to switch to the front camera
                    .iconRearCamera(R.drawable.mcam_camera_rear)       // Sets a custom icon for the button used to switch to the rear camera
                    .iconPlay(R.drawable.evp_action_play)              // Sets a custom icon used to start playback
                    //.iconPause(R.drawable.evp_action_pause)            // Sets a custom icon used to pause playback
                    // .iconRestart(R.drawable.evp_action_restart)        // Sets a custom icon used to restart playback
                    //.labelRetry(R.string.mcam_retry)                   // Sets a custom button label for the button used to retry recording, when available
                    // .labelConfirm(R.string.mcam_use_video)             // Sets a custom button label for the button used to confirm/submit a recording
                    .autoRecordWithDelaySec(1)                         // The video camera will start recording automatically after a 5 second countdown. This disables switching between the front and back camera initially.
                    .audioDisabled(false)
                    .autoSubmit(true).allowRetry(false)// Set to true to record video without any audio.
                    .countdownSeconds(10)
                    .start(CAMERA_RQ);

        }else {
            new MaterialCamera((Activity) mcont)                               // Constructor takes an Activity
                    .autoSubmit(true)                                 // Whether or not user is allowed to playback videos after recording. This can affect other things, discussed in the next section.
                    .saveDir(saveFolder)                               // The folder recorded videos are saved to
                    .primaryColorAttr(Color.parseColor("#ceffffff"))             // The theme color used for the camera, defaults to colorPrimary of Activity in the constructor
                    .showPortraitWarning(true)                         // Whether or not a warning is displayed if the user presses record in portrait orientation
                    .defaultToFrontFacing(true)                       // Whether or not the camera will initially show the front facing camera
                    .retryExits(false)                                 // If true, the 'Retry' button in the playback screen will exit the camera instead of going back to the recorder
                    .restartTimerOnRetry(false)                        // If true, the countdown timer is reset to 0 when the user taps 'Retry' in playback
                    .videoEncodingBitRate(104000)                     // Sets a custom bit rate for video recording.
                    .continueTimerInPlayback(false)                    // If true, the countdown timer will continue to go down during playback, rather than pausing.
                    //.videoEncodingBitRate(180000)                     // Sets a custom bit rate for video recording.
                    .audioEncodingBitRate(50000)                       // Sets a custom bit rate for audio recording.
                    .videoFrameRate(20)                                // Sets a custom frame rate (FPS) for video recording.
                    .qualityProfile(MaterialCamera.QUALITY_HIGH)       // Sets a quality profile, manually setting bit rates or frame rates with other settings will overwrite individual quality profile settings
                    .videoPreferredHeight(720)                         // Sets a preferred height for the recorded video output.
                    .videoPreferredAspect(3f / 3f)                     // Sets a preferred aspect ratio for the recorded video output.
                    .maxAllowedFileSize(1024 * 1024 * 30)               // Sets a max file size of 5MB, recording will stop if file reaches this limit. Keep in mind, the FAT file system has a file size limit of 4GB.
                    .iconRecord(R.drawable.red_dot)        // Sets a custom icon for the button used to start recording
                    .iconStop(R.drawable.mcam_action_stop)             // Sets a custom icon for the button used to stop recording
                    .iconFrontCamera(R.drawable.mcam_camera_front)     // Sets a custom icon for the button used to switch to the front camera
                    .iconRearCamera(R.drawable.mcam_camera_rear)       // Sets a custom icon for the button used to switch to the rear camera
                    .iconPlay(R.drawable.evp_action_play)              // Sets a custom icon used to start playback
                    //.iconPause(R.drawable.evp_action_pause)            // Sets a custom icon used to pause playback
                    // .iconRestart(R.drawable.evp_action_restart)        // Sets a custom icon used to restart playback
                    //.labelRetry(R.string.mcam_retry)                   // Sets a custom button label for the button used to retry recording, when available
                    // .labelConfirm(R.string.mcam_use_video)             // Sets a custom button label for the button used to confirm/submit a recording
                    .autoRecordWithDelaySec(5)                         // The video camera will start recording automatically after a 5 second countdown. This disables switching between the front and back camera initially.
                    .autoRecordWithDelayMs(5000)                      // Same as the above, expressed with milliseconds instead of seconds.
                    .audioDisabled(false)
                    .autoSubmit(true).allowRetry(false)// Set to true to record video without any audio.
                    .countdownSeconds(time_value)
                    .start(CAMERA_RQ);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_RQ) {

            if (resultCode == RESULT_OK) {
                Log.e("tasks","resultact");
                record_status="stopRecording";
                String[] msg = data.getDataString().split("/");
                for (int i = 0; i < msg.length; i++) {
                    Log.v("paths :" + i + "", msg[i]);
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 3; i < msg.length; i++) {
                    sb.append("/");
                    sb.append(msg[i].toString());
                }
                String sbuilder = sb.toString();
                sb.setLength(0);
                //sb.append("[");
                sb.append(sbuilder);
                pathis=sb.toString();
                if(new PrefManager(mcont).get_typevr().equalsIgnoreCase("trail")) {
                    new PrefManager(VideoPerformanceActivity.this).set_trailvrpath(sb.toString());
                }else {
                    new PrefManager(VideoPerformanceActivity.this).saveVideoResponseDataPath(sb.toString());
                }
                previewTasks();
            } else if(data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                e.printStackTrace();
                Toast.makeText(VideoPerformanceActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static void startCameraResponse(){
        //call vr changesr
        if(new PrefManager(mcont).getvideoFrom().equalsIgnoreCase("contribution")) {
            time_value=6000;


        }else {
            time_value=60;

        }

        launchCamera();
    }
}
