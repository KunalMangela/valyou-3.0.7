package com.globalgyan.getvalyou;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.ArrayList;
import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Globalgyan on 06-12-2017.
 */


public class LeaderboardAdapter2 extends RecyclerView.Adapter<LeaderboardAdapter2.MyViewHolder> {

    private Context context;
    private ArrayList<LeaderBoardAssgroupModel> leaderboard_names;

    public LeaderboardAdapter2(Context context, ArrayList<LeaderBoardAssgroupModel> leaderboard_names) {
        this.context = context;
        this.leaderboard_names = leaderboard_names;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView name,num;
        ImageView leader_bg;

        public MyViewHolder(View view) {
            super(view);

            name = (FontTextView) view.findViewById(R.id.leaderboard_content);
            num = (FontTextView) view.findViewById(R.id.num);
            leader_bg=(ImageView)view.findViewById(R.id.leader_list_bg);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leaderboard_ass_grp_items, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final LeaderBoardAssgroupModel leaderBoardAssgroupModel=leaderboard_names.get(position);
        holder.name.setSelected(true);
        holder.name.setText(leaderBoardAssgroupModel.getName());
        Typeface tf2 = Typeface.createFromAsset(context.getAssets(), "Gotham-Medium.otf");
        holder.name.setTypeface(tf2);
        final int pos = position+1;

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionUtils connectionUtils=new ConnectionUtils(context);
                if(connectionUtils.isConnectionAvailable()) {
                    LeaderboardActivity.clickis = true;
                    LeaderboardActivity.rankGet(leaderBoardAssgroupModel.getAss_ids(),leaderBoardAssgroupModel.getName());
                }else {
                    Toast.makeText(context,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }
            }
        });

       // holder.num.setText(""+pos);
    }
    @Override
    public int getItemCount() {
        return leaderboard_names.size();
    }
}