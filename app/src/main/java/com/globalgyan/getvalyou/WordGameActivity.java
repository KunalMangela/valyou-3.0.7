package com.globalgyan.getvalyou;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import me.itangqi.waveloadingview.WaveLoadingView;

public class WordGameActivity extends AppCompatActivity implements View.OnClickListener {

    @SuppressLint("StaticFieldLeak")
    RotateAnimation clock, anticlock;
    MyCount counter;
    long timervalue = 60000;
    ImageView pause;
    ImageView img;
    int clickcount = 0;
    int pauseCount = 0;
    String result;
    DataBaseHelper dataBase;
    String txtwrd;
    public ArrayList<String> arrayList = new ArrayList<>();
    public ArrayAdapter<String> adapter;
    EditText wrd;
    Button  back, a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;
    ImageView add;
    TextView error, randomwrd, timer, score,word_count,scorelabel,wordlabel;
    TextView addedword;

    AlertDialog.Builder alertDialog;
    AlertDialog alert;
    Random random = new Random();
    private static final String _CHAR = "abcdefghijklmnopqrstuvwxyz";
    private static final int RANDOM_STR_LENGTH = 1;
    Typeface tf1;
    int counter1, total1;
    long s1;

    double _count = 0;
    WaveLoadingView waveLoadingView;
    boolean no = true,finish_flag=false;
    Handler m_handler;
    Runnable m_handlerTask;
    static double timeleft = 60;
    Boolean firstcall=false;
    String word;
    Dialog dialog_is;
    LinearLayout error_layout;
    String[] words={"name","apple","better","can","dust","eleven","frog","grow","height"};
    ConnectionUtils connectionUtils;
    int minchar=0,one_word_points=0,min_words_points=0,totalscore=0,duration_time=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_game);
     /*   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    */    this.setFinishOnTouchOutside(false);

        minchar=2;

        addedword = (TextView) findViewById(R.id.addedword);
        scorelabel = (TextView) findViewById(R.id.score_label);
        wordlabel=(TextView)findViewById(R.id.words_label);
        addedword = (TextView) findViewById(R.id.addedword);
        error_layout=(LinearLayout)findViewById(R.id.error_layout);

        WaterFallData waterFallData=new WaterFallData();
        waterFallData.execute(AppConstant.Ip_url+"waterfall");
       // word_count = (TextView) findViewById(R.id.count_words);
        img = (ImageView) findViewById(R.id.img2);
        a=(Button)findViewById(R.id.a);
        b=(Button)findViewById(R.id.b);
        c=(Button)findViewById(R.id.c);
        d=(Button)findViewById(R.id.d);
        e=(Button)findViewById(R.id.e);
        f=(Button)findViewById(R.id.f);
        g=(Button)findViewById(R.id.g);
        h=(Button)findViewById(R.id.h);
        i=(Button)findViewById(R.id.i);
        j=(Button)findViewById(R.id.j);
        k=(Button)findViewById(R.id.k);
        l=(Button)findViewById(R.id.l);
        m=(Button)findViewById(R.id.m);
        n=(Button)findViewById(R.id.n);
        o=(Button)findViewById(R.id.o);
        p=(Button)findViewById(R.id.p);
        q=(Button)findViewById(R.id.q);
        r=(Button)findViewById(R.id.r);
        s=(Button)findViewById(R.id.s);
        t=(Button)findViewById(R.id.t);
        u=(Button)findViewById(R.id.u);
        v=(Button)findViewById(R.id.v);
        w=(Button)findViewById(R.id.w);
        x=(Button)findViewById(R.id.x);
        y=(Button)findViewById(R.id.y);
        z=(Button)findViewById(R.id.z);
        back=(Button)findViewById(R.id.back);
        add=(ImageView)findViewById(R.id.add);
        connectionUtils=new ConnectionUtils(this);
     //   word_count.setText("");
        // adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.txtitem, arrayList);
        // listView.setAdapter(adapter);
        error = (TextView) findViewById(R.id.error);
        score = (TextView) findViewById(R.id.score);


        timer = (TextView) findViewById(R.id.timer1);
        pause = (ImageView) findViewById(R.id.pause);
        randomwrd = (TextView) findViewById(R.id.letter);
        randomwrd.setText(getRandomString());
        waveLoadingView = (WaveLoadingView) findViewById(R.id.waveLoadingView);

        pause.setOnClickListener(this);

        wrd = (EditText) findViewById(R.id.word);
        counter = new MyCount(timervalue, 3);
        counter.start();



        //listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        //listView.setStackFromBottom(false);

        tf1 = Typeface.createFromAsset(getAssets(), "fonts/Kidsn.ttf");
        new CallbackTask().execute(inflections());

        error.setTypeface(tf1);

        randomwrd.setTypeface(tf1);
        timer.setTypeface(tf1, Typeface.BOLD);
        score.setTypeface(tf1, Typeface.BOLD);
        a.setTypeface(tf1, Typeface.BOLD);
        b.setTypeface(tf1, Typeface.BOLD);
        c.setTypeface(tf1, Typeface.BOLD);
        d.setTypeface(tf1, Typeface.BOLD);
        e.setTypeface(tf1, Typeface.BOLD);
        f.setTypeface(tf1, Typeface.BOLD);
        g.setTypeface(tf1, Typeface.BOLD);
        h.setTypeface(tf1, Typeface.BOLD);
        i.setTypeface(tf1, Typeface.BOLD);
        j.setTypeface(tf1, Typeface.BOLD);
        k.setTypeface(tf1, Typeface.BOLD);
        l.setTypeface(tf1, Typeface.BOLD);
        m.setTypeface(tf1, Typeface.BOLD);
        n.setTypeface(tf1, Typeface.BOLD);
        o.setTypeface(tf1, Typeface.BOLD);
        p.setTypeface(tf1, Typeface.BOLD);
        q.setTypeface(tf1, Typeface.BOLD);
        r.setTypeface(tf1, Typeface.BOLD);
        s.setTypeface(tf1, Typeface.BOLD);
        t.setTypeface(tf1, Typeface.BOLD);
        u.setTypeface(tf1, Typeface.BOLD);
        v.setTypeface(tf1, Typeface.BOLD);
        w.setTypeface(tf1, Typeface.BOLD);
        x.setTypeface(tf1, Typeface.BOLD);
        y.setTypeface(tf1, Typeface.BOLD);
        z.setTypeface(tf1, Typeface.BOLD);
        wordlabel.setTypeface(tf1, Typeface.BOLD);
        scorelabel.setTypeface(tf1, Typeface.BOLD);
      //  word_count.setTypeface(tf1, Typeface.BOLD);
        back.setTypeface(tf1, Typeface.BOLD);


        addedword.setTypeface(tf1, Typeface.BOLD);

        wrd.setTypeface(tf1, Typeface.BOLD);



        dataBase = new DataBaseHelper(this);


        /* t=new Thread(){


            @Override
            public void run(){

                while(!isInterrupted()){

                    try {
                        Thread.sleep(1000);  //1000ms = 1 sec


                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                _count = _count+1.7;
                                waveLoadingView.setProgressValue((int) _count);
                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        t.start();*/


        m_handler = new Handler();
        m_handlerTask = new Runnable() {
            @Override
            public void run() {
                if (duration_time >= 0) {
                    // do stuff

                    _count = _count + 1.7;
                    waveLoadingView.setProgressValue((int) _count);
                } else {
                    m_handler.removeCallbacks(m_handlerTask); // cancel run
                }
                m_handler.postDelayed(m_handlerTask, 1000);
            }
        };
        m_handlerTask.run();



        wrd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
               // hideKeyboard(WordGameActivity.this);
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    addword();

                }
                return true;
            }
        });
        clock = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);

        clock.setDuration(200);
        clock.setRepeatCount((int) 0.5);

        anticlock = new RotateAnimation(360, 0,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);

        anticlock.setDuration(200);
        anticlock.setRepeatCount((int) 0.5);


    }

    public void dialog() {


        alertDialog = new AlertDialog.Builder(WordGameActivity.this, R.style.MyAlertDialogStyle);

        //     View view = LayoutInflater.from(WordGameActivity.this).inflate(R.layout.custom_alert_dialog, null);

        //   TextView msg = (TextView)view.findViewById(R.id.message);

        // msg.setTypeface(tf1);
        // msg.setText("Are you sure you want to quit this game");
        alertDialog.setMessage("Are you sure you want to quit this game?");
        alertDialog.setCancelable(false);


        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                waveLoadingView.cancelAnimation();// cancel run
                int count = arrayList.size();
                String countno = String.valueOf(count);
                //need
                String score1 = "" + count * 5;


                finish_flag=true;
                Intent intent = new Intent(WordGameActivity.this, ScoreActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                intent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.putExtra("score", String.valueOf(totalscore));
                intent.putExtra("count", String.valueOf(countno));
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                timervalue = s1;
                counter = new MyCount(s1, 1000);
                m_handlerTask.run();
                counter.start();


                waveLoadingView.startAnimation();
            }

        });

        alert = alertDialog.create();
        if(alert.isShowing()){

        }else {
            alert.show();

        }
            }


    @Override
    public void onClick(View view) {
        Log.e("dialogue","click");

        m_handler.removeCallbacks(m_handlerTask);
        counter.cancel();

        waveLoadingView.cancelAnimation();
        try {
            if(alert.isShowing()){

            }else {
                dialog();
            }
        }catch (Exception e){
            e.printStackTrace();
            dialog();
        }


    }




    public class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {


            int count = arrayList.size();
            String countno = String.valueOf(count);
            //need
            String score = "" + count * 5;

           /* SharedPreferences.Editor editor = getSharedPreferences("scoring", MODE_PRIVATE).edit();
            editor.putString("noOfAns", countno);
            editor.putString("score", score);
            editor.apply();*/

            finish_flag=true;
            Intent i = new Intent(WordGameActivity.this, ScoreActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
            i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.putExtra("score", String.valueOf(totalscore));
            i.putExtra("count", String.valueOf(countno));
            startActivity(i);
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            finish();
        }

        @SuppressLint("SimpleDateFormat")
        @Override
        public void onTick(long millisUntilFinished) {
            s1 = millisUntilFinished;
            timer.setText(new SimpleDateFormat("ss").format(new Date(millisUntilFinished)));



        }
    }

    private String inflections() {
        final String language = "en";
        if(firstcall){
             word = wrd.getText().toString().trim();
        }else {
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(8);
            String wordToDisplay = words[randomInt];
             word = wordToDisplay;
        }
        final String word = wrd.getText().toString().trim();
        final String word_id = word.toLowerCase(); //word id is case sensitive and lowercase is required
        return "https://od-api.oxforddictionaries.com:443/api/v1/inflections/" + language + "/" + word_id;
    }


    public String getRandomString() {
        StringBuffer randStr = new StringBuffer();
        for (int i = 0; i < RANDOM_STR_LENGTH; i++) {
            int number = getRandomNumber();
            char ch = _CHAR.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }

    private int getRandomNumber() {
        int randomInt = 0;
        randomInt = random.nextInt(_CHAR.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }


    public void addword() {

        add.setEnabled(false);

        error_layout.setVisibility(View.INVISIBLE);
        if (TextUtils.isEmpty(wrd.getText().toString().trim())) {
            showErrorText("Please enter a word");
            add.setEnabled(true);

        } else if (wrd.getText().toString().trim().length() < minchar) {

            showErrorText("Word must be of minimum "+minchar+" letters!");

            add.setEnabled(true);

        } else if (dataBase.CheckIsWordAlreadyInDBorNot(wrd.getText().toString().trim())) {
            add.setEnabled(true);
            clickcount = clickcount + 1;
            txtwrd = wrd.getText().toString().toLowerCase().trim();
            changeTextSize(txtwrd);
            //  if(listView.getAdapter().getCount() == 0){
            if (arrayList.isEmpty()) {

                String ran = randomwrd.getText().toString().trim();
                int len1 = ran.length();
                int len2 = txtwrd.length();

                final char one = txtwrd.charAt(0);
                final char two = ran.charAt(0);

                final char txtlast = txtwrd.charAt(len2 - 1);
                if (one == two) {

                    String l = String.valueOf(txtlast);


                    //logic
                    //Toast.makeText(this, "Successfully inserted", Toast.LENGTH_LONG).show();
                    measureScore(txtwrd);
                    arrayList.add(txtwrd);


                    String s = score.getText().toString();
                    int i = Integer.parseInt(s);
                    int count = arrayList.size();
                    //need
                    String score1 = "" + count * 5;

                 //   score.setText(score1);
                    scoreAnimation(i, String.valueOf(totalscore), score);


                    error.setText("");

                    if (clickcount % 2 == 0) {
                        addedword.setText(txtwrd);
                        //  addedword.startAnimation(clock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        //  img.startAnimation(clock);
                        // addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));

                        //  head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  head.startAnimation(clock);
                        randomwrd.setText(l);
                        //  randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  randomwrd.startAnimation(clock);
                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    } else {
                        addedword.setText(txtwrd);
                        // addedword.startAnimation(anticlock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        // img.startAnimation(anticlock);
                        //  addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));

                        // head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        //  head.startAnimation(anticlock);

                        randomwrd.setText(l);
                        // randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // randomwrd.startAnimation(anticlock);

                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    }

                    //adapter.notifyDataSetChanged();

                } else
                    showErrorText("Word must start with the given letter");

            } else if (arrayList.contains(txtwrd)) {

                showErrorText("Word already exists!");

            } else {
                String arraylast = arrayList.get(arrayList.size() - 1);
                int len = txtwrd.length();
                int len1 = arraylast.length();
                final char lastLetter = arraylast.charAt(len1 - 1);

                final char firstLetter = txtwrd.charAt(0);
                final char txtlast = txtwrd.charAt(len - 1);

                if (firstLetter == lastLetter) {

                    String l = String.valueOf(txtlast);

                    //logic
                    //Toast.makeText(this, "Successfully inserted", Toast.LENGTH_LONG).show();
                    measureScore(txtwrd);
                    arrayList.add(txtwrd);


                    String s = score.getText().toString();
                    int i = Integer.parseInt(s);
                    int count = arrayList.size();
                    //need
                    String score1 = "" + count * 5;

               //     score.setText(score1);
                    scoreAnimation(i, String.valueOf(totalscore), score);


                    error.setText("");
                    if (clickcount % 2 == 0) {
                        addedword.setText(txtwrd);
                        //  addedword.startAnimation(clock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        //  img.startAnimation(clock);
                        // addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));

                        //  head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  head.startAnimation(clock);
                        randomwrd.setText(l);
                        //  randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  randomwrd.startAnimation(clock);
                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    } else {
                        addedword.setText(txtwrd);
                        // addedword.startAnimation(anticlock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        // img.startAnimation(anticlock);
                        //  addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));

                        // head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        //  head.startAnimation(anticlock);

                        randomwrd.setText(l);
                        // randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // randomwrd.startAnimation(anticlock);

                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    }


                    // adapter.notifyDataSetChanged();


                } else
                    showErrorText("Word must start with the given letter");


            }
        } else {

            if(connectionUtils.isConnectionAvailable()){
                new CallbackTask().execute(inflections());

            }else {
                add.setEnabled(true);
                Toast.makeText(WordGameActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
            }


        }


    }

    private void measureScore(String txtwrd) {
        int length=txtwrd.length();
        int length_more_than_actual_min_count=length-minchar;
        int minchar_score=min_words_points;
        int other_word_score=length_more_than_actual_min_count*one_word_points;
        totalscore=totalscore+minchar_score+other_word_score;
        score.setText(String.valueOf(totalscore));
        Log.e("data_word",txtwrd);
        Log.e("data_length",String.valueOf(txtwrd.length()));
        Log.e("data_score",String.valueOf(minchar_score+other_word_score));
        Log.e("data_total",String.valueOf(totalscore));
        minchar_score=0;
        other_word_score=0;
    }


    @SuppressLint("StaticFieldLeak")
    private class CallbackTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            //TODO: replace with your own app id and app key
            final String app_id = "18135b6e";
            final String app_key = "639fe17c8f82ea0339f387df0e210a92";

            try {

                URL url = new URL(params[0]);
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("app_id", app_id);
                urlConnection.setRequestProperty("app_key", app_key);

                //   code = urlConnection.getResponseCode();

                // read the output from the server
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                StringBuilder stringBuilder = new StringBuilder();

                String line;

                while ((line = reader.readLine()) != null) {


                    stringBuilder.append(line + "\n");


                }

                return stringBuilder.toString();


            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        add.setEnabled(true);

                    }
                });
                return e.toString();
            }
        }

        @Override
        protected void onPostExecute(String rslt) {
            super.onPostExecute(result);
            result = rslt;



            search();

        }

    }


    public void search() {
        if (result.contains("Exception")) {
            if(firstcall){
                showErrorText("Invalid Word!");

            }else {
            }
            firstcall=true;

        } else {
            clickcount = clickcount + 1;
            error.setText("");
            // TODO Auto-generated method stub


            txtwrd = wrd.getText().toString().trim().toLowerCase();
            //  if(listView.getAdapter().getCount() == 0){
            if (arrayList.isEmpty()) {

                String ran = randomwrd.getText().toString().trim();
                int len1 = ran.length();
                int len2 = txtwrd.length();

                final char one = txtwrd.charAt(0);
                final char two = ran.charAt(0);

                final char txtlast =
                        txtwrd.charAt(len2 - 1);
                if (one == two) {

                    String l = String.valueOf(txtlast);


                    //Toast.makeText(this, "Successfully inserted", Toast.LENGTH_LONG).show();
                    //logic
                    measureScore(txtwrd);
                    changeTextSize(txtwrd);
                    arrayList.add(txtwrd);
                    dataBase.insertInWordWaterfall(txtwrd);

               //     word_count.setText(String.valueOf(arrayList.size()));

                    String s = score.getText().toString();
                    int i = Integer.parseInt(s);
                    int count = arrayList.size();
                    //need
                    String score1 = "" + count * 5;

             //       score.setText(score1);
                    scoreAnimation(i, String.valueOf(totalscore), score);


                    error.setText("");

                    if (clickcount % 2 == 0) {
                        addedword.setText(txtwrd);
                        //  addedword.startAnimation(clock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        //  img.startAnimation(clock);
                        // addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));

                        //  head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  head.startAnimation(clock);
                        randomwrd.setText(l);
                        //  randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  randomwrd.startAnimation(clock);
                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    } else {
                        addedword.setText(txtwrd);
                        // addedword.startAnimation(anticlock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        // img.startAnimation(anticlock);
                        //  addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));

                        // head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        //  head.startAnimation(anticlock);

                        randomwrd.setText(l);
                        // randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // randomwrd.startAnimation(anticlock);

                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    }


                    //adapter.notifyDataSetChanged();


                } else
                    showErrorText("Word must start with the given letter");

            } else if (arrayList.contains(txtwrd)) {

                showErrorText("Word already exists!");

            } else {
                String arraylast = arrayList.get(arrayList.size() - 1);
                int len = txtwrd.length();
                int len1 = arraylast.length();
                final char lastLetter = arraylast.charAt(len1 - 1);

                final char firstLetter = txtwrd.charAt(0);
                final char txtlast = txtwrd.charAt(len - 1);

                if (firstLetter == lastLetter) {

                    String l = String.valueOf(txtlast);

                    //Toast.makeText(this, "Successfully inserted", Toast.LENGTH_LONG).show();
                    //logic
                    measureScore(txtwrd);
                    changeTextSize(txtwrd);
                    arrayList.add(txtwrd);
                    dataBase.insertInWordWaterfall(txtwrd);
                    String s = score.getText().toString();
                    int i = Integer.parseInt(s);
                    int count = arrayList.size();
                    //need
                    String score1 = "" + count * 5;

                  //  score.setText(score1);
                    scoreAnimation(i, String.valueOf(totalscore)
                            , score);
                    error.setText("");

                    if (clickcount % 2 == 0) {
                        addedword.setText(txtwrd);
                        //  addedword.startAnimation(clock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        //  img.startAnimation(clock);
                        // addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));

                        //  head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  head.startAnimation(clock);
                        randomwrd.setText(l);
                        //  randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.lefttoright));
                        //  randomwrd.startAnimation(clock);
                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    } else {
                        addedword.setText(txtwrd);
                        // addedword.startAnimation(anticlock);
                        YoYo.with(Techniques.BounceInUp)

                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.addedword));
                        // img.startAnimation(anticlock);
                        //  addedword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // img.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));

                        // head.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        //  head.startAnimation(anticlock);

                        randomwrd.setText(l);
                        // randomwrd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.righttoleft));
                        // randomwrd.startAnimation(anticlock);

                        wrd.setText(l);
                        wrd.setSelection(wrd.getText().toString().trim().length());
                    }


                    // adapter.notifyDataSetChanged();


                } else
                    showErrorText("Word must start with the given letter");


            }

        }
        firstcall=true;
        add.setEnabled(true);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onPause() {
        super.onPause();

        if(finish_flag){

        }else {
            Log.e("dialogue", "pause");

            try {


                if (alert.isShowing() && alert != null) {
                    alert.dismiss();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            ActivityManager activityManager = (ActivityManager) getApplicationContext()
                    .getSystemService(Context.ACTIVITY_SERVICE);

            activityManager.moveTaskToFront(getTaskId(), 0);

            if (pauseCount == 0) {
                m_handler.removeCallbacks(m_handlerTask);

                counter.cancel();
                waveLoadingView.cancelAnimation();
                try {
                    if (alert.isShowing()) {

                    } else {
                        dialog();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog();
                }
                pauseCount++;
            } else {
                m_handler.removeCallbacks(m_handlerTask);


                counter.cancel();
                waveLoadingView.cancelAnimation();
                if (!alert.isShowing()) {
                    try {
                        if (alert.isShowing()) {

                        } else {
                            dialog();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        dialog();
                    }
                } else {

                }

            }

        }

    }
    public void scoreAnimation(int i, String st, final TextView txt){
        Animation expandIn = AnimationUtils.loadAnimation(WordGameActivity.this, R.anim.expand);
        txt.startAnimation(expandIn);


        counter1=i;
        total1 = Integer.parseInt(st);

        new Thread((new Runnable() {
            @Override
            public void run() {

                while (counter1<total1){
                    try{
                        Thread.sleep(102);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    txt.post(new Runnable() {
                        @Override
                        public void run() {

                            txt.setText("" + counter1);


                        }
                    });

                    counter1++;


                }
            }
        })).start();

    }

    public void a(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), a.getText().toString().trim()));
    }
    public void b(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), b.getText().toString().trim()));

    }
    public void c(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), c.getText().toString().trim()));

    }
    public void d(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), d.getText().toString().trim()));

    }
    public void e(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), e.getText().toString().trim()));

    }
    public void f(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().length(), f.getText().toString()));

    }
    public void g(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), g.getText().toString().trim()));

    }
    public void h(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), h.getText().toString().trim()));

    }
    public void i(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), i.getText().toString().trim()));
    }
    public void j(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), j.getText().toString().trim()));

    }
    public void k(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), k.getText().toString().trim()));

    }
    public void l(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), l.getText().toString().trim()));

    }
    public void m(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), m.getText().toString().trim()));
    }
    public void n(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(),n.getText().toString().trim()));
    }
    public void o(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), o.getText().toString().trim()));
    }
    public void p(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), p.getText().toString().trim()));

    }
    public void q(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), q.getText().toString().trim()));

    }
    public void r(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), r.getText().toString().trim()));

    }
    public void s(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), s.getText().toString().trim()));
    }
    public void t(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), t.getText().toString().trim()));

    }
    public void u(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), u.getText().toString().trim()));

    }
    public void v(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), v.getText().toString().trim()));
    }
    public void w(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), w.getText().toString().trim()));

    }
    public void x(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), x.getText().toString().trim()));
    }
    public void y(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), y.getText().toString().trim()));

    }
    public void z(View view) {
        wrd.setText(wrd.getText().insert(wrd.getText().toString().trim().length(), z.getText().toString().trim()));

    }
    public void back(View view) {
        if(wrd.getText().toString().trim().length() == 0){

        }
        else
            wrd.setText(wrd.getText().delete(wrd.getText().toString().trim().length()-1, wrd.getText().toString().trim().length()));

    }
    public void add(final View view) {

        AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
        view.startAnimation(buttonClick);


        addword();

    }
    public void cleartext(View view) {
        wrd.setText("");
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);

    }
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    public  void changeTextSize(String txtwrd){
        if(txtwrd.length()>3){
            long size=dpToPx(25);
            addedword.setTextSize(23);
        }else if(txtwrd.length()>5) {
            long size=dpToPx(20);
            addedword.setTextSize(17);

        }else if(txtwrd.length()>7){
            long size=dpToPx(18);
            addedword.setTextSize(10);
        }else if(txtwrd.length()<4) {
            long size = dpToPx(18);
            addedword.setTextSize(28);
        }
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void showErrorText(String errorstring){
        error_layout.setVisibility(View.VISIBLE);
        error.setText(errorstring);
        error.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    public class WaterFallData extends AsyncTask<String ,String,String >{


        //ProgressDialog progressDialog= new ProgressDialog(LoginActivity.this);


        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection httpURLConnection=null;
            String result="",responcestring="";

            try {
                Thread.currentThread();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            try {
                url=new URL(params[0]);
                httpURLConnection=(HttpURLConnection)url.openConnection();

                responcestring=readStream(httpURLConnection.getInputStream());
                result=responcestring;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(httpURLConnection!=null){
                    httpURLConnection.disconnect();
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            Log.v("LoginAct json", "in");

            try {
                JSONArray js=new JSONArray(s);
                JSONObject job=js.getJSONObject(0);
                minchar= Integer.parseInt(job.getString("ww_min_characters"));
                min_words_points=Integer.parseInt(job.getString("ww_character_score"));
                one_word_points=Integer.parseInt(job.getString("ww_min_score"));
                duration_time=Integer.parseInt(job.getString("ww_max_time"));
            } catch (JSONException e1) {
                e1.printStackTrace();
                minchar= 3;
                min_words_points=3;
                one_word_points=1;
                duration_time=60;
            }


        }
    }

    private String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }


}




