package com.globalgyan.getvalyou;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by NaNi on 13/09/17.
 */

public class AnswerAdapter extends PagerAdapter {

    private List<String> data;
    int qid=0;
    int ans=0;
    int score=0;
    private Context context;
    private LayoutInflater mLayoutInflater;





    public AnswerAdapter(List<String> data, Context context, int qid, int ans) {
        this.data = data;
        mLayoutInflater = LayoutInflater.from(context);
        this.qid=qid;
        this.ans=ans;

    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }
    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view = mLayoutInflater.inflate(R.layout.answer_card, container, false);

        final TextView txt = (TextView) view.findViewById(R.id.ans_opt);
        txt.setText(data.get(position));

        container.addView(view);
        return view;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}