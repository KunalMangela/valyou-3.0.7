package com.globalgyan.getvalyou;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.model.Professional;

import java.util.ArrayList;

/**
 * Created by techniche-android on 10/1/17.
 */

public class ProfileProfessionalAdapter extends RecyclerView.Adapter<ProfileProfessionalAdapter.ViewHolder>{
    Context context = null;
    ArrayList<Professional> professionals = new ArrayList<Professional>();
    private  boolean canEdit = false;
    public ProfileProfessionalAdapter(Context context, ArrayList<Professional> professionals, boolean b) {
        this.context = context;
        this.professionals = professionals;
        this.canEdit = b;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_professional_item,null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Professional education = professionals.get(position);
        holder.txtCompanyName.setText(education.getOrganizationName());
        holder.txtDesignation.setText(education.getPositionHeld());
        String timePeriod = education.getFrom();
        String toDate = education.getTo();
        if(!TextUtils.isEmpty(toDate) && toDate.length()>2) {
            holder.txtTime.setText(timePeriod + " to "+toDate);
        }else{
            new PrefManager(context).saveCurrentWork("yes");

            holder.txtTime.setText(timePeriod + " to current");
        }

    }

    @Override
    public int getItemCount() {
        return professionals!=null ? professionals.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCompanyName;
        TextView txtDesignation;
        TextView txtTime;
        public ViewHolder(View itemView) {
            super(itemView);

            txtCompanyName = (TextView) itemView.findViewById(R.id.profname);
            txtDesignation = (TextView) itemView.findViewById(R.id.desg);
            txtTime = (TextView) itemView.findViewById(R.id.yearsprof);
        }
    }
}
