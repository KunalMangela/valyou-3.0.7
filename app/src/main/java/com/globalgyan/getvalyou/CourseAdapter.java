package com.globalgyan.getvalyou;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.AssessFragment;
import com.globalgyan.getvalyou.simulation.SimulationStartPageActivity;
import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

import static com.thefinestartist.utils.content.ContextUtil.startActivity;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<CourseModel> fungame;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,progress;
        SeekBar progress_seekbar;
        LinearLayout main_linear;
        public MyViewHolder(View view) {
            super(view);

            name=(TextView)view.findViewById(R.id.coursename);
            progress=(TextView)view.findViewById(R.id.progress);
            progress_seekbar=(SeekBar)view.findViewById(R.id.seekBar_luminosite);
            main_linear=(LinearLayout)view.findViewById(R.id.main_linear);

        }
    }


    public CourseAdapter(Context mContext, ArrayList<CourseModel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;


    }

    @Override
    public CourseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.course_litem_ayout, parent, false);



        return new CourseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final CourseModel funModel=fungame.get(position);
        holder.progress_seekbar.setEnabled(false);
        holder.name.setText(funModel.getName());
        Typeface typeface1 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.name.setTypeface(typeface1,Typeface.NORMAL);
        holder.progress_seekbar.setProgress(50);
        holder.progress.setText(String.valueOf(50)+"%");
        holder.progress.setTypeface(typeface1,Typeface.NORMAL);
        // holder.progress.setText(funModel.getName()+" %");

        holder.main_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if(funModel.getContent().length()<6){
                    Toast.makeText(mContext,"No data present in a course",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(mContext, ModulesActivity.class);
                    intent.putExtra("content", funModel.getContent());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                }*/
            }
        });


    }


    @Override
    public int getItemCount() {
        return fungame.size();
    }

    public static void clickCourses(){

    }
}

