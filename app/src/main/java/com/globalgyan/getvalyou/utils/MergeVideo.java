package com.globalgyan.getvalyou.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.WindowManager;

import com.globalgyan.getvalyou.interfaces.MergeUpdator;
import com.googlecode.mp4parser.BasicContainer;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;

public class MergeVideo extends AsyncTask<String, Integer, String> {
    private Context context = null;
    private List<String> sourceFiles = null;
    private MergeUpdator mergeUpdator = null;
    private ProgressDialog progressDialog = null;
    private String outputFile = null;
    public MergeVideo(Context context, List<String> sourceFiles, MergeUpdator mergeUpdator, String outputFile){
        this.sourceFiles = sourceFiles;
        this.context = context;
        this.mergeUpdator = mergeUpdator;
        this.outputFile = outputFile;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        progressDialog.setMessage("please wait...");
        progressDialog.setCancelable(false);
        if (progressDialog != null) {
            progressDialog.show();
        }
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            //String paths[] = new String[sourceFiles.size()];
            Log.e("/////",""+sourceFiles.size());
            Movie[] inMovies = new Movie[sourceFiles.size()];
            String parent = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath();
            //String firstFile = FileUtils.getVideoPathName(FileUtils.VIDEO_FIRST);
            //String secondFile = FileUtils.getVideoPathName(FileUtils.VIDEO_SECOND);
          //  paths[0] =firstFile;
          //  inMovies[0] = MovieCreator.build(paths[0]);
           // paths[1] =secondFile;
           // inMovies[1] = MovieCreator.build(paths[1]);
            int index =0;
            for (String s : sourceFiles) {
                try {
                    Log.e("filename",""+ s);
                    inMovies[index] = MovieCreator.build(s);
                }catch (Exception ex){
                    Log.e("exception",""+ ex.getMessage());
                }
                index++;
            }

            List<Track> videoTracks = new LinkedList<Track>();
            List<Track> audioTracks = new LinkedList<Track>();
            for (Movie m : inMovies) {
                Log.e("-----",""+m.toString());
                for (Track t : m.getTracks()) {
                    if (t.getHandler().equals("soun")) {
                        audioTracks.add(t);
                    }
                    if (t.getHandler().equals("vide")) {
                        videoTracks.add(t);
                    }
                }
            }

            Movie result = new Movie();

            if (audioTracks.size() > 0) {
                result.addTrack(new AppendTrack(audioTracks
                        .toArray(new Track[audioTracks.size()])));
            }
            if (videoTracks.size() > 0) {
                result.addTrack(new AppendTrack(videoTracks
                        .toArray(new Track[videoTracks.size()])));
            }

            BasicContainer out = (BasicContainer) new DefaultMp4Builder()
                    .build(result);

            @SuppressWarnings("resource")
            FileChannel fc = new RandomAccessFile(String.format(outputFile),
                    "rw").getChannel();
            out.writeContainer(fc);
            fc.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (Exception ex){
            Log.e("----",""+ex.getMessage());
        }
        Log.e("Final File---",outputFile);
        return outputFile;
    }



    @Override
    protected void onPostExecute(String value) {
        super.onPostExecute(value);
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        mergeUpdator.onMergeFinished();
     /*   progressDialog.dismiss();
        Intent i = new Intent(Video.this, VideoUpload.class);
        i.putExtra("videopath", value);
        i.putExtra("id", id);
        i.putExtra("name", name);
        i.putExtra("photo", photo);
        startActivity(i);
        finish();*/
    }

}