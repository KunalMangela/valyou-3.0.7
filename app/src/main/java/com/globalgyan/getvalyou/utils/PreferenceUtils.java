package com.globalgyan.getvalyou.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Created by Pranav J.Dev
 * Email : pranavjaydev@gmail.com
 * on 27-09-2016.
 */
public class PreferenceUtils {

    private static final String PREFERNCE_NAME = "ValYou";

    public static final String PREFERNCE_KEY_VALYOU_SCORE = "game_score";


    private static final String CANDIDATE_ID = "candidate_id";
    private static final String PROFILE_UPLOADS = "profile_uploads";
    private static final String PROGRESS_VALUE= "progress_value";
    private static final String NOTIFICATION_COUNT = "NOTIFICATION_COUNT";
    private static final String SEEN_NOTIFICATION = "is_seen_notification";
    private static final String IS_CAMERA_TUTORIAL = "is_tutorial";
    private static final String FCM_TOKEN = "fcm_token";

    public static void setProfileDone(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putBoolean("profileDone", true);
        editor.commit();
    }

    public static boolean isProfileDone(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        return prefs.getBoolean("profileDone", false);
    }

    public static void setVerified(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putBoolean("verified", true);
        editor.commit();
    }
    public static void reSetVerified(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putBoolean("verified", false);
        editor.commit();
    }

    public static void setShowCaseShown(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putBoolean("showCase", true);
        editor.commit();
    }



    public static boolean showCaseShown(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        return prefs.getBoolean("showCase", false);
    }



    public static boolean isVerified(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        return prefs.getBoolean("verified", false);
    }

    public static int getGameCount(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        return prefs.getInt("gameCount", 0);
    }


    public static void setGameCount(Context context, int count){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putInt("gameCount", count);
        editor.commit();
    }

    public static int getInteractionCount(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        return prefs.getInt("interaction", 0);
    }


    public static void setInteractionCount(Context context, int count){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putInt("interaction", count);
        editor.commit();
    }


    public static void setNotification(Context context, int count){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putInt(NOTIFICATION_COUNT, count);
        editor.commit();
    }

    public static int getNotificationCount(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        return prefs.getInt(NOTIFICATION_COUNT, 0);
    }

    public static void setFcmToken(Context context, String token){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putString(FCM_TOKEN, token);
        editor.commit();
    }

    public static String getFcmToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        return prefs.getString(FCM_TOKEN, null);
    }

    public static void setLogout(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.putBoolean("profileDone", false);
        editor.commit();
    }

    public static void clearAll(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }
    public static void removesharedPreferenceKey(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public static String getValueFromSharedPreference(Context context,
                                                      String key) {

        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static boolean setValueInSharedPreference(Context context,
                                                     String key, String value) {

        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (!TextUtils.isEmpty(value))
            editor.putString(key, value);
        else
            editor.putString(key, "");
        return editor.commit();
    }

    public static void setCandidateId(String candidateId, Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CANDIDATE_ID,candidateId);
        editor.commit();
    }

    public static String getCandidateId(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(CANDIDATE_ID,null);
    }

    public static boolean isSeen(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(SEEN_NOTIFICATION,false);
    }

    public static void setSeen(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SEEN_NOTIFICATION,true);
        editor.commit();
    }

    public static void setTutorialSeen(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_CAMERA_TUTORIAL,true);
        editor.commit();
    }

    public static boolean getTutorialSeen(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(IS_CAMERA_TUTORIAL,false);
    }


    public static void setProgress(boolean status,Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PROGRESS_VALUE,status);
        editor.commit();
    }

    public static boolean getProgress(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(PROGRESS_VALUE,false);
    }

   /* public static void setProfileUploads(String uploads,Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFERNCE_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROFILE_UPLOADS,uploads);
        editor.commit();
    }

    public static String getProfileUploads(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                PREFERNCE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(PROFILE_UPLOADS,null);
    }
*/
}