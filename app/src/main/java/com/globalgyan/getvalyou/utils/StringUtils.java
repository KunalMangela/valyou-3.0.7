package com.globalgyan.getvalyou.utils;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 17/2/17
 *         Module : Valyou.
 */

public class StringUtils {


    public static String toCamelCase(String input) {
        String[] words = input.split(" ");
        StringBuilder sb = new StringBuilder();
        if (words[0].length() > 0) {
            sb.append(Character.toUpperCase(words[0].charAt(0)) + words[0].subSequence(1, words[0].length()).toString().toLowerCase());
            for (int i = 1; i < words.length; i++) {
                sb.append(" ");
                try {
                    sb.append(Character.toUpperCase(words[i].charAt(0)) + words[i].subSequence(1, words[i].length()).toString().toLowerCase());
                }catch (Exception ex){

                }
            }
        }

        return sb.toString();
    }
}
