/*
 * Copyright (c) 2016 Techniche E-commerce Solutions Pvt Ltd
 * No.14, 6th Floor,
 * Orchid Techscape, STPI Campus,
 * Cyber Park, Electronics City Phase1,
 * Bangalore-560100.
 *
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Techniche E-commerce
 * Solutions Pvt Ltd. You shall not disclose such Confidential Information and shall use it
 * only in accordance with the terms of the license agreement you entered into with
 * Techniche E-commerce Solutions Pvt Ltd.
 */

package com.globalgyan.getvalyou.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;

/**
 * @author Pranav J.Dev E-mail : pranav@techniche.co
 *         Date : 13/7/16
 *         Module : ShopOnWheelsAgent.
 */
public class ConnectionUtils {

    private static ConnectionUtils instance = null;
    private Context mContext;

    public ConnectionUtils(Context mContext2) {
        mContext = mContext2;
    }

    public synchronized static ConnectionUtils getInstance(Context mContext) {

        if (instance == null) {
            instance = new ConnectionUtils(mContext);
        }
        return instance;
    }

    public boolean isConnectionAvailable() {

        ConnectivityManager mConnectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (mConnectivity != null) {

            NetworkInfo info = mConnectivity.getActiveNetworkInfo();

            if (info != null && info.getState()== NetworkInfo.State.CONNECTED) {

                return true;

            }

        }

        return false;

    }
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }


}
