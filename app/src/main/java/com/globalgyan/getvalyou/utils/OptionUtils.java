package com.globalgyan.getvalyou.utils;

import android.view.View;
import android.widget.TextView;

import com.globalgyan.getvalyou.R;

/**
 * Created by NaNi on 15/09/17.
 */

public class OptionUtils {

    public static void setupItem(final View view, final LibraryObject libraryObject) {
        final TextView txt = (TextView) view.findViewById(R.id.ans_opt);
        txt.setText(libraryObject.getTitle());

    }

    public static class LibraryObject {

        private String mTitle;

        public int getId() {
            return mid;
        }

        public void setId(int id) {
            mid = id;
        }

        private int mid;

        public LibraryObject(final String title, int id) {
            mTitle = title;
            mid=id;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(final String title) {
            mTitle = title;
        }

    }
}
