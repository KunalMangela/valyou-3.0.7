package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.request.UpdateProfileRequest;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by NaNi on 15/10/17.
 */

public class ListProfessionalDetailsActivity extends AppCompatActivity implements GUICallback {

    private RecyclerView professionalDetailsList = null;
    String kya,save;
    private DataBaseHelper dataBaseHelper = null;
    AppCompatButton next,prolistmore;
    ConnectionUtils connectionUtils;
    boolean flag_netcheck=false;
    Dialog progress_profile;
    Typeface tf1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.professional_details_list_fragment);
        dataBaseHelper = new DataBaseHelper(this);
        connectionUtils=new ConnectionUtils(ListProfessionalDetailsActivity.this);
        professionalDetailsList = (RecyclerView) findViewById(R.id.professionalDetails);
        professionalDetailsList.setLayoutManager(new LinearLayoutManager(this));
        tf1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        prolistmore=(AppCompatButton)findViewById(R.id.prolistmore);

        next=(AppCompatButton)findViewById(R.id.prolistnext);
//        progress_profile = KProgressHUD.create(ListProfessionalDetailsActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        prolistmore.setTypeface(tf1);
        next.setTypeface(tf1);
        createProgress();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }

        try{
            kya=getIntent().getExtras().getString("kya");
            save=getIntent().getExtras().getString("save");
        }catch (Exception e){

        }


        if(save.equalsIgnoreCase("yes")){
           next.setText("SAVE");
        }
        findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });


        findViewById(R.id.prolistmore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(ListProfessionalDetailsActivity.this, ProfessionalDetailsActivity.class);
                it.putExtra("kya","prof");
                startActivity(it);
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                //  finish();
            }
        });

        findViewById(R.id.prolistnext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.prolistnext).setEnabled(false);
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        findViewById(R.id.prolistnext).setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){

                    progress_profile.show();
                    final CheckInternetTask t920=new CheckInternetTask();
                    t920.execute();

                    new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            t920.cancel(true);
                            progress_profile.dismiss();
                            if (flag_netcheck) {
                                flag_netcheck = false;



                                if ( !TextUtils.isEmpty(dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT)) ) {
                                    long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                                    if ( selectedid > 0 ) {
                                        dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                                    } else {
                                        dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "");
                                    }
                                    String candidateId = PreferenceUtils.getCandidateId(ListProfessionalDetailsActivity.this);
                                    if ( !TextUtils.isEmpty(candidateId) ) {

                                        UpdateProfileRequest request = new UpdateProfileRequest(candidateId, ListProfessionalDetailsActivity.this);

                                        JsonParser jsonParser = new JsonParser();
                                        JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());

                                        Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ListProfessionalDetailsActivity.this, ListProfessionalDetailsActivity.this);
                                        processor.updateProfile(candidateId, gsonObject);
                                        Intent it = new Intent(ListProfessionalDetailsActivity.this,ProfileActivity.class);
                                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(it);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                    }


                                } else {
                                    String candidateId = PreferenceUtils.getCandidateId(ListProfessionalDetailsActivity.this);

                                    UpdateProfileRequest request = new UpdateProfileRequest(candidateId, ListProfessionalDetailsActivity.this);

                                    JsonParser jsonParser = new JsonParser();
                                    JsonObject gsonObject = (JsonObject) jsonParser.parse(request.getURLEncodedPostdata().toString());

                                    Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(ListProfessionalDetailsActivity.this, ListProfessionalDetailsActivity.this);
                                    processor.updateProfile(candidateId, gsonObject);

                                    PrefManager prefManager = new PrefManager(ListProfessionalDetailsActivity.this);
                                    prefManager.setIsProfileComplete(true);
                                    PreferenceUtils.setProfileDone(ListProfessionalDetailsActivity.this);
                                    Intent it= new Intent(ListProfessionalDetailsActivity.this, MainPlanetActivity.class);
                                    it.putExtra("kya",kya);
                                    it.putExtra("tabs","normal");
                                    startActivity(it);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                }

                            }else {

                                flag_netcheck=false;
                                Toast.makeText(ListProfessionalDetailsActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.start();



            }else {
                Toast.makeText(ListProfessionalDetailsActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

            }

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        loadEducationData();
    }

    private void loadEducationData() {

        ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
        if ( professionals != null && professionals.size() > 0 ) {
            ProfessionalDetailsListAdapter professionalDetailsListAdapter = new ProfessionalDetailsListAdapter(this, professionals);
            professionalDetailsList.setAdapter(professionalDetailsListAdapter);
        } else {
            Intent it = new Intent(ListProfessionalDetailsActivity.this, ProfessionalDetailsActivity.class);
            it.putExtra("kya","prof");
            startActivity(it);
            finish();
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

        }

    }


    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {

            }
        }
    }


    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)ListProfessionalDetailsActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }
    public void createProgress() {
        progress_profile = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        progress_profile.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress_profile.setContentView(R.layout.planet_loader);
        Window window = progress_profile.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progress_profile.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progress_profile.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progress_profile.setCancelable(false);
        //     progress_profile.show();

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
    }
}
