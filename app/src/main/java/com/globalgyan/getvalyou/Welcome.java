package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.fragments.LoginFragment;
import com.globalgyan.getvalyou.fragments.SignUpFragment;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;

import static android.view.View.VISIBLE;
import static com.globalgyan.getvalyou.IntroThree.astro;
import static com.globalgyan.getvalyou.IntroThree.astro_outx;
import static com.globalgyan.getvalyou.IntroThree.astro_outy;
import static com.globalgyan.getvalyou.IntroThree.flag_anim_done3;


import static com.globalgyan.getvalyou.IntroTwo.astro2;
import static com.globalgyan.getvalyou.IntroTwo.flag_anim_done2;
import static com.globalgyan.getvalyou.IntroTwo.scaleAnimation;


/**
 * Created by NaNi on 07/09/17.
 */

public class Welcome extends AppCompatActivity implements LoginFragment.buttonClick, IntroLanguageFragment.lang_chinese, IntroLanguageFragment.lang_spanish, IntroLanguageFragment.lang_english, SignUpFragment.buttonClick2{

    //  private ViewPager mViewPager;
    // public static ParallaxViewPager mViewPager;
    public static CustomViewPager mViewPager;

    RelativeLayout main_bg;
    private Button mNextBtn, mFinishBtn;
    MyViewPagerAdapter mSectionsPagerAdapter;
    public static MyPagerAdapter myPagerAdapter;
    private ImageView zero, one, two, three, four;
    private ImageView[] indicators;
    private PrefManager prefManager;
    FontTextView intro1_title1,intro1_title2,intro2_title1,intro3_title1,i2_text1,i2_text2,i2_text3,i2_text4,
            i3_text1,i3_text2,i3_text3,i3_text4;
    private int currentPosition = 0;
    int lastLeftValue = 0;
    static String token="", lang_name;
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int responseCode,responseCod,responseCo;
    private Timer recordingTimer = null;
    static final String TAG = "PagerActivity";
    int page = 0;
    private int[] layouts;
    HttpURLConnection urlConnection, urlConnection1;
    static InputStream is = null;
    static String json = "",jso="";
    static String intro1_title1_string,intro1_title2_string,intro2_title1_string,intro2_desc1_string,intro2_desc2_string,intro2_desc3_string,
            intro2_desc4_string,intro3_title1_string,intro3_desc1_string,intro3_desc2_string,intro3_desc3_string,intro3_desc4_string;
    static Typeface tf2;

    static ArrayList<String> lang_array = new ArrayList<>();
    Handler handler = new Handler();
    boolean flag_lang_selected = false;

    boolean flag_page3 = false;

    boolean skip_pressed = false;
    String selected_lang ="";
    KProgressHUD loading_popup;

    boolean is_swipe = false;
    int pointer_count= 0;
    IntroLanguageFragment  m1stFragment;
    IntroOne  m2ndFragment;
    IntroTwo  m3rdFragment;
    IntroThree  m4thFragment;
    LoginFragment  m5thFragment;
    SignUpFragment  m6thFragment;

    static boolean startanim = false;
    String gotologingpage="";
    Random randon ;
    boolean flag_signout = false;
    FrameLayout frameLayout;
    static ImageView moon, astro_alien, blue_planet, red_planet, orange_planet, purple_planet;
    AnimatorSet setred1,setred2,setred3,setpurple1,setpurple2;

    android.animation.ObjectAnimator animationorangex,animationorangereversex, animationpurplex, animationpurplereversex, animationearthx, animationearthreversex ;
    float strtangle=0,endangle=180;
    boolean flag_swipe = false;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;

    String skip,login;

    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    public static int heightis,widthis;
    static int batLevel;
    static AppConstant appConstant;
    static boolean isPowerSaveMode;
    static PowerManager pm;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        //  getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        heightis=metrics.heightPixels;
        widthis=metrics.widthPixels;
        Log.e("matrices","height: "+String.valueOf(heightis)+"  "+"width: "+String.valueOf(widthis));
        prefManager = new PrefManager(this);

        appConstant = new AppConstant(this);
      /*  if(Build.VERSION.SDK_INT>=23) {
            checkPermissions();
        }*/

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();


/*
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
*/
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        Log.e("battery", ""+batLevel);

        setupWindowAnimations();

        main_bg = (RelativeLayout)findViewById(R.id.main_bg);


        //  int width = getWindowManager().getDefaultDisplay().getWidth();

        //   int nextX = randon.nextInt(width);
        setred1 = new AnimatorSet();
        setred2 = new AnimatorSet();
        setred3 = new AnimatorSet();
        setpurple1 = new AnimatorSet();
        setpurple2 = new AnimatorSet();

        /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Welcome.this);
         skip = prefs.getString("skip", "Next");
        login = prefs.getString("login", "Login");*/

        skip = "Next";
        login = "Skip";
        blue_planet = (ImageView)findViewById(R.id.blue_planet);

        red_planet = (ImageView)findViewById(R.id.red_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);

        orange_planet = (ImageView)findViewById(R.id.orange_planet);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            startanimation();
        }
        else{

        }
        if (!prefManager.isFirstTimeLaunch()) {
            Intent intent = getIntent();
            gotologingpage = intent.getStringExtra("gotologinpage");
            flag_signout = true;
            mViewPager = (CustomViewPager) findViewById(R.id.container);
            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
            final int sdk = Build.VERSION.SDK_INT;
//            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                mViewPager.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.big_intro_bg));
//            } else {
//                mViewPager.setBackground(ContextCompat.getDrawable(this, R.drawable.big_intro_bg));
//            }
            mViewPager.setAdapter(myPagerAdapter);
        } else {
            flag_signout = false;
            mViewPager = (CustomViewPager) findViewById(R.id.container);
            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
            final int sdk = Build.VERSION.SDK_INT;
//            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                mViewPager.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.big_intro_bg));
//            } else {
//                mViewPager.setBackground(ContextCompat.getDrawable(this, R.drawable.big_intro_bg));
//            }
            mViewPager.setAdapter(myPagerAdapter);
        }

        ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
//                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.rocket_intro1, -0.1f, 0.1f))
                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.singup_button, -0.1f, 0.1f))
                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.login_button, 0.1f, -0.1f));

        mViewPager.setPageTransformer(true, pageTransformer); //set page transformer

        View view1 = LayoutInflater.from(Welcome.this).inflate(R.layout.intro_one, null);
        View view2 = LayoutInflater.from(Welcome.this).inflate(R.layout.intro_two, null);
        View view3 = LayoutInflater.from(Welcome.this).inflate(R.layout.intro_three, null);
        tf2 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");


        layouts = new int[]{
                R.layout.intro_one,
                R.layout.intro_two,
                R.layout.intro_three};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().setStatusBarColor(Color.parseColor("#291C5A"));
        }

      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      */  //  mSectionsPagerAdapter = new MyViewPagerAdapter();



        mNextBtn = (Button) findViewById(R.id.intro_btn_skip);
        mFinishBtn = (Button) findViewById(R.id.intro_btn_finish);


        mNextBtn.setTypeface(tf2);
        mFinishBtn.setTypeface(tf2);


        mNextBtn.setText(skip);
        mFinishBtn.setText(login);


        zero = (ImageView) findViewById(R.id.intro_indicator_0);
        one = (ImageView) findViewById(R.id.intro_indicator_1);
        two = (ImageView) findViewById(R.id.intro_indicator_2);
        three = (ImageView) findViewById(R.id.intro_indicator_3);
        four = (ImageView) findViewById(R.id.intro_indicator_4);

        three.setVisibility(View.GONE);
        four.setVisibility(View.GONE);

        moon = (ImageView) findViewById(R.id.moon);

        moon.setVisibility(View.GONE);
        astro_alien = (ImageView) findViewById(R.id.astro_alien);
        astro_alien.setVisibility(View.GONE);

        frameLayout = (FrameLayout) findViewById(R.id.indicators);



        indicators = new ImageView[]{zero, one, two, three, four};

        // Set up the ViewPager with the sections adapter.
        //   mViewPager = (ViewPager) findViewById(R.id.container);
        intro1_title1 = (FontTextView) view1.findViewById(R.id.title_text1_intro1);
        intro1_title2 = (FontTextView) view1.findViewById(R.id.title_text2_intro1);
        intro2_title1 = (FontTextView) view2.findViewById(R.id.title_text_intro2);
        intro3_title1 = (FontTextView) view3.findViewById(R.id.title_text_intro_page3);
        i2_text1 = (FontTextView) view2.findViewById(R.id.intropage2_text1);
        i2_text2 = (FontTextView) view2.findViewById(R.id.intropage2_text2);
        i2_text3 = (FontTextView) view2.findViewById(R.id.intropage2_text3);
        i2_text4 = (FontTextView) view2.findViewById(R.id.intropage2_text4);
        i3_text1 = (FontTextView) view3.findViewById(R.id.intropage3_text1);
        i3_text2 = (FontTextView) view3.findViewById(R.id.intropage3_text2);
        i3_text3 = (FontTextView) view3.findViewById(R.id.intropage3_text3);
        i3_text4 = (FontTextView) view3.findViewById(R.id.intropage3_text4);


        // mViewPager.setAdapter(mSectionsPagerAdapter);

        // mViewPager.setAdapter(myPagerAdapter);

//        if (recordingTimer != null) {
//            recordingTimer.cancel();
//            recordingTimer = null;
//        }
//        recordingTimer = new Timer();
//        recordingTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        if (currentPosition + 1 < 3)
//                            mViewPager.setCurrentItem(currentPosition + 1, true);
//                    }
//                });
//            }
//        }, 6000, 6000);



        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //if(page!=3) {
                if(position>3){

//                        RotateAnimation rotateAnim = new RotateAnimation(0, endangle,
//                                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
//                                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
//                        rotateAnim.setDuration(700);
//                        rotateAnim.setFillAfter(true);
//                       // rotateAnim.setFillBefore(true);
//                    rotateAnim.setFillEnabled(true);
//                        moon.startAnimation(rotateAnim);
//                    ObjectAnimator moonrotate = ObjectAnimator.ofFloat(moon ,
//                            "rotation", 0f, 180f);
//                    moonrotate.setDuration(700); // miliseconds
//                    moonrotate.start();


                    //    }
                    //  moon.setRotation(360);

                }

                if(position<4){

//                    RotateAnimation rotateAnim = new RotateAnimation(endangle, strtangle,
//                            RotateAnimation.RELATIVE_TO_SELF, 0.5f,
//                            RotateAnimation.RELATIVE_TO_SELF, 0.5f);
//                    rotateAnim.setDuration(700);
////                    rotateAnim.setFillAfter(true);
////                    rotateAnim.setFillBefore(true);
////                    rotateAnim.setFillEnabled(true);
//                    moon.startAnimation(rotateAnim);

//                    if(flag_swipe) {
//                        ObjectAnimator moonrotate = ObjectAnimator.ofFloat(moon,
//                                "rotation", 180f, -90f);
//                        moonrotate.setDuration(700); // miliseconds
//                        moonrotate.start();
//
//                    }

                    //    }
                    //  moon.setRotation(360);

                }
               /* mNextBtn.setVisibility(position >= 2 ? View.GONE : View.VISIBLE);
                mFinishBtn.setVisibility(position == 2 ? View.VISIBLE : View.GONE);*/

                updateIndicators(position);
            }

            @Override
            public void onPageSelected(final int position) {

                flag_page3 = true;



                if (position == 1) {

                    if (astro2.getVisibility() != VISIBLE) {

                        astro2.setVisibility(View.VISIBLE);
                        if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode) || batLevel == 0) {
                            if (!flag_anim_done2) {
                                astro2.startAnimation(scaleAnimation);

                            }

                            scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {


                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {

                                    flag_anim_done2 = true;
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                        }
                    }
                }

                if(position == 2) {
                    if (astro.getVisibility() != VISIBLE) {

                        astro.setVisibility(View.VISIBLE);

                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                            if (!flag_anim_done3) {
                                astro_outx.start();
                                astro_outy.start();
                            }
                            astro_outx.addListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {

                                    flag_anim_done3 = true;
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });


                        }
                    }
                }
                if (position == 3) {
                    if(moon.getVisibility()!=VISIBLE) {
                        skip_pressed = false;
                        mViewPager.setClickable(true);

                        // mNextBtn.setEnabled(true);
                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                            Animation animation = new TranslateAnimation(0, 0, 400, 0);
                            animation.setDuration(500);
                            animation.setFillAfter(true);
                            moon.startAnimation(animation);
                        }

                        else {
                            moon.setVisibility(View.VISIBLE);

                        }
                        //moon.setRotation(0);

                    }
                    moon.setVisibility(View.VISIBLE);
                    astro_alien.setVisibility(View.INVISIBLE);

                    mViewPager.setAllowedSwipeDirection(SwipeDirection.right);
                    frameLayout.setVisibility(View.GONE);

                }
                else if(position == 4){

                    flag_swipe=true;
                    astro_alien.setVisibility(View.VISIBLE);

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                        Animation scaleAnimation = new ScaleAnimation(
                                0f, 1f, // Start and end values for the X axis scaling
                                0f, 1f, // Start and end values for the Y axis scaling
                                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                                Animation.RELATIVE_TO_SELF, 0.5f);
                        scaleAnimation.setDuration(500);
                        // scaleAnimation.setFillAfter(true);


                        astro_alien.startAnimation(scaleAnimation);

                    }


                    //moon.setVisibility(View.VISIBLE);
                    mViewPager.setAllowedSwipeDirection(SwipeDirection.left);
                    frameLayout.setVisibility(View.GONE);
                }/*else {
                    currentPosition = position;
                }*/

//                recordingTimer.cancel();
//                recordingTimer = new Timer();
//                recordingTimer.schedule(new TimerTask() {
//                    @Override
//                    public void run() {
//                        runOnUiThread(new Runnable() {
//                            public void run() {
//                                if (currentPosition + 1 < 3) {
//                                    page=currentPosition+1;
//                                  //  mNextBtn.setVisibility(position == 2 ? View.GONE : View.VISIBLE);
//                                   // mFinishBtn.setVisibility(position == 3 ? View.VISIBLE : View.GONE);
//                                    //mViewPager.setCurrentItem(currentPosition + 1, true);
//                                }
//                            }
//                        });
//                    }
//                }, 6000, 6000);


                currentPosition = position;
                Log.e("currentPosition", "" + currentPosition);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });


        if (flag_signout) {
//            launchHomeScreen();
            //    new PrefManager(Welcome.this).setFirstTimeLaunch(false);


            flag_signout = true;
            flag_lang_selected = true;


            page = 3;

            new CountDownTimer(50, 50) {
                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    //  loading_popup.dismiss();
                    mViewPager.setCurrentItem(page, false);
                }
            }.start();


            updateIndicators(page);
            myPagerAdapter.notifyDataSetChanged();
            //   finish();
        } else {

            mViewPager.setCurrentItem(page);

            updateIndicators(page);
            //      getData();

        }


        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("clicked","clicked");
                // mNextBtn.setEnabled(false);

                mViewPager.setClickable(false);
                skip_pressed = true;



              /*  new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        mNextBtn.setEnabled(true);
                    }
                }.start();*/

                // launchHomeScreen();
                //      new PrefManager(Welcome.this).setFirstTimeLaunch(false);


                int nxt_page = currentPosition+1;
                mViewPager.setCurrentItem(nxt_page, true);

                if(nxt_page==3) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        checkPermissions();

                    }
                }



            }
        });

        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // launchHomeScreen();
                //   new PrefManager(Welcome.this).setFirstTimeLaunch(false);


                mViewPager.setCurrentItem(3, true);
                if(Build.VERSION.SDK_INT>=23) {
                    checkPermissions();
                }


            }
        });


//
//            final View touchView = findViewById(R.id.container);
//            touchView.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    if (flag_lang_selected) {
//                        return false;
//                    } else
//                        return true;
//                }
//
//            });

        /*main_bg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (skip_pressed) {
                    Log.e("skipped, touched","skipped");
                    return true;
                }
                else
                    return false;
            }
        });*/

        final View touchView = findViewById(R.id.container);
        touchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                pointer_count = event.getPointerCount();
               /* Log.e("TEST", "Raw event: " + event.getAction() + ", (" + event.getRawX() + ", " + event.getRawY() + ")");
                gestureDetector.onTouchEvent(event);*/
                if (flag_page3 && detectSwipeToRight(event)) {
                    return true;
                }

            /*    else if(is_swipe){
                    is_swipe = false;
                    return true;
                }*/
                else/*{
                    is_swipe = true;
                }*/
                    return false;

            }

        /*    private GestureDetector gestureDetector = new GestureDetector(Welcome.this, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    Log.e("TEST", "onDoubleTap");

                    if(e.getAction() != MotionEvent.ACTION_UP) {
                        is_swipe = true;
                        return false;  // Don't do anything for other actions
                    }
                    else return true;

                }
            });
*/

        });




    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)

    private void setupWindowAnimations() {
        Slide slide = new Slide();
        slide.setDuration(1000);
        getWindow().setExitTransition(slide);
    }
    //    @Override
//    public boolean onInterceptTouchEvent(MotionEvent event) {
//        if (this.enabled && detectSwipeToRight(event)) {
//            return super.onInterceptTouchEvent(event);
//        }
//
//        return false;
//    }
    public boolean detectSwipeToRight(MotionEvent event){

        int initialXValue = 0; // as we have to detect swipe to right
        final int SWIPE_THRESHOLD = 100; // detect swipe
        boolean result = false;

        try {
            float diffX = event.getX() - initialXValue;

            if (Math.abs(diffX) > SWIPE_THRESHOLD ) {
                if (diffX > 0) {
                    // swipe from left to right detected ie.SwipeRight
                    result = false;
                } else {
                    // swipe from right to left detected ie.SwipeLeft
                    result = true;
                }
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    @Override
    public void buttonClicked1(View v) {

        mViewPager.setCurrentItem(4, true);
        if(Build.VERSION.SDK_INT>=23) {
            checkPermissions();
        }


    }

    @Override
    public void buttonClicked2(View v) {

        mViewPager.setCurrentItem(3, true);
        if(Build.VERSION.SDK_INT>=23) {
            checkPermissions();
        }

    }



    @Override
    public void clicked_span_lang(View v) {
        //  selected_lang = txt1;
        selected_lang = lang_array.get(0);

        flag_lang_selected = true;
        new GetIntroInfo().execute();
    }

    @Override
    public void clicked_chi_lang(View v) {

        //   selected_lang = txt2;
        selected_lang = lang_array.get(1);
        flag_lang_selected = true;
        new GetIntroInfo().execute();    }


    @Override
    public void clicked_eng_lang(View v) {
        //  selected_lang = txt3;
        selected_lang = lang_array.get(2);

        flag_lang_selected = true;
        new GetIntroInfo().execute();    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {


            switch (pos) {

                case 0:
                    return new IntroOne();
                case 1:
                    return new IntroTwo();
                case 2:
                    return new IntroThree();
                case 3:

                    return new LoginFragment();

                case 4:

                    return new SignUpFragment();

            }

            return null;

        }

//        @Override
//        public Fragment getItem(int pos) {
//            switch(pos) {
//
////                case 0: return IntroOne.newInstance("FirstFragment, Instance 1");
////                case 1: return IntroTwo.newInstance("SecondFragment, Instance 1");
////                case 2: return IntroThree.newInstance("ThirdFragment, Instance 1");
////                //default: return new IntroOne();
////
////                default: return IntroOne.newInstance("firstfrag, Default");
//                case 0:if(m1stFragment == null)
//                    m1stFragment = new IntroLanguageFragment();
//                    return m1stFragment;
//                case 1:
//                    if(m2ndFragment == null)
//                        m2ndFragment = new IntroOne();
//                    return m2ndFragment;
//                case 2:
//                    if(m3rdFragment == null)
//                        m3rdFragment = new IntroTwo();
//                    return m1stFragment;
//                case 3:
//                    if(m4thFragment == null)
//                        m4thFragment = new IntroThree();
//                    return m4thFragment;
//                case 4:
//                    if(m5thFragment == null)
//                        m5thFragment = new LoginFragment();
//                    return m5thFragment;
//
//                case 5:
//                    if(m6thFragment == null)
//                        m6thFragment = new SignUpFragment();
//                    return m6thFragment;
//
//                //default: return new IntroOne();
//
//                default: return new IntroOne();
//            }
//        }
//        @Override
//        public Object instantiateItem(ViewGroup container, int position) {
//            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
//            // save the appropriate reference depending on position
//            switch (position) {
//                case 0:
//                      m1stFragment = (IntroLanguageFragment) createdFragment;
//                    String firstTag = createdFragment.getTag();
//
//                    break;
//                case 1:
//                      m2ndFragment = (IntroOne) createdFragment;
//                    String sec = createdFragment.getTag();
//
//                    break;
//                case 2:
//                      m3rdFragment = (IntroTwo) createdFragment;
//                    String thrd = createdFragment.getTag();
//
//                    break;
//                case 3:
//                      m4thFragment = (IntroThree) createdFragment;
//                    String fourth = createdFragment.getTag();
//
//                    break;
//                case 4:
//
//                      m5thFragment = (LoginFragment) createdFragment;
//                    String fifth = createdFragment.getTag();
//
//                    break;
//                case 5:
//                      m6thFragment = (SignUpFragment) createdFragment;
//                    String six = createdFragment.getTag();
//
//                    break;
//
//            }
//            return createdFragment;
//        }

        @Override
        public int getCount() {

            return 5;
            /// return 6;

        }
    }
    private void getData() {
        new GetToken().execute();
    }

    private class GetToken extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream1(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jObj = new JSONObject(result1);
                token=jObj.getString("access_token");
                String token_type=jObj.getString("token_type");
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(responseCod != urlConnection.HTTP_OK){

                Toast.makeText(Welcome.this,"Error while loading user data",Toast.LENGTH_SHORT).show();
            }else
            {


                new GetLanguages().execute();


            }

        }
    }

    private class GetLanguages extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            String result1 = "";
            try {


                try {



                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/get_languages");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");



                    responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.v("Response", responseString);
                    result1 = responseString;



                    JSONArray lang_data = new JSONArray(result1);

                    for (int i = 0; i < lang_data.length(); i++) {

                        JSONObject jo = lang_data.getJSONObject(i);

                        // Storing each json item in variable
                        int l_id = jo.getInt("l_id");
                        lang_name = jo.getString("language_name");
                        String country = jo.getString("country");
                        String status = jo.getString("status");


                        // show the values in our logcat
                        Log.e("api lang data ", "l_id: " + l_id
                                + ", language_name: " + lang_name
                                + ", country: " + country + ", status: " + status);

                        lang_array.add(lang_name);

                    }





//                    myPagerAdapter.notifyDataSetChanged();

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // mViewPager.setOffscreenPageLimit(5);
//            if(gotologingpage.equals("loginpage")&&!prefManager.isFirstTimeLaunch()){
//
//                mViewPager.setCurrentItem(page);
//                mViewPager.setAdapter(myPagerAdapter);
//
//                loading_popup.dismiss();
//            }
//
//            else {
            mViewPager.setAdapter(myPagerAdapter);
            ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.singup_button, -1f, 1f));
            //.addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.rocket_intro2, 1f, -1f));


            mViewPager.setPageTransformer(true, pageTransformer); //set page transformer
            loading_popup.dismiss();


            //  }
            // myPagerAdapter.notifyDataSetChanged();

        }

    }


//    private class GetIntroInfo extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//
//            loading_popup.show();
//            super.onPreExecute();
//
//        }
//
//
//        @Override
//        protected String doInBackground(String... urlkk) {
//            String result1 = "";
//            try {
//
//
//                try {
//                    // httpClient = new DefaultHttpClient();
//                    // httpPost = new HttpPost("http://35.154.93.176/Player/TodayGames");
//
//
//                    String jon = new JsonBuilder(new GsonAdapter())
//                            .object("data")
//                            .object("language_name", ""+selected_lang)
//                            .build().toString();
//
//
//
//                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/get_languages_info");
//                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
//                    urlConnection1.setDoOutput(true);
//                    urlConnection1.setFixedLengthStreamingMode(
//                            jon.getBytes().length);
//                    urlConnection1.setRequestProperty("Content-Type", "application/json");
//                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
//                    urlConnection1.setRequestMethod("POST");
//
//
//                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
//                    wr.write(jon);
//                    wr.flush();
//
//                    responseCode = urlConnection1.getResponseCode();
//
//
//                    is= urlConnection1.getInputStream();
//
//                    String responseString = readStream(is);
//                    Log.v("Response", responseString);
//                    result1 = responseString;
//
//
//
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                } catch (ClientProtocolException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            finally {
//                if (urlConnection1 != null)
//                    urlConnection1.disconnect();
//            }
//
//            return result1;
//        }
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            try {
//
//
//                JSONObject jsonObject=new JSONObject(s);
//                JSONArray jsonArrayIntro1_title1=jsonObject.getJSONArray("intropageTitles_1");
//                 intro1_title1_string=jsonArrayIntro1_title1.get(0).toString();
//                 intro1_title2_string=jsonArrayIntro1_title1.get(1).toString();
//                JSONArray jsonArrayIntro2_title1=jsonObject.getJSONArray("intropageTitles_2");
//                 intro2_title1_string=jsonArrayIntro2_title1.get(0).toString();
//                JSONArray jsonArrayIntro2_Desc=jsonObject.getJSONArray("intropageDescription_2");
//                 intro2_desc1_string=jsonArrayIntro2_Desc.get(0).toString();
//                 intro2_desc2_string=jsonArrayIntro2_Desc.get(1).toString();
//                 intro2_desc3_string=jsonArrayIntro2_Desc.get(2).toString();
//                 intro2_desc4_string=jsonArrayIntro2_Desc.get(3).toString();
//                JSONArray jsonArrayIntro3_title=jsonObject.getJSONArray("intropageTitles_3");
//                 intro3_title1_string=jsonArrayIntro3_title.get(0).toString();
//                JSONArray jsonArrayIntro3_Desc=jsonObject.getJSONArray("intropageDescription_3");
//                 intro3_desc1_string=jsonArrayIntro3_Desc.get(0).toString();
//                 intro3_desc2_string=jsonArrayIntro3_Desc.get(1).toString();
//                 intro3_desc3_string=jsonArrayIntro3_Desc.get(2).toString();
//                 intro3_desc4_string=jsonArrayIntro3_Desc.get(3).toString();
//
//               // mViewPager.setBackgroundResource(R.drawable.bg_intro);
//             //   setContentView(mViewPager);
//              //  String [] h2=intro2_title1_string.split(" ");
//              //  String lastOne = h2[h2.length-1];
//
//                if(!selected_lang.equals("Chinese")) {
//                    colored_str_in_intro2 = new SpannableString(intro2_title1_string);
//                    String firstWord1 = intro2_title1_string.substring(0, intro2_title1_string.lastIndexOf(" "));
//                    colored_str_in_intro2.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord1.length(), intro2_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//                    colored_str_in_intro3 = new SpannableString(intro3_title1_string);
//                    String firstWord2 = intro3_title1_string.substring(0, intro3_title1_string.lastIndexOf(" "));
//                    colored_str_in_intro3.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord2.length(), intro3_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//                }
//                else{
//
//                    colored_str_in_intro2 = new SpannableString(intro2_title1_string);
//                    colored_str_in_intro3 = new SpannableString(intro3_title1_string);
//                    String firstWord2 = intro3_title1_string.substring(0, intro3_title1_string.lastIndexOf(" "));
//                    colored_str_in_intro3.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord2.length(), intro3_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//
//                }
//              // mViewPager.setAdapter(myPagerAdapter);
//                myPagerAdapter.notifyDataSetChanged();
//
//                new Handler().post(new Runnable() {
//                    @Override
//                    public void run() {
//                        mViewPager.setCurrentItem(1);
//                    }
//                });
//                new CountDownTimer(500, 500) {
//                    @Override
//                    public void onTick(long l) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                      loading_popup.dismiss();
//                    }
//                }.start();
//
////                SpannableStringBuilder SS1 = new SpannableStringBuilder(intro1_title1_string);
////                SS1.setSpan(new CustomTypefaceSpan("", tf2), 0, intro1_title1_string.length()-1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//    }


    private class GetIntroInfo extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            loading_popup.show();
            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {
                    // httpClient = new DefaultHttpClient();
                    // httpPost = new HttpPost("http://35.154.93.176/Player/TodayGames");


                    String jon = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", "")
                            .object("language_name", ""+selected_lang)
                            .build().toString();



                    URL urlToRequest = new URL(AppConstant.Ip_url+"GameSettings");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jon.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jon);
                    wr.flush();

                    responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.v("Response", responseString);
                    result1 = responseString;



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {



                JSONObject jsonObject1=new JSONObject(s);

                String jsonObj = jsonObject1.getString("language_json");
                JSONObject jsonObject=new JSONObject(jsonObj);
                JSONArray jsonArrayIntro1_title1=jsonObject.getJSONArray("intropageTitles_1");
                intro1_title1_string=jsonArrayIntro1_title1.get(0).toString();
                intro1_title2_string=jsonArrayIntro1_title1.get(1).toString();
                JSONArray jsonArrayIntro2_title1=jsonObject.getJSONArray("intropageTitles_2");
                intro2_title1_string=jsonArrayIntro2_title1.get(0).toString();
                JSONArray jsonArrayIntro2_Desc=jsonObject.getJSONArray("intropageDescription_2");
                intro2_desc1_string=jsonArrayIntro2_Desc.get(0).toString();
                intro2_desc2_string=jsonArrayIntro2_Desc.get(1).toString();
                intro2_desc3_string=jsonArrayIntro2_Desc.get(2).toString();
                intro2_desc4_string=jsonArrayIntro2_Desc.get(3).toString();
                JSONArray jsonArrayIntro3_title=jsonObject.getJSONArray("intropageTitles_3");
                intro3_title1_string=jsonArrayIntro3_title.get(0).toString();
                JSONArray jsonArrayIntro3_Desc=jsonObject.getJSONArray("intropageDescription_3");
                intro3_desc1_string=jsonArrayIntro3_Desc.get(0).toString();
                intro3_desc2_string=jsonArrayIntro3_Desc.get(1).toString();
                intro3_desc3_string=jsonArrayIntro3_Desc.get(2).toString();
                intro3_desc4_string=jsonArrayIntro3_Desc.get(3).toString();




////                if(!selected_lang.equals("中国")) {
////                    colored_str_in_intro2 = new SpannableString(intro2_title1_string);
////                    String firstWord1 = intro2_title1_string.substring(0, intro2_title1_string.lastIndexOf(" "));
////                    colored_str_in_intro2.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord1.length(), intro2_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
////
////                    colored_str_in_intro3 = new SpannableString(intro3_title1_string);
////                    String firstWord2 = intro3_title1_string.substring(0, intro3_title1_string.lastIndexOf(" "));
////                    colored_str_in_intro3.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord2.length(), intro3_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
////
////
////                }
////                else{
////
////                    colored_str_in_intro2 = new SpannableString(intro2_title1_string);
////                    colored_str_in_intro3 = new SpannableString(intro3_title1_string);
////                    String firstWord2 = intro3_title1_string.substring(0, intro3_title1_string.lastIndexOf(" "));
////                    colored_str_in_intro3.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord2.length(), intro3_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
////
////
////                }
//                SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(Welcome.this).edit();
//                prefEditor.putString("intro1_title1_string", intro1_title1_string);
//                prefEditor.putString("intro1_title2_string", intro1_title2_string);
//                prefEditor.putString("colored_str_in_intro2", String.valueOf(colored_str_in_intro2));
//                prefEditor.putString("intro2_desc1_string", intro2_desc1_string);
//                prefEditor.putString("intro2_desc2_string", intro2_desc2_string);
//                prefEditor.putString("intro2_desc3_string", intro2_desc3_string);
//                prefEditor.putString("intro2_desc4_string", intro2_desc4_string);
//                prefEditor.putString("colored_str_in_intro3", String.valueOf(colored_str_in_intro3));
//                prefEditor.putString("intro3_desc1_string", intro3_desc1_string);
//                prefEditor.putString("intro3_desc2_string", intro3_desc2_string);
//                prefEditor.putString("intro3_desc4_string", intro3_desc4_string);
//                prefEditor.apply();




                myPagerAdapter.notifyDataSetChanged();

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mViewPager.setCurrentItem(1);
                    }
                });
                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        loading_popup.dismiss();
                    }
                }.start();

//                SpannableStringBuilder SS1 = new SpannableStringBuilder(intro1_title1_string);
//                SS1.setSpan(new CustomTypefaceSpan("", tf2), 0, intro1_title1_string.length()-1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
    private String readStream1(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {

                sb.append(line);
            }
            is.close();

            jso = sb.toString();


            Log.e("JSONStrr", jso);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return jso;
    }



    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(i == position ? R.drawable.highlighted_dot_intro : R.drawable.dark_purple_dot);
//
        }
    }
    private void launchHomeScreen() {
        new PrefManager(Welcome.this).setFirstTimeLaunch(false);
        //do changes
        startActivity(new Intent(Welcome.this, LoginSignupActivity.class));
        finish();
        //   mViewPager.setCurrentItem(4,false);

    }


    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);



            View view = layoutInflater.inflate(layouts[position], container, false);
//            if(position==2){
//                AppCompatButton gs = (AppCompatButton)view.findViewById(R.id.gs);
//                gs.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        launchHomeScreen();
//                    }
//                });
//            }
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
    @Override
    public void onBackPressed() {

    }

    public void startanimation()
    {

        animationearthx =  android.animation.ObjectAnimator.ofFloat(blue_planet, "translationX", 0f,
                500f);
        animationearthx.setDuration(112000);

        animationearthx.setInterpolator(new LinearInterpolator());


        animationearthx.start();
        animationearthreversex =  android.animation.ObjectAnimator.ofFloat(blue_planet, "translationX", -950f,
                0f);

        animationearthreversex.setDuration(120000);

        animationearthreversex.setInterpolator(new LinearInterpolator());

        animationearthx.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animationearthreversex.start();

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animationearthreversex.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animationearthx.start();

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });



        animationorangex = android.animation.ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,
                950f);
        animationorangex.setDuration(70000);

        animationorangex.setInterpolator(new LinearInterpolator());


        animationorangereversex = android.animation.ObjectAnimator.ofFloat(orange_planet, "translationX", -400f,
                0f);
        animationorangereversex.setDuration(64000);

        animationorangereversex.setInterpolator(new LinearInterpolator());


        animationorangex.start();

        animationorangex.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

                animationorangereversex.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animationorangereversex.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animationorangex.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });



        animationpurplex = android.animation.ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,
                1050f);
        animationpurplex.setDuration(120000);

        animationpurplex.setInterpolator(new LinearInterpolator());
        animationpurplereversex = android.animation.ObjectAnimator.ofFloat(purple_planet, "translationX", -180f,
                0f);
        animationpurplereversex.setDuration(20000);

        animationpurplereversex.setInterpolator(new LinearInterpolator());


        animationpurplex.start();

        animationpurplex.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animationpurplereversex.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animationpurplereversex.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animationpurplex.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION

        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }


}
