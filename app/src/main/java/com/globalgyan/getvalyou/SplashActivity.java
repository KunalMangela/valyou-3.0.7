package com.globalgyan.getvalyou;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dragankrstic.autotypetextview.AutoTypeTextView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

//import static com.facebook.login.widget.ProfilePictureView.TAG;

public class SplashActivity extends Activity {
    private PrefManager prefManager;
    JSONObject jsonObj;
    Dialog dialog;
    Response response;
    boolean flag_netcheck=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        prefManager = new PrefManager(SplashActivity.this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if(new ConnectionUtils(SplashActivity.this).isConnectionAvailable()){
            final CheckInternetTask t913=new CheckInternetTask();
            t913.execute();

            new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    t913.cancel(true);

                    if (flag_netcheck) {
                        flag_netcheck = false;
                        new GetContacts().execute();

                    }else {
                        flag_netcheck=false;
                        check();
                       // Toast.makeText(SplashActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    }
                }
            }.start();

        }else {
            check();
            Toast.makeText(SplashActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();
            //createAlertDialogue();
        }
        TextView tv = (AutoTypeTextView)findViewById(R.id.tvs);
        ImageView iv =(ImageView)findViewById(R.id.imageView);
//        TranslateAnimation animation = new TranslateAnimation(1500.0f, 0.0f, 0.0f, 0.0f); // new TranslateAnimation (float fromXDelta,float toXDelta, float fromYDelta, float toYDelta)
//        animation.setDuration(2000); // animation duration
//        animation.setRepeatCount(0); // animation repeat count if u repeat only once set to 1 if u don't repeat set to 0
//        animation.setFillAfter(false);
//        iv.startAnimation(animation);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/Montserrat-Regular.ttf");
        tv.setTypeface(type);
//        AlphaAnimation fadeIn = new AlphaAnimation(0.0f , 1.0f ) ;
//         AlphaAnimation fadeOut = new AlphaAnimation( 1.0f , 0.0f ) ;
//        tv.startAnimation(fadeIn);
//        tv.startAnimation(fadeOut);
//        fadeIn.setDuration(1200);
//        fadeIn.setFillAfter(true);
//        fadeOut.setDuration(1200);
//        fadeOut.setFillAfter(true);
//        fadeOut.setStartOffset(4200+fadeIn.getStartOffset());

if(PreferenceUtils.getCandidateId(SplashActivity.this)!=null){
    logUser();
}


    }





    public void afterCheck(){
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
               /* if (!prefManager.isFirstTimeLaunch()) {
                    launchHomeScreen();
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, Welcome.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
                }*/



//                Intent intent = new Intent(SplashActivity.this, ProPic.class);
//                startActivity(intent);
//                SplashActivity.this.finish();
                try {

                    if (jsonObj.getInt("check") == 0 ||  jsonObj.getInt("status") <= getVersionCode(SplashActivity.this)) {

                        if (!prefManager.isFirstTimeLaunch()) {
                            launchHomeScreen();
                            finish();
                        } else {
                            String lang = "";
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                             lang = prefs.getString("lang", "");

                             if(!lang.equals("")){
                                 Log.e("lang", lang);

                                 Intent intent = new Intent(SplashActivity.this, Welcome.class);
                                 // intent.putExtra("gotolangpage", "gotolangpage");
                                 startActivity(intent);
                                 overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                                 finish();
                             }
                            else{
                                 Log.e("lang", lang);
                                Intent intent = new Intent(SplashActivity.this, LanguageActivity.class);
                                // intent.putExtra("gotolangpage", "gotolangpage");
                                startActivity(intent);
                                 overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                 finish();
                            }
                        }


                    }else{
                        dialog = new Dialog(SplashActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.update_popup);

                        dialog.show();

                        Button b = (Button)dialog.findViewById(R.id.submitclg);
                        b.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String appPackageName = getApplicationContext().getPackageName();
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                                dialog.dismiss();

                            }
                        });
                    }

                } catch (JSONException e) {

                }
            }


        }, 2000L);
    }


public void check(){
    if (!prefManager.isFirstTimeLaunch()) {
        launchHomeScreen();
        finish();
    } else {
        String lang = "";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        lang = prefs.getString("lang", "");

        if(!lang.equals("")){
            Log.e("lang", lang);

            Intent intent = new Intent(SplashActivity.this, Welcome.class);
            // intent.putExtra("gotolangpage", "gotolangpage");
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            finish();
        }
        else{
            Log.e("lang", lang);
            Intent intent = new Intent(SplashActivity.this, LanguageActivity.class);
            // intent.putExtra("gotolangpage", "gotolangpage");
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            finish();
        }
        Intent intent = new Intent(SplashActivity.this, LanguageActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

        finish();
    }
}

    public static int getVersionCode(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            return pi.versionCode;
        } catch (PackageManager.NameNotFoundException ex) {}
        return 0;
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

        Crashlytics.setUserIdentifier(PreferenceUtils.getCandidateId(SplashActivity.this));
        Crashlytics.setUserName(deviceName);
    }


    private void launchHomeScreen() {

    Log.e("login", "login success");
            prefManager.setFirstTimeLaunch(false);
            Log.e("candidateId,", "" + PreferenceUtils.getCandidateId(SplashActivity.this));
            Log.e("isVerified,", "" + PreferenceUtils.isVerified(SplashActivity.this) + PreferenceUtils.isProfileDone(SplashActivity.this));
            if (!TextUtils.isEmpty(PreferenceUtils.getCandidateId(SplashActivity.this)) && !PreferenceUtils.isVerified(SplashActivity.this)) {
                AppConstant.OTP_FROM_SIGNUP = true;
                Intent intent = new Intent(SplashActivity.this, EnterOtpActivity.class);
                DataBaseHelper dataBaseHelper = new DataBaseHelper(SplashActivity.this);
                String phone = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE);
                if (!TextUtils.isEmpty(phone))
                    intent.putExtra("phoneNumber", phone);
                intent.putExtra("_id", PreferenceUtils.getCandidateId(SplashActivity.this));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            } else if (PreferenceUtils.isProfileDone(SplashActivity.this)) {
                DataBaseHelper dataBaseHelper = new DataBaseHelper(SplashActivity.this);
                String phone = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE);
                Calendar c = Calendar.getInstance();
                //  System.out.println("Current time => " + c.getTime());

                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = df.format(c.getTime());
                if (prefManager.getDate().equals(formattedDate)) {
                    Log.e("done", "done ");
                    Intent intent = new Intent(SplashActivity.this, MainPlanetActivity.class);
                    intent.putExtra("tabs","normal");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    finish();
                } else {
                    prefManager.saveDat(formattedDate);
                    Log.e("done", "done ");
                    Intent intent = new Intent(SplashActivity.this, MainPlanetActivity.class);
                    intent.putExtra("tabs", "normal");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();
                }
            } else {
                String lang = "";
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                lang = prefs.getString("login_success", "");


                if (!lang.equals("login_success")) {
                    Log.e("gotologinpage", "gotologinpage");
                    //do changes
                    Intent intent = new Intent(SplashActivity.this, Welcome.class);
                    intent.putExtra("gotologinpage", "loginpage");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }

                else{
                    Intent intent = new Intent(SplashActivity.this, MainPlanetActivity.class);
                    intent.putExtra("tabs", "normal");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

                }
            }
    }



    private class GetContacts extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();



        }

        @Override
        protected String doInBackground(String... urlkk) {
//            HttpHandler sh = new HttpHandler();
//            // Making a request to url and getting response
//            String url = "https://s3.ap-south-1.amazonaws.com/maimg/update.json";
//            String jsonStr = sh.makeServiceCall(url);
//
//            Log.e("eror", "Response from url: " + jsonStr);
//            if (jsonStr != null) {
//                try {
//                     jsonObj = new JSONObject(jsonStr);
//
//                    Log.e("STATUS",jsonObj.getInt("status")+"");
//
//                } catch (JSONException e) {
//                    Log.e(TAG, "Json parsing error: " + e.getMessage());
//                }
//
//            } else {
//                Log.e(TAG, "Couldn't get json from server.");
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(),
//                                "Couldn't get json from server. Check LogCat for possible errors!",
//                                Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url("https://s3.ap-south-1.amazonaws.com/maimg/update.json")
                    .get()
                    .build();
            try {
                response = client.newCall(request).execute();
                //response.is
            } catch (IOException e) {

            }


            try {
                if (response.isSuccessful()) {
                    //ResponseBody jsonstr = response.body();

                    try {
                        String nofnoe = response.body().string().toString();
                        jsonObj = new JSONObject(nofnoe);

                        Log.e("STATUS", jsonObj.getInt("status") + "");

                    } catch (JSONException e) {
                        Log.e("EEEE", "Json parsing error: " + e.getMessage());
                    } catch (IOException rr) {

                    }
                }
            }catch (Exception e){
                Log.e("SplashActivity","Exception");
            }

                return null;
            }




        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {

                if (response.isSuccessful()) {
                    afterCheck();
                } else {
                    check();
                }
            }catch (Exception e){
                e.printStackTrace();
                Log.e("SplashActivity","Exception postexcecute");
                check();

            }
        }
    }

    public void createAlertDialogue(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SplashActivity.this);
        builder1.setTitle("Warning");
        builder1.setMessage("Your phone has no internet connection!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)SplashActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }
}
