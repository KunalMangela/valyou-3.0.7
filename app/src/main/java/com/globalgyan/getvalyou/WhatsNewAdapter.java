package com.globalgyan.getvalyou;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by NaNi on 26/09/17.
 */

public class WhatsNewAdapter extends RecyclerView.Adapter<WhatsNewAdapter.ViewHolder> {
    private Context context;
    private List<String> notificationses = null;
    private List<String> nlkn= null;
    private List<String> ilist=null;
    private List<String> clist=null;
    static boolean isclick=false;
    //, List<String> clist, List<String> ilist
    public WhatsNewAdapter(Context context, List<String> articleList, List<String> mes) {
        this.context = context;
        this.notificationses = articleList;
        this.nlkn=mes;
//        this.clist=clist;
//        this.ilist=ilist;
    }
    @Override
    public WhatsNewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_whatsnew, null);
        WhatsNewAdapter.ViewHolder viewHolder = new WhatsNewAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WhatsNewAdapter.ViewHolder holder, int position) {
        holder.gruptitle.setText(notificationses.get(position));
        holder.gt.setText(nlkn.get(position));

            holder.cl.setVisibility(View.GONE);
            holder.il.setVisibility(View.GONE);
            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isclick=true;
                    holder.main.setEnabled(false);
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("tabs","normal");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
//                    ((Activity)context).finish();
                }
            });
    }

    @Override
    public int getItemCount() {
        return notificationses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FontTextView gruptitle;
        FontTextView gt,ct,it;

        LinearLayout cl,gl,il,main;


        public ViewHolder(View itemView) {
            super(itemView);
            gruptitle = (FontTextView) itemView.findViewById(R.id.grouptitle);
            it = (FontTextView) itemView.findViewById(R.id.intercount);
            gt=(FontTextView)itemView.findViewById(R.id.gamecount);
            ct=(FontTextView)itemView.findViewById(R.id.coursecount);
            cl=(LinearLayout)itemView.findViewById(R.id.cll);
            gl=(LinearLayout)itemView.findViewById(R.id.gll);
            il=(LinearLayout)itemView.findViewById(R.id.ill);
            main=(LinearLayout)itemView.findViewById(R.id.main_linear);
        }
    }
}
