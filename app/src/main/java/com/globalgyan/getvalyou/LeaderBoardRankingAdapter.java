package com.globalgyan.getvalyou;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

public class LeaderBoardRankingAdapter extends RecyclerView.Adapter<LeaderBoardRankingAdapter.MyViewHolder> {

    private Context ctx;
    private ArrayList<LeaderBoardUserRankingModel> leaderboard_names;

    public LeaderBoardRankingAdapter(Context mContext, ArrayList<LeaderBoardUserRankingModel> leaderboard_names) {
        this.ctx = mContext;
        this.leaderboard_names = leaderboard_names;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView name,num,perc;
        ImageView leader_bg,user_selected;
        CircularProgressBar progrssbar;

        public MyViewHolder(View view) {
            super(view);

            name = (FontTextView) view.findViewById(R.id.leaderboard_content);
            num = (FontTextView) view.findViewById(R.id.num);
            leader_bg=(ImageView)view.findViewById(R.id.leader_list_bg);
            perc=(FontTextView) view.findViewById(R.id.perc);
            progrssbar=(CircularProgressBar)view.findViewById(R.id.progrssbar);
            user_selected=(ImageView)view.findViewById(R.id.user_selected);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leaderboard_items2, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LeaderBoardUserRankingModel leaderBoardUserRankingModel=leaderboard_names.get(position);
        holder.name.setText(leaderBoardUserRankingModel.getName());
        holder.name.setSelected(true);
        Typeface tf2 = Typeface.createFromAsset(ctx.getAssets(), "Gotham-Medium.otf");
        holder.name.setTypeface(tf2);
        final int pos = position+1;

        holder.num.setText(leaderBoardUserRankingModel.getRank());
        holder.perc.setText(leaderBoardUserRankingModel.getPercent()+"%");
        holder.progrssbar.setProgress(Float.parseFloat(leaderBoardUserRankingModel.getPercent()));
        if(LeaderboardActivity.name_no==position){
            holder.user_selected.setVisibility(View.VISIBLE);
        }else {
            holder.user_selected.setVisibility(View.INVISIBLE);

        }
    }

    @Override
    public int getItemCount() {
        return leaderboard_names.size();
    }
}