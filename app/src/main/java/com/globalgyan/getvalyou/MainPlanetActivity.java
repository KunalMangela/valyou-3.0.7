package com.globalgyan.getvalyou;

import android.*;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.request.UpdateProfileRequest;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;

import org.androidannotations.annotations.App;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.HomeActivity.appConstant;
import static com.globalgyan.getvalyou.HomeActivity.batLevel;
import static com.globalgyan.getvalyou.HomeActivity.isPowerSaveMode;
import static com.globalgyan.getvalyou.HomeActivity.pr;
import static com.globalgyan.getvalyou.apphelper.BatteryReceiver.flag_play_anim;

public class MainPlanetActivity extends AppCompatActivity implements GUICallback{

    public static String profileImageUrl;
    public static DataBaseHelper dataBaseHelper = null;
    ImageView close_info;
    public static PrefManager prefManager;
    TextView text_profile_compulsary;
    public static FrameLayout profilepicframe;
    public static Bitmap bitmap, decodedByte;
    Dialog alertDialogis;
    Dialog assess_progress;
    public boolean genderflag=false,professionalstatusflag=false,collegecompanyflag=false;
    public static FaceServiceClient faceServiceClient;
    String cN,oN;
    FontTextView message_status;
    String tag_gender;
    ImageView learningplanet,greylearningplanet,assessmentplanet,greyassessmentplanet,scoreplanet,greyscoreplanet,a_ring,j_ring,l_ring;
    Animation animation_fade1,fade2,fade3,fade4;
    ImageView cmt_box,ntf_click,menu;
    ConnectionUtils connectionUtils;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    FrameLayout main_frame;
    String professionval="";
    BatteryReceiver mBatInfoReceiver;
    private int ORGANIZATION_PICKER_REQUEST_CODE = 191;
    private int COLLEGE_PICKER_REQUEST_CODE = 181;
    ImageView profile_pic_compulsary;
    FontTextView skip_profile,username,uname_pro,uname_comp,uname_clg;
    public static Context mcont;
    //new
    RelativeLayout info_relative,gender_relative,student_professional_relative,college_company_list_relative;
    LinearLayout malepanel,femalepanel,studentpanel,professionalpanel,company_panel,college_panel;
    TextView male_text,female_text,student_text,professional_text,next_btn;
    ImageView female_icon,male_icon,student_icon,professional_icon;
    FontTextView companylabel,collegelabel;
    TextView companyname,collegename;
    int c=0,c1=0,c2=0;
    String myName;
    FontTextView done_gender,done_professionalstatus;
    String jngender,jn_profession;
    FrameLayout clg_frame,comp_frame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_planet);

        if(Build.VERSION.SDK_INT>=23) {
            checkPermissions();
        }

        mcont=MainPlanetActivity.this;




        close_info=(ImageView)findViewById(R.id.close_info);
        profilepicframe=(FrameLayout)findViewById(R.id.profilepicframe);
        next_btn=(TextView)findViewById(R.id.next_btn);
        next_btn.setEnabled(false);
        clg_frame=(FrameLayout)findViewById(R.id.clg_frame);
        comp_frame=(FrameLayout)findViewById(R.id.comp_frame);
        done_gender=(FontTextView)findViewById(R.id.done_gender);
        done_gender.setEnabled(false);
        done_professionalstatus=(FontTextView)findViewById(R.id.done_professionalstatus);
        done_professionalstatus.setEnabled(false);
        username=(FontTextView)findViewById(R.id.username);
        uname_pro=(FontTextView)findViewById(R.id.usernameis);
        uname_comp=(FontTextView)findViewById(R.id.usernameisis);
        uname_clg=(FontTextView)findViewById(R.id.usernameisisis);
        profile_pic_compulsary=(ImageView)findViewById(R.id.profile_pic_compulsary);
        text_profile_compulsary=(TextView)findViewById(R.id.text_profile_compulsary);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
         connectionUtils=new ConnectionUtils(this);
        if(connectionUtils.isConnectionAvailable()){
            getFrKey();
        }
        prefManager = new PrefManager(MainPlanetActivity.this);
        skip_profile=(FontTextView)findViewById(R.id.skip_profile);
        info_relative=(RelativeLayout)findViewById(R.id.info_relative);
        gender_relative=(RelativeLayout)findViewById(R.id.gender_relative);
        malepanel=(LinearLayout) findViewById(R.id.malepanel);
        male_text=(TextView)findViewById(R.id.male_text);
        femalepanel=(LinearLayout)findViewById(R.id.femalepanel);
        studentpanel=(LinearLayout)findViewById(R.id.studentpanel);
        company_panel=(LinearLayout)findViewById(R.id.company_panel);
        college_panel=(LinearLayout)findViewById(R.id.college_panel);
        professionalpanel=(LinearLayout)findViewById(R.id.professionalpanel);
        professional_icon=(ImageView)findViewById(R.id.professional_icon);
        student_icon=(ImageView)findViewById(R.id.student_icon);
        companylabel=(FontTextView)findViewById(R.id.companyLabel);
        companyname=(TextView)findViewById(R.id.companyName);
        collegelabel=(FontTextView)findViewById(R.id.collegeLabel);
        collegename=(TextView)findViewById(R.id.collegeName);
        student_text=(TextView)findViewById(R.id.student_text);
        female_text=(TextView)findViewById(R.id.female_text);
        professional_text=(TextView)findViewById(R.id.professional_text);
        student_professional_relative=(RelativeLayout)findViewById(R.id.student_professional_relative);
        college_company_list_relative=(RelativeLayout)findViewById(R.id.college_company_list_relative);
        female_icon=(ImageView)findViewById(R.id.female_icon);
        male_icon=(ImageView)findViewById(R.id.male_icon);
        dataBaseHelper = new DataBaseHelper(    this);
        String myGender = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER);
        collegename.setSelected(true);
        companyname.setSelected(true);
         myName = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);
        if ( !TextUtils.isEmpty(myName) ) {
            username.setText("Hi "+myName);
            uname_pro.setText("Hi "+myName);
            uname_comp.setText("Hi "+myName);
            uname_clg.setText("Hi "+myName);

        } else {
            username.setText("Hi ");
            uname_pro.setText("Hi ");
            uname_comp.setText("Hi ");
            uname_clg.setText("Hi ");
            // upper_heading.setText("Welcome ");

        }


        //profile pic listner
        profile_pic_compulsary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile_pic_compulsary.setEnabled(false);

                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        profile_pic_compulsary.setEnabled(true);
                    }
                }.start();
//                alertDialogis.dismiss();
                Intent it = new Intent(MainPlanetActivity.this,ProPic.class);
                it.putExtra("fromwhere","home");
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


            }
        });

        //skip profile pic listner
        skip_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PrefManager(MainPlanetActivity.this).profile_skiped(true);
                profilepicframe.setVisibility(View.GONE);

            }
        });

        profilepicframe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilepicframe.setVisibility(View.GONE);
                new PrefManager(MainPlanetActivity.this).profile_skiped(true);
            }
        });


        //done gender click
        done_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(connectionUtils.isConnectionAvailable()) {

                    if(genderflag){
                        new SendGender().execute(AppConstant.BASE_URL+"saveGender",jngender);
                    }else {
                        Toast.makeText(MainPlanetActivity.this,"Please select your gender",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }

            }
        });

        done_gender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(genderflag){
                    if(event.getAction() == MotionEvent.ACTION_DOWN) {
                        done_gender.setBackgroundResource(R.drawable.skip_click);
                        new CountDownTimer(150, 150) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                done_gender.setBackgroundResource(R.drawable.skip_profile);
                            }
                        }.start();

                    }

                }else {

                }

                return false;
            }
        });

        done_professionalstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectionUtils.isConnectionAvailable()) {

                    if(professionalstatusflag){
                        new SendProfessionalstatus().execute(AppConstant.BASE_URL+"saveProfessionalDetails",jn_profession);
                    }else {
                        Toast.makeText(MainPlanetActivity.this,"Please select your profession",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }


            }
        });

        done_professionalstatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if(professionalstatusflag){

                    if(event.getAction() == MotionEvent.ACTION_DOWN) {
                        done_professionalstatus.setBackgroundResource(R.drawable.skip_click);
                        new CountDownTimer(150, 150) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                done_professionalstatus.setBackgroundResource(R.drawable.skip_profile);
                            }
                        }.start();
                    }

                }else {

                }

                return false;
            }
        });


        next_btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    next_btn.setBackgroundResource(R.drawable.skip_click);
                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            next_btn.setBackgroundResource(R.drawable.skip_profile);
                        }
                    }.start();
                }

                return false;
            }
        });
        ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
        ArrayList<EducationalDetails> arrayListedu = dataBaseHelper.getEducationalDetails();

        RelativeLayout banner_propic=(RelativeLayout)findViewById(R.id.banner_propic);
        banner_propic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilepicframe.setVisibility(View.VISIBLE);
            }
        });


        if((TextUtils.isEmpty(myGender))||((professionals.size()<1)&&(arrayListedu.size()<1))){
            info_relative.setVisibility(View.VISIBLE);
            Log.e("mainre","whole empty");
        }else {
            info_relative.setVisibility(View.GONE);

            Log.e("mainre","whole not empty");

        }


        if ( !TextUtils.isEmpty(myGender) ) {
            Log.e("mainre","gender notempty");

            gender_relative.setVisibility(View.GONE);
        } else{
            Log.e("mainre","gender empty");


            try{
                if(new PrefManager(MainPlanetActivity.this).get_firstlaunchflag().equalsIgnoreCase("no")){

                }else {
                    showverificationDialogueMain();
                    new PrefManager(MainPlanetActivity.this).set_firstlaunchflag("no");

                }
            }catch (Exception e){
                new PrefManager(MainPlanetActivity.this).set_firstlaunchflag("no");
                showverificationDialogueMain();
                e.printStackTrace();
            }

            gender_relative.setVisibility(View.VISIBLE);

        }

         if ( (professionals.size()<1)&&(arrayListedu.size()<1) ) {
            Log.e("mainre","pro empty");
            student_professional_relative.setVisibility(View.VISIBLE);
        } else{
            Log.e("mainre","pro notempty");
            student_professional_relative.setVisibility(View.GONE);
        }



        Typeface tf2 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");

        companyname.setTypeface(tf2);
        collegename.setTypeface(tf2);
        male_text.setTypeface(tf2,Typeface.BOLD);
        female_text.setTypeface(tf2,Typeface.BOLD);
        student_text.setTypeface(tf2,Typeface.BOLD);
        professional_text.setTypeface(tf2,Typeface.BOLD);

        malepanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                femalepanel.setEnabled(false);
                new CountDownTimer(300, 300) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        femalepanel.setEnabled(true);
                    }
                }.start();
                    tag_gender="male";
                    genderflag=true;
                done_gender.setEnabled(true);
                  //  disableGenderFields();
                    scalemaleiconandtext();
                    scalefemaleiconandtextError();


            }
        });
        info_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        close_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info_relative.setVisibility(View.GONE);
            }
        });
        femalepanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                malepanel.setEnabled(false);
                new CountDownTimer(300, 300) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        malepanel.setEnabled(true);
                    }
                }.start();
                    tag_gender="female";
                    genderflag=true;
                done_gender.setEnabled(true);
                  //  disableGenderFields();
                    scalefemaleiconandtext();
                    scalemaleiconandtextError();

            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectionUtils.isConnectionAvailable()) {
                   /* if(professionval.equalsIgnoreCase("student")){
                          String jn = new JsonBuilder(new GsonAdapter())
                                .object("candidateId", PreferenceUtils.getCandidateId(MainPlanetActivity.this))
                                .object("collegeName", cN)
                                .build().toString();




                        new SendProfessionalData().execute(AppConstant.BASE_URL+"saveCollageName",jn);

                    }else {
                        String jn = new JsonBuilder(new GsonAdapter())
                                .object("candidateId", PreferenceUtils.getCandidateId(MainPlanetActivity.this))
                                .object("organisationName", oN)
                                .build().toString();

                        new SendProfessionalData().execute(AppConstant.BASE_URL+"saveOrganisationName",jn);

                    }

*/

                    if(professionval.equalsIgnoreCase("student")){

                        String jsis=getJsonedudata(cN);
                        Log.e("jsisst",jsis);
                        JsonParser jsonParser = new JsonParser();
                        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsis);
                        UpdateProfileRequest request = new UpdateProfileRequest(PreferenceUtils.getCandidateId(MainPlanetActivity.this), MainPlanetActivity.this);


                        Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(MainPlanetActivity.this, MainPlanetActivity.this);
                        processor.updateProfile(PreferenceUtils.getCandidateId(MainPlanetActivity.this), gsonObject);

                    }else {
                        String jsis=getJsonprodata(oN);
                        Log.e("jsispro",jsis);

                        JsonParser jsonParser = new JsonParser();
                        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsis);

                        UpdateProfileRequest request = new UpdateProfileRequest(PreferenceUtils.getCandidateId(MainPlanetActivity.this), MainPlanetActivity.this);


                        Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(MainPlanetActivity.this, MainPlanetActivity.this);
                        processor.updateProfile(PreferenceUtils.getCandidateId(MainPlanetActivity.this), gsonObject);


                    }



                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }
            }
        });


        studentpanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectionUtils.isConnectionAvailable()) {
                    professionval="student";
                    professionalstatusflag=true;
                    done_professionalstatus.setEnabled(true);
                 //  disableproFields();
                    scalestudenticonandtext();
                    scaleprofessionaliconandtextError();
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }
            }
        });


        professionalpanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectionUtils.isConnectionAvailable()) {
                    professionval="professional";
                    professionalstatusflag=true;
                    done_professionalstatus.setEnabled(true);
                  //  disableproFields();
                    scaleprofessionaliconandtext();
                    scalestudenticonandtextError();
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                }
            }
        });


        collegename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(MainPlanetActivity.this, CollegeActivity.class);
                    startActivityForResult(intent, COLLEGE_PICKER_REQUEST_CODE);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


                } else {
                    Toast.makeText(MainPlanetActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        clg_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(MainPlanetActivity.this, CollegeActivity.class);
                    startActivityForResult(intent, COLLEGE_PICKER_REQUEST_CODE);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


                } else {
                    Toast.makeText(MainPlanetActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        comp_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectionUtils.isConnectionAvailable()){
                    Intent intent = new Intent(MainPlanetActivity.this, OrganizationActivity.class);
                    startActivityForResult(intent, ORGANIZATION_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });
        companyname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectionUtils.isConnectionAvailable()){
                    Intent intent = new Intent(MainPlanetActivity.this, OrganizationActivity.class);
                    startActivityForResult(intent, ORGANIZATION_PICKER_REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });


        createProgress();


        cmt_box=(ImageView)findViewById(R.id.cmt_box);
        ntf_click=(ImageView)findViewById(R.id.ntf_click);
        menu=(ImageView)findViewById(R.id.menu);



        menu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    menu.setBackgroundResource(R.drawable.nav_back_ripple1);

                    new CountDownTimer(150, 80) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            menu.setBackgroundResource(R.drawable.nav_back_ripple2);

                        }

                        @Override
                        public void onFinish() {
                            menu.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }
*/
                return false;
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();
                new PrefManager(MainPlanetActivity.this).saveNav("Ass");
                new PrefManager(MainPlanetActivity.this).saveback_status("main");
                Intent intent = new Intent(MainPlanetActivity.this, HomeActivity.class);
                intent.putExtra("tabs", "more_tab");

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });


        ntf_click.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

/*
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    ntf_click.setBackgroundColor(Color.parseColor("#33ffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            ntf_click.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }
*/

                return false;
            }
        });

        ntf_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();
                // new PrefManager(MainPlanetActivity.this).saveAsstype("Ass");
                new PrefManager(MainPlanetActivity.this).saveNav("Ass");
                new PrefManager(MainPlanetActivity.this).saveback_status("main");
                Intent intent = new Intent(MainPlanetActivity.this, HomeActivity.class);
                intent.putExtra("tabs", "ntf_tab");

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


            }
        });


        main_frame=(FrameLayout)findViewById(R.id.main_frame);
        animation_fade1 =
                AnimationUtils.loadAnimation(MainPlanetActivity.this,
                        R.anim.fadeing_out_animation_planet);
        fade2 =
                AnimationUtils.loadAnimation(MainPlanetActivity.this,
                        R.anim.fadeing_out_animation_planet);
        fade3 =
                AnimationUtils.loadAnimation(MainPlanetActivity.this,
                        R.anim.fadeing_out_animation_planet);

        fade4 =
                AnimationUtils.loadAnimation(MainPlanetActivity.this,
                        R.anim.fade_main_planet_cmt);

       /* if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }*/

        message_status=(FontTextView)findViewById(R.id.message_status);
        learningplanet=(ImageView)findViewById(R.id.learning_planet);
        assessmentplanet=(ImageView)findViewById(R.id.assessment_planet);
        scoreplanet=(ImageView)findViewById(R.id.score_planet);

        greylearningplanet=(ImageView)findViewById(R.id.grey_learning_planet);
        greyassessmentplanet=(ImageView)findViewById(R.id.grey_assessment_planet);
        greyscoreplanet=(ImageView)findViewById(R.id.grey_score_planet);
        a_ring=(ImageView)findViewById(R.id.a_ring);
        j_ring=(ImageView)findViewById(R.id.j_ring);
        l_ring=(ImageView)findViewById(R.id.l_ring);

            scoreplanet.startAnimation(animation_fade1);
            cmt_box.startAnimation(fade4);
            cmt_box.setVisibility(View.INVISIBLE);
            message_status.setVisibility(View.INVISIBLE);

      /*      scoreplanet.setEnabled(false);
        assessmentplanet.setEnabled(false);
        learningplanet.setEnabled(false);
        main_frame.setEnabled(false);*/

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {

            ObjectAnimator imageViewObjectAnimator1 = ObjectAnimator.ofFloat(a_ring,
                    "rotation", 0f, 360f);
            imageViewObjectAnimator1.setDuration(100000); // miliseconds
            imageViewObjectAnimator1.setStartDelay(0);
            imageViewObjectAnimator1.start();


            ObjectAnimator imageViewObjectAnimator2 = ObjectAnimator.ofFloat(j_ring,
                    "rotation", 0f, 360f);
            imageViewObjectAnimator2.setDuration(100000); // miliseconds
            imageViewObjectAnimator2.setStartDelay(0);
            imageViewObjectAnimator2.start();


            ObjectAnimator imageViewObjectAnimator3 = ObjectAnimator.ofFloat(l_ring,
                    "rotation", 0f, 360f);
            imageViewObjectAnimator3.setDuration(100000); // miliseconds
            imageViewObjectAnimator3.setStartDelay(0);
            imageViewObjectAnimator3.start();
        }

        new CountDownTimer(1200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

                scoreplanet.setVisibility(View.VISIBLE);

                if(c==1){
                    cmt_box.setBackgroundResource(R.drawable.brown_cmt);
                    cmt_box.setVisibility(View.VISIBLE);
                    message_status.setText("Commence Your Journey");

                    message_status.setVisibility(View.VISIBLE);
                }
                c++;
            }

            @Override
            public void onFinish() {
                cmt_box.setVisibility(View.INVISIBLE);
                message_status.setVisibility(View.INVISIBLE);
                    assessmentplanet.startAnimation(fade2);
                    cmt_box.startAnimation(fade4);
                cmt_box.setBackgroundResource(R.drawable.green_cmt);

                // assessmentplanet.setVisibility(View.VISIBLE);

                new CountDownTimer(1200, 200) {
                    @Override
                    public void onTick(long millisUntilFinished)
                    {
                        assessmentplanet.setVisibility(View.VISIBLE);
                        if(c1==1) {
                            cmt_box.setBackgroundResource(R.drawable.green_cmt);
                            cmt_box.setVisibility(View.VISIBLE);
                            message_status.setText("Commence Your Assessments");
                            message_status.setVisibility(View.VISIBLE);
                        }
                        c1++;
                    }

                    @Override
                    public void onFinish() {
                        cmt_box.setVisibility(View.INVISIBLE);
                        message_status.setVisibility(View.INVISIBLE);
                        learningplanet.startAnimation(fade3);
                        cmt_box.startAnimation(fade4);
                        cmt_box.setBackgroundResource(R.drawable.orange_cmt);

                        //  learningplanet.setVisibility(View.VISIBLE);
                        new CountDownTimer(1200, 200) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                learningplanet.setVisibility(View.VISIBLE);
                                if(c2==1) {
                                    cmt_box.setVisibility(View.VISIBLE);

                                    message_status.setText("Commence Your Learning");
                                    message_status.setVisibility(View.VISIBLE);
                                }
                                c2++;
                            }

                            @Override
                            public void onFinish() {
                                cmt_box.setVisibility(View.GONE);
                                message_status.setVisibility(View.GONE);
//                                scoreplanet.setEnabled(true);
//                                learningplanet.setEnabled(true);
//                                assessmentplanet.setEnabled(true);
//                                main_frame.setEnabled(true);
                            }
                        }.start();

                    }
                }.start();

            }
        }.start();

        main_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cmt_box.setVisibility(View.VISIBLE);
                message_status.setVisibility(View.VISIBLE);
                message_status.setText("Click on any of planet to proceed");
                new CountDownTimer(3000, 3000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        cmt_box.setVisibility(View.GONE);
                        message_status.setVisibility(View.GONE);
                    }
                }.start();

            }
        });


        greylearningplanet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();
                if(connectionUtils.isConnectionAvailable()) {
                    assess_progress.show();
                    cmt_box.setVisibility(View.GONE);
                    message_status.setVisibility(View.GONE);

                    cmt_box.setBackgroundResource(R.drawable.orange_cmt);
//                    message_status.setText("Check Your Learning Skills");
                    // cmt_box.startAnimation(fade4);

//                    new CountDownTimer(100, 50) {
//                        @Override
//                        public void onTick(long millisUntilFinished) {
//
////                            cmt_box.setVisibility(View.VISIBLE);
//                        }
//
//                        @Override
//                        public void onFinish() {
////                            message_status.setVisibility(View.VISIBLE);
//
//
//                        }
//                    }.start();

                    new PrefManager(MainPlanetActivity.this).saveLearnNav("main");
                    new CountDownTimer(10, 10) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            Intent intent = new Intent(MainPlanetActivity.this, GetLearningGroupsActivity.class);
                            // intent.putExtra("tabs", "normal");
                            new PrefManager(MainPlanetActivity.this).saveNav("learn");

                            if(assess_progress.isShowing()){
                                assess_progress.dismiss();
                            }
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);
                            finish();
                        }
                    }.start();
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });

        learningplanet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();
                if(connectionUtils.isConnectionAvailable()) {
                    cmt_box.setVisibility(View.GONE);
                    message_status.setVisibility(View.GONE);
                    assess_progress.show();
                    cmt_box.setBackgroundResource(R.drawable.orange_cmt);
//                    message_status.setText("Check Your Learning Skills");
                   // cmt_box.startAnimation(fade4);
//
//                    new CountDownTimer(100, 50) {
//                        @Override
//                        public void onTick(long millisUntilFinished) {
//
////                            cmt_box.setVisibility(View.VISIBLE);
//                        }
//
//                        @Override
//                        public void onFinish() {
////                            message_status.setVisibility(View.VISIBLE);
//
//
//                        }
//                    }.start();
                    new CountDownTimer(10, 10) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            Intent intent = new Intent(MainPlanetActivity.this, GetLearningGroupsActivity.class);
                            // intent.putExtra("tabs", "normal");
                            new PrefManager(MainPlanetActivity.this).saveNav("learn");
                            if(assess_progress.isShowing()){
                                assess_progress.dismiss();
                            }
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);
                            finish();
                        }
                    }.start();
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });



        greyassessmentplanet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();


                if(connectionUtils.isConnectionAvailable()){
                    cmt_box.setVisibility(View.GONE);
                    message_status.setVisibility(View.GONE);
                    cmt_box.setBackgroundResource(R.drawable.green_cmt);

//                    message_status.setText("Check Your Assesments");
                    //cmt_box.startAnimation(fade4);
//                    new CountDownTimer(100, 50) {
//                        @Override
//                        public void onTick(long millisUntilFinished) {
//
////                            cmt_box.setVisibility(View.VISIBLE);
//                        }
//
//                        @Override
//                        public void onFinish() {
////                            message_status.setVisibility(View.VISIBLE);
//
//
//                        }
//                    }.start();

                    new CountDownTimer(10, 10) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            assess_progress.show();
                            new PrefManager(MainPlanetActivity.this).saveRatingtype("assessment_group");

                            new PrefManager(MainPlanetActivity.this).saveAsstype("Ass");
                            Intent intent = new Intent(MainPlanetActivity.this, AssessmentGroupsActivity.class);
                            new PrefManager(MainPlanetActivity.this).saveNav("Ass");
                            new PrefManager(MainPlanetActivity.this).saveback_status("Assgroup");
                            if(assess_progress.isShowing())
                            {
                                assess_progress.dismiss();
                            }
                            // intent.putExtra("navigatefrom", "mainplanet");

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                            overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);
                            finish();


                        }
                    }.start();
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });

        assessmentplanet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();


                if(connectionUtils.isConnectionAvailable()){
                    cmt_box.setVisibility(View.GONE);
                    message_status.setVisibility(View.GONE);
                    cmt_box.setBackgroundResource(R.drawable.green_cmt);
//                    message_status.setText("Check Your Assesments");
                    //cmt_box.startAnimation(fade4);
                 /*   new CountDownTimer(100, 50) {
                        @Override
                        public void onTick(long millisUntilFinished) {

//                            cmt_box.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFinish() {
//                            message_status.setVisibility(View.VISIBLE);


                        }
                    }.start();*/

                    new CountDownTimer(10, 10) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            assess_progress.show();
                            new PrefManager(MainPlanetActivity.this).saveRatingtype("assessment_group");

                            new PrefManager(MainPlanetActivity.this).saveAsstype("Ass");
                            Intent intent = new Intent(MainPlanetActivity.this, AssessmentGroupsActivity.class);
                            new PrefManager(MainPlanetActivity.this).saveNav("Ass");
                            new PrefManager(MainPlanetActivity.this).saveback_status("Assgroup");
                            if(assess_progress.isShowing())
                            {
                                assess_progress.dismiss();
                            }
                            // intent.putExtra("navigatefrom", "mainplanet");

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                            overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);
                            finish();


                        }
                    }.start();
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }




            }
        });


        greyscoreplanet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();
                if(connectionUtils.isConnectionAvailable()) {
                    message_status.setVisibility(View.GONE);
                    cmt_box.setVisibility(View.GONE);
                    cmt_box.setBackgroundResource(R.drawable.brown_cmt);
                    assess_progress.show();
//                    message_status.setText("See Your Journey");
                    //cmt_box.startAnimation(fade4);

//                    new CountDownTimer(100, 50) {
//                        @Override
//                        public void onTick(long millisUntilFinished) {
//
////                            cmt_box.setVisibility(View.VISIBLE);
//                        }
//
//                        @Override
//                        public void onFinish() {
////                            message_status.setVisibility(View.VISIBLE);
//
//
//                        }
//                    }.start();
                    new CountDownTimer(10, 10) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            Intent intent = new Intent(MainPlanetActivity.this, JourneyMainScreenActivity.class);
                            // intent.putExtra("tabs", "normal");
                            if(assess_progress.isShowing()){
                                assess_progress.dismiss();
                            }
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);
                        }
                    }.start();
               /* AppConstant appConstant=new AppConstant(MainPlanetActivity.this);
                appConstant.showLogoutDialogue();*/
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });

        scoreplanet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableButtons();
                if(connectionUtils.isConnectionAvailable()) {
                    message_status.setVisibility(View.GONE);
                    cmt_box.setVisibility(View.GONE);
                    cmt_box.setBackgroundResource(R.drawable.brown_cmt);
                    assess_progress.show();
//                    message_status.setText("See Your Journey");
                    //cmt_box.startAnimation(fade4);

//                    new CountDownTimer(100, 50) {
//                        @Override
//                        public void onTick(long millisUntilFinished) {
//
////                            cmt_box.setVisibility(View.VISIBLE);
//                        }
//
//                        @Override
//                        public void onFinish() {
////                            message_status.setVisibility(View.VISIBLE);
//
//
//                        }
//                    }.start();
                    new CountDownTimer(10, 10) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            Intent intent = new Intent(MainPlanetActivity.this, JourneyMainScreenActivity.class);
                            // intent.putExtra("tabs", "normal");
                            if(assess_progress.isShowing()){
                                assess_progress.dismiss();
                            }
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fade_planet_anim, R.anim.fade_planet_anim_two);
                        }
                    }.start();
               /* AppConstant appConstant=new AppConstant(MainPlanetActivity.this);
                appConstant.showLogoutDialogue();*/
                }else {
                    Toast.makeText(MainPlanetActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    public String getJsonedudata(String cn) {
        String myGender = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER);

        String mobile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE);
        if ( !TextUtils.isEmpty(mobile) ) {

        } else {
           mobile="";

        }


        String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);
        if ( !TextUtils.isEmpty(email) ) {

        } else {
            email="";
        }


        String st="{\n" +
                "  \"gender\": \""+myGender+"\",\n" +
                "  \"profileComplete\":\"true\",\n" +
                "  \"displayName\": \""+myName+"\",\n" +
                "  \"mobile\": \""+mobile+"\",\n" +
                "  \"email\": \""+email+"\",\n" +
                "  \"currentEducation\": [\n" +
                "    {\n" +
                "      \"collegeName\": \""+cn+"\",\n" +
                "      \"degreeName\": \" \",\n" +
                "      \"startDate\": {\n" +
                "        \"year\": 2000,\n" +
                "        \"month\":1\n" +
                "      },\n" +
                "      \"endDate\": {\n" +
                "        \"year\":2000,\n" +
                "        \"month\":12  \n" +
                "      }\n" +
                "    }\n" +
                "  ],\n" +
                "  \"professionalExperience\": []\n" +
                "}";
      return st;
    }



    public String getJsonprodata(String cn) {
        String myGender = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER);

        String mobile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE);
        if ( !TextUtils.isEmpty(mobile) ) {

        } else {
            mobile="";

        }


        String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);
        if ( !TextUtils.isEmpty(email) ) {

        } else {
            email="";
        }


        String st="{\n" +
                "  \"gender\": \""+myGender+"\",\n" +
                "  \"profileComplete\": \"true\",\n" +
                "  \"displayName\": \""+myName+"\",\n" +
                "  \"mobile\": \""+mobile+"\",\n" +
                "  \"email\": \""+email+"\",\n" +
                "  \"currentEducation\": [],\n" +
                "  \"professionalExperience\": [\n" +
                "    {\n" +
                "      \"positionHeld\": \" \",\n" +
                "      \"organisationName\": \""+cn+"\",\n" +
                "      \"startDate\": {\n" +
                "        \"year\":2000,\n" +
                "        \"month\":1\n" +
                "      },\n" +
                "      \"endDate\": {\n" +
                "        \"year\":2000,\n" +
                "        \"month\":12\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        return st;
    }


    private void createProgress() {
            assess_progress = new Dialog(MainPlanetActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            assess_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
            assess_progress.setContentView(R.layout.planet_loader);
            Window window = assess_progress.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
            assess_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



            GifView gifView1 = (GifView) assess_progress.findViewById(R.id.loader);
            gifView1.setVisibility(View.VISIBLE);
            gifView1.play();
            gifView1.setGifResource(R.raw.loader_planet);
            gifView1.getGifResource();
            assess_progress.setCancelable(false);
            // assess_progress.show();


    }

    @Override
    public void onBackPressed() {

    }




    @Override
    protected void onResume() {
        super.onResume();
      /*  cmt_box.setVisibility(View.GONE);
        message_status.setVisibility(View.GONE);*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.RECORD_AUDIO,
                android.Manifest.permission.WAKE_LOCK ,
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION

        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }
    public void disableButtons(){
        assessmentplanet.setEnabled(false);
        learningplanet.setEnabled(false);
        scoreplanet.setEnabled(false);
        ntf_click.setEnabled(false);
        menu.setEnabled(false);
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                assessmentplanet.setEnabled(true);
                learningplanet.setEnabled(true);
                scoreplanet.setEnabled(true);
                ntf_click.setEnabled(true);
                menu.setEnabled(true);
            }
        }.start();

    }
    public static void checkIfProfilePicAvailable(){
        try {
            profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
        }catch (Exception e){
            e.printStackTrace();
            //   profileImageUrl="";
        }
        try {
            if (prefManager.getPic() == " ") {

                if((profileImageUrl!="")||(profileImageUrl!=null)){


                    //pic not present so download it first and then detect it

                    new DownloadImage().execute(AppConstant.BASE_URL+"candidateMobileReadImage/"+profileImageUrl);
                    //not present
                    //showProfilePicDialogue();
                }else {
                    //not present in shared data
                    // showProfilePicDialogue();


                }
            } else {
                //pic already present then detect it directly

                byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                detectpic(decodedByte);
            }
        }catch (Exception e){
            e.printStackTrace();
            //    showProfilePicDialogue();
        }


    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {


                    if(professionval.equalsIgnoreCase("student")){
                        Log.e("jsisst","Successfullysaved profession");
                        EducationalDetails educationalDetails = new EducationalDetails();
                        educationalDetails.setCollegeName(cN);
                        educationalDetails.setDegreeName("");

                   /* timePeriod = txtStart.getText().toString() + " to " + txtEnd.getText().toString();
                    educationalDetails.setYearOfGraduation(timePeriod);*/
                        educationalDetails.setFrom("Month/Year");
                        educationalDetails.setTo("Month/Year");
                        String selectedEducationId = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON);

                        if (TextUtils.isEmpty(selectedEducationId)) {
                            dataBaseHelper.createEducation(educationalDetails);
                        } else {
                            dataBaseHelper.updateEducation(selectedEducationId, educationalDetails);
                            long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                            if (selectedid > 0) {
                                dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON, "");
                            }
                        }
                    }else {
                        Log.e("jsispro","Successfullysaved profession");
                        Professional professional = null;
                        String selectedEducationId = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_PROFESSION);
                        Log.e("----ID", selectedEducationId + "");
                        if ( !TextUtils.isEmpty(selectedEducationId) ) {
                            professional = dataBaseHelper.getProfessionDetailsFromId(selectedEducationId);

                        }
                        Professional educationalDetails = new Professional();
                        educationalDetails.setOrganizationName(oN);
                        educationalDetails.setPositionHeld("");
                        educationalDetails.setFrom("Month/Year");
                        educationalDetails.setTo("Month/Year");

                        if (professional == null) {
                            dataBaseHelper.createProfessionlDetails(educationalDetails);
                        } else {
                            dataBaseHelper.updateProfession(professional.getId(), educationalDetails);
                            long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                            if (selectedid > 0) {
                                dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_PROFESSION, "");
                            }
                        }
                    }
                    info_relative.setVisibility(View.GONE);


            }else {
                Toast.makeText(MainPlanetActivity.this,"Error while submitting data",Toast.LENGTH_SHORT).show();

            }
        }
    }

    public static class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];


            try {
                // Download Image from URL
                InputStream input = new URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                bitmap=null;
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if(result!=null) {
                // Set the bitmap into ImageView
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                result.compress(Bitmap.CompressFormat.PNG, 0, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                Log.e("ENCID", encodedImage);
                prefManager.savePic(encodedImage);
                Log.e("EN", prefManager.getPic());
                byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                detectpic(decodedByte);
            }
            else{

                showProfilePicDialogue();

            }
            // dialog.cancel();
        }
    }
    public static void detectpic(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            //    Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                            Log.e("mainplanetactivity","No face detected in mainplanetactivity");

                            //   sl.setVisibility(View.VISIBLE);

                            //show dialog again if no face is detected in available profile pic
                            showProfilePicDialogue();
                        }else {
                            //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("mainplanetactivity","face detected in mainplanetactivity");



                            try {
                                List<Face> faces;

                                assert result != null;
                                faces = Arrays.asList(result);
                                for (Face face : faces) {
                                    UUID mFaceId1 = face.faceId;
                                    String faceid = mFaceId1.toString();

                                    prefManager.setprofilefaceid(faceid);
                                    prefManager.detectionDone(true);
                                    //txt.setText(faceid);
                                    // Log.e("fid",mFaceId1+"  "+mface);
                                }

                            }catch (Exception e){

                                Log.e("connection lost", "in mainplanetactivity");
                                e.printStackTrace();
                            }

                        }



                    }
                };
        detectTask.execute(inputStream);
    }
    public static void showProfilePicDialogue(){
        profilepicframe.setVisibility(View.VISIBLE);
    }
    public void scalemaleiconandtext(){
        male_icon.setImageResource(R.drawable.male_blue);
         jngender = new JsonBuilder(new GsonAdapter())
                .object("candidateId", PreferenceUtils.getCandidateId(MainPlanetActivity.this))
                .object("gender", "male")
                .build().toString();

        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
              //  malepanel.setBackgroundColor(Color.parseColor("#9BEDCA"));

            }
        }.start();
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(male_text, "scaleX", 1.2f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(male_text, "scaleY", 1.2f);
        ObjectAnimator scaleDownXt = ObjectAnimator.ofFloat(male_icon, "scaleX", 1.2f);
        ObjectAnimator scaleDownYt = ObjectAnimator.ofFloat(male_icon, "scaleY", 1.2f);
        scaleDownX.setDuration(500);
        scaleDownY.setDuration(500);
        scaleDownXt.setDuration(500);
        scaleDownYt.setDuration(500);

        AnimatorSet scaleup = new AnimatorSet();
        scaleup.playTogether(scaleDownX,scaleDownY,scaleDownXt,scaleDownYt);
        scaleup.start();

    }
    public void scalefemaleiconandtext(){
        female_icon.setImageResource(R.drawable.female_blue);
         jngender = new JsonBuilder(new GsonAdapter())
                .object("candidateId", PreferenceUtils.getCandidateId(MainPlanetActivity.this))
                .object("gender", "female")
                .build().toString();

        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
              //  femalepanel.setBackgroundColor(Color.parseColor("#9BEDCA"));

            }
        }.start();
        ObjectAnimator scaleDownXf = ObjectAnimator.ofFloat(female_text, "scaleX", 1.2f);
        ObjectAnimator scaleDownYf = ObjectAnimator.ofFloat(female_text, "scaleY", 1.2f);
        ObjectAnimator scaleDownXtf = ObjectAnimator.ofFloat(female_icon, "scaleX", 1.2f);
        ObjectAnimator scaleDownYtf = ObjectAnimator.ofFloat(female_icon, "scaleY", 1.2f);
        scaleDownXf.setDuration(500);
        scaleDownYf.setDuration(500);
        scaleDownXtf.setDuration(500);
        scaleDownYtf.setDuration(500);

        AnimatorSet scaleupf = new AnimatorSet();
        scaleupf.playTogether(scaleDownXf,scaleDownYf,scaleDownXtf,scaleDownYtf);
        scaleupf.start();

    }
    public void scalemaleiconandtextError(){
        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
               // malepanel.setBackgroundColor(Color.parseColor("#ffffff"));
                male_icon.setImageResource(R.drawable.male_white);

            }
        }.start();
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(male_text, "scaleX", 1f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(male_text, "scaleY", 1f);
        ObjectAnimator scaleDownXt = ObjectAnimator.ofFloat(male_icon, "scaleX", 1f);
        ObjectAnimator scaleDownYt = ObjectAnimator.ofFloat(male_icon, "scaleY", 1f);
        scaleDownX.setDuration(500);
        scaleDownY.setDuration(500);
        scaleDownXt.setDuration(500);
        scaleDownYt.setDuration(500);

        AnimatorSet scaleup = new AnimatorSet();
        scaleup.playTogether(scaleDownX,scaleDownY,scaleDownXt,scaleDownYt);
        scaleup.start();

    }
    public void scalefemaleiconandtextError(){

        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
             //   femalepanel.setBackgroundColor(Color.parseColor("#ffffff"));
                female_icon.setImageResource(R.drawable.female_white);

            }
        }.start();
        ObjectAnimator scaleDownXf = ObjectAnimator.ofFloat(female_text, "scaleX", 1f);
        ObjectAnimator scaleDownYf = ObjectAnimator.ofFloat(female_text, "scaleY", 1f);
        ObjectAnimator scaleDownXtf = ObjectAnimator.ofFloat(female_icon, "scaleX", 1f);
        ObjectAnimator scaleDownYtf = ObjectAnimator.ofFloat(female_icon, "scaleY", 1f);
        scaleDownXf.setDuration(500);
        scaleDownYf.setDuration(500);
        scaleDownXtf.setDuration(500);
        scaleDownYtf.setDuration(500);

        AnimatorSet scaleupf = new AnimatorSet();
        scaleupf.playTogether(scaleDownXf,scaleDownYf,scaleDownXtf,scaleDownYtf);
        scaleupf.start();

    }
    private class SendGender extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";

            try {
                url = new URL(params[0]);
                ref_id = params[1];
                Log.v("URL", "URL: " + url);
                Log.v("postParameters",ref_id);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();

                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.v("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        enableGenderFields();
                        scalefemaleiconandtextError();
                        scalemaleiconandtextError();
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            if(s.contains("successfully")){
                Log.e("respis","Successfullysaved gender");
                long id2 = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                if (id2 > 0) {
                    dataBaseHelper.updateTableDetails(String.valueOf(id2), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER, tag_gender);
                }
                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        gender_relative.setVisibility(View.GONE);
                        ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
                        ArrayList<EducationalDetails> arrayListedu = dataBaseHelper.getEducationalDetails();

                        if ( (professionals.size()<1)&&(arrayListedu.size()<1) ) {
                            Log.e("mainre","pro empty");
                           // student_professional_relative.setVisibility(View.VISIBLE);
                        } else{
                            Log.e("mainre","pro notempty");
                            info_relative.setVisibility(View.GONE);

                        }

                        //student_professional_relative.setVisibility(View.VISIBLE);
                    }
                }.start();
            }else {
                Toast.makeText(MainPlanetActivity.this,"Please try again",Toast.LENGTH_SHORT).show();

                enableGenderFields();
                scalefemaleiconandtextError();
                scalemaleiconandtextError();
            }

        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
    public void disableGenderFields(){
        gender_relative.setEnabled(false);
        malepanel.setEnabled(false);
        femalepanel.setEnabled(false);
    }
    public void enableGenderFields(){
        gender_relative.setEnabled(true);
        malepanel.setEnabled(true);
        femalepanel.setEnabled(true);
    }
    public void scalestudenticonandtext(){
        student_icon.setImageResource(R.drawable.student_blue);
        jn_profession = new JsonBuilder(new GsonAdapter())
                .object("candidateId", PreferenceUtils.getCandidateId(MainPlanetActivity.this))
                .object("professionalStatus", "Student")
                .build().toString();

        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
              //  studentpanel.setBackgroundColor(Color.parseColor("#9BEDCA"));

            }
        }.start();
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(student_text, "scaleX", 1.2f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(student_text, "scaleY", 1.2f);
        ObjectAnimator scaleDownXt = ObjectAnimator.ofFloat(student_icon, "scaleX", 1.2f);
        ObjectAnimator scaleDownYt = ObjectAnimator.ofFloat(student_icon, "scaleY", 1.2f);
        scaleDownX.setDuration(500);
        scaleDownY.setDuration(500);
        scaleDownXt.setDuration(500);
        scaleDownYt.setDuration(500);

        AnimatorSet scaleup = new AnimatorSet();
        scaleup.playTogether(scaleDownX,scaleDownY,scaleDownXt,scaleDownYt);
        scaleup.start();

    }
    public void scaleprofessionaliconandtext(){
        professional_icon.setImageResource(R.drawable.professtion_blue);

         jn_profession = new JsonBuilder(new GsonAdapter())
                .object("candidateId", PreferenceUtils.getCandidateId(MainPlanetActivity.this))
                .object("professionalStatus", "Professional")
                .build().toString();

        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
               // professionalpanel.setBackgroundColor(Color.parseColor("#9BEDCA"));

            }
        }.start();
        ObjectAnimator scaleDownXf = ObjectAnimator.ofFloat(professional_text, "scaleX", 1.2f);
        ObjectAnimator scaleDownYf = ObjectAnimator.ofFloat(professional_text, "scaleY", 1.2f);
        ObjectAnimator scaleDownXtf = ObjectAnimator.ofFloat(professional_icon, "scaleX", 1.2f);
        ObjectAnimator scaleDownYtf = ObjectAnimator.ofFloat(professional_icon, "scaleY", 1.2f);
        scaleDownXf.setDuration(500);
        scaleDownYf.setDuration(500);
        scaleDownXtf.setDuration(500);
        scaleDownYtf.setDuration(500);

        AnimatorSet scaleupf = new AnimatorSet();
        scaleupf.playTogether(scaleDownXf,scaleDownYf,scaleDownXtf,scaleDownYtf);
        scaleupf.start();

    }
    public void scalestudenticonandtextError(){
        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
               // studentpanel.setBackgroundColor(Color.parseColor("#ffffff"));
                student_icon.setImageResource(R.drawable.student_white);
            }
        }.start();
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(student_text, "scaleX", 1f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(student_text, "scaleY", 1f);
        ObjectAnimator scaleDownXt = ObjectAnimator.ofFloat(student_icon, "scaleX", 1f);
        ObjectAnimator scaleDownYt = ObjectAnimator.ofFloat(student_icon, "scaleY", 1f);
        scaleDownX.setDuration(500);
        scaleDownY.setDuration(500);
        scaleDownXt.setDuration(500);
        scaleDownYt.setDuration(500);

        AnimatorSet scaleup = new AnimatorSet();
        scaleup.playTogether(scaleDownX,scaleDownY,scaleDownXt,scaleDownYt);
        scaleup.start();

    }
    public void scaleprofessionaliconandtextError(){

        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
            //    professionalpanel.setBackgroundColor(Color.parseColor("#ffffff"));
                professional_icon.setImageResource(R.drawable.professtion_white);

            }
        }.start();
        ObjectAnimator scaleDownXf = ObjectAnimator.ofFloat(professional_text, "scaleX", 1f);
        ObjectAnimator scaleDownYf = ObjectAnimator.ofFloat(professional_text, "scaleY", 1f);
        ObjectAnimator scaleDownXtf = ObjectAnimator.ofFloat(professional_icon, "scaleX", 1f);
        ObjectAnimator scaleDownYtf = ObjectAnimator.ofFloat(professional_icon, "scaleY", 1f);
        scaleDownXf.setDuration(500);
        scaleDownYf.setDuration(500);
        scaleDownXtf.setDuration(500);
        scaleDownYtf.setDuration(500);

        AnimatorSet scaleupf = new AnimatorSet();
        scaleupf.playTogether(scaleDownXf,scaleDownYf,scaleDownXtf,scaleDownYtf);
        scaleupf.start();

    }
    private class SendProfessionalstatus extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";

            try {
                url = new URL(params[0]);
                ref_id = params[1];
                Log.v("URL", "URL: " + url);
                Log.v("postParameters",ref_id);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();

                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.v("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        enableproFields();
                        scaleprofessionaliconandtextError();
                        scalestudenticonandtextError();;
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            if(s.contains("successfully")){
                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        student_professional_relative.setVisibility(View.GONE);
                        college_company_list_relative.setVisibility(View.VISIBLE);
                        if(professionval.equalsIgnoreCase("student")){
                            college_panel.setVisibility(View.VISIBLE);
                            company_panel.setVisibility(View.GONE);
                        }else {
                            college_panel.setVisibility(View.GONE);
                            company_panel.setVisibility(View.VISIBLE);
                        }
                    }
                }.start();
                Log.e("respis","Successfullysaved profession");

            }else {
                Toast.makeText(MainPlanetActivity.this,"Please try again",Toast.LENGTH_SHORT).show();
                enableproFields();
                scaleprofessionaliconandtextError();
                scalestudenticonandtextError();
            }

        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }

    public void disableproFields(){
        student_professional_relative.setEnabled(false);
        studentpanel.setEnabled(false);
        professionalpanel.setEnabled(false);
    }
    public void enableproFields(){
        student_professional_relative.setEnabled(true);
        studentpanel.setEnabled(true);
        professionalpanel.setEnabled(true);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COLLEGE_PICKER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                next_btn.setEnabled(true);
                String college = data.getStringExtra("COLLEGENAME");
                String collegeId = data.getStringExtra("id");
                cN=college;
                Log.e("college resp",college+"  "+collegeId);
                collegename.setText(college);
            }
        }
        if (requestCode == ORGANIZATION_PICKER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                next_btn.setEnabled(true);
                String company_is = data.getStringExtra("INDUSTRIES");
                String companyId = data.getStringExtra("id");
                oN=company_is;
                Log.e("company resp",company_is+"  "+companyId);
                companyname.setText(company_is);
            }
        }

    }

    private class SendProfessionalData extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";

            try {
                url = new URL(params[0]);
                ref_id = params[1];
                Log.v("URL", "URL: " + url);
                Log.v("postParameters",ref_id);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /*urlConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");*/
                urlConnection.setDoOutput(true);
                urlConnection.setFixedLengthStreamingMode(
                        ref_id.getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(ref_id);
                wr.flush();

                //int responseCode = urlConnection.getResponseCode();


                //if(responseCode == HttpURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
                Log.v("Response", responseString);
                result=responseString;

                /*}else{
                    Log.v("Response Code", "Response code:"+ responseCode);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        enableproFields();
                        scaleprofessionaliconandtextError();
                        scalestudenticonandtextError();;
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            if(s.contains("successfully")){
                Log.e("respis","Successfullysaved profession");
                if(professionval.equalsIgnoreCase("student")){

                    String jsis=getJsonedudata(cN);
                    Log.e("jsisst",jsis);
                    JsonParser jsonParser = new JsonParser();
                    JsonObject gsonObject = (JsonObject) jsonParser.parse(jsis);
                    UpdateProfileRequest request = new UpdateProfileRequest(PreferenceUtils.getCandidateId(MainPlanetActivity.this), MainPlanetActivity.this);


                    Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(MainPlanetActivity.this, MainPlanetActivity.this);
                    processor.updateProfile(PreferenceUtils.getCandidateId(MainPlanetActivity.this), gsonObject);

                }else {
                    String jsis=getJsonprodata(oN);
                    Log.e("jsispro",jsis);

                    JsonParser jsonParser = new JsonParser();
                    JsonObject gsonObject = (JsonObject) jsonParser.parse(jsis);

                    UpdateProfileRequest request = new UpdateProfileRequest(PreferenceUtils.getCandidateId(MainPlanetActivity.this), MainPlanetActivity.this);


                    Log.e("updateResponse", "" + request.getURLEncodedPostdata().toString());
                    RetrofitRequestProcessor processor = new RetrofitRequestProcessor(MainPlanetActivity.this, MainPlanetActivity.this);
                    processor.updateProfile(PreferenceUtils.getCandidateId(MainPlanetActivity.this), gsonObject);


                }

            }else {
                Toast.makeText(MainPlanetActivity.this,"Please try again",Toast.LENGTH_SHORT).show();

            }

        }

        private String readStream(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }


    public void showverificationDialogueMain() {
        alertDialogis = new Dialog(MainPlanetActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialogis.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogis.setContentView(R.layout.verify_mainscreen_popup);
        Window window = alertDialogis.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        //alertDialogis.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView resesndbtn = (FontTextView)alertDialogis.findViewById(R.id.resesndbtn);
        FrameLayout frameLayout=(FrameLayout)alertDialogis.findViewById(R.id.frame_main);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogis.dismiss();
            }
        });
        resesndbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogis.dismiss();
            }
        });

        resesndbtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    resesndbtn.setBackgroundResource(R.drawable.skip_click);
                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            resesndbtn.setBackgroundResource(R.drawable.skip_profile);
                        }
                    }.start();

                }


                return false;
            }
        });




        try{
            alertDialogis.show();
        }
        catch (Exception e)
        {}

    }

    public void getFrKey(){
        AppConstant appConstant=new AppConstant(MainPlanetActivity.this);
        appConstant.getFrkey();
    }

    public static void getFaceClientObj(){
        faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points,new PrefManager(mcont).get_frkey());

        //
        if(!new PrefManager(mcont).isProfileSkipped()) {
            checkIfProfilePicAvailable();
        }
    }
}
