package com.globalgyan.getvalyou;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.simulation.DownloadEntryModel;
import com.globalgyan.getvalyou.simulation.FiledownloadModel;
import com.globalgyan.getvalyou.simulation.GallaryAdapter;
import com.globalgyan.getvalyou.simulation.GallaryModel;
import com.globalgyan.getvalyou.simulation.SimulationActivity;
import com.globalgyan.getvalyou.simulation.core.DownloadManagerPro;
import com.globalgyan.getvalyou.simulation.report.listener.DownloadManagerListener;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.globalgyan.getvalyou.apphelper.BatteryReceiver.flag_play_anim;

public class WebviewVideoActivity extends AppCompatActivity implements DownloadManagerListener {
    static DownloadManagerPro dm;
    static File folder,folderExtracted;
    private MyWebChromeClient mWebChromeClient = null;
    private View mCustomView;
    private RelativeLayout mContentView;
    private FrameLayout mCustomViewContainer;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    String url="";
    CircularProgressBar downloadbar;
    private WebView myWebView;
    TextView per_text,text_dwnl;
    ImageView download_button;
    String name_file,fullname;
    ConnectionUtils connectionUtils;

    Context context;
    Dialog dialog2;
    AlertDialog alertDialog;

    RelativeLayout rl;
    ObjectAnimator astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;
    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_video);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        appConstant = new AppConstant(this);
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        myWebView = (WebView) findViewById(R.id.webView);
        downloadbar=(CircularProgressBar)findViewById(R.id.downloadbar);
        rl=(RelativeLayout)findViewById(R.id.main_rel);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        context = WebviewVideoActivity.this;
        per_text=(TextView)findViewById(R.id.text_percentage);
        connectionUtils=new ConnectionUtils(WebviewVideoActivity.this);
        url=getIntent().getStringExtra("link");

        text_dwnl=(TextView)findViewById(R.id.text_dwnl);
        download_button=(ImageView)findViewById(R.id.download_button);
        createFolder();
        dm = new DownloadManagerPro(this.getApplicationContext());
        dm.init(folder.getPath(), 12, WebviewVideoActivity.this);
        String [] pic=url.split("/");
        String last=pic[pic.length-1];
        String [] lastname=last.split("\\.");
        name_file=lastname[0];
        fullname=last;
        if(url.contains(".pdf")){
            url="http://docs.google.com/gview?embedded=true&url="+url;
            loadPagePdf();
        }else {
            loadPage();
        }
      /*  try{
            File file=new File(folder,name_file);
            if(file.exists()){
                try {
                    File htmlfile = new File(folder, name_file + "index.html");
                    if (htmlfile.exists()) {
                        loadPage();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("status", "completed");
                                createFolderExtracted();
                                File file = new File(folder, fullname);
                                downloadbar.setVisibility(View.GONE);
                                per_text.setVisibility(View.GONE);
                                text_dwnl.setText("Please wait...");
                                extractiontask(file.getPath(), "New" + name_file);
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("status", "completed");
                            createFolderExtracted();
                            File file = new File(folder, fullname);

                            text_dwnl.setText("Please wait...");
                            extractiontask(file.getPath(), "New" + name_file);
                        }
                    });
                }

            }else {
                downloadData(url,name_file);
            }
        }catch (Exception e){
            downloadData(url,name_file);
        }
*/
        download_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                download_button.setEnabled(false);
                download_button.setClickable(false);
                new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        download_button.setEnabled(true);
                        download_button.setClickable(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()){
                    downloadData(url,name_file);

                }else {
                   Toast.makeText(WebviewVideoActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    public class MyWebChromeClient extends WebChromeClient {

        FrameLayout.LayoutParams LayoutParameters = new FrameLayout.LayoutParams(rl.getWidth(),rl.getHeight());

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mContentView = (RelativeLayout) findViewById(R.id.main_rel);
            mContentView.setVisibility(View.GONE);
            mCustomViewContainer = new FrameLayout(WebviewVideoActivity.this);
            mCustomViewContainer.setLayoutParams(LayoutParameters);
            mCustomViewContainer.setBackgroundResource(android.R.color.black);
            view.setLayoutParams(LayoutParameters);
            mCustomViewContainer.addView(view);
            mCustomView = view;
            mCustomViewCallback = callback;
            mCustomViewContainer.setVisibility(View.VISIBLE);
            setContentView(mCustomViewContainer);
        }

        @Override
        public void onHideCustomView() {
            if (mCustomView == null) {
                return;
            } else {
                // Hide the custom view.
                mCustomView.setVisibility(View.GONE);
                // Remove the custom view from its container.
                mCustomViewContainer.removeView(mCustomView);
                mCustomView = null;
                mCustomViewContainer.setVisibility(View.GONE);
                mCustomViewCallback.onCustomViewHidden();
                // Show the content view.
                mContentView.setVisibility(View.VISIBLE);
                setContentView(mContentView);
            }
        }
    }


    @Override
    public void onBackPressed() {


            closewebwindowonback();

    }

    public void closewebwindowonback() {

        dialog2 = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.webview_quit_alert);
        Window window = dialog2.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog2.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dialog2.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final ImageView astro = (ImageView) dialog2.findViewById(R.id.astro);
        final ImageView alien = (ImageView) dialog2.findViewById(R.id.alien);


        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView no = (ImageView) dialog2.findViewById(R.id.no_quit);
        final ImageView yes = (ImageView) dialog2.findViewById(R.id.yes_quit);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                no.setEnabled(false);
                yes.setEnabled(false);

                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        dialog2.dismiss();
                    }
                }.start();


            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                no.setEnabled(false);

                yes.setEnabled(false);

                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        dialog2.dismiss();

                        new PrefManager(WebviewVideoActivity.this).isclicked("yes");

                        new PrefManager(WebviewVideoActivity.this).isResultscreenpause("no");

                        if(new PrefManager(WebviewVideoActivity.this).get_sm_nav_from().equalsIgnoreCase("ass")){
                            Intent i = new Intent(WebviewVideoActivity.this, HomeActivity.class );
                            Log.e("navigation","ass");
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                            i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("tabs","normal");
                            finish();
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        }
                        else {
                            if (new PrefManager(WebviewVideoActivity.this).getstatusforFeedback().equalsIgnoreCase("completed")) {
                                Intent i = new Intent(WebviewVideoActivity.this, ModulesActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                            } else {
                                if (new PrefManager(WebviewVideoActivity.this).getcompulsaryfeedback().equalsIgnoreCase("yes")) {
                                    Intent it = new Intent(WebviewVideoActivity.this, FeedbackActivity.class);
              /*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*/
                                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(it);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    finish();
                                } else {
                                    Intent i = new Intent(WebviewVideoActivity.this, ModulesActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                                    i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    finish();
                                    startActivity(i);
                                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                }


                            }


                        }

                    }
                }.start();

            }
        });

        try {
            dialog2.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void downloadData(String uri, String substring) {


        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {

            dm.startDownload(task);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void createFolder() {
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "ArticulateData");
        boolean success = true;
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }
    @Override
    public void OnDownloadStarted(long taskId) {
        Log.e("status",String.valueOf("started"));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                downloadbar.setVisibility(View.VISIBLE);
                per_text.setVisibility(View.VISIBLE);
                text_dwnl.setVisibility(View.VISIBLE);
                download_button.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void OnDownloadPaused(long taskId) {
        Log.e("status",String.valueOf("puase"));

    }

    @Override
    public void onDownloadProcess(long taskId, final double percent, long downloadedLength) {
        Log.e("status",String.valueOf(percent));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        downloadbar.setProgress((float) percent);
                        per_text.setText(String.valueOf((int)percent)+"%");
                    }
                });
            }
        });
    }

    @Override
    public void OnDownloadFinished(long taskId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.e("status","completed");
                createFolderExtracted();
                File file=new File(folder,fullname);
                downloadbar.setProgress(100);
                per_text.setText("100%");
                text_dwnl.setText("Completed");
                extractiontask(file.getPath(),"New"+name_file);
            }
        });

    }

    private void extractiontask(final String path, final String newazr) {

        new CountDownTimer(2000, 2000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                downloadbar.setVisibility(View.GONE);
                per_text.setVisibility(View.GONE);
                text_dwnl.setText("Please wait...");
                new ExtractFile().execute(path,newazr);

            }
        }.start();
    }


    @Override
    public void OnDownloadRebuildStart(long taskId) {

    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {

    }

    @Override
    public void OnDownloadCompleted(long taskId) {
        Log.e("status","completed");

    }

    @Override
    public void connectionLost(long taskId) {

        dm.delete((int) taskId,true);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                download_button.setVisibility(View.VISIBLE);
                per_text.setVisibility(View.GONE);
                text_dwnl.setVisibility(View.GONE);
                downloadbar.setVisibility(View.GONE);
            }
        });
    }



    public class ExtractFile extends AsyncTask<String,Void,String> {
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equalsIgnoreCase("true")){
                Log.e("status","true");
                downloadbar.setVisibility(View.GONE);
                per_text.setVisibility(View.GONE);
                text_dwnl.setVisibility(View.GONE);
                myWebView.setVisibility(View.VISIBLE);
                String pathis=folder.getPath();
                Log.e("pathis",pathis);
                loadPage();
            }else {
                Log.e("status","false");
            }
        }

        @Override
        protected String doInBackground(String... voids) {

            InputStream is;
            ZipInputStream zis;
            try
            {
                String filename;
                is = new FileInputStream(voids[0]);
                zis = new ZipInputStream(new BufferedInputStream(is));
                ZipEntry ze;
                byte[] buffer = new byte[1024];
                int count;

                while ((ze = zis.getNextEntry()) != null)
                {
                    // zapis do souboru
                    filename = ze.getName();

                    // Need to create directories if not exists, or
                    // it will generate an Exception...
                    if (ze.isDirectory()) {
                        File fmd = new File(folderExtracted + filename);
                        fmd.mkdirs();
                        continue;
                    }

                    FileOutputStream fout = new FileOutputStream(folderExtracted + filename);

                    // cteni zipu a zapis
                    while ((count = zis.read(buffer)) != -1)
                    {
                        fout.write(buffer, 0, count);
                    }

                    fout.close();
                    zis.closeEntry();
                }

                zis.close();
            }
            catch(IOException e)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(WebviewVideoActivity.this,"Error while loading data",Toast.LENGTH_SHORT).show();
                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }
                });
                e.printStackTrace();
                return String.valueOf(false);
            }

            return String.valueOf(true);
        }
    }

    private void loadPage() {
        downloadbar.setVisibility(View.GONE);
        per_text.setVisibility(View.GONE);
        text_dwnl.setVisibility(View.GONE);
        myWebView.setVisibility(View.VISIBLE);
        File htmlfile=new File(folder,name_file+"index.html");
        Log.e("urlpath",htmlfile.getPath());
        /*mWebChromeClient = new MyWebChromeClient();
        myWebView.setWebChromeClient(mWebChromeClient);

        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });*/
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.getSettings().setAllowFileAccess(true);
        myWebView.getSettings().setDomStorageEnabled(true);
        myWebView.getSettings().setAppCachePath(String.valueOf(getApplicationContext()));
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        myWebView.getSettings().setAppCacheEnabled(true);
        myWebView.getSettings().setDisplayZoomControls(false);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setSupportZoom(false);
        myWebView.getSettings().setAllowFileAccess(true);
        myWebView.getSettings().setDatabaseEnabled(true);
        myWebView.getSettings().setLoadWithOverviewMode(true);
        myWebView.getSettings().setUseWideViewPort(true);
       // myWebView.loadUrl(htmlfile.getPath());
        myWebView.loadUrl(url);

    }


    private void loadPagePdf() {
        downloadbar.setVisibility(View.GONE);
        per_text.setVisibility(View.GONE);
        text_dwnl.setVisibility(View.GONE);
        myWebView.setVisibility(View.VISIBLE);
        File htmlfile=new File(folder,name_file+"index.html");
        Log.e("urlpath",htmlfile.getPath());
        mWebChromeClient = new MyWebChromeClient();
        myWebView.setWebChromeClient(mWebChromeClient);

        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.getSettings().setAllowFileAccess(true);
        myWebView.getSettings().setDomStorageEnabled(true);
        myWebView.getSettings().setAppCachePath(String.valueOf(getApplicationContext()));
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        myWebView.getSettings().setAppCacheEnabled(true);
        myWebView.getSettings().setDisplayZoomControls(false);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setSupportZoom(false);
        myWebView.getSettings().setAllowFileAccess(true);
        myWebView.getSettings().setDatabaseEnabled(true);
        myWebView.getSettings().setLoadWithOverviewMode(true);
        myWebView.getSettings().setUseWideViewPort(true);
        // myWebView.loadUrl(htmlfile.getPath());
        myWebView.loadUrl(url);

    }

    private void getAllFiles(String type_string) {
        String path = folder.getPath();
        Log.e("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();



        Log.e("Files", "Size: " + files.length);
    }

    public void closewebwindow(View view){

//        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
//                WebviewVideoActivity.this, R.style.DialogTheme);
//        LayoutInflater inflater = LayoutInflater.from(WebviewVideoActivity.this);
//        View dialogView = inflater.inflate(R.layout.quit_warning, null);
//
//        dialogBuilder.setView(dialogView);
//
//
//        alertDialog = dialogBuilder.create();
//
//
//
//        alertDialog.setCancelable(false);
//        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//
//        alertDialog.show();
//        final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
//
//        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
//        astro_outx.setDuration(300);
//        astro_outx.setInterpolator(new LinearInterpolator());
//        astro_outx.setStartDelay(0);
//        astro_outx.start();
//        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
//        astro_outy.setDuration(300);
//        astro_outy.setInterpolator(new LinearInterpolator());
//        astro_outy.setStartDelay(0);
//        astro_outy.start();
//
//
//        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);
//
//        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
//        alien_outx.setDuration(300);
//        alien_outx.setInterpolator(new LinearInterpolator());
//        alien_outx.setStartDelay(0);
//        alien_outx.start();
//        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
//        alien_outy.setDuration(300);
//        alien_outy.setInterpolator(new LinearInterpolator());
//        alien_outy.setStartDelay(0);
//        alien_outy.start();
//
//        ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
//
//        no.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
//                astro_inx.setDuration(300);
//                astro_inx.setInterpolator(new LinearInterpolator());
//                astro_inx.setStartDelay(0);
//                astro_inx.start();
//                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
//                astro_iny.setDuration(300);
//                astro_iny.setInterpolator(new LinearInterpolator());
//                astro_iny.setStartDelay(0);
//                astro_iny.start();
//
//                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
//                alien_inx.setDuration(300);
//                alien_inx.setInterpolator(new LinearInterpolator());
//                alien_inx.setStartDelay(0);
//                alien_inx.start();
//                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
//                alien_iny.setDuration(300);
//                alien_iny.setInterpolator(new LinearInterpolator());
//                alien_iny.setStartDelay(0);
//                alien_iny.start();
//
//                new CountDownTimer(400, 400) {
//
//
//                    @Override
//                    public void onTick(long l) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        alertDialog.dismiss();
//                    }
//                }.start();
//
//
//            }
//        });
//        ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);
//
//        yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//
//
//                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
//                astro_inx.setDuration(300);
//                astro_inx.setInterpolator(new LinearInterpolator());
//                astro_inx.setStartDelay(0);
//                astro_inx.start();
//                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
//                astro_iny.setDuration(300);
//                astro_iny.setInterpolator(new LinearInterpolator());
//                astro_iny.setStartDelay(0);
//                astro_iny.start();
//
//                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
//                alien_inx.setDuration(300);
//                alien_inx.setInterpolator(new LinearInterpolator());
//                alien_inx.setStartDelay(0);
//                alien_inx.start();
//                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
//                alien_iny.setDuration(300);
//                alien_iny.setInterpolator(new LinearInterpolator());
//                alien_iny.setStartDelay(0);
//                alien_iny.start();
//
//                new CountDownTimer(400, 400) {
//
//
//                    @Override
//                    public void onTick(long l) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        alertDialog.dismiss();
//                        finish();
//                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
//
//                    }
//                }.start();
//
//            }
//        });


        dialog2 = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.webview_quit_alert);
        Window window = dialog2.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog2.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dialog2.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final ImageView astro = (ImageView) dialog2.findViewById(R.id.astro);
        final ImageView alien = (ImageView) dialog2.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();

        }
        final ImageView no = (ImageView) dialog2.findViewById(R.id.no_quit);
        final ImageView yes = (ImageView) dialog2.findViewById(R.id.yes_quit);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                no.setEnabled(false);
                yes.setEnabled(false);
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        dialog2.dismiss();
                    }
                }.start();


            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                no.setEnabled(false);
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        dialog2.dismiss();

                        new PrefManager(WebviewVideoActivity.this).isclicked("yes");

                        new PrefManager(WebviewVideoActivity.this).isResultscreenpause("no");

                        if(new PrefManager(WebviewVideoActivity.this).get_sm_nav_from().equalsIgnoreCase("ass")){
                            Intent i = new Intent(WebviewVideoActivity.this, HomeActivity.class );
                            Log.e("navigation","ass");
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                            i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("tabs","normal");
                            finish();
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        }

                        else
                        if(new PrefManager(WebviewVideoActivity.this).getstatusforFeedback().equalsIgnoreCase("completed")){
                           Intent i = new Intent(WebviewVideoActivity.this, ModulesActivity.class );
                           i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                           i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                       i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                        }else {
                            if(new PrefManager(WebviewVideoActivity.this).getcompulsaryfeedback().equalsIgnoreCase("yes")) {
                                Intent it = new Intent(WebviewVideoActivity.this,FeedbackActivity.class);
              /*  it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));*/
                                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(it);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                             finish();
                            }
                            else
                            {
                               Intent i = new Intent(WebviewVideoActivity.this, ModulesActivity.class );
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                               i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                              i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                            }


                        }


                    }
                }.start();

            }
        });

        try {
            dialog2.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }


//        new CountDownTimer(3000, 3000) {
//            @Override
//            public void onTick(long l) {
//
//            }
//
//            @Override
//            public void onFinish() {
//                try {
//                    if (alertDialog.isShowing()) {
//                        astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
//                        astro_inx.setDuration(300);
//                        astro_inx.setInterpolator(new LinearInterpolator());
//                        astro_inx.setStartDelay(0);
//                        astro_inx.start();
//                        astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
//                        astro_iny.setDuration(300);
//                        astro_iny.setInterpolator(new LinearInterpolator());
//                        astro_iny.setStartDelay(0);
//                        astro_iny.start();
//
//                        alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
//                        alien_inx.setDuration(300);
//                        alien_inx.setInterpolator(new LinearInterpolator());
//                        alien_inx.setStartDelay(0);
//                        alien_inx.start();
//                        alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
//                        alien_iny.setDuration(300);
//                        alien_iny.setInterpolator(new LinearInterpolator());
//                        alien_iny.setStartDelay(0);
//                        alien_iny.start();
//
//                        new CountDownTimer(400, 400) {
//
//
//                            @Override
//                            public void onTick(long l) {
//
//                            }
//
//                            @Override
//                            public void onFinish() {
//                                alertDialog.dismiss();
//
//
//                            }
//                        }.start();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();


    }



    private void createFolderExtracted() {
        folderExtracted = new File(Environment.getExternalStorageDirectory() +
                File.separator + "ArticulateData/"+name_file);
        boolean success = true;
        if (!folderExtracted.exists()) {
            folderExtracted.mkdirs();
        }
    }


}


