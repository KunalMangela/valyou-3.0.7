package com.globalgyan.getvalyou;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Welcome2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ViewPager pager = (ViewPager) findViewById(R.id.container);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

//                case 0: return IntroOne.newInstance("FirstFragment, Instance 1");
//                case 1: return IntroTwo.newInstance("SecondFragment, Instance 1");
//                case 2: return IntroThree.newInstance("ThirdFragment, Instance 1");
//                //default: return new IntroOne();
//
//                default: return IntroOne.newInstance("firstfrag, Default");

                case 0: return new IntroOne();
                case 1: return new IntroTwo();
                case 2: return new IntroThree();
                //default: return new IntroOne();

                default: return new IntroOne();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}