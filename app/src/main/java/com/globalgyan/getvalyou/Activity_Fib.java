package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.com.google.gson.JsonArray;
import com.amazonaws.com.google.gson.JsonObject;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.androidhiddencamera.CameraConfig;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.customwidget.FlowLayout;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.listeners.PictureCapturingListener;
import com.globalgyan.getvalyou.model.QuestionModel;
import com.globalgyan.getvalyou.services.APictureCapturingService;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.iambedant.text.OutlineTextView;
import com.loopj.android.http.HttpGet;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.VerifyResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.UUID;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import tyrantgit.explosionfield.ExplosionField;

import static com.globalgyan.getvalyou.ProPic.mFaceId1;

/**
 * Created by NaNi on 28/09/17.
 */

public class Activity_Fib extends AppCompatActivity  implements PictureCapturingListener{


    private APictureCapturingService pictureService;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    Face mface=null;
    UUID faceid1,faceid2;
    public String now;
    boolean checkonp=true;
    CameraConfig mCameraConfig;
    boolean wtf;
    ExplosionField mExplosionField;
    File fol;
    String newquid;
    List<Face> faces;
    ArrayList<String> qid_array;
    Dialog dialog,dialoge;
    int code1,code2,code3,optionsize;
    private CountDownTimer countDownTimer;
    private  long startTime = 90 * 1000;
    private final long interval = 1 * 1000;
    private long secLeft = startTime;

    DefaultHttpClient httpClient;
    HttpURLConnection urlConnection;
    Boolean flag_is_present_id=false;

    static  boolean flag_ques_done = false;

    int picture_count=0;
    int pic_frequency;

    //  KProgressHUD qs_progress;
    // KProgressHUD verification_progress;
    //  KProgressHUD assess_progress;
    // KProgressHUD progressDialog;



    Dialog qs_progress;
    Dialog verification_progress;
    Dialog assess_progress;
    Dialog progressDialog;
    IntentFilter filter;


    HttpPost httpPost;
    HttpGet httpGet;
    int qansr=0;
    Vibrator vibrator;
    Bitmap storedpic;
    FontTextView timerrr, dqus,timer_img;
    OutlineTextView qno;
    String tres="";
    int cqa = 0;
    int count_detect_no=0,image_face_count=0,auto_capt_count=0;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null;

    private SimpleLocation location;

    ArrayList<String> qidj,ansj,stj,endj,test,qtid;
    JSONArray questions;
    static String json = "", jso = "", Qus = "",Qs="", token = "", token_type = "", quesDis = "";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int score = 0;
    ArrayList<String> optioncard;
    ImageButton pause;

    public static RecyclerView rc_view;
    public static Fib_adapter fib_adapter;
    URL image1url,image2url,image3url;
    public static final String IMAGE_DIRECTORY = "Face Detection";

    //leveling variables
    public static int correctquestioncount=0,twointgap=0,levelis=0;
    public static boolean correctquestionflag1=false,correctquestionflag2=false;
    public static boolean islevelingenable=false,isques_limit_exceed=false;
    File folder;
    //first four questions correct ccount
    public static int firstfourcorrectquestioncount=0;
    //first four questions completion flag
    public static boolean firstfourquestionflag=false;


    QuestionModel qsmodel;
    ArrayList<QuestionModel> qs=new ArrayList<>();
    int pauseCount = 0, qc = 0, qid = 0,tq=0,indexis=0;
    int bgcolor;

    FrameLayout afbg;
    String uidd,adn,ceid,gdid,game,cate,tgid,sid,aid,assessg,tg_groupid,certifcation_id,course_id,lg_id;
    int currentlevel,maxlevel,q_count,min_level,int_qs_size;
    String candidateId;
    public static String ans_submission;
    float scale1x,scale2x,scale3x,scale4x,scale5x,scale1y,scale2y,scale3y,scale4y,scale5y;
    File filepro;
    SimpleDateFormat df1;
    Calendar c;

    RelativeLayout timer_ques;


    boolean flag_netcheck=false;


    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR
            = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR
            = new AccelerateInterpolator();
    ConnectionUtils connectionUtils;


    private Camera camera;
    Timer timer1;
    public static String mImageFileLocation = "";
    public int cameraId = 2;
    Bitmap bitmap;
    Uri imageUri;
    Bitmap bmpother;
    CountDownTimer waitTimer;
    FaceServiceClient faceServiceClient;
    String faceid;
    Bitmap decodedByte;
    int detectioncount = 0;
    String img1,img2="",img3="";

    PrefManager prefManager;
    private CameraPreview mPreview;
    FrameLayout preview;
    ArrayList<UUID> detectedfaceid = new ArrayList<>();
    ArrayList<String> imagelocation = new ArrayList<>();
    ArrayList<Bitmap> autotakenpic = new ArrayList<>();
    ImageView close;
    //remain
    ArrayList<String > facedetectedimagelocation = new ArrayList<>();
    String autocapimg1loc1, autocapimg1loc2,autocapimg1loc3;
    ArrayList<String > autocapimgloc = new ArrayList<>();
    ArrayList<String > imgurllist = new ArrayList<>();
    String replacedurl;
    int piccapturecount;
    public static int crct;
    int i=0;
    Dialog alertDialog;



    //local storage
    JSONArray ques_ans_array=new JSONArray();


    BatteryManager bm;
    int batLevel;
    static Animation blinking_anim;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2,astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;

    public static MediaPlayer mp_click;
    BatteryReceiver mBatInfoReceiver;
    boolean isPowerSaveMode;
    PowerManager pm;
    AppConstant appConstant;
    String get_nav = " ";
    String first_msg = "", quit_msg="";


    //new
    public static File get_quesdata,get_ansdata,getdatafromfile;
    public static String old_get_ques_data,formdatanewstring="",mainformdatastring="",ans_submission_string="",ansdatafromfile="",postvaluearray="",postvalues="";
    public static boolean flaggameattempfirst=true;
    LinearLayout layoutbg;


    String mfaceid;
    UUID mfaceid1;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fib);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        location = new SimpleLocation(this);
        appConstant = new AppConstant(this);

        layoutbg=(LinearLayout)findViewById(R.id.layoutbg);
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        afbg=(FrameLayout)findViewById(R.id.activity_fib_bg);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        qsmodel=new QuestionModel();
        timer_ques = (RelativeLayout)findViewById(R.id.rl_above);
        c = Calendar.getInstance();
        filter = new IntentFilter("Show_screen_fib");
        this.registerReceiver(mReceiver, filter);
        optioncard = new ArrayList<>();
       // createFolder();
        fib_adapter=new Fib_adapter(Activity_Fib.this,optioncard);
        rc_view=(RecyclerView)findViewById(R.id.main_recycler);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_view.setLayoutManager(linearLayoutManager1);
        rc_view.setAdapter(fib_adapter);


        close=(ImageView)findViewById(R.id.close);

        mp_click= MediaPlayer.create(this,R.raw.button_click);

        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);



        connectionUtils=new ConnectionUtils(Activity_Fib.this);
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        qs_progress = KProgressHUD.create(Activity_Fib.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);





            get_nav =new PrefManager(Activity_Fib.this).getNav();


        qs_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        qs_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qs_progress.setContentView(R.layout.planet_loader);
        Window window = qs_progress.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        qs_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        qs_progress.setCancelable(false);

        GifView gifView1 = (GifView) qs_progress.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();


        verification_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        verification_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verification_progress.setContentView(R.layout.planet_loader);
        Window window1 = verification_progress.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        verification_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        verification_progress.setCancelable(false);
        GifView gifView2 = (GifView) verification_progress.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();


        qid_array=new ArrayList<>();
        camera = getCameraInstance();

        preview = (FrameLayout) findViewById(R.id.camera_preview);
        faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(Activity_Fib.this).get_frkey());

        assess_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        assess_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assess_progress.setContentView(R.layout.planet_loader);
        Window window2 = assess_progress.getWindow();
        WindowManager.LayoutParams wlp2 = window2.getAttributes();
        wlp2.gravity = Gravity.CENTER;
        wlp2.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window2.setAttributes(wlp2);
        assess_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        assess_progress.setCancelable(false);
        GifView gifView3 = (GifView) assess_progress.findViewById(R.id.loader);
        gifView3.setVisibility(View.VISIBLE);
        gifView3.play();
        gifView3.setGifResource(R.raw.loader_planet);
        gifView3.getGifResource();


         blinking_anim = new AlphaAnimation(0.0f, 1.0f);
        blinking_anim.setDuration(500);
        blinking_anim.setStartOffset(10);
        blinking_anim.setRepeatMode(Animation.REVERSE);
        blinking_anim.setRepeatCount(Animation.INFINITE);


        progressDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.planet_loader);
        Window window3 = progressDialog.getWindow();
        WindowManager.LayoutParams wlp3 = window3.getAttributes();
        wlp3.gravity = Gravity.CENTER;
        wlp3.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window3.setAttributes(wlp3);
        progressDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressDialog.setCancelable(false);
        GifView gifView4 = (GifView) progressDialog.findViewById(R.id.loader);
        gifView4.setVisibility(View.VISIBLE);
        gifView4.play();
        gifView4.setGifResource(R.raw.loader_planet);
        gifView4.getGifResource();




        mExplosionField = ExplosionField.attach2Window(this);
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        candidateId =PreferenceUtils.getCandidateId(Activity_Fib.this);
        qidj = new ArrayList<>();
        ansj = new ArrayList<>();
        stj = new ArrayList<>();
        endj = new ArrayList<>();
        test=new ArrayList<>();
        qtid=new ArrayList<>();






        try{
            uidd= getIntent().getExtras().getString("uidg");
            adn= getIntent().getExtras().getString("adn");
            ceid= getIntent().getExtras().getString("ceid");
            gdid= getIntent().getExtras().getString("gdid");
            game= getIntent().getExtras().getString("game");
            cate= getIntent().getExtras().getString("category");
            startTime= Long.parseLong(getIntent().getExtras().getString("timeis"));
            tgid = getIntent().getExtras().getString("tgid");
            aid=getIntent().getExtras().getString("aid");
            assessg=getIntent().getExtras().getString("assessgroup");
            token = getIntent().getExtras().getString("token");
            tg_groupid = getIntent().getExtras().getString("TG_GroupId");
            certifcation_id = getIntent().getExtras().getString("certification_id");
            course_id = getIntent().getExtras().getString("course_id");
            lg_id = getIntent().getExtras().getString("lg_id");
            pic_frequency = getIntent().getIntExtra("pic_frequency",3);


            Log.e("pic_frequency",""+pic_frequency);


        }catch (Exception ex){

        }

        //new changes
        jsonMakeFile(cate+game+gdid+ceid+uidd+tgid," ");

        first_msg = "We are submitting your answers and you will not be able to answer again the same category";

        quit_msg = first_msg+ " "+ cate;

        timerrr = (FontTextView) findViewById(R.id.timerr);
        timer_img = (FontTextView) findViewById(R.id.timer_img);

        qno = (OutlineTextView) findViewById(R.id.qno);
        Typeface typeface3 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        qno.setTypeface(typeface3,Typeface.BOLD);
        dqus = (FontTextView) findViewById(R.id.qdis);
        countDownTimer = new MyCountDownTimer(startTime, interval);
        timerrr.setText(String.valueOf(startTime / 1000));
        pause = (ImageButton) findViewById(R.id.pause);
        // pictureService = PictureCapturingServiceImpl.getInstance(this);


        prefManager = new PrefManager(Activity_Fib.this);
        mfaceid = prefManager.getprofilefaceid();

        mfaceid1 = UUID.fromString(mfaceid);


        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pauseCount == 0) {
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    dialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.pause_popup);
                    Window window = dialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                    window.setAttributes(wlp);
                    dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                    dialog.setCancelable(false);
                    try {
                        dialog.show();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    Button pu =(Button)dialog.findViewById(R.id.resume);
                    pu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                          /*  if(qc<qs.size()-1) {

                                Collections.shuffle(qs);
                                changeQues();
                            }*/
                            dialog.dismiss();
                          /*  countDownTimer = new MyCountDownTimer(secLeft, interval);
                            countDownTimer.start();*/
                            pauseCount++;
                        }
                    });

                } else {
                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        scoreActivityandPostscore();
                    } else {
                        tres = formdata();
                        checkonp = false;
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        scoreActivityandPostscore();
                    }
                }


            }
        });



      //  final CheckInternetTask t923=new CheckInternetTask();
       // t923.execute();

        new CountDownTimer(100,100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                //t923.cancel(true);

                //new changes remove show dialogue call and add it into Getquestions
                ConnectionUtils connectionUtils=new ConnectionUtils(Activity_Fib.this);
                if (connectionUtils.isConnectionAvailable()) {
                  //  flag_netcheck = false;

                    if(flaggameattempfirst){
                        new GetQues().execute();
                    }else {
                        loadofflinedata();
                    }

                }else {
                    try {
                      //  qs_progress.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                  //  flag_netcheck=false;
                    Toast.makeText(Activity_Fib.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }.start();


        // checkfacedetection();
    }




    public String formdata() {
        String jn = "";
        try{

            JSONObject student1 = new JSONObject();
            student1.put("U_Id", uidd);
            student1.put("S_Id", sid);
            student1.put("Q_Id", newquid);
            student1.put("TG_Id", tgid);
            student1.put("SD_UserAnswer", "E");
            student1.put("SD_StartTime", df1.format(c.getTime()).toString());
            student1.put("SD_EndTime", df1.format(c.getTime()).toString());


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(student1);


            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn = studentsObj.toString();
        } catch(Exception e)

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jn;
    }



    //detection
    private  class S3getimageurl extends AsyncTask<Void,Void,Void> {
        URL image1url, image2url,image3url;
        @Override
        protected Void doInBackground(Void... params) {
            String ACCESS_KEY ="AKIAJQTIZUYTX2OYTBJQ";
            String SECRET_KEY = "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";

            try {
                Random ramd = new Random();

                AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY));

                Log.e("outsidecond", String.valueOf(count_detect_no));

                if (autocapimgloc.get(auto_capt_count) != null) {
                    autocapimg1loc1 = autocapimgloc.get(auto_capt_count);
                    int innd = ramd.nextInt(100000);
                    File pic1 = new File(autocapimg1loc1);
                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg", pic1);
                    PutObjectResult putResponse = s3Client.putObject(pp);
                    String prurl = s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                    image1url = s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                    img1 = image1url.toString();
                    Log.e("AMAZON RESPONSE", img1);
                    auto_capt_count++;

                    replacedurl = img1.replace("https://valyouinputbucket.s3.amazonaws.com/", "https://valyouinputbucket.s3.ap-south-1.amazonaws.com/");
                } else {
                    img1 = "";
                    auto_capt_count++;
                }
                imgurllist.add(replacedurl);
                Log.e("detectis in s3", String.valueOf(count_detect_no));
                count_detect_no++;

                verify();
            }catch (Exception e){
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (verification_progress.isShowing() || verification_progress != null) {

                                verification_progress.dismiss();


                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                showretrys3urlDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            showretrys3urlDialogue();
                        }
                    }
                });
            }
            return  null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  new SExample(ppp.toString()).execute();
            //GameModel gameModel = new GameModel();
            //String params="U_Id="+candidateId+"&g_id="+12+"&image_url="+image1url+","+image2url+","+image3url+"&fr_status="+0+"&assesment_name="+ "FIB"+"&assesment_id="+2+"&company_name="+"TATACOMMUNICATION";



            // Log.e("params" , params);
            // new ImageSend().execute();

        }
    }

    private class SExample extends AsyncTask<Void,Void,Void> {
        String cid,url;


        SExample(String url){
            this.url=url;
        }

        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "userId="+candidateId+"&assessment_id="+aid+"&FRimage="+url);
            Request request = new Request.Builder()
                    .url(AppConstant.BASE_URL+"postfacialRecognition/"+ candidateId)
                    .put(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    String jj =response.body().toString();
                    try {
                        JSONObject jdsl = new JSONObject(jj);
                        String iddd= jdsl.getString("_id");
                        Log.e("lsbs",iddd);
                    }catch (Exception e){

                    }
                }

            }catch (IOException e){

            }
            return  null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


        }
    }

    @Override
    public void onCaptureDone(final String pictureUrl, final byte[] pictureData) {
        if (pictureData != null && pictureUrl != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    filepro = new File(Environment.getExternalStorageDirectory() + "/" + 1 + "_pic.jpg");
                    try  {
                        final OutputStream output = new FileOutputStream(filepro);
                        output.write(pictureData);
                    } catch (IOException e) {
                        Log.e("flbfpn", "Exception occurred while saving picture to external storage ", e);
                    }
                    final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
                    final int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    Matrix matrix = new Matrix();

                    matrix.postRotate(270);

                    final Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    Bitmap rotatedBitmap = Bitmap.createBitmap(scaled , 0, 0, scaled.getWidth(), scaled.getHeight(), matrix, true);

                    Matrix matri = new Matrix();

                    matrix.postRotate(180);
                    final Bitmap scale = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    Bitmap rotatedBitma = Bitmap.createBitmap(scaled , 0, 0, scaled.getWidth(), scaled.getHeight(), matrix, true);
                    filepro = new File(Environment.getExternalStorageDirectory()+"/"+11+"_pic.jpg");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    try  {
                        final OutputStream output = new FileOutputStream(filepro);
                        output.write(pictureData);
                    } catch (IOException e) {
                        Log.e("flbfpn", "Exception occurred while saving picture to external storage ", e);
                    }
                    if (pictureUrl.contains("1_pic.jpg")){
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
                        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
                        //  new DetectionTask(0).execute(inputStream);
                    }

                    // uploadFrontPhoto.setImageBitmap(scaled);

                }
            });

        }
    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {

    }


    private class GetQues extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qs_progress.show();
        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("CE_Id", ceid)
                            .object("Game", game)
                            .object("Category", cate)
                            .object("GD_Id", gdid)
                            .build().toString();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/v1/GetQuestions");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    Log.e("category",""+cate);
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code1 = urlConnection.getResponseCode();


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (code1 == 200) {

                    String responseString = readStream(urlConnection.getInputStream());
                    //         Log.e("Response", responseString);
                    result1 =responseString;

                    jsonMakeFile(cate+game+gdid+ceid+uidd+tgid,responseString);
                    try {
                        JSONArray data = new JSONArray(result1);
                        //        Log.e("QUESDATA", data.length() + "");
                        // JSONArray sidd = data.getJSONArray(0);
                        final JSONObject sidobj = data.getJSONObject(0);
                        sid = sidobj.getString("S_Id");
                        q_count=sidobj.getInt("question_count");
                        currentlevel=sidobj.getInt("currentGameLevel");
                        maxlevel=sidobj.getInt("max_level");
                        min_level=sidobj.getInt("min_level");
                        if(currentlevel<min_level){
                            currentlevel=min_level;
                        }
                        if(currentlevel>maxlevel){
                            currentlevel=min_level;
                        }
                        levelis=currentlevel;


                        JSONArray js_levelingarray=data.getJSONArray(1);

                        try {
                            for (int j = 0; j < maxlevel - min_level; j++) {
                                if (js_levelingarray.getJSONArray(j).length() == q_count) {
                                    islevelingenable = true;
                                } else {
                                    islevelingenable = false;
                                }

                            }
                        }catch (Exception e){
                            islevelingenable = false;
                            e.printStackTrace();
                        }

                        for(int j=0;j<js_levelingarray.length();j++) {
                            questions = js_levelingarray.getJSONArray(j);
                            for (int i = 0; i < questions.length(); i++) {
                                JSONObject js = questions.getJSONObject(i);
                                QuestionModel s = new QuestionModel();
                                s.setQT_id(js.getString("QT_Id"));
                                s.setQ_id(js.getString("Q_Id"));
                                s.setQuestion(js.getString("Q_Question"));
                                s.setOpt1(js.optString("Q_Option1"));
                                s.setOpt2(js.optString("Q_Option2"));
                                s.setOpt3(js.optString("Q_Option3"));
                                s.setOpt4(js.optString("Q_Option4"));
                                s.setOpt5(js.optString("Q_Option5"));
                                s.setCorrectanswer(js.getString("Q_Answer"));
                                s.setTime(js.getString("Q_MaxTime"));
                                s.setQ_level(js.getString("GL_Level"));
                                qs.add(s);
                            }
                        }

                        int_qs_size=qs.size();
                        if(q_count>qs.size()){
                            isques_limit_exceed=true;
                        }

                        //      Log.e("QUES", questions.length() + "");
                        tq = qs.size();

                    } catch (JSONException e) {

                        //   Log.e("QUES", "Error parsing data " + e.toString());
                    }
                }


            } catch(Exception e){
                e.getMessage();
                //   Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                qs_progress.dismiss();
            }catch (Exception e){

            }
            if(code1==200){
                dialoge = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
                dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialoge.setContentView(R.layout.game_popup);
                Window window = dialoge.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.CENTER;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                window.setAttributes(wlp);
                dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
                dialoge.setCancelable(false);

                try {

                    dialoge.show();
                }
                catch (Exception e){

                    e.printStackTrace();
                }
                startTime = startTime+1;
                startTime = startTime * 1000;

                timerrr.setText(String.valueOf(startTime / 1000));
                changeQues();
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    startPlanetAnimation();
                }
                 startVerification();
                //Face Recognization start here
                // checkfacedetection();
            }else{
                Intent it = new Intent(Activity_Fib.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }

        }
    }

    //detection
    private void checkfacedetection() {
        if(prefManager.getPic()==" "){
            String profileImageUrl = new DataBaseHelper(Activity_Fib.this).getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
            // new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);
            Log.e("facedetection","download pic");
        }else {
            faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(Activity_Fib.this).get_frkey());

            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Log.e("facedetection","alreday present");
            detectAndFrame(decodedByte);
        }
    }
    //detection
    public class ImageSend extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";
         //   String lg_id = " ", cert_id=" ", course_id = " ", tg_group_id=" ";

            String responseString="";

            /*try{
                lg_id = new PrefManager(Activity_Fib.this).getlgid();

                course_id = new PrefManager(Activity_Fib.this).getCourseid();

                cert_id =  new PrefManager(Activity_Fib.this).getcertiid();

              //  tg_group_id = new ToadysgameModel().getAss_groupid();

                Log.e("ids in imagesend", ""+lg_id+" ,"+course_id+" ,"+cert_id+" ,"+tg_groupid);


                if(lg_id.equals("")||lg_id.equals(null)){
                    lg_id = "0";
                }
                if(course_id.equals("")||course_id.equals(null)){
                    course_id = "0";
                }
                if(cert_id.equals("")||cert_id.equals(null)){
                    cert_id = "0";
                }
                if(tg_groupid.equals("")||tg_groupid.equals(null)){
                    tg_groupid = "0";
                }
                Log.e("ids in imagesend", ""+lg_id+" ,"+course_id+" ,"+cert_id+" ,"+tg_groupid);

            }

            catch (Exception e){
                e.printStackTrace();
            }*/
            try {

                Log.e("ids in imagesend", ""+lg_id+" ,"+course_id+" ,"+certifcation_id+" ,"+tg_groupid);



                JSONObject child = new JSONObject();

                if(get_nav.equalsIgnoreCase("learn"))
                {
                    child.put("U_Id", candidateId);
                    child.put("GD_Id", gdid);
                    child.put("fr_status", "0");
                    child.put("assesment_name", game);
                    child.put("CE_Id", ceid);
                    child.put("company_name", HomeActivity.tg_group);
                    child.put("TG_GroupId", tg_groupid);
                    child.put("lg_id", lg_id);
                    child.put("course_id", course_id);
                    child.put("certification_id",certifcation_id);
                }
                else {
                    child.put("U_Id", candidateId);
                    child.put("GD_Id", gdid);
                    child.put("fr_status", "0");
                    child.put("assesment_name", game);
                    child.put("CE_Id", ceid);
                    child.put("company_name", HomeActivity.tg_group);

                    child.put("TG_GroupId", tg_groupid);
                    child.put("lg_id", lg_id);
                    child.put("course_id", course_id);
                    child.put("certification_id",certifcation_id);

                }
                JSONArray jsonArray = new JSONArray();
                for(int i=0;i<imgurllist.size();i++){
                    jsonArray.put(imgurllist.get(i));
                }
                JSONObject mainobj=new JSONObject();
                child.put("image_url",jsonArray);
                mainobj.put("data",child);


                Log.e("datais",mainobj.toString());
                URL urlToRequest = new URL(AppConstant.Ip_url+"Player/UploadFrImages");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();


                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                urlConnection.setRequestMethod("POST");




                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(mainobj.toString());
                wr.flush();

                responseString = readStream1(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result=responseString;



            }
            catch (JSONException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            verification_progress.dismiss();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                Log.e("JSONException"," showretryimagesendDialogue");

                                showretryimagesendDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("JSONException"," showretryimagesendDialogue");

                            showretryimagesendDialogue();
                        }
                    }
                });
            }catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            verification_progress.dismiss();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                Log.e("Exception"," showretryimagesendDialogue");

                                showretryimagesendDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("Exception"," showretryimagesendDialogue");

                            showretryimagesendDialogue();
                        }
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            Log.e("datais","post");
            new PostAns().execute();
//            Intent it = new Intent(Activity_Fib.this, CheckP.class);
//            it.putExtra("qt", questions.length() + "");
//            it.putExtra("cq", cqa + "");
//            it.putExtra("po", score + "");
//            it.putExtra("assessg",assessg);
//            it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(it);
//            finish();



        }

        private String readStream1(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
    public String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qus = sb.toString();


            //Log.e("JSONStrr", Qus);

        } catch (Exception e) {
            e.getMessage();
            //  Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qus;
    }
    private void reset(View root) {
        if (root instanceof ViewGroup) {
            ViewGroup parent = (ViewGroup) root;
            for (int i = 0; i < parent.getChildCount(); i++) {
                reset(parent.getChildAt(i));
            }
        } else {
            root.setScaleX(1);
            root.setScaleY(1);
            root.setAlpha(1);
        }
    }
    public void changeQues() {


        if(islevelingenable){
            //leveling enable

          /*  try{
                blinking_anim.cancel();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }*/

            now = df1.format(c.getTime()).toString();
            flag_is_present_id=false;

            if (qc < q_count) {

                //below first four questions


                if(qc>3){
                    firstfourquestionflag=true;
                }

                if(qc<4){
                    int templevel=0;
                    for(int i=0;i<qs.size();i++){
                        QuestionModel qsmodel=qs.get(i);
                        if(qsmodel.getQ_level().equalsIgnoreCase(String.valueOf(levelis))){
                            templevel=i;
                            break;
                        }

                    }
                    qsmodel=qs.get(templevel);
                    indexis=templevel;

                }else {


                    //leveling questions after completing first four questions
                    if(qc==4) {

                        if (firstfourcorrectquestioncount > 3) {
                            levelis++;
                        } else {
                            levelis = levelis;
                        }
                        int templevel=0;
                        for(int i=0;i<qs.size();i++){
                            QuestionModel qsmodel=qs.get(i);
                            if(qsmodel.getQ_level().equalsIgnoreCase(String.valueOf(levelis))){
                                templevel=i;
                                break;
                            }

                        }
                        qsmodel=qs.get(templevel);
                        indexis=templevel;

                    }else {
                        if(twointgap==2){
                            twointgap=0;
                            if(correctquestioncount==2){
                                //next level
                                if(levelis<maxlevel){
                                    levelis++;
                                }

                            }
                            if(correctquestioncount==1){
                                //same level
                                levelis=levelis;
                            }
                            if(correctquestioncount==0){
                                //below level
                                if(levelis>min_level){
                                    levelis--;
                                }
                            }
                            correctquestioncount=0;
                        }
                    }
                    int templevel=0;
                    for(int i=0;i<qs.size();i++){
                        QuestionModel qsmodel=qs.get(i);
                        if(qsmodel.getQ_level().equalsIgnoreCase(String.valueOf(levelis))){
                            templevel=i;
                            break;
                        }

                    }
                    qsmodel=qs.get(templevel);
                    indexis=templevel;

                }


                startTime= Long.parseLong(qsmodel.getTime());
                startTime=startTime*1000;
                Log.e("timeris",qsmodel.getTime());
                countDownTimer = new MyCountDownTimer(startTime, interval);
                startTimer();
                for(int i=0;i<qid_array.size();i++){
                    if(qsmodel.getQ_id().equalsIgnoreCase(qid_array.get(i))){
                        flag_is_present_id=true;
                    }
                }


                if(!flag_is_present_id) {


//            if(qc==2){
//                takePicture();
//            }

                    final String now = df1.format(c.getTime()).toString();
                    timerrr.setTextColor(Color.parseColor("#ffffff"));
                    timer_img.setBackgroundResource(R.drawable.msq_timer_img);

                    try {
                        qno.setText(qc + 1 + "/" + q_count);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        //single question

                        newquid = qsmodel.getQ_id();
                        optioncard = new ArrayList<>();
                        int nois=qc+1;
                        quesDis = "Q"+nois+": "+qsmodel.getQuestion();
                        optioncard.add(quesDis);
                        if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                            String s1=qsmodel.getOpt1();
                            s1=s1.trim();
                            optioncard.add(s1);
                        }
                        if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                            String s2=qsmodel.getOpt2();
                            s2=s2.trim();
                            optioncard.add(s2);
                        }
                        if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                            String s3=qsmodel.getOpt3();
                            s3=s3.trim();
                            optioncard.add(s3);
                        }
                        if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                            String s4=qsmodel.getOpt4();
                            s4=s4.trim();
                            optioncard.add(s4);
                        }
                        if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                            String s5=qsmodel.getOpt5();
                            s5=s5.trim();
                            optioncard.add(s5);
                        }
                        String crcans = qsmodel.getCorrectanswer();

                        int crcopt = 0;
                        switch (crcans) {
                            case "A":
                                crcopt = 0;
                                break;
                            case "B":
                                crcopt = 1;
                                break;
                            case "C":
                                crcopt = 2;
                                break;
                            case "D":
                                crcopt = 3;
                                break;
                            case "E":
                                crcopt = 4;
                                break;
                            default:
                                System.out.println("Not in 10, 20 or 30");
                        }
                        crct = crcopt;
                        qid = Integer.parseInt(qsmodel.getQ_id());

                        //  quesDis = "Q"+nois+": "+qsmodel.getQuestion();

                        //   dqus.setText(quesDis);
                        dialoge.dismiss();


                        new CountDownTimer(200, 200) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {

                                fib_adapter=new Fib_adapter(Activity_Fib.this,optioncard);
                                LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(Activity_Fib.this, LinearLayoutManager.VERTICAL, false);
                                rc_view.setLayoutManager(linearLayoutManager1);
                                rc_view.setAdapter(fib_adapter);

//                            fastScroller.attachRecyclerView(rc_options_non_clickable);
//
//                            fastScroller.attachAdapter(optionAdapterNonClikable);
                                final Animation animation_fade = AnimationUtils.loadAnimation(Activity_Fib.this, R.anim.fade_in);
                                rc_view.startAnimation(animation_fade);
                                fib_adapter.notifyDataSetChanged();

                            }
                        }.start();


                    } catch (Exception e) {
                        Log.e("CHANGE QUES", "Error parsing data " + e.toString());
                    }


                }else {
               /* Collections.shuffle(qs);
                changeQues();*/
                }

               // qc++;
                if (firstfourquestionflag) {
                    twointgap++;
                }
                formdatalocal(qc,0);
                if (firstfourquestionflag) {
                    twointgap--;
                }


            } else {
//            rc_view.setVisibility(View.GONE);

                try{
                    dialoge.dismiss();
                }catch (Exception e){
                    e.printStackTrace();
                }

                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                } else {
                    tres = formdata();
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                }
            }

        }else {
            //leveling not enable

            timerrr.setTextColor(Color.parseColor("#ffffff"));
            timer_img.setBackgroundResource(R.drawable.msq_timer_img);

            now = df1.format(c.getTime()).toString();
            flag_is_present_id=false;


            int cis=0;
            if(isques_limit_exceed){
                cis=int_qs_size;
            }else {
                cis=q_count;
            }


            if (qc < cis) {

                qsmodel=qs.get(qc);

                startTime= Long.parseLong(qsmodel.getTime());
                startTime=startTime*1000;
                Log.e("timeris",qsmodel.getTime());
                countDownTimer = new MyCountDownTimer(startTime, interval);
                startTimer();
                for(int i=0;i<qid_array.size();i++){
                    if(qsmodel.getQ_id().equalsIgnoreCase(qid_array.get(i))){
                        flag_is_present_id=true;
                    }
                }


                if(!flag_is_present_id) {


//            if(qc==2){
//                takePicture();
//            }

                    final String now = df1.format(c.getTime()).toString();
                    try {
                        qno.setText(qc + 1 + "/" + cis);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        //single question

                        newquid = qsmodel.getQ_id();
                        optioncard = new ArrayList<>();
                        int nois=qc+1;
                        quesDis = "Q"+nois+": "+qsmodel.getQuestion();
                        optioncard.add(quesDis);
                        if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                            String s1=qsmodel.getOpt1();
                            s1=s1.trim();
                            optioncard.add(s1);
                        }
                        if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                            String s2=qsmodel.getOpt2();
                            s2=s2.trim();
                            optioncard.add(s2);
                        }
                        if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                            String s3=qsmodel.getOpt3();
                            s3=s3.trim();
                            optioncard.add(s3);
                        }
                        if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                            String s4=qsmodel.getOpt4();
                            s4=s4.trim();
                            optioncard.add(s4);
                        }
                        if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                            String s5=qsmodel.getOpt5();
                            s5=s5.trim();
                            optioncard.add(s5);
                        }
                        String crcans = qsmodel.getCorrectanswer();

                        int crcopt = 0;
                        switch (crcans) {
                            case "A":
                                crcopt = 0;
                                break;
                            case "B":
                                crcopt = 1;
                                break;
                            case "C":
                                crcopt = 2;
                                break;
                            case "D":
                                crcopt = 3;
                                break;
                            case "E":
                                crcopt = 4;
                                break;
                            default:
                                System.out.println("Not in 10, 20 or 30");
                        }
                        crct = crcopt;
                        qid = Integer.parseInt(qsmodel.getQ_id());

                        //  quesDis = "Q"+nois+": "+qsmodel.getQuestion();

                        //   dqus.setText(quesDis);
                        dialoge.dismiss();


                        new CountDownTimer(200, 200) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {

                                fib_adapter=new Fib_adapter(Activity_Fib.this,optioncard);
                                LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(Activity_Fib.this, LinearLayoutManager.VERTICAL, false);
                                rc_view.setLayoutManager(linearLayoutManager1);
                                rc_view.setAdapter(fib_adapter);

//                            fastScroller.attachRecyclerView(rc_options_non_clickable);
//
//                            fastScroller.attachAdapter(optionAdapterNonClikable);
                                final Animation animation_fade = AnimationUtils.loadAnimation(Activity_Fib.this, R.anim.fade_in);
                                rc_view.startAnimation(animation_fade);
                                fib_adapter.notifyDataSetChanged();

                            }
                        }.start();


                    } catch (Exception e) {
                        Log.e("CHANGE QUES", "Error parsing data " + e.toString());
                    }


                }else {
               /* Collections.shuffle(qs);
                changeQues();*/
                }








               // qc++;
                formdatalocal(qc,0);


            } else {
//            rc_view.setVisibility(View.GONE);

                try{
                    dialoge.dismiss();
                }catch (Exception e){
                    e.printStackTrace();
                }

                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                } else {
                    tres = formdata();
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                }
            }









        }

    }

    public String formdata(int anan){
        String jn="";


        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < anan; i++) {
                JSONObject student = new JSONObject();
                student.put("U_Id", uidd);
                student.put("S_Id", sid);
                student.put("Q_Id", qidj.get(i));
                student.put("QT_Id",qtid.get(i));
                student.put("TG_Id", tgid);
                student.put("SD_UserAnswer", ansj.get(i));
                student.put("SD_StartTime", stj.get(i));
                student.put("SD_EndTime", endj.get(i));
                jsonArray.put(student);
            }

            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn=studentsObj.toString();
            Log.e("jsonpostSTring",jn);
        }catch (Exception e){
            e.printStackTrace();
        }
        return jn;
    }

    //new changes
    public String formdatalocal(int qc,int val){
        String jn="";


        try {
            JSONArray js=new JSONArray();
            JSONObject student=null;
                for(int i=0;i<qc+1;i++) {
                     student = new JSONObject();
                    student.put("U_Id", uidd);
                    student.put("S_Id", sid);
                    student.put("TG_Id", tgid);

                    if(val==0){
                        if(i==qc) {
                            student.put("Q_Id", qsmodel.getQ_id());
                            student.put("QT_Id", qsmodel.getQ_id());
                            student.put("SD_UserAnswer", " ");
                            student.put("SD_StartTime", now);
                            student.put("SD_EndTime", now);
                        }else {
                            student.put("Q_Id", qidj.get(i));
                            student.put("QT_Id",qtid.get(i));
                            student.put("SD_UserAnswer", ansj.get(i));
                            student.put("SD_StartTime", stj.get(i));
                            student.put("SD_EndTime", endj.get(i));
                        }
                    }else {
                        student.put("Q_Id", qidj.get(i));
                        student.put("QT_Id",qtid.get(i));
                        student.put("SD_UserAnswer", ansj.get(i));
                        student.put("SD_StartTime", stj.get(i));
                        student.put("SD_EndTime", endj.get(i));
                    }

                    js.put(student);

                }

            jn=js.toString();
            Log.e("klocal_qc",""+qc);

            if(val==0){
                Log.e("klocal_formdata0",jn);

            }else {
                Log.e("klocal_formdata1",jn);

            }
            formdatanewstring=jn;
            jsonMakeFileAnswerandParams(cate+game+gdid+ceid+uidd+tgid,jn);

        }catch (Exception e){
            e.printStackTrace();
        }
        return jn;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub

        if(event.getPointerCount() > 1) {
            //System.out.println("Multitouch detected!");
            event.setAction(MotionEvent.ACTION_POINTER_DOWN);
            return false;
        }
        else
            return super.onTouchEvent(event);
    }


    public class MyCountDownTimer extends CountDownTimer {


        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }


        @Override

        public void onFinish() {

         //   blinking_anim.cancel();
            if(islevelingenable) {
                //leveling enable

                if (qc < q_count) {
                    performAction(100);


                } else {
                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();
                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();
                    }

                }

            }else {

                int cis=0;
                if(isques_limit_exceed){
                    cis=int_qs_size;
                }else {
                    cis=q_count;
                }
                //leveling not enable
                if (qc < cis) {
                    performAction(100);

                }else {
                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();
                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();
                    }

                }
            }



        }


        @Override

        public void onTick(long millisUntilFinished) {

            timerrr.setText("" + millisUntilFinished / 1000);
            secLeft = millisUntilFinished / 1000;

            if(secLeft<=5){
                timerrr.setTextColor(Color.parseColor("#ff0000"));
                timer_img.setBackgroundResource(R.drawable.msq_red_timer);

//                timerrr.startAnimation(blinking_anim);
//                timer_img.startAnimation(blinking_anim);
            }

            if( secLeft==1){
        //        blinking_anim.cancel();
            }


        }

    }

    void startTimer() {
        countDownTimer.start();
    }


    @Override
    protected void onPause() {
        super.onPause();
       /* ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);
*/


      //  camera.stopPreview();
        if (checkonp) {
            try {
                if(alertDialog!=null&&alertDialog.isShowing()){
                  //  alertDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (dialoge != null && dialoge.isShowing()) {
                 //   dialoge.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

           // close.setEnabled(false);
            // vdo_performance_ques_timer.cancel();
            try{
                if(!alertDialog.isShowing())
                {


                    alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.assess_quit_warning);
                    Window window = alertDialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alertDialog.setCancelable(false);

                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);


                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();



                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);


                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);
                                    alertDialog.dismiss();
                                  /*  if (qc < qs.size() - 1) {
                                        Collections.shuffle(qs);
                                        changeQues();
                                    }*/
                                   /* countDownTimer = new MyCountDownTimer(secLeft, interval);
                                    countDownTimer.start();        */                }
                            }.start();



                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);


                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);
                                    alertDialog.dismiss();
                                    try {
                                        countDownTimer.cancel();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    if (qansr > 0) {
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();
                                    } else {
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();
                                    }
                                }
                            }.start();




                        }
                    });

                    try {
                      //  alertDialog.show();
                    }catch (Exception e){}
                }

            }catch (Exception e){


                alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialog.setContentView(R.layout.assess_quit_warning);
                Window window = alertDialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.CENTER;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                alertDialog.setCancelable(false);

                alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                msg.setText(quit_msg);
                final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
                final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                    astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                    astro_outx.setDuration(300);
                    astro_outx.setInterpolator(new LinearInterpolator());
                    astro_outx.setStartDelay(0);
                    astro_outx.start();
                    astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                    astro_outy.setDuration(300);
                    astro_outy.setInterpolator(new LinearInterpolator());
                    astro_outy.setStartDelay(0);
                    astro_outy.start();



                    alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                    alien_outx.setDuration(300);
                    alien_outx.setInterpolator(new LinearInterpolator());
                    alien_outx.setStartDelay(0);
                    alien_outx.start();
                    alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                    alien_outy.setDuration(300);
                    alien_outy.setInterpolator(new LinearInterpolator());
                    alien_outy.setStartDelay(0);
                    alien_outy.start();

                }
                final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        no.setEnabled(false);
                        yes.setEnabled(false);


                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                            astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                            astro_inx.setDuration(300);
                            astro_inx.setInterpolator(new LinearInterpolator());
                            astro_inx.setStartDelay(0);
                            astro_inx.start();
                            astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                            astro_iny.setDuration(300);
                            astro_iny.setInterpolator(new LinearInterpolator());
                            astro_iny.setStartDelay(0);
                            astro_iny.start();

                            alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                            alien_inx.setDuration(300);
                            alien_inx.setInterpolator(new LinearInterpolator());
                            alien_inx.setStartDelay(0);
                            alien_inx.start();
                            alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                            alien_iny.setDuration(300);
                            alien_iny.setInterpolator(new LinearInterpolator());
                            alien_iny.setStartDelay(0);
                            alien_iny.start();
                        }
                        new CountDownTimer(400,400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                close.setEnabled(true);
                                alertDialog.dismiss();
                              /*  if (qc < qs.size() - 1) {
                                    Collections.shuffle(qs);
                                    changeQues();
                                }*/
                               /* countDownTimer = new MyCountDownTimer(secLeft, interval);
                                countDownTimer.start();*/                        }
                        }.start();



                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        yes.setEnabled(false);
                        no.setEnabled(false);


                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                            astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                            astro_inx.setDuration(300);
                            astro_inx.setInterpolator(new LinearInterpolator());
                            astro_inx.setStartDelay(0);
                            astro_inx.start();
                            astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                            astro_iny.setDuration(300);
                            astro_iny.setInterpolator(new LinearInterpolator());
                            astro_iny.setStartDelay(0);
                            astro_iny.start();

                            alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                            alien_inx.setDuration(300);
                            alien_inx.setInterpolator(new LinearInterpolator());
                            alien_inx.setStartDelay(0);
                            alien_inx.start();
                            alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                            alien_iny.setDuration(300);
                            alien_iny.setInterpolator(new LinearInterpolator());
                            alien_iny.setStartDelay(0);
                            alien_iny.start();
                        }
                        new CountDownTimer(400,400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                close.setEnabled(true);
                                alertDialog.dismiss();
                                try {
                                    countDownTimer.cancel();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                if (qansr > 0) {
                                    tres = formdata(qansr);
                                    checkonp = false;
                                    scoreActivityandPostscore();
                                } else {
                                    tres = formdata();
                                    checkonp = false;
                                    scoreActivityandPostscore();
                                }
                            }
                        }.start();




                    }
                });
                try {
                //    alertDialog.show();
                }catch (Exception ex){}
            }


        }
    }

    //detection
    private void scoreActivityandPostscore() {
          stop();
        if(connectionUtils.isConnectionAvailable()) {



            if (piccapturecount != 0) {
                try {
                    if (!verification_progress.isShowing() || verification_progress != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                verification_progress.show();
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                layoutbg.setVisibility(View.GONE);
                verify();
            } else {
                try {
                    if (!verification_progress.isShowing() || verification_progress != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                verification_progress.show();
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                layoutbg.setVisibility(View.GONE);
                // new ImageSend().execute();
                new PostAns().execute();

            }
//            if(i+1==piccapturecount) {
//                new ImageSend().execute();
//            }



        }else {
            showretryDialogue();
        }


    }


    public void showretryDialogue() {
        Log.e("respmsg","dialogue");

        alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        if(connectionUtils.isConnectionAvailable()){
                            scoreActivityandPostscore();
                        }else {
                            try {
                                if (!qs_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretrydetectDialogue() {
        Log.e("showretrydetectDialogue","showretrydetectDialogue");

        alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                            verification_progress.show();


                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            detectAndFrameother1(autotakenpic.get(count_detect_no));
                        }else {
                            try {
                                if (!qs_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretrydetectDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretryverifyDialogue() {
        Log.e("showretryverifyDialogue","showretryverifyDialogue");

        alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                            verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new VerificationTask(mfaceid1, detectedfaceid.get(image_face_count)).execute();
                        }else {
                            try {
                                if (!qs_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryverifyDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretrys3urlDialogue() {
        Log.e("showretrys3urlDialogue","showretrys3urlDialogue");

        alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new S3getimageurl().execute();
                        }else {
                            try {
                                if (!qs_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretrys3urlDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretryimagesendDialogue() {
        Log.e("showretryimgsndDialogue","showretryimagesendDialogue");

        alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new ImageSend().execute();
                        }else {
                            try {
                                if (!qs_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryimagesendDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }


    private class PostAns extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {

                Log.e("datais","ip: "+AppConstant.getLocalIpAddressApp()+" os: "+AppConstant.getAndroidVersionApp()
                        +" version: "+AppConstant.getversionofApp());
                try {



                    getPostData();
                    try {
                        Thread.currentThread();
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/v1/Answers");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            postvalues.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");

                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestProperty("app_version",AppConstant.getversionofApp());
                    urlConnection.setRequestProperty("app_os",AppConstant.getAndroidVersionApp());
                    urlConnection.setRequestProperty("app_ip",AppConstant.getLocalIpAddressApp());
                    urlConnection.setRequestProperty("app_latlong",location.getLatitude()+","+location.getLongitude());

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(postvalues);
                    Log.e("resp_data",postvalues);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code2 = urlConnection.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.e("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {

                //needc


                new Ends().execute();
            }
            else{

                try{
                    verification_progress.dismiss();
                    flag_ques_done = true;

                    disable_option();
                }catch (Exception e){
                    e.printStackTrace();
                }

                Intent it = new Intent(Activity_Fib.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        }
    }

    private class Ends extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            if(new PrefManager(Activity_Fib.this).getGetFirsttimegame().equalsIgnoreCase("first")){
                new PrefManager(Activity_Fib.this).saveFirsttimeGame("last");
            }else if(new PrefManager(Activity_Fib.this).getGetFirsttimegame().equalsIgnoreCase("last")){

            }else {
                new PrefManager(Activity_Fib.this).saveFirsttimeGame("first");
            }


        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {


                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("S_Id", sid)
                            .object("course_id", Integer.valueOf(new PrefManager(Activity_Fib.this).getcourseid_ass()))
                            .object("lg_id",new PrefManager(Activity_Fib.this).getlgid_ass())
                            .object("certification_id",new PrefManager(Activity_Fib.this).getcertiid_ass())
                            .build().toString();


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/EndSession");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " +token);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                    Log.v("Response2", ""+code3);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if(code3==200){
                    String responseString = readStream1(is);
                    Log.e("Response2", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                verification_progress.dismiss();
                flag_ques_done = true;
                disable_option();

                Log.e("result", ""+result);

            }catch (Exception e){
                e.printStackTrace();
            }

            if(code3!=200){

                Toast.makeText(getApplicationContext(),"Error while submitting answers", Toast.LENGTH_SHORT).show();
                Intent it = new Intent(Activity_Fib.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
            else{
                try {
                    get_quesdata = new File(getFilesDir() + "" + cate + game + gdid + ceid +uidd + tgid + ".json");
                    get_ansdata = new File(getFilesDir() + "Answer" + cate + game + gdid + ceid + uidd + tgid +".json");
                    get_quesdata.delete();
                    get_ansdata.delete();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                Intent it = new Intent(Activity_Fib.this, CheckP.class);
                int questionsc=0;
                if(isques_limit_exceed){
                    questionsc=int_qs_size;
                }else {
                    questionsc=q_count;
                }

                it.putExtra("qt", questionsc+ "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.putExtra("assessg",assessg);
                // it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();

            }

        }
    }

    private String readStream1(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qs = sb.toString();


            Log.e("JSONStrr", Qs);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qs;
    }

    @Override
    public void onBackPressed() {
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mReceiver);

        try{
            timer1.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            if(progressDialog.isShowing() && progressDialog!=null){
                progressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            if(dialoge.isShowing()){
                dialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    //profile image detection
    private void detectAndFrame(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            //    Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","No face detected");

                        }else {
                            //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","face detected");
                            //    startVerification();
                        }


                        // imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                        //   Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                        //imageBitmap.recycle();


                        List<Face> faces;

                        assert result != null;
                        faces = Arrays.asList(result);
                        for (Face face: faces) {
                            mFaceId1 = face.faceId;
                            faceid=mFaceId1.toString();
                            //txt.setText(faceid);
                            Log.e("fid",mFaceId1+"  "+mface);

                        }

                    }
                };
        detectTask.execute(inputStream);
    }

    //detection
    private void startVerification() {
        camera = getCameraInstance();

        waitTimer = new CountDownTimer(60000, 3) {


            @SuppressLint("SimpleDateFormat")
            @Override
            public void onTick(long l) {


            }
            public void onFinish() {

                stop();

            }



        }.start();

    }

    //Download profile image
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];


            try {
                // Download Image from URL
                InputStream input = new URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            result.compress(Bitmap.CompressFormat.PNG, 0, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            Log.e("ENCID",encodedImage);
            prefManager.savePic(encodedImage);
            Log.e("EN",prefManager.getPic());
            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            detectAndFrame(decodedByte);
            // dialog.cancel();
        }
    }

    //detection
    private Camera getCameraInstance() {
        Camera camera = null;
        if (!getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //   Toast.makeText(this, "No camera on this device", Toast.LENGTH_LONG)
            //    .show();
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId < 0) {
                //  Toast.makeText(this, "No front facing camera found.",
                //       Toast.LENGTH_LONG).show();
            } else {

                try {
                    camera = Camera.open(cameraId);
                    mPreview = new CameraPreview(this, camera);
                    preview.addView(mPreview);
                    onClick();

                } catch (Exception e) {
                    // cannot get camera or does not exist
                }
            }
        }

        return camera;
    }

    //detection
    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d("status", "Camera found..");
                Log.d("camera id", "Camera id"+i);
                cameraId = i;
                break;
            }else {
                Log.e("Status","No front camera found..");
                Log.e("camera id", "Camera id"+i);
            }
        }
        return cameraId;
    }
    //detection
    public void onClick() {


        timer1 = new Timer();
        timer1.schedule(new TimerTask()
        {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {

                        if(picture_count<pic_frequency) {

                            try {
                                camera.setDisplayOrientation(90);
                                camera.startPreview();


                                camera.takePicture(null, null, new PhotoHandler(getApplicationContext()));
                                picture_count++;

                                Log.e("pic count", "" + picture_count);

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        else{
                            stop();
                        }


                    }
                });
            }
        }, 3000, 9000);


    }
    //detection
    public void stop(){
        preview.setVisibility(View.GONE);

        //   waitTimer.cancel();


        try {
            timer1.cancel();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    //detection
    public static Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    //detection
    private void detectAndFrameother1(Bitmap imageBitmap) {


        /*if(imageBitmap.getWidth()>imageBitmap.getHeight()){
            imageBitmap=rotateBitmap(imageBitmap,-90);
            Log.e("rotated","yes");
        }*/
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        try {
                            // if (result == null)
                            //   Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                            //if face is not detected in image
                            if ((result != null && result.length == 0)) {                                //    Toast.makeText(Activity_Fib.this,"other face is not detected",Toast.LENGTH_SHORT).show();
                                // detectedfaceid.add(UUID.fromString(""));
                                facedetectedimagelocation.add(count_detect_no, "");
                                Log.e("outside ", String.valueOf(count_detect_no) + " nondetect");

                                count_detect_no++;
                                verify();
                            }

                            //if face is detected in image
                            else {
                                //   Toast.makeText(Activity_Fib.this, "other face is  detected", Toast.LENGTH_SHORT).show();

                                Log.e("outside ", String.valueOf(count_detect_no) + " detect");
                                //imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                                //  Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                                // imageBitmap.recycle();


                                //imagelocation.add(mImageFileLocation);

                                detectioncount++;
                                //    Log.e("imglocation", mImageFileLocation);
//                            switch (piccapturecount){
//
//                                case 1:
//                                    verifiedimagelocation.add(imagelocation.get(0));
//                                    break;
//                                case 2:
//                                    verifiedimagelocation.add(imagelocation.get(1));
//                                    break;
//                                case 3:
//                                    verifiedimagelocation.add(imagelocation.get(2));
//                                    break;
//
//                            }

                                //adding detected image location in an array to get its url from s3
                                facedetectedimagelocation.add(imagelocation.get(count_detect_no));
                                Log.e("VALUE of i", "" + i);
                                List<Face> faces;

                                UUID mFaceIdother;
                                assert result != null;
                                faces = Arrays.asList(result);
                                for (Face face : faces) {
                                    mFaceIdother = face.faceId;
                                    String faceid = mFaceIdother.toString();
                                    // txt.setText(faceid);


                                    //adding detected face uuid in an array

                                    detectedfaceid.add(image_face_count, mFaceIdother);


                                    UUID[] uuids = new UUID[]{mFaceIdother};

                                    //    Log.e("mfaceid", String.valueOf(mFaceIdother));

                                    mFaceIdother = UUID.fromString(faceid);
                                    //      new VerificationTask(mFaceId1, mFaceIdother).execute();
                                    //  Log.e("face id size", String.valueOf(detectedfaceid.size()));
//                                if(detectedfaceid.size()==3){
//

                                    //


                                    //String mfaceid = prefManager.getprofilefaceid();

                                    Log.e("detectedfaceid", "" + detectedfaceid.get(image_face_count));

                                 // UUID mfaceid1 = UUID.fromString(mfaceid);

                                    new VerificationTask(mfaceid1, detectedfaceid.get(image_face_count)).execute();

//

//
//                                }


                                }


                            }

                            // i++;
//                        if(i<piccapturecount){
//                            detectAndFrameother1(autotakenpic.get(i));
//
//
//                        }
//                        else{
//                            new ImageSend().execute();
//                        }
                        }catch (Exception e){
                            ConnectionUtils connectionUtils = new ConnectionUtils(Activity_Fib.this);
                            if(connectionUtils.isConnectionAvailable()){
                                if(result==null){
                                    facedetectedimagelocation.add(count_detect_no, "");
                                    Log.e("outside", String.valueOf(count_detect_no) + "nondetect");
                                    count_detect_no++;
                                    verify();
                                }
                                else{

                                }
                            }
                            else{
                                showretrydetectDialogue();
                            }

                            e.printStackTrace();
                            try {
                                if (verification_progress.isShowing() || verification_progress != null) {

                                            verification_progress.dismiss();

                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                           // showretrydetectDialogue();
                        }
                    }

                };

        detectTask.execute(inputStream);
    }
    private void detectAndFrameother2(Bitmap imageBitmap) {


        if(imageBitmap.getWidth()>imageBitmap.getHeight()){
            imageBitmap=rotateBitmap(imageBitmap,-90);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        //   Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            //            Toast.makeText(Activity_Fib.this,"other face is not detected",Toast.LENGTH_SHORT).show();

                            // new S3getimageurl().execute();
                        }else {
                            //          Toast.makeText(Activity_Fib.this, "other face is  detected", Toast.LENGTH_SHORT).show();


                            //imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                            //  Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                            // imageBitmap.recycle();


                            //imagelocation.add(mImageFileLocation);

                            detectioncount++;
                            //    Log.e("imglocation", mImageFileLocation);


                            List<Face> faces;


                            assert result != null;
                            faces = Arrays.asList(result);
                            for (Face face : faces) {
                                UUID mFaceIdother = face.faceId;
                                String faceid = mFaceIdother.toString();
                                // txt.setText(faceid);

                                detectedfaceid.add(mFaceIdother);
                                UUID[] uuids = new UUID[]{mFaceIdother};

                                //    Log.e("mfaceid", String.valueOf(mFaceIdother));

                                mFaceIdother = UUID.fromString(faceid);
                                //      new VerificationTask(mFaceId1, mFaceIdother).execute();
                                //  Log.e("face id size", String.valueOf(detectedfaceid.size()));
//                                if(detectedfaceid.size()==3){
//
//
//
//                                }
                                new VerificationTask2(mFaceId1, mFaceIdother);
                            }
                        }

                    }
                };
        detectTask.execute(inputStream);
    }
    private void detectAndFrameother3(Bitmap imageBitmap) {


        if(imageBitmap.getWidth()>imageBitmap.getHeight()){
            imageBitmap=rotateBitmap(imageBitmap,-90);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        //   Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            //          Toast.makeText(Activity_Fib.this,"other face is not detected",Toast.LENGTH_SHORT).show();

                            // new S3getimageurl().execute();
                        }else {
                            //        Toast.makeText(Activity_Fib.this, "other face is  detected", Toast.LENGTH_SHORT).show();


                            //imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                            //  Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                            // imageBitmap.recycle();


                            //imagelocation.add(mImageFileLocation);

                            detectioncount++;
                            //    Log.e("imglocation", mImageFileLocation);


                            List<Face> faces;


                            assert result != null;
                            faces = Arrays.asList(result);
                            for (Face face : faces) {
                                UUID mFaceIdother = face.faceId;
                                String faceid = mFaceIdother.toString();
                                // txt.setText(faceid);

                                detectedfaceid.add(mFaceIdother);
                                UUID[] uuids = new UUID[]{mFaceIdother};

                                //    Log.e("mfaceid", String.valueOf(mFaceIdother));

                                mFaceIdother = UUID.fromString(faceid);
                                //      new VerificationTask(mFaceId1, mFaceIdother).execute();
                                //  Log.e("face id size", String.valueOf(detectedfaceid.size()));
//                                if(detectedfaceid.size()==3){
//
//
//
//                                }
                                new VerificationTask3(mFaceId1, mFaceIdother);
                            }
                        }

                    }
                };
        detectTask.execute(inputStream);
    }
    //detection
    private void verify() {

        Log.e("detectis count",String.valueOf(count_detect_no));
        // detecting the captured images
        if(count_detect_no<piccapturecount) {
            detectAndFrameother1(autotakenpic.get(count_detect_no));
        }

        if(count_detect_no==piccapturecount){
            //senddata to server
            for(int i=0;i<imgurllist.size();i++){
                Log.e("outsideurl",i+" "+imgurllist.get(i));
            }
            try{
                //   verification_progress.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
            // new PostAns().execute();

            if(imgurllist.size()==0){


                new PostAns().execute();
//                Intent it = new Intent(Activity_Fib.this, CheckP.class);
//                it.putExtra("qt", questions.length() + "");
//                it.putExtra("cq", cqa + "");
//                it.putExtra("po", score + "");
//                it.putExtra("assessg",assessg);
//                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(it);
//                finish();

            }else {
                new ImageSend().execute();
            }

        }

        // while(i<piccapturecount){

        //  detectAndFrameother1(autotakenpic.get(i));
        //}

//        while (i<piccapturecount){
//            detectAndFrameother1(autotakenpic.get(i));
//            i++;
//
//        }

//        if(i<piccapturecount){
//            detectAndFrameother1(autotakenpic.get(i));
//        }
//        else {
//            new ImageSend().execute();
//        }
//        for(i=0;i<=2;i++){
//
//
//
//            UUID faceid = detectedfaceid.get(i);
//
//            Log.e("faceid", faceid.toString());

//        if(detectioncount==1){
//            new VerificationTask(mFaceId1, detectedfaceid.get(0)).execute();}
//        else if(detectioncount<=2){
//            new VerificationTask(mFaceId1, detectedfaceid.get(0)).execute();
//            new VerificationTask2(mFaceId1, detectedfaceid.get(1)).execute();
//        }
//        else if(detectioncount<=3){
//            new VerificationTask(mFaceId1, detectedfaceid.get(0)).execute();
//            new VerificationTask2(mFaceId1, detectedfaceid.get(1)).execute();
//            new VerificationTask3(mFaceId1, detectedfaceid.get(2)).execute();
//        }



//
//        while(i<piccapturecount){
//
//            detectAndFrameother1(autotakenpic.get(i));
//            i++;
//        }




//        switch (piccapturecount){
//
//            case 1:
//                detectAndFrameother1(autotakenpic.get(0));
//                        break;
//            case 2:
//                detectAndFrameother1(autotakenpic.get(1));
//                break;
//            case 3:
//                detectAndFrameother1(autotakenpic.get(2));
//                break;
//
//        }


//
//        if(piccapturecount==1){
//            detectAndFrameother1(autotakenpic.get(0));
//        }
//        else if(piccapturecount==2){
//            detectAndFrameother1(autotakenpic.get(0));
//            detectAndFrameother2(autotakenpic.get(1));
//
//
//        }
//
//        else if(piccapturecount==3){
//            detectAndFrameother1(autotakenpic.get(0));
//            detectAndFrameother2(autotakenpic.get(1));
//            detectAndFrameother3(autotakenpic.get(2));
//        }



    }
    //detection
    public class PhotoHandler implements Camera.PictureCallback {


        private final Context context;

        public PhotoHandler(Context context) {
            this.context = context;
        }

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            piccapturecount++;
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException e) {
                e.printStackTrace();
            }

            String authorities = getApplicationContext().getPackageName() + ".fileprovider";



            try {
                assert photoFile != null;
                FileOutputStream fos = new FileOutputStream(photoFile);
                fos.write(data);
                fos.flush();
                fos.close();
                fos.close();
                //   Toast.makeText(context, "New Image saved:" + photoFile,
                //       Toast.LENGTH_LONG).show();
            } catch (Exception error) {

                //   Toast.makeText(context, "Image could not be saved.",
                //         Toast.LENGTH_LONG).show();
            }

            //String filepath=  pictureFile.getPath();

            bmpother = BitmapFactory.decodeFile(mImageFileLocation);
            imageUri= FileProvider.getUriForFile(getApplicationContext(), authorities, photoFile);


          //  int rotateImage = getCameraPhotoOrientation(Activity_Fib.this, imageUri, mImageFileLocation);


          //  Log.e("rotateImage",""+i);
            // imageView.setImageBitmap(bitmap);
            //   Bitmap b = decodeFile(pictureFile);
            // Uri mImageUri = Uri.fromFile(pictureFile);

           /* ExifInterface exif = null;
            try {
                exif = new ExifInterface(photoFile.getPath());

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = -90;
            }
            else if (orientation == ExifInterface.ORIENTATION_NORMAL) {
                angle = 0;
            }
            Matrix mat = new Matrix();
            mat.setRotate(angle);
                Log.e("rotateImage",""+angle);
            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(photoFile), null, null);
                bmpother = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);


            } catch (IOException e) {
                e.printStackTrace();
            }*/
            Matrix mMatrix = new Matrix();

            mMatrix.setRotate(-90);
            bmpother = Bitmap.createBitmap(bmpother, 0, 0, bmpother.getWidth(),
                    bmpother.getHeight(), mMatrix, true);




       //     Log.e("rotated","yes");
            // saving location of auto capture image in an array
            imagelocation.add(mImageFileLocation);

            //saving auto capture bitmap image in an array
            autotakenpic.add(bmpother);


//            Toast.makeText(context, "Image saved."+imagelocation.size(),
//                            Toast.LENGTH_LONG).show();

            // detectAndFrameother(bmpother);



        }

        public  File createImageFile() throws IOException {

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "IMAGE_" + timeStamp + "_";
            File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
            mImageFileLocation = image.getAbsolutePath();

            return image;

        }

    }
    //detection
    private class VerificationTask extends AsyncTask<Void, String, VerifyResult> {
        // The IDs of two face to verify.
        private UUID mFaceId0;
        private UUID mFaceId1;

        VerificationTask (UUID faceId0, UUID faceId1) {
            mFaceId0 = faceId0;
            mFaceId1 = faceId1;
        }

        @Override
        protected VerifyResult doInBackground(Void... params) {
            // Get an instance of face service client to detect faces in image.
            try{
                publishProgress("Verifying...");

                // Start verification.
                return faceServiceClient.verify(
                        mFaceId0,      /* The first face ID to verify */
                        mFaceId1);     /* The second face ID to verify */
            }  catch (Exception e) {
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(VerifyResult result) {

            image_face_count++;
            Log.e("image_face_count",""+image_face_count);
            // Show the result on screen when verification is done.
            setUiAfterVerification(result);
        }
    }
    //detection
    private void setUiAfterVerification(VerifyResult result) {
        // Verification is done, hide the progress dialog.

        // Enable all the buttons.

        // Show verification result.

        try {
            if (result != null) {
                //   Log.e("facedetection","Result");
                DecimalFormat formatter = new DecimalFormat("0.0%");
                String verificationResult = (result.isIdentical ? "The same person" : "Different persons")
                        + ". The confidence is " + formatter.format(result.confidence);
                //  Log.e("facedetection",verificationResult);


                if (verificationResult.contains("Different")) {

                    //    if(i==1)

                    // adding mismatch image location in an array
                    autocapimgloc.add(auto_capt_count, facedetectedimagelocation.get(count_detect_no));

                    Log.e("added", facedetectedimagelocation.get(count_detect_no));

                    //getting image url from s3
                    new S3getimageurl().execute();


                    Log.e("not outside s3", String.valueOf(count_detect_no));

                } else {
                    Log.e("outside s3", String.valueOf(count_detect_no));
                    count_detect_no++;

                    verify();

                }

            }
        }
        catch (Exception e){


            e.printStackTrace();

            try {
                if (verification_progress.isShowing() || verification_progress != null) {

                            verification_progress.dismiss();


                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
            showretryverifyDialogue();

        }
    }



    private class VerificationTask2 extends AsyncTask<Void, String, VerifyResult> {
        // The IDs of two face to verify.
        private UUID mFaceId0;
        private UUID mFaceId1;

        VerificationTask2 (UUID faceId0, UUID faceId1) {
            mFaceId0 = faceId0;
            mFaceId1 = faceId1;
        }

        @Override
        protected VerifyResult doInBackground(Void... params) {
            // Get an instance of face service client to detect faces in image.
            try{
                publishProgress("Verifying...");

                // Start verification.
                return faceServiceClient.verify(
                        mFaceId0,      /* The first face ID to verify */
                        mFaceId1);     /* The second face ID to verify */
            }  catch (Exception e) {
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(VerifyResult result) {


            // Show the result on screen when verification is done.
            setUiAfterVerification2(result);
        }


    }
    private void setUiAfterVerification2(VerifyResult result) {
        // Verification is done, hide the progress dialog.

        // Enable all the buttons.

        // Show verification result.
        if (result != null) {
            //   Log.e("facedetection","Result");
            DecimalFormat formatter = new DecimalFormat("0.0%");
            String verificationResult = (result.isIdentical ? "The same person": "Different persons")
                    + ". The confidence is " + formatter.format(result.confidence);
            //  Log.e("facedetection",verificationResult);


            if (verificationResult.contains("Different")){


                autocapimg1loc2 = imagelocation.get(1);
                Log.e("image2", autocapimg1loc2);





            }

            // Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
            // img.setImageBitmap(bitmap);
        }
    }
    private class VerificationTask3 extends AsyncTask<Void, String, VerifyResult> {
        // The IDs of two face to verify.
        private UUID mFaceId0;
        private UUID mFaceId1;

        VerificationTask3 (UUID faceId0, UUID faceId1) {
            mFaceId0 = faceId0;
            mFaceId1 = faceId1;
        }

        @Override
        protected VerifyResult doInBackground(Void... params) {
            // Get an instance of face service client to detect faces in image.
            try{
                publishProgress("Verifying...");

                // Start verification.
                return faceServiceClient.verify(
                        mFaceId0,      /* The first face ID to verify */
                        mFaceId1);     /* The second face ID to verify */
            }  catch (Exception e) {
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(VerifyResult result) {


            // Show the result on screen when verification is done.
            setUiAfterVerification3(result);
        }


    }
    private void setUiAfterVerification3(VerifyResult result) {
        // Verification is done, hide the progress dialog.

        // Enable all the buttons.

        // Show verification result.
        if (result != null) {
            //   Log.e("facedetection","Result");
            DecimalFormat formatter = new DecimalFormat("0.0%");
            String verificationResult = (result.isIdentical ? "The same person": "Different persons")
                    + ". The confidence is " + formatter.format(result.confidence);
            //  Log.e("facedetection",verificationResult);


            if (verificationResult.contains("Different")){

                autocapimg1loc3 = imagelocation.get(2);
                Log.e("image3", autocapimg1loc3);




            }

            // Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
            // img.setImageBitmap(bitmap);
        }
    }
    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)Activity_Fib.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }
    public void gameClose(final View close){
        if (pauseCount == 0) {

            close.setEnabled(false);


//                    vdo_performance_ques_timer.cancel();
//                    dialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialog.setContentView(R.layout.pause_popup);
//                    Window window = dialog.getWindow();
//                    WindowManager.LayoutParams wlp = window.getAttributes();
//
//                    wlp.gravity = Gravity.CENTER;
//                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//                    window.setAttributes(wlp);
//                    dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
//                    dialog.setCancelable(false);
//                    try {
//                        dialog.show();
//                    }
//                    catch (Exception e){
//                        e.printStackTrace();
//                    }
//                    Button pu =(Button)dialog.findViewById(R.id.resume);
//                    pu.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            if(qc<qs.size()-1) {
//
//                                Collections.shuffle(qs);
//                                changeQues();
//                            }
//                            dialog.dismiss();
//                            vdo_performance_ques_timer = new MyCountDownTimer(secLeft, interval);
//                            vdo_performance_ques_timer.start();
//                            pauseCount++;
//                        }
//                    });
            //  vdo_performance_ques_timer.cancel();
          /*  AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Activity_Fib.this, R.style.DialogTheme);
// ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = LayoutInflater.from(Activity_Fib.this);
            View dialogView = inflater.inflate(R.layout.quit_warning, null);
            dialogBuilder.setView(dialogView);



            alertDialog = dialogBuilder.create();

            alertDialog.setCancelable(false);
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

            alertDialog.show();
*/ alertDialog = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.assess_quit_warning);
            Window window = alertDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
            alertDialog.setCancelable(false);

            alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
            msg.setText(quit_msg);
            final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
            final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                astro_outx.setDuration(300);
                astro_outx.setInterpolator(new LinearInterpolator());
                astro_outx.setStartDelay(0);
                astro_outx.start();
                astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                astro_outy.setDuration(300);
                astro_outy.setInterpolator(new LinearInterpolator());
                astro_outy.setStartDelay(0);
                astro_outy.start();



                alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                alien_outx.setDuration(300);
                alien_outx.setInterpolator(new LinearInterpolator());
                alien_outx.setStartDelay(0);
                alien_outx.start();
                alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                alien_outy.setDuration(300);
                alien_outy.setInterpolator(new LinearInterpolator());
                alien_outy.setStartDelay(0);
                alien_outy.start();

            }
            final ImageView no = (ImageView)alertDialog.findViewById(R.id.no_quit);
            final ImageView yes = (ImageView)alertDialog.findViewById(R.id.yes_quit);

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {


                    no.setEnabled(false);
                    yes.setEnabled(false);
                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -700f);
                        astro_inx.setDuration(300);
                        astro_inx.setInterpolator(new LinearInterpolator());
                        astro_inx.setStartDelay(0);
                        astro_inx.start();
                        astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 1000f);
                        astro_iny.setDuration(300);
                        astro_iny.setInterpolator(new LinearInterpolator());
                        astro_iny.setStartDelay(0);
                        astro_iny.start();

                        alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 700f);
                        alien_inx.setDuration(300);
                        alien_inx.setInterpolator(new LinearInterpolator());
                        alien_inx.setStartDelay(0);
                        alien_inx.start();
                        alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 1000f);
                        alien_iny.setDuration(300);
                        alien_iny.setInterpolator(new LinearInterpolator());
                        alien_iny.setStartDelay(0);
                        alien_iny.start();
                    }
                    new CountDownTimer(200,200) {


                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            alertDialog.dismiss();
                            close.setEnabled(true);

                           /* if(qc<qs.size()-1) {
                                Collections.shuffle(qs);
                                changeQues();
                            }*/
                          /*  countDownTimer = new MyCountDownTimer(secLeft, interval);
                            countDownTimer.start(); */                       }
                    }.start();


                }
            });

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    yes.setEnabled(false);
                    no.setEnabled(false);
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -700f);
                        astro_inx.setDuration(300);
                        astro_inx.setInterpolator(new LinearInterpolator());
                        astro_inx.setStartDelay(0);
                        astro_inx.start();
                        astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 1000f);
                        astro_iny.setDuration(300);
                        astro_iny.setInterpolator(new LinearInterpolator());
                        astro_iny.setStartDelay(0);
                        astro_iny.start();

                        alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 700f);
                        alien_inx.setDuration(300);
                        alien_inx.setInterpolator(new LinearInterpolator());
                        alien_inx.setStartDelay(0);
                        alien_inx.start();
                        alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 1000f);
                        alien_iny.setDuration(300);
                        alien_iny.setInterpolator(new LinearInterpolator());
                        alien_iny.setStartDelay(0);
                        alien_iny.start();
                    }
                    new CountDownTimer(200,200) {


                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            alertDialog.dismiss();
                            close.setEnabled(true);

                            if (checkonp) {
                                Log.e("PAUSE", checkonp + "");
                                try {
                                    countDownTimer.cancel();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                if (qansr > 0) {
                                    //tres made
                                    tres = formdata(qansr);
                                    checkonp = false;
                                    scoreActivityandPostscore();
                                } else {
                                    tres = formdata();
                                    checkonp = false;
                                    scoreActivityandPostscore();
                                }
                            } else {

                            }
                        }
                    }.start();

                }
            });
            try {
                alertDialog.show();
            }catch (Exception e){}
        } else {
            if (qansr > 0) {
                tres = formdata(qansr);
                checkonp = false;
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
                scoreActivityandPostscore();
            } else {
                tres = formdata();
                checkonp = false;
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
                scoreActivityandPostscore();
            }
        }


    }


    public void startPlanetAnimation()
    {

        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }

    public void disable_option(){
        timer_ques.setVisibility(View.GONE);

        rc_view.setVisibility(View.GONE);

    }

    public void performAction(int posis){

        if(islevelingenable) {
            //leveling enable

            try {
                countDownTimer.cancel();
            }catch (Exception e){
                e.printStackTrace();
            }
            c = Calendar.getInstance();

            if (posis == 0) {
                ans_submission = "A";
            }
            if (posis == 1) {
                ans_submission = "B";
            }
            if (posis == 2) {
                ans_submission = "C";
            }
            if (posis == 3) {
                ans_submission = "D";

            }
            if (posis == 4) {
                ans_submission = "E";

            }
            if (posis == 100) {
                ans_submission = " ";

            }

            if (firstfourquestionflag) {
                twointgap++;
            }

            if (crct == posis) {

                if (!firstfourquestionflag) {
                    firstfourcorrectquestioncount++;
                } else {
                    correctquestioncount++;
                }


                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add(ans_submission);
                            stj.add(now);
                            endj.add(df1.format(c.getTime()));
                            test.add("test1");
                            qansr++;
                            qs.remove(indexis);
                            formdatalocal(qc,1);
                        } catch (Exception exx) {

                        }
                        qc++;
                        changeQues();
                    }
                }, 1000);


            } else {
                if (!firstfourquestionflag) {

                } else {

                }


                vibrator.vibrate(500);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add(ans_submission);
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            test.add("test1");
                            qansr++;
                            qs.remove(indexis);
                            formdatalocal(qc,1);
                        } catch (Exception exx) {

                        }
                        qc++;
                        changeQues();
                    }
                }, 1000);

            }

        }else {
            //leveling not enable
            try {
                countDownTimer.cancel();
            }catch (Exception e){
                e.printStackTrace();
            }
            c = Calendar.getInstance();

            if(posis==0){
                ans_submission="A";
            }
            if(posis==1){
                ans_submission="B";
            }
            if(posis==2){
                ans_submission="C";
            }
            if(posis==3){
                ans_submission="D";

            }
            if(posis==4){
                ans_submission="E";

            }
            if(posis==100){
                ans_submission=" ";

            }

            if (crct == posis) {

                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add(ans_submission);
                            stj.add(now);
                            endj.add(df1.format(c.getTime()));
                            test.add("test1");
                            qansr++;
                            formdatalocal(qc,1);
                        } catch (Exception exx) {

                        }
                        qc++;
                        changeQues();
                    }
                }, 1000);
               // qs.remove(indexis);
            } else {
                vibrator.vibrate(500);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add(ans_submission);
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            test.add("test1");
                            qansr++;
                            formdatalocal(qc,1);
                        } catch (Exception exx) {

                        }
                        qc++;
                        changeQues();
                    }
                }, 1000);
              //  qs.remove(indexis);

            }

        }


    }


    private final BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position= Integer.parseInt(intent.getStringExtra("pos"));
            if(position==1000){
                //temp_ques_layout_window.setVisibility(View.GONE);
            }else {

                performAction(position);
            }
        }
    };





    //store getquestion data locally new changes
    private void jsonMakeFile(String s, String responseString) {

        File folder = new File(this.getFilesDir() +
                File.separator + "MCQdata");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            Log.v("folder", "Craeted");

        } else {
            Log.v("folder","Not Craeted");
        }
        get_quesdata = new File( getFilesDir() +""+s+".json");
        String previousJson = " ";
        try{
            if (get_quesdata.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(get_quesdata);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        previousJson = Charset.defaultCharset().decode(bb).toString();
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                try {
                    get_quesdata.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            Log.e("prejson",previousJson);



        }catch (NullPointerException e){
            try {
                get_quesdata.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }


        if(previousJson.length()>4){
            //json file of questions is not an empty
            Log.e("prejson","not emplty file,second attempt");
            try {
                FileInputStream stream = new FileInputStream(get_quesdata);

                try {
                    FileChannel fc = stream.getChannel();
                    MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                    old_get_ques_data = Charset.defaultCharset().decode(bb).toString();
                    Log.e("prejson ", "getques old data-" + old_get_ques_data);
                } finally {
                    stream.close();
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            //need
            flaggameattempfirst=false;
        }else {
            //json file of questions is an empty
            Log.e("prejson","emplty file,first attempt");
            try {

                FileWriter file = new FileWriter(get_quesdata);
                file.write(responseString);
                file.flush();
                file.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            //need
            flaggameattempfirst=true;

        }


        //  Log.e("Jsonfile write", jarray.toString());

        // jsonRead();
    }


    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();




    }


    //stored answers and params in file new changes
    private void jsonMakeFileAnswerandParams(String s, String answers) {


        get_ansdata = new File( getFilesDir() +"Answer"+s+".json");
        String previousJson = " ";
        try{
            if (get_ansdata.exists()) {


            } else {
                try {
                    get_ansdata.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            Log.e("prejson",previousJson);



        }catch (NullPointerException e){
            try {
                get_ansdata.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        try {
            Log.e("prejson","putting data");
            FileWriter file = new FileWriter(get_ansdata);
            file.write("");
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

            //json file of questions is an empty

        JSONObject jsonObject=new JSONObject();
        try {

            //ans array
            JSONArray main_arrayis=null;

            if(flaggameattempfirst){
                //if first time
                JSONArray jsonarrayanswers=new JSONArray(answers);
                main_arrayis=new JSONArray(answers);
                //post ans submission time value
                ans_submission_string=main_arrayis.toString();

            }else {
                //if more than first time

                //new stored answeres
                JSONArray jsonarrayanswers=new JSONArray(answers);
                //old stored answeres
                JSONArray oldStringarray=new JSONArray(mainformdatastring);
               // String string1=answers.substring(1,answers.length()-1);
               // String string2=mainformdatastring.substring(1,mainformdatastring.length()-1);
               // String conc=string2+string1;
                main_arrayis=new JSONArray(answers);
                //post ans submission time value
                ans_submission_string=main_arrayis.toString();

            }



            jsonObject.put("answeres",main_arrayis);
            jsonObject.put("levelis",levelis);
            jsonObject.put("currentquestionis",qc+1);
            jsonObject.put("firstfourcount",firstfourcorrectquestioncount);
            jsonObject.put("firstfourflag",firstfourquestionflag);
            jsonObject.put("twointgap",twointgap);
            jsonObject.put("correctquestoftwointgap",correctquestioncount);
            jsonObject.put("indexis",indexis);
            jsonObject.put("correctanswerscountparam",cqa);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //putting data into file
            try {
                Log.e("klocal_jobjdata",jsonObject.toString());
                FileWriter file = new FileWriter(get_ansdata);
                file.write(jsonObject.toString());
                file.flush();
                file.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            //need




        //  Log.e("Jsonfile write", jarray.toString());

        // jsonRead();
    }


    //load data when user already killed game new changes
    private void loadofflinedata() {
       String responsestored=old_get_ques_data;

        try {
            JSONArray data = new JSONArray(responsestored);
            //        Log.e("QUESDATA", data.length() + "");
            // JSONArray sidd = data.getJSONArray(0);
            final JSONObject sidobj = data.getJSONObject(0);
            sid = sidobj.getString("S_Id");
            q_count=sidobj.getInt("question_count");
            currentlevel=sidobj.getInt("currentGameLevel");
            maxlevel=sidobj.getInt("max_level");
            min_level=sidobj.getInt("min_level");
            if(currentlevel<min_level){
                currentlevel=min_level;
            }
            if(currentlevel>maxlevel){
                currentlevel=min_level;
            }
            levelis=currentlevel;


            JSONArray js_levelingarray=data.getJSONArray(1);

            try {
                for (int j = 0; j < maxlevel - min_level; j++) {
                    if (js_levelingarray.getJSONArray(j).length() == q_count) {
                        islevelingenable = true;
                    } else {
                        islevelingenable = false;
                    }

                }
            }catch (Exception e){
                islevelingenable = false;
                e.printStackTrace();
            }

            for(int j=0;j<js_levelingarray.length();j++) {
                questions = js_levelingarray.getJSONArray(j);
                for (int i = 0; i < questions.length(); i++) {
                    JSONObject js = questions.getJSONObject(i);
                    QuestionModel s = new QuestionModel();
                    s.setQT_id(js.getString("QT_Id"));
                    s.setQ_id(js.getString("Q_Id"));
                    s.setQuestion(js.getString("Q_Question"));
                    s.setOpt1(js.optString("Q_Option1"));
                    s.setOpt2(js.optString("Q_Option2"));
                    s.setOpt3(js.optString("Q_Option3"));
                    s.setOpt4(js.optString("Q_Option4"));
                    s.setOpt5(js.optString("Q_Option5"));
                    s.setCorrectanswer(js.getString("Q_Answer"));
                    s.setTime(js.getString("Q_MaxTime"));
                    s.setQ_level(js.getString("GL_Level"));
                    qs.add(s);
                }
            }

            int_qs_size=qs.size();
            if(q_count>qs.size()){
                isques_limit_exceed=true;
            }

            //      Log.e("QUES", questions.length() + "");
            tq = qs.size();





            setlevelingparams(cate+game+gdid+ceid+uidd+tgid);


            dialoge = new Dialog(Activity_Fib.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialoge.setContentView(R.layout.game_popup);
            Window window = dialoge.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
            dialoge.setCancelable(false);

            try {

                dialoge.show();
            }
            catch (Exception e){

                e.printStackTrace();
            }
            startTime = startTime+1;
            startTime = startTime * 1000;

            timerrr.setText(String.valueOf(startTime / 1000));
            changeQues();
            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                startPlanetAnimation();
            }
            startVerification();

        } catch (JSONException e) {

            //   Log.e("QUES", "Error parsing data " + e.toString());
        }



    }

    //set leveling params,get params data,answers data new changes
    private void setlevelingparams(String s) {
        getdatafromfile = new File( getFilesDir() +"Answer"+s+".json");
        String levelingparamsandanswers = " ";
        try{
            if (getdatafromfile.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(getdatafromfile);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        ansdatafromfile = Charset.defaultCharset().decode(bb).toString();
                        Log.e("prejson ", "getques old data-" + ansdatafromfile);
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
               //new GetQues().execute();
            }
            //get data from file
            Log.e("prejson",ansdatafromfile);



        }catch (NullPointerException e){
            try {
                getdatafromfile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }



        //json file of questions is an empty



        Log.e("klevel_fromfile",ansdatafromfile);

        try {
            JSONObject jsonObjectis=new JSONObject(ansdatafromfile);
            JSONArray jsonarrayanswers=jsonObjectis.getJSONArray("answeres");

            for(int i=0;i<jsonarrayanswers.length();i++)
            {
                JSONObject js=jsonarrayanswers.getJSONObject(i);
                qidj.add(i,js.getString("Q_Id"));
                qtid.add(i,js.getString("QT_Id"));
                ansj.add(i,js.getString("SD_UserAnswer"));
                stj.add(i,js.getString("SD_StartTime"));
                endj.add(i,js.getString("SD_EndTime"));

            }
            //old stored answers
            mainformdatastring=jsonarrayanswers.toString();
            levelis=jsonObjectis.getInt("levelis");
            qc=jsonObjectis.getInt("currentquestionis");
            firstfourcorrectquestioncount=jsonObjectis.getInt("firstfourcount");
            firstfourquestionflag=jsonObjectis.getBoolean("firstfourflag");
            twointgap=jsonObjectis.getInt("twointgap");
            correctquestioncount=jsonObjectis.getInt("correctquestoftwointgap");
            cqa=jsonObjectis.getInt("correctanswerscountparam");
            indexis=jsonObjectis.getInt("indexis");

            for(int i=0;i<jsonarrayanswers.length();i++){
                JSONObject jsonObject=jsonarrayanswers.getJSONObject(i);
                String ids=jsonObject.getString("Q_Id");
                for(int j=0;j<qs.size();j++){
                    QuestionModel questionModel=qs.get(j);
                    if(ids.equalsIgnoreCase(questionModel.getQ_id())){
                        if(islevelingenable){
                            qs.remove(j);
                        }else {

                        }

                    }
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }




    }

    //new changes
    public void getPostData(){

        getdatafromfile = new File( getFilesDir() +"Answer"+cate+game+gdid+ceid+uidd+tgid+".json");
        String levelingparamsandanswers = " ";
        try{
            if (getdatafromfile.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(getdatafromfile);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        ansdatafromfile = Charset.defaultCharset().decode(bb).toString();
                        Log.e("prejson ", "getques old data-" + ansdatafromfile);
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                //new GetQues().execute();
            }
            //get data from file
            Log.e("prejson",ansdatafromfile);



        }catch (NullPointerException e){
            try {
                getdatafromfile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        try {
            JSONObject jsonObjectis=new JSONObject(ansdatafromfile);

            JSONArray jsonarrayanswers=jsonObjectis.getJSONArray("answeres");
            postvaluearray=jsonarrayanswers.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject studentsObj = new JSONObject();
        JSONArray jsonArray= null;
        try {
            jsonArray = new JSONArray(postvaluearray);
            studentsObj.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postvalues=studentsObj.toString();

    }

}


