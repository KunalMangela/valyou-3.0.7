package com.globalgyan.getvalyou;

public class LeaderBoardAssgroupModel {
    String id,name,ass_ids,enable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAss_ids() {
        return ass_ids;
    }

    public void setAss_ids(String ass_ids) {
        this.ass_ids = ass_ids;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }
}
