package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.cms.response.GetOrganizationResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.Organization;

import java.util.ArrayList;
import java.util.List;

public class OrganizationActivity extends AppCompatActivity implements GUICallback {
    private TextView txtNext = null;
    private EditText txtSearch = null;
    private OrganisationAdapter organisationAdapter = null;
    private RecyclerView recyclerView = null;
    private Button btnDone = null;
    private List<Organization> organizations = new ArrayList<>() ;
    private String selected = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);

       /* try {
            selected = getIntent().getExtras().getString("selected");
        }catch (Exception ex){
            selected = null;
        }*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }

        TextView txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMessage = new Intent();
                setResult(RESULT_CANCELED ,intentMessage);
                finish();
                //overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });


        txtNext = (TextView) findViewById(R.id.txtNext);
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        btnDone = (Button) findViewById(R.id.btnDone);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(OrganizationActivity.this));


        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(this,this);
        processor.getAllOrganisations();



       /* final List<Industry> industries = new ArrayList<>();
        for(int i=0;i<25;i++){
            Industry industry = new Industry();
            industry.setName("Organization Name "+String.valueOf(i));
            industry.setDescription("Description "+String.valueOf(i));
            industries.add(industry);
        }*/

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(organisationAdapter.getStringBuffer())){
                    Intent intentMessage = new Intent();
                    intentMessage.putExtra("INDUSTRIES", organisationAdapter.getStringBuffer());
                    setResult(RESULT_OK, intentMessage);

                    if (getParent() == null) {
                        setResult(RESULT_OK, intentMessage);
                    } else {
                        getParent().setResult(RESULT_OK, intentMessage);
                    }
                    finish();
                }else{
                    Toast.makeText(OrganizationActivity.this,"Please select one ", Toast.LENGTH_SHORT).show();
                }

            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
                charSequence = charSequence.toString().trim().toLowerCase();

                final List<Organization> filteredList = new ArrayList<>();
                boolean itemFound = false;
                for (int i = 0; i < organizations.size(); i++) {

                    final String text = organizations.get(i).getName().toLowerCase();
                    if (text.contains(charSequence)) {

                        filteredList.add(organizations.get(i));
                    }
                }
                if (!itemFound) {
                    Organization college = new Organization();
                    college.setName(charSequence.toString().toUpperCase());
                    college.set_id("new");
                    selected = charSequence.toString();
                    filteredList.add(college);
                }
                organisationAdapter = new OrganisationAdapter(filteredList,OrganizationActivity.this,selected);
                recyclerView.setAdapter(organisationAdapter);
                organisationAdapter.notifyDataSetChanged();  // data set changed
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {
                if (guiResponse.isStatus()) {
                    GetOrganizationResponse response = (GetOrganizationResponse) guiResponse;
                    if (response != null && response.getOrganizations() != null && response.getOrganizations().size() > 0) {
                        organizations = response.getOrganizations();
                        organisationAdapter = new OrganisationAdapter(organizations, this, selected);
                        recyclerView.setAdapter(organisationAdapter);
                    } else {
                        Toast.makeText(this, "No organisations added ", Toast.LENGTH_SHORT).show();
                    }
                }
            } else
                Toast.makeText(this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }
    }
}
