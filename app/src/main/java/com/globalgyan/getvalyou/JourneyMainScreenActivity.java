package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.utils.ConnectionUtils;

public class JourneyMainScreenActivity extends AppCompatActivity {

    ImageView profileplanet,leaderboard,progress,close, myjourney;
    ConnectionUtils connectionUtils;
    TextView textjourney;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_main_screen);
        profileplanet=(ImageView)findViewById(R.id.profile_planet);
        leaderboard=(ImageView)findViewById(R.id.leaderboard_planet);
        progress=(ImageView)findViewById(R.id.Progress_planet);
        myjourney=(ImageView)findViewById(R.id.lower_palnet);
        close=(ImageView)findViewById(R.id.backbtnis_journey);
        connectionUtils=new ConnectionUtils(this);
        textjourney=(TextView)findViewById(R.id.textjourney);
        myjourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JourneyMainScreenActivity.this, MainPlanetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        });

        profileplanet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileplanet.setEnabled(false);
                leaderboard.setEnabled(false);
                progress.setEnabled(false);
                new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        profileplanet.setEnabled(true);
                        leaderboard.setEnabled(true);
                        progress.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {
                    Intent intent = new Intent(JourneyMainScreenActivity.this, ProfileActivity.class);
                    intent.putExtra("nav_to","journey");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                }else {
                    Toast.makeText(JourneyMainScreenActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }

            }
        });




        leaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileplanet.setEnabled(false);
                leaderboard.setEnabled(false);
                progress.setEnabled(false);
                new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        profileplanet.setEnabled(true);
                        leaderboard.setEnabled(true);
                        progress.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {

                    Intent intent = new Intent(JourneyMainScreenActivity.this, LeaderboardActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                }else {
                    Toast.makeText(JourneyMainScreenActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }

                   }
        });

        progress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileplanet.setEnabled(false);
                leaderboard.setEnabled(false);
                progress.setEnabled(false);
                new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        profileplanet.setEnabled(true);
                        leaderboard.setEnabled(true);
                        progress.setEnabled(true);
                    }
                }.start();
                if(connectionUtils.isConnectionAvailable()) {

                    Intent intent = new Intent(JourneyMainScreenActivity.this, ActiveCoursesActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();
                }
                else {
                    Toast.makeText(JourneyMainScreenActivity.this,"Please check your internet connection",Toast.LENGTH_SHORT).show();

                }
               // Toast.makeText(JourneyMainScreenActivity.this,"Coming Soon...",Toast.LENGTH_SHORT).show();

            }
        });

        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        textjourney.setTypeface(typeface1);


        textjourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JourneyMainScreenActivity.this, MainPlanetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JourneyMainScreenActivity.this, MainPlanetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        });


        close.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    close.setBackgroundColor(Color.parseColor("#33ffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            close.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }*/
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(JourneyMainScreenActivity.this, MainPlanetActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
        finish();
    }
}
