package com.globalgyan.getvalyou;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.globalgyan.getvalyou.apphelper.AppConstant;

import de.morrox.fontinator.FontTextView;

import static android.content.Context.BATTERY_SERVICE;
import static com.globalgyan.getvalyou.LanguageActivity.selected_lang;
import static com.globalgyan.getvalyou.Welcome.intro1_title2_string;
import static com.globalgyan.getvalyou.Welcome.intro2_desc1_string;
import static com.globalgyan.getvalyou.Welcome.intro2_desc2_string;
import static com.globalgyan.getvalyou.Welcome.intro2_desc3_string;
import static com.globalgyan.getvalyou.Welcome.intro2_desc4_string;
import static com.globalgyan.getvalyou.Welcome.intro2_title1_string;
import static com.globalgyan.getvalyou.Welcome.startanim;
import static com.globalgyan.getvalyou.Welcome.tf2;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroTwo extends Fragment {
    View v;
    Spannable colored_str_in_intro2;
    Animation zoom_in;

    boolean visible = false;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
   static boolean flag_anim_done2 = false;
    static ImageView astro2;
    static Animation scaleAnimation;
    public IntroTwo() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.intro_two, container, false);


        setRetainInstance(true);


//        astro2 = (ImageView) v.findViewById(R.id.rocket_intro2);
//        zoom_in = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in_astro_image);





        return v;
    }
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//     //   super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//          //  astro_img.setVisibility(View.INVISIBLE);
//            Handler handler = new Handler();
//
//            Runnable runnableimage = new Runnable() {
//                public void run() {
//                    //HERE YOUR ANIMATION
//                    astro_img.setVisibility(View.VISIBLE);
//
//                }
//            };
//            handler.postDelayed(runnableimage, 1000);
//      //      astro_img.startAnimation(zoom_in);
//        }
//        else {
//            visible = true;
//           // astro_img.setVisibility(View.INVISIBLE);
//        }
//    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);

        appConstant = new AppConstant(getActivity());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String lang = prefs.getString("lang", "lang");

        String intro2_title1_string = prefs.getString("colored_str_in_intro2", "colored_str_in_intro2");
        String intro2_desc1_string = prefs.getString("intro2_desc1_string", "intro2_desc1_string");
        String intro2_desc2_string = prefs.getString("intro2_desc2_string", "intro2_desc2_string");
        String intro2_desc3_string = prefs.getString("intro2_desc3_string", "intro2_desc3_string");
        String intro2_desc4_string = prefs.getString("intro2_desc4_string", "intro2_desc4_string");

        if(!lang.equals("中国")) {
            colored_str_in_intro2 = new SpannableString(intro2_title1_string);
            String firstWord1 = intro2_title1_string.substring(0, intro2_title1_string.lastIndexOf(" "));
            colored_str_in_intro2.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord1.length(), intro2_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        }
        else{

            colored_str_in_intro2 = new SpannableString(intro2_title1_string);


        }

        FontTextView tv = (FontTextView) v.findViewById(R.id.title_text_intro2);

            tv.setText(colored_str_in_intro2);


        FontTextView tv1 = (FontTextView) v.findViewById(R.id.intropage2_text1);
        tv1.setText(intro2_desc1_string);
        FontTextView tv2 = (FontTextView) v.findViewById(R.id.intropage2_text2);
        tv2.setText(intro2_desc2_string);
        FontTextView tv3 = (FontTextView) v.findViewById(R.id.intropage2_text3);
        tv3.setText(intro2_desc3_string);
        FontTextView tv4 = (FontTextView) v.findViewById(R.id.intropage2_text4);
        tv4.setText(intro2_desc4_string);

        tv.setTypeface(tf2);
        tv1.setTypeface(tf2);
        tv2.setTypeface(tf2);
        tv3.setTypeface(tf2);
        tv4.setTypeface(tf2);

        bm = (BatteryManager)getActivity().getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        Log.e("battery", ""+batLevel);


        astro2  = (ImageView)v.findViewById(R.id.rocket_intro2);
        astro2.setVisibility(View.INVISIBLE);

        scaleAnimation = new ScaleAnimation(
                0f, 1f, // Start and end values for the X axis scaling
                0f, 1f, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(500);


      /*  if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus"))||batLevel==0){
            if (!flag_anim_done2) {
                astro2.startAnimation(scaleAnimation);

            }

            scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {


                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    flag_anim_done2 = true;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }*/
    }
    public static IntroTwo newInstance(String text) {

        IntroTwo f = new IntroTwo();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

}
