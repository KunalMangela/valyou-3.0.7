package com.globalgyan.getvalyou.ListeningGame;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.globalgyan.getvalyou.MasterSlaveGame.MasterSlaveGame;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.customwidget.FlowLayout;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import static com.globalgyan.getvalyou.ListeningGame.ListeningGameActivity.applyBlurMaskFilter;
import static com.globalgyan.getvalyou.ListeningGame.ListeningGameActivity.doblur;

/**
 * Created by Globalgyan on 09-03-2018.
 */

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.MyViewHolder> {

    private Context mContext;
    private static String[] options_array;
    Dialog dialogeis_trans;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView option;
        CircularProgressBar circularProgressBar,circularProgressBar_middle;


        public MyViewHolder(View view) {
            super(view);

            option = (TextView) view.findViewById(R.id.option_is);
            circularProgressBar = (CircularProgressBar)view.findViewById(R.id.circularbar);
            circularProgressBar_middle = (CircularProgressBar)view.findViewById(R.id.circularbar_middle);
        }
    }


    public OptionsAdapter(Context mContext, String[] options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.option_items_layout, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.option.setText(options_array[position]);

//         if(!doblur){
//
//            holder.option.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
//            holder.option.getPaint().setMaskFilter(null);
//            holder.option.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent brodcast_receiver_screen=new Intent("Show_screen");
//                    brodcast_receiver_screen.putExtra("pos", String.valueOf(position));
//                    mContext.sendBroadcast(brodcast_receiver_screen);
//
//                }
//            });
//
//        }
//        else {
//
//            applyBlurMaskFilter(holder.option, BlurMaskFilter.Blur.NORMAL);
//
//        }
        Typeface typeface1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/AppleChancery.ttf");
        holder.option.setTypeface(typeface1,Typeface.BOLD);
        holder.option.setText(options_array[position]);

        if(ListeningGameActivity.blurflag){
            applyBlurMaskFilter(holder.option, BlurMaskFilter.Blur.NORMAL);
        }else {
            holder.option.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
            holder.option.getPaint().setMaskFilter(null);
            holder.option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.option.setEnabled(false);
                    holder.option.setClickable(false);
                    //showDialoguetransparent();
                    ListeningGameActivity.rc_options.addOnItemTouchListener(ListeningGameActivity.disabler);

                    new CountDownTimer(1200, 1200) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            //dialogeis_trans.dismiss();
                            ListeningGameActivity.rc_options.removeOnItemTouchListener(ListeningGameActivity.disabler);
                            holder.option.setEnabled(true);
                            holder.option.setClickable(true);
                        }
                    }.start();
                    if(ListeningGameActivity.crct==position){
                        holder.circularProgressBar.setColor(ContextCompat.getColor(mContext, R.color.lime));
                        holder.circularProgressBar_middle.setColor(ContextCompat.getColor(mContext, R.color.lime));

                    }else {
                        holder.circularProgressBar.setColor(ContextCompat.getColor(mContext, R.color.red));
                        holder.circularProgressBar_middle.setColor(ContextCompat.getColor(mContext, R.color.red));
                    }

                    Intent brodcast_receiver_screen=new Intent("Show_screen_listen");
                    brodcast_receiver_screen.putExtra("pos", String.valueOf(position));
                    mContext.sendBroadcast(brodcast_receiver_screen);
                    int animationDuration = 1000; // 2500ms = 2,5s
                    holder.circularProgressBar.setProgressWithAnimation(100, animationDuration);
                    holder.circularProgressBar_middle.setProgressWithAnimation(100, animationDuration);
                }
            });
        }

    }
    @Override
    public int getItemCount() {
        return options_array.length;
    }

    public void showDialoguetransparent(){
        dialogeis_trans = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        dialogeis_trans.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogeis_trans.setContentView(R.layout.game_popup);
        Window window = dialogeis_trans.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialogeis_trans.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
        dialogeis_trans.show();
    }
}




