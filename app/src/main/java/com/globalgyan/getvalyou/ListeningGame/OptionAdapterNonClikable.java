package com.globalgyan.getvalyou.ListeningGame;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.globalgyan.getvalyou.MasterSlaveGame.CustomTypefaceSpan;
import com.globalgyan.getvalyou.MasterSlaveGame.MasterSlaveGame;
import com.globalgyan.getvalyou.R;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.ListeningGame.ListeningGameActivity.applyBlurMaskFilter;
import static com.globalgyan.getvalyou.ListeningGame.ListeningGameActivity.doblur;

/**
 * Created by Globalgyan on 09-03-2018.
 */

public class OptionAdapterNonClikable extends RecyclerView.Adapter<OptionAdapterNonClikable.MyViewHolder> {

    private Context mContext;
    private static ArrayList<String> options_array;
    Typeface typeface1;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView option;
        public ImageView im_tick;

        public MyViewHolder(View view) {
            super(view);

            option = (FontTextView) view.findViewById(R.id.option_is_non_clikable);
            im_tick=(ImageView)view.findViewById(R.id.opt);
        }
    }


    public OptionAdapterNonClikable(Context mContext, ArrayList<String> options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.options_item_layout_non_clikable, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.option.setText(options_array.get(position));


        if(ListeningGameActivity.blurflag){
            applyBlurMaskFilter(holder.option, BlurMaskFilter.Blur.NORMAL);
        }else {
            holder.option.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
           holder.option.getPaint().setMaskFilter(null);
        }
        if(position==0){
            typeface1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Montserrat-Regular.ttf");
            Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Montserrat-Regular.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(options_array.get(position));
            SS.setSpan(new CustomTypefaceSpan("", typeface1), 0, 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", tf2), 2, options_array.get(position).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
          //  SS.setSpan(new RelativeSizeSpan(1.4f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            // txt.setText(SS);
            holder.option.setText(SS);
        }else {
            typeface1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Montserrat-Regular.ttf");
            Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Montserrat-Regular.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(options_array.get(position));
            SS.setSpan(new CustomTypefaceSpan("", typeface1), 0, 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", tf2), 2, options_array.get(position).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
           // SS.setSpan(new RelativeSizeSpan(1.4f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            // txt.setText(SS);
            holder.option.setText(SS);
        }
        int pos=position-1;
        if(ListeningGameActivity.last_tick) {
            if (ListeningGameActivity.pos_clicked == pos) {
                Log.e("matched", pos + "match");
                if(ListeningGameActivity.crct==pos){
                    holder.im_tick.setImageResource(R.drawable.check_right);
                }else {
                    holder.im_tick.setImageResource(R.drawable.check_wrong);
                }
                holder.im_tick.setVisibility(View.VISIBLE);

                ListeningGameActivity.last_tick = false;
            } else {
                holder.im_tick.setVisibility(View.INVISIBLE);
                Log.e("matched", pos + "notmatch");

            }
        }



//        if(!doblur){
//
//            holder.option.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
//            holder.option.getPaint().setMaskFilter(null);
//        }
//        else {
//            applyBlurMaskFilter(holder.option, BlurMaskFilter.Blur.NORMAL);
//
//        }

    }


    @Override
    public int getItemCount() {
        return options_array.size();
    }
}





