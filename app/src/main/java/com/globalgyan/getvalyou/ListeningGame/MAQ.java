package com.globalgyan.getvalyou.ListeningGame;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.androidhiddencamera.CameraConfig;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.Activity_Fib;
import com.globalgyan.getvalyou.CameraPreview;
import com.globalgyan.getvalyou.CheckP;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.MasterSlaveGame.New_MSQ;
import com.globalgyan.getvalyou.MasterSlaveGame.Temp_model;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.ToadysgameModel;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.QuestionModel;
import com.globalgyan.getvalyou.simulation.core.DownloadManagerPro;
import com.globalgyan.getvalyou.simulation.report.listener.DownloadManagerListener;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.iambedant.text.OutlineTextView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.HttpGet;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.VerifyResult;
import com.pluscubed.recyclerfastscroll.RecyclerFastScroller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;

public class MAQ extends AppCompatActivity implements DownloadManagerListener {
    static Typeface tf1;
    static NestedScrollView ques_scrollview;
    boolean flag_click = false, ismediaplayimg = false;
    boolean flag_scroll = false;
    static Animation ques_anim;
    static Animation ques_anim_left;
    private SimpleLocation location;
    static Vibrator vibrator;
    int para_count = 0,para_called_count = 0;
    static AnimationSet animationSet;
    Animation fadeOut;
    static RelativeLayout main_ques_layout_window;
    static LinearLayout maq_ques;
    ImageView astro_maq;
    boolean flag_isvisible = false, flag_rc_scrolling = false;
    int count = 0, mediaIndex = 0;
    public static int pos_clicked;
    public static boolean last_tick = false;
    static RecyclerView rc_options_non_clickable;
    Option_clickable optionAdapterNonClikable;
    MediaPlayer mp;
    int mplength = 0;
    int fulltime;
    Dialog alertDialog,alert_on_retry, proceed_dialog,alert_on_audio_close,alert_on_audio_ques_close;

    int para_audio_count = 0;
    int paragraph_count = 0;

    int current_audio_count = 0;

    FontTextView current_audio_text;
    boolean alert_audio_close =false, alert_on_close = false;
    boolean close_on_get_ques = false;
    //new
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    Face mface = null;
    public static int crct;
    boolean flag_is_present_id = false;
    UUID faceid1, faceid2;
    boolean checkonp = true;
    CameraConfig mCameraConfig;
    boolean wtf;
    ImageView play_btn, playing_btn;
    ArrayList<String> qid_array;
    QuestionModel qsmodel;
    String newquid;
    Dialog dialog;
    ArrayList<QuestionModel> qs = new ArrayList<>();
    File fol;
    public static boolean blurflag = false;
    List<Face> faces;
    ArrayList<Integer> audio=new ArrayList<>();
    ArrayList<String> assets = new ArrayList<>();
    ArrayList<Integer> levels=new ArrayList<>();
    ArrayList<Integer> asset_id=new ArrayList<>();
    private CountDownTimer countDownTimer;
    private long startTime = 180 * 1000;
    private final long interval = 1 * 1000;
    private long secLeft = startTime;
    DefaultHttpClient httpClient;
    HttpURLConnection urlConnection;
    HttpPost httpPost;
    HttpGet httpGet;
    int qansr = 0;
    Bitmap storedpic;
    int img_count;
    static FontTextView timerrr, dqus, timer_img;
    OutlineTextView qno;
    String tres = "";
    int code1, code2, code3;
    static DownloadManagerPro dm;
    static File folder;
    int cqa = 0;
    IntentFilter filter;
    ImageView close, audio_close;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null;
    ArrayList<String> qidj, ansj, stj, endj,qtid;
    JSONArray questions;
    JSONArray audios=new JSONArray();
    static String json = "", jso = "", Qus = "", Qs = "", token = "", token_type = "", quesDis = "", assessg = "";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int score = 0;
    ArrayList<String> optioncard;
    ImageButton pause;
    URL ppp;
    int pauseCount = 0, qc = 0, qid = 0, tq = 0, ac = -1, cnt = 0, no_of_ques_in_list = 0, anim_limit, no_of_current_ques_per_para=0;
    SimpleDateFormat df1;
    Calendar c;
    LinearLayout cl1, cl2, cl3, cl4, cl5;
    FontTextView o1, o2, o3, o4, o5;
    String uidd, adn, ceid, gdid, game, cate, tgid, sid, aid,tg_groupid,certifcation_id,course_id,lg_id;
    String candidateId;

    boolean media_pause=false;
    String now;
    // KProgressHUD assess_progress;

    Dialog assess_progress;
    //no use
    KProgressHUD progressDialog;


    private AudioManager mAudioManager;


    //  KProgressHUD quesLoading;

    Dialog quesLoading;

    // KProgressHUD verification_progress;

    Dialog verification_progress;

    public static RecyclerView.OnItemTouchListener disabler;
    public static MediaPlayer mp_click;

    boolean quit_msg_showing = false;

   static Animation blinking_anim;
    String faceid;
    static UUID mFaceId1;
    public static String mImageFileLocation = "";
    Uri imageUri;
    Bitmap bmpother;
    int count_detect_no=0,image_face_count=0,auto_capt_count=0;
    ArrayList<UUID> detectedfaceid = new ArrayList<>();
    ArrayList<String> imagelocation = new ArrayList<>();
    ArrayList<Bitmap> autotakenpic = new ArrayList<>();
    ArrayList<String > facedetectedimagelocation = new ArrayList<>();
    String autocapimg1loc1, autocapimg1loc2,autocapimg1loc3;
    ArrayList<String > autocapimgloc = new ArrayList<>();
    ArrayList<String > imgurllist = new ArrayList<>();
    String img1,img2="",img3="";
    String replacedurl;
    private Camera camera;
    Timer timer1;
    CountDownTimer waitTimer;
    public int cameraId = 2;
    private CameraPreview mPreview;
    FrameLayout preview;
    FaceServiceClient faceServiceClient;
    PrefManager prefManager;
    Bitmap decodedByte;
    int piccapturecount;
    public static boolean changeparaflag=false;
    ArrayList<Temp_model> temp_list=new ArrayList<>();

    RelativeLayout timer_ques;

    int picture_count=0;
    int pic_frequency;


    //leveling
    int currentlevel,maxlevel,min_level,int_qs_size,q_count,levelis=0,paraques=0,correctques=0,index=0,qc_index=0,firstpara=0;
    public static boolean islevelingenable=false;
    public static boolean parachangeis=false,isques_limit_exceed=false;



    VisualizerView mVisualizerView;
    Visualizer mVisualizer;
    AssetManager asset;
    AssetFileDescriptor afd;

    static boolean doblur = false;
    static boolean flagdone = false;
    FrameLayout rel_popup;
    VideoView videoView;




    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2, astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;



    RecyclerFastScroller fastScroller;
    BatteryReceiver mBatInfoReceiver;

    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;

    boolean proceed_dialog_showing = false;
    String first_msg = "", quit_msg="";
    String get_nav = " ";
    FontTextView category_name;




    //new
    public static File get_quesdata,get_ansdata,getdatafromfile;
    public static String old_get_ques_data,formdatanewstring="",mainformdatastring="",ans_submission_string="",ansdatafromfile="",postvaluearray="",postvalues="";
    public static boolean flaggameattempfirst=true;
    public static int acq=0,im_temp=0;

    String mfaceid;
    UUID mfaceid1;

    @SuppressLint({"NewApi", "ClickableViewAccessibility"})

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_maq);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        appConstant = new AppConstant(this);
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        location = new SimpleLocation(this);

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
      /*  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      */  getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

      faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(MAQ.this).get_frkey());
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        int media_current_volume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        int media_max_volume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        int mid_volume = media_max_volume/2;


        if(media_current_volume<mid_volume){
            Log.e("volume level", ""+media_current_volume);



            //  Toast.makeText(getApplicationContext(),"Please turn up the volume",Toast.LENGTH_LONG).show();
        }

        tf1 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");


        qsmodel = new QuestionModel();
        createFolder();
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);



//        verification_progress = KProgressHUD.create(MAQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);
        get_nav =new PrefManager(MAQ.this).getNav();


        verification_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        verification_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verification_progress.setContentView(R.layout.planet_loader);
        Window window1 = verification_progress.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        verification_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        verification_progress.setCancelable(false);
        GifView gifView2 = (GifView) verification_progress.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();





        camera = getCameraInstance();
        prefManager = new PrefManager(MAQ.this);

        mfaceid = prefManager.getprofilefaceid();
        mfaceid1 = UUID.fromString(mfaceid);

        preview = (FrameLayout) findViewById(R.id.camera_prv);

        mp_click=MediaPlayer.create(this, R.raw.button_click);

        dm = new DownloadManagerPro(this.getApplicationContext());
        dm.init(folder.getPath(), 12, MAQ.this);
        rel_popup = (FrameLayout) findViewById(R.id.playing_audio_layout);
        mVisualizerView = (VisualizerView) findViewById(R.id.myvisualizerview);
        // para_image = (ImageView) findViewById(R.id.para_image);
        play_btn = (ImageView) findViewById(R.id.play_btn);
        playing_btn = (ImageView) findViewById(R.id.playing);
        close = (ImageView) findViewById(R.id.close_btn);

        audio_close = (ImageView)findViewById(R.id.audio_close);


        //   fastScroller = (RecyclerFastScroller)findViewById(R.id.fastscroll) ;

        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);


        category_name = (FontTextView) findViewById(R.id.category_name);
        current_audio_text = (FontTextView) findViewById(R.id.current_audio);


        //  ques=(TextView)findViewById(R.id.qdis);
        //  ques_scrollview=(NestedScrollView)findViewById(R.id.firstscroll);
        play_btn.setEnabled(false);
        rc_options_non_clickable = (RecyclerView) findViewById(R.id.non_clickableoptions_list);

        //options_clickable
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        String[] options_array = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"};
        int no_options = 5;
        String[] confirm_options = new String[no_options];

        for (int i = 0; i < 5; i++) {
            confirm_options[i] = options_array[i];
        }
//        rc_options.setAdapter(new OptionsAdapter(this,confirm_options));
        //options_non_clickable
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_options_non_clickable.setLayoutManager(linearLayoutManager1);
        String[] options_array_non_clikable = {"Q1: Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating?", "Option A:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating", "Option B:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating", "Option C:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating", "Option D:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating", "Option E:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating", "Option F", "Option G"};
        String[] confirm_options_non_clickable = new String[no_options + 1];

        for (int i = 0; i < 6; i++) {
            confirm_options_non_clickable[i] = options_array_non_clikable[i];
        }
        // rc_options_non_clickable.setAdapter(new OptionAdapterNonClikable(this,confirm_options_non_clickable));
        filter = new IntentFilter("Show_screen_listen");
        this.registerReceiver(mReceiver, filter);

        //option_non_clickable_temp
        LinearLayoutManager linearLayoutManager11 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        //  rc_options_non_clickable_temp.setAdapter(new OptionAdapterNonClikable(this,confirm_options_non_clickable));

        maq_ques = (LinearLayout) findViewById(R.id.maq_ques);
        astro_maq = (ImageView) findViewById(R.id.astro_maq);

        timer_ques =(RelativeLayout)findViewById(R.id.rl_above);
        main_ques_layout_window = (RelativeLayout) findViewById(R.id.main_ques_layout_window);
        //    timer_seek = (IndicatorSeekBar) findViewById(R.id.timer_seek);

         blinking_anim = new AlphaAnimation(0.0f, 1.0f);

        blinking_anim.setDuration(500); //You can manage the blinking time with this parameter
        blinking_anim.setStartOffset(10);
        blinking_anim.setRepeatMode(Animation.REVERSE);
        blinking_anim.setRepeatCount(Animation.INFINITE);

        blinking_anim.setInterpolator(new AccelerateInterpolator());

        videoView = (VideoView) findViewById(R.id.video_view);
        play_btn.setImageResource(R.drawable.maq_play);

       // close.setVisibility(View.GONE);

        maq_ques.setVisibility(View.GONE);
        astro_maq.setVisibility(View.GONE);
        //  ques.setTypeface(tf2);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        disabler = new RecyclerViewDisabler();

        //ques_frame.setLayoutParams(new LinearLayout.LayoutParams(width,height));
        // main_scroller.scrollTo(0,main_scroller.getTop());
        final Animation animation_right =
                AnimationUtils.loadAnimation(MAQ.this,
                        R.anim.blink);


        ques_anim =
                AnimationUtils.loadAnimation(MAQ.this,
                        R.anim.right);
        ques_anim.setInterpolator(new AccelerateInterpolator());

        ques_anim_left =
                AnimationUtils.loadAnimation(MAQ.this,
                        R.anim.left);
        ques_anim_left.setInterpolator(new AccelerateInterpolator());


        fadeOut = new AlphaAnimation(0.0f, 1.0f);
        fadeOut.setDuration(1500);
        mp = new MediaPlayer();



        play_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound();

            }
        });


        //  doblur = media_frame.isEnabled();
        //new
        c = Calendar.getInstance();
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        progressDialog = KProgressHUD.create(MAQ.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Logging in")
                .setDimAmount(0.7f)
                .setCancellable(false);
//        quesLoading = KProgressHUD.create(MAQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Loading assets...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);



        quesLoading = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        quesLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        quesLoading.setContentView(R.layout.assess_ques_loading_loader);
        Window window = quesLoading.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        quesLoading.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        quesLoading.setCancelable(false);
        GifView gifView1 = (GifView) quesLoading.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();

        ImageView close_btn = (ImageView) quesLoading.findViewById(R.id.close_loader);



//        assess_progress = KProgressHUD.create(MAQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);


        assess_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        assess_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assess_progress.setContentView(R.layout.planet_loader);
        Window window2 = assess_progress.getWindow();
        WindowManager.LayoutParams wlp2 = window2.getAttributes();
        wlp2.gravity = Gravity.CENTER;
        wlp2.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window2.setAttributes(wlp2);
        assess_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        assess_progress.setCancelable(false);
        GifView gifView3 = (GifView) assess_progress.findViewById(R.id.loader);
        gifView3.setVisibility(View.VISIBLE);
        gifView3.play();
        gifView3.setGifResource(R.raw.loader_planet);
        gifView3.getGifResource();

        qid_array = new ArrayList<>();


        candidateId = PreferenceUtils.getCandidateId(MAQ.this);
        qidj = new ArrayList<>();
        ansj = new ArrayList<>();
        stj = new ArrayList<>();
        endj = new ArrayList<>();
        qtid=new ArrayList<>();
        //need to change
        try {
            uidd = getIntent().getExtras().getString("uidg");
            adn = getIntent().getExtras().getString("adn");
            ceid = getIntent().getExtras().getString("ceid");
            gdid = getIntent().getExtras().getString("gdid");
            startTime = Long.parseLong(getIntent().getExtras().getString("timeis"));
            game = getIntent().getExtras().getString("game");
            cate = getIntent().getExtras().getString("category");
            tgid = getIntent().getExtras().getString("tgid");
            aid = getIntent().getExtras().getString("aid");
            token = getIntent().getExtras().getString("token");
            assessg = getIntent().getExtras().getString("assessgroup");

            tg_groupid = getIntent().getExtras().getString("TG_GroupId");
            certifcation_id = getIntent().getExtras().getString("certification_id");
            course_id = getIntent().getExtras().getString("course_id");
            lg_id = getIntent().getExtras().getString("lg_id");
            pic_frequency = getIntent().getIntExtra("pic_frequency",0);

            Log.e("pic_frequency",""+pic_frequency);


            startTime = startTime * 1000;
            secLeft = startTime;

        } catch (Exception ex) {

        }
        category_name.setText(cate);


        jsonMakeFile(cate+game+gdid+ceid+uidd+tgid," ");

        first_msg = "We are submitting your answers and you will not be able to answer again the same category";

        quit_msg = first_msg+ " "+ cate;

        timerrr = (FontTextView) findViewById(R.id.timerr);
        timer_img = (FontTextView) findViewById(R.id.timer_img);
        qno = (OutlineTextView) findViewById(R.id.qno);
        qno.setTypeface(tf1,Typeface.BOLD);
        // dqus = (FontTextView) findViewById(R.id.qdis);
        countDownTimer = new MyCountDownTimer(startTime, interval);
        timerrr.setText(String.valueOf(startTime / 1000));
        pause = (ImageButton) findViewById(R.id.pause);


        audio_close.setEnabled(false);
        audio_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                audio_close.setEnabled(false);
                if (mp != null && mp.isPlaying()) {
                    try {

                        mp.pause();
                        mplength = mp.getCurrentPosition();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (pauseCount == 0) {

//                    dialog = new Dialog(MAQ.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialog.setContentView(R.layout.pause_popup);
//                    Window window = dialog.getWindow();
//                    WindowManager.LayoutParams wlp = window.getAttributes();
//
//                    wlp.gravity = Gravity.CENTER;
//                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//                    window.setAttributes(wlp);
//                    dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
//                    dialog.setCancelable(false);
//                    try {
//                        dialog.show();
//                    }
//                    catch (Exception e){
//                        e.printStackTrace();
//                    }
//                    Button pu = (Button) dialog.findViewById(R.id.resume);
//                    pu.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            dialog.dismiss();
//                            changeQues();
//                            if (!ismediaplayimg) {
//                                vdo_performance_ques_timer = new MyCountDownTimer(secLeft, interval);
//                                vdo_performance_ques_timer.start();
//                            } else {
//                                try {
//                                    mp.seekTo(mplength);
//                                    mp.start();
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            pauseCount++;
//                        }
//                    });
                    //  vdo_performance_ques_timer.cancel();
//                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MAQ.this);
//// ...Irrelevant code for customizing the buttons and title
//                    LayoutInflater inflater = LayoutInflater.from(MAQ.this);
//                    View dialogView = inflater.inflate(R.layout.quit_warning, null);
//                    dialogBuilder.setView(dialogView);
//
//
//                    alertDialog = dialogBuilder.create();
//
//                    alertDialog.setCancelable(false);
//                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//
//                    alertDialog.show();
//
//
//                    final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
//                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);
                    alert_on_audio_ques_close = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alert_on_audio_ques_close.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert_on_audio_ques_close.setContentView(R.layout.assess_quit_warning);
                    Window window = alert_on_audio_ques_close.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alert_on_audio_ques_close.setCancelable(false);

                    alert_on_audio_ques_close.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alert_on_audio_ques_close.findViewById(R.id.msg);
                    msg.setText(quit_msg);

                    final ImageView astro = (ImageView) alert_on_audio_ques_close.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alert_on_audio_ques_close.findViewById(R.id.alien);
                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();



                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();
                    }

                    final ImageView no = (ImageView)alert_on_audio_ques_close.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView)alert_on_audio_ques_close.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    audio_close.setEnabled(true);
                                    quit_msg_showing = false;
                                    alert_on_audio_ques_close.dismiss();
                                    alert_on_close = false;

                              //      changeQues();
                                    if (!ismediaplayimg) {

                                    } else {
                                        try {
                                            mp.seekTo(mplength);
                                            mp.start();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        audio_close.setEnabled(true);

                                        alert_on_audio_ques_close.dismiss();
                                        alert_on_close = false;
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        audio_close.setEnabled(true);

                                        alert_on_audio_ques_close.dismiss();
                                        alert_on_close = false;
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();


                        }
                    });

                    try {
                        alert_on_audio_ques_close.show();
                        alert_on_close = true;
                        quit_msg_showing = true;
                    }catch (Exception e){

                    }
                } else {
                    if (qansr > 0) {
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }


            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close.setEnabled(false);
                if (mp != null && mp.isPlaying()) {
                    try {
                        mp.pause();
                        mplength = mp.getCurrentPosition();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (pauseCount == 0) {

//                    dialog = new Dialog(MAQ.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialog.setContentView(R.layout.pause_popup);
//                    Window window = dialog.getWindow();
//                    WindowManager.LayoutParams wlp = window.getAttributes();
//
//                    wlp.gravity = Gravity.CENTER;
//                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//                    window.setAttributes(wlp);
//                    dialog.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
//                    dialog.setCancelable(false);
//                    try {
//                        dialog.show();
//                    }
//                    catch (Exception e){
//                        e.printStackTrace();
//                    }
//                    Button pu = (Button) dialog.findViewById(R.id.resume);
//                    pu.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            dialog.dismiss();
//                            changeQues();
//                            if (!ismediaplayimg) {
//                                vdo_performance_ques_timer = new MyCountDownTimer(secLeft, interval);
//                                vdo_performance_ques_timer.start();
//                            } else {
//                                try {
//                                    mp.seekTo(mplength);
//                                    mp.start();
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            pauseCount++;
//                        }
//                    });
                    //  vdo_performance_ques_timer.cancel();
//                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MAQ.this);
//// ...Irrelevant code for customizing the buttons and title
//                    LayoutInflater inflater = LayoutInflater.from(MAQ.this);
//                    View dialogView = inflater.inflate(R.layout.quit_warning, null);
//                    dialogBuilder.setView(dialogView);
//
//
//                    alertDialog = dialogBuilder.create();
//
//                    alertDialog.setCancelable(false);
//                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//
//                    alertDialog.show();
//
//
//                    final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
//                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);
                    alert_on_audio_ques_close = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alert_on_audio_ques_close.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert_on_audio_ques_close.setContentView(R.layout.assess_quit_warning);
                    Window window = alert_on_audio_ques_close.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alert_on_audio_ques_close.setCancelable(false);

                    alert_on_audio_ques_close.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alert_on_audio_ques_close.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView) alert_on_audio_ques_close.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alert_on_audio_ques_close.findViewById(R.id.alien);
                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();



                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();
                    }

                    final ImageView no = (ImageView)alert_on_audio_ques_close.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView)alert_on_audio_ques_close.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);

                                    alert_on_audio_ques_close.dismiss();
                                    alert_on_close = false;
                                    quit_msg_showing = false;
                                    //changeQues();
                                    if (!ismediaplayimg) {

                                    } else {
                                        try {
                                            mp.seekTo(mplength);
                                            mp.start();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        close.setEnabled(true);

                                        alert_on_audio_ques_close.dismiss();
                                        alert_on_close = false;
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        close.setEnabled(true);

                                        alert_on_audio_ques_close.dismiss();
                                        alert_on_close = false;
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();


                        }
                    });

                    try {
                        alert_on_audio_ques_close.show();
                        quit_msg_showing = true;
                        alert_on_close = true;
                    }catch (Exception e){

                    }
                } else {
                    if (qansr > 0) {
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }


            }
        });

        new CountDownTimer(100,100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                //t923.cancel(true);

                //new changes remove show dialogue call and add it into Getquestions
                ConnectionUtils connectionUtils=new ConnectionUtils(MAQ.this);
                if (connectionUtils.isConnectionAvailable()) {
                    //  flag_netcheck = false;

                    if(flaggameattempfirst){
                        new GetQues().execute();
                    }else {
                        loadofflinedata();
                    }

                }else {
                    try {
                        //  qs_progress.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    //  flag_netcheck=false;
                    Toast.makeText(MAQ.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }.start();

        //  checkfacedetection();
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close_on_get_ques = true;
                try{
                    new GetQues().cancel(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Intent i = new Intent(MAQ.this, HomeActivity.class );
                Log.e("navigation","ass");
                new PrefManager(MAQ.this).set_back_nav_from("ass_land");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tabs","normal");
                startActivity(i);
                finish();

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            }
        });

    }

    private void playSound() {
        play_btn.setEnabled(false);

        play_btn.setVisibility(View.GONE);
        playing_btn.setVisibility(View.VISIBLE);
      /*  try {
            countDownTimer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        try {
            // mp.setDataSource(AudioGame.this, Uri.parse(audiourl[acq]));
            //  mp.setDataSource(audiourl[acq]);
            //probability

            if(islevelingenable){
                Log.e("indexis","1  "+index);
                Log.e("indexis","1  "+assets.get(index));
                String[] pic = assets.get(index).split("/");

                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                File audio_file = new File(folder, pic[pic.length - 1]);
                Log.e("pathis", audio_file
                        .getPath());
                mp.reset();
//                 Uri videouri = Uri.parse(audio_file.getPath());
//                 videoView.setVideoURI(videouri);
//               //  videoView.setVideoPath(assets[mediaIndex]);
                //    FileInputStream is = new FileInputStream(audio_file);
                //   FileDescriptor fd = is.getFD();

                //   mp.setDataSource(assets[mediaIndex]);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);

                mp.setDataSource(audio_file.getPath());
                mp.prepareAsync();


                setupVisualizerFxAndUI();

                mVisualizer.setEnabled(true);
            }else {
                Log.e("indexis","1  "+mediaIndex);
                Log.e("indexis","1  "+assets.get(mediaIndex));
                String[] pic = assets.get(mediaIndex).split("/");

                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                File audio_file = new File(folder, pic[pic.length - 1]);
                Log.e("pathis", audio_file
                        .getPath());
                mp.reset();
//                 Uri videouri = Uri.parse(audio_file.getPath());
//                 videoView.setVideoURI(videouri);
//               //  videoView.setVideoPath(assets[mediaIndex]);
                //    FileInputStream is = new FileInputStream(audio_file);
                //   FileDescriptor fd = is.getFD();

                //   mp.setDataSource(assets[mediaIndex]);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);

                mp.setDataSource(audio_file.getPath());
                mp.prepareAsync();


                setupVisualizerFxAndUI();

                mVisualizer.setEnabled(true);
            }



        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                try {
                    mp.start();
                    ismediaplayimg = true;
                    play_btn.setImageResource(R.drawable.playing_audio);
                    play_btn.setEnabled(false);


                } catch (Exception e) {

                }
            }
        });
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                ismediaplayimg = false;
                mplength = 0;

                rel_popup.setVisibility(View.GONE);
                maq_ques.setVisibility(View.VISIBLE);
              //  astro_maq.setVisibility(View.VISIBLE);
                play_btn.setVisibility(View.VISIBLE);
                playing_btn.setVisibility(View.GONE);
                close.setVisibility(View.VISIBLE);

                try {
                    startTime= Long.parseLong(qsmodel.getTime());
                    startTime=startTime*1000;
                    Log.e("timeris",qsmodel.getTime());
                    countDownTimer = new MyCountDownTimer(startTime, interval);
                    startTimer();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                blurflag = false;
                optionAdapterNonClikable.notifyDataSetChanged();

                play_btn.setEnabled(false);
                mVisualizer.setEnabled(false);
                play_btn.setImageResource(R.drawable.maq_play);
                play_btn.setEnabled(true);
                //  doblur= false;
                //  blurDrawable = new BlurDrawable(ques_frame, 0);

                // ques_frame.setForeground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.blurimgsec));
                //ques_frame.setForeground(blurDrawable);
                flagdone = true;


            }

        });
    }


    /**
     * checking  permissions at Runtime.
     */

    private void setupVisualizerFxAndUI() {

        // Create the Visualizer object and attach it to our media player.
        mVisualizer = new Visualizer(mp.getAudioSessionId());
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        mVisualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    @SuppressLint("ClickableViewAccessibility")

    private void createFolder() {
        folder = new File(/*Environment.getExternalStorageDirectory()*/getFilesDir() +
                File.separator + "SimulationData");
        boolean success = true;
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    public void clickMethods(final int position) {
        last_tick = true;
        flag_click = true;
        try {
            countDownTimer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        // rc_options_non_clickable.addOnItemTouchListener(disabler);
        //  optionAdapterNonClikable.notifyDataSetChanged();
        //if count is equals to question count

        new CountDownTimer(10, 10) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                anim_limit=qc+1;

                if(islevelingenable) {
                    //if count is equals to question count
                    if (anim_limit == q_count) {
                        giveAnswers(position);
                    } else {
                        //put dta in temp list
                   /* temp_ques_layout_window.setVisibility(View.VISIBLE);
                    animListdata();
                    giveAnswers(position);
                    main_ques_layout_window.startAnimation(ques_anim_left);
                    Log.e("animstatus","putdata");
                    temp_ques_layout_window.startAnimation(ques_anim);
                    hideArrow();*/
                        giveAnswers(position);


                    }
                }else {

                    int cis=0;
                    if(isques_limit_exceed){
                        cis=int_qs_size;
                    }else {
                        cis=q_count;
                    }
                    //no leveling
                    if(anim_limit==cis) {
                        giveAnswers(position);
                    }else {
                        //put dta in temp list
                   /* temp_ques_layout_window.setVisibility(View.VISIBLE);
                    animListdata();
                    giveAnswers(position);
                    main_ques_layout_window.startAnimation(ques_anim_left);
                    Log.e("animstatus","putdata");
                    temp_ques_layout_window.startAnimation(ques_anim);
                    hideArrow();*/
                        giveAnswers(position);


                    }
                }
            }
        }.start();



    }


    private void animListdata() {
        if (anim_limit < qs.size()) {
            qsmodel = qs.get(anim_limit);


            qno.setText(anim_limit + 1 + "/" + qs.size());
            try {
                //single question

                optioncard = new ArrayList<>();
                optioncard.add("Q. " + qsmodel.getQuestion());
                if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                    String s1 = qsmodel.getOpt1();
                    s1 = s1.trim();
                    optioncard.add("" + s1);
                }
                if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                    String s2 = qsmodel.getOpt2();

                    s2 = s2.trim();
                    optioncard.add("" + s2);
                }
                if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                    String s3 = qsmodel.getOpt3();
                    s3 = s3.trim();
                    optioncard.add("" + s3);
                }
                if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                    String s4 = qsmodel.getOpt4();
                    s4 = s4.trim();
                    optioncard.add("" + s4);
                }
                if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                    String s5 = qsmodel.getOpt5();
                    s5 = s5.trim();
                    optioncard.add("" + s5);
                }

                Log.e("animstatus", "putdata");
                String[] options_array = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"};
                int no_options = optioncard.size();
                String[] confirm_options = new String[no_options - 1];

                for (int i = 0; i < no_options - 1; i++) {
                    confirm_options[i] = options_array[i];
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private void giveAnswers(int position) {

        paraques++;

        c = Calendar.getInstance();
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //  now = df1.format(c.getTime()).toString();
        no_of_ques_in_list++;
        no_of_current_ques_per_para++;
        if (position == 0) {
            if (crct == 0) {
                correctques++;

                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("A");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()));
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);

                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("A");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

            }
        }

        if (position == 1) {
            if (crct == 1) {
                correctques++;

                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("B");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);

                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("B");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if (position == 2) {
            if (crct == 2) {
                correctques++;

                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("C");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);

                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("C");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {


                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if (position == 3) {
            if (crct == 3) {
                correctques++;

                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("D");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);

                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("D");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if (position == 4) {
            if (crct == 4) {
                correctques++;

                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("E");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

            } else {
                vibrator.vibrate(500);

                Handler han = new Handler();

                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("E");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }
        if (position == 1000) {
            vibrator.vibrate(500);

            Handler han = new Handler();
            han.postDelayed(new Runnable() {
                @Override
                public void run() {

                    try {
                        qidj.add(qsmodel.getQ_id());
                        qtid.add(qsmodel.getQT_id());
                        qid_array.add(qsmodel.getQ_id());
                        ansj.add(" ");
                        stj.add(now);
                        endj.add(df1.format(c.getTime()).toString());
                        qansr++;
                        if(islevelingenable){
                            qs.remove(qc_index);

                        }
                        formdatalocal(qc,1);
                        qc++;
                    } catch (Exception exx) {

                    }
                    changeQues();
                }
            }, 1000);


        }


    }


    public int getPixel(ListeningGameActivity simulationActivity, int dps) {
        Resources r = simulationActivity.getResources();

        int px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }


    public void showGetQuesAgainDialogue() {
        Log.e("respmsg","get ques dialogue");



        alert_on_retry = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alert_on_retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert_on_retry.setContentView(R.layout.retry_anim_dialogue);
        Window window = alert_on_retry.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alert_on_retry.setCancelable(false);

        alert_on_retry.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alert_on_retry.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alert_on_retry.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alert_on_retry.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alert_on_retry.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);


                if (alert_on_retry.isShowing() && alert_on_retry != null) {
                    alert_on_retry.dismiss();

                }
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();

                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

//                        alert_on_retry.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(MAQ.this);
                        if (connectionUtils.isConnectionAvailable()) {
                            new GetQues().execute();
                        } else {

                            quesLoading.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (quesLoading.isShowing() && quesLoading != null) {
                                        quesLoading.dismiss();


                                    }

                                    Log.e("connection not avlble"," show");
                                    showGetQuesAgainDialogue();
                                }
                            }.start();
                        }

                    }
                }.start();




            }
        });
        try {
            if(!alert_on_retry.isShowing())
                alert_on_retry.show();
        }
        catch (Exception e){

            e.printStackTrace();
        }
    }

    @Override
    public void OnDownloadStarted(long taskId) {

    }

    @Override
    public void OnDownloadPaused(long taskId) {

    }

    @Override
    public void onDownloadProcess(long taskId, double percent, long downloadedLength) {

    }

    @Override
    public void OnDownloadFinished(long taskId) {
        img_count = img_count - 1;
        if (img_count == 0) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startTime = questions.length() * 30 * 1000;
                    changeParagraph(-1);
                    // startTimer();

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

                        startPlanetAnimation();

                    }
                   startVerification();
                    //  checkfacedetection();
                    try {
                        if (quesLoading.isShowing()) {
                            quesLoading.dismiss();
                        }
                    } catch (Exception e) {

                    }
                }
            });


        }
    }

    @Override
    public void OnDownloadRebuildStart(long taskId) {
    Log.e("status","rebuild");
    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {
        Log.e("status","rebuildfinish");

    }

    @Override
    public void OnDownloadCompleted(long taskId) {

    }

    @Override
    public void connectionLost(long taskId) {

        Log.e("lost", "lost");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                qs.clear();
                try {
                    if (quesLoading.isShowing()) {
                        quesLoading.dismiss();
                    }
                } catch (Exception e) {

                }
                try {
                    if (alert_on_retry.isShowing()) {

                    } else {
                        Log.e("connection lost try"," show");
                        showGetQuesAgainDialogue();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e("connection lost catch"," show");

                    showGetQuesAgainDialogue();
                }

            }
        });
    }



    public class MyCountDownTimer extends CountDownTimer


    {


        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }

        @Override
        public void onTick(long millisUntilFinished) {

            timerrr.setText("" + millisUntilFinished / 1000);
            secLeft = millisUntilFinished;
            long sec_lef = millisUntilFinished/1000;

            int sec= (int) (fulltime/1000-secLeft/1000);
            Log.e("sec",String.valueOf(sec));

            // timer_seek.setMax((float)(fulltime-1)/1000);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    float s=Float.parseFloat(String.valueOf(fulltime/1000-secLeft/1000));
                    // float divs= (float) (s/2.5);
                    //  timer_seek.setProgress(s);
                    Log.e("pris",String.valueOf(s));

                }
            });
            final int MINUTES_IN_AN_HOUR = 60;
            final int SECONDS_IN_A_MINUTE = 60;



            int minutes =  (sec / SECONDS_IN_A_MINUTE);
            sec -= minutes * SECONDS_IN_A_MINUTE;

            int hours = minutes / MINUTES_IN_AN_HOUR;
            minutes -= hours * MINUTES_IN_AN_HOUR;
            if(sec_lef<=5){
                timerrr.setTextColor(Color.parseColor("#ff0000"));
                timer_img.setBackgroundResource(R.drawable.msq_red_timer);

                //timer_img.startAnimation(blinking_anim);
               // timerrr.startAnimation(blinking_anim);
            }

            if( sec_lef==1){
               // blinking_anim.cancel();
                timerrr.setVisibility(View.VISIBLE);
                timer_img.setVisibility(View.VISIBLE);
            }
        }


        @Override

        public void onFinish() {

            // blinking_anim.cancel();
            if(islevelingenable){
                if (qc < q_count) {

                    clickMethods(1000);

                } else {

                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }
            }else {
                int cis=0;
                if(isques_limit_exceed){
                    cis=int_qs_size;
                }else {
                    cis=q_count;
                }

                if (qc < cis) {

                    clickMethods(1000);

                } else {

                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }

            }





        }
    }

    private class GetQues extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            img_count=0;
            try {
                if((quesLoading!=null)&&(!quesLoading.isShowing()))
                    quesLoading.show();
            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                countDownTimer.cancel();
            }catch (Exception e){

            }
        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {
                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/GetQuestions");*/

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("CE_Id", ceid)
                            .object("Game", game)
                            .object("Category", cate)
                            .object("GD_Id", gdid)
                            .build().toString();


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/v1/GetQuestions");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();




                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alert_on_retry.isShowing()) {

                                } else {
                                    Log.e("UnsupportedExceptry"," show");

                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                Log.e("UnsupportedExcepcatch"," show");

                                showGetQuesAgainDialogue();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alert_on_retry.isShowing()) {

                                } else {
                                    Log.e("ClientProtExceptry"," show");

                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                Log.e("ClientProtoExcepcatch"," show");

                                showGetQuesAgainDialogue();
                            }
                        }
                    });
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alert_on_retry.isShowing()) {

                                } else {
                                    Log.e("IOException try"," show");

                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                Log.e("IOException catch"," show");

                                showGetQuesAgainDialogue();
                            }
                        }
                    });
                    e.printStackTrace();
                }

                String responseString = readStream(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result1 =responseString;

            } catch (Exception e) {
                e.getMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        qs.clear();
                        try {
                            if (quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                        }catch (Exception e){

                        }
                        try {
                            if (alert_on_retry.isShowing()) {

                            } else {
                                Log.e("exception try"," show");

                                showGetQuesAgainDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("exception catch"," show");

                            showGetQuesAgainDialogue();
                        }
                    }
                });
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            return result1;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            jsonMakeFile(cate+game+gdid+ceid+uidd+tgid,result);

            try {
                JSONArray data = new JSONArray(result);
                Log.e("QUESDATA", data.length() + "");
                // JSONArray sidd = data.getJSONArray(0);
                final JSONObject sidobj = data.getJSONObject(0);
                sid = sidobj.getString("S_Id");
                q_count=sidobj.getInt("question_count");
                currentlevel=sidobj.getInt("currentGameLevel");
                maxlevel=sidobj.getInt("max_level");
                min_level=sidobj.getInt("min_level");
                if(currentlevel<min_level){
                    currentlevel=min_level;
                }
                if(currentlevel>maxlevel){
                    currentlevel=min_level;
                }
                levelis=currentlevel;

                JSONArray js1 = data.getJSONArray(1);



                for(int i=0;i<js1.length();i++){
                    JSONArray jsnew=js1.getJSONArray(i);
                    for(int j=0;j<jsnew.length();j++){
                        JSONObject jobjis=jsnew.getJSONObject(j);
                        audios.put(jobjis);
                    }

                }
                Log.e("adddddd", audios.length() + "");

                islevelingenable=true;
                para_audio_count = audios.length();
                Log.e("para_image_count", para_audio_count + "");



                for (int i = 0; i < audios.length(); i++) {

                    assets.add(i,"");
                    JSONObject aaqonj = audios.getJSONObject(i);
                    asset_id.add(i,aaqonj.getInt("QT_Id"));
                    String aaq = aaqonj.getString("Q_ChildQIds");
                    if( aaqonj.getString("Q_Asset").equalsIgnoreCase(""))
                    {

                    }else {
                        assets.set(i,aaqonj.getString("Q_Asset"));
                        img_count++;
                        String [] pic=aaqonj.getString("Q_Asset").split("/");
                        String last=pic[pic.length-1];
                        String [] lastname=last.split("\\.");
                        String name=lastname[0];
                        downloadData(aaqonj.getString("Q_Asset"),name);
                    }
                    levels.add(i,aaqonj.getInt("GL_Level"));
                    StringTokenizer st = new StringTokenizer(aaq, ",");
                    audio.add(i,st.countTokens());

                }
                im_temp=img_count;



                for(int i=0;i<levels.size();i++){
                    Temp_model temp_model=new Temp_model();
                    temp_model.setLevel(levels.get(i));
                    temp_model.setCount(audio.get(i));
                    temp_list.add(temp_model);
                }


                try {
                    int temp_count;
                    ArrayList<Integer> level_ques_count = new ArrayList<>();
                    for (int i = 0; i <= maxlevel; i++) {
                        level_ques_count.add(i, 0);
                    }

                    for (int i = min_level; i <= maxlevel; i++) {
                        level_ques_count.set(i, 0);
                    }


                    for (int i = min_level; i <= maxlevel; i++) {
                        temp_count = 0;
                        for (int j = 0; j < temp_list.size(); j++) {
                            Temp_model temp_model = temp_list.get(j);
                            if (i == temp_model.getLevel()) {
                                temp_count = temp_count + temp_model.getCount();
                            }
                            level_ques_count.set(i, temp_count);
                        }
                    }

                    for (int i = min_level; i <= maxlevel; i++) {
                        Log.e("valueis", "" + level_ques_count.get(i));
                        if (level_ques_count.get(i) == q_count) {
                            islevelingenable = true;
                        } else {
                            islevelingenable = false;
                        }
                    }

                }catch (Exception e){
                    islevelingenable = false;
                    e.printStackTrace();
                }


                questions = data.getJSONArray(2);
                for(int i=0;i<questions.length();i++){
                    JSONObject js=questions.getJSONObject(i);
                    QuestionModel s=new QuestionModel();
                    s.setQT_id(js.getString("QT_Id"));
                    s.setQ_id(js.getString("Q_Id"));
                    s.setQuestion(js.getString("Q_Question"));
                    s.setOpt1(js.optString("Q_Option1"));
                    s.setOpt2(js.optString("Q_Option2"));
                    s.setOpt3(js.optString("Q_Option3"));
                    s.setOpt4(js.optString("Q_Option4"));
                    s.setOpt5(js.optString("Q_Option5"));
                    s.setCorrectanswer(js.getString("Q_Answer"));
                    s.setTime(js.getString("Q_MaxTime"));
                    Log.e("QUESTIME", js.getString("Q_MaxTime"));
                    qs.add(s);
                }

                int_qs_size=qs.size();
                if(q_count>qs.size()){
                    isques_limit_exceed=true;
                }
                Log.e("QUES", questions.length() + "");
                tq = questions.length();
                secLeft=startTime;
                final int MINUTES_IN_AN_HOUR = 60;
                final int SECONDS_IN_A_MINUTE = 60;

                int sec= (int) secLeft/1000;
                int minutes =  (sec / SECONDS_IN_A_MINUTE);
                sec -= minutes * SECONDS_IN_A_MINUTE;

                int hours = minutes / MINUTES_IN_AN_HOUR;
                minutes -= hours * MINUTES_IN_AN_HOUR;
                fulltime= (int) secLeft;
                // timer_seek.setMin(0);
                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if(im_temp>0){

                        }else {
                            startTime = questions.length() * 30 * 1000;
                            changeParagraph(-1);
                            // startTimer();
                            quesLoading.dismiss();
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


                                startPlanetAnimation();
                            }
                                 startVerification();
                            // checkfacedetection();
                        }
                    }
                }.start();


            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        qs.clear();
                        try {
                            if (quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                        }catch (Exception e){

                        }
                        try {
                            if (alert_on_retry.isShowing()) {

                            } else {
                                Log.e("postexception try"," show");

                                showGetQuesAgainDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("postexception catch"," show");

                            showGetQuesAgainDialogue();
                        }
                    }
                });
                Log.e("QUES", "Error parsing data " + e.toString());
            }




        }
    }

//    private class GetQues extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//
//            super.onPreExecute();
//            img_count=0;
//            try {
//                if(quesLoading.isShowing()){
//
//                }else {
//                    quesLoading.show();
//                }
//            }catch (Exception e){
//
//            }
//
//        }
//
//        ///Authorization
//        @Override
//        protected String doInBackground(String... urlkk) {
//            String result1 = "";
//            try {
//
//
//             /*   try {
//                   *//* httpClient = new DefaultHttpClient();
//                    httpPost = new HttpPost("http://35.154.93.176/Player/GetQuestions");*//*
//
//                    String jn = new JsonBuilder(new GsonAdapter())
//                            .object("data")
//                            .object("U_Id", uidd)
//                            .object("CE_Id", ceid)
//                            .object("Game", game)
//                            .object("Category", cate)
//                            .object("GD_Id", gdid)
//                            .build().toString();
//                   *//* StringEntity se = new StringEntity(jn.toString());
//                    Log.e("Reqt", jn + "");
//                    Log.e("Request", se + "");
//                    httpPost.addHeader("Authorization", "Bearer " + token);
//                    httpPost.setHeader("Content-Type", "application/json");
//                    httpPost.setEntity(se);
//                    HttpResponse httpResponse = httpClient.execute(httpPost);
//                    HttpEntity httpEntity = httpResponse.getEntity();
//                    is = httpEntity.getContent();
//                    Log.e("FKJ", httpResponse + "CHARAN" + is);*//*
//
//
//                    URL urlToRequest = new URL("http://35.154.93.176/Player/GetQuestions");
//                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
//                    urlConnection.setDoOutput(true);
//                    urlConnection.setFixedLengthStreamingMode(
//                            jn.getBytes().length);
//                    urlConnection.setRequestProperty("Content-Type", "application/json");
//                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
//                    urlConnection.setRequestMethod("POST");
//
//
//                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
//                    wr.write(jn);
//                    wr.flush();
//                    is = urlConnection.getInputStream();
//
//
//
//
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                } catch (ClientProtocolException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                String responseString = readStream(urlConnection.getInputStream());
//                Log.e("Response", responseString);
//                result1 = responseString;
//             */    result1="[{\"U_Id\":65776,\"S_GameState\":\"InProgress\",\"Category\":93,\"CE_Id\":0,\"S_WebSessionId\":\"5Fpgnyj8HVFWqNJ3R2J7VHo8XgY99AFeeieDrMTY\",\"CREATED_AT\":\"2018-03-10 23:53:17\",\"S_Id\":94029},[{\"Q_Id\":12869,\"QT_Id\":3317,\"Q_ChildQIds\":\"12864,12865,12866,12867,12868\",\"Q_Asset\":\"https://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3\"},{\"Q_Id\":12869,\"QT_Id\":3317,\"Q_ChildQIds\":\"12854,12855,12856,12857,1288\",\"Q_Asset\":\"https://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3\"}],[{\"QT_Id\":3317,\"Q_Id\":12864,\"Q_Question\":\"question 1\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12865,\"Q_Question\":\"question 2ai9235772956298yt928yyt9246t92uhr9c7y845y97c25y9\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12866,\"Q_Question\":\"question 3ai9235772956298yt928yyt9246t92uhr9c7y845y97c25y9\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12867,\"Q_Question\":\"question 4\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12868,\"Q_Question\":\"question 5\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12854,\"Q_Question\":\"question 6\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12855,\"Q_Question\":\"question 7\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12856,\"Q_Question\":\"question 8\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12857,\"Q_Question\":\"question 9\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10},{\"QT_Id\":3317,\"Q_Id\":12858,\"Q_Question\":\"question 10\",\"Q_Option1\":\"Ans1\",\"Q_Option2\":\"Ans2\",\"Q_Option3\":\"Ans3\",\"Q_Option4\":\"Ans4\",\"Q_Option5\":\"Ans5\",\"Q_Answer\":\"A\",\"Q_MaxTime\":10}]]\n";
//
//                try {
//                    JSONArray data = new JSONArray(result1);
//                    Log.e("QUESDATA", data.length() + "");
//                    // JSONArray sidd = data.getJSONArray(0);
//                    final JSONObject sidobj = data.getJSONObject(0);
//                    sid = sidobj.getString("S_Id");
//                    Log.e("fbwwp", sid);
//                    audios = data.getJSONArray(1);
//                    Log.e("adddddd", audios.length() + "");
//                    for (int i = 0; i < audios.length(); i++) {
//
//                        assets[i] = "";
//                        JSONObject aaqonj = audios.getJSONObject(i);
//                        String aaq = aaqonj.getString("Q_ChildQIds");
//                        if( aaqonj.getString("Q_Asset").equalsIgnoreCase(""))
//                        {
//
//                        }else {
//                            assets[i] = aaqonj.getString("Q_Asset");
//                            img_count++;
//                            String [] pic=aaqonj.getString("Q_Asset").split("/");
//                            String last=pic[pic.length-1];
//                            String [] lastname=last.split("\\.");
//                            String name=lastname[0];
//                            downloadData(aaqonj.getString("Q_Asset"),name);
//                        }
//                        StringTokenizer st = new StringTokenizer(aaq, ",");
//                        audio[i] = st.countTokens();
//
//                    }
//                    questions = data.getJSONArray(2);
//                    for(int i=0;i<questions.length();i++){
//                        JSONObject js=questions.getJSONObject(i);
//                        QuestionModel s=new QuestionModel();
//                        s.setQT_id(js.getString("QT_Id"));
//                        s.setQ_id(js.getString("Q_Id"));
//                        s.setQuestion(js.getString("Q_Question"));
//                        s.setOpt1(js.optString("Q_Option1"));
//                        s.setOpt2(js.optString("Q_Option2"));
//                        s.setOpt3(js.optString("Q_Option3"));
//                        s.setOpt4(js.optString("Q_Option4"));
//                        s.setOpt5(js.optString("Q_Option5"));
//                        s.setCorrectanswer(js.getString("Q_Answer"));
//                        s.setTime(js.getString("Q_MaxTime"));
//                        qs.add(s);
//                    }
//                    Log.e("QUES", questions.length() + "");
//                    tq = questions.length();
//
//                } catch (JSONException e) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            qs.clear();
//                            try {
//                                if (quesLoading.isShowing()) {
//                                    quesLoading.dismiss();
//                                }
//                            }catch (Exception e){
//
//                            }
//                            showGetQuesAgainDialogue();
//
//                        }
//                    });
//                    Log.e("QUES", "Error parsing data " + e.toString());
//                }
//
//            } catch (Exception e) {
//                e.getMessage();
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        qs.clear();
//                        try {
//                            if (quesLoading.isShowing()) {
//                                quesLoading.dismiss();
//                            }
//                        }catch (Exception e){
//
//                        }
//                        showGetQuesAgainDialogue();
//
//                    }
//                });
//                Log.e("Buffer Error", "Error converting result " + e.toString());
//            }
//            return null;
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//          /*  dialoge = new Dialog(MasterSlaveGame.this, android.R.style.Theme_Translucent_NoTitleBar);
//            dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialoge.setContentView(R.layout.game_popup);
//            Window window = dialoge.getWindow();
//            WindowManager.LayoutParams wlp = window.getAttributes();
//
//            wlp.gravity = Gravity.CENTER;
//            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//            window.setAttributes(wlp);
//            dialoge.getWindow().setLayout(FlowLayout.LayoutParams.MATCH_PARENT, FlowLayout.LayoutParams.MATCH_PARENT);
//            dialoge.show();*/
//
//            if(img_count>0){
//
//            }else {
//                startTime = questions.length() * 30 * 1000;
//                changeParagraph(-1);
//                startTimer();
//                quesLoading.dismiss();
//            }
//        }
//    }


    void startTimer() {
        countDownTimer.start();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void changeParagraph(int mm) {
        acq=mm;
        para_called_count++;
        para_count++;

        Log.e("para called",""+para_called_count);

        // doblur = media_frame.isEnabled();

        //ques_frame.setVisibility(View.GONE);

        //   blurDrawable1 = new BlurDrawable(ques_frame, 6);
        //  ques_frame.setForeground(blurDrawable1);

        try {

            countDownTimer.cancel();
            // countDownTimer=null;
        }
        catch (Exception e){

            e.printStackTrace();
        }
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
        mediaIndex=acq;
        Log.e("qc is",String.valueOf(qc));
        if (acq == -1) {
            changeParagraph(0);
            changeparaflag=true;
            changeQues();

            return;
        }else {


            if(islevelingenable){

                if(paragraph_count>0 || paragraph_count == para_audio_count-1) {
                    Log.e("changeparacalled", "" + para_count);

                    if (quit_msg_showing) {

                    } else {
                        try {
                            countDownTimer.cancel();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        int i = para_count - 2;
                        String message = "";
                        String complete_msg = "";
                        String total_count = "";

                        switch (para_audio_count) {

                            case 1:

                                total_count = "out of one audio";
                                break;

                            case 2:

                                total_count = "out of two audios";
                                break;

                            case 3:

                                total_count = "out of three audios";
                                break;

                            case 4:

                                total_count = "out of four audios";
                                break;
                            case 5:

                                total_count = "out of five audios";
                                break;
                            case 6:

                                total_count = "out of six audios";
                                break;

                            case 7:

                                total_count = "out of seven audios";
                                break;
                            case 8:

                                total_count = "out of eight audios";
                                break;
                            case 9:

                                total_count = "out of nine audios";
                                break;
                            case 10:

                                total_count = "out of ten audios";
                                break;
                            case 11:

                                total_count = "out of eleven audios";
                                break;
                            case 12:

                                total_count = "out of twelve audios";
                                break;
                            case 13:

                                total_count = "out of thirteen audios";
                                break;
                            case 14:

                                total_count = "out of fourteen audios";
                                break;
                            case 15:

                                total_count = "out of fifteen audios";
                                break;
                            case 16:

                                total_count = "out of sixteen audios";
                                break;
                            case 17:

                                total_count = "out of seventeen audios";
                                break;
                            case 18:

                                total_count = "out of eighteen audios";
                                break;
                            case 19:

                                total_count = "out of nineteen audios";
                                break;
                            case 20:

                                total_count = "out of twenty audios";
                                break;
                        }

                        switch (paragraph_count) {

                            case 1:
                                message = "You have completed your first master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 2:
                                message = "You have completed your second master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 3:
                                message = "You have completed your third master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 4:
                                message = "You have completed your fourth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 5:
                                message = "You have completed your fifth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 6:
                                message = "You have completed your sixth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 7:
                                message = "You have completed your seventh master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 8:
                                message = "You have completed your eighth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 9:
                                message = "You have completed your ninth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 10:
                                message = "You have completed your tenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 11:
                                message = "You have completed your eleventh master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 12:
                                message = "You have completed your twelveth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 13:
                                message = "You have completed your thirteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 14:
                                message = "You have completed your fourteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 15:
                                message = "You have completed your fifteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 16:
                                message = "You have completed your sixteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 17:
                                message = "You have completed your seventeenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 18:
                                message = "You have completed your eighteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 19:
                                message = "You have completed your nineteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 20:
                                message = "You have completed your twentieth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                        }


//            showProceedDialog("You have completed your first audio questions");
                    }
                }
                paragraph_count++;

                //changing

                if (firstpara > 0) {
                    Log.e("para called","into more than one");
                    assets.remove(index);
                    levels.remove(index);
                    audio.remove(index);
                    asset_id.remove(index);
                    if (paraques == correctques) {
                        Log.e("stis","all correct");
                        if (levelis < maxlevel) {
                            levelis++;
                        }
                    }else {
                        Log.e("stis","all not correct");

                        if (correctques == 0) {
                            Log.e("stis","no one correct");
                            Log.e("stis",""+levelis+"  "+currentlevel);
                            if (levelis > min_level) {
                                Log.e("stis","level down");
                                levelis--;
                            }
                        }else {
                            Log.e("stis","few correct");

                            levelis = levelis;
                        }

                    }
                    /*else {
                        levelis = levelis;
                    }*/

                }
                paraques = 0;
                correctques = 0;
                for (int i = 0; i < assets.size(); i++) {
                    if (levelis == levels.get(i)) {
                        index = i;
                        break;
                    }
                }
                Log.e("indexiss",""+index);

                firstpara++;



                play_btn.setEnabled(true);
                play_btn.setImageResource(R.drawable.maq_play);
                flagdone=false;
//            mVisualizer.setEnabled(true);

                blurflag=true;
                Log.e("acq", String.valueOf(acq));
                maq_ques.setVisibility(View.GONE);
                //   close.setVisibility(View.GONE);

                astro_maq.setVisibility(View.GONE);
                rel_popup.setVisibility(View.VISIBLE);


                current_audio_count++;
                Log.e("current audio", ""+ current_audio_count +"/"+para_audio_count);

                current_audio_text.setText("Audio "+current_audio_count);
                no_of_current_ques_per_para++;
                Log.e("no_of_current_quespara",""+no_of_current_ques_per_para);

            }else {


                if (paragraph_count > 0 || paragraph_count == para_audio_count - 1) {
                    Log.e("changeparacalled", "" + para_count);

                    if (quit_msg_showing) {

                    } else {
                        try {
                            countDownTimer.cancel();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        int i = para_count - 2;
                        String message = "";
                        String complete_msg = "";
                        String total_count = "";

                        switch (para_audio_count) {

                            case 1:

                                total_count = "out of one audio";
                                break;

                            case 2:

                                total_count = "out of two audios";
                                break;

                            case 3:

                                total_count = "out of three audios";
                                break;

                            case 4:

                                total_count = "out of four audios";
                                break;
                            case 5:

                                total_count = "out of five audios";
                                break;
                            case 6:

                                total_count = "out of six audios";
                                break;

                            case 7:

                                total_count = "out of seven audios";
                                break;
                            case 8:

                                total_count = "out of eight audios";
                                break;
                            case 9:

                                total_count = "out of nine audios";
                                break;
                            case 10:

                                total_count = "out of ten audios";
                                break;
                            case 11:

                                total_count = "out of eleven audios";
                                break;
                            case 12:

                                total_count = "out of twelve audios";
                                break;
                            case 13:

                                total_count = "out of thirteen audios";
                                break;
                            case 14:

                                total_count = "out of fourteen audios";
                                break;
                            case 15:

                                total_count = "out of fifteen audios";
                                break;
                            case 16:

                                total_count = "out of sixteen audios";
                                break;
                            case 17:

                                total_count = "out of seventeen audios";
                                break;
                            case 18:

                                total_count = "out of eighteen audios";
                                break;
                            case 19:

                                total_count = "out of nineteen audios";
                                break;
                            case 20:

                                total_count = "out of twenty audios";
                                break;
                        }

                        switch (paragraph_count) {

                            case 1:
                                message = "You have completed your first master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 2:
                                message = "You have completed your second master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 3:
                                message = "You have completed your third master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 4:
                                message = "You have completed your fourth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 5:
                                message = "You have completed your fifth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 6:
                                message = "You have completed your sixth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 7:
                                message = "You have completed your seventh master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 8:
                                message = "You have completed your eighth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 9:
                                message = "You have completed your ninth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 10:
                                message = "You have completed your tenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 11:
                                message = "You have completed your eleventh master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 12:
                                message = "You have completed your twelveth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 13:
                                message = "You have completed your thirteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 14:
                                message = "You have completed your fourteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 15:
                                message = "You have completed your fifteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 16:
                                message = "You have completed your sixteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 17:
                                message = "You have completed your seventeenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 18:
                                message = "You have completed your eighteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 19:
                                message = "You have completed your nineteenth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 20:
                                message = "You have completed your twentieth master audio";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                        }


//            showProceedDialog("You have completed your first audio questions");
                    }
                }
                paragraph_count++;

                play_btn.setEnabled(true);
                play_btn.setImageResource(R.drawable.maq_play);
                flagdone = false;
//            mVisualizer.setEnabled(true);

                blurflag = true;
                Log.e("acq", String.valueOf(acq));
                maq_ques.setVisibility(View.GONE);
                //   close.setVisibility(View.GONE);

                astro_maq.setVisibility(View.GONE);
                rel_popup.setVisibility(View.VISIBLE);


                current_audio_count++;
                Log.e("current audio", "" + current_audio_count + "/" + para_audio_count);

                current_audio_text.setText("Audio "+current_audio_count );
                no_of_current_ques_per_para++;
                Log.e("no_of_current_quespara", "" + no_of_current_ques_per_para);
            }
            }
    }

    private void showProceedDialog(String msg) {

        msg="Next Audio Question";
        if(alertDialog!=null && alertDialog.isShowing()){

            try{
                alertDialog.dismiss();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(alert_on_audio_ques_close!=null && alert_on_audio_ques_close.isShowing()){
            try{
                alert_on_audio_ques_close.dismiss();
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();

        try {
            proceed_dialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
            proceed_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            proceed_dialog.setContentView(R.layout.congo_maq);
            Window window = proceed_dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
            proceed_dialog.setCancelable(false);

            proceed_dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);




            final FontTextView yes = (FontTextView) proceed_dialog.findViewById(R.id.yes_quit);

            FontTextView msgs = (FontTextView)proceed_dialog.findViewById(R.id.txt);
            msgs.setText(msg);

            final LinearLayout banner=(LinearLayout) proceed_dialog.findViewById(R.id.banner_level_page);

            banner.setVisibility(View.GONE);

            Animation scaleAnimation = new ScaleAnimation(
                    0f, 1f, // Start and end values for the X axis scaling
                    0f, 1f, // Start and end values for the Y axis scaling
                    Animation.REVERSE, 0.5f, // Pivot point of X scaling
                    Animation.REVERSE, 0.5f);
            scaleAnimation.setDuration(500);


            banner.startAnimation(scaleAnimation);
            banner.setVisibility(View.VISIBLE);

            new CountDownTimer(500, 100) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {

                }
            }.start();




            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    yes.setEnabled(false);

                    Animation scaleAnimation = new ScaleAnimation(
                            1f, 0f, // Start and end values for the X axis scaling
                            1f, 0f, // Start and end values for the Y axis scaling
                            Animation.REVERSE, 0.5f, // Pivot point of X scaling
                            Animation.REVERSE, 0.5f);
                    scaleAnimation.setDuration(500);


                    banner.startAnimation(scaleAnimation);
                    new CountDownTimer(400,400) {


                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            //need


                            close.setEnabled(true);
                            audio_close.setEnabled(true);
                            proceed_dialog_showing = false;
                            proceed_dialog.dismiss();
                           /* try {
                                startTime= Long.parseLong(qsmodel.getTime());
                                startTime=startTime*1000;
                                Log.e("timeris",qsmodel.getTime());
                                countDownTimer = new MyCountDownTimer(startTime, interval);
                                startTimer();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/



                        }
                    }.start();


                }
            });

            try {
                proceed_dialog.show();
                proceed_dialog_showing = true;

            }catch (Exception e){}

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void changeQues() {

        Log.e("mystatus","changeq");
        if(islevelingenable) {

            if (qc < q_count) {


                try {
                //    blinking_anim.cancel();
                    timerrr.setVisibility(View.VISIBLE);
                    timer_img.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                c = Calendar.getInstance();
                df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                now = df1.format(c.getTime()).toString();
//            if(qc==2){
//                takePicture();
//            }
                if (audio.size() > 1) {
                    if (no_of_ques_in_list == audio.get(cnt)) {

                        no_of_current_ques_per_para = 0;
                        changeParagraph(cnt + 1);
                        cnt++;
                        no_of_ques_in_list = 0;


                        // arrow1.performClick();
                        //  ObjectAnimator.ofInt(para_frame, "scrollY",  main_scroller.getTop()).setDuration(10).start();
                    }
                    //no_of_ques_in_list++;
                }


          /*  for(int i=0;i<qid_array.size();i++){
                if(qsmodel.getQ_id().equalsIgnoreCase(qid_array.get(i))){
                    flag_is_present_id=true;
                }
            }*/
                int qt_temp=asset_id.get(index);
                boolean n=false;
                for(int i=0;i<qs.size();i++){
                    QuestionModel qsm=qs.get(i);
                    if(Integer.parseInt(qsm.getQT_id())==qt_temp){
                        qc_index=i;
                        n=true;
                        break;
                    }
                    if(n){
                        break;
                    }
                }


                qsmodel = qs.get(qc_index);

                if (!flag_is_present_id) {

                    if (qc == 0) {
                        // qno.setText(qc + 1 + "/" + qs.size());
                        timerrr.setTextColor(Color.parseColor("#ffffff"));
                        timer_img.setBackgroundResource(R.drawable.msq_timer_img);

                        try {
                            qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                       // Log.e("current_ques.", no_of_current_ques_per_para + "/" + audio.get(cnt));
                    }
                    try {
                        //single question
                        if (flag_click && flagdone) {
                            //     animListdata();

                            Log.e("animstatus", "putdata");
                            // hideArrow();

                        }
                        newquid = qsmodel.getQ_id();
                        optioncard = new ArrayList<>();
                        optioncard.add("Q. " + qsmodel.getQuestion());
                        if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                            String s1 = qsmodel.getOpt1();
                            s1 = s1.trim();
                            optioncard.add("" + s1);
                        }
                        if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                            String s2 = qsmodel.getOpt2();
                            s2 = s2.trim();
                            optioncard.add("" + s2);
                        }
                        if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                            String s3 = qsmodel.getOpt3();
                            s3 = s3.trim();
                            optioncard.add("" + s3);
                        }
                        if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                            String s4 = qsmodel.getOpt4();
                            s4 = s4.trim();
                            optioncard.add("" + s4);
                        }
                        if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                            String s5 = qsmodel.getOpt5();
                            s5 = s5.trim();
                            optioncard.add("" + s5);
                        }
                        String crcans = qsmodel.getCorrectanswer();
                        int crcopt = 0;
                        switch (crcans) {
                            case "A":
                                crcopt = 0;
                                break;
                            case "B":
                                crcopt = 1;
                                break;
                            case "C":
                                crcopt = 2;
                                break;
                            case "D":
                                crcopt = 3;
                                break;
                            case "E":
                                crcopt = 4;
                                break;
                            default:
                                System.out.println("Not in 10, 20 or 30");
                        }
                        crct = crcopt;

                        qid = Integer.parseInt(qsmodel.getQ_id());


                        optionAdapterNonClikable = new Option_clickable(this, optioncard);
                        //rc_options_non_clickable_temp.setAdapter(optionAdapterNonClikable);
                        new CountDownTimer(600, 600) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {

                                rc_options_non_clickable.setAdapter(optionAdapterNonClikable);
//                            fastScroller.attachRecyclerView(rc_options_non_clickable);
//
//                            fastScroller.attachAdapter(optionAdapterNonClikable);
                                final Animation animation_fade = AnimationUtils.loadAnimation(MAQ.this, R.anim.fade_in);
                                rc_options_non_clickable.startAnimation(animation_fade);


                            }
                        }.start();
                        Log.e("timimg", "adapter load");

                        String[] options_array = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"};
                        int no_options = optioncard.size();
                        String[] confirm_options = new String[no_options - 1];

                        for (int i = 0; i < no_options - 1; i++) {
                            confirm_options[i] = options_array[i];
                        }

                        if (flag_click) {
                            new CountDownTimer(700, 700) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    //  temp_ques_layout_window.setVisibility(View.VISIBLE);

                                }

                                @Override
                                public void onFinish() {
                                    //  ques_scrollview.setVerticalScrollBarEnabled(true);
                                    //  rc_options_non_clickable.removeOnItemTouchListener(disabler);
                                    // qno.setText(qc + 1 + "/" + qs.size());
                                    timerrr.setTextColor(Color.parseColor("#ffffff"));

                                    timer_img.setBackgroundResource(R.drawable.msq_timer_img);
                                    try {
                                        qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    if (changeparaflag) {

                                    } else {
                                        startTime = Long.parseLong(qsmodel.getTime());
                                        startTime = startTime * 1000;
                                        Log.e("timeris", qsmodel.getTime());
                                        countDownTimer = new MyCountDownTimer(startTime, interval);
                                        startTimer();

                                    }
                                }
                            }.start();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (changeparaflag) {

                        } else {
                            startTime = Long.parseLong(qsmodel.getTime());
                            startTime = startTime * 1000;
                            Log.e("timeris", qsmodel.getTime());
                            countDownTimer = new MyCountDownTimer(startTime, interval);
                            startTimer();

                        }
                    }


                } else {
                    //Collections.shuffle(qs);
                    //changeQues();
                    //shuffle
                }
                paraques++;
                no_of_ques_in_list++;
                no_of_current_ques_per_para++;
                formdatalocal(qc,0);
                paraques--;
                no_of_ques_in_list--;
                no_of_current_ques_per_para--;

            } else {


                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                } else {
                    tres = formdata();
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                }
            }
            changeparaflag = false;
        }else
            {



                int cis=0;
                if(isques_limit_exceed){
                    cis=int_qs_size;
                }else {
                    cis=q_count;
                }


        if (qc <cis) {


            try {
                timerrr.setVisibility(View.VISIBLE);
                timer_img.setVisibility(View.VISIBLE);
               // blinking_anim.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            qsmodel = qs.get(qc);

            c = Calendar.getInstance();
            df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            now = df1.format(c.getTime()).toString();
//            if(qc==2){
//                takePicture();
//            }
            if (audio.size() > 1) {
                if (no_of_ques_in_list == audio.get(cnt)) {

                    no_of_current_ques_per_para = 0;
                    changeParagraph(cnt + 1);
                    cnt++;
                    no_of_ques_in_list = 0;


                    // arrow1.performClick();
                    //  ObjectAnimator.ofInt(para_frame, "scrollY",  main_scroller.getTop()).setDuration(10).start();
                }
                //no_of_ques_in_list++;
            }


          /*  for(int i=0;i<qid_array.size();i++){
                if(qsmodel.getQ_id().equalsIgnoreCase(qid_array.get(i))){
                    flag_is_present_id=true;
                }
            }*/


            if (!flag_is_present_id) {

                if (qc == 0) {
                    // qno.setText(qc + 1 + "/" + qs.size());
                    timerrr.setTextColor(Color.parseColor("#ffffff"));
                    timer_img.setBackgroundResource(R.drawable.msq_timer_img);
                    try {
                        qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    //Log.e("current_ques.", no_of_current_ques_per_para + "/" + audio.get(cnt));
                }
                try {
                    //single question
                    if (flag_click && flagdone) {
                        //     animListdata();

                        Log.e("animstatus", "putdata");
                        // hideArrow();

                    }
                    newquid = qsmodel.getQ_id();
                    optioncard = new ArrayList<>();
                    optioncard.add("Q. " + qsmodel.getQuestion());
                    if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                        String s1 = qsmodel.getOpt1();
                        s1 = s1.trim();
                        optioncard.add("" + s1);
                    }
                    if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                        String s2 = qsmodel.getOpt2();
                        s2 = s2.trim();
                        optioncard.add("" + s2);
                    }
                    if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                        String s3 = qsmodel.getOpt3();
                        s3 = s3.trim();
                        optioncard.add("" + s3);
                    }
                    if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                        String s4 = qsmodel.getOpt4();
                        s4 = s4.trim();
                        optioncard.add("" + s4);
                    }
                    if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                        String s5 = qsmodel.getOpt5();
                        s5 = s5.trim();
                        optioncard.add("" + s5);
                    }
                    String crcans = qsmodel.getCorrectanswer();
                    int crcopt = 0;
                    switch (crcans) {
                        case "A":
                            crcopt = 0;
                            break;
                        case "B":
                            crcopt = 1;
                            break;
                        case "C":
                            crcopt = 2;
                            break;
                        case "D":
                            crcopt = 3;
                            break;
                        case "E":
                            crcopt = 4;
                            break;
                        default:
                            System.out.println("Not in 10, 20 or 30");
                    }
                    crct = crcopt;

                    qid = Integer.parseInt(qsmodel.getQ_id());


                    optionAdapterNonClikable = new Option_clickable(this, optioncard);
                    //rc_options_non_clickable_temp.setAdapter(optionAdapterNonClikable);
                    new CountDownTimer(600, 600) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            rc_options_non_clickable.setAdapter(optionAdapterNonClikable);
//                            fastScroller.attachRecyclerView(rc_options_non_clickable);
//
//                            fastScroller.attachAdapter(optionAdapterNonClikable);
                            final Animation animation_fade = AnimationUtils.loadAnimation(MAQ.this, R.anim.fade_in);
                            rc_options_non_clickable.startAnimation(animation_fade);


                        }
                    }.start();
                    Log.e("timimg", "adapter load");

                    String[] options_array = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"};
                    int no_options = optioncard.size();
                    String[] confirm_options = new String[no_options - 1];

                    for (int i = 0; i < no_options - 1; i++) {
                        confirm_options[i] = options_array[i];
                    }

                    if (flag_click) {
                        new CountDownTimer(700, 700) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                //  temp_ques_layout_window.setVisibility(View.VISIBLE);

                            }

                            @Override
                            public void onFinish() {
                                //  ques_scrollview.setVerticalScrollBarEnabled(true);
                                //  rc_options_non_clickable.removeOnItemTouchListener(disabler);
                                // qno.setText(qc + 1 + "/" + qs.size());
                                timerrr.setTextColor(Color.parseColor("#ffffff"));

                                timer_img.setBackgroundResource(R.drawable.msq_timer_img);
                                try {
                                    qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                if (changeparaflag) {

                                } else {
                                    startTime = Long.parseLong(qsmodel.getTime());
                                    startTime = startTime * 1000;
                                    Log.e("timeris", qsmodel.getTime());
                                    countDownTimer = new MyCountDownTimer(startTime, interval);
                                    startTimer();

                                }
                            }
                        }.start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (changeparaflag) {

                    } else {
                        startTime = Long.parseLong(qsmodel.getTime());
                        startTime = startTime * 1000;
                        Log.e("timeris", qsmodel.getTime());
                        countDownTimer = new MyCountDownTimer(startTime, interval);
                        startTimer();

                    }
                }


            } else {
                //Collections.shuffle(qs);
                //changeQues();
                //shuffle
            }
            paraques++;
            no_of_ques_in_list++;
            no_of_current_ques_per_para++;
            formdatalocal(qc,0);
            paraques--;
            no_of_ques_in_list--;
            no_of_current_ques_per_para--;

        } else {


            if (qansr > 0) {
                tres = formdata(qansr);
                checkonp = false;
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
                scoreActivityandPostscore();
            } else {
                tres = formdata();
                checkonp = false;
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
                scoreActivityandPostscore();
            }
        }
        changeparaflag = false;
    }





    }


    public String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qus = sb.toString();


            Log.e("JSONStrr", Qus);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qus;
    }
    public String formdata() {
        String jn = "";
        try{

            JSONObject student1 = new JSONObject();
            student1.put("U_Id", uidd);
            student1.put("S_Id", sid);
            student1.put("Q_Id", newquid);
            student1.put("TG_Id", tgid);
            student1.put("SD_UserAnswer", "E");
            student1.put("SD_StartTime", Calendar.getInstance().getTime().toString());
            student1.put("SD_EndTime", Calendar.getInstance().getTime().toString());


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(student1);


            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn = studentsObj.toString();
        } catch(Exception e)

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jn;
    }

    public String formdata(int anan){
        String jn = "";

        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < anan; i++) {
                JSONObject student = new JSONObject();
                student.put("U_Id", uidd);
                student.put("S_Id", sid);
                student.put("Q_Id", qidj.get(i));
                student.put("QT_Id",qtid.get(i));
                student.put("TG_Id", tgid);
                student.put("SD_UserAnswer", ansj.get(i));
                student.put("SD_StartTime", stj.get(i));
                student.put("SD_EndTime", endj.get(i));
                jsonArray.put(student);
            }

            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn=studentsObj.toString();
            Log.e("jsonpostSTring",jn);
        }catch (Exception e){
            e.printStackTrace();
        }

        return jn;

    }
    private void scoreActivityandPostscore() {
        if (mp != null && mp.isPlaying()) {
            try {
                mp.pause();

            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
          stop();
        ConnectionUtils connectionUtils=new ConnectionUtils(MAQ.this);
        if(connectionUtils.isConnectionAvailable()){


//            for(i=1;i<=detectioncount;i++){
//
//                UUID faceid = detectedfaceid.get(i-1);
//
//                Log.e("location", imagelocation.get(i-1));
//                new VerificationTask(mFaceId1, faceid).execute();
//            }
//         //   new S3getimageurl().execute();

            if (piccapturecount != 0) {
                try {
                    if (!verification_progress.isShowing() || verification_progress != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                verification_progress.show();
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                rc_options_non_clickable.setVisibility(View.GONE);
                timerrr.setVisibility(View.GONE);
                qno.setVisibility(View.GONE);
                timer_img.setVisibility(View.GONE);
                maq_ques.setVisibility(View.GONE);

                verify();
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        verification_progress.show();
                    }
                });
                // new ImageSend().execute();
                rc_options_non_clickable.setVisibility(View.GONE);
                timerrr.setVisibility(View.GONE);
                qno.setVisibility(View.GONE);
                timer_img.setVisibility(View.GONE);
                maq_ques.setVisibility(View.GONE);

                new PostAns().execute();


//                Intent it = new Intent(ListeningGameActivity.this, CheckP.class);
//                it.putExtra("qt", questions.length() + "");
//                it.putExtra("cq", cqa + "");
//                it.putExtra("po", score + "");
//                it.putExtra("assessg",assessg);
//                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(it);
//                finish();

            }
//            if(i+1==piccapturecount) {
//                new ImageSend().execute();
//            }


        }else {
            showretryDialogue();
        }

    }
    public void showretryDialogue() {
        Log.e("respmsg","dialogue");

//        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
//                MAQ.this, R.style.DialogTheme);
//        LayoutInflater inflater = LayoutInflater.from(MAQ.this);
//        View dialogView = inflater.inflate(R.layout.retry_anim_dialogue, null);
//
//        dialogBuilder.setView(dialogView);
//
//
//        alertDialog = dialogBuilder.create();
//
//
////                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
//
//        alertDialog.setCancelable(false);
//        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//
//        alertDialog.show();
        alertDialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils=new ConnectionUtils(MAQ.this);
                        if(connectionUtils.isConnectionAvailable()){
                            scoreActivityandPostscore();
                        }else {
                            assess_progress.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            if(!alertDialog.isShowing())
                alertDialog.show();
        }
        catch (Exception e){

        }
    }



    private class PostAns extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {
                Log.e("datais","ip: "+AppConstant.getLocalIpAddressApp()+" os: "+AppConstant.getAndroidVersionApp()
                        +" version: "+AppConstant.getversionofApp());


                try {

                    getPostData();
                    try {
                        Thread.currentThread();
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/v1/Answers");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            postvalues.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestProperty("app_version",AppConstant.getversionofApp());
                    urlConnection.setRequestProperty("app_os",AppConstant.getAndroidVersionApp());
                    urlConnection.setRequestProperty("app_ip",AppConstant.getLocalIpAddressApp());
                    urlConnection.setRequestProperty("app_latlong",location.getLatitude()+","+location.getLongitude());

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(postvalues);
                    Log.e("resp_data",postvalues);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code2 = urlConnection.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {
                Log.e("code  200 in post is", ""+code2);

                Log.e("code  200 in post", "code  200");

                new Ends().execute();
            }
            else{

                try{
                    verification_progress.dismiss();
                    disable_option();
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e("code not 200 in post", ""+code2);

                Log.e("code not 200 in post", "code not 200");

                Intent it = new Intent(MAQ.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        }
    }


    private class Ends extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            if(new PrefManager(MAQ.this).getGetFirsttimegame().equalsIgnoreCase("first")){
                new PrefManager(MAQ.this).saveFirsttimeGame("last");
            }else if(new PrefManager(MAQ.this).getGetFirsttimegame().equalsIgnoreCase("last")){

            }else {
                new PrefManager(MAQ.this).saveFirsttimeGame("first");
            }


        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("S_Id", sid)
                            .object("course_id", Integer.valueOf(new PrefManager(MAQ.this).getcourseid_ass()))
                            .object("lg_id",new PrefManager(MAQ.this).getlgid_ass())
                            .object("certification_id",new PrefManager(MAQ.this).getcertiid_ass())
                            .build().toString();


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/EndSession");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " +token);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                    Log.v("Response2", ""+code3);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if(code3==200){
                    Log.e("code  200 in ends", "code  200");
                    String responseString = readStream1(is);
                    Log.e("Response2", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                verification_progress.dismiss();
                disable_option();
                Log.e("result", ""+result);

            }catch (Exception e){
                e.printStackTrace();
            }

            if(code3!=200){
                Log.e("code not 200 in ends", "code not 200");

                Toast.makeText(getApplicationContext(),"Error while submitting answers", Toast.LENGTH_SHORT).show();
                Intent it = new Intent(MAQ.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
            else{
                Log.e("code  200 in ends", "code  200");
                try {
                    get_quesdata = new File(getFilesDir() + "" + cate + game + gdid + ceid +uidd + tgid + ".json");
                    get_ansdata = new File(getFilesDir() + "Answer" + cate + game + gdid + ceid + uidd + tgid +".json");
                    get_quesdata.delete();
                    get_ansdata.delete();
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                Intent it = new Intent(MAQ.this, CheckP.class);

                int questionsc=0;
                if(isques_limit_exceed){
                    questionsc=int_qs_size;
                }else {
                    questionsc=q_count;
                }

                it.putExtra("qt", questionsc + "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.putExtra("assessg",assessg);
                //   it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();

            }

        }
    }
    public String readStream1(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qs = sb.toString();


            Log.e("JSONStrr", Qs);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qs;
    }

    @Override
    protected void onPause() {
        super.onPause();

        /*ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);*/


        if (checkonp) {
            if (mp != null && mp.isPlaying()) {
                try {
                    media_pause=true;
                    mp.pause();
                    mplength = mp.getCurrentPosition();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }

            try{
                if(alertDialog!=null&&alertDialog.isShowing()){
                   // alertDialog.dismiss();
                }
            }catch (Exception e){
                e.printStackTrace();
            }

//            close.setEnabled(false);
//            audio_close.setEnabled(false);
            //   vdo_performance_ques_timer.cancel();
            try {
                if (!close_on_get_ques && !proceed_dialog.isShowing()&& !proceed_dialog_showing && !alert_on_audio_close.isShowing()&&!alert_audio_close&&!alert_on_audio_ques_close.isShowing()&&!alert_on_close) {

//                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MAQ.this);
//// ...Irrelevant code for customizing the buttons and title
//                    LayoutInflater inflater = LayoutInflater.from(MAQ.this);
//                    View dialogView = inflater.inflate(R.layout.quit_warning, null);
//                    dialogBuilder.setView(dialogView);
//
//
//                    alertDialog = dialogBuilder.create();
//
//                    alertDialog.setCancelable(false);
//                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//
//                    alertDialog.show();
                    alertDialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.assess_quit_warning);
                    Window window = alertDialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alertDialog.setCancelable(false);

                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
                    final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);

                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();


                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView)alertDialog.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView)alertDialog.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);

                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);

                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);
                                    audio_close.setEnabled(true);


                                    alertDialog.dismiss();
                                    quit_msg_showing = false;
                                    //changeQues();
                                    if (!ismediaplayimg) {
//                                        vdo_performance_ques_timer = new MyCountDownTimer(secLeft, interval);
//                                        vdo_performance_ques_timer.start();
                                    } else {
                                        try {
                                            mp.seekTo(mplength);
                                            mp.start();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        close.setEnabled(true);
                                        audio_close.setEnabled(true);

                                        alertDialog.dismiss();
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        close.setEnabled(true);
                                        audio_close.setEnabled(true);

                                        alertDialog.dismiss();
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();


                        }
                    });

                    try {
                        //quit_msg_showing = true;
                     //   alertDialog.show();
                    }catch (Exception e){}
                }
            }catch (Exception e) {
                e.printStackTrace();

                if (!close_on_get_ques &&!alert_audio_close &&!alert_on_close) {

//
//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MAQ.this);
//// ...Irrelevant code for customizing the buttons and title
//                LayoutInflater inflater = LayoutInflater.from(MAQ.this);
//                View dialogView = inflater.inflate(R.layout.quit_warning, null);
//                dialogBuilder.setView(dialogView);
//
//
//                alertDialog = dialogBuilder.create();
//
//                alertDialog.setCancelable(false);
//                alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//
//
//                try {
//                    alertDialog.show();
//                }catch (Exception ex){
//                    ex.printStackTrace();
//                }

                    alertDialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.assess_quit_warning);
                    Window window = alertDialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alertDialog.setCancelable(false);

                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

                    alertDialog.setCancelable(false);
                    FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                    if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();


                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);
                                    audio_close.setEnabled(true);
                                    alertDialog.dismiss();
                                    quit_msg_showing = false;
                                    // changeQues();
                                    if (!ismediaplayimg) {
//                                        vdo_performance_ques_timer = new MyCountDownTimer(secLeft, interval);
//                                        vdo_performance_ques_timer.start();
                                    } else {
                                        try {
                                            mp.seekTo(mplength);
                                            mp.start();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        close.setEnabled(true);
                                        audio_close.setEnabled(true);

                                        alertDialog.dismiss();
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        close.setEnabled(true);
                                        audio_close.setEnabled(true);
                                        alertDialog.dismiss();
                                        quit_msg_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();


                        }
                    });
                    try {
                      //  quit_msg_showing = true;
                      //  alertDialog.show();
                    } catch (Exception ex) {
                    }
                }
            }
        } else {
            if(checkonp){
                Log.e("PAUSE",checkonp+"");
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    scoreActivityandPostscore();
                } else {
                    tres = formdata();
                    checkonp = false;
                    scoreActivityandPostscore();
                }
            }else{

            }
        }
    }
    private final BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position= Integer.parseInt(intent.getStringExtra("pos"));
            if(position==1000){
                //temp_ques_layout_window.setVisibility(View.GONE);
            }else {

                clickMethods(position);
                pos_clicked=position;
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mReceiver);
//        try{
//            timer1.cancel();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        try {

            if(alertDialog.isShowing()){
                alertDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {

            if(quesLoading.isShowing()){
                quesLoading.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
    public static void downloadData(String uri, String substring) {

        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {

            dm.startDownload(task);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void applyBlurMaskFilter(TextView tv, BlurMaskFilter.Blur style){
        /*
            MaskFilter
                Known Direct Subclasses
                    BlurMaskFilter, EmbossMaskFilter

                MaskFilter is the base class for object that perform transformations on an
                alpha-channel mask before drawing it. A subclass of MaskFilter may be installed
                into a Paint. Blur and emboss are implemented as subclasses of MaskFilter.

        */
        /*
            BlurMaskFilter
                This takes a mask, and blurs its edge by the specified radius. Whether or or not to
                include the original mask, and whether the blur goes outside, inside, or straddles,
                the original mask's border, is controlled by the Blur enum.
        */
        /*
            public BlurMaskFilter (float radius, BlurMaskFilter.Blur style)
                Create a blur maskfilter.

            Parameters
                radius : The radius to extend the blur from the original mask. Must be > 0.
                style : The Blur to use
            Returns
                The new blur maskfilter
        */
        /*
            BlurMaskFilter.Blur
                INNER : Blur inside the border, draw nothing outside.
                NORMAL : Blur inside and outside the original border.
                OUTER : Draw nothing inside the border, blur outside.
                SOLID : Draw solid inside the border, blur outside.
        */
        /*
            public float getTextSize ()
                Returns the size (in pixels) of the default text size in this TextView.
        */

        // Define the blur effect radius
        float radius = tv.getTextSize()/10;

        // Initialize a new BlurMaskFilter instance
        BlurMaskFilter filter = new BlurMaskFilter(9,style);

        /*
            public void setLayerType (int layerType, Paint paint)
                Specifies the type of layer backing this view. The layer can be LAYER_TYPE_NONE,
                LAYER_TYPE_SOFTWARE or LAYER_TYPE_HARDWARE.

                A layer is associated with an optional Paint instance that controls how the
                layer is composed on screen.

            Parameters
                layerType : The type of layer to use with this view, must be one of
                    LAYER_TYPE_NONE, LAYER_TYPE_SOFTWARE or LAYER_TYPE_HARDWARE
                paint : The paint used to compose the layer. This argument is optional and
                    can be null. It is ignored when the layer type is LAYER_TYPE_NONE
        */
        /*
            public static final int LAYER_TYPE_SOFTWARE
                Indicates that the view has a software layer. A software layer is backed by
                a bitmap and causes the view to be rendered using Android's software rendering
                pipeline, even if hardware acceleration is enabled.
        */

        // Set the TextView layer type
        tv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        /*
            public MaskFilter setMaskFilter (MaskFilter maskfilter)
                Set or clear the maskfilter object.

                Pass null to clear any previous maskfilter. As a convenience, the parameter
                passed is also returned.

            Parameters
                maskfilter : May be null. The maskfilter to be installed in the paint
            Returns
                maskfilter
        */

        // Finally, apply the blur effect on TextView text
        tv.getPaint().setMaskFilter(filter);
    }

    @Override
    public void onBackPressed() {

    }

    private  class S3getimageurl extends AsyncTask<Void,Void,Void> {
        URL image1url, image2url,image3url;
        @Override
        protected Void doInBackground(Void... params) {
            String ACCESS_KEY ="AKIAJQTIZUYTX2OYTBJQ";
            String SECRET_KEY = "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";

            try {
                Random ramd = new Random();

                AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY));

                Log.e("outsidecond", String.valueOf(count_detect_no));

                if (autocapimgloc.get(auto_capt_count) != null) {
                    autocapimg1loc1 = autocapimgloc.get(auto_capt_count);
                    int innd = ramd.nextInt(100000);
                    File pic1 = new File(autocapimg1loc1);
                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg", pic1);
                    PutObjectResult putResponse = s3Client.putObject(pp);
                    String prurl = s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                    image1url = s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                    img1 = image1url.toString();
                    Log.e("AMAZON RESPONSE", img1);
                    replacedurl = img1.replace("https://valyouinputbucket.s3.amazonaws.com/", "https://valyouinputbucket.s3.ap-south-1.amazonaws.com/");

                    auto_capt_count++;
                } else {
                    img1 = "";
                    auto_capt_count++;
                }
                imgurllist.add(replacedurl);
                Log.e("detectis in s3", String.valueOf(count_detect_no));
                count_detect_no++;

                verify();
            }
            catch (Exception e){
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (verification_progress.isShowing() || verification_progress != null) {

                                verification_progress.dismiss();


                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                showretrys3urlDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            showretrys3urlDialogue();
                        }
                    }
                });
            }
            return  null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  new SExample(ppp.toString()).execute();
            //GameModel gameModel = new GameModel();
            //String params="U_Id="+candidateId+"&g_id="+12+"&image_url="+image1url+","+image2url+","+image3url+"&fr_status="+0+"&assesment_name="+ "FIB"+"&assesment_id="+2+"&company_name="+"TATACOMMUNICATION";



            // Log.e("params" , params);
            // new ImageSend().execute();

        }
    }
    public class ImageSend extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";
            //   String lg_id = " ", cert_id=" ", course_id = " ", tg_group_id=" ";

            String responseString="";

          /*  try{
                lg_id = new PrefManager(MAQ.this).getlgid();

                course_id = new PrefManager(MAQ.this).getCourseid();

                cert_id =  new PrefManager(MAQ.this).getcertiid();
                tg_group_id = new ToadysgameModel().getAss_groupid();
            }

            catch (Exception e){
                e.printStackTrace();
            }
*/
            try {


                JSONObject child = new JSONObject();

                if(get_nav.equalsIgnoreCase("learn"))
                {
                    child.put("U_Id", candidateId);
                    child.put("GD_Id", gdid);
                    child.put("fr_status", "0");
                    child.put("assesment_name", game);
                    child.put("CE_Id", ceid);
                    child.put("company_name", HomeActivity.tg_group);
                    child.put("TG_GroupId", tg_groupid);
                    child.put("lg_id", lg_id);
                    child.put("course_id", course_id);
                    child.put("certification_id",certifcation_id);
                }
                else {

                    child.put("U_Id", candidateId);
                    child.put("GD_Id", gdid);
                    child.put("fr_status", "0");
                    child.put("assesment_name", game);
                    child.put("CE_Id", ceid);

                    child.put("company_name", HomeActivity.tg_group);
                    child.put("TG_GroupId", tg_groupid);
                    child.put("lg_id", lg_id);
                    child.put("course_id", course_id);
                    child.put("certification_id",certifcation_id);
                }
                JSONArray jsonArray = new JSONArray();
                for(int i=0;i<imgurllist.size();i++){
                    jsonArray.put(imgurllist.get(i));
                }
                JSONObject mainobj=new JSONObject();
                child.put("image_url",jsonArray);
                mainobj.put("data",child);


                Log.e("datais",mainobj.toString());
                URL urlToRequest = new URL(AppConstant.Ip_url+"Player/UploadFrImages");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();


                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                urlConnection.setRequestMethod("POST");




                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(mainobj.toString());
                wr.flush();

                responseString = readStream1(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result=responseString;



            }
            catch (JSONException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            verification_progress.dismiss();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                Log.e("JSONException"," showretryimagesendDialogue");

                                showretryimagesendDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("JSONException"," showretryimagesendDialogue");

                            showretryimagesendDialogue();
                        }
                    }
                });
            }catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            verification_progress.dismiss();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                Log.e("Exception"," showretryimagesendDialogue");

                                showretryimagesendDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("Exception"," showretryimagesendDialogue");

                            showretryimagesendDialogue();
                        }
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            Log.e("datais","post");
            new PostAns().execute();
//            Intent it = new Intent(ListeningGameActivity.this, CheckP.class);
//            it.putExtra("qt", questions.length() + "");
//            it.putExtra("cq", cqa + "");
//            it.putExtra("po", score + "");
//            it.putExtra("assessg",assessg);
//            it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(it);
//            finish();



        }

        private String readStream1(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
    private void startVerification() {
        camera = getCameraInstance();

        waitTimer = new CountDownTimer(60000, 3) {


            @SuppressLint("SimpleDateFormat")
            @Override
            public void onTick(long l) {


            }

            public void onFinish() {

                stop();

            }



        }.start();

    }
    private Camera getCameraInstance() {
        Camera camera = null;
        if (!getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //   Toast.makeText(this, "No camera on this device", Toast.LENGTH_LONG)
            //    .show();
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId < 0) {
                //  Toast.makeText(this, "No front facing camera found.",
                //       Toast.LENGTH_LONG).show();
            } else {

                try {
                    camera = Camera.open(cameraId);
                    mPreview = new CameraPreview(this, camera);
                    preview.addView(mPreview);
                    onClick();

                } catch (Exception e) {
                    // cannot get camera or does not exist
                }
            }
        }

        return camera;
    }
    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d("status", "Camera found");
                cameraId = i;
                break;
            }else {
                Log.e("Status","No front camera found");
            }
        }
        return cameraId;
    }
    public void onClick() {


        timer1 = new Timer();
        timer1.schedule(new TimerTask()
        {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        if(picture_count<pic_frequency) {

                            try {
                                camera.setDisplayOrientation(90);
                                camera.startPreview();


                                camera.takePicture(null, null, new PhotoHandler(getApplicationContext()));
                                picture_count++;

                                Log.e("pic count", "" + picture_count);

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        else{
                            stop();
                        }


                    }
                });
            }
        }, 3000, 9000);


    }
    public void stop(){
        preview.setVisibility(View.GONE);

        // waitTimer.cancel();


        try {
            timer1.cancel();
        }
        catch (Exception e){
            e.printStackTrace();
        }


    }
    public static Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    private void detectAndFrameother1(Bitmap imageBitmap) {


        /*if(imageBitmap.getWidth()>imageBitmap.getHeight()){
            imageBitmap=rotateBitmap(imageBitmap,-90);
        }*/
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {


                        try {

                            // if (result == null)
                            //   Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                            //if face is not detected in image
                            if ((result != null && result.length == 0)) {
                                //  Toast.makeText(MAQ.this,"other face is not detected",Toast.LENGTH_SHORT).show();
                                // detectedfaceid.add(UUID.fromString(""));
                                facedetectedimagelocation.add(count_detect_no, "");
                                Log.e("outside", String.valueOf(count_detect_no) + "nondetect");
                                count_detect_no++;
                                verify();
                            }

                            //if face is detected in image
                            else {
                                //       Toast.makeText(MAQ.this, "other face is  detected", Toast.LENGTH_SHORT).show();

                                Log.e("outside", String.valueOf(count_detect_no) + "detect");
                                //imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                                //  Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                                // imageBitmap.recycle();


                                //imagelocation.add(mImageFileLocation);


                                //    Log.e("imglocation", mImageFileLocation);
//                            switch (piccapturecount){
//
//                                case 1:
//                                    verifiedimagelocation.add(imagelocation.get(0));
//                                    break;
//                                case 2:
//                                    verifiedimagelocation.add(imagelocation.get(1));
//                                    break;
//                                case 3:
//                                    verifiedimagelocation.add(imagelocation.get(2));
//                                    break;
//
//                            }

                                //adding detected image location in an array to get its url from s3
                                facedetectedimagelocation.add(imagelocation.get(count_detect_no));

                                List<Face> faces;

                                UUID mFaceIdother;
                                assert result != null;
                                faces = Arrays.asList(result);
                                for (Face face : faces) {
                                    mFaceIdother = face.faceId;
                                    String faceid = mFaceIdother.toString();
                                    // txt.setText(faceid);


                                    //adding detected face uuid in an array

                                    detectedfaceid.add(image_face_count, mFaceIdother);


                                    UUID[] uuids = new UUID[]{mFaceIdother};

                                    //    Log.e("mfaceid", String.valueOf(mFaceIdother));

                                    mFaceIdother = UUID.fromString(faceid);
                                    //      new VerificationTask(mFaceId1, mFaceIdother).execute();
                                    //  Log.e("face id size", String.valueOf(detectedfaceid.size()));
//                                if(detectedfaceid.size()==3){
//

                                    //
                                 /*   String mfaceid = prefManager.getprofilefaceid();

                                    UUID mfaceid1 = UUID.fromString(mfaceid);*/
                                    new VerificationTask(mfaceid1, detectedfaceid.get(image_face_count)).execute();

//

//
//                                }


                                }


                            }
                        }catch (Exception e){

                            ConnectionUtils connectionUtils = new ConnectionUtils(MAQ.this);
                            if(connectionUtils.isConnectionAvailable()){
                                if(result==null){
                                    facedetectedimagelocation.add(count_detect_no, "");
                                    Log.e("outside", String.valueOf(count_detect_no) + "nondetect");
                                    count_detect_no++;
                                    verify();
                                }
                                else{

                                }
                            }
                            else{
                                showretrydetectDialogue();
                            }

                            e.printStackTrace();
                            try {
                                if (verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.dismiss();

                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                            //showretrydetectDialogue();
                        }



                    }

                };

        detectTask.execute(inputStream);
    }
    private void verify() {

        Log.e("detectis count",String.valueOf(count_detect_no));
        // detecting the captured images
        if(count_detect_no<piccapturecount) {
            detectAndFrameother1(autotakenpic.get(count_detect_no));
        }

        if(count_detect_no==piccapturecount){
            //senddata to server
            for(int i=0;i<imgurllist.size();i++){
                Log.e("outsideurl",i+" "+imgurllist.get(i));
            }

            try{
                //   verification_progress.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
            // new PostAns().execute();

            if(imgurllist.size()==0){


                new PostAns().execute();
//                Intent it = new Intent(ListeningGameActivity.this, CheckP.class);
//                it.putExtra("qt", questions.length() + "");
//                it.putExtra("cq", cqa + "");
//                it.putExtra("po", score + "");
//                it.putExtra("assessg",assessg);
//                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(it);
//                finish();

            }else {
                new ImageSend().execute();
            }

        }




    }
    public class PhotoHandler implements Camera.PictureCallback {


        private final Context context;

        public PhotoHandler(Context context) {
            this.context = context;
        }

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            piccapturecount++;
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException e) {
                e.printStackTrace();
            }

            String authorities = getApplicationContext().getPackageName() + ".fileprovider";



            try {
                assert photoFile != null;
                FileOutputStream fos = new FileOutputStream(photoFile);
                fos.write(data);
                fos.flush();
                fos.close();
                fos.close();
                //   Toast.makeText(context, "New Image saved:" + photoFile,
                //       Toast.LENGTH_LONG).show();
            } catch (Exception error) {

                //   Toast.makeText(context, "Image could not be saved.",
                //         Toast.LENGTH_LONG).show();
            }

            //String filepath=  pictureFile.getPath();

            bmpother = BitmapFactory.decodeFile(mImageFileLocation);
            imageUri= FileProvider.getUriForFile(getApplicationContext(), authorities, photoFile);


            // imageView.setImageBitmap(bitmap);
            //   Bitmap b = decodeFile(pictureFile);
            // Uri mImageUri = Uri.fromFile(pictureFile);




            Matrix mMatrix = new Matrix();

            mMatrix.setRotate(-90);
            bmpother = Bitmap.createBitmap(bmpother, 0, 0, bmpother.getWidth(),
                    bmpother.getHeight(), mMatrix, false);


            // saving location of auto capture image in an array
            imagelocation.add(mImageFileLocation);

            //saving auto capture bitmap image in an array
            autotakenpic.add(bmpother);

//
//            Toast.makeText(context, "Image saved."+imagelocation.size(),
//                    Toast.LENGTH_LONG).show();

            // detectAndFrameother(bmpother);

        }


        public  File createImageFile() throws IOException {

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "IMAGE_" + timeStamp + "_";
            File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
            mImageFileLocation = image.getAbsolutePath();

            return image;

        }

    }
    private class VerificationTask extends AsyncTask<Void, String, VerifyResult> {
        // The IDs of two face to verify.
        private UUID mFaceId0;
        private UUID mFaceId1;

        VerificationTask (UUID faceId0, UUID faceId1) {
            mFaceId0 = faceId0;
            mFaceId1 = faceId1;
        }

        @Override
        protected VerifyResult doInBackground(Void... params) {
            // Get an instance of face service client to detect faces in image.
            try{
                publishProgress("Verifying...");

                // Start verification.
                return faceServiceClient.verify(
                        mFaceId0,      /* The first face ID to verify */
                        mFaceId1);     /* The second face ID to verify */
            }  catch (Exception e) {
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(VerifyResult result) {

            image_face_count++;
            // Show the result on screen when verification is done.
            setUiAfterVerification(result);
        }
    }
    //detection
    private void setUiAfterVerification(VerifyResult result) {
        // Verification is done, hide the progress dialog.

        // Enable all the buttons.

        // Show verification result.

        try {
            if (result != null) {
                //   Log.e("facedetection","Result");
                DecimalFormat formatter = new DecimalFormat("0.0%");
                String verificationResult = (result.isIdentical ? "The same person" : "Different persons")
                        + ". The confidence is " + formatter.format(result.confidence);
                //  Log.e("facedetection",verificationResult);


                if (verificationResult.contains("Different")) {

                    //    if(i==1)

                    // adding mismatch image location in an array
                    autocapimgloc.add(auto_capt_count, facedetectedimagelocation.get(count_detect_no));

                    Log.e("added", facedetectedimagelocation.get(count_detect_no));

                    //getting image url from s3
                    new S3getimageurl().execute();

                    Log.e("not outside s3", String.valueOf(count_detect_no));

                } else {
                    Log.e("outside s3", String.valueOf(count_detect_no));
                    count_detect_no++;

                    verify();

                }

            }
        }  catch (Exception e){


            e.printStackTrace();

            try {
                if (verification_progress.isShowing() || verification_progress != null) {

                    verification_progress.dismiss();


                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
            showretryverifyDialogue();

        }
    }
    private void detectAndFrame(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
//                            Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","No face detected");
                        }else {
                            //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","face detected");
                            //  startVerification();
                        }


                        // imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                        //   Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                        //imageBitmap.recycle();


                        List<Face> faces;

                        assert result != null;
                        faces = Arrays.asList(result);
                        for (Face face: faces) {
                            mFaceId1 = face.faceId;
                            faceid=mFaceId1.toString();
                            //txt.setText(faceid);
                            Log.e("fid",mFaceId1+"  "+mface);

                        }

                    }
                };
        detectTask.execute(inputStream);
    }
    private void checkfacedetection() {
        if(prefManager.getPic()==" "){
            String profileImageUrl = new DataBaseHelper(MAQ.this).getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
            // new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);
            Log.e("facedetection","download pic");
        }else {
            faceServiceClient =
                    new FaceServiceRestClient(AppConstant.FR_end_points,new PrefManager(MAQ.this).get_frkey());

            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Log.e("facedetection","alreday present");
            detectAndFrame(decodedByte);
        }
    }


    public void startPlanetAnimation()
    {

        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }
    public void disable_option(){
        main_ques_layout_window.setVisibility(View.GONE);
        timer_ques.setVisibility(View.GONE);



    }

    @Override
    protected void onResume() {
        super.onResume();
        if(media_pause) {
            try {
                mp.seekTo(mplength);
                mp.start();
                media_pause=false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{

        }
    }




    //store getquestion data locally new changes
    private void jsonMakeFile(String s, String responseString) {

        get_quesdata = new File( getFilesDir() +""+s+".json");
        String previousJson = " ";
        try{
            if (get_quesdata.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(get_quesdata);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        previousJson = Charset.defaultCharset().decode(bb).toString();
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                try {
                    get_quesdata.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            Log.e("prejson",previousJson);



        }catch (NullPointerException e){
            try {
                get_quesdata.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }


        if(previousJson.length()>4){
            //json file of questions is not an empty
            Log.e("prejson","not emplty file,second attempt");
            try {
                FileInputStream stream = new FileInputStream(get_quesdata);

                try {
                    FileChannel fc = stream.getChannel();
                    MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                    old_get_ques_data = Charset.defaultCharset().decode(bb).toString();
                    Log.e("prejson ", "getques old data-" + old_get_ques_data);
                } finally {
                    stream.close();
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            //need
            flaggameattempfirst=false;
        }else {
            //json file of questions is an empty
            Log.e("prejson","emplty file,first attempt");
            try {

                FileWriter file = new FileWriter(get_quesdata);
                file.write(responseString);
                file.flush();
                file.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            //need
            flaggameattempfirst=true;

        }


        //  Log.e("Jsonfile write", jarray.toString());

        // jsonRead();
    }


    private void loadofflinedata() {
        String responsestored=old_get_ques_data;
        try {
            JSONArray data = new JSONArray(responsestored);
            Log.e("QUESDATA", data.length() + "");
            // JSONArray sidd = data.getJSONArray(0);
            final JSONObject sidobj = data.getJSONObject(0);
            sid = sidobj.getString("S_Id");
            q_count=sidobj.getInt("question_count");
            currentlevel=sidobj.getInt("currentGameLevel");
            maxlevel=sidobj.getInt("max_level");
            min_level=sidobj.getInt("min_level");
            if(currentlevel<min_level){
                currentlevel=min_level;
            }
            if(currentlevel>maxlevel){
                currentlevel=min_level;
            }
            levelis=currentlevel;

            JSONArray js1 = data.getJSONArray(1);



            for(int i=0;i<js1.length();i++){
                JSONArray jsnew=js1.getJSONArray(i);
                for(int j=0;j<jsnew.length();j++){
                    JSONObject jobjis=jsnew.getJSONObject(j);
                    audios.put(jobjis);
                }

            }

            para_audio_count = audios.length();





            questions = data.getJSONArray(2);
            for(int i=0;i<questions.length();i++){
                JSONObject js=questions.getJSONObject(i);
                QuestionModel s=new QuestionModel();
                s.setQT_id(js.getString("QT_Id"));
                s.setQ_id(js.getString("Q_Id"));
                s.setQuestion(js.getString("Q_Question"));
                s.setOpt1(js.optString("Q_Option1"));
                s.setOpt2(js.optString("Q_Option2"));
                s.setOpt3(js.optString("Q_Option3"));
                s.setOpt4(js.optString("Q_Option4"));
                s.setOpt5(js.optString("Q_Option5"));
                s.setCorrectanswer(js.getString("Q_Answer"));
                s.setTime(js.getString("Q_MaxTime"));
                Log.e("QUESTIME", js.getString("Q_MaxTime"));
                qs.add(s);
            }

            int_qs_size=qs.size();
            if(q_count>qs.size()){
                isques_limit_exceed=true;
            }
            Log.e("QUES", questions.length() + "");
            tq = questions.length();


            setlevelingparams(cate+game+gdid+ceid+uidd+tgid);
            rel_popup.setVisibility(View.GONE);
            maq_ques.setVisibility(View.VISIBLE);


            secLeft=startTime;
            final int MINUTES_IN_AN_HOUR = 60;
            final int SECONDS_IN_A_MINUTE = 60;

            int sec= (int) secLeft/1000;
            int minutes =  (sec / SECONDS_IN_A_MINUTE);
            sec -= minutes * SECONDS_IN_A_MINUTE;

            int hours = minutes / MINUTES_IN_AN_HOUR;
            minutes -= hours * MINUTES_IN_AN_HOUR;
            fulltime= (int) secLeft;
            // timer_seek.setMin(0);
            new CountDownTimer(500, 500) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                        startTime = questions.length() * 30 * 1000;
                    Log.e("mystatus","ques params");
                    changeQues();
                        // startTimer();
                        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


                            startPlanetAnimation();
                        }
                        startVerification();
                        // checkfacedetection();

                }
            }.start();


        } catch (Exception e) {
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    qs.clear();
                    try {
                        if (quesLoading.isShowing()) {
                            quesLoading.dismiss();
                        }
                    }catch (Exception e){

                    }
                    try {
                        if (alert_on_retry.isShowing()) {

                        } else {
                            Log.e("postexception try"," show");

                            showGetQuesAgainDialogue();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        Log.e("postexception catch"," show");

                        showGetQuesAgainDialogue();
                    }
                }
            });
            Log.e("QUES", "Error parsing data " + e.toString());
        }





    }

    //stored answers and params in file new changes
    private void jsonMakeFileAnswerandParams(String s, String answers) {


        get_ansdata = new File( getFilesDir() +"Answer"+s+".json");
        String previousJson = " ";
        try{
            if (get_ansdata.exists()) {


            } else {
                try {
                    get_ansdata.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            Log.e("prejson",previousJson);



        }catch (NullPointerException e){
            try {
                get_ansdata.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        try {
            Log.e("prejson","putting data");
            FileWriter file = new FileWriter(get_ansdata);
            file.write("");
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        //json file of questions is an empty

        JSONObject jsonObject=new JSONObject();
        try {

            //ans array
            JSONArray main_arrayis=null;

            if(flaggameattempfirst){
                //if first time
                main_arrayis=new JSONArray(answers);
                ans_submission_string=main_arrayis.toString();

            }else {
                //if more than first time
                main_arrayis=new JSONArray(answers);
                ans_submission_string=main_arrayis.toString();

            }



            jsonObject.put("answeres",main_arrayis);
            jsonObject.put("levelis",levelis);
            jsonObject.put("currentquestionis",qc+1);
            jsonObject.put("firstparacount",firstpara);
            //leveling
            jsonObject.put("paraQueslevel",paraques);
            jsonObject.put("correctQueslevel",correctques);
            jsonObject.put("no of ques in list",no_of_ques_in_list);
            jsonObject.put("no of ques in para",no_of_current_ques_per_para);
            jsonObject.put("indexis",qc_index);
            jsonObject.put("main_assests_indexis",index);
            jsonObject.put("correctanswerscountparam",cqa);
            jsonObject.put("paracount",para_count);
            jsonObject.put("current_audio_count",current_audio_count);
            jsonObject.put("no_of_current_ques_per_para",no_of_current_ques_per_para);
            jsonObject.put("total ques shows",cnt);
            jsonObject.put("bannerparagraph_count",paragraph_count);
            String assetsstring="";
            for(int i=0;i<assets.size();i++){
                if(i==0){
                    assetsstring=assets.get(i);
                }else {
                    assetsstring=assetsstring+"qwerty123"+assets.get(i);
                }
            }

            String levelstring="";
            for(int i=0;i<levels.size();i++){
                if(i==0){
                    levelstring= String.valueOf(levels.get(i));
                }else {
                    levelstring=levelstring+"qwerty123"+String.valueOf(levels.get(i));
                }
            }

            String audiostring="";
            for(int i=0;i<audio.size();i++){
                if(i==0){
                    audiostring= String.valueOf(audio.get(i));
                }else {
                    audiostring=audiostring+"qwerty123"+String.valueOf(audio.get(i));
                }
            }

            String assetsidstring="";
            for(int i=0;i<asset_id.size();i++){
                if(i==0){
                    assetsidstring= String.valueOf(asset_id.get(i));
                }else {
                    assetsidstring=assetsidstring+"qwerty123"+String.valueOf(asset_id.get(i));
                }
            }

            jsonObject.put("assetsarray",assetsstring);
            jsonObject.put("levelsarray",levelstring);
            jsonObject.put("audioarray",audiostring);
            jsonObject.put("assetsid",assetsidstring);

            if(islevelingenable){
                jsonObject.put("islevelingenable","true");
            }else {
                jsonObject.put("islevelingenable","false");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //putting data into file
        try {
            Log.e("klocal_jobjdata",jsonObject.toString());
            FileWriter file = new FileWriter(get_ansdata);
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        //need




        //  Log.e("Jsonfile write", jarray.toString());

        // jsonRead();
    }


    //set leveling params,get params data,answers data new changes
    private void setlevelingparams(String s) {
        getdatafromfile = new File( getFilesDir() +"Answer"+s+".json");
        String levelingparamsandanswers = " ";
        try{
            if (getdatafromfile.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(getdatafromfile);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        ansdatafromfile = Charset.defaultCharset().decode(bb).toString();
                        Log.e("prejson ", "getques old data-" + ansdatafromfile);
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                //new GetQues().execute();
            }
            //get data from file
            Log.e("prejson",ansdatafromfile);



        }catch (NullPointerException e){
            try {
                getdatafromfile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }



        //json file of questions is an empty



        Log.e("klevel_fromfile",ansdatafromfile);

        try {
            JSONObject jsonObjectis=new JSONObject(ansdatafromfile);
            JSONArray jsonarrayanswers=jsonObjectis.getJSONArray("answeres");

            for(int i=0;i<jsonarrayanswers.length();i++){
                JSONObject js=jsonarrayanswers.getJSONObject(i);
                qidj.add(i,js.getString("Q_Id"));
                qtid.add(i,js.getString("QT_Id"));
                ansj.add(i,js.getString("SD_UserAnswer"));
                stj.add(i,js.getString("SD_StartTime"));
                endj.add(i,js.getString("SD_EndTime"));

            }
            //old stored answers
            mainformdatastring=jsonarrayanswers.toString();
            levelis=jsonObjectis.getInt("levelis");
            qc=jsonObjectis.getInt("currentquestionis");
            cqa=jsonObjectis.getInt("correctanswerscountparam");
            firstpara=jsonObjectis.getInt("firstparacount");
            paraques=jsonObjectis.getInt("paraQueslevel");
            correctques=jsonObjectis.getInt("correctQueslevel");
            qc_index=jsonObjectis.getInt("indexis");
            index=jsonObjectis.getInt("main_assests_indexis");
            para_count=jsonObjectis.getInt("paracount");
            no_of_ques_in_list=jsonObjectis.getInt("no of ques in list");
            no_of_current_ques_per_para=jsonObjectis.getInt("no of ques in para");
            current_audio_count=jsonObjectis.getInt("current_audio_count");
            paragraph_count=jsonObjectis.getInt("bannerparagraph_count");
            no_of_current_ques_per_para=jsonObjectis.getInt("no_of_current_ques_per_para");
            cnt=jsonObjectis.getInt("total ques shows");

            if(jsonObjectis.getString("islevelingenable").equalsIgnoreCase("true")){
                islevelingenable=true;
            }else {
                islevelingenable=false;
            }

            String assetsarray=jsonObjectis.getString("assetsarray");
            String levelsarray=jsonObjectis.getString("levelsarray");
            String audioarray=jsonObjectis.getString("audioarray");
            String assetsid=jsonObjectis.getString("assetsid");

            String assetsarrayis[]=assetsarray.split("qwerty123");
            for(int i=0;i<assetsarrayis.length;i++){
                assets.add(i,assetsarrayis[i]);
            }

            String levelsarrayis[]=levelsarray.split("qwerty123");
            for(int i=0;i<levelsarrayis.length;i++){
                levels.add(i, Integer.valueOf(levelsarrayis[i]));
            }

            String audioarrayis[]=audioarray.split("qwerty123");
            for(int i=0;i<audioarrayis.length;i++){
                audio.add(i,Integer.valueOf(audioarrayis[i]));
            }

            String assetsidis[]=assetsid.split("qwerty123");
            for(int i=0;i<assetsidis.length;i++){
                asset_id.add(i,Integer.valueOf(assetsidis[i]));
            }




            for(int i=0;i<jsonarrayanswers.length();i++){
                JSONObject jsonObject=jsonarrayanswers.getJSONObject(i);
                String ids=jsonObject.getString("Q_Id");
                for(int j=0;j<qs.size();j++){
                    QuestionModel questionModel=qs.get(j);
                    if(ids.equalsIgnoreCase(questionModel.getQ_id())){
                        if(islevelingenable){
                            qs.remove(j);
                        }else {

                        }

                    }
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("mystatus","leveling params");

        flag_click=true;
       // qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));

    }

    public void getPostData(){

        getdatafromfile = new File( getFilesDir() +"Answer"+cate+game+gdid+ceid+uidd+tgid+".json");
        String levelingparamsandanswers = " ";
        try{
            if (getdatafromfile.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(getdatafromfile);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        ansdatafromfile = Charset.defaultCharset().decode(bb).toString();
                        Log.e("prejson ", "getques old data-" + ansdatafromfile);
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                //new GetQues().execute();
            }
            //get data from file
            Log.e("prejson",ansdatafromfile);



        }catch (NullPointerException e){
            try {
                getdatafromfile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        try {
            JSONObject jsonObjectis=new JSONObject(ansdatafromfile);

            JSONArray jsonarrayanswers=jsonObjectis.getJSONArray("answeres");
            postvaluearray=jsonarrayanswers.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject studentsObj = new JSONObject();
        JSONArray jsonArray= null;
        try {
            jsonArray = new JSONArray(postvaluearray);
            studentsObj.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postvalues=studentsObj.toString();

    }

    //new changes
    public String formdatalocal(int qc,int val){
        String jn="";


        try {
            JSONArray js=new JSONArray();
            JSONObject student=null;
            for(int i=0;i<qc+1;i++) {
                student = new JSONObject();
                student.put("U_Id", uidd);
                student.put("S_Id", sid);
                student.put("TG_Id", tgid);

                if(val==0){
                    if(i==qc) {
                        student.put("Q_Id", qsmodel.getQ_id());
                        student.put("QT_Id", qsmodel.getQ_id());
                        student.put("SD_UserAnswer", " ");
                        student.put("SD_StartTime", now);
                        student.put("SD_EndTime", now);
                    }else {
                        student.put("Q_Id", qidj.get(i));
                        student.put("QT_Id",qtid.get(i));
                        student.put("SD_UserAnswer", ansj.get(i));
                        student.put("SD_StartTime", stj.get(i));
                        student.put("SD_EndTime", endj.get(i));
                    }
                }else {
                    student.put("Q_Id", qidj.get(i));
                    student.put("QT_Id",qtid.get(i));
                    student.put("SD_UserAnswer", ansj.get(i));
                    student.put("SD_StartTime", stj.get(i));
                    student.put("SD_EndTime", endj.get(i));
                }

                js.put(student);

            }

            jn=js.toString();
            Log.e("klocal_qc",""+qc);

            if(val==0){
                Log.e("klocal_formdata0",jn);

            }else {
                Log.e("klocal_formdata1",jn);

            }
            formdatanewstring=jn;
            jsonMakeFileAnswerandParams(cate+game+gdid+ceid+uidd+tgid,jn);

        }catch (Exception e){
            e.printStackTrace();
        }
        return jn;
    }

    public void showretrydetectDialogue() {
        Log.e("showretrydetectDialogue","showretrydetectDialogue");

        alertDialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(MAQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();


                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            detectAndFrameother1(autotakenpic.get(count_detect_no));
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretrydetectDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretryverifyDialogue() {
        Log.e("showretryverifyDialogue","showretryverifyDialogue");

        alertDialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(MAQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new VerificationTask(mfaceid1, detectedfaceid.get(image_face_count)).execute();
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryverifyDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretrys3urlDialogue() {
        Log.e("showretrys3urlDialogue","showretrys3urlDialogue");

        alertDialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(MAQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new S3getimageurl().execute();
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretrys3urlDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretryimagesendDialogue() {
        Log.e("showretryimgsndDialogue","showretryimagesendDialogue");

        alertDialog = new Dialog(MAQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(MAQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new ImageSend().execute();
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryimagesendDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }

}
