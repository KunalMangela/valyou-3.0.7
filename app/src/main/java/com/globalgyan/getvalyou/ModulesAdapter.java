package com.globalgyan.getvalyou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.simulation.SimulationStartPageActivity;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.google.gson.JsonObject;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by komal on 18-04-2018.
 */

public class ModulesAdapter extends RecyclerView.Adapter<ModulesAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<ModulesModel> fungame;
    ConnectionUtils connectionUtils;

    boolean flag_locked = false;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView module_names,author,price_module,description_button_module,progressis_module,desc_arrow_module,topics,description_text_module,topic_plus_sign,topics_text;
        FrameLayout main_fr;
        ImageView module_cover,status_image;
        SimpleRatingBar ratingbar_module;
        LinearLayout description_linear_module;
        RelativeLayout description_relative_module,lock_layout_module,played_layout_module;
        LinearLayout view_topics_linear_module;
        RelativeLayout topic_list_relative;
        RecyclerView topiclist_recycler;

        public MyViewHolder(View view) {
            super(view);
            module_names=(TextView)view.findViewById(R.id.moduleename);
            author=(TextView)view.findViewById(R.id.author_module);
            module_cover=(ImageView)view.findViewById(R.id.module_cover);
            main_fr=(FrameLayout)view.findViewById(R.id.main_fr_module);
            price_module=(TextView)view.findViewById(R.id.price_module);
            status_image=(ImageView)view.findViewById(R.id.status_image);
            topic_list_relative=(RelativeLayout)view.findViewById(R.id.topic_list_relative);
            ratingbar_module=(SimpleRatingBar)view.findViewById(R.id.ratingbar_module);
            description_linear_module=(LinearLayout)view.findViewById(R.id.description_linear_module);
            description_button_module=(TextView)view.findViewById(R.id.description_button_module);
            description_relative_module=(RelativeLayout)view.findViewById(R.id.description_relative_module);
            desc_arrow_module=(TextView)view.findViewById(R.id.desc_arrow_module);
            topics=(TextView)view.findViewById(R.id.topics);
            progressis_module=(TextView)view.findViewById(R.id.progressis_module);
            //topic click
            view_topics_linear_module=(LinearLayout) view.findViewById(R.id.view_topics_linear_module);
            topic_plus_sign=(TextView)view.findViewById(R.id.topic_plus_sign);
            topics_text=(TextView)view.findViewById(R.id.topics_text);
            topiclist_recycler=(RecyclerView)view.findViewById(R.id.topiclist_recycler);

            description_text_module=(TextView)view.findViewById(R.id.description_text_module);
            lock_layout_module=(RelativeLayout)view.findViewById(R.id.lock_layout_module);
            played_layout_module=(RelativeLayout)view.findViewById(R.id.played_layout_module);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            topiclist_recycler.setLayoutManager(linearLayoutManager);
            topiclist_recycler.setAdapter(ModulesActivity.topicAdapter);
            topiclist_recycler.setHasFixedSize(true);
            //topiclist_recycler.setLayoutFrozen(true);
           // topiclist_recycler.addOnItemTouchListener(mScrollTouchListener);

        }
    }


    public ModulesAdapter(Context mContext, ArrayList<ModulesModel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;
        connectionUtils=new ConnectionUtils(mContext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.modules_list, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Log.e("whenpossible","modules-"+position);
        final ModulesModel modulesModel=fungame.get(position);
        holder.module_names.setSelected(true);
        holder.author.setSelected(true);
        holder.setIsRecyclable(false);

        if(modulesModel.getType().equalsIgnoreCase("lesson")){
            if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")){
                flag_locked = false;
                if(modulesModel.getIs_locked().equalsIgnoreCase("completed")){
                    // holder.played_layout_module.setVisibility(View.VISIBLE);
                    holder.progressis_module.setText("Completed");
                    holder.status_image.setVisibility(View.VISIBLE);
                    holder.status_image.setImageResource(R.drawable.complete_green);
                    holder.lock_layout_module.setVisibility(View.GONE);
                }else  if(modulesModel.getIs_locked().equalsIgnoreCase("inprogress")){
                    // holder.played_layout_module.setVisibility(View.VISIBLE);
                    holder.progressis_module.setText("Inprogress");
                    holder.status_image.setVisibility(View.GONE);

                    holder.lock_layout_module.setVisibility(View.GONE);
                }else {
                    // holder.played_layout_module.setVisibility(View.GONE);
                    holder.progressis_module.setText("New");
                    holder.status_image.setVisibility(View.GONE);

                    holder.lock_layout_module.setVisibility(View.GONE);

                }

            }else {
                flag_locked = true;
                // holder.played_layout_module.setVisibility(View.GONE);
                holder.progressis_module.setText("Locked");
                holder.status_image.setVisibility(View.GONE);

                holder.lock_layout_module.setVisibility(View.VISIBLE);
            }
        }


        if(modulesModel.getType().equalsIgnoreCase("lesson")){
          holder.author.setText("Lesson");
        }else if(modulesModel.getType().equalsIgnoreCase("assessment")){
            holder.author.setText("Assessment");
        }else {
            if(modulesModel.getAuthor().equals("null")){
                holder.author.setText(new PrefManager(mContext).getCourseauthor());
            }else {
                holder.author.setText("By"+modulesModel.getAuthor());

            }
        }

        if(ModulesActivity.open_desc_status.get(position).equalsIgnoreCase("close")){
            holder.description_relative_module.setVisibility(View.GONE);
            holder.topic_plus_sign.setBackgroundResource(R.drawable.add_icon);
            Log.e("bhn","close"+position);
        }else {
            holder.description_relative_module.setVisibility(View.VISIBLE);
            holder.topic_plus_sign.setBackgroundResource(R.drawable.minus_icon);
            Log.e("bhn","open"+position);
        }


       /* if(ModulesActivity.open_topic_status.get(position).equalsIgnoreCase("close")){
           // holder.topic_list_relative.setVisibility(View.GONE);
            holder.topic_plus_sign.setBackgroundResource(R.drawable.add_icon);
        }else {
           // holder.topic_list_relative.setVisibility(View.VISIBLE);
            holder.topic_plus_sign.setBackgroundResource(R.drawable.minus_icon);
        }
*/
       // holder.price_module.setText("Price: 2300");
        holder.played_layout_module.setVisibility(View.GONE);










        holder.module_names.setText(modulesModel.getObj_name());
        Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.module_names.setTypeface(tf2);
        holder.topics_text.setTypeface(tf2);
        holder.author.setTypeface(tf2);
        holder.description_text_module.setTypeface(tf2);
        holder.price_module.setTypeface(tf2);
        holder.description_button_module.setTypeface(tf2);
        holder.topics.setTypeface(tf2);

        holder.ratingbar_module.setRating(Float.parseFloat(modulesModel.getRating()));
        if(modulesModel.getPrice().equalsIgnoreCase("0")){
            holder.price_module.setText("Free");

        }else {
            holder.price_module.setText("₹ "+modulesModel.getPrice());

        }

      /*  holder.description_linear_module.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")) {
                    ModulesActivity.clickelement(modulesModel.getChildrens());
                    new PrefManager(mContext).saveModuleid(String.valueOf(modulesModel.getId()));

                    String status_is=ModulesActivity.open_desc_status.get(position);
                    if(status_is.equalsIgnoreCase("close")){
                        ModulesActivity.open_desc_status.add(position,"open");
                        holder.description_relative_module.setVisibility(View.VISIBLE);
                        holder.topic_plus_sign.setBackgroundResource(R.drawable.minus_icon);
                    }else {
                        ModulesActivity.open_desc_status.add(position,"close");
                        holder.description_relative_module.setVisibility(View.GONE);
                        holder.topic_plus_sign.setBackgroundResource(R.drawable.add_icon);
                    }

                }else {

                }
            }
        });*/


      holder.description_text_module.setText(modulesModel.getDesc());

/*
        holder.main_fr.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN  &&(!modulesModel.getIs_locked().equalsIgnoreCase("locked") || (modulesModel.getType().equalsIgnoreCase("topic") && !modulesModel.getIs_locked().equalsIgnoreCase("locked")) ||(modulesModel.getType().equalsIgnoreCase("lesson")&&!modulesModel.getIs_locked().equalsIgnoreCase("locked"))) ) {




                    holder.main_fr.setBackgroundColor(Color.parseColor("#ceffffff"));

                        new CountDownTimer(150, 150) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                holder.main_fr.setBackgroundResource(R.drawable.rounded_corner_courselist_items);


                            }
                        }.start();


                }

                return false;

            }



        });*/
        holder.main_fr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ModulesActivity.mLastClickTime  = SystemClock.elapsedRealtime();
                if(!modulesModel.getIs_locked().equalsIgnoreCase("locked")) {

                    if (connectionUtils.isConnectionAvailable()) {
                        if (modulesModel.getType().equalsIgnoreCase("lesson")) {
                            //module
                            new PrefManager(mContext).saveTargetid(String.valueOf(modulesModel.getId()));
                            new PrefManager(mContext).saveModuleid(String.valueOf(modulesModel.getId()));
                            String status_is=ModulesActivity.open_desc_status.get(position);
                            if(status_is.equalsIgnoreCase("close")){

                                for(int i=0;i<ModulesActivity.open_desc_status.size();i++){
                                    if(i==position){
                                        ModulesActivity.open_desc_status.set(i,"open");

                                    }else {
                                        ModulesActivity.open_desc_status.set(i,"close");
                                    }
                                }


                               /* holder.description_relative_module.setVisibility(View.VISIBLE);
                                holder.topic_plus_sign.setBackgroundResource(R.drawable.minus_icon);*/
                             //   ModulesActivity.clickelement(modulesModel.getChildrens(),position);
                            }else {

                                for(int i=0;i<ModulesActivity.open_desc_status.size();i++){
                                    if(i==position){
                                        ModulesActivity.open_desc_status.set(i,"close");

                                    }else {
                                        ModulesActivity.open_desc_status.set(i,"close");
                                    }
                                }
                                /*holder.description_relative_module.setVisibility(View.GONE);
                                holder.topic_plus_sign.setBackgroundResource(R.drawable.add_icon);*/
                            }
                            ModulesActivity.clickelement(modulesModel.getChildrens(),position);

                        }else {
                            /*if (modulesModel.getType().equalsIgnoreCase("assessment")) {
                                //assessment
                                ModulesActivity.sendLessonsStartData(modulesModel.getId(), modulesModel.getOrderid(), "assess", "ass");
                                new PrefManager(mContext).saveinnermoduleid(String.valueOf(modulesModel.getId()));
                                new PrefManager(mContext).saveorderid(String.valueOf(modulesModel.getOrderid()));
                                Log.e("orderidis",""+modulesModel.getOrderid());
                                new PrefManager(mContext).saveRatingtype("assessment_group");
                                new PrefManager(mContext).saveAsstype("Ass");
                                Intent intent = new Intent(mContext, HomeActivity.class);
                                intent.putExtra("tabs", "normal");

                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                mContext.startActivity(intent);

                                if (mContext instanceof Activity) {
                                    ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                    //((Activity) mContext).finish();
                                }

                            } else {

                                if (!modulesModel.getIs_locked().equalsIgnoreCase("locked")) {
                                    new PrefManager(mContext).saveinnermoduleid(String.valueOf(modulesModel.getId()));
                                    new PrefManager(mContext).saveorderid(String.valueOf(modulesModel.getOrderid()));
                                    if (modulesModel.getLms_url().length() > 5) {
                                        //lms

                                        ModulesActivity.sendLessonsStartData(modulesModel.getId(), modulesModel.getOrderid(), "topic", "lms");
                                        new PrefManager(mContext).saveRatingtype(modulesModel.getType());
                                        new PrefManager(mContext).savestatusforFeedback(modulesModel.getIs_locked());

                                        Log.e("status", "last lesson");
                                        Intent intent = new Intent(mContext, WebviewVideoActivity.class);
                                        intent.putExtra("link", modulesModel.getLms_url());
                                        mContext.startActivity(intent);
                                    } else {
                                        //simulation
                                        ModulesActivity.sendLessonsStartData(modulesModel.getId(), modulesModel.getOrderid(), "topic", "sm");

                                        new PrefManager(mContext).saveRatingtype(modulesModel.getType());
                                        new PrefManager(mContext).savestatusforFeedback(modulesModel.getIs_locked());
                                        Log.e("status", "last lesson");
                                        Log.e("status", modulesModel.getChildrens());
                                        Intent i = new Intent(mContext, SimulationStartPageActivity.class);
                                        try {
                                            JSONArray jsonObject = new JSONArray(modulesModel.getChildrens());
                                            for (int in = 0; in < jsonObject.length(); in++) {
                                                JSONObject sim_model = jsonObject.getJSONObject(in);

                                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                i.putExtra("sm_name", sim_model.getString("sm_name"));
                                                i.putExtra("sm_landing_page_image", sim_model.getString("sm_landing_page_image"));
                                                i.putExtra("sm_bg_color", sim_model.getString("sm_bg_color"));
                                                i.putExtra("sm_bg_image", sim_model.getString("sm_bg_image"));
                                                i.putExtra("sm_decision_bg_color", sim_model.getString("sm_decision_bg_color"));
                                                i.putExtra("sm_decision_bg_image", sim_model.getString("sm_decision_bg_image"));
                                                i.putExtra("sm_bg_sound", sim_model.getString("sm_bg_sound"));
                                                i.putExtra("sm_icon", sim_model.getString("sm_icon"));
                                                i.putExtra("sm_id", sim_model.getString("sm_id"));
                                                i.putExtra("sm_company", sim_model.getString("sm_company"));
                                                i.putExtra("sm_icon_image", sim_model.getString("sm_icon_image"));
                                                i.putExtra("gif_file", sim_model.getString("sm_bg_gif"));
                                                i.putExtra("sm_description", sim_model.getString("sm_description"));
                                                i.putExtra("sm_gif_loading_time", sim_model.getString("sm_gif_loading_time"));
                                                i.putExtra("sm_title_color", sim_model.getString("sm_title_color"));
                                                i.putExtra("sm_title_description_color", sim_model.getString("sm_title_description_color"));

                                            }
                                            mContext.startActivity(i);
                                            if (mContext instanceof Activity) {
                                                // ((Activity) mContext).finish();
                                                ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }

                                } else {
                                    ModulesActivity.nocourseavailableframe.setVisibility(View.VISIBLE);
                                    ModulesActivity.noavailabletext.setText(modulesModel.getWarn_text());
                                  //  Toast.makeText(mContext, "Currently this lesson is unavailable", Toast.LENGTH_SHORT).show();
                                }
                            }*/
                        }
                    } else {
                        Toast.makeText(mContext, "Please check your internet connection", Toast.LENGTH_SHORT).show();

                    }


                }else {
                    ModulesActivity.nocourseavailableframe.setVisibility(View.VISIBLE);
                    ModulesActivity.noavailabletext.setText(modulesModel.getWarn_text());
                  //  Toast.makeText(mContext, "Currently this lesson is unavailable", Toast.LENGTH_SHORT).show();

                }





            }


        });

    }


    @Override
    public int getItemCount() {
        return fungame.size();

    }
    RecyclerView.OnItemTouchListener mScrollTouchListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            int action = e.getAction();
            switch (action) {
                case MotionEvent.ACTION_MOVE:
                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    };
}

