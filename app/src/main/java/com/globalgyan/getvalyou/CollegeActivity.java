package com.globalgyan.getvalyou;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.globalgyan.getvalyou.cms.response.GetAllCollegeResponse;
import com.globalgyan.getvalyou.cms.response.ValYouResponse;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.model.College;

import java.util.ArrayList;
import java.util.List;

public class CollegeActivity extends AppCompatActivity implements GUICallback {
    private TextView txtNext = null;
    private EditText txtSearch = null;
    private CollegeAdapter industryAdapter = null;
    private List<College> collegeList = new ArrayList<>();
    private RecyclerView recyclerView = null;
    private String selected = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_college);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");

        txtNext = (TextView) findViewById(R.id.txtNext);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#201b81"));
        }
        txtNext.setTypeface(tf2);
        txtNext.setVisibility(View.GONE);
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        TextView txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMessage = new Intent();
                setResult(RESULT_CANCELED, intentMessage);

                finish();
                //overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


            }
        });


        recyclerView.setLayoutManager(new LinearLayoutManager(CollegeActivity.this));
       /* try {
            selected = getIntent().getExtras().getString("selected");
        } catch (Exception ex) {
            selected = null;
        }*/
        /*GetAllCollegeRequest request = new GetAllCollegeRequest();
        new AsyncTaskExecutor<ValYouRequest, Void, ValYouResponse>().execute(
                new RequestProcessorGet(CollegeActivity.this,
                        CollegeActivity.this, true), request);*/

        RetrofitRequestProcessor processor = new RetrofitRequestProcessor(CollegeActivity.this,CollegeActivity.this);
        processor.getAllColleges();


        industryAdapter = new CollegeAdapter(collegeList, this, selected);

        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentMessage = new Intent();
                intentMessage.putExtra("COLLEGENAME", industryAdapter.getStringBuffer());
                setResult(RESULT_OK, intentMessage);

                if (getParent() == null) {
                    setResult(RESULT_OK, intentMessage);
                } else {
                    getParent().setResult(RESULT_OK, intentMessage);
                }
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);


            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
                charSequence = charSequence.toString().trim().toLowerCase();

                final List<College> filteredList = new ArrayList<>();

                boolean itemFound = false;
                for (int i = 0; i < collegeList.size(); i++) {

                    final String text = collegeList.get(i).getName().toLowerCase();
                    if (text.contains(charSequence)) {
                        itemFound = true;
                        filteredList.add(collegeList.get(i));
                    }
                }

                if (!itemFound) {
                    College college = new College();
                    college.setName(charSequence.toString().toUpperCase());
                    college.set_id("new");
                    selected = charSequence.toString();
                    filteredList.add(college);
                }
                industryAdapter = new CollegeAdapter(filteredList, CollegeActivity.this, selected);
                recyclerView.setAdapter(industryAdapter);
                industryAdapter.notifyDataSetChanged();  // data set changed
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void requestProcessed(ValYouResponse guiResponse, RequestStatus status) {
        if(guiResponse!=null) {
            if (status.equals(RequestStatus.SUCCESS)) {

                GetAllCollegeResponse getAllCollegeResponse = (GetAllCollegeResponse) guiResponse;
                if (getAllCollegeResponse != null && getAllCollegeResponse.isStatus() && getAllCollegeResponse.getColleges() != null) {
                    List<College> colleges = getAllCollegeResponse.getColleges();
                    for (int i = 0; i < colleges.size(); i++) {
                        collegeList.add(colleges.get(i));
                    }
                    industryAdapter = new CollegeAdapter(collegeList, CollegeActivity.this, selected);
                    recyclerView.setAdapter(industryAdapter);
                    industryAdapter.notifyDataSetChanged();  // data set changed
                } else {
                    Toast.makeText(CollegeActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(CollegeActivity.this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intentMessage = new Intent();

        if (getParent() == null) {
            setResult(RESULT_CANCELED, intentMessage);
        } else {
            getParent().setResult(RESULT_CANCELED, intentMessage);
        }
        finish();
        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }
}
