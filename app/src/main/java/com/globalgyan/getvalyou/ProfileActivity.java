package com.globalgyan.getvalyou;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.cms.request.ValYouRequest;
import com.globalgyan.getvalyou.cms.task.RetrofitRequestProcessor;
import com.globalgyan.getvalyou.customwidget.SimpleDividerItemDecoration;
import com.globalgyan.getvalyou.fragments.MoreFragment;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.EducationalDetails;
import com.globalgyan.getvalyou.model.PriorExperience;
import com.globalgyan.getvalyou.model.Professional;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.HttpGet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;

/*import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;*/

/**
 * Created by NaNi on 18/10/17.
 */

public class ProfileActivity extends AppCompatActivity {
    private DataBaseHelper dataBaseHelper = null;
    ImageView backButton = null;
    PrefManager prefManager;
    private ProgressDialog dialog;
    String profile_string_nav;
    ImageView userimage,proplus;
    private RecyclerView recyclerPrefessional = null;
    private RecyclerView recyclerEducational = null;
    int responseCod;
    DefaultHttpClient httpClient;
    HttpPost httpPost;
    HttpGet httpGet;
    FontTextView timerrr, dqus,qno;
    int cqa=0;
    TextView textprofile;
    String profileImageUrl;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null,jObjeya=null;
    JSONArray data;
    JSONObject dataJSONobject;
    static String json = "",jsoneya="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    Bitmap bitmap = null;

    IntentFilter filter;
    FontTextView dn,gen,mob,emal,ind,dob,name,upper_heading;
    ImageView persedit,eduedit,profedit;
    boolean flag_netcheck=false;
    NestedScrollView scroller;
    Dialog progress_profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile2);
        scroller=(NestedScrollView) findViewById(R.id.scroller);

        scroller.fullScroll(ScrollView.FOCUS_UP);

        scroller.smoothScrollTo(0,0);

        dn=(FontTextView)findViewById(R.id.displayname);
        upper_heading=(FontTextView)findViewById(R.id.title);
        name=(FontTextView)findViewById(R.id.name);
        emal=(FontTextView)findViewById(R.id.emailpro);
        mob=(FontTextView)findViewById(R.id.phone);
        gen=(FontTextView)findViewById(R.id.gender);
        ind=(FontTextView)findViewById(R.id.industryFocus);
        dob=(FontTextView)findViewById(R.id.birthday);
        persedit=(ImageView)findViewById(R.id.persedit);
        eduedit=(ImageView)findViewById(R.id.eduedit);
        profedit=(ImageView)findViewById(R.id.profedit);
         filter = new IntentFilter("Set pic");
//           progress_profile = KProgressHUD.create(ProfileActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setDimAmount(0.7f)
//                .setCancellable(false);


        progress_profile = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        progress_profile.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress_profile.setContentView(R.layout.planet_loader);
        Window window1 = progress_profile.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        progress_profile.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progress_profile.setCancelable(false);
        GifView gifView2 = (GifView) progress_profile.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();


        profile_string_nav=getIntent().getStringExtra("nav_to");
        eduedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                if ( selectedid > 0 ) {
                    dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "edit");
                } else {
                    dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "edit");
                }
                Intent intent = new Intent(getApplicationContext(), ListEducationActivity.class);
                intent.putExtra("save","yes");
                intent.putExtra("kya","stud");
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                //finish();
               // finish();
            }
        });

        persedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Activity_Myself.class);
                intent.putExtra("forEdit", true);
                intent.putExtra("from","profile");
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });

        profedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                if ( selectedid > 0 ) {
                    dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "edit");
                } else {
                    dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_IS_OPEN_FOR_EDIT, "edit");
                }
                Intent intent = new Intent(getApplicationContext(), ListProfessionalDetailsActivity.class);
                intent.putExtra("kya","prof");
                intent.putExtra("save","yes");
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                // finish();
            }
        });

        recyclerPrefessional = (RecyclerView) findViewById(R.id.profrcv);
        recyclerEducational = (RecyclerView) findViewById(R.id.edurcv);



        recyclerPrefessional.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerEducational.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        dataBaseHelper = new DataBaseHelper(getApplicationContext());
        backButton = (ImageView) findViewById(R.id.bckbtn);

        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                /*if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    backButton.setBackgroundColor(Color.parseColor("#33ffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            backButton.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }*/
                return false;
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (profile_string_nav.equalsIgnoreCase("more")) {

                        Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                        intent.putExtra("tabs", "more_tab");

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        AppConstant.isalive = true;

                    } else {

                        Intent intent = new Intent(ProfileActivity.this, JourneyMainScreenActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                    intent.putExtra("tabs", "more_tab");

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    AppConstant.isalive = true;
                }
            }
        });

        textprofile=(TextView)findViewById(R.id.textprofile);
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        textprofile.setTypeface(typeface1);
        textprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (profile_string_nav.equalsIgnoreCase("more")) {

                        Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                        intent.putExtra("tabs", "more_tab");

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        AppConstant.isalive = true;

                    } else {

                        Intent intent = new Intent(ProfileActivity.this, JourneyMainScreenActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                    intent.putExtra("tabs", "more_tab");

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    AppConstant.isalive = true;
                }
            }
        });


        userimage=(ImageView)findViewById(R.id.userimage);
        proplus=(ImageView)findViewById(R.id.proplus);
        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
        profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
        prefManager = new PrefManager(ProfileActivity.this);
        if(prefManager.getPic()==" "){
            if(profileImageUrl==null){
                Log.e("lnowndnwpmdpw","lfnpnfpnnpewnpfpnfpnfpewnfewpffpnfpewnfpwnfnfp"+profileImageUrl);
                loadImag();
               // new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);

                // loadImage(profileImageUrl);
            }else{
              /*  final ProgressDialog progressDialog=new ProgressDialog(ProfileActivity.this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setTitle("Please wait...");
                progressDialog.show();
                String url="http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl;
                ImageRequest imgRequest = new ImageRequest(url,
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {
                                userimage.setImageBitmap(response);
                                progressDialog.cancel();
                            }
                        }, 0, 0, ImageView.ScaleType.FIT_XY, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        userimage.setBackgroundColor(Color.parseColor("#ff0000"));
                        error.printStackTrace();
                        progressDialog.cancel();
                    }
                });

                Volley.newRequestQueue(this).add(imgRequest);
              */
                progress_profile.show();
                final CheckInternetTask t917=new CheckInternetTask();
                t917.execute();

                new CountDownTimer(AppConstant.timeout, AppConstant.timeout) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        t917.cancel(true);

                        if (flag_netcheck) {
                            flag_netcheck = false;
                            new DownloadImage().execute(AppConstant.BASE_URL+"candidateMobileReadImage/"+profileImageUrl);

                        }else {
                            progress_profile.dismiss();
                            flag_netcheck=false;
                            Toast.makeText(ProfileActivity.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                        }
                    }
                }.start();

             //   new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/59fc25b66217bda954ad8dad");


            }
        }else{
            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            userimage.setImageBitmap(decodedByte);
        }


        userimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent it = new Intent(ProfileActivity.this,WebviewVideoActivity.class);
                startActivity(it);*/
                Intent it = new Intent(ProfileActivity.this,ProPic.class);
                it.putExtra("fromwhere","profile");
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });
        proplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(ProfileActivity.this,ProPic.class);
                it.putExtra("fromwhere","profile");
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        });


        String myName = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);
        if ( !TextUtils.isEmpty(myName) ) {
                dn.setText("Welcome "+myName);
        //        upper_heading.setText("Welcome "+myName);
            name.setText(myName);

        } else {
                dn.setText("");
            name.setText("");
           // upper_heading.setText("Welcome ");

        }

        String myGender = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER);
        if ( !TextUtils.isEmpty(myGender) ) {
            StringBuilder sb = new StringBuilder(myGender);
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));

            gen.setText(sb.toString());
        } else{
            gen.setText("");
        }


        String myDOB = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB);
        if ( !TextUtils.isEmpty(myDOB) ) {
            dob.setText(myDOB);
        } else
        {
            dob.setText("");
        }



        String myDesc = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY);
        if ( !TextUtils.isEmpty(myDesc) ) {
            ind.setText(myDesc);
        } else {
            ind.setText("");
        }




        String mobile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE);
        if ( !TextUtils.isEmpty(mobile) ) {
                mob.setText(mobile);
        } else {
                mob.setText("");

        }

        String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);
        if ( !TextUtils.isEmpty(email) ) {
            emal.setText(email);
        } else {
            emal.setText("");
        }







        ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
        ArrayList<PriorExperience> priorExperiences = dataBaseHelper.getPriorWorkExp();
        if ( professionals != null && professionals.size() > 0 ) {
            ProfileProfessionalAdapter professionalAdapter = new ProfileProfessionalAdapter(getApplicationContext(), professionals, false);
            recyclerPrefessional.setAdapter(professionalAdapter);
        } else if ( priorExperiences != null && priorExperiences.size() > 0 ) {
        } else {
        }
        recyclerPrefessional.addItemDecoration(new SimpleDividerItemDecoration(getResources()));

        ArrayList<EducationalDetails> arrayList = dataBaseHelper.getEducationalDetails();
        if ( arrayList != null && arrayList.size() > 0 ) {
            ProfileEducationAdapter educationalQualificationAdapter = new ProfileEducationAdapter(getApplicationContext(), arrayList, false);
            recyclerEducational.setAdapter(educationalQualificationAdapter);
        } else {
        }
        recyclerEducational.addItemDecoration(new SimpleDividerItemDecoration(getResources()));



    }


    @Override
    protected void onResume() {
        super.onResume();
        if(AppConstant.isalive){
            this.finish();
        }
        this.registerReceiver(mReceiver, filter);
        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
        String profileImageUrl = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
//        if(prefManager.getPic()==" "){
//            if(profileImageUrl==null){
//                Log.e("lnowndnwpmdpw","lfnpnfpnnpewnpfpnfpnfpewnfewpffpnfpewnfpwnfnfp"+profileImageUrl);
//                loadImag();
//                // new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);
//
//                // loadImage(profileImageUrl);
//            }else{
//                //new GetEywa().execute();
//                dialog = new ProgressDialog(ProfileActivity.this);
//                dialog.setMessage("Please wait...");
//                dialog.setIndeterminate(true);
//                dialog.setCancelable(true);
//                dialog.setCanceledOnTouchOutside(false);
//                dialog.show();
//                new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);
//
//            }
//        }else{
//            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//
//            userimage.setImageBitmap(decodedByte);
//        }
        String myName = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);
        if ( !TextUtils.isEmpty(myName) ) {
            dn.setText("Welcome "+myName);
            name.setText(myName);
        //    upper_heading.setText("Welcome "+myName);
        } else {
            dn.setText("");
            name.setText("");
        //    upper_heading.setText("Welcome ");
        }

        String myGender = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_GENDER);
        if ( !TextUtils.isEmpty(myGender) ) {
            StringBuilder sb = new StringBuilder(myGender);
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));

            gen.setText(sb.toString());
        } else{
            gen.setText("");
        }


        String myDOB = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_DOB);
        if ( !TextUtils.isEmpty(myDOB) ) {
            dob.setText(myDOB);
        } else
        {
            dob.setText("");
        }



        String myDesc = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_INDUSTRY);
        if ( !TextUtils.isEmpty(myDesc) ) {
            ind.setText(myDesc);
        } else {
            ind.setText("");
        }




        String mobile = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_MOBILE);
        if ( !TextUtils.isEmpty(mobile) ) {
            mob.setText(mobile);
        } else {
            mob.setText("");

        }

        String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);
        if ( !TextUtils.isEmpty(email) ) {
            emal.setText(email);
        } else {
            emal.setText("");
        }







        ArrayList<Professional> professionals = dataBaseHelper.getProfessionalDetails();
        ArrayList<PriorExperience> priorExperiences = dataBaseHelper.getPriorWorkExp();
        if ( professionals != null && professionals.size() > 0 ) {
            ProfileProfessionalAdapter professionalAdapter = new ProfileProfessionalAdapter(getApplicationContext(), professionals, false);
            recyclerPrefessional.setAdapter(professionalAdapter);
        } else if ( priorExperiences != null && priorExperiences.size() > 0 ) {
        } else {
        }
        recyclerPrefessional.addItemDecoration(new SimpleDividerItemDecoration(getResources()));

        ArrayList<EducationalDetails> arrayList = dataBaseHelper.getEducationalDetails();
        if ( arrayList != null && arrayList.size() > 0 ) {
            ProfileEducationAdapter educationalQualificationAdapter = new ProfileEducationAdapter(getApplicationContext(), arrayList, false);
            recyclerEducational.setAdapter(educationalQualificationAdapter);
        } else {
        }
        recyclerEducational.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
    }

    public void loadImage(String profileImageUrl){
        Glide.with(getApplicationContext())
                .load(ValYouRequest.HOME_IMAGES + profileImageUrl).asBitmap().placeholder(R.drawable.onbpic).error(R.drawable.onbpic)
                .fitCenter()
                .override(500, 500)
                .dontAnimate().dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(userimage);
    }

    public void loadImag(){
        Picasso.with(getApplicationContext())
                .load(AppConstant.BASE_URL+"candidateMobileReadImage/"+profileImageUrl)
                .placeholder(R.drawable.onbpic)   // optional
                .resize(500,500)                        // optional
                .into(userimage);
    }



    private class GetEywa extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                httpClient = new DefaultHttpClient();
                String url = AppConstant.BASE_URL+"getCandidateDetails/" + PreferenceUtils.getCandidateId(ProfileActivity.this);
                httpGet = new HttpGet(url);


                httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
                httpGet.setEntity(new UrlEncodedFormEntity(params));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                responseCod = httpResponse.getStatusLine().getStatusCode();
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                jsoneya = sb.toString();


                Log.e("JSONNOYU", jsoneya);

            } catch (Exception e) {
                e.getMessage();
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            try {
                jObjeya = new JSONObject(jsoneya);

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }




        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(responseCod==200){
                if(jObjeya.has("profileImageURL")){

                    try{
                        Log.e("PROFILE IMAE",jObjeya.getString("profileImageURL"));
                        long id = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_PROFILE);
                        dataBaseHelper.updateTableDetails(String.valueOf(id), DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL, jObjeya.getString("profileImageURL"));
                        loadImage(jObjeya.getString("profileImageURL"));
                    }catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }


                }
            }else{
                loadImag();
            }

        }

    }


    // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];


            try {
                // Download Image from URL
                InputStream input = new URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            result.compress(Bitmap.CompressFormat.PNG, 0, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            Log.e("ENCID",encodedImage);
            prefManager.savePic(encodedImage);
            Log.e("EN",prefManager.getPic());
            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            userimage.setImageBitmap(decodedByte);
            if(progress_profile.isShowing()){

                progress_profile.dismiss();
            }
           // dialog.cancel();
        }
    }


    private final BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            userimage.setImageBitmap(decodedByte);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mReceiver);
    }

    @Override
    public void onBackPressed() {
       /* Intent it= new Intent(ProfileActivity.this, HomeActivity.class);
        it.putExtra("tabs","normal");
        startActivity(it);*/

       try {
           if (profile_string_nav.equalsIgnoreCase("more")) {

               Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
               intent.putExtra("tabs", "more_tab");

               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               startActivity(intent);

               overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
               AppConstant.isalive = true;

           } else {

               Intent intent = new Intent(ProfileActivity.this, JourneyMainScreenActivity.class);

               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               startActivity(intent);
               finish();
               overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

           }


       }catch (Exception e){
           e.printStackTrace();
           Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
           intent.putExtra("tabs", "more_tab");

           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           startActivity(intent);

           overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
           AppConstant.isalive = true;
       }



    }


    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)ProfileActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }
}
