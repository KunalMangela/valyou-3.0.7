package com.globalgyan.getvalyou;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

@SuppressLint("AppCompatCustomView")
public class GridviewItem extends ImageView {
    public GridviewItem(Context context) {
        super(context);
    }

    public GridviewItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridviewItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredHeight();
        setMeasuredDimension(width, width);
    }
}