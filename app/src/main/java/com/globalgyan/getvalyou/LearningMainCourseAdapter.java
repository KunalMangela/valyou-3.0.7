package com.globalgyan.getvalyou;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.globalgyan.getvalyou.utils.ConnectionUtils;

import java.util.List;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Globalgyan on 06-12-2017.
 */

public class LearningMainCourseAdapter extends RecyclerView.Adapter<LearningMainCourseAdapter.MyViewHolder> {

    private Context mContext;
    private static List<String > title;
    private static List<String > company;
    ConnectionUtils connectionUtils;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView title,lower_label;
        public LinearLayout bg;


        public MyViewHolder(View view) {
            super(view);

            title = (FontTextView) view.findViewById(R.id.title);
            lower_label = (FontTextView) view.findViewById(R.id.lower_label);
            bg=(LinearLayout)view.findViewById(R.id.main_bg);

        }
    }


    public LearningMainCourseAdapter(Context mContext, List<String> title,List<String> company) {
        this.mContext = mContext;
        this.title = title;
        this.company=company;
        connectionUtils=new ConnectionUtils(mContext);
    }

    @Override
    public LearningMainCourseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_learning_layout, parent, false);



        return new LearningMainCourseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LearningMainCourseAdapter.MyViewHolder holder, final int position) {
        holder.title.setText(title.get(position));
        holder.lower_label.setText(company.get(position));
        /*if(position==0){
            holder.bg.setBackgroundResource(R.drawable.learn_video_bg_purple);
        }
        if(position==1){
            holder.bg.setBackgroundResource(R.drawable.learn_video_pink_bg);
        }*/



              /* if(gameModel.getDisplayname().equalsIgnoreCase("Listening")){
            holder.bg.setBackgroundResource(R.drawable.listening_skills_game);
        }else
        if(gameModel.getDisplayname().contains("Finance")){
            holder.bg.setBackgroundResource(R.drawable.business_awareness_game);
        }else
        if(gameModel.getDisplayname().contains("verbal")){
            holder.bg.setBackgroundResource(R.drawable.logical_reasoning_game);
        }else
        if(gameModel.getDisplayname().equalsIgnoreCase("Listening skills")){
            holder.bg.setBackgroundResource(R.drawable.listening_skills_game);
        }else {
            holder.bg.setBackgroundResource(R.drawable.electronic_game);

        }
       */
        // setAnimation(holder.itemView, position);


        // loading expertCardModel cover using Glide library

        //if user select for call from profile activity

    }
    @Override
    public int getItemCount() {
        return title.size();
    }
}
