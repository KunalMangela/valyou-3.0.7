package com.globalgyan.getvalyou;

public class LearningGroupCourseProgressModel {
    String lgname,course_dependent,islock,coursename,perecentage;
    int lgid,courseid;

    public String getPerecentage() {
        return perecentage;
    }

    public void setPerecentage(String perecentage) {
        this.perecentage = perecentage;
    }

    public String getLgname() {
        return lgname;
    }

    public void setLgname(String lgname) {
        this.lgname = lgname;
    }

    public String getCourse_dependent() {
        return course_dependent;
    }

    public void setCourse_dependent(String course_dependent) {
        this.course_dependent = course_dependent;
    }

    public String getIslock() {
        return islock;
    }

    public void setIslock(String islock) {
        this.islock = islock;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    public int getLgid() {
        return lgid;
    }

    public void setLgid(int lgid) {
        this.lgid = lgid;
    }

    public int getCourseid() {
        return courseid;
    }

    public void setCourseid(int courseid) {
        this.courseid = courseid;
    }


}
