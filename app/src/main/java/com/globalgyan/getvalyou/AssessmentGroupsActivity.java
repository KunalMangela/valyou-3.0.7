package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;

import org.androidannotations.annotations.rest.Get;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;

public class AssessmentGroupsActivity extends AppCompatActivity {
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;
    public static RecyclerView rc_ass_group;
    public static MaterialRefreshLayout swipe;
    ArrayList<AssessmentGroupModel> arrayListlg=new ArrayList<>();
    public static ArrayList<String> desc_ass_group=new ArrayList<>();
    static JSONObject jObj = null,jObjeya=null;
    HttpURLConnection urlConnection;
    public static String token="";
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="";
    int responseCode,responseCod,responseCo;
    public static Dialog alertDialog;
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    static InputStream is = null;
    PowerManager pm;
    boolean isPowerSaveMode;
    TextView textlghome;
    public static AssessmentGroupsAdapter assessmentGroupsAdapter;
    public static FontTextView no_assgroup_text;
    public static TextView textassgroup_heading;
    static public int batLevel;
    BatteryManager bm;
    static public AppConstant appConstant;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_groups);
        rc_ass_group=(RecyclerView)findViewById(R.id.assgroup_list);
        swipe=(MaterialRefreshLayout)findViewById(R.id.swipeassgroups);
        textlghome=(TextView)findViewById(R.id.textassgrouphome);
        red_planet = (ImageView) findViewById(R.id.red_planet);
        yellow_planet = (ImageView) findViewById(R.id.yellow_planet);
        purple_planet = (ImageView) findViewById(R.id.purple_planet);
        earth = (ImageView) findViewById(R.id.earth);
        no_assgroup_text=(FontTextView)findViewById(R.id.no_assgroup_text);
        no_assgroup_text.setVisibility(View.GONE);
        textassgroup_heading=(TextView)findViewById(R.id.textassgroupheading);
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        textlghome.setTypeface(typeface1);
        no_assgroup_text.setTypeface(typeface1);
        textassgroup_heading.setTypeface(typeface1);
        appConstant=new AppConstant(this);
        orange_planet = (ImageView) findViewById(R.id.orange_planet);
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        new PrefManager(AssessmentGroupsActivity.this).saveRatingtype("assessment_group");
        new PrefManager(AssessmentGroupsActivity.this).saveAsstype("Ass");
        new PrefManager(AssessmentGroupsActivity.this).saveNav("Ass");
        new PrefManager(AssessmentGroupsActivity.this).saveback_status("Assgroup");

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            isPowerSaveMode = pm.isPowerSaveMode();
        }
        animationstart();
        assessmentGroupsAdapter=new AssessmentGroupsAdapter(this,arrayListlg);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_ass_group.setLayoutManager(linearLayoutManager);
        rc_ass_group.setHasFixedSize(true);
        rc_ass_group.setAdapter(assessmentGroupsAdapter);
        swipe.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override

            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //refreshing...
                ConnectionUtils connectionUtils=new ConnectionUtils(AssessmentGroupsActivity.this);
                if(connectionUtils.isConnectionAvailable()){
                    arrayListlg.clear();
                    assessmentGroupsAdapter.notifyDataSetChanged();
                    new GetContacts().execute();
                }else {
                    swipe.finishRefresh();
                    Toast.makeText(AssessmentGroupsActivity.this,"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }

            }

            @Override

            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //load more refreshing...

            }
        });

        new CountDownTimer(100, 50) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                swipe.autoRefresh();

            }
        }.start();

        final ImageView back=(ImageView)findViewById(R.id.backbtnis_assgroups);
        back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    back.setBackgroundColor(Color.parseColor("#33ffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            back.setBackgroundColor(Color.parseColor("#00ffffff"));

                        }
                    }.start();

                }*/
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AssessmentGroupsActivity.this, MainPlanetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        });

        textlghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AssessmentGroupsActivity.this, MainPlanetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        });

    }
    private void animationstart() {
        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {
            red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f, 700f);
            red_planet_anim_1.setDuration(130000);
            red_planet_anim_1.setInterpolator(new LinearInterpolator());
            red_planet_anim_1.setStartDelay(0);

            red_planet_anim_1.start();

            red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700, 0f);
            red_planet_anim_2.setDuration(130000);
            red_planet_anim_2.setInterpolator(new LinearInterpolator());
            red_planet_anim_2.setStartDelay(0);

            red_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            red_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  yellow planet animation
            yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f, 1100f);
            yellow_planet_anim_1.setDuration(220000);
            yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_1.setStartDelay(0);
            yellow_planet_anim_1.start();

            yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f, 0f);
            yellow_planet_anim_2.setDuration(22000);
            yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_2.setStartDelay(0);

            yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  purple planet animation
            purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f, 100f);
            purple_planet_anim_1.setDuration(9500);
            purple_planet_anim_1.setInterpolator(new LinearInterpolator());
            purple_planet_anim_1.setStartDelay(0);
            purple_planet_anim_1.start();

            purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f, 0f);
            purple_planet_anim_2.setDuration(100000);
            purple_planet_anim_2.setInterpolator(new LinearInterpolator());
            purple_planet_anim_2.setStartDelay(0);


            purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            //  earth animation
            earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f, 1150f);
            earth_anim_1.setDuration(90000);
            earth_anim_1.setInterpolator(new LinearInterpolator());
            earth_anim_1.setStartDelay(0);
            earth_anim_1.start();

            earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f, 0f);
            earth_anim_2.setDuration(55000);
            earth_anim_2.setInterpolator(new LinearInterpolator());
            earth_anim_2.setStartDelay(0);

            earth_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            earth_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  orange planet animation
            orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f, 300f);
            orange_planet_anim_1.setDuration(56000);
            orange_planet_anim_1.setInterpolator(new LinearInterpolator());
            orange_planet_anim_1.setStartDelay(0);
            orange_planet_anim_1.start();

            orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f, 0f);
            orange_planet_anim_2.setDuration(75000);
            orange_planet_anim_2.setInterpolator(new LinearInterpolator());
            orange_planet_anim_2.setStartDelay(0);

            orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


        }
    }


    private class GetContacts extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {
            no_assgroup_text.setVisibility(View.GONE);
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        no_assgroup_text.setVisibility(View.VISIBLE);

                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        no_assgroup_text.setVisibility(View.VISIBLE);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipe.finishRefresh();
                        }catch (Exception e){

                        }
                        no_assgroup_text.setVisibility(View.VISIBLE);
                    }
                });
            }
            try {
                jObj = new JSONObject(result1);
                token=jObj.getString("access_token");
                token_type=jObj.getString("token_type");
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(responseCod != urlConnection.HTTP_OK){

                try{
                    swipe.finishRefresh();
                }catch (Exception e){

                }
                Toast.makeText(AssessmentGroupsActivity.this,"Error while loading user data",Toast.LENGTH_SHORT).show();
                no_assgroup_text.setVisibility(View.VISIBLE);
            }else {


                new GetAssessmentsGroups().execute();
            }
        }
    }
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
    //get groups
    private class GetAssessmentsGroups extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            no_assgroup_text.setVisibility(View.GONE);

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            HttpURLConnection urlConnection1 = null;
            try {


                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(AssessmentGroupsActivity.this))

                        .build().toString();

                try {
                    // httpClient = new DefaultHttpClient();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/get_assessments");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    int responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.e("Course", responseString);
                    result1 = responseString;





                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipe.finishRefresh();
                            }catch (Exception e){

                            }
                            no_assgroup_text.setVisibility(View.VISIBLE);
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipe.finishRefresh();
                            }catch (Exception e){

                            }
                            no_assgroup_text.setVisibility(View.VISIBLE);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                swipe.finishRefresh();
                            }catch (Exception e){

                            }
                            no_assgroup_text.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            desc_ass_group.clear();
            arrayListlg.clear();
            Log.e("respmsgassgroups",s);
            try{
                swipe.finishRefresh();
            }catch (Exception e){

            }


            String ss=new PrefManager(AssessmentGroupsActivity.this).getAuthToken().toString();


            if(s.contains("error")){
                Log.e("respmsg","error");
                // swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONObject js=new JSONObject(s);
                    String errormsg=js.getString("error");
                    AppConstant appConstant=new AppConstant(AssessmentGroupsActivity.this);
                    appConstant.showLogoutDialogue(errormsg);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("respmsg","errorexc");
                }

            }else {


                String resp=s;
                Log.e("respmsg","noerror");
                try {
                    JSONArray jsonArray=new JSONArray(s);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);
                        Log.e("respmsgis",String.valueOf(jsonObject));
                        AssessmentGroupModel assessmentGroupModel=new AssessmentGroupModel();
                        assessmentGroupModel.setName(jsonObject.getString("assessment_name"));
                        assessmentGroupModel.setStartdate(jsonObject.getString("start_date"));
                        assessmentGroupModel.setEnddate(jsonObject.getString("end_date"));
                        assessmentGroupModel.setCeid(jsonObject.getInt("CE_Id"));
                        assessmentGroupModel.setDescrption(jsonObject.getString("description"));
                        assessmentGroupModel.setUrl(jsonObject.getString("img_url"));
                        arrayListlg.add(assessmentGroupModel);
                        Log.e("respmsg","success");
                        assessmentGroupsAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(arrayListlg.size()==0){
                    no_assgroup_text.setVisibility(View.VISIBLE);
                }else {
                    no_assgroup_text.setVisibility(View.GONE);
                }
                for(int i=0;i<arrayListlg.size();i++){
                    desc_ass_group.add(i,"close");
                }
                assessmentGroupsAdapter.notifyDataSetChanged();

            }


        }
    }
    private String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(AssessmentGroupsActivity.this, MainPlanetActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
        finish();
    }
}
