package com.globalgyan.getvalyou.apphelper;

import android.content.Context;
import android.content.SharedPreferences;

import com.globalgyan.getvalyou.model.Topic_data_model;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by NaNi on 07/09/17.
 */

public class PrefManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "valyous";
    private static final String IS_PROFILE_SKIPPED = "IsProfileSkipped";
    private static final String IS_DETECTION_DONE = "IsDetectionDone";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String IS_PROFILE_COMPLETE = "PROFILE";



    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /*public void set_back_nav_from(String  nav){

        editor.putString("back_nav", nav);
        editor.commit();
    }

    public String get_back_nav_from(){

        return pref.getString("back_nav", "");

    }

    public void set_back_nav_from_course(String  nav){

        editor.putString("bck_nav", nav);
        editor.commit();
    }

    public String get_back_nav_from_course(){

        return pref.getString("bck_nav", "");

    }*/
    public void setprofilefaceid(String  faceid){

        editor.putString("mfaceid1", faceid);
        editor.commit();
    }

    public String getprofilefaceid(){

        return pref.getString("mfaceid1", "");

    }

    public void profile_skiped(boolean isSkiped){
        editor.putBoolean(IS_PROFILE_SKIPPED, isSkiped);
        editor.commit();

    }
    public boolean isProfileSkipped() {
        return pref.getBoolean(IS_PROFILE_SKIPPED, false);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {

        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }
    public void setIsProfileComplete(boolean isFirstTime) {

        editor.putBoolean(IS_PROFILE_COMPLETE, isFirstTime);
        editor.commit();
    }

    public void saveVideolistIndex(String date){
        editor.putString("index", date);
        editor.commit();
    }

    public String getVideolistIndex(){
        return pref.getString("index","");
    }



    public void setDescriptionVideo(String date){
        editor.putString("contri_desc", date);
        editor.commit();
    }

    public String getDescriptionVideo(){
        return pref.getString("contri_desc","");
    }




    public void saveCurrentWork(String date){
        editor.putString("cwork", date);
        editor.commit();
    }

    public String getCurrentWork(){
        return pref.getString("cwork","");
    }


    public void saveFirsttimeGame(String date){
        editor.putString("game", date);
        editor.commit();
    }

    public String getGetFirsttimegame(){
        return pref.getString("game","");
    }


    public boolean  getIsProfileComplete() {
        return pref.getBoolean(IS_PROFILE_COMPLETE,false);
    }

    public void saveDate(String date){
        editor.putString("DATE", date);
        editor.commit();
    }
    public void saveDat(String date){
        editor.putString("DATE", date);
        editor.apply();
    }
    public  String getDate(){
        return  pref.getString("DATE","FC");
    }


    public void setvideoFrom(String date){
        editor.putString("videofrom", date);
        editor.apply();
    }
    public  String getvideoFrom(){
        return  pref.getString("videofrom","");
    }



    public void saventfcount(int date){
        editor.putInt("count", date);
        editor.apply();
    }
    public int getntfcount(){
        return  pref.getInt("count",0);
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void savePic(String date){
        editor.putString("PIC", date);
        editor.commit();
    }
    public void savePi(String date){
        editor.remove("PIC");
        editor.apply();
    }

    public void isResultscreenpause(String date){
        editor.putString("ispause", date);
        editor.commit();
    }
    public String getResultscreenpause(){
        return pref.getString("ispause"," ");
    }

    public void setIsCapturing(String date){
        editor.putString("capture", date);
        editor.commit();
    }
    public String getIsCapturing(){
        return pref.getString("capture"," ");
    }

    public void isclicked(String date){
        editor.putString("click", date);
        editor.commit();
    }
    public String getclick(){
        return pref.getString("click"," ");
    }


    public String getPic(){
        return pref.getString("PIC"," ");
    }


    public void savePicRID(String date){
        editor.putString("RID", date);
        editor.commit();
    }
    public void savePiRID(String date){
        editor.remove("RID");
        editor.apply();
    }

    public void clearPref(){
        editor.clear().commit();
    }



    public String getPicRID(){
        return pref.getString("RID","");
    }

    public void saveUUID(String date){
        editor.putString("UUIDPRO",date);
        editor.commit();
    }

    public void saveUU(String date){
        editor.remove("UUIDPRO");
        editor.apply();
    }

    public String getUUID(){
        return pref.getString("UUIDPRO","");
    }

    public void saveRetryreponseString(String valuestring){
        editor.putString("retry_value",valuestring);
        editor.commit();
    }

    public String getRetryresponseString(){
        return pref.getString("retry_value","");
    }
    public void saveVideoResponseDataPath(String valuestring){
        editor.putString("file_path",valuestring);
        editor.commit();
    }

    public void saveAuthToken(String date){
        editor.putString("Auth",date);
        editor.apply();
    }

    public String getAuthToken(){
        return pref.getString("Auth","");
    }
    public String getVideoResponseDataPath(){
        return pref.getString("file_path","");
    }



    public void saveAsstype(String date){
        editor.putString("ass_type",date);
        editor.apply();
    }

    public String getAsstype(){
        return pref.getString("ass_type","");
    }


    public void saveNav(String date){
        editor.putString("nav_type",date);
        editor.apply();
    }

    public String getNav(){
        return pref.getString("nav_type","");
    }


    public void saveLearnNav(String date){
        editor.putString("nav_learn_type",date);
        editor.apply();
    }

    public String getLearnNav(){
        return pref.getString("nav_learn_type","");
    }

    public void saveCourseProgressalive(String date){
        editor.putString("isalive",date);
        editor.apply();
    }

    public String getCourseProgressalive(){
        return pref.getString("isalive","");
    }


    public void savetoken(String date){
        editor.putString("tok",date);
        editor.apply();
    }

    public String getToken(){
        return pref.getString("tok","");
    }
    public void saveRatingtype(String date){
        editor.putString("rate",date);
        editor.apply();
    }

    public String getRatingtype(){
        return pref.getString("rate","");
    }

    public void saveTargetid(String date){
        editor.putString("targetid",date);
        editor.apply();
    }

    public String getTargetid(){
        return pref.getString("targetid","");
    }

    public void saventfFirstlaunch(String date){
        editor.putString("ntf_first",date);
        editor.apply();
    }

    public String getntfFirstlaunch(){
        return pref.getString("ntf_first","");
    }

    public void saveCourseid(String date){
        editor.putString("courseid",date);
        editor.apply();
    }

    public String getCourseid(){
        return pref.getString("courseid","");
    }

    public void saveModuleid(String date){
        editor.putString("module_id",date);
        editor.apply();
    }

    public String getModuleid(){
        return pref.getString("module_id","");
    }

    public void saveinnermoduleid(String date){
        editor.putString("moduleid",date);
        editor.apply();
    }

    public String getinnermoduleid(){
        return pref.getString("moduleid","");
    }

    public void saveorderid(String date){
        editor.putString("orderid",date);
        editor.apply();
    }

    public String getorderid(){
        return pref.getString("orderid","");
    }
    public void saveCourseauthor(String date){
        editor.putString("author",date);
        editor.apply();
    }

    public String getCourseauthor(){
        return pref.getString("author","");
    }




    public void savelgid(String date){
        editor.putString("lgid",date);
        editor.apply();
    }

    public String getlgid(){
        return pref.getString("lgid","");
    }



    public void savecertiid(String date){
        editor.putString("certi",date);
        editor.apply();
    }

    public String getcertiid(){
        return pref.getString("certi","");
    }

    public void savecertiname(String date){
        editor.putString("certiname",date);
        editor.apply();
    }

    public String getcertiname(){
        return pref.getString("certiname","");
    }




    //ass end task
    public void savecertiid_ass(String date){
        editor.putString("certiid_ass",date);
        editor.apply();
    }

    public String getcertiid_ass(){
        return pref.getString("certiid_ass","");
    }


    public void savecourseid_ass(String date){
        editor.putString("courseid_ass",date);
        editor.apply();
    }

    public String getcourseid_ass(){
        return pref.getString("courseid_ass","");
    }

    public void savelgid_ass(String date){
        editor.putString("lgid_ass",date);
        editor.apply();
    }

    public String getlgid_ass(){
        return pref.getString("lgid_ass","");
    }

    public void savestatusforFeedback(String date){
        editor.putString("feedback_status",date);
        editor.apply();
    }

    public String getstatusforFeedback(){
        return pref.getString("feedback_status","");
    }



    public void saveLearningGroups(String date){
        editor.putString("l_groups",date);
        editor.apply();
    }

    public String getLearningGroups(){
        return pref.getString("l_groups","");
    }

    public void savecompulsaryfeedback(String date){
        editor.putString("feedback_compulsary",date);
        editor.apply();
    }

    public String getcompulsaryfeedback(){
        return pref.getString("feedback_compulsary","");
    }
    public void saveemailverificationString(String date){
        editor.putString("veri_email",date);
        editor.apply();
    }

    public String getemailverificationString(){
        return pref.getString("veri_email","");
    }


    public void saveAssgroupId(String date){
        editor.putString("ass_groupId",date);
        editor.apply();
    }

    public String getAssgroupId(){
        return pref.getString("ass_groupId","");
    }

    public void saveback_status(String date){
        editor.putString("back",date);
        editor.apply();
    }

    public String getgetback_status(){
        return pref.getString("back","");
    }


    public void set_sm_nav_from(String date){
        editor.putString("sm_save",date);
        editor.apply();
    }

    public String get_sm_nav_from(){
        return pref.getString("sm_save","");
    }

    //new changes
    public  void save_course_ids(ArrayList<String> list, String key){

        com.google.gson.Gson gson = new com.google.gson.Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();
    }
    public  ArrayList<String> get_course_array_ids(String key){

        com.google.gson.Gson gson = new com.google.gson.Gson();
        String json = pref.getString(key, null);
        Type type = new com.google.gson.reflect.TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }
    public  void save_topic_ids(ArrayList<Topic_data_model> list, String key){

        com.google.gson.Gson gson = new com.google.gson.Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();
    }
    public  ArrayList<Topic_data_model> get_topic_array_ids(String key){

        com.google.gson.Gson gson = new com.google.gson.Gson();
        String json = pref.getString(key, null);
        Type type = new com.google.gson.reflect.TypeToken<ArrayList<Topic_data_model>>() {}.getType();
        return gson.fromJson(json, type);

    }

    public void set_back_nav_from(String  nav){

        editor.putString("back_nav", nav);
        editor.commit();
    }

    public String get_back_nav_from(){

        return pref.getString("back_nav", "");

    }

    public void set_back_nav_from_course(String  nav){

        editor.putString("bck_nav", nav);
        editor.commit();
    }

    public String get_back_nav_from_course(){

        return pref.getString("bck_nav", "");

    }


    public void set_firstlaunchflag(String  nav){

        editor.putString("f_flag", nav);
        editor.commit();
    }

    public String get_firstlaunchflag(){

        return pref.getString("f_flag", "");

    }


    public void set_lang(String  nav){

        editor.putString("language", nav);
        editor.commit();
    }

    public String get_lang(){

        return pref.getString("language", "");

    }

    public void set_frkey(String  nav){

        editor.putString("fr", nav);
        editor.commit();
    }

    public String get_frkey(){

        return pref.getString("fr", "");

    }




    public void set_trailvrpath(String  nav){

        editor.putString("trailvr", nav);
        editor.commit();
    }

    public String get_trailvrpath(){

        return pref.getString("trailvr", "");

    }

    public void set_typevr(String  nav){

        editor.putString("typevr", nav);
        editor.commit();
    }

    public String get_typevr(){

        return pref.getString("typevr", "");

    }

    public void set_gender(String  nav){

        editor.putString("gender", nav);
        editor.commit();
    }

    public String get_gender(){

        return pref.getString("gender", "");

    }


    public void detectionDone(boolean done){
        editor.putBoolean(IS_DETECTION_DONE, done);
        editor.commit();

    }
    public boolean is_detection_done() {
        return pref.getBoolean(IS_DETECTION_DONE, false);
    }

}


