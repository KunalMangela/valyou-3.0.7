package com.globalgyan.getvalyou.apphelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

import com.globalgyan.getvalyou.interfaces.SmsListener;

public class SmsReceiver extends BroadcastReceiver {

    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();

        Object[] pdus = (Object[]) data.get("pdus");

        for (int i = 0; i < pdus.length; i++) {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

            String sender = smsMessage.getDisplayOriginatingAddress();
            //You must check here if the sender is your provider and not another one with same text.

            Log.e("--SMS--", "sender=" + sender);
            if (!TextUtils.isEmpty(sender) && sender.contains("GLBGYN")) {
                String messageBody = smsMessage.getMessageBody();

                String[] list = messageBody.split(" ");
                if(mListener!=null) {
                    if (list.length > 0) {
                        mListener.messageReceived(list[0]);
                    } else
                        mListener.messageReceived(messageBody);
                }
            }
        }

    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
    public static void unBindListener() {
        mListener = null;
    }
}