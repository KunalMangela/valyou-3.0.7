package com.globalgyan.getvalyou.apphelper;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.Activity_Fib;
import com.globalgyan.getvalyou.CheckP;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.LanguageActivity;
import com.globalgyan.getvalyou.MainPlanetActivity;
import com.globalgyan.getvalyou.MasterSlaveGame.MSQ;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.Welcome;
import com.globalgyan.getvalyou.cms.response.LogOutResponse;
import com.globalgyan.getvalyou.fragments.AssessFragment;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.interfaces.GUICallback;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.FileUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.microsoft.projectoxford.face.FaceServiceRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.HomeActivity.appConstant;
import static com.globalgyan.getvalyou.HomeActivity.batLevel;
import static com.globalgyan.getvalyou.HomeActivity.isPowerSaveMode;
import static com.globalgyan.getvalyou.HomeActivity.token;
import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;
import static com.thefinestartist.utils.content.ContextUtil.getFilesDir;
import static com.thefinestartist.utils.content.ContextUtil.getPackageName;
import static com.thefinestartist.utils.content.ContextUtil.getSystemService;

/**
 * Created by techniche-v1 on 3/11/16.
 */

public class AppConstant {
    public static Context mcontext;
    public AppConstant(Activity mcontext) {
        this.mcontext=mcontext;
    }
    public static HttpURLConnection urlConnection;
    public static InputStream is;
    public static String Qs;
    public static int code3;
    public static Dialog alertDialogis;
    public String token_is;
    public static DataBaseHelper dataBaseHelper = null;
    public static final String IMAGE_URL = "http://www.voicendata.com/wp-content/uploads/2015/03/intel-office.jpg";
    public static final String DUMMY_URL = "http://130.211.52.161/tradeo-content/themes/nucleare-pro/images/no-image-box.png";
    public static final int MYJOB_VIEWED = 0;
    public static final int MYJOB_SAVED = 1;
    public static final int MYJOB_APPLIED = 2;
    public static final boolean response_retry =false;
    public static boolean isotpty_active=false;
    public static boolean ishome_active=false;
    public static boolean isalive=false;
    public static Dialog progressDialog;
    public static final String GOOGLE_ANALYTICS_TRACKING_ID = "UA-101009482-1";
    public static boolean flag_netcheck=false,check_timeout_flag=false;
    public static String verifythrough="";
    static JSONObject jObj = null,jObjeya=null;
    JSONArray data;
    JSONObject dataJSONobject;
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int responseCode,responseCod,responseCo;

    //testUrl
   /* public static final String BASE_URL = "http://testadmin.getvalyou.com/api/";
    public static final String Ip_url = "http://ec2-13-126-183-85.ap-south-1.compute.amazonaws.com/";
*/
    //live url
    public static final String BASE_URL = "http://admin.getvalyou.com/api/";
    public static final String Ip_url = "http://35.154.93.176/";


    //currently used for simulationlist and waterfall
    public static final String Test_url = "http://ec2-13-126-183-85.ap-south-1.compute.amazonaws.com/";





    //"http://34.199.13.140/api/";//"http://admin.getvalyou.com/api/";//"http://admin.getvalyou.com/api/";//"http://34.199.13.140/api/";//"http://admin.getvalyou.com/api/";
    public static final String GAME_BASE_URL = "http://valyou.holodeckus.com/Api/";
    public static final String OTP_API_KEY = "3834f2b3-0a20-11e7-9462-00163ef91450";
    public static final String OTP_BASE_URL = "http://2factor.in/API/V1/" + OTP_API_KEY + "/SMS/";
    public static final String VIDEO_URL = BASE_URL + "candidateMobileSeeVideoProfileFile/";
    //public static String FR_key="0b503a58189d473ab2102b8aa899418b";
    public static String FR_end_points="https://eastus.api.cognitive.microsoft.com/face/v1.0";


    public static JSONObject socialData = null;

    // public static boolean uploadFiles = false;


    public static boolean OTP_FROM_SIGNUP = false;

    public static boolean GAME_COUNT_RECEIVED = false;

    public static boolean IS_ASSISTED_VIDEO = false;

    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
    public static File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), "valYou");
    public static String outputFile = mediaStorageDir.getPath() + File.separator + "videoProfile.mp4";
    public static String tempFile = mediaStorageDir.getPath() + File.separator + "videoProfileTemp.mp4";
    public static String stillFile = mediaStorageDir.getPath() + File.separator + "profile.png";

    public static String OPEN_TOK_API_KEY = "";
    public static long timeout=3000;
    public static String OPEN_TOK_SESSION_ID = "";

    public static String OPEN_TOK_TOKEN = "";


 Dialog alertDialog;

    ObjectAnimator  astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;


    public boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

    }


    public class CheckInternetTask extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {

            Log.e("statusvalue", "pre");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("statusvalue", s);
            if (s.equals("true")) {
                flag_netcheck = true;
            } else {
                flag_netcheck = false;
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (hasActiveInternetConnection()) {
                return String.valueOf(true);
            } else {
                return String.valueOf(false);
            }

        }

        public boolean hasActiveInternetConnection() {
            if (isInternetAvailable()) {
                Log.e("status", "network available!");
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    //  urlc.setConnectTimeout(3000);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("status", "Error checking internet connection", e);
                }
            } else {
                Log.e("status", "No network available!");
            }
            return false;
        }


    }

    public String getDevice() {
        String deviceName = Build.MANUFACTURER + " " + Build.MODEL;

        return deviceName;
    }

    public void showLogoutDialogue(String errormsg) {
        Log.e("respmsg","dialogue");
//        AlertDialog.Builder builder1 = new AlertDialog.Builder(mcontext);
//        builder1.setTitle("Alert");
//        builder1.setMessage(errormsg);
//        builder1.setCancelable(false);
//
//        builder1.setPositiveButton(
//                "OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                        String params="candidateId="+ PreferenceUtils.getCandidateId(mcontext)+"&FCMToken="+PreferenceUtils.getFcmToken(mcontext);
//                        new LogOutTask().execute(params);
//                    }
//                });
//        AlertDialog alert11 = builder1.create();
//        alert11.show();


        /*final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                mcontext, R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View dialogView = inflater.inflate(R.layout.logout_error_alert, null);

        dialogBuilder.setView(dialogView);


        alertDialog = dialogBuilder.create();


//                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

        alertDialog.show();*/
        alertDialog = new Dialog(mcontext, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.logout_error_alert);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText(errormsg);

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);
                if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0) {

                    astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                    astro_inx.setDuration(300);
                    astro_inx.setInterpolator(new LinearInterpolator());
                    astro_inx.setStartDelay(0);
                    astro_inx.start();
                    astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                    astro_iny.setDuration(300);
                    astro_iny.setInterpolator(new LinearInterpolator());
                    astro_iny.setStartDelay(0);
                    astro_iny.start();

                    alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                    alien_inx.setDuration(300);
                    alien_inx.setInterpolator(new LinearInterpolator());
                    alien_inx.setStartDelay(0);
                    alien_inx.start();
                    alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                    alien_iny.setDuration(300);
                    alien_iny.setInterpolator(new LinearInterpolator());
                    alien_iny.setStartDelay(0);
                    alien_iny.start();
                }
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        alertDialog.dismiss();
                        String params = "candidateId=" + PreferenceUtils.getCandidateId(mcontext) + "&FCMToken=" + PreferenceUtils.getFcmToken(mcontext);
                        new LogOutTask().execute(params);

                    }
                }.start();


            }
        });

        try{
            alertDialog.show();
        }
        catch (Exception e)
        {}

    }



    public void showLogoutDialogueis(String errormsg) {
        Log.e("respmsg","dialogue");
        AlertDialog.Builder builder1 = new AlertDialog.Builder(mcontext);
        builder1.setTitle("Logout");
        builder1.setMessage(errormsg);
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        String params="candidateId="+ PreferenceUtils.getCandidateId(mcontext)+"&FCMToken="+PreferenceUtils.getFcmToken(mcontext);
                        new LogOutTask().execute(params);
                    }
                });

        builder1.setNegativeButton(
                "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private class LogOutTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            createProgress();
        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/EndSession");*/


                    URL urlToRequest = new URL(AppConstant.BASE_URL+"candidateMobileAppLogout");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            urlkk[0].getBytes().length);
                    urlConnection.setRequestMethod("PUT");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(urlkk[0]);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                    Log.e("APPResponse2", ""+code3);
                }
                catch (Exception e) {
                    e.printStackTrace();

                }
                if(code3==200){
                    String responseString = readStream1(is);
                    Log.e("APPResponse2", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
             try{
                progressDialog.dismiss();
                Log.e("APPresult", ""+result);

                }catch (Exception e)
                {
                e.printStackTrace();
                }

            if(result.contains("Sucess")){
                try {

                    PreferenceUtils.clearAll(mcontext);
                    FileUtils.deleteFileFromSdCard(AppConstant.outputFile);
                    FileUtils.deleteFileFromSdCard(AppConstant.stillFile);
                    new PrefManager(mcontext).saveemailverificationString("notverified");

                } catch (Exception ex) {

                }

                DataBaseHelper dataBaseHelper = new DataBaseHelper(mcontext);
                dataBaseHelper.deleteAllTables();

                try {
                    File folder = new File(/*Environment.getExternalStorageDirectory()*/getFilesDir() +
                            File.separator + "SimulationData");
                    if (folder.exists()) {
                        deleteDirectory(folder);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                try {
                    File folder_outside = new File(Environment.getExternalStorageDirectory() +
                            File.separator + "SimulationData");
                    if (folder_outside.exists()) {
                        deleteDirectory(folder_outside);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                try{
                    NotificationManager notificationManager =
                            (NotificationManager) mcontext.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancelAll();
                }catch (Exception e){
                    e.printStackTrace();
                }
                new PrefManager(mcontext).saveemailverificationString("notverified");
                new PrefManager(mcontext).savePic(" ");
                PrefManager prefManager = new PrefManager(mcontext);
                prefManager.clearPref();
                prefManager.setFirstTimeLaunch(false);

                SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(mcontext).edit();
                prefEditor.remove("login_success");
                prefEditor.apply();
                //do changes
                Intent intent = new Intent(mcontext, Welcome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.putExtra("gotologinpage","loginpage");
                mcontext.startActivity(intent);
                if (mcontext instanceof Activity) {
                    ((Activity) mcontext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                }



            }else {
                Toast.makeText(mcontext,"SERVER ERROR",Toast.LENGTH_SHORT).show();
            }

        }
    }
    public static void createProgress() {
        progressDialog = new Dialog(mcontext, android.R.style.Theme_Translucent_NoTitleBar);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.planet_loader);
        Window window = progressDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        progressDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



        GifView gifView1 = (GifView) progressDialog.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
    public static String readStream1(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qs = sb.toString();


            Log.e("JSONStrr", Qs);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qs;
    }
    public static void deleteDirectory( File dir )
    {

        if ( dir.isDirectory() )
        {
            String [] children = dir.list();
            for ( int i = 0 ; i < children.length ; i ++ )
            {
                File child =    new File( dir , children[i] );
                if(child.isDirectory()){
                    deleteDirectory( child );
                    child.delete();
                }else{
                    child.delete();

                }
            }
            dir.delete();
        }
    }
    public static String getversionofApp(){
        String version="";
        try {
            PackageInfo pInfo = mcontext.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            return  version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
    public static String getAndroidVersionApp() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.e("RELEASE",release);
        return release;
    }
    public static String getLocalIpAddressApp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    //create loader

    //verification mail
    public class CheckVerificationmail extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();



        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {
                    dataBaseHelper = new DataBaseHelper(mcontext);
                    String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("email",email)
                            .build().toString();

                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/check_email_verified");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " +token_is);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                    Log.v("Response2", ""+code3);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    try{
                        ((Activity)mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
                if(code3==200){
                    String responseString = readStream1(is);
                    Log.e("Response2", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("veri",result);

            try {
                progressDialog.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }

            try {
                JSONObject jsonObject=new JSONObject(result);
                if(jsonObject.getString("message").equalsIgnoreCase("Email not verified.")){
                    if(verifythrough.equalsIgnoreCase("main")){
                        showverificationDialogueMain();
                    }else {
                        showverificationDialogue();
                    }

                    Log.e("veri","not verify");
                }else {
                    new PrefManager(mcontext).saveemailverificationString("verified");
                    Log.e("veri","verify");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    //send verification link
    public class SendVerificationmail extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();



        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {

                try {
                    dataBaseHelper = new DataBaseHelper(getApplicationContext());
                    String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);
                    String myName = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_NAME);
                    String candidateId = PreferenceUtils.getCandidateId(mcontext);
                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("email",email)
                            .object("name",myName)
                            .object("U_Id",candidateId)
                            .build().toString();

                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/send_verify_email");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " +token_is);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                    Log.v("Response2", ""+code3);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if(code3==200){
                    String responseString = readStream1(is);
                    Log.e("Response2", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("veriresend",result);
            Log.e("veri","resend");
        }
    }

    public  void showverificationDialogue() {
        alertDialogis = new Dialog(mcontext, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialogis.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogis.setContentView(R.layout.verify_email_layout);
        Window window = alertDialogis.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        alertDialogis.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView resesndbtn = (FontTextView)alertDialogis.findViewById(R.id.resesndbtn);

        resesndbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionUtils connectionUtils=new ConnectionUtils(mcontext);
                if(connectionUtils.isConnectionAvailable()) {
                   SendVerificationmail SendVerificationmail = new SendVerificationmail();
                    SendVerificationmail.execute();
                    alertDialogis.dismiss();
                   /* Intent intent = new Intent(mcontext, MainPlanetActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    if (mcontext instanceof Activity) {
                        ((Activity) mcontext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        ((Activity) mcontext).finish();
                        ((Activity) mcontext).startActivity(intent);
                    }*/
                }else {
                    Toast.makeText(mcontext,"Please check your internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });

        resesndbtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    resesndbtn.setBackgroundResource(R.drawable.skip_click);
                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            resesndbtn.setBackgroundResource(R.drawable.skip_profile);
                        }
                    }.start();

                }


                return false;
            }
        });


        resesndbtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return false;
            }
        });

        try{
            alertDialogis.show();
        }
        catch (Exception e)
        {}

    }

    public void showverificationDialogueMain() {
        alertDialogis = new Dialog(mcontext, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialogis.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogis.setContentView(R.layout.verify_mainscreen_popup);
        Window window = alertDialogis.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        //alertDialogis.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView resesndbtn = (FontTextView)alertDialogis.findViewById(R.id.resesndbtn);
        FrameLayout frameLayout=(FrameLayout)alertDialogis.findViewById(R.id.frame_main);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogis.dismiss();
            }
        });
        resesndbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogis.dismiss();
            }
        });

        resesndbtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    resesndbtn.setBackgroundResource(R.drawable.skip_click);
                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            resesndbtn.setBackgroundResource(R.drawable.skip_profile);
                        }
                    }.start();

                }


                return false;
            }
        });


        resesndbtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return false;
            }
        });

        try{
            alertDialogis.show();
        }
        catch (Exception e)
        {}

    }






    public class GetTokenMain extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {

          createProgress();
          try {
              progressDialog.dismiss();
          }catch (Exception e){
              e.printStackTrace();
          }
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream1(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                try{
                    ((Activity)mcontext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                try{
                    ((Activity)mcontext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                try{
                    ((Activity)mcontext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            try {
                jObj = new JSONObject(result1);
                token_is=jObj.getString("access_token");
                try {
                    new PrefManager(mcontext).savetoken(token_is);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                token_type=jObj.getString("token_type");
            } catch (Exception e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if (responseCod != urlConnection.HTTP_OK) {

                    try{

                                progressDialog.dismiss();


                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                } else {

                       new GetIntroInfo().execute();

                }

            }catch (Exception e){

            }
        }
    }

    public class GetTokenAss extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {

            createProgress();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream1(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                try{
                    ((Activity)mcontext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                try{
                    ((Activity)mcontext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                try{
                    ((Activity)mcontext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            try {
                jObj = new JSONObject(result1);
                token_is=jObj.getString("access_token");
                try {
                    new PrefManager(mcontext).savetoken(token_is);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                token_type=jObj.getString("token_type");
            } catch (Exception e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if (responseCod != urlConnection.HTTP_OK) {

                    try{

                        progressDialog.dismiss();


                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                } else {

                        verifythrough="ass";
                        CheckVerificationmail checkVerificationmail = new CheckVerificationmail();
                        checkVerificationmail.execute();


                }

            }catch (Exception e){

            }
        }
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    public void getFrkey(){
        new GetTokenMain().execute();
    }
    public void getTokenAss(){
        new GetTokenAss().execute();
    }


    private class GetIntroInfo extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {
                    // httpClient = new DefaultHttpClient();
                    // httpPost = new HttpPost("http://35.154.93.176/Player/TodayGames");


                    String jon = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", "")
                            .object("language_name", ""+new PrefManager(mcontext).get_lang())
                            .build().toString();



                    URL urlToRequest = new URL(AppConstant.Ip_url+"GameSettings");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jon.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token_is);
                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jon);
                    wr.flush();

                    responseCode = urlConnection.getResponseCode();


                    is= urlConnection.getInputStream();

                    String responseString = readStream(is);
                    Log.v("Response", responseString);
                    result1 = responseString;



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    try{
                        ((Activity)mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    try{
                        ((Activity)mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    try{
                        ((Activity)mcontext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try{

                        progressDialog.dismiss();
            }catch (Exception ex){
                ex.printStackTrace();
            }

            try {



                JSONObject jsonObject1=new JSONObject(s);

                String frkey=jsonObject1.getString("FR_Key");
                Log.e("frkey",frkey);
                new PrefManager(mcontext).set_frkey(frkey);
                MainPlanetActivity.getFaceClientObj();
//                SpannableStringBuilder SS1 = new SpannableStringBuilder(intro1_title1_string);
//                SS1.setSpan(new CustomTypefaceSpan("", tf2), 0, intro1_title1_string.length()-1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);



            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
    private String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {

                sb.append(line);
            }
            is.close();

            jso = sb.toString();


            Log.e("JSONStrr", jso);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return jso;
    }

}
