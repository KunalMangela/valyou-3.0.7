package com.globalgyan.getvalyou.apphelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by techniche-v1 on 20/10/16.
 */

public class DateUtils {

    public static String UtcToJavaDateFormat(String myUTC, SimpleDateFormat requiredDateFormat){
        String myDate = null;
        SimpleDateFormat existingUTCFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        existingUTCFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        try{
            Date getDate = existingUTCFormat.parse(myUTC);
            myDate = requiredDateFormat.format(getDate);
        }
        catch(ParseException ex){
            ex.printStackTrace();
        }
        return myDate;
    }


    public static long getDateForCalender(String myUTC){
        long returnDate = 0;
        SimpleDateFormat existingUTCFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        existingUTCFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        try{
            Date getDate = existingUTCFormat.parse(myUTC);
            returnDate = getDate.getTime();
        }
        catch(ParseException ex){
            ex.printStackTrace();
        }
        return returnDate;
    }
}
