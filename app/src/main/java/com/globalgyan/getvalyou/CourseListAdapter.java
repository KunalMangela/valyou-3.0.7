package com.globalgyan.getvalyou;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.simulation.SimulationStartPageActivity;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CourseListAdapter extends RecyclerView.Adapter<CourseListAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<CourseListUniversalmodel> fungame;
    ConnectionUtils connectionUtils;

    boolean flag_locked = false;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView course_names,author,certification_name,start,end,price,rate_us_button,certificationheaing,desc_arrow,description_text,play;

        ImageView lock,course_cover,seen_course;
        FrameLayout main_fr_is,certi_start_line,certi_end_line,description_button;
        LinearLayout description_linear;
        SimpleRatingBar ratingbar;
        ImageView certi_label_img;

        RelativeLayout certificatename_linear,description_relative,lock_layout;
        public MyViewHolder(View view) {
            super(view);
            course_names=(TextView)view.findViewById(R.id.coursename);
            course_cover=(ImageView)view.findViewById(R.id.course_cover);

            seen_course=(ImageView)view.findViewById(R.id.seen_course);
            play=(TextView)view.findViewById(R.id.play);

            author=(TextView)view.findViewById(R.id.author);
            certification_name=(TextView)view.findViewById(R.id.certification_name);
            certificationheaing=(TextView)view.findViewById(R.id.certificationheaing);
            start=(TextView)view.findViewById(R.id.starttext);
            description_text=(TextView)view.findViewById(R.id.description_text);
            desc_arrow=(TextView)view.findViewById(R.id.desc_arrow);
            certi_label_img=(ImageView)view.findViewById(R.id.certi_label_img);
            description_linear=(LinearLayout)view.findViewById(R.id.description_linear);
            description_relative=(RelativeLayout)view.findViewById(R.id.description_relative);
            certificatename_linear=(RelativeLayout)view.findViewById(R.id.certificatename_linear);
            price=(TextView)view.findViewById(R.id.price);
            ratingbar=(SimpleRatingBar)view.findViewById(R.id.ratingbar);
            end=(TextView)view.findViewById(R.id.endtext);
            rate_us_button=(TextView)view.findViewById(R.id.rate_us_button);
            description_button=(FrameLayout) view.findViewById(R.id.description_button);
            main_fr_is=(FrameLayout)view.findViewById(R.id.main_fr);
            certi_start_line=(FrameLayout)view.findViewById(R.id.certi_start_line);
            certi_end_line=(FrameLayout)view.findViewById(R.id.certi_end_line);
            lock_layout=(RelativeLayout)view.findViewById(R.id.lock_layout);
        }
    }


    public CourseListAdapter(Context mContext, ArrayList<CourseListUniversalmodel> fungame) {
        this.mContext = mContext;
        this.fungame = fungame;
        connectionUtils=new ConnectionUtils(mContext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.course_items, parent, false);



        return new MyViewHolder(itemView);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CourseListUniversalmodel courseListUniversalmodel=fungame.get(position);
        Typeface typeface3 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");

        holder.setIsRecyclable(false);
        holder.course_names.setSelected(true);
        holder.author.setSelected(true);
        holder.certificationheaing.setSelected(true);
        holder.course_names.setText(courseListUniversalmodel.getName());

        if(courseListUniversalmodel.getCost().equalsIgnoreCase("0")){
            holder.price.setText("Free");

        }else {
            holder.price.setText("₹ "+courseListUniversalmodel.getCost());

        }


        holder.start.setTypeface(typeface3,Typeface.BOLD);
        holder.end.setTypeface(typeface3,Typeface.BOLD);
        holder.certification_name.setTypeface(typeface3,Typeface.BOLD);
        holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
        holder.course_names.setTypeface(typeface3);
        holder.price.setTypeface(typeface3);
        holder.author.setTypeface(typeface3);
        holder.rate_us_button.setTypeface(typeface3);
        holder.description_text.setTypeface(typeface3);
        holder.description_text.setText(courseListUniversalmodel.getDesc_course());
       // holder.ratingbar.setRating(3.5f);
        holder.ratingbar.setRating(Float.parseFloat(courseListUniversalmodel.getRating()));
        Glide.with(mContext).load(courseListUniversalmodel.getUrl_ic())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.course_cover);
        /*if(position==0){
            Log.e("pos","0");
            if(!courseListUniversalmodel.getCerti_id().equalsIgnoreCase("0")){
                if(!CourseLIstActivity.arrayList_cerificate_ids.get(position).equalsIgnoreCase(CourseLIstActivity.arrayList_cerificate_ids.get(position+1))){
                    holder.certi_start_line.setVisibility(View.VISIBLE);
                    holder.certificatename_linear.setVisibility(View.VISIBLE);
                    holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                    holder.certi_end_line.setVisibility(View.VISIBLE);
                }else {

                    if(CourseLIstActivity.arrayList_lg_ids.get(position).equals(CourseLIstActivity.arrayList_lg_ids.get(position+1))){
                        holder.certi_start_line.setVisibility(View.VISIBLE);
                        holder.certificatename_linear.setVisibility(View.VISIBLE);
                        holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                        holder.certi_end_line.setVisibility(View.GONE);
                    }else {
                        holder.certi_start_line.setVisibility(View.VISIBLE);
                        holder.certificatename_linear.setVisibility(View.VISIBLE);
                        holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                        holder.certi_end_line.setVisibility(View.VISIBLE);
                    }


                }
            }else {
                holder.certi_start_line.setVisibility(View.GONE);
                holder.certi_end_line.setVisibility(View.GONE);
                holder.certificatename_linear.setVisibility(View.GONE);
            }
        }
        if(position==fungame.size()-1){
            Log.e("pos","1");
            if(!courseListUniversalmodel.getCerti_id().equalsIgnoreCase("0")){
                if(!CourseLIstActivity.arrayList_cerificate_ids.get(position).equalsIgnoreCase(CourseLIstActivity.arrayList_cerificate_ids.get(position-1))) {
                    holder.certi_end_line.setVisibility(View.VISIBLE);
                    holder.certi_start_line.setVisibility(View.VISIBLE);
                    holder.certificatename_linear.setVisibility(View.VISIBLE);
                    holder.certification_name.setText(courseListUniversalmodel.getCerti_name());

                }else {
                    if(CourseLIstActivity.arrayList_lg_ids.get(position).equals(CourseLIstActivity.arrayList_lg_ids.get(position-1))){
                        holder.certi_end_line.setVisibility(View.VISIBLE);
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                    }else {
                        holder.certi_end_line.setVisibility(View.VISIBLE);
                        holder.certi_start_line.setVisibility(View.VISIBLE);
                        holder.certificatename_linear.setVisibility(View.VISIBLE);
                    }




                }



            }else {
                holder.certi_start_line.setVisibility(View.GONE);
                holder.certi_end_line.setVisibility(View.GONE);
                holder.certificatename_linear.setVisibility(View.GONE);
            }
        }

        if((position!=0)&&(position!=fungame.size()-1)){
            Log.e("pos","other");
            if(!courseListUniversalmodel.getCerti_id().equalsIgnoreCase("0")){
                String A,B,C;
                String A_lg,B_lg,C_lg;

                A=CourseLIstActivity.arrayList_cerificate_ids.get(position-1);
                B=CourseLIstActivity.arrayList_cerificate_ids.get(position);
                C=CourseLIstActivity.arrayList_cerificate_ids.get(position+1);

                A_lg=CourseLIstActivity.arrayList_lg_ids.get(position-1);
                B_lg=CourseLIstActivity.arrayList_lg_ids.get(position);
                C_lg=CourseLIstActivity.arrayList_lg_ids.get(position+1);


                if((!A.equals(B))&&(!B.equals(C))){
                    holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                    holder.certi_start_line.setVisibility(View.VISIBLE);
                    holder.certi_end_line.setVisibility(View.VISIBLE);
                    holder.certificatename_linear.setVisibility(View.VISIBLE);
                }

                if((A.equals(B))&&(B.equals(C))){
                    if((A_lg.equals(B_lg))&&(B_lg.equals(C_lg))){
                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                    }else {
                        if(A_lg.equals(B_lg)){
                            holder.certi_start_line.setVisibility(View.GONE);
                            holder.certi_end_line.setVisibility(View.GONE);
                            holder.certificatename_linear.setVisibility(View.GONE);
                        }
                        if(C_lg.equals(B_lg)){
                            holder.certi_start_line.setVisibility(View.GONE);
                            holder.certi_end_line.setVisibility(View.GONE);
                            holder.certificatename_linear.setVisibility(View.GONE);
                        }
                        if(!A_lg.equals(B_lg)){
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certi_end_line.setVisibility(View.GONE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                        }
                        if(!C_lg.equals(B_lg)){
                            holder.certi_start_line.setVisibility(View.GONE);
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.GONE);
                        }
                        if((!A_lg.equals(B_lg))&&(!B_lg.equals(C_lg))){
                            holder.certi_start_line.setVisibility(View.VISIBLE);
                            holder.certi_end_line.setVisibility(View.VISIBLE);
                            holder.certificatename_linear.setVisibility(View.VISIBLE);
                        }

                    }


                }

                if((!A.equals(B))&&(B.equals(C))){
                    if(B_lg.equals(C_lg)){
                        holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                        holder.certi_start_line.setVisibility(View.VISIBLE);
                        holder.certi_end_line.setVisibility(View.GONE);
                        holder.certificatename_linear.setVisibility(View.VISIBLE);
                    }else {
                        holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                        holder.certi_start_line.setVisibility(View.VISIBLE);
                        holder.certi_end_line.setVisibility(View.VISIBLE);
                        holder.certificatename_linear.setVisibility(View.VISIBLE);
                    }


                }

                if((A.equals(B))&&(!B.equals(C))){
                   // holder.certification_name.setText(courseListUniversalmodel.getCerti_name());
                    if(A_lg.equals(B_lg)){

                        holder.certi_start_line.setVisibility(View.GONE);
                        holder.certi_end_line.setVisibility(View.VISIBLE);
                        holder.certificatename_linear.setVisibility(View.GONE);
                    }else {

                        holder.certi_start_line.setVisibility(View.VISIBLE);
                        holder.certi_end_line.setVisibility(View.VISIBLE);
                        holder.certificatename_linear.setVisibility(View.VISIBLE);
                    }
                }

            }else {
                holder.certi_start_line.setVisibility(View.GONE);
                holder.certi_end_line.setVisibility(View.GONE);
                holder.certificatename_linear.setVisibility(View.GONE);
            }
        }*/
        if (courseListUniversalmodel.getType().equalsIgnoreCase("lesson")) {
            if(!courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("locked")){
                flag_locked = false;
               // holder.lock.setImageResource(R.drawable.unlock);
                holder.main_fr_is.setAlpha(1f);
                holder.lock_layout.setVisibility(View.GONE);
          //      holder.main_fr_is.setBackgroundResource(R.drawable.ass_ripple);
            }else {
                flag_locked = true;
               // holder.lock.setImageResource(R.drawable.lock);
                holder.lock_layout.setVisibility(View.VISIBLE);
               // holder.main_fr_is.setAlpha(0.6f);
             //   holder.main_fr_is.setBackgroundResource(R.drawable.module_is);

            }

        }else {
            if(!courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1")){
                flag_locked = false;
                //holder.lock.setImageResource(R.drawable.unlock);
                holder.main_fr_is.setAlpha(1f);
                holder.lock_layout.setVisibility(View.GONE);
              //  holder.main_fr_is.setBackgroundResource(R.drawable.ass_ripple);
            }else {
                flag_locked = true;
              //  holder.lock.setImageResource(R.drawable.lock);
                holder.lock_layout.setVisibility(View.VISIBLE);
              //  holder.main_fr_is.setAlpha(0.6f);
               // holder.main_fr_is.setBackgroundResource(R.drawable.module_is);

            }

        }


        if(courseListUniversalmodel.getCourse_seen().equalsIgnoreCase("seen")) {

            holder.play.setBackgroundResource(R.drawable.course_continue);
            holder.seen_course.setVisibility(View.VISIBLE);
        }else {
            holder.play.setBackgroundResource(R.drawable.play_white_arrow_icon);
            holder.seen_course.setVisibility(View.GONE);

        }


        if(!courseListUniversalmodel.getCerti_id().equalsIgnoreCase("0")) {
            holder.certi_label_img.setVisibility(View.VISIBLE);
            holder.certificationheaing.setVisibility(View.VISIBLE);
            holder.certificationheaing.setText(courseListUniversalmodel.getCerti_name());
        }else {
            holder.certi_label_img.setVisibility(View.GONE);
            holder.certificationheaing.setVisibility(View.GONE);
            holder.certificationheaing.setText("");

        }


            if(courseListUniversalmodel.getAuthor().equals("null")){
            holder.author.setText("");

        }else {
            holder.author.setText("By"+courseListUniversalmodel.getAuthor());

        }
        Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "Gotham-Medium.otf");
        holder.course_names.setTypeface(tf2);
        holder.author.setTypeface(tf2);
        holder.certificationheaing.setTypeface(tf2);
        if(CourseLIstActivity.desc_statuslist.get(position).equalsIgnoreCase("close")){
            holder.description_relative.setVisibility(View.GONE);
            holder.desc_arrow.setBackgroundResource(R.drawable.ic_arrow_drop_down_white_24dp);
        }else {
            holder.description_relative.setVisibility(View.VISIBLE);
            holder.desc_arrow.setBackgroundResource(R.drawable.ic_arrow_drop_up_white_24dp);

        }

        holder.rate_us_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1")) {

                    CourseLIstActivity.rating_relative.setVisibility(View.VISIBLE);

                    if (!courseListUniversalmodel.getCerti_id().equalsIgnoreCase("0")) {
                        CourseLIstActivity.certi_course_name = courseListUniversalmodel.getCerti_name();
                        new PrefManager(mContext).savecertiname(courseListUniversalmodel.getCerti_name());
                    } else {
                        new PrefManager(mContext).savecertiname("");
                    }
                    new PrefManager(mContext).saveCourseid(String.valueOf(courseListUniversalmodel.getCourse_id()));
                    new PrefManager(mContext).saveCourseauthor(String.valueOf(courseListUniversalmodel.getAuthor()));
                    new PrefManager(mContext).savelgid(String.valueOf(courseListUniversalmodel.getLg_id()));
                    new PrefManager(mContext).savecertiid(String.valueOf(courseListUniversalmodel.getCerti_id()));

                }
                   }
        });

        holder.description_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN && ((courseListUniversalmodel.getType().equalsIgnoreCase("lesson") && !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("locked")) || !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1"))) {

                    holder.description_button.setBackgroundResource(R.drawable.play_purple_ripple);
                    holder.main_fr_is.setBackgroundColor(Color.parseColor("#ffffffff"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            holder.description_button.setBackgroundResource(R.drawable.play_purple_round);
                            holder.main_fr_is.setBackgroundResource(R.drawable.rounded_corner_courselist_items);

                        }
                    }.start();

                }


                return false;
            }
        });

        holder.main_fr_is.setOnTouchListener(new View.OnTouchListener(){
                                                 @Override
                                                 public boolean onTouch(View v, MotionEvent event) {

                                                     if(event.getAction() == MotionEvent.ACTION_DOWN && ((courseListUniversalmodel.getType().equalsIgnoreCase("lesson") && !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("locked")) || !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1"))) {

                                                         holder.main_fr_is.setBackgroundColor(Color.parseColor("#ffffffff"));

                                                         new CountDownTimer(150, 150) {
                                                             @Override
                                                             public void onTick(long millisUntilFinished) {

                                                             }

                                                             @Override
                                                             public void onFinish() {
                                                                 holder.main_fr_is.setBackgroundResource(R.drawable.rounded_corner_courselist_items);

                                                             }
                                                         }.start();

                                                     }

                                                     return false;

                                                 }



                                     });


        holder.rate_us_button.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN && ((courseListUniversalmodel.getType().equalsIgnoreCase("lesson") && !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("locked")) || !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1"))) {

                    holder.rate_us_button.setBackgroundResource(R.drawable.desc_rat_bg_ripple);

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            holder.rate_us_button.setBackgroundResource(R.drawable.desc_rat_bg);

                        }
                    }.start();

                }

                return false;

            }



        });

      /*  holder.description_linear.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN && ((courseListUniversalmodel.getType().equalsIgnoreCase("lesson") && !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("locked")) || !courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1"))) {

                    holder.description_linear.setBackgroundColor(Color.parseColor("#ce181135"));

                    new CountDownTimer(150, 150) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            holder.description_linear.setBackgroundResource(R.drawable.desc_rat_bg);

                        }
                    }.start();

                }

                return false;

            }



        });



        holder.description_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1")) {




                    if(!courseListUniversalmodel.getCerti_id().equalsIgnoreCase("0")) {
                        CourseLIstActivity.certi_course_name=courseListUniversalmodel.getCerti_name();
                        new PrefManager(mContext).savecertiname(courseListUniversalmodel.getCerti_name());
                    }else {
                        new PrefManager(mContext).savecertiname("");
                    }
                    CourseModel courseModel = CourseLIstActivity.arrayList.get(position);
                    new PrefManager(mContext).saveCourseid(String.valueOf(courseModel.getId()));
                    new PrefManager(mContext).saveCourseauthor(String.valueOf(courseModel.getAuthor()));
                    new PrefManager(mContext).savelgid(String.valueOf(courseModel.getLg_id()));
                    new PrefManager(mContext).savecertiid(String.valueOf(courseModel.getCerti_id()));

                    CourseLIstActivity.sendCourseStartData(courseModel.getId(), courseModel.getIsdependent());
                    CourseLIstActivity.clickCours(courseListUniversalmodel.getContent());

                }else {
                    CourseLIstActivity.nocourseavailableframe.setVisibility(View.VISIBLE);
                    CourseLIstActivity.noavailabletext.setText(courseListUniversalmodel.getWarn_msg());
                    // Toast.makeText(mContext, "Currently this course is unavailable", Toast.LENGTH_SHORT).show();
                }
                }
        });*/
        holder.main_fr_is.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - CourseLIstActivity.mLastClickTime < 1000) {
                    return;
                }
                CourseLIstActivity.mLastClickTime = SystemClock.elapsedRealtime();
                    if (connectionUtils.isConnectionAvailable()) {

                        if (courseListUniversalmodel.getType().equalsIgnoreCase("lesson")) {

                            int pos = position - CourseLIstActivity.arrayList.size();
                            LessonsModel lessonsModel = CourseLIstActivity.arrayList_lessons.get(pos);
                            if (lessonsModel.getIs_locked().equalsIgnoreCase("unlocked")) {

                                if (lessonsModel.getLms_url().length() > 5) {
                                    //lms
                                    CourseLIstActivity.sendLessonsStartData(lessonsModel.getId(), lessonsModel.getOrderid(), "lms");

                                    new PrefManager(mContext).saveRatingtype(lessonsModel.getType());
                                    new PrefManager(mContext).saveTargetid(String.valueOf(lessonsModel.getId()));
                                    Log.e("status", "last lesson");
                                    Intent intent = new Intent(mContext, WebviewVideoActivity.class);
                                    intent.putExtra("link", lessonsModel.getLms_url());
                                    mContext.startActivity(intent);
                                } else {
                                    //simulation
                                    new PrefManager(mContext).saveRatingtype("simulation");

                                    CourseLIstActivity.sendLessonsStartData(lessonsModel.getId(), lessonsModel.getOrderid(), "sm");

                                    new PrefManager(mContext).saveRatingtype(lessonsModel.getType());
                                    new PrefManager(mContext).saveTargetid(String.valueOf(lessonsModel.getId()));

                                    Log.e("status", "last lesson");
                                    Log.e("status", lessonsModel.getChildrens());
                                    Intent i = new Intent(mContext, SimulationStartPageActivity.class);
                                    try {
                                        JSONArray jsonObject = new JSONArray(lessonsModel.getChildrens());
                                        for (int in = 0; in < jsonObject.length(); in++) {
                                            JSONObject sim_model = jsonObject.getJSONObject(in);

                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            i.putExtra("sm_name", sim_model.getString("sm_name"));
                                            i.putExtra("sm_landing_page_image", sim_model.getString("sm_landing_page_image"));
                                            i.putExtra("sm_bg_color", sim_model.getString("sm_bg_color"));
                                            i.putExtra("sm_bg_image", sim_model.getString("sm_bg_image"));
                                            i.putExtra("sm_decision_bg_color", sim_model.getString("sm_decision_bg_color"));
                                            i.putExtra("sm_decision_bg_image", sim_model.getString("sm_decision_bg_image"));
                                            i.putExtra("sm_bg_sound", sim_model.getString("sm_bg_sound"));
                                            i.putExtra("sm_icon", sim_model.getString("sm_icon"));
                                            i.putExtra("sm_id", sim_model.getString("sm_id"));
                                            i.putExtra("sm_company", sim_model.getString("sm_company"));
                                            i.putExtra("sm_icon_image", sim_model.getString("sm_icon_image"));
                                            i.putExtra("gif_file", sim_model.getString("sm_bg_gif"));
                                            i.putExtra("sm_description", sim_model.getString("sm_description"));
                                            i.putExtra("sm_gif_loading_time", sim_model.getString("sm_gif_loading_time"));
                                            i.putExtra("sm_title_color", sim_model.getString("sm_title_color"));
                                            i.putExtra("sm_title_description_color", sim_model.getString("sm_title_description_color"));

                                        }
                                        mContext.startActivity(i);
                                        if (mContext instanceof Activity) {
                                            // ((Activity) mContext).finish();
                                            ((Activity) mContext).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            } else {
                                Toast.makeText(mContext, "Currently this module is unavailable", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (courseListUniversalmodel.getContent().length() < 6) {
                                Toast.makeText(mContext, "No data present in a course", Toast.LENGTH_SHORT).show();
                            } else {
                                if(!courseListUniversalmodel.getStatus_lock().equalsIgnoreCase("1")) {




                                    if(!courseListUniversalmodel.getCerti_id().equalsIgnoreCase("0")) {
                                        CourseLIstActivity.certi_course_name=courseListUniversalmodel.getCerti_name();
                                        new PrefManager(mContext).savecertiname(courseListUniversalmodel.getCerti_name());
                                    }else {
                                        new PrefManager(mContext).savecertiname("");
                                    }
                                   // CourseModel courseModel = CourseLIstActivity.arrayList.get(position);
                                    new PrefManager(mContext).saveCourseid(String.valueOf(courseListUniversalmodel.getCourse_id()));
                                    new PrefManager(mContext).saveCourseauthor(String.valueOf(courseListUniversalmodel.getAuthor()));
                                    new PrefManager(mContext).savelgid(String.valueOf(courseListUniversalmodel.getLg_id()));
                                    new PrefManager(mContext).savecertiid(String.valueOf(courseListUniversalmodel.getCerti_id()));

                                    CourseLIstActivity.sendCourseStartData(courseListUniversalmodel.getCourse_id(), courseListUniversalmodel.getIsdependent());
                                    CourseLIstActivity.clickCours(courseListUniversalmodel.getContent());

                                    }else {
                                    CourseLIstActivity.nocourseavailableframe.setVisibility(View.VISIBLE);
                                    CourseLIstActivity.noavailabletext.setText(courseListUniversalmodel.getWarn_msg());
                                   // Toast.makeText(mContext, "Currently this course is unavailable", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                    } else {
                        Toast.makeText(mContext, "Please check your internet connection", Toast.LENGTH_SHORT).show();

                    }


            }
        });

    }

    //                    Toast.makeText(mContext, "Currently this course is unavailable", Toast.LENGTH_SHORT).show();

    @Override
    public int getItemCount() {
        return fungame.size();

    }
}

