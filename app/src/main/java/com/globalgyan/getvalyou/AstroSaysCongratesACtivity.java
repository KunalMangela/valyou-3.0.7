package com.globalgyan.getvalyou;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

public class AstroSaysCongratesACtivity extends AppCompatActivity {

    RelativeLayout banner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.congo_maq);

         banner=(RelativeLayout)findViewById(R.id.banner_level_page);

        banner.setVisibility(View.GONE);
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Animation scaleAnimation = new ScaleAnimation(
                        0f, 1f, // Start and end values for the X axis scaling
                        0f, 1f, // Start and end values for the Y axis scaling
                        Animation.REVERSE, 0.5f, // Pivot point of X scaling
                        Animation.REVERSE, 0.5f);
                scaleAnimation.setDuration(1000);


                banner.startAnimation(scaleAnimation);
                new CountDownTimer(1000, 100) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        banner.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFinish() {

                    }
                }.start();

            }
        }.start();
    }
}
