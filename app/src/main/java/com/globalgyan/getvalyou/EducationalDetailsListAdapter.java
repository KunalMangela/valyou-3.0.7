package com.globalgyan.getvalyou;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.EducationalDetails;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

/**
 * Created by sagar on 27/2/17.
 */

public class EducationalDetailsListAdapter extends RecyclerView.Adapter<EducationalDetailsListAdapter.ViewHolder> {
    Context context = null;
    private Dialog dialog = null;
    private ArrayList<EducationalDetails> educationalDetailses = null;



    public EducationalDetailsListAdapter(Context context, ArrayList<EducationalDetails> educationalDetailses) {
        this.context = context;
        this.educationalDetailses = educationalDetailses;

    }

    @Override
    public EducationalDetailsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.educational_details_list_item, parent, false);
        EducationalDetailsListAdapter.ViewHolder viewHolder = new EducationalDetailsListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EducationalDetailsListAdapter.ViewHolder holder, int position) {
        final EducationalDetails educationalDetails = educationalDetailses.get(position);

        holder.txtDegreeName.setText(educationalDetails.getDegreeName());
        holder.txtCollegeName.setText(educationalDetails.getCollegeName());
        //holder.txtDate.setText(educationalDetails.getFrom()+" to "+educationalDetails.getTo());
        if(!TextUtils.isEmpty(educationalDetails.getTo()) && educationalDetails.getTo().length()>2) {
            holder.txtDate.setText(educationalDetails.getFrom() +" to "+educationalDetails.getTo());
        }else{
            holder.txtDate.setText(educationalDetails.getFrom() +" to current");
        }
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
                long selectedid = dataBaseHelper.getMaxId(DataBaseHelper.TABLE_STRING_CONTANTS);
                if (selectedid > 0) {
                    dataBaseHelper.updateTableDetails(String.valueOf(selectedid), DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON, educationalDetails.getId());
                }else{
                    dataBaseHelper.createTable(DataBaseHelper.TABLE_STRING_CONTANTS, DataBaseHelper.KEY_STRING_SELCTED_EDUCATON, educationalDetails.getId());
                }
                Activity activity = (Activity) context;
                Intent it= new Intent(context, EducationalDetailsActivity.class);
                it.putExtra("kya","stud");
               context.startActivity(it);



            }
        });
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(educationalDetails.getId(),educationalDetails);

            }
        });

    }

    @Override
    public int getItemCount() {
        return educationalDetailses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtDegreeName;
        TextView txtCollegeName;
        TextView txtDate;
        LinearLayout editButton;
        LinearLayout deleteButton;

        public ViewHolder(View itemView) {
            super(itemView);

            txtDegreeName = (TextView) itemView.findViewById(R.id.degreeName);
            txtCollegeName = (TextView) itemView.findViewById(R.id.collegeName);
            txtDate = (TextView) itemView.findViewById(R.id.dates);
            editButton  = (LinearLayout) itemView.findViewById(R.id.editButton);
            deleteButton  = (LinearLayout) itemView.findViewById(R.id.deleteButton);
        }
    }

    public void openDialog(final String id, final EducationalDetails professional) {

        dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_video_view);
        dialog.setCancelable(false);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        FontTextView infoText = (FontTextView) dialog.findViewById(R.id.dialog_info);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        Button btnyes = (Button) dialog.findViewById(R.id.btnYes);
        infoText.setText("This will delete your Educational details, Are you sure?");
        btnNo.setText("No");
        btnyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
                        dataBaseHelper.deleteEducation(id);
                        educationalDetailses.remove(professional);
                        notifyDataSetChanged();
                    }
                }, 10);
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 10);

            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

}
