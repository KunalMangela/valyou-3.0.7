package com.globalgyan.getvalyou;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.Course;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.pluscubed.recyclerfastscroll.RecyclerFastScroller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import java.util.Collections;
import java.util.Comparator;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

import static com.globalgyan.getvalyou.HomeActivity.token;
import static com.globalgyan.getvalyou.apphelper.BatteryReceiver.flag_play_anim;

public class CourseLIstActivity extends AppCompatActivity {
    RecyclerView rc_course;
    public static String lg_id_from_intent_is;
    public static ArrayList<CourseModel> arrayList=new ArrayList<>();
    public static ArrayList<CourseListUniversalmodel> arrayList_main_list=new ArrayList<>();
    public static ArrayList<LessonsModel> arrayList_lessons=new ArrayList<>();
    LessonsModel lessonsModel;
    public static String certi_course_name="";
    CourseListUniversalmodel courseListUniversalmodel;
    CourseListAdapter courseAdapter;
    static boolean flag_update = false;
    public static int code2;
    KProgressHUD assess_progressis;
    static InputStream is = null;
    String result1 ="";
    public String frompush="no";
    TextView textcourse;
    Dialog verification_progress;
    static ArrayList<String> course_ids = new ArrayList<>();
    static JSONObject jObj = null,jObjeya=null;
    HttpURLConnection urlConnection;
    public static String token="";
    static String json = "",jsoneya="",jso="",Qus="",token_type="", dn="",emaileya="";
    int responseCode,responseCod,responseCo;
    public static Context mc;
    MaterialRefreshLayout swipecourse;
    public static HttpURLConnection urlConnection2;
    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2;
    static long mLastClickTime = 0;
    FrameLayout course_frame;
    public static boolean backclick=false;
    TextView nocourse;
    public static ArrayList<String> arrayList_cerificate_ids=new ArrayList<>();
    public static ArrayList<String> arrayList_lg_ids=new ArrayList<>();
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    public static Context context;
    public static String type_ass="";
    BatteryReceiver mBatInfoReceiver;
    public static Context mcontext;
    BatteryManager bm;


    public static TextView submit_btn_text,rateustext,rate_desc_text;
    public static RatingBar ratingBar;
    public static RelativeLayout rating_relative;
    public static ImageView red_planetastro,close_rate;
    public static Float rating_is;
    public static FrameLayout nocourseavailableframe;
    public static TextView noavailabletext;
    public static ImageView okbtn;
    int batLevel;
    public static ArrayList<String> desc_statuslist=new ArrayList<>();
    AppConstant appConstant;
    TextView textcourseheading;
    boolean isPowerSaveMode;
    PowerManager pm;
    String get_learn_nav = " ";
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        if(new PrefManager(CourseLIstActivity.this).getLearnNav().equalsIgnoreCase("course_progress")) {

        }else {
            new PrefManager(CourseLIstActivity.this).saveLearnNav("main");
        }
        new PrefManager(CourseLIstActivity.this).saveNav("learn");

        appConstant = new AppConstant(this);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        mcontext=CourseLIstActivity.this;
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        textcourseheading=(TextView)findViewById(R.id.textcourseheading);
        textcourse=(TextView)findViewById(R.id.textcourse);
        Log.e("battery", ""+batLevel);
        rc_course=(RecyclerView)findViewById(R.id.course_list_is);
        rc_course.setHasFixedSize(true);
        nocourseavailableframe=(FrameLayout)findViewById(R.id.nocourseavailableframe);
        noavailabletext=(TextView)findViewById(R.id.noavailabletext);
        okbtn=(ImageView)findViewById(R.id.okbtn);
        //
        frompush=getIntent().getStringExtra("pushis");
        try{
           Log.e("frompushvalue",frompush);
        }catch (Exception e){
            frompush="no";
            e.printStackTrace();
        }

        try {
            if (frompush.equalsIgnoreCase("yes")) {
                new PrefManager(CourseLIstActivity.this).saveLearningGroups(getIntent().getStringExtra("course_id_push"));

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        lg_id_from_intent_is=new PrefManager(CourseLIstActivity.this).getLearningGroups();

        try{
           Log.e("frompush",frompush);
        }catch (Exception e){
            e.printStackTrace();
        }

     /*   if(frompush.equalsIgnoreCase("yes")){
            new PrefManager(CourseLIstActivity.this).saveLearningGroups(getIntent().getStringExtra("course_id_push"));

        }*/
        Typeface typeface2 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        rating_relative=(RelativeLayout)findViewById(R.id.rating_relative);
        ratingBar=(RatingBar)findViewById(R.id.ratingBar);
        submit_btn_text=(TextView)findViewById(R.id.submit_btn);
        rateustext=(TextView)findViewById(R.id.rate_text);
        rate_desc_text=(TextView)findViewById(R.id.simpel_texts);
        red_planetastro=(ImageView)findViewById(R.id.red_planetastro);
        submit_btn_text.setTypeface(typeface2,Typeface.BOLD);
        rateustext.setTypeface(typeface2);
        rate_desc_text.setTypeface(typeface2);
        close_rate=(ImageView)findViewById(R.id.close_rate);
        final Animation animation_pend = AnimationUtils.loadAnimation(CourseLIstActivity.this, R.anim.pendulam_type_animation);
        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            red_planetastro.startAnimation(animation_pend);

        }


        verification_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        verification_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verification_progress.setContentView(R.layout.planet_loader);
        Window window1 = verification_progress.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        verification_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        verification_progress.setCancelable(false);
        GifView gifView2 = (GifView) verification_progress.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating_is= ratingBar.getRating();
            }
        });


        close_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rating_relative.setVisibility(View.GONE);
            }
        });
        submit_btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_btn_text.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        submit_btn_text.setEnabled(true);

                    }
                }.start();
                ConnectionUtils connectionUtils=new ConnectionUtils(CourseLIstActivity.this);
                if(connectionUtils.isConnectionAvailable()){
                    rating_relative.setVisibility(View.GONE);
                    new SendRating().execute();
                }else {
                    Toast.makeText(CourseLIstActivity.this,"Please check your Internet connection!",Toast.LENGTH_SHORT).show();
                }

            }
        });


        context=CourseLIstActivity.this;
        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);
        nocourse=(TextView)findViewById(R.id.no_cours_text);
        course_frame=(FrameLayout)findViewById(R.id.course_frame);


        swipecourse=(MaterialRefreshLayout)findViewById(R.id.swipecourse);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_course.setLayoutManager(linearLayoutManager);
        Typeface typeface1 = Typeface.createFromAsset(this.getAssets(), "Gotham-Medium.otf");
        nocourse.setTypeface(typeface1);
          courseAdapter=new CourseListAdapter(this,arrayList_main_list);
        textcourseheading.setTypeface(typeface1);
        rc_course.setAdapter(courseAdapter);


        textcourse.setTypeface(typeface1);


        noavailabletext.setTypeface(typeface1,Typeface.BOLD);
        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nocourseavailableframe.setVisibility(View.GONE);
            }
        });

        nocourseavailableframe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nocourseavailableframe.setVisibility(View.GONE);
            }
        });
        mc=CourseLIstActivity.this;
        assess_progressis = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setDimAmount(0.7f)
                .setCancellable(false);
        ImageView back=(ImageView)findViewById(R.id.backbtnis_courseis);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() -mLastClickTime < 1000) {
                    return;
                }
              mLastClickTime = SystemClock.elapsedRealtime();
                if(backclick){
                    try{
                        new GetContacts().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new GetCourses().cancel(true);
                        new GetCourses().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new SendCourseStartProgress().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new SendLessonStartProgress().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        get_learn_nav = new PrefManager(CourseLIstActivity.this).getLearnNav();
                    }catch (Exception e){
                        get_learn_nav = " ";
                        e.printStackTrace();
                    }

                    if(get_learn_nav.equalsIgnoreCase("course_progress")){
                        Intent intent = new Intent(CourseLIstActivity.this, ActiveCoursesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        finish();

                    }else {
                        Intent intent = new Intent(CourseLIstActivity.this, GetLearningGroupsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }

                }else {
                    try{
                        get_learn_nav = new PrefManager(CourseLIstActivity.this).getLearnNav();
                    }catch (Exception e){
                        get_learn_nav = " ";
                        e.printStackTrace();
                    }
                    if(get_learn_nav.equalsIgnoreCase("course_progress")){
                        Intent intent = new Intent(CourseLIstActivity.this, ActiveCoursesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        finish();

                    }else {
                        Intent intent = new Intent(CourseLIstActivity.this, GetLearningGroupsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        finish();
                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

                    }

                }
            }
        });


        textcourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() -mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if(backclick){
                    try{
                        new GetContacts().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new GetCourses().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new SendCourseStartProgress().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        new SendLessonStartProgress().cancel(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try{
                        get_learn_nav = new PrefManager(CourseLIstActivity.this).getLearnNav();
                    }catch (Exception e){
                        get_learn_nav = " ";
                        e.printStackTrace();
                    }

                    if(get_learn_nav.equalsIgnoreCase("course_progress")){
                        Intent intent = new Intent(CourseLIstActivity.this, MainPlanetActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        finish();

                    }else {
                        Intent intent = new Intent(CourseLIstActivity.this, MainPlanetActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        finish();
                    }

                }else {
                    try{
                        get_learn_nav = new PrefManager(CourseLIstActivity.this).getLearnNav();
                    }catch (Exception e){
                        get_learn_nav = " ";
                        e.printStackTrace();
                    }
                    if(get_learn_nav.equalsIgnoreCase("course_progress")){
                        Intent intent = new Intent(CourseLIstActivity.this, MainPlanetActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        finish();

                    }else {
                        Intent intent = new Intent(CourseLIstActivity.this, MainPlanetActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                        finish();
                    }

                }
            }
        });
        Animation animation_fade1 =
                AnimationUtils.loadAnimation(CourseLIstActivity.this,
                        R.anim.fadeing_out_animation_planet);
      //  course_frame.startAnimation(animation_fade1);
        new CountDownTimer(100, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
                course_frame.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFinish() {
                swipecourse.autoRefresh();

            }
        }.start();


        swipecourse.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {

                ConnectionUtils connectionUtils=new ConnectionUtils(CourseLIstActivity.this);
                if(connectionUtils.isConnectionAvailable()){

                    String retry_str = " ";

                    try{
                        retry_str = new PrefManager(CourseLIstActivity.this).getRetryresponseString();
                    }
                    catch (Exception e){
                        retry_str = " ";
                        e.printStackTrace();
                    }

                    if(retry_str.equalsIgnoreCase("notdone")){
                        Toast.makeText(CourseLIstActivity.this,"Video uploading is in progrss,Can't access latest notifications now",Toast.LENGTH_LONG).show();
                        swipecourse.finishRefresh();
                    }else {
                        arrayList.clear();
                        arrayList_main_list.clear();
                        arrayList_lessons.clear();
                        course_ids.clear();
                        flag_update = true;
                        courseAdapter.notifyDataSetChanged();
                        getCourses();
                       /* int positionView = ((LinearLayoutManager)recyclerGen.getLayoutManager()).findFirstVisibleItemPosition();
                        Log.e("pos",String.valueOf(positionView));
                        if(positionView==0) {
                            swipeRefreshLayout.setEnabled(true);


                        }else {

                        }
*/
                    }
                }else {
                    swipecourse.finishRefresh();
                    Toast.makeText(CourseLIstActivity.this,"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
       // getCourses();

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
            //  red planet animation
            red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f, 700f);
            red_planet_anim_1.setDuration(130000);
            red_planet_anim_1.setInterpolator(new LinearInterpolator());
            red_planet_anim_1.setStartDelay(0);
            red_planet_anim_1.start();

            red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700, 0f);
            red_planet_anim_2.setDuration(130000);
            red_planet_anim_2.setInterpolator(new LinearInterpolator());
            red_planet_anim_2.setStartDelay(0);

            red_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            red_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    red_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  yellow planet animation
            yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f, 1100f);
            yellow_planet_anim_1.setDuration(220000);
            yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_1.setStartDelay(0);
            yellow_planet_anim_1.start();

            yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f, 0f);
            yellow_planet_anim_2.setDuration(22000);
            yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
            yellow_planet_anim_2.setStartDelay(0);

            yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    yellow_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  purple planet animation
            purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f, 100f);
            purple_planet_anim_1.setDuration(9500);
            purple_planet_anim_1.setInterpolator(new LinearInterpolator());
            purple_planet_anim_1.setStartDelay(0);
            purple_planet_anim_1.start();

            purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f, 0f);
            purple_planet_anim_2.setDuration(100000);
            purple_planet_anim_2.setInterpolator(new LinearInterpolator());
            purple_planet_anim_2.setStartDelay(0);


            purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    purple_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            //  earth animation
            earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f, 1150f);
            earth_anim_1.setDuration(90000);
            earth_anim_1.setInterpolator(new LinearInterpolator());
            earth_anim_1.setStartDelay(0);
            earth_anim_1.start();

            earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f, 0f);
            earth_anim_2.setDuration(55000);
            earth_anim_2.setInterpolator(new LinearInterpolator());
            earth_anim_2.setStartDelay(0);

            earth_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            earth_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    earth_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


            //  orange planet animation
            orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f, 300f);
            orange_planet_anim_1.setDuration(56000);
            orange_planet_anim_1.setInterpolator(new LinearInterpolator());
            orange_planet_anim_1.setStartDelay(0);
            orange_planet_anim_1.start();

            orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f, 0f);
            orange_planet_anim_2.setDuration(75000);
            orange_planet_anim_2.setInterpolator(new LinearInterpolator());
            orange_planet_anim_2.setStartDelay(0);

            orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_2.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    orange_planet_anim_1.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });


        }
    }

    private void getCourses() {
        new GetContacts().execute();
    }




    private static String readStream(InputStream in) {

        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            is.close();

            json = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }

    private class GetContacts extends AsyncTask<String, String, String> {
        String result1 ="";
        @Override
        protected void onPreExecute() {


            super.onPreExecute();
            backclick=true;

        }

        @Override
        protected String doInBackground(String... urlkk) {
            try{

                // httpClient = new DefaultHttpClient();
                // httpPost = new HttpPost("http://35.154.93.176/oauth/token");

                params.add(new BasicNameValuePair("scope", ""));
                params.add(new BasicNameValuePair("client_id", "5"));
                params.add(new BasicNameValuePair("client_secret", "n2o2BMpoQOrc5sGRrOcRc1t8qdd7bsE7sEkvztp3"));
                params.add(new BasicNameValuePair("grant_type", "password"));
                params.add(new BasicNameValuePair("username","admin@admin.com"));
                params.add(new BasicNameValuePair("password","password"));

                URL urlToRequest = new URL(AppConstant.Ip_url+"oauth/token");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                //urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setFixedLengthStreamingMode(getQuery(params).getBytes().length);

                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(getQuery(params));
                wr.flush();

                responseCod = urlConnection.getResponseCode();

                is = urlConnection.getInputStream();
                //if(responseCode == HttpURLConnection.HTTP_OK){
                String responseString = readStream(is);
                Log.v("Response", responseString);
                result1=responseString;
                // httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                //  httpPost.setEntity(new UrlEncodedFormEntity(params));
                // HttpResponse httpResponse = httpClient.execute(httpPost);
                // responseCod = httpResponse.getStatusLine().getStatusCode();
                // HttpEntity httpEntity = httpResponse.getEntity();
                // is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                backclick=false;

                try{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            swipecourse.finishRefresh();
                        }catch (Exception e){

                        }
                    }
                });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                backclick = false;
                try{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            swipecourse.finishRefresh();
                        } catch (Exception e) {

                        }
                    }
                });
            }catch (Exception ex){
                ex.printStackTrace();
            }
            } catch (IOException e) {
                e.printStackTrace();
                backclick=false;
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                swipecourse.finishRefresh();
                            } catch (Exception e) {

                            }
                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
            try {
                jObj = new JSONObject(result1);
                token=jObj.getString("access_token");


                try{
                   new PrefManager(CourseLIstActivity.this).savetoken(token);
                }
               catch (Exception e){
                    e.printStackTrace();
               }

                token_type=jObj.getString("token_type");
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(responseCod != urlConnection.HTTP_OK){
                backclick=false;
                try{
                    swipecourse.finishRefresh();
                }catch (Exception e){

                }
                Toast.makeText(CourseLIstActivity.this,"Error while loading user data",Toast.LENGTH_SHORT).show();
            }else {


                new GetCourses().execute();

            }
        }
    }
    private class GetCourses extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            arrayList_cerificate_ids.clear();

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            HttpURLConnection urlConnection1 = null;
            try {

                DataBaseHelper dataBaseHelper=new DataBaseHelper(CourseLIstActivity.this);
                String email = dataBaseHelper.getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_EMAIL);

                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(CourseLIstActivity.this))
                        .object("email", email)
                        .object("lg_id",lg_id_from_intent_is)
                        .build().toString();

                try {

                    String auth_token = " ";

                    try{
                        auth_token =new PrefManager(CourseLIstActivity.this).getAuthToken();
                    }
                    catch (Exception e){
                        auth_token = " ";
                        e.printStackTrace();
                    }

                    // httpClient = new DefaultHttpClient();
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_courses");
                    urlConnection1 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection1.setDoOutput(true);
                    urlConnection1.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection1.setRequestProperty("Content-Type", "application/json");
                    urlConnection1.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection1.setRequestProperty("auth_token",auth_token);
                    urlConnection1.setRequestMethod("POST");

                    // Log.e("AuthT",new PrefManager(getActivity()).getAuthToken());
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection1.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    int responseCode = urlConnection1.getResponseCode();


                    is= urlConnection1.getInputStream();

                    String responseString = readStream(is);
                    Log.e("Course", responseString);
                    result1 = responseString;



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    backclick=false;

                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    swipecourse.finishRefresh();
                                } catch (Exception e) {

                                }
                            }
                        });
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    backclick=false;
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    swipecourse.finishRefresh();

                                } catch (Exception e) {

                                }
                            }
                        });
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                } catch (IOException e) {
                    backclick=false;
                    e.printStackTrace();
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    swipecourse.finishRefresh();

                                } catch (Exception e) {

                                }
                            }
                        });
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            finally {
                if (urlConnection1 != null)
                    urlConnection1.disconnect();
            }

            return result1;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            backclick=false;
            arrayList_main_list.clear();

            try{
                swipecourse.finishRefresh();
            }catch (Exception e){

            }
            if(s.equalsIgnoreCase("")){
              //  rc_course.setVisibility(View.GONE);
               nocourse.setVisibility(View.VISIBLE);
            }else {
              //  rc_course.setVisibility(View.VISIBLE);
                nocourse.setVisibility(View.GONE);
            }

            String ss=" ";

            try {
                ss=new PrefManager(CourseLIstActivity.this).getAuthToken().toString();
            }catch (Exception e){
                ss = " ";
                e.printStackTrace();
            }
            if(s.contains("error")){
                Log.e("respmsg","error");
                // swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONObject js=new JSONObject(s);
                    String errormsg=js.getString("error");
                    AppConstant appConstant=new AppConstant(CourseLIstActivity.this);
                    appConstant.showLogoutDialogue(errormsg);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("respmsg","errorexc");
                }

            }else {
                desc_statuslist.clear();
                String resp=s;
                JSONArray course_array=null;
                JSONArray lesson_array=null;
                try {
                    JSONObject jsonObject_main=new JSONObject(s);
                    course_array=jsonObject_main.optJSONArray("course_array");
                    lesson_array=jsonObject_main.optJSONArray("lesson_array");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("respmsg","noerror");
                try {
                    //JSONArray jsonArray=new JSONArray(s);
                    for(int i=0;i<course_array.length();i++){
                        JSONObject jsonObject= (JSONObject) course_array.get(i);
                        Log.e("respmsgis",String.valueOf(jsonObject));
                        CourseModel courseModel=new CourseModel();
                        courseModel.setId(jsonObject.getInt("course_id"));
                        courseModel.setName(jsonObject.getString("course_name"));
                        courseModel.setDesc(jsonObject.getString("course_description"));
                        courseModel.setCost(jsonObject.getString("course_cost"));
                        courseModel.setRating(jsonObject.getString("course_rating"));
                        courseModel.setUrl_ic(jsonObject.getString("course_icon"));
                        courseModel.setHashtags(jsonObject.getString("hashtags"));
                        courseModel.setWarn_msg(jsonObject.getString("lock_message"));
                        courseModel.setCourse_seen(jsonObject.getString("course_seen"));
                        try {
                            courseModel.setCerti_name(jsonObject.getString("certification_name"));
                        }catch (Exception e){
                            e.printStackTrace();
                            courseModel.setCerti_name(" ");

                        }
                        try {
                            courseModel.setStatus_lock(String.valueOf(jsonObject.getInt("is_locked")));
                        }catch (Exception e){
                            e.printStackTrace();
                            courseModel.setStatus_lock("0");

                        }
                        //jsonObject.getString("is_locked")
                        courseModel.setAuthor(jsonObject.getString("author_name"));
                        courseModel.setLg_id(jsonObject.getString("lg_id"));
                        courseModel.setCerti_id(jsonObject.getString("certification_id"));
                        courseModel.setRating(jsonObject.getString("course_rating"));
                        arrayList_cerificate_ids.add(jsonObject.getString("certification_id"));
                        arrayList_lg_ids.add(jsonObject.getString("lg_id"));
                        courseModel.setContent(String.valueOf(jsonObject.getJSONArray("course_content")));
                        courseModel.setIsdependent(jsonObject.getInt("is_dependent"));
                        Log.e("content",String.valueOf(jsonObject.getJSONArray("course_content")));
                        arrayList.add(courseModel);
                        Log.e("respmsg","success");

                        CourseListUniversalmodel courseListUniversalmodel=new CourseListUniversalmodel();
                        courseListUniversalmodel.setType("Course");
                        courseListUniversalmodel.setRating(jsonObject.getString("course_rating"));
                        courseListUniversalmodel.setCourse_id(jsonObject.getInt("course_id"));
                        courseListUniversalmodel.setCourse_seen(jsonObject.getString("course_seen"));
                        courseListUniversalmodel.setWarn_msg(jsonObject.getString("lock_message"));
                        courseListUniversalmodel.setName(jsonObject.getString("course_name"));
                        courseListUniversalmodel.setLg_id(jsonObject.getString("lg_id"));
                        courseListUniversalmodel.setCost(jsonObject.getString("course_cost"));
                        courseListUniversalmodel.setUrl_ic(jsonObject.getString("course_icon"));
                        courseListUniversalmodel.setDesc_course(jsonObject.getString("course_description"));
                        courseListUniversalmodel.setRating(jsonObject.getString("course_rating"));
                        courseListUniversalmodel.setIsdependent(jsonObject.getInt("is_dependent"));
                        courseListUniversalmodel.setCerti_id(jsonObject.getString("certification_id"));
                        courseListUniversalmodel.setAuthor(jsonObject.getString("author_name"));
                        try {
                            courseListUniversalmodel.setCerti_name(jsonObject.getString("certification_name"));
                        }catch (Exception e){
                            e.printStackTrace();
                            courseListUniversalmodel.setCerti_name(" ");

                        }

                        try {
                            courseListUniversalmodel.setStatus_lock(String.valueOf(jsonObject.getInt("is_locked")));
                        }catch (Exception e){
                            e.printStackTrace();
                            courseListUniversalmodel.setStatus_lock("0");

                        }

                        courseListUniversalmodel.setContent(String.valueOf(jsonObject.getJSONArray("course_content")));

                        if(lg_id_from_intent_is.equals(jsonObject.getString("lg_id"))){
                            arrayList_main_list.add(courseListUniversalmodel);
                        }
                        courseAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*try {
                    //JSONArray jsonArray=new JSONArray(s);
                    for(int i=0;i<lesson_array.length();i++){
                        JSONObject jsonObject= (JSONObject) lesson_array.get(i);
                        Log.e("respmsgis",String.valueOf(jsonObject));
                        LessonsModel lessonsModel=new LessonsModel();
                        lessonsModel.setId(jsonObject.getInt("id"));
                        lessonsModel.setObj_name(jsonObject.getString("object_name"));
                        lessonsModel.setType(jsonObject.getString("type"));
                        lessonsModel.setLms_url(jsonObject.getString("lms_url"));
                        lessonsModel.setSm_id(jsonObject.getInt("sm_id"));
                        try {
                            lessonsModel.setAuthor(jsonObject.getString("author_name"));
                        }catch (Exception e){
                            e.printStackTrace();
                            lessonsModel.setAuthor("");

                        }
                        lessonsModel.setOrderid(jsonObject.getInt("order_id"));
                        lessonsModel.setIs_locked(jsonObject.getString("is_locked"));
                        arrayList_lessons.add(lessonsModel);
                        Log.e("respmsg","success");
                        courseAdapter.notifyDataSetChanged();
                        CourseListUniversalmodel courseListUniversalmodel=new CourseListUniversalmodel();
                        courseListUniversalmodel.setType("lesson");
                        courseListUniversalmodel.setName(jsonObject.getString("object_name"));
                        courseListUniversalmodel.setAuthor(jsonObject.getString("author_name"));
                        courseListUniversalmodel.setWarn_msg(jsonObject.getString(""));
                        courseListUniversalmodel.setStatus_lock(jsonObject.getString("is_locked"));
                        courseListUniversalmodel.setCerti_id(jsonObject.getString("0"));

                        courseListUniversalmodel.setCerti_name(" ");


                        courseListUniversalmodel.setContent("");

                        arrayList_main_list.add(courseListUniversalmodel);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                if(arrayList_main_list.size()==0){

                    //  rc_course.setVisibility(View.GONE);
                    nocourse.setVisibility(View.VISIBLE);

                }else {

                    for(int i=0;i<arrayList_main_list.size();i++){
                        desc_statuslist.add(i,"close");
                    }


                  //  rc_course.setVisibility(View.VISIBLE);
                    nocourse.setVisibility(View.GONE);

                }





              /*  Collections.sort(arrayList_main_list, new Comparator<CourseListUniversalmodel>() {
                    @Override
                    public int compare(CourseListUniversalmodel lhs, CourseListUniversalmodel rhs) {
                        String r1=rhs.getCerti_id();
                        String r2=lhs.getCerti_id();
                        return r2.compareTo(r1);
                    }
                });*/

                courseAdapter.notifyDataSetChanged();
            }

        }
    }

        private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    public void backJourney(View view){
        finish();
    }

    public static void clickCours(String content){
        Intent intent = new Intent(mc, ModulesActivity.class);
        intent.putExtra("content",content );
        intent.putExtra("token",token );
        intent.putExtra("c_name",certi_course_name );
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        mc.startActivity(intent);
        if (mc instanceof Activity) {
            ((Activity) mc).overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
           // ((Activity)mc).finish();
        }
      //  mc.overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

    }

    @Override
    public void onBackPressed() {

        if(backclick){
            try{
                new GetContacts().cancel(true);
            }catch (Exception e){
                e.printStackTrace();
            }

            try{
                new GetCourses().cancel(true);
            }catch (Exception e){
                e.printStackTrace();
            }

            try{
                new SendCourseStartProgress().cancel(true);
            }catch (Exception e){
                e.printStackTrace();
            }

            try{
                new SendLessonStartProgress().cancel(true);
            }catch (Exception e){
                e.printStackTrace();
            }

            try {
                get_learn_nav = new PrefManager(CourseLIstActivity.this).getLearnNav();
            }
            catch (Exception e){
                get_learn_nav=" ";
                e.printStackTrace();
            }
            if(get_learn_nav.equalsIgnoreCase("course_progress")){
                Intent intent = new Intent(CourseLIstActivity.this, ActiveCoursesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();

            }else {
                Intent intent = new Intent(CourseLIstActivity.this, GetLearningGroupsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }
        }else {
            try {
                get_learn_nav = new PrefManager(CourseLIstActivity.this).getLearnNav();
            }
            catch (Exception e){
                get_learn_nav=" ";
                e.printStackTrace();
            }
            if(get_learn_nav.equalsIgnoreCase("course_progress")){
                    Intent intent = new Intent(CourseLIstActivity.this, ActiveCoursesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                    finish();

            }else {
                Intent intent = new Intent(CourseLIstActivity.this, GetLearningGroupsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);

            }

        }
    }


    public static void sendCourseStartData(int id, int isdependent){
        new SendCourseStartProgress().execute(id,isdependent);
    }
    public static void sendLessonsStartData(int id, int order,String ty){
        type_ass=ty;
        new SendLessonStartProgress().execute(id,order);
    }
    private static class SendCourseStartProgress extends AsyncTask<Integer, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        ///Authorization
        @Override
        protected String doInBackground(Integer... urlkk) {
            String result1 = "",lg_id =" ",cert_id=" ";
            try {

             try {
                 lg_id = new PrefManager(context).getlgid();
                 cert_id = new PrefManager(context).getcertiid();
             }
             catch (Exception e){
                 lg_id = " ";
                 cert_id = " ";
                 e.printStackTrace();
             }

                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(context))
                        .object("course_id", urlkk[0])
                        .object("is_dependent", urlkk[1])
                        .object("lg_id",lg_id)
                        .object("certification_id",cert_id)


                        .build().toString();

                try {

                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_add_user_progress");
                    urlConnection2 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection2.setDoOutput(true);
                    urlConnection2.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection2.setRequestProperty("Content-Type", "application/json");
                    urlConnection2.setRequestProperty("Authorization", "Bearer " + token);

                    urlConnection2.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection2.getOutputStream());
                    wr.write(jn);
                    Log.e("resp_data",jn);
                    wr.flush();
                    is = urlConnection2.getInputStream();
                    code2 = urlConnection2.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                } catch (ClientProtocolException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection2 != null)
                    urlConnection2.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {
                Log.e("myvalue","postans 200");
                }
            else{
                Log.e("myvalue","postans not 200");

            }
        }
    }

    private static class SendLessonStartProgress extends AsyncTask<Integer, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        ///Authorization
        @Override
        protected String doInBackground(Integer... urlkk) {
            String result1 = "", lg_id=" ", cert_id = " ";
            try {

                try {
                    lg_id = new PrefManager(context).getlgid();
                    cert_id = new PrefManager(context).getcertiid();
                }
                catch (Exception e){
                    lg_id = " ";
                    cert_id = " ";
                    e.printStackTrace();
                }

                String pr="";
                if(type_ass.equals("lms")){
                    pr="completed";

                }else {
                    pr="inprogress";

                }
                String jn = new JsonBuilder(new GsonAdapter())
                        .object("data")
                        .object("U_Id", PreferenceUtils.getCandidateId(context))
                        .object("course_id", 0)
                        .object("lesson_id", urlkk[0])
                        .object("assessment_id", 0)
                        .object("order_id", urlkk[1])
                        .object("status", pr)
                        .object("lg_id",lg_id)
                        .object("certification_id",cert_id)


                        .build().toString();

                    try {
                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_update_user_progress");
                    urlConnection2 = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection2.setDoOutput(true);
                    urlConnection2.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection2.setRequestProperty("Content-Type", "application/json");
                    urlConnection2.setRequestProperty("Authorization", "Bearer " + token);

                    urlConnection2.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection2.getOutputStream());
                    wr.write(jn);
                    Log.e("resp_data",jn);
                    wr.flush();
                    is = urlConnection2.getInputStream();
                    code2 = urlConnection2.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                } catch (ClientProtocolException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection2 != null)
                    urlConnection2.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {
                Log.e("myvalue","postans 200");
            }
            else{
                Log.e("myvalue","postans not 200");

            }
        }
    }

    private class SendRating extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            verification_progress.show();
            super.onPreExecute();

        }


        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {


                try {


                    String jon = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("feedback", " ")
                            .object("u_id", PreferenceUtils.getCandidateId(CourseLIstActivity.this))
                            .object("rating_for","course")
                            .object("target_id", new PrefManager(CourseLIstActivity.this).getCourseid())
                            .object("star_rating",  String.valueOf(rating_is))
                            .build().toString();



                    URL urlToRequest = new URL(AppConstant.Ip_url+"api/learning_add_feedback");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jon.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + new PrefManager(CourseLIstActivity.this).getToken());

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jon);
                    Log.e("resp_data",jon);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code2 = urlConnection.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.contains("error")) {
                Toast.makeText(CourseLIstActivity.this,"Error while submiting rating",Toast.LENGTH_SHORT).show();

            }
            else{
            }
            try{
                verification_progress.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.e("intentis","new");
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        flag_update = false;
        courseAdapter.notifyDataSetChanged();
    }
}
