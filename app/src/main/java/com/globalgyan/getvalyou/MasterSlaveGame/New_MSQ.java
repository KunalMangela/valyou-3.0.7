package com.globalgyan.getvalyou.MasterSlaveGame;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.androidhiddencamera.CameraConfig;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.Activity_Fib;
import com.globalgyan.getvalyou.CameraPreview;
import com.globalgyan.getvalyou.CheckP;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.ToadysgameModel;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.QuestionModel;
import com.globalgyan.getvalyou.simulation.core.DownloadManagerPro;
import com.globalgyan.getvalyou.simulation.report.listener.DownloadManagerListener;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.iambedant.text.OutlineTextView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.HttpGet;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.VerifyResult;
import com.pluscubed.recyclerfastscroll.RecyclerFastScroller;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.uncopt.android.widget.text.justify.JustifiedTextView;
import com.wunderlist.slidinglayer.SlidingLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;
import tyrantgit.explosionfield.ExplosionField;
import uk.co.senab.photoview.PhotoViewAttacher;

import static com.globalgyan.getvalyou.ProPic.mFaceId1;

public class New_MSQ extends AppCompatActivity implements DownloadManagerListener {
    //masterslave
    Typeface tf1,tf2,tf3;
    static NestedScrollView ques_scrollview;
    JustifiedTextView paragraph;
    private SimpleLocation location;
    public static String flag_sizechanged="";
    public static RelativeLayout qno_layout;
    int height, width;
    private SimpleGestureFilter detector;
    public static String crcans="";
    public static int crcopt = 0;
    boolean flag_scroll=false;
    public static FrameLayout para_frame;
    public static RelativeLayout below_layout;
    public static int height1,height2;
    static Animation ques_anim;
    static Animation ques_anim_left;
    Animation fadeOut;
    Option_clickable option_clickable;
    RelativeLayout clickable_options_layout;
    static RelativeLayout main_ques_layout_window;
    boolean flag_isvisible=false, flag_rc_scrolling=false,para_touch_boolean=false;
    int count=0;
    int cis=0;
    static RecyclerView rc_options_non_clickable;
    int para_count=0;
    static Vibrator vibrator;
    boolean close_on_get_ques = false;
    static Animation blinking_anim;
    int picture_count=0;
    int pic_frequency;
    //new
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    Face mface = null;
    //changes new design
    public static int crct;
    boolean flag_is_present_id=false;
    UUID faceid1, faceid2;
    Dialog alertDialog,proceed_dialog,alert_on_close;
    boolean alert_onclose_showing = false;
    boolean checkonp = true;
    CameraConfig mCameraConfig;
    boolean wtf;
    int scrollx,scrolly;
    ArrayList<String> qid_array;
    QuestionModel qsmodel;
    String newquid;
    Dialog dialog,dialoge;
    int fulltime;
    ArrayList<QuestionModel> qs=new ArrayList<>();
    File fol;
    public static int pos_clicked;
    List<Face> faces;
    ArrayList<Integer> audio=new ArrayList<>();
    ArrayList<String> assets = new ArrayList<>();
    ArrayList<Integer> levels=new ArrayList<>();
    ArrayList<Integer> asset_id=new ArrayList<>();

    private CountDownTimer countDownTimer;
    private long startTime = 180 * 1000;
    private final long interval = 1 * 1000;
    private long secLeft = startTime;
    DefaultHttpClient httpClient;
    HttpURLConnection urlConnection;
    boolean flagarrow_lick=false;
    HttpPost httpPost;
    HttpGet httpGet;
    int qansr = 0,scrollcount=0;
    Bitmap storedpic;
    //second boolean to prevent nonclickable list from scrolling
    boolean flag_click=false,click_to_prevent_scroll_list=false;
    public static boolean list_tick_show=false;
    public static RecyclerView.OnItemTouchListener disabler;
    int img_count;
    FontTextView timerrr, dqus,timer_text,timer_img;
    OutlineTextView qno;
    String tres = "";
    int code1,code2,code3;
    static DownloadManagerPro dm;
    static File folder;
    int cqa = 0;
    IntentFilter filter;
    ImageView para_image;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null;
    ArrayList<String> qidj, ansj, stj, endj,qtid;
    JSONArray questions;
    JSONArray audios=new JSONArray();
    NestedScrollView para_scroller;
    static String json = "", jso = "", Qus = "", Qs = "", token = "", token_type = "", quesDis = "",assessg="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int score = 0;
    ArrayList<String> optioncard;
    ImageView close;
    URL ppp;
    int pauseCount = 0, qc = 0, qid = 0, tq = 0, ac = -1,cnt=0,no_of_ques_in_list=0,anim_limit,height_scrollview_in_pixel,no_of_current_ques_per_para=0;
    SimpleDateFormat df1;
    Calendar c;
    LinearLayout cl1, cl2, cl3, cl4, cl5;
    FontTextView o1, o2, o3, o4, o5;
    String uidd, adn, ceid, gdid, game, cate, tgid, sid, aid,tg_groupid,certifcation_id,course_id,lg_id;
    private ExplosionField mExplosionField;
    String candidateId;
    RelativeLayout rel_bg_temp;
    File filepro;
    int optionsize;
    boolean isvisibleoption=false;
    String now;

    boolean quit_warning_showing = false;
    int para_image_count = 0;

    int paragraph_count = 0;
    //  KProgressHUD assess_progress;

    Dialog assess_progress;

    //no use
    KProgressHUD progressDialog;


    boolean show_ques = false;
    //KProgressHUD quesLoading;

    Dialog quesLoading;


    //KProgressHUD verification_progress;

    Dialog verification_progress;

    FrameLayout frameimageim;

    public static MediaPlayer mp_click;


    RelativeLayout timer_ques;

    boolean proceed_dialog_showing = false;


    public static LinearLayout weightedlinear;
    String faceid;
    public static String mImageFileLocation = "";
    Uri imageUri;
    Bitmap bmpother;

    ArrayList<Temp_model> temp_list=new ArrayList<>();
    int count_detect_no=0,image_face_count=0,auto_capt_count=0;
    ArrayList<UUID> detectedfaceid = new ArrayList<>();
    ArrayList<String> imagelocation = new ArrayList<>();
    ArrayList<Bitmap> autotakenpic = new ArrayList<>();
    ArrayList<String > facedetectedimagelocation = new ArrayList<>();
    String autocapimg1loc1, autocapimg1loc2,autocapimg1loc3;
    ArrayList<String > autocapimgloc = new ArrayList<>();
    ArrayList<String > imgurllist = new ArrayList<>();
    String img1,img2="",img3="";
    String replacedurl;
    private Camera camera;
    Timer timer1;
    CountDownTimer waitTimer;
    public int cameraId = 2;
    private CameraPreview mPreview;
    FrameLayout preview;
    FaceServiceClient faceServiceClient;
    PrefManager prefManager;
    Bitmap decodedByte;
    int piccapturecount;



    FrameLayout paraframe;


    float p1_rel_y=0,p2_rel_y=0;
    String path_image="";
    String direction="bottom";
    Animation slideUpAnimation,slidedownAnimation;
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR
            = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR
            = new AccelerateInterpolator();

    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2,astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;


    RecyclerFastScroller recyclerFastScroller;
    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    String first_msg = "", quit_msg="";
    SlidingLayer slidingLayer;
    FrameLayout arrowframe,master_parent;
    public SlidingUpPanelLayout slidingUpPanelLayout;
    SlidingUpPanelLayout.PanelState panelStateis,panelnew;
    ImageView a1;

    //leveling
    int currentlevel,maxlevel,min_level,q_count,int_qs_size,levelis=0,paraques=0,correctques=0,index=0,qc_index=0,firstpara=0;
    public static boolean islevelingenable=false;
    public static boolean parachangeis=false,isques_limit_exceed=false;

    //new
    public static int assetssize=0,acq=0,im_temp=0;
    public static File get_quesdata,get_ansdata,getdatafromfile;
    public static String old_get_ques_data,formdatanewstring="",mainformdatastring="",ans_submission_string="",ansdatafromfile="",postvaluearray="",postvalues="";
    public static boolean flaggameattempfirst=true;
    FrameLayout dr_new;




    LinearLayout master_text;
    FontTextView go_for_ques;
    boolean change_para_called = false;
    String get_nav = " ";

    String mfaceid;
    UUID mfaceid1;

    @SuppressLint({"NewApi", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_msq);
        //masterslave
     /*   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
  */      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        appConstant = new AppConstant(this);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        location = new SimpleLocation(this);
        dr_new=(FrameLayout)findViewById(R.id.dr_new);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        weightedlinear=(LinearLayout)findViewById(R.id.weightedlinear);
        qno_layout=(RelativeLayout)findViewById(R.id.qno_layout);
        faceServiceClient = new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(New_MSQ.this).get_frkey());

        tf1 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");
        tf2 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");
        tf3 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");

        qsmodel=new QuestionModel();

        paragraph=(JustifiedTextView)findViewById(R.id.paragraphis);
        createFolder();
        para_scroller=(NestedScrollView) findViewById(R.id.para_scrolleris);

        mp_click=MediaPlayer.create(this, R.raw.button_click);

        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);

        //recyclerFastScroller=(RecyclerFastScroller) findViewById(R.id.fastscroll);

        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        timer_ques = (RelativeLayout)findViewById(R.id.rl_above);

        dm = new DownloadManagerPro(this.getApplicationContext());
        dm.init(folder.getPath(), 12, New_MSQ.this);
        para_image=(ImageView)findViewById(R.id.para_imageis);

        paraframe = (FrameLayout)findViewById(R.id.para_frame);
        findViewById(R.id.para_frame).requestFocus();
        frameimageim=(FrameLayout)findViewById(R.id.frameimageim);
        //  ques=(TextView)findViewById(R.id.qdis);
        //  ques_scrollview=(NestedScrollView)findViewById(R.id.firstscroll);

        //  timer_seek=(IndicatorSeekBar)findViewById(R.id.timer_seek);


//        verification_progress = KProgressHUD.create(MSQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        arrowframe=(FrameLayout) findViewById(R.id.rc_frame);
        master_parent=(FrameLayout) findViewById(R.id.master_parent);

        //  slidingUpPanelLayout=(SlidingUpPanelLayout)findViewById(R.id.sliding_layoutis);

         slidingLayer = (SlidingLayer) findViewById(R.id.slidingLayer1);
        a1=(ImageView)findViewById(R.id.arrow1);
        master_text=(LinearLayout) findViewById(R.id.master);
        go_for_ques = (FontTextView)findViewById(R.id.go_for_ques);


        get_nav =new PrefManager(New_MSQ.this).getNav();

         blinking_anim = new AlphaAnimation(0.0f, 1.0f);
        blinking_anim.setDuration(500); //You can manage the blinking time with this parameter
        blinking_anim.setStartOffset(10);
        blinking_anim.setRepeatMode(Animation.REVERSE);
        blinking_anim.setRepeatCount(Animation.INFINITE);

        verification_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        verification_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verification_progress.setContentView(R.layout.planet_loader);
        Window window1 = verification_progress.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        verification_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        verification_progress.setCancelable(false);
        GifView gifView2 = (GifView) verification_progress.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();


        rc_options_non_clickable=(RecyclerView)findViewById(R.id.non_clickableoptions_listis);
        para_frame=(FrameLayout)findViewById(R.id.para_frameis);
        below_layout=(RelativeLayout)findViewById(R.id.below_layout);
        //options_clickable
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        String[] options_array={"A","B","C","D","E","F","G","H","I","J","K","L","M"};
        int no_options=5;
        String [] confirm_options=new String[no_options];

        for(int i=0;i<5;i++){
            confirm_options[i]=options_array[i];
        }
//        rc_options.setAdapter(new OptionsAdapter(this,confirm_options));
        //options_non_clickable
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_options_non_clickable.setLayoutManager(linearLayoutManager1);
        String[] options_array_non_clikable={"Q1: Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating?","Option A:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option B:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option C:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option D:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option E:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option F","Option G"};
        final String [] confirm_options_non_clickable=new String[no_options+1];
        for(int i=0;i<6;i++){
            confirm_options_non_clickable[i]=options_array_non_clikable[i];
        }
        // rc_options_non_clickable.setAdapter(new OptionAdapterNonClikable(this,confirm_options_non_clickable));
        filter = new IntentFilter("Show_screen");
        this.registerReceiver(mReceiver, filter);

        //option_non_clickable_temp
        LinearLayoutManager linearLayoutManager11 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        //  rc_options_non_clickable_temp.setAdapter(new OptionAdapterNonClikable(this,confirm_options_non_clickable));

        para_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("eventis","paratouch");
                flag_sizechanged="para";
                //changeSize();
                return false;
            }
        });

        paragraph.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                flag_sizechanged="para";
                Log.e("eventis","paratouch");
               // changeSize();
                return false;
            }
        });

        /*rc_options_non_clickable.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("eventis","belowtouch");
                return false;
            }
        });*/
        prefManager = new PrefManager(New_MSQ.this);


        mfaceid = prefManager.getprofilefaceid();

        mfaceid1 = UUID.fromString(mfaceid);


        clickable_options_layout=(RelativeLayout)findViewById(R.id.clickable_option_layout_is);

        camera = getCameraInstance();
        preview = (FrameLayout) findViewById(R.id.camera_previewis);

        paragraph.setTypeface(tf1);
        disabler = new RecyclerViewDisabler();

//        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.blinking_anim.slideup);
//
//        slidedownAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.blinking_anim.slidedown);

        //  ques.setTypeface(tf2);
        paragraph.setText("Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating the subconscious, and is offered a chance to have his criminal history erased as payment for the implantation of another person's idea into a target's subconscious.[4] The ensemble cast additionally includes Ken Watanabe, Joseph Gordon-Levitt, Marion Cotillard, Ellen Page, Tom Hardy, Dileep Rao, Cillian Murphy, Tom Berenger, and Michael Caine.\n" +
                "\n" + "After the 2002 completion of Insomnia, Nolan presented to Warner Bros. a written 80-page treatment about a horror film envisioning  based on lucid dreaming.[5] Deciding he needed more experience before tackling a production of this magnitude and complexity, Nolan retired the project and instead worked on 2005's Batman Begins, 2006's The Prestige, and The Dark Knight in 2008.[6] The treatment was revised over 6 months and was purchased by Warner in February 2009.[7] Inception was filmed in six countries, beginning in Tokyo on June 19 and ending in Canada on November 22.[8] Its official budget was $160 million, split between Warner Bros and Legendary.[9] Nolan's reputation and success with The Dark Knight helped secure the film's $100 million in advertising expenditure.\n" +
                " Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating the subconscious, and is offered a chance to have his criminal history erased as payment for the implantation of another person's idea into a target's subconscious.[4] The ensemble cast additionally includes Ken Watanabe, Joseph Gordon-Levitt, Marion Cotillard, Ellen Page, Tom Hardy, Dileep Rao, Cillian Murphy, Tom Berenger, and Michael Caine.\n" +
                "\n" + "  After the 2002 completion of Insomnia, Nolan presented to Warner Bros.");

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;


        height_scrollview_in_pixel=getPixel(New_MSQ.this,400);
        //   main_scroller.setLayoutParams(new LinearLayout.LayoutParams(width,height));

        //  para_frame.setLayoutParams(new LinearLayout.LayoutParams(width,height-getPixel(MasterSlaveGame.this,150)));
        // height=height-getPixel(MasterSlaveGame.this,180);
        // ques_frame.setLayoutParams(new LinearLayout.LayoutParams(width,height));
        // para_scroller.scrollTo(0,para_scroller.getTop());
        // main_scroller.scrollTo(0,main_scroller.getTop());

        final Animation animation_right =
                AnimationUtils.loadAnimation(New_MSQ.this,
                        R.anim.blink);

        ques_anim =
                AnimationUtils.loadAnimation(New_MSQ.this,
                        R.anim.right);
        ques_anim_left =
                AnimationUtils.loadAnimation(New_MSQ.this,
                        R.anim.left);
        fadeOut = new AlphaAnimation(0.0f, 1.0f);



        para_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        master_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                master_text.setEnabled(false);
                arrowframe.setEnabled(false);
                master_text.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        master_text.setEnabled(true);
                        arrowframe.setEnabled(true);
                        master_parent.setEnabled(true);
                    }
                }.start();

                if(slidingLayer.isOpened()){
                    //slidingLayer.closeLayer(true);
//                    a1.setImageResource(R.drawable.msq_right_arrow);

                }
                else{
                    slidingLayer.openLayer(true);
//                    a1.setImageResource(R.drawable.msq_left_arrow);
                }
            }
        });



        master_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                master_text.setEnabled(false);
                arrowframe.setEnabled(false);
                master_text.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        master_text.setEnabled(true);
                        arrowframe.setEnabled(true);
                        master_parent.setEnabled(true);
                    }
                }.start();

                if(slidingLayer.isOpened()){
                 //   slidingLayer.closeLayer(true);
//                    a1.setImageResource(R.drawable.msq_right_arrow);

                }
                else{
                    slidingLayer.openLayer(true);
//                    a1.setImageResource(R.drawable.msq_left_arrow);
                }
            }
        });

        arrowframe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("click","arrow");
                Log.e("clickprev",String.valueOf(panelStateis));
                Log.e("clicknew",String.valueOf(panelnew));

                master_text.setEnabled(false);
                arrowframe.setEnabled(false);
                master_text.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        master_text.setEnabled(true);
                        arrowframe.setEnabled(true);
                        master_parent.setEnabled(true);
                    }
                }.start();

                if(slidingLayer.isOpened()){
                    slidingLayer.closeLayer(true);
//                    a1.setImageResource(R.drawable.msq_right_arrow);

                }
                else{
                    slidingLayer.openLayer(true);
//                    a1.setImageResource(R.drawable.msq_left_arrow);
                }
            }
        });



        slidingLayer.setOnInteractListener(new SlidingLayer.OnInteractListener() {
            @Override
            public void onOpen() {
                try {
                    if (show_ques) {
                        go_for_ques.setVisibility(View.VISIBLE);
                        arrowframe.setVisibility(View.INVISIBLE);
                    } else {
                        arrowframe.setVisibility(View.VISIBLE);
                        go_for_ques.setVisibility(View.INVISIBLE);
                    }
                    a1.setImageResource(R.drawable.msq_left_arrow);

                    master_text.setVisibility(View.GONE);
                    para_image.setClickable(true);
                    rc_options_non_clickable.setVerticalScrollBarEnabled(false);

                    option_clickable.notifyDataSetChanged();
                }catch (Exception e){

                }

            }

            @Override
            public void onShowPreview() {

            }

            @Override
            public void onClose() {
       /*         a1.setImageResource(R.drawable.msq_right_arrow);
                master_text.setVisibility(View.VISIBLE);
*/             // arrowframe.setVisibility(View.VISIBLE);
                arrowframe.setVisibility(View.VISIBLE);
                a1.setImageResource(R.drawable.msq_right_arrow);
                master_text.setVisibility(View.VISIBLE);
                para_image.setClickable(false);
                rc_options_non_clickable.setVerticalScrollBarEnabled(true);
                rc_options_non_clickable.setScrollbarFadingEnabled(false);
                rc_options_non_clickable.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
                option_clickable.notifyDataSetChanged();

                /*final Animation animation_fade = new AlphaAnimation(0.0f,1.0f);
                animation_fade.setInterpolator(new AccelerateInterpolator());

                animation_fade.setDuration(100);
                master_text.startAnimation(animation_fade);*/
            }

            @Override
            public void onOpened() {


                a1.setImageResource(R.drawable.msq_left_arrow);
                master_text.setVisibility(View.GONE);
           /*     rc_options_non_clickable.setVerticalScrollBarEnabled(false);
                option_clickable.notifyDataSetChanged();*/

            }

            @Override
            public void onPreviewShowed() {

            }

            @Override
            public void onClosed() {
                arrowframe.setVisibility(View.VISIBLE);
                if(change_para_called){

                    show_ques = false;
                    startTime= Long.parseLong(qsmodel.getTime());
                    startTime=startTime*1000;
                    Log.e("go_ques_timeris",qsmodel.getTime());
                    countDownTimer = new MyCountDownTimer(startTime, interval);
                    timerrr.setVisibility(View.VISIBLE);

                    startTimer();
                  //  go_for_ques.setEnabled(false);
                    change_para_called = false;
                }



                /*a1.setImageResource(R.drawable.msq_right_arrow);
                master_text.setVisibility(View.VISIBLE);*/
                /*final Animation animation_fade = new AlphaAnimation(0.0f,1.0f);
                animation_fade.setInterpolator(new AccelerateInterpolator());

                animation_fade.setDuration(100);
                master_text.startAnimation(animation_fade);*/
            }
        });

        //new
        c = Calendar.getInstance();
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        progressDialog = KProgressHUD.create(New_MSQ.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Logging in")
                .setDimAmount(0.7f)
                .setCancellable(false);
//        quesLoading = KProgressHUD.create(MSQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("please wait")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        quesLoading = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        quesLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        quesLoading.setContentView(R.layout.assess_ques_loading_loader);
        Window window = quesLoading.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        quesLoading.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        quesLoading.setCancelable(false);
        GifView gifView1 = (GifView) quesLoading.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        ImageView close_btn = (ImageView) quesLoading.findViewById(R.id.close_loader);


//        assess_progress = KProgressHUD.create(MSQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        assess_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        assess_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assess_progress.setContentView(R.layout.planet_loader);
        Window window2 = assess_progress.getWindow();
        WindowManager.LayoutParams wlp2 = window2.getAttributes();
        wlp2.gravity = Gravity.CENTER;
        wlp2.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window2.setAttributes(wlp2);
        assess_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        assess_progress.setCancelable(false);
        GifView gifView3 = (GifView) assess_progress.findViewById(R.id.loader);
        gifView3.setVisibility(View.VISIBLE);
        gifView3.play();
        gifView3.setGifResource(R.raw.loader_planet);
        gifView3.getGifResource();



        qid_array=new ArrayList<>();


        candidateId = PreferenceUtils.getCandidateId(New_MSQ.this);
        qidj = new ArrayList<>();
        ansj = new ArrayList<>();
        stj = new ArrayList<>();
        endj = new ArrayList<>();
        qtid=new ArrayList<>();

        //need to change
        try {
            uidd = getIntent().getExtras().getString("uidg");
            adn = getIntent().getExtras().getString("adn");
            ceid = getIntent().getExtras().getString("ceid");
            gdid = getIntent().getExtras().getString("gdid");
            game = getIntent().getExtras().getString("game");
            cate = getIntent().getExtras().getString("category");
            tgid = getIntent().getExtras().getString("tgid");
            aid = getIntent().getExtras().getString("aid");
            token = getIntent().getExtras().getString("token");
            assessg=getIntent().getExtras().getString("assessgroup");
            tg_groupid = getIntent().getExtras().getString("TG_GroupId");
            certifcation_id = getIntent().getExtras().getString("certification_id");
            course_id = getIntent().getExtras().getString("course_id");
            lg_id = getIntent().getExtras().getString("lg_id");
            pic_frequency = getIntent().getIntExtra("pic_frequency",0);

            Log.e("pic_frequency",""+pic_frequency);

            startTime= Long.parseLong(getIntent().getExtras().getString("timeis"));
            startTime=startTime*1000;
        } catch (Exception ex) {

        }



        //new changes
        jsonMakeFile(cate+game+gdid+ceid+uidd+tgid," ");

        first_msg = "We are submitting your answers and you will not be able to answer again the same category";

        quit_msg = first_msg+ " "+ cate;

        timerrr = (FontTextView) findViewById(R.id.timerris);
        timer_img = (FontTextView) findViewById(R.id.timer_img);
        timer_text = (FontTextView) findViewById(R.id.timer_text);
        qno = (OutlineTextView) findViewById(R.id.qnois);
        qno.setTypeface(tf3);
        // dqus = (FontTextView) findViewById(R.id.qdis);
      //  countDownTimer = new MyCountDownTimer(startTime, interval);
       // timerrr.setText(String.valueOf(startTime / 1000));
        timerrr.setVisibility(View.INVISIBLE);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pauseCount == 0) {
                    //    vdo_performance_ques_timer.cancel();
                    close.setEnabled(false);

                   /* AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MSQ.this, R.style.DialogTheme);
// ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = LayoutInflater.from(MSQ.this);
                    View dialogView = inflater.inflate(R.layout.quit_warning, null);
                    dialogBuilder.setView(dialogView);


                    alertDialog = dialogBuilder.create();

                    alertDialog.setCancelable(false);
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

                    alertDialog.show();*/

                    alert_on_close = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alert_on_close.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert_on_close.setContentView(R.layout.assess_quit_warning);
                    Window window = alert_on_close.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alert_on_close.setCancelable(false);

                    alert_on_close.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alert_on_close.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView)alert_on_close.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alert_on_close.findViewById(R.id.alien);

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();



                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView)alert_on_close.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView)alert_on_close.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            close.setEnabled(true);
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    alert_onclose_showing = false;

                                    alert_on_close.dismiss();
                                   // changeQues();
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            close.setEnabled(true);
                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        alert_on_close.dismiss();
                                        alert_onclose_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        alert_on_close.dismiss();
                                        alert_onclose_showing = false;

                                        quit_warning_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();


                        }
                    });

                    try{
                        alert_on_close.show();
                        alert_onclose_showing = true;
                        quit_warning_showing = true;
                    }catch(Exception e){

                    }
                } else {
                    if (qansr > 0) {
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        try {
                            countDownTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }


            }
        });




        para_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayImage();
            }
        });
        ImageView imgfullscreen_close=(ImageView)findViewById(R.id.closeimageis);
        imgfullscreen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close.setEnabled(true);
                close.setClickable(true);
                frameimageim.setVisibility(View.GONE);
            }
        });



        new CountDownTimer(100,100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                //t923.cancel(true);

                //new changes remove show dialogue call and add it into Getquestions
                ConnectionUtils connectionUtils=new ConnectionUtils(New_MSQ.this);
                if (connectionUtils.isConnectionAvailable()) {
                    //  flag_netcheck = false;

                    if(flaggameattempfirst){
                        new GetQues().execute();
                    }else {
                        loadofflinedata();
                    }

                }else {
                    try {
                        //  qs_progress.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    //  flag_netcheck=false;
                    Toast.makeText(New_MSQ.this, "Please check your internet connection !", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }.start();


//        checkfacedetection();

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close_on_get_ques = true;
                try{
                    new GetQues().cancel(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Intent i = new Intent(New_MSQ.this, HomeActivity.class );
                Log.e("navigation","ass");
                new PrefManager(New_MSQ.this).set_back_nav_from("ass_land");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tabs","normal");
                startActivity(i);
                finish();

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            }
        });


    }




    @SuppressLint("ClickableViewAccessibility")

    private void createFolder() {
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "SimulationData");
        boolean success = true;
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }
    public  void clickMethods(final int position) {
        list_tick_show=true;
        //  click_to_prevent_scroll_list=true;
        flag_click=true;
        //   rc_options_non_clickable.addOnItemTouchListener(disabler);

        // option_clickable.notifyDataSetChanged();

        //if count is equals to question count

        new CountDownTimer(10, 10) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                anim_limit=qc+1;

                if(islevelingenable) {
                    //if count is equals to question count
                    if (anim_limit == q_count) {
                        giveAnswers(position);
                    } else {
                        //put dta in temp list
                   /* temp_ques_layout_window.setVisibility(View.VISIBLE);
                    animListdata();
                    giveAnswers(position);
                    main_ques_layout_window.startAnimation(ques_anim_left);
                    Log.e("animstatus","putdata");
                    temp_ques_layout_window.startAnimation(ques_anim);
                    hideArrow();*/
                        giveAnswers(position);


                    }
                }else {
                    //no leveling
                    int cis=0;
                    if(isques_limit_exceed){
                        cis=int_qs_size;
                    }else {
                        cis=q_count;
                    }

                    if(anim_limit==cis) {
                        giveAnswers(position);
                    }else {
                        //put dta in temp list
                   /* temp_ques_layout_window.setVisibility(View.VISIBLE);
                    animListdata();
                    giveAnswers(position);
                    main_ques_layout_window.startAnimation(ques_anim_left);
                    Log.e("animstatus","putdata");
                    temp_ques_layout_window.startAnimation(ques_anim);
                    hideArrow();*/
                        giveAnswers(position);


                    }
                }
            }
        }.start();


    }

    private void animListdata() {
        if (anim_limit < q_count) {
            qsmodel=qs.get(anim_limit);


            qno.setText(anim_limit + 1 + "/" + q_count);

            try {
                //single question

                ArrayList<String> optioncardis = new ArrayList<>();
                optioncard.add("Q. "+qsmodel.getQuestion());
                if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                    String s1 = qsmodel.getOpt1();
                    s1 = s1.trim();
                    optioncard.add(""+s1);
                }
                if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                    String s2 = qsmodel.getOpt2();
                    s2 = s2.trim();
                    optioncard.add(""+s2);
                }
                if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                    String s3 = qsmodel.getOpt3();
                    s3 = s3.trim();
                    optioncard.add(""+s3);
                }
                if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                    String s4 = qsmodel.getOpt4();
                    s4 = s4.trim();
                    optioncard.add(""+s4);
                }
                if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                    String s5 = qsmodel.getOpt5();
                    s5 = s5.trim();
                    optioncard.add(""+s5);
                }

                OPtionNonclickableAdapterTemp oPtionNonclickableAdapterTemp=new OPtionNonclickableAdapterTemp(this,optioncardis);
                Log.e("animstatus","putdata");
                String[] options_array={"A","B","C","D","E","F","G","H","I","J","K","L","M"};
                int no_options=optioncardis.size();
                String [] confirm_options=new String[no_options-1];

                for(int i=0;i<no_options-1;i++){
                    confirm_options[i]=options_array[i];
                }
                //     rc_options.setAdapter(new NewOptionsAdapter(this,confirm_options));

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private void giveAnswers(int position) {
        try {
            countDownTimer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        paraques++;


        c = Calendar.getInstance();
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
       // now = df1.format(c.getTime()).toString();
        no_of_current_ques_per_para++;
        no_of_ques_in_list++;
        if(position==0){
            if (crct == 0) {
                correctques++;
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("A");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()));
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("A");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

            }
        }

        if(position==1){
            if (crct == 1) {
                correctques++;
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("B");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("B");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==2){
            if (crct == 2) {
                correctques++;
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("C");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("C");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==3){
            if (crct == 3) {
                correctques++;
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("D");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                vibrator.vibrate(500);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("D");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==4){
            if (crct == 4) {
                correctques++;

                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("E");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

            } else {
                vibrator.vibrate(500);
                Handler han = new Handler();

                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("E");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==1000){
            vibrator.vibrate(500);
                Handler han = new Handler();

                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            qidj.add(qsmodel.getQ_id());
                            qtid.add(qsmodel.getQT_id());

                            qid_array.add(qsmodel.getQ_id());
                            ansj.add(" ");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                            if(islevelingenable){
                                qs.remove(qc_index);

                            }
                            formdatalocal(qc,1);
                            qc++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

        }


    }


    public static void hideArrow(){

    }
    public int getPixel(New_MSQ simulationActivity, int dps){
        Resources r = simulationActivity.getResources();

        int  px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }

    @Override
    public void OnDownloadStarted(long taskId) {

    }

    @Override
    public void OnDownloadPaused(long taskId) {

    }

    @Override
    public void onDownloadProcess(long taskId, double percent, long downloadedLength) {

    }

    @Override
    public void OnDownloadFinished(long taskId) {
        Log.e("image_count", ""+img_count);

        img_count=img_count-1;
        if(img_count==0){

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    startTime = questions.length() * 10000 * 1000;
                    changeParagraph(-1);
                    Log.e("www","paracalledn"+"img"+img_count);
                    //startTimer();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            slidingLayer.openLayer(true);
                            a1.setImageResource(R.drawable.msq_left_arrow);

                            master_text.setEnabled(false);
                            arrowframe.setEnabled(false);


                            new CountDownTimer(1000, 1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    master_text.setEnabled(true);
                                    arrowframe.setEnabled(true);

                                }
                            }.start();
                                   /* anim_frame.setVisibility(View.VISIBLE);
                                    animarrow1.startAnimation(slideUpAnimation);
                                    animarrow2.startAnimation(slideUpAnimation);*/
                        }
                    });

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        startanim();
                    }

                    startVerification();
                    //    checkfacedetection();
                    try {
                        if (quesLoading.isShowing()) {
                            quesLoading.dismiss();
                        }
                    }catch (Exception e){

                    }
                }
            });


        }
    }

    @Override
    public void OnDownloadRebuildStart(long taskId) {

    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {

    }

    @Override
    public void OnDownloadCompleted(long taskId) {

    }

    @Override
    public void connectionLost(long taskId) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                qs.clear();
                try {
                    if (quesLoading.isShowing()) {
                        quesLoading.dismiss();
                    }
                }catch (Exception e){

                }
                try {
                    if (alertDialog.isShowing()) {

                    } else {
                        showGetQuesAgainDialogue();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    showGetQuesAgainDialogue();
                }

            }
        });
    }

    public void showGetQuesAgainDialogue() {
        Log.e("respmsg","dialogue");

      /*  final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                MSQ.this, R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(MSQ.this);
        View dialogView = inflater.inflate(R.layout.retry_anim_dialogue, null);

        dialogBuilder.setView(dialogView);


        alertDialog = dialogBuilder.create();


//                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

        alertDialog.show();*/
        alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();

                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(New_MSQ.this);
                        if (connectionUtils.isConnectionAvailable()) {
                            new GetQues().execute();
                        } else {
                            quesLoading.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (quesLoading.isShowing() && quesLoading != null) {
                                        quesLoading.dismiss();


                                    }
                                    showGetQuesAgainDialogue();
                                }
                            }.start();
                        }

                    }
                }.start();




            }
        });

        try {
            if(!alertDialog.isShowing())
                alertDialog.show();
        }
        catch (Exception e){

        }
    }

    public void go_ques(View view) {


        go_for_ques.setEnabled(false);
        new CountDownTimer(800, 800) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                go_for_ques.setEnabled(true);
            }
        }.start();

        if(change_para_called) {
            show_ques =false;
            slidingLayer.closeLayer(true);
            startTime = Long.parseLong(qsmodel.getTime());
            startTime = startTime * 1000;
            Log.e("go_ques_timeris", qsmodel.getTime());
            countDownTimer = new MyCountDownTimer(startTime, interval);
            timerrr.setVisibility(View.VISIBLE);
            startTimer();
            change_para_called = false;
          //  go_for_ques.setEnabled(false);
        }
        else{
            slidingLayer.closeLayer(true);
          //  arrowframe.setVisibility(View.VISIBLE);
        }
      //  go_for_ques.setVisibility(View.INVISIBLE);

    }


    public class MyCountDownTimer extends CountDownTimer {


        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }

        @Override
        public void onTick(long millisUntilFinished) {
            //  timer_seek.setMin(0);
            timerrr.setText("" + millisUntilFinished / 1000);

            secLeft = millisUntilFinished;

            long sec_lef = millisUntilFinished/1000;

            int sec= (int) (fulltime/1000-secLeft/1000);

            final int MINUTES_IN_AN_HOUR = 60;
            final int SECONDS_IN_A_MINUTE = 60;



            int minutes =  (sec / SECONDS_IN_A_MINUTE);
            sec -= minutes * SECONDS_IN_A_MINUTE;

            int hours = minutes / MINUTES_IN_AN_HOUR;
            minutes -= hours * MINUTES_IN_AN_HOUR;

            if(sec_lef<=5){
                timerrr.setTextColor(Color.parseColor("#ff0000"));
                timer_text.setTextColor(Color.parseColor("#ff0000"));
                timer_img.setBackgroundResource(R.drawable.msq_red_timer);

                /*timerrr.startAnimation(blinking_anim);
                timer_text.startAnimation(blinking_anim);

              timer_img.startAnimation(blinking_anim);*/
            }

            if( sec_lef==1){
               // blinking_anim.cancel();
            }

        }


        @Override

        public void onFinish() {




                // blinking_anim.cancel();
            if(islevelingenable){
                if (qc < q_count) {

                    clickMethods(1000);

                } else {

                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }
            }else {
                int cis=0;
                if(isques_limit_exceed){
                    cis=int_qs_size;
                }else {
                    cis=q_count;
                }
                if (qc < cis) {

                    clickMethods(1000);

                } else {

                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }


            }






        }
    }
    private class GetQues extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            img_count=0;
            try {
                if((quesLoading!=null)&&(!quesLoading.isShowing()))
                quesLoading.show();
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {
                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/GetQuestions");*/

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("CE_Id", ceid)
                            .object("Game", game)
                            .object("Category", cate)
                            .object("GD_Id", gdid)
                            .build().toString();


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/v1/GetQuestions");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();


                    Log.e("dismiss", ""+is);


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alertDialog.isShowing()) {

                                } else {
                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                showGetQuesAgainDialogue();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alertDialog.isShowing()) {

                                } else {
                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                showGetQuesAgainDialogue();
                            }

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alertDialog.isShowing()) {

                                } else {
                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                showGetQuesAgainDialogue();
                            }

                        }
                    });
                    e.printStackTrace();
                }

                String responseString = readStream(urlConnection.getInputStream());
                Log.e("Response", responseString);
                  result1 =responseString;

            } catch (Exception e) {
                e.getMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        qs.clear();
                        try {
                            if (quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                        }catch (Exception e){

                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                showGetQuesAgainDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            showGetQuesAgainDialogue();
                        }

                    }
                });
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            return result1;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            jsonMakeFile(cate+game+gdid+ceid+uidd+tgid,result);

            try {
                JSONArray data = new JSONArray(result);
                Log.e("QUESDATA", data.length() + "");
                // JSONArray sidd = data.getJSONArray(0);
                final JSONObject sidobj = data.getJSONObject(0);
                sid = sidobj.getString("S_Id");
                q_count=sidobj.getInt("question_count");
                currentlevel=sidobj.getInt("currentGameLevel");
                maxlevel=sidobj.getInt("max_level");
                min_level=sidobj.getInt("min_level");
                if(currentlevel<min_level){
                    currentlevel=min_level;
                }
                if(currentlevel>maxlevel){
                    currentlevel=min_level;
                }
                levelis=currentlevel;
                Log.e("fbwwp", sid);
                JSONArray js1 = data.getJSONArray(1);



                for(int i=0;i<js1.length();i++){
                    JSONArray jsnew=js1.getJSONArray(i);
                    for(int j=0;j<jsnew.length();j++){
                        JSONObject jobjis=jsnew.getJSONObject(j);
                        audios.put(jobjis);
                    }

                }
                Log.e("adddddd", audios.length() + "");

                islevelingenable=true;
                para_image_count = audios.length();
                Log.e("para_image_count", para_image_count + "");



                for (int i = 0; i < audios.length(); i++) {


                    assets.add(i,"");
                    JSONObject aaqonj = audios.getJSONObject(i);
                    asset_id.add(i,aaqonj.getInt("QT_Id"));
                    String aaq = aaqonj.getString("Q_ChildQIds");

                    if(( aaqonj.getString("Q_Asset").length()>4)&&(aaqonj.getString("Q_Question").length()>4)) {
                        String tobe_add=aaqonj.getString("Q_Asset")+"7769896014"+aaqonj.getString("Q_Question");
                        assets.set(i,tobe_add);
                        img_count++;
                        String [] pic=aaqonj.getString("Q_Asset").split("/");
                        String last=pic[pic.length-1];
                        String [] lastname=last.split("\\.");
                        String name=lastname[0];
                        downloadData(aaqonj.getString("Q_Asset"),name);
                    }else if( aaqonj.getString("Q_Asset").equalsIgnoreCase(""))
                    {
                        assets.set(i,aaqonj.getString("Q_Question"));

                    }else {
                        assets.set(i,aaqonj.getString("Q_Asset"));
                        img_count++;
                        String [] pic=aaqonj.getString("Q_Asset").split("/");
                        String last=pic[pic.length-1];
                        String [] lastname=last.split("\\.");
                        String name=lastname[0];
                        downloadData(aaqonj.getString("Q_Asset"),name);
                    }
                    levels.add(i,aaqonj.getInt("GL_Level"));
                    StringTokenizer st = new StringTokenizer(aaq, ",");
                    audio.add(i,st.countTokens());

                }
                im_temp=img_count;
                for(int i=0;i<levels.size();i++){
                    Temp_model temp_model=new Temp_model();
                    temp_model.setLevel(levels.get(i));
                    temp_model.setCount(audio.get(i));
                    temp_list.add(temp_model);
                }
                int temp_count;

                try {
                    ArrayList<Integer> level_ques_count = new ArrayList<>();
                    for (int i = 0; i <= maxlevel; i++) {
                        level_ques_count.add(i, 0);
                    }

                    for (int i = min_level; i <= maxlevel; i++) {
                        level_ques_count.set(i, 0);
                    }


                    for(int i=min_level;i<=maxlevel;i++){
                        temp_count=0;
                        for(int j=0;j<temp_list.size();j++){
                            Temp_model temp_model=temp_list.get(j);
                            if(i==temp_model.getLevel()){
                                temp_count=temp_count+temp_model.getCount();
                            }
                            level_ques_count.set(i,temp_count);
                        }
                    }

                    for(int i=min_level;i<=maxlevel;i++){
                        Log.e("valueis",""+level_ques_count.get(i));
                        if(level_ques_count.get(i)==q_count){
                            islevelingenable=true;
                        }else {
                            islevelingenable=false;
                        }
                    }
                }catch (Exception e){
                    islevelingenable=false;
                    e.printStackTrace();
                }




                questions = data.getJSONArray(2);
                for(int i=0;i<questions.length();i++){
                    JSONObject js=questions.getJSONObject(i);
                    QuestionModel s=new QuestionModel();
                    s.setQT_id(js.getString("QT_Id"));
                    s.setQ_id(js.getString("Q_Id"));
                    s.setQuestion(js.getString("Q_Question"));
                    s.setOpt1(js.optString("Q_Option1"));
                    s.setOpt2(js.optString("Q_Option2"));
                    s.setOpt3(js.optString("Q_Option3"));
                    s.setOpt4(js.optString("Q_Option4"));
                    s.setOpt5(js.optString("Q_Option5"));
                    s.setCorrectanswer(js.getString("Q_Answer"));
                    s.setTime(js.getString("Q_MaxTime"));
                    qs.add(s);
                }
                int_qs_size=qs.size();
                if(q_count>qs.size()){
                    isques_limit_exceed=true;
                }

                timerrr.setVisibility(View.VISIBLE);
                Log.e("QUES", questions.length() + "");
                tq = questions.length();

                secLeft=startTime;
                final int MINUTES_IN_AN_HOUR = 60;
                final int SECONDS_IN_A_MINUTE = 60;

                int sec= (int) secLeft/1000;
                int minutes =  (sec / SECONDS_IN_A_MINUTE);
                sec -= minutes * SECONDS_IN_A_MINUTE;

                int hours = minutes / MINUTES_IN_AN_HOUR;
                minutes -= hours * MINUTES_IN_AN_HOUR;
                fulltime= (int) secLeft;

                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        Log.e("imagecount", ""+im_temp);

                        if(im_temp>0){

                        }else {
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                startanim();
                            }
                            startTime = questions.length() * 30 * 1000;
                            changeParagraph(-1);
                            Log.e("www","paracalleda"+" img"+im_temp);
                            // startTimer();

                            startVerification();
                            // checkfacedetection();
                            if(quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                            slidingLayer.openLayer(true);
                            a1.setImageResource(R.drawable.msq_left_arrow);
                            master_text.setEnabled(false);
                            arrowframe.setEnabled(false);


                            new CountDownTimer(1000, 1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    master_text.setEnabled(true);
                                    arrowframe.setEnabled(true);

                                }
                            }.start();
                           /* anim_frame.setVisibility(View.VISIBLE);
                            animarrow1.startAnimation(slideUpAnimation);
                            animarrow2.startAnimation(slideUpAnimation);*/
                        }
                    }
                }.start();


            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        qs.clear();
                        try {
                            if (quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                        }catch (Exception e){

                        }
                        try {
                            if ((alertDialog.isShowing())&&(alertDialog!=null)) {

                            } else {
                                showGetQuesAgainDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            showGetQuesAgainDialogue();
                        }
                    }
                });
                Log.e("QUES", "Error parsing data " + e.toString());
            }











        }
    }



    void startTimer() {
        countDownTimer.start();
    }
    public static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            try {
                BitmapFactory.decodeStream(new FileInputStream(f),null,o);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        }
        catch (FileNotFoundException e) {}
        return null;
    }
    private void changeParagraph(final int mm) {
        acq=mm;
        timerrr.setVisibility(View.INVISIBLE);
        change_para_called = true;
        show_ques = true;
        Log.e("mystatusis","changepis" +paragraph_count);
        /*go_for_ques.setVisibility(View.VISIBLE);
        arrowframe.setVisibility(View.INVISIBLE);*/

        para_count++;
        try {

            countDownTimer.cancel();
            // countDownTimer=null;
        }
        catch (Exception e){

            e.printStackTrace();
        }
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();


        go_for_ques.setEnabled(true);

        Log.e("qc is",String.valueOf(qc));
        if (acq == -1) {
            changeParagraph(0);
            changeQues();
            return;
        }else {
            Log.e("mystatus","changep" +paragraph_count);


           // timerrr.setVisibility(View.GONE);
            if(islevelingenable) {


                if (paragraph_count > 0 || paragraph_count == para_image_count - 1) {

                    if (quit_warning_showing) {


                    } else {
                        master_text.setEnabled(false);
                        arrowframe.setEnabled(false);


                        Log.e("calledchangepara", "" + paragraph_count);
                        try {

                            countDownTimer.cancel();
                            // countDownTimer=null;
                        } catch (Exception e) {

                            e.printStackTrace();
                        }


                        int i = para_count - 2;
                        String message = "";
                        String complete_msg = "";
                        String total_count = "";

                        switch (para_image_count) {

                            case 1:

                                total_count = "out of one master slave question";
                                break;

                            case 2:

                                total_count = "out of two master slave questions";
                                break;

                            case 3:

                                total_count = "out of three master slave questions";
                                break;

                            case 4:

                                total_count = "out of four master slave questions";
                                break;
                            case 5:

                                total_count = "out of five master slave questions";
                                break;
                            case 6:

                                total_count = "out of six master slave questions";
                                break;

                            case 7:

                                total_count = "out of seven master slave questions";
                                break;
                            case 8:

                                total_count = "out of eight master slave questions";
                                break;
                            case 9:

                                total_count = "out of nine master slave questions";
                                break;
                            case 10:

                                total_count = "out of ten master slave questions";
                                break;
                            case 11:

                                total_count = "out of eleven master slave questions";
                                break;
                            case 12:

                                total_count = "out of twelve master slave questions";
                                break;
                            case 13:

                                total_count = "out of thirteen master slave questions";
                                break;
                            case 14:

                                total_count = "out of fourteen master slave questions";
                                break;
                            case 15:

                                total_count = "out of fifteen master slave questions";
                                break;
                            case 16:

                                total_count = "out of sixteen master slave questions";
                                break;
                            case 17:

                                total_count = "out of seventeen master slave questions";
                                break;
                            case 18:

                                total_count = "out of eighteen master slave questions";
                                break;
                            case 19:

                                total_count = "out of nineteen master slave questions";
                                break;
                            case 20:

                                total_count = "out of twenty master slave questions";
                                break;
                        }
                        switch (paragraph_count) {

                            case 1:
                                message = "You have completed your first master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 2:
                                message = "You have completed your second master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 3:
                                message = "You have completed your third master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 4:
                                message = "You have completed your fourth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 5:
                                message = "You have completed your fifth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 6:
                                message = "You have completed your sixth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 7:
                                message = "You have completed your seventh master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 8:
                                message = "You have completed your eighth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 9:
                                message = "You have completed your ninth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 10:
                                message = "You have completed your tenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 11:
                                message = "You have completed your eleventh master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 12:
                                message = "You have completed your twelveth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 13:
                                message = "You have completed your thirteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 14:
                                message = "You have completed your fourteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 15:
                                message = "You have completed your fifteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 16:
                                message = "You have completed your sixteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 17:
                                message = "You have completed your seventeenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 18:
                                message = "You have completed your eighteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 19:
                                message = "You have completed your nineteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 20:
                                message = "You have completed your twentieth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                        }


//            showProceedDialog("You have completed your first audio questions");
                    }
                }


                paragraph_count++;
                master_text.setEnabled(true);
                arrowframe.setEnabled(true);

                //changing

                if (firstpara > 0) {

                    assets.remove(index);
                    levels.remove(index);
                    audio.remove(index);
                    asset_id.remove(index);
                    if (paraques == correctques) {
                        if (levelis < maxlevel) {
                            levelis++;
                        }
                    }else {
                        if (correctques == 0) {
                            if (levelis > min_level) {
                                levelis--;
                            }
                        }else {
                            levelis = levelis;
                        }

                    }
                    /*else {
                        levelis = levelis;
                    }*/

                }
                paraques = 0;
                correctques = 0;
                for (int i = 0; i < assets.size(); i++) {
                    if (levelis == levels.get(i)) {
                        index = i;
                        break;
                    }
                }


                firstpara++;
                //   ObjectAnimator.ofInt(main_scroller, "scrollY",  main_scroller.getTop()).setDuration(10).start();
                Log.e("acq", String.valueOf(index));
                Log.e("parachnage", "" + cis);
                cis++;
                if (assets.get(index).contains("7769896014")) {
                    String[] valuesare = assets.get(index).split("7769896014");
                    String imgis = valuesare[0];
                    String parais = valuesare[1];
                    final Animation animation_fade =
                            AnimationUtils.loadAnimation(New_MSQ.this,
                                    R.anim.fade_in);


                    String[] pic = imgis.split("/");
                    String last = pic[pic.length - 1];
                    String[] lastname = last.split("\\.");
                    String name = lastname[0];
                    final File image_file = new File(folder, pic[pic.length - 1]);
                    path_image = image_file.getPath();
                    final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());


                    //  paragraph.startAnimation(animation_fade);


                    paragraph.setText(parais);
                    new CountDownTimer(600, 600) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            para_image.setVisibility(View.VISIBLE);
                            paragraph.setVisibility(View.VISIBLE);


                            // para_scroller.requestChildFocus(paraframe,paraframe);
                            para_scroller.scrollTo(0, para_scroller.getTop());
                            para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                            para_scroller.smoothScrollTo(0, 0);
                            para_image.startAnimation(animation_fade);

                            paragraph.startAnimation(animation_fade);

                            Log.e("w n h", bmp.getWidth() + "," + bmp.getHeight());
                            //  para_image.setImageBitmap(decodeFile(image_file,300,400));

                            para_image.setImageBitmap(bmp);
                      /*      try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
*/

                        }
                    }.start();
                } else if ((assets.get(index).contains(".jpg")) || (assets.get(index).contains(".jpeg")) || (assets.get(index).contains(".png"))) {
                    paragraph.setVisibility(View.GONE);


                    String[] pic = assets.get(index).split("/");
                    String last = pic[pic.length - 1];
                    String[] lastname = last.split("\\.");
                    String name = lastname[0];
                    final File image_file = new File(folder, pic[pic.length - 1]);
                    path_image = image_file.getPath();
                    final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());

                    new CountDownTimer(600, 600) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            para_image.setVisibility(View.VISIBLE);
                            // para_scroller.requestChildFocus(paraframe,paraframe);
                            para_scroller.scrollTo(0, para_scroller.getTop());
                            para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                            para_scroller.smoothScrollTo(0, 0);
                            final Animation animation_fade =
                                    AnimationUtils.loadAnimation(New_MSQ.this,
                                            R.anim.fade_in);
                            para_image.startAnimation(animation_fade);


                            Log.e("w n h", bmp.getWidth() + "," + bmp.getHeight());
                            //para_image.setImageBitmap(decodeFile(image_file,300,400));
                            para_image.setImageBitmap(bmp);
                         /*   try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/

                        }
                    }.start();
                } else {


                    new CountDownTimer(600, 600) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            para_image.setVisibility(View.GONE);


                            paragraph.setVisibility(View.VISIBLE);
                            paragraph.requestFocus();
                            // paragraph.scrollTo(0,para_scroller.getTop());
                            //para_scroller.requestChildFocus(paraframe,paraframe);
                            para_scroller.scrollTo(0, para_scroller.getTop());
                            para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                            para_scroller.smoothScrollTo(0, 0);

                            final Animation animation_fade =
                                    AnimationUtils.loadAnimation(New_MSQ.this,
                                            R.anim.fade_in);
                            paragraph.startAnimation(animation_fade);
                            paragraph.setText(assets.get(index));

                        }
                    }.start();
                }

            }else {
                //no leveling


                if(paragraph_count>0 || paragraph_count == para_image_count-1) {

                    if (quit_warning_showing) {


                    } else {
                        master_text.setEnabled(false);
                        arrowframe.setEnabled(false);


                        Log.e("calledchangepara", "" + paragraph_count);
                        try {

                            countDownTimer.cancel();
                            // countDownTimer=null;
                        } catch (Exception e) {

                            e.printStackTrace();
                        }


                        int i = para_count - 2;
                        String message = "";
                        String complete_msg = "";
                        String total_count = "";

                        switch (para_image_count) {

                            case 1:

                                total_count = "out of one master slave question";
                                break;

                            case 2:

                                total_count = "out of two master slave questions";
                                break;

                            case 3:

                                total_count = "out of three master slave questions";
                                break;

                            case 4:

                                total_count = "out of four master slave questions";
                                break;
                            case 5:

                                total_count = "out of five master slave questions";
                                break;
                            case 6:

                                total_count = "out of six master slave questions";
                                break;

                            case 7:

                                total_count = "out of seven master slave questions";
                                break;
                            case 8:

                                total_count = "out of eight master slave questions";
                                break;
                            case 9:

                                total_count = "out of nine master slave questions";
                                break;
                            case 10:

                                total_count = "out of ten master slave questions";
                                break;
                            case 11:

                                total_count = "out of eleven master slave questions";
                                break;
                            case 12:

                                total_count = "out of twelve master slave questions";
                                break;
                            case 13:

                                total_count = "out of thirteen master slave questions";
                                break;
                            case 14:

                                total_count = "out of fourteen master slave questions";
                                break;
                            case 15:

                                total_count = "out of fifteen master slave questions";
                                break;
                            case 16:

                                total_count = "out of sixteen master slave questions";
                                break;
                            case 17:

                                total_count = "out of seventeen master slave questions";
                                break;
                            case 18:

                                total_count = "out of eighteen master slave questions";
                                break;
                            case 19:

                                total_count = "out of nineteen master slave questions";
                                break;
                            case 20:

                                total_count = "out of twenty master slave questions";
                                break;
                        }
                        switch (paragraph_count) {

                            case 1:
                                message = "You have completed your first master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 2:
                                message = "You have completed your second master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 3:
                                message = "You have completed your third master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 4:
                                message = "You have completed your fourth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 5:
                                message = "You have completed your fifth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 6:
                                message = "You have completed your sixth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 7:
                                message = "You have completed your seventh master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 8:
                                message = "You have completed your eighth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 9:
                                message = "You have completed your ninth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 10:
                                message = "You have completed your tenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 11:
                                message = "You have completed your eleventh master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 12:
                                message = "You have completed your twelveth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 13:
                                message = "You have completed your thirteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 14:
                                message = "You have completed your fourteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 15:
                                message = "You have completed your fifteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 16:
                                message = "You have completed your sixteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 17:
                                message = "You have completed your seventeenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 18:
                                message = "You have completed your eighteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 19:
                                message = "You have completed your nineteenth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                            case 20:
                                message = "You have completed your twentieth master slave question";
                                complete_msg = message + " " + total_count;
                                showProceedDialog(complete_msg);
                                break;
                        }


//            showProceedDialog("You have completed your first audio questions");
                    }
                }
                paragraph_count++;
                master_text.setEnabled(true);
                arrowframe.setEnabled(true);


                //   ObjectAnimator.ofInt(main_scroller, "scrollY",  main_scroller.getTop()).setDuration(10).start();
                Log.e("acq", String.valueOf(acq));
                if(assets.get(acq).contains("7769896014")){
                    String [] valuesare=assets.get(acq).split("7769896014");
                    String imgis=valuesare[0];
                    String parais=valuesare[1];
                    final Animation animation_fade =
                            AnimationUtils.loadAnimation(New_MSQ.this,
                                    R.anim.fade_in);


                    String[] pic = imgis.split("/");
                    String last = pic[pic.length - 1];
                    String[] lastname = last.split("\\.");
                    String name = lastname[0];
                    final File image_file = new File(folder, pic[pic.length - 1]);
                    path_image=image_file.getPath();
                    final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());


                    //  paragraph.startAnimation(animation_fade);


                    paragraph.setText(parais);
                    new CountDownTimer(600, 600) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            para_image.setVisibility(View.VISIBLE);
                            paragraph.setVisibility(View.VISIBLE);


                            // para_scroller.requestChildFocus(paraframe,paraframe);
                            para_scroller.scrollTo(0,para_scroller.getTop());
                            para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                            para_scroller.smoothScrollTo(0,0);
                            para_image.startAnimation(animation_fade);

                            paragraph.startAnimation(animation_fade);

                            Log.e("w n h", bmp.getWidth()+","+bmp.getHeight());
                            //  para_image.setImageBitmap(decodeFile(image_file,300,400));

                            para_image.setImageBitmap(bmp);
                      /*      try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
*/

                        }
                    }.start();
                }else
                if ((assets.get(acq).contains(".jpg")) || (assets.get(acq).contains(".jpeg")) || (assets.get(acq).contains(".png"))) {
                    paragraph.setVisibility(View.GONE);



                    String[] pic = assets.get(acq).split("/");
                    String last = pic[pic.length - 1];
                    String[] lastname = last.split("\\.");
                    String name = lastname[0];
                    final File image_file = new File(folder, pic[pic.length - 1]);
                    path_image=image_file.getPath();
                    final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());

                    new CountDownTimer(600, 600) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            para_image.setVisibility(View.VISIBLE);
                            // para_scroller.requestChildFocus(paraframe,paraframe);
                            para_scroller.scrollTo(0,para_scroller.getTop());
                            para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                            para_scroller.smoothScrollTo(0,0);
                            final Animation animation_fade =
                                    AnimationUtils.loadAnimation(New_MSQ.this,
                                            R.anim.fade_in);
                            para_image.startAnimation(animation_fade);


                            Log.e("w n h", bmp.getWidth()+","+bmp.getHeight());
                            //para_image.setImageBitmap(decodeFile(image_file,300,400));
                            para_image.setImageBitmap(bmp);
                         /*   try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/

                        }
                    }.start();
                } else {


                    new CountDownTimer(600, 600) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {

                            para_image.setVisibility(View.GONE);


                            paragraph.setVisibility(View.VISIBLE);
                            paragraph.requestFocus();
                            // paragraph.scrollTo(0,para_scroller.getTop());
                            //para_scroller.requestChildFocus(paraframe,paraframe);
                            para_scroller.scrollTo(0,para_scroller.getTop());
                            para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                            para_scroller.smoothScrollTo(0,0);

                            final Animation animation_fade =
                                    AnimationUtils.loadAnimation(New_MSQ.this,
                                            R.anim.fade_in);
                            paragraph.startAnimation(animation_fade);
                            paragraph.setText(assets.get(acq));

                        }
                    }.start();
                }
            }




        }


        no_of_current_ques_per_para++;
        Log.e("no_of_current_quespara",""+no_of_current_ques_per_para);
    }

    private void showProceedDialog(String msg ) {

        msg="Next Question";

        if(frameimageim.getVisibility()==View.VISIBLE){
            frameimageim.setVisibility(View.GONE);
        }
        if(alertDialog!=null && alertDialog.isShowing()){

         try{
             alertDialog.dismiss();
         }
         catch (Exception e){
             e.printStackTrace();
         }
        }
        else if(alert_on_close!=null && alert_on_close.isShowing()){
            try{
                alert_on_close.dismiss();
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }

            //countDownTimer.cancel();
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();

        try {
                proceed_dialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
            proceed_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            proceed_dialog.setContentView(R.layout.congo_msq);
                Window window = proceed_dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.CENTER;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
            proceed_dialog.setCancelable(false);

            proceed_dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);




                final FontTextView yes = (FontTextView) proceed_dialog.findViewById(R.id.yes_quit);

                FontTextView msgs = (FontTextView)proceed_dialog.findViewById(R.id.txt);
                msgs.setText(msg);

            final LinearLayout banner=(LinearLayout) proceed_dialog.findViewById(R.id.banner_level_page);

            banner.setVisibility(View.GONE);

                    Animation scaleAnimation = new ScaleAnimation(
                            0f, 1f, // Start and end values for the X axis scaling
                            0f, 1f, // Start and end values for the Y axis scaling
                            Animation.REVERSE, 0.5f, // Pivot point of X scaling
                            Animation.REVERSE, 0.5f);
                    scaleAnimation.setDuration(500);


                    banner.startAnimation(scaleAnimation);
            banner.setVisibility(View.VISIBLE);
                    new CountDownTimer(500, 100) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                            Log.e("mystatus","bannervisible");
                        }

                        @Override
                        public void onFinish() {

                        }
                    }.start();




                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        yes.setEnabled(false);
                        Animation scaleAnimation = new ScaleAnimation(
                                1f, 0f, // Start and end values for the X axis scaling
                                1f, 0f, // Start and end values for the Y axis scaling
                                Animation.REVERSE, 0.5f, // Pivot point of X scaling
                                Animation.REVERSE, 0.5f);
                        scaleAnimation.setDuration(500);


                        banner.startAnimation(scaleAnimation);
                        new CountDownTimer(400,400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                close.setEnabled(true);
                                proceed_dialog_showing = false;
                                proceed_dialog.dismiss();

                              //  countDownTimer.cancel();

                             /*   startTime= Long.parseLong(qsmodel.getTime());
                                startTime=startTime*1000;
                                Log.e("timeris",qsmodel.getTime());
                                countDownTimer = new MyCountDownTimer(startTime, interval);
                                startTimer();*/



                            }
                        }.start();


                    }
                });

                try {
                    proceed_dialog.show();
                    proceed_dialog_showing = true;

                }catch (Exception e){}
            }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void changeQues() {


       // timerrr.setVisibility(View.VISIBLE);
        if(islevelingenable) {
            c = Calendar.getInstance();
            df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            now = df1.format(c.getTime()).toString();

           /* try {
                blinking_anim.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            if (qc < q_count) {
                Log.e("no_of_current_ques", "" + no_of_current_ques_per_para);
                Log.e("qc size", String.valueOf(qc));
                Log.e("qs size", String.valueOf(q_count));

//            if(qc==2){
//                takePicture();
//            }
                if (audio.size() > 1) {
                    if (no_of_ques_in_list == audio.get(cnt)) {
                        no_of_current_ques_per_para = 0;
                        changeParagraph(cnt + 1);
                        Log.e("www","paracalledc");
                        cnt++;
                        no_of_ques_in_list = 0;

                        Log.e("cnt", "" + cnt);
                        Log.e("no_of_current_ques.", "" + no_of_current_ques_per_para);
                        slidingLayer.openLayer(true);
                        a1.setImageResource(R.drawable.msq_left_arrow);

                        master_text.setEnabled(false);
                        arrowframe.setEnabled(false);

                        new CountDownTimer(1000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                master_text.setEnabled(true);
                                arrowframe.setEnabled(true);
                            }
                        }.start();

                    /*a1.setImageResource(R.drawable.arrowup);
                    a2.setImageResource(R.drawable.arrowup);*/
                        //  slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                        direction = "bottom";
                    }
                    //no_of_ques_in_list++;
                }


          /*  for(int i=0;i<qid_array.size();i++){
                if(qsmodel.getQ_id().equalsIgnoreCase(qid_array.get(i))){
                    flag_is_present_id=true;
                }
            }*/

                int qt_temp=asset_id.get(index);
                boolean n=false;
                for(int i=0;i<qs.size();i++){
                    QuestionModel qsm=qs.get(i);
                    if(Integer.parseInt(qsm.getQT_id())==qt_temp){
                        qc_index=i;
                        n=true;
                        break;
                    }
                    if(n){
                        break;
                    }
                }


                qsmodel = qs.get(qc_index);

                if (!flag_is_present_id) {

                    if (qc == 0) {
                        //  qno.setText(qc + 1 + "/" + qs.size());
                        timerrr.setTextColor(Color.parseColor("#ffffff"));
                        timer_text.setTextColor(Color.parseColor("#ffffff"));

                        timer_img.setBackgroundResource(R.drawable.msq_timer_img);
                        try {
                            qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    Log.e("valuesis", String.valueOf(Float.parseFloat(String.valueOf(qc + 1))));
                    try {
                        //single question
                        if (flag_click) {
//                        final Animation animation_fade = AnimationUtils.loadAnimation(MSQ.this, R.blinking_anim.fade_in);
//
//                         rc_options_non_clickable.startAnimation(animation_fade);
//
                            //animListdata();
//                        main_ques_layout_window.startAnimation(ques_anim_left);
//                        Log.e("animstatus", "putdata");
//                        temp_ques_layout_window.startAnimation(ques_anim);
//                        hideArrow();
//                        temp_ques_layout_window.setVisibility(View.VISIBLE);
                        }
                        newquid = qsmodel.getQ_id();
                        optioncard = new ArrayList<>();
                        optioncard.add("Q: " + qsmodel.getQuestion());
                        if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                            String s1 = qsmodel.getOpt1();
                            s1 = s1.trim();
                            optioncard.add("" + s1);
                        }
                        if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                            String s2 = qsmodel.getOpt2();
                            s2 = s2.trim();
                            optioncard.add("" + s2);
                        }
                        if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                            String s3 = qsmodel.getOpt3();
                            s3 = s3.trim();
                            optioncard.add("" + s3);
                        }
                        if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                            String s4 = qsmodel.getOpt4();
                            s4 = s4.trim();
                            optioncard.add("" + s4);
                        }
                        if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                            String s5 = qsmodel.getOpt5();
                            s5 = s5.trim();
                            optioncard.add("" + s5);
                        }

                        crcans = qsmodel.getCorrectanswer();

                        switch (crcans) {
                            case "A":
                                crcopt = 0;
                                break;
                            case "B":
                                crcopt = 1;
                                break;
                            case "C":
                                crcopt = 2;
                                break;
                            case "D":
                                crcopt = 3;
                                break;
                            case "E":
                                crcopt = 4;
                                break;
                            default:
                                System.out.println("Not in 10, 20 or 30");
                        }
                        crct = crcopt;

                        qid = Integer.parseInt(qsmodel.getQ_id());


                        option_clickable = new Option_clickable(this, optioncard);
                        //rc_options_non_clickable_temp.setAdapter(option_clickable);
                        new CountDownTimer(700, 700) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {

                                rc_options_non_clickable.setAdapter(option_clickable);

//                            recyclerFastScroller.attachRecyclerView(rc_options_non_clickable);
//                            recyclerFastScroller.attachAdapter(option_clickable);


                                final Animation animation_fade = AnimationUtils.loadAnimation(New_MSQ.this, R.anim.fade_in);

                                rc_options_non_clickable.startAnimation(animation_fade);
                                if(change_para_called){

                                }else {
                                    startTime = Long.parseLong(qsmodel.getTime());
                                    startTime = startTime * 1000;
                                    Log.e("timeris", qsmodel.getTime());
                                    countDownTimer = new MyCountDownTimer(startTime, interval);
                                    startTimer();
                                }
                            }
                        }.start();
                        Log.e("timimg", "adapter load");

                        String[] options_array = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"};
                        int no_options = optioncard.size();
                        String[] confirm_options = new String[no_options - 1];

                        for (int i = 0; i < no_options - 1; i++) {
                            confirm_options[i] = options_array[i];
                        }
                        //   rc_options.setAdapter(new NewOptionsAdapter(this,confirm_options));
                        if (flag_click) {
                            new CountDownTimer(700, 700) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    //  temp_ques_layout_window.setVisibility(View.VISIBLE);

                                }

                                @Override
                                public void onFinish() {
                                    //   rc_options_non_clickable.removeOnItemTouchListener(disabler);
                                    if (qc != 0) {
                                        //   qno.setText(qc + 1 + "/" + qs.size());
                                        timerrr.setTextColor(Color.parseColor("#ffffff"));
                                        timer_text.setTextColor(Color.parseColor("#ffffff"));

                                        timer_img.setBackgroundResource(R.drawable.msq_timer_img);
                                        try {
                                            qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                    //  ques_scrollview.setVerticalScrollBarEnabled(true);


                                }
                            }.start();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //Collections.shuffle(qs);
                    // changeQues();
                    //shuffle
                }
                paraques++;
                no_of_ques_in_list++;
                no_of_current_ques_per_para++;
                formdatalocal(qc,0);
                paraques--;
                no_of_ques_in_list--;
                no_of_current_ques_per_para--;

            } else {

                try {
                    dialoge.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                } else {
                    tres = formdata();
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                }
            }

        }else {
            //no leveling
            c = Calendar.getInstance();
            df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            now = df1.format(c.getTime()).toString();

           /* try{
                blinking_anim.cancel();
            }
            catch (Exception e){
                e.printStackTrace();
            }*/


            int cis=0;
            if(isques_limit_exceed){
                cis=int_qs_size;
            }else {
                cis=q_count;
            }

            if (qc <cis) {
                Log.e("no_of_current_ques",""+no_of_current_ques_per_para);
                Log.e("qc size",String.valueOf(qc));
                qsmodel=qs.get(qc);

//            if(qc==2){
//                takePicture();
//            }
                if (audio.size() > 1) {
                    if (no_of_ques_in_list == audio.get(cnt)) {
                        no_of_current_ques_per_para = 0;
                        changeParagraph(cnt + 1);
                        Log.e("www","paracalledca");
                        cnt++;
                        no_of_ques_in_list = 0;

                        Log.e("cnt",""+cnt);
                        Log.e("no_of_current_ques.",""+no_of_current_ques_per_para);
                        slidingLayer.openLayer(true);
                        a1.setImageResource(R.drawable.msq_left_arrow);

                        master_text.setEnabled(false);
                        arrowframe.setEnabled(false);

                        new CountDownTimer(1000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                master_text.setEnabled(true);
                                arrowframe.setEnabled(true);
                            }
                        }.start();

                    /*a1.setImageResource(R.drawable.arrowup);
                    a2.setImageResource(R.drawable.arrowup);*/
                        //  slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                        direction="bottom";
                    }
                    //no_of_ques_in_list++;
                }


          /*  for(int i=0;i<qid_array.size();i++){
                if(qsmodel.getQ_id().equalsIgnoreCase(qid_array.get(i))){
                    flag_is_present_id=true;
                }
            }*/


                if(!flag_is_present_id) {

                    if(qc==0) {
                        //  qno.setText(qc + 1 + "/" + qs.size());
                        timerrr.setTextColor(Color.parseColor("#ffffff"));
                        timer_text.setTextColor(Color.parseColor("#ffffff"));

                        timer_img.setBackgroundResource(R.drawable.msq_timer_img);
                        try {
                            qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    Log.e("valuesis",String.valueOf(Float.parseFloat(String.valueOf(qc+1))));
                    try {
                        //single question
                        if(flag_click) {
//                        final Animation animation_fade = AnimationUtils.loadAnimation(MSQ.this, R.blinking_anim.fade_in);
//
//                         rc_options_non_clickable.startAnimation(animation_fade);
//
                            //animListdata();
//                        main_ques_layout_window.startAnimation(ques_anim_left);
//                        Log.e("animstatus", "putdata");
//                        temp_ques_layout_window.startAnimation(ques_anim);
//                        hideArrow();
//                        temp_ques_layout_window.setVisibility(View.VISIBLE);
                        }
                        newquid = qsmodel.getQ_id();
                        optioncard = new ArrayList<>();
                        optioncard.add("Q: "+qsmodel.getQuestion());
                        if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                            String s1 = qsmodel.getOpt1();
                            s1 = s1.trim();
                            optioncard.add(""+s1);
                        }
                        if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                            String s2 = qsmodel.getOpt2();
                            s2 = s2.trim();
                            optioncard.add(""+s2);
                        }
                        if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                            String s3 = qsmodel.getOpt3();
                            s3 = s3.trim();
                            optioncard.add(""+s3);
                        }
                        if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                            String s4 = qsmodel.getOpt4();
                            s4 = s4.trim();
                            optioncard.add(""+s4);
                        }
                        if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                            String s5 = qsmodel.getOpt5();
                            s5 = s5.trim();
                            optioncard.add(""+s5);
                        }

                        crcans = qsmodel.getCorrectanswer();

                        switch (crcans) {
                            case "A":
                                crcopt = 0;
                                break;
                            case "B":
                                crcopt = 1;
                                break;
                            case "C":
                                crcopt = 2;
                                break;
                            case "D":
                                crcopt = 3;
                                break;
                            case "E":
                                crcopt = 4;
                                break;
                            default:
                                System.out.println("Not in 10, 20 or 30");
                        }
                        crct = crcopt;

                        qid = Integer.parseInt(qsmodel.getQ_id());


                        option_clickable =new Option_clickable(this,optioncard);
                        //rc_options_non_clickable_temp.setAdapter(option_clickable);
                        new CountDownTimer(700, 700) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {

                                rc_options_non_clickable.setAdapter(option_clickable);

//                            recyclerFastScroller.attachRecyclerView(rc_options_non_clickable);
//                            recyclerFastScroller.attachAdapter(option_clickable);


                                final Animation animation_fade = AnimationUtils.loadAnimation(New_MSQ.this, R.anim.fade_in);

                                rc_options_non_clickable.startAnimation(animation_fade);
                                if(change_para_called){

                                }else {
                                    startTime = Long.parseLong(qsmodel.getTime());
                                    startTime = startTime * 1000;
                                    Log.e("timeris", qsmodel.getTime());
                                    countDownTimer = new MyCountDownTimer(startTime, interval);
                                    startTimer();
                                }
                            }
                        }.start();
                        Log.e("timimg","adapter load");

                        String[] options_array={"A","B","C","D","E","F","G","H","I","J","K","L","M"};
                        int no_options=optioncard.size();
                        String [] confirm_options=new String[no_options-1];

                        for(int i=0;i<no_options-1;i++){
                            confirm_options[i]=options_array[i];
                        }
                        //   rc_options.setAdapter(new NewOptionsAdapter(this,confirm_options));
                        if(flag_click) {
                            new CountDownTimer(700, 700) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    //  temp_ques_layout_window.setVisibility(View.VISIBLE);

                                }

                                @Override
                                public void onFinish() {
                                    //   rc_options_non_clickable.removeOnItemTouchListener(disabler);
                                    if(qc!=0)
                                    {
                                        //   qno.setText(qc + 1 + "/" + qs.size());
                                        timerrr.setTextColor(Color.parseColor("#ffffff"));
                                        timer_text.setTextColor(Color.parseColor("#ffffff"));

                                        timer_img.setBackgroundResource(R.drawable.msq_timer_img);
                                        try {
                                            qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                    }
                                    //  ques_scrollview.setVerticalScrollBarEnabled(true);


                                }
                            }.start();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    //Collections.shuffle(qs);
                    // changeQues();
                    //shuffle
                }





                paraques++;
                no_of_ques_in_list++;
                no_of_current_ques_per_para++;
                formdatalocal(qc,0);
                paraques--;
                no_of_ques_in_list--;
                no_of_current_ques_per_para--;

            } else {

                try{
                    dialoge.dismiss();
                }catch (Exception e){
                    e.printStackTrace();
                }

                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                } else {
                    tres = formdata();
                    checkonp = false;
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    scoreActivityandPostscore();
                }
            }

        }







    }

    public String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qus = sb.toString();


            Log.e("JSONStrr", Qus);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qus;
    }
    public String formdata() {
        String jn = "";
        try{

            JSONObject student1 = new JSONObject();
            student1.put("U_Id", uidd);
            student1.put("S_Id", sid);
            student1.put("Q_Id", newquid);
            student1.put("TG_Id", tgid);
            student1.put("SD_UserAnswer", "E");
            student1.put("SD_StartTime", Calendar.getInstance().getTime().toString());
            student1.put("SD_EndTime", Calendar.getInstance().getTime().toString());


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(student1);


            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn = studentsObj.toString();
        } catch(Exception e)

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jn;
    }

    public String formdata(int anan){
        String jn = "";

        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < anan; i++) {
                JSONObject student = new JSONObject();
                student.put("U_Id", uidd);
                student.put("S_Id", sid);
                student.put("Q_Id", qidj.get(i));
                student.put("QT_Id",qtid.get(i));
                student.put("TG_Id", tgid);
                student.put("SD_UserAnswer", ansj.get(i));
                student.put("SD_StartTime", stj.get(i));
                student.put("SD_EndTime", endj.get(i));
                jsonArray.put(student);
            }

            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn=studentsObj.toString();
            Log.e("jsonpostSTring",jn);
        }catch (Exception e){
            e.printStackTrace();
        }

        return jn;

    }

    //detection
    private void scoreActivityandPostscore() {
               stop();
        ConnectionUtils connectionUtils=new ConnectionUtils(New_MSQ.this);
        if(connectionUtils.isConnectionAvailable()) {



            if (piccapturecount != 0) {
                try {
                    if (!verification_progress.isShowing() || verification_progress != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                verification_progress.show();
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                rc_options_non_clickable.setVisibility(View.GONE);
                dr_new.setVisibility(View.GONE);
                timerrr.setVisibility(View.GONE);
                timer_img.setVisibility(View.GONE);
                timer_text.setVisibility(View.GONE);
                verify();
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        verification_progress.show();
                    }
                });
                rc_options_non_clickable.setVisibility(View.GONE);
                dr_new.setVisibility(View.GONE);
                timerrr.setVisibility(View.GONE);
                timer_img.setVisibility(View.GONE);
                timer_text.setVisibility(View.GONE);
                // new ImageSend().execute();
                 new PostAns().execute();



//                Intent it = new Intent(this, CheckP.class);
//                it.putExtra("qt", questions.length() + "");
//                it.putExtra("cq", cqa + "");
//                it.putExtra("po", score + "");
//                it.putExtra("assessg",assessg);
//                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(it);
//                finish();

            }
//            if(i+1==piccapturecount) {
//                new ImageSend().execute();
//            }



        }else {
            showretryDialogue();
        }


    }
    public void showretryDialogue() {
        Log.e("respmsg","dialogue");

        /*final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                MSQ.this, R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(MSQ.this);
        View dialogView = inflater.inflate(R.layout.retry_anim_dialogue, null);

        dialogBuilder.setView(dialogView);


        alertDialog = dialogBuilder.create();


//                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

        alertDialog.show();*/
        alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);
        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils=new ConnectionUtils(New_MSQ.this);
                        if(connectionUtils.isConnectionAvailable()){
                            scoreActivityandPostscore();
                        }else {assess_progress.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            if(!alertDialog.isShowing())
                alertDialog.show();
        }
        catch (Exception e){

        }
    }

    private class PostAns extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {

                Log.e("datais","ip: "+AppConstant.getLocalIpAddressApp()+" os: "+AppConstant.getAndroidVersionApp()
                        +" version: "+AppConstant.getversionofApp());

                try {

                    getPostData();
                    try {
                        Thread.currentThread();
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/v1/Answers");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            postvalues.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestProperty("app_version",AppConstant.getversionofApp());
                    urlConnection.setRequestProperty("app_os",AppConstant.getAndroidVersionApp());
                    urlConnection.setRequestProperty("app_ip",AppConstant.getLocalIpAddressApp());
                    urlConnection.setRequestProperty("app_latlong",location.getLatitude()+","+location.getLongitude());

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(postvalues);
                    Log.e("resp_data",postvalues);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code2 = urlConnection.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {


                new Ends().execute();
            }
            else{

                try{
                    verification_progress.dismiss();
                    disable_option();
                }catch (Exception e){
                    e.printStackTrace();
                }

                Intent it = new Intent(New_MSQ.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        }
    }


    private class Ends extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            if(new PrefManager(New_MSQ.this).getGetFirsttimegame().equalsIgnoreCase("first")){
                new PrefManager(New_MSQ.this).saveFirsttimeGame("last");
            }else if(new PrefManager(New_MSQ.this).getGetFirsttimegame().equalsIgnoreCase("last")){

            }else {
                new PrefManager(New_MSQ.this).saveFirsttimeGame("first");
            }


        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("S_Id", sid)
                            .object("course_id", Integer.valueOf(new PrefManager(New_MSQ.this).getcourseid_ass()))
                            .object("lg_id",new PrefManager(New_MSQ.this).getlgid_ass())
                            .object("certification_id",new PrefManager(New_MSQ.this).getcertiid_ass())
                            .build().toString();


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/EndSession");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " +token);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                    Log.v("Response2", ""+code3);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if(code3==200){
                    String responseString = readStream1(is);
                    Log.e("Response2", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                verification_progress.dismiss();
                disable_option();
                Log.e("result", ""+result);

            }catch (Exception e){
                e.printStackTrace();
            }

            if(code3!=200){

                Toast.makeText(getApplicationContext(),"Error while submitting answers", Toast.LENGTH_SHORT).show();
                Intent it = new Intent(New_MSQ.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
            else{

                try {
                    get_quesdata = new File(getFilesDir() + "" + cate + game + gdid + ceid +uidd + tgid + ".json");
                    get_ansdata = new File(getFilesDir() + "Answer" + cate + game + gdid + ceid + uidd + tgid +".json");
                    get_quesdata.delete();
                    get_ansdata.delete();
                }
                catch (Exception e){
                    e.printStackTrace();
                }


                Intent it = new Intent(New_MSQ.this, CheckP.class);

                int questionsc=0;
                if(isques_limit_exceed){
                    questionsc=int_qs_size;
                }else {
                    questionsc=q_count;
                }

                it.putExtra("qt", questionsc+ "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.putExtra("assessg",assessg);
                //   it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();

            }

        }
    }
    public String readStream1(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qs = sb.toString();


            Log.e("JSONStrr", Qs);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qs;
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);*/
        if (pauseCount == 0 && checkonp) {
            try {
                if (alertDialog != null && alertDialog.isShowing()) {
                   // alertDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (dialoge != null && dialoge.isShowing()) {
                 //   dialoge.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  vdo_performance_ques_timer.cancel();
      //      close.setEnabled(false);


            try {
                if (!close_on_get_ques && !proceed_dialog.isShowing() && !proceed_dialog_showing && !alert_on_close.isShowing() && !alert_onclose_showing) {

              /*      AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MSQ.this, R.style.DialogTheme);
// ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = LayoutInflater.from(MSQ.this);
                    View dialogView = inflater.inflate(R.layout.quit_warning, null);
                    dialogBuilder.setView(dialogView);


                    alertDialog = dialogBuilder.create();

                    alertDialog.setCancelable(false);
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

                    alertDialog.show();
*/
                    alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.assess_quit_warning);
                    Window window = alertDialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alertDialog.setCancelable(false);

                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                    if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);

                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();


                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);

                                    alertDialog.dismiss();
                                //    changeQues();
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            close.setEnabled(true);
                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();
                                        quit_warning_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();

                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();
                        }
                    });

                    try {
                     //   quit_warning_showing = true;
                     //   alertDialog.show();

                    } catch (Exception e) {

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                if (!close_on_get_ques && !alert_onclose_showing) {

        /*        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MSQ.this, R.style.DialogTheme);
// ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = LayoutInflater.from(MSQ.this);
                View dialogView = inflater.inflate(R.layout.quit_warning, null);
                dialogBuilder.setView(dialogView);


                alertDialog = dialogBuilder.create();

                alertDialog.setCancelable(false);
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);


                try {
                    alertDialog.show();
                }catch (Exception ex){
                    ex.printStackTrace();
                }*/

                    alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.assess_quit_warning);
                    Window window = alertDialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alertDialog.setCancelable(false);

                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                    if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);

                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();


                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);

                                    alertDialog.dismiss();
                              //      changeQues();
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            close.setEnabled(true);
                            try {
                                countDownTimer.cancel();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();
                                        quit_warning_showing = false;
                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();

                                        try {
                                            countDownTimer.cancel();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();
                        }
                    });
                    try {
                     //   quit_warning_showing = true;
                      //  alertDialog.show();
                    } catch (Exception ex) {
                    }
                }
            }
            } else{
                if (checkonp) {
                    Log.e("PAUSE", checkonp + "");
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();
                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();
                    }
                } else {

                }
            }

    }
    private final BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int position= Integer.parseInt(intent.getStringExtra("pos"));


            if(position==1000){
//                temp_ques_layout_window.setVisibility(View.GONE);
            }else {
                Log.e("broad", "msgget" +position);
                clickMethods(position);
                pos_clicked=position;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        try {
            flag_click=false;
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mReceiver);
//        try{
//            timer1.cancel();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        try {

            if(dialog.isShowing()){
                dialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {

            if(dialoge.isShowing()){
                dialoge.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {

            if(quesLoading.isShowing()&& quesLoading !=null){
                quesLoading.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void downloadData(String uri, String substring) {

        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {

            dm.startDownload(task);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onBackPressed() {

    }
    private  class S3getimageurl extends AsyncTask<Void,Void,Void> {
        URL image1url, image2url,image3url;
        @Override
        protected Void doInBackground(Void... params) {
            String ACCESS_KEY ="AKIAJQTIZUYTX2OYTBJQ";
            String SECRET_KEY = "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";

            try {
                Random ramd = new Random();

                AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY));

                Log.e("outsidecond", String.valueOf(count_detect_no));

                if (autocapimgloc.get(auto_capt_count) != null) {
                    autocapimg1loc1 = autocapimgloc.get(auto_capt_count);
                    int innd = ramd.nextInt(100000);
                    File pic1 = new File(autocapimg1loc1);
                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg", pic1);
                    PutObjectResult putResponse = s3Client.putObject(pp);
                    String prurl = s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                    image1url = s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                    img1 = image1url.toString();
                    Log.e("AMAZON RESPONSE", img1);
                    replacedurl = img1.replace("https://valyouinputbucket.s3.amazonaws.com/", "https://valyouinputbucket.s3.ap-south-1.amazonaws.com/");

                    auto_capt_count++;
                } else {
                    img1 = "";
                    auto_capt_count++;
                }
                imgurllist.add(replacedurl);
                Log.e("detectis in s3", String.valueOf(count_detect_no));
                count_detect_no++;

                verify();
            }
            catch (Exception e){
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (verification_progress.isShowing() || verification_progress != null) {

                                verification_progress.dismiss();


                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                showretrys3urlDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            showretrys3urlDialogue();
                        }
                    }
                });
            }
            return  null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  new SExample(ppp.toString()).execute();
            //GameModel gameModel = new GameModel();
            //String params="U_Id="+candidateId+"&g_id="+12+"&image_url="+image1url+","+image2url+","+image3url+"&fr_status="+0+"&assesment_name="+ "FIB"+"&assesment_id="+2+"&company_name="+"TATACOMMUNICATION";



            // Log.e("params" , params);
            // new ImageSend().execute();

        }
    }
    public class ImageSend extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";
            //   String lg_id = " ", cert_id=" ", course_id = " ", tg_group_id=" ";
            String responseString="";

            /*try{
                lg_id = new PrefManager(New_MSQ.this).getlgid();

                course_id = new PrefManager(New_MSQ.this).getCourseid();

                cert_id =  new PrefManager(New_MSQ.this).getcertiid();
                tg_group_id = new ToadysgameModel().getAss_groupid();

            }

            catch (Exception e){
                e.printStackTrace();
            }*/

            try {
                Log.e("ids in imagesend", ""+lg_id+" ,"+course_id+" ,"+certifcation_id+" ,"+tg_groupid);

                JSONObject child = new JSONObject();

                if(get_nav.equalsIgnoreCase("learn"))
                {
                    child.put("U_Id", candidateId);
                    child.put("GD_Id", gdid);
                    child.put("fr_status", "0");
                    child.put("assesment_name", game);
                    child.put("CE_Id", ceid);
                    child.put("company_name", HomeActivity.tg_group);
                    child.put("TG_GroupId", tg_groupid);
                    child.put("lg_id", lg_id);
                    child.put("course_id", course_id);
                    child.put("certification_id",certifcation_id);
                }
                else {

                    child.put("U_Id", candidateId);
                    child.put("GD_Id", gdid);
                    child.put("fr_status", "0");
                    child.put("assesment_name", game);
                    child.put("CE_Id", ceid);
                    child.put("company_name", HomeActivity.tg_group);
                    child.put("TG_GroupId", tg_groupid);
                    child.put("lg_id", lg_id);
                    child.put("course_id", course_id);
                    child.put("certification_id",certifcation_id);
                }
                JSONArray jsonArray = new JSONArray();
                for(int i=0;i<imgurllist.size();i++){
                    jsonArray.put(imgurllist.get(i));
                }
                JSONObject mainobj=new JSONObject();
                child.put("image_url",jsonArray);
                mainobj.put("data",child);


                Log.e("datais",mainobj.toString());
                URL urlToRequest = new URL(AppConstant.Ip_url + "Player/UploadFrImages");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();


                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                urlConnection.setRequestMethod("POST");




                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(mainobj.toString());
                wr.flush();

                responseString = readStream1(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result=responseString;



            }
            catch (JSONException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            verification_progress.dismiss();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                Log.e("JSONException"," showretryimagesendDialogue");

                                showretryimagesendDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("JSONException"," showretryimagesendDialogue");

                            showretryimagesendDialogue();
                        }
                    }
                });
            }catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            verification_progress.dismiss();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                Log.e("Exception"," showretryimagesendDialogue");

                                showretryimagesendDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("Exception"," showretryimagesendDialogue");

                            showretryimagesendDialogue();
                        }
                    }
                });
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            Log.e("datais","post");
            new PostAns().execute();
//            Intent it = new Intent(MasterSlaveGame.this, CheckP.class);
//            it.putExtra("qt", questions.length() + "");
//            it.putExtra("cq", cqa + "");
//            it.putExtra("po", score + "");
//            it.putExtra("assessg",assessg);
//            it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(it);
//            finish();



        }

        private String readStream1(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
    private void startVerification() {
        camera = getCameraInstance();

        waitTimer = new CountDownTimer(60000, 3) {


            @SuppressLint("SimpleDateFormat")
            @Override
            public void onTick(long l) {


            }

            public void onFinish() {

                stop();

            }



        }.start();

    }
    private Camera getCameraInstance() {
        Camera camera = null;
        if (!getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //   Toast.makeText(this, "No camera on this device", Toast.LENGTH_LONG)
            //    .show();
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId < 0) {
                //  Toast.makeText(this, "No front facing camera found.",
                //       Toast.LENGTH_LONG).show();
            } else {

                try {
                    camera = Camera.open(cameraId);
                    mPreview = new CameraPreview(this, camera);
                    preview.addView(mPreview);
                    onClick();

                } catch (Exception e) {
                    // cannot get camera or does not exist
                }
            }
        }

        return camera;
    }
    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d("status", "Camera found");
                cameraId = i;
                break;
            }else {
                Log.e("Status","No front camera found");
            }
        }
        return cameraId;
    }
    public void onClick() {


        timer1 = new Timer();
        timer1.schedule(new TimerTask()
        {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {


                        if(picture_count<pic_frequency) {
                            try{
                            camera.setDisplayOrientation(90);
                            camera.startPreview();


                            camera.takePicture(null, null, new PhotoHandler(getApplicationContext()));
                            picture_count++;
                            Log.e("pic count", ""+picture_count);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        }

                        else{
                            stop();
                        }
                    }
                });
            }
        }, 3000, 9000);


    }
    public void stop(){
        preview.setVisibility(View.GONE);

//        waitTimer.cancel();


        try {
            timer1.cancel();
        }catch (Exception e){

            e.printStackTrace();
        }

    }
    public static Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    private void detectAndFrameother1(Bitmap imageBitmap) {


        /*if(imageBitmap.getWidth()>imageBitmap.getHeight()){
            imageBitmap=rotateBitmap(imageBitmap,-90);
        }*/
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        try {
                            // if (result == null)
                            //   Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                            //if face is not detected in image
                            if ((result != null && result.length == 0)||(result==null)) {                                //   Toast.makeText(getApplicationContext(),"other face is not detected",Toast.LENGTH_SHORT).show();
                                // detectedfaceid.add(UUID.fromString(""));
                                facedetectedimagelocation.add(count_detect_no, "");
                                Log.e("outside", String.valueOf(count_detect_no) + "nondetect");
                                count_detect_no++;
                                verify();
                            }

                            //if face is detected in image
                            else {
                                //     Toast.makeText(getApplicationContext(), "other face is  detected", Toast.LENGTH_SHORT).show();

                                Log.e("outside", String.valueOf(count_detect_no) + "detect");
                                //imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                                //  Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                                // imageBitmap.recycle();


                                //imagelocation.add(mImageFileLocation);


                                //    Log.e("imglocation", mImageFileLocation);
//                            switch (piccapturecount){
//
//                                case 1:
//                                    verifiedimagelocation.add(imagelocation.get(0));
//                                    break;
//                                case 2:
//                                    verifiedimagelocation.add(imagelocation.get(1));
//                                    break;
//                                case 3:
//                                    verifiedimagelocation.add(imagelocation.get(2));
//                                    break;
//
//                            }

                                //adding detected image location in an array to get its url from s3
                                facedetectedimagelocation.add(imagelocation.get(count_detect_no));

                                List<Face> faces;

                                UUID mFaceIdother;
                                assert result != null;
                                faces = Arrays.asList(result);
                                for (Face face : faces) {
                                    mFaceIdother = face.faceId;
                                    String faceid = mFaceIdother.toString();
                                    // txt.setText(faceid);


                                    //adding detected face uuid in an array

                                    detectedfaceid.add(image_face_count, mFaceIdother);


                                    UUID[] uuids = new UUID[]{mFaceIdother};

                                    //    Log.e("mfaceid", String.valueOf(mFaceIdother));

                                    mFaceIdother = UUID.fromString(faceid);
                                    //      new VerificationTask(mFaceId1, mFaceIdother).execute();
                                    //  Log.e("face id size", String.valueOf(detectedfaceid.size()));
//                                if(detectedfaceid.size()==3){
//

                                    //
                                /*String mfaceid = prefManager.getprofilefaceid();

                                UUID mfaceid1 = UUID.fromString(mfaceid);*/
                                    new VerificationTask(mfaceid1, detectedfaceid.get(image_face_count)).execute();

                                    Log.e("mfaceid1 and two", "" + mFaceId1 + "," + detectedfaceid.get(image_face_count));
//

//
//                                }


                                }


                            }
                        }catch (Exception e){
                            ConnectionUtils connectionUtils = new ConnectionUtils(New_MSQ.this);
                            if(connectionUtils.isConnectionAvailable()){
                                if(result==null){
                                    facedetectedimagelocation.add(count_detect_no, "");
                                    Log.e("outside", String.valueOf(count_detect_no) + "nondetect");
                                    count_detect_no++;
                                    verify();
                                }
                                else{

                                }
                            }
                            else{
                                showretrydetectDialogue();
                            }
                            e.printStackTrace();


                            try {
                                if (verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.dismiss();

                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                          //  showretrydetectDialogue();
                        }


                        // i++;
//                        if(i<piccapturecount){
//                            detectAndFrameother1(autotakenpic.get(i));
//
//
//                        }
//                        else{
//                            new ImageSend().execute();
//                        }

                    }

                };

        detectTask.execute(inputStream);
    }
    private void verify() {

        stop();
        Log.e("detectis count",String.valueOf(count_detect_no));
        // detecting the captured images
        if(count_detect_no<piccapturecount) {
            detectAndFrameother1(autotakenpic.get(count_detect_no));
        }

        if(count_detect_no==piccapturecount){
            //send data to server
            for(int i=0;i<imgurllist.size();i++){
                Log.e("outsideurl",i+" "+imgurllist.get(i));
            }
            try{
                verification_progress.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
            // new PostAns().execute();

            if(imgurllist.size()==0){
                new PostAns().execute();
//                Intent it = new Intent(this, CheckP.class);
//                it.putExtra("qt", questions.length() + "");
//                it.putExtra("cq", cqa + "");
//                it.putExtra("po", score + "");
//                it.putExtra("assessg",assessg);
//                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(it);
//                finish();

            }else {
                new ImageSend().execute();
            }

        }




    }
    public class PhotoHandler implements Camera.PictureCallback {


        private final Context context;

        public PhotoHandler(Context context) {
            this.context = context;
        }

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            piccapturecount++;
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException e) {
                e.printStackTrace();
            }

            String authorities = getApplicationContext().getPackageName() + ".fileprovider";



            try {
                assert photoFile != null;
                FileOutputStream fos = new FileOutputStream(photoFile);
                fos.write(data);
                fos.flush();
                fos.close();
                fos.close();
                //   Toast.makeText(context, "New Image saved:" + photoFile,
                //       Toast.LENGTH_LONG).show();
            } catch (Exception error) {

                //   Toast.makeText(context, "Image could not be saved.",
                //         Toast.LENGTH_LONG).show();
            }

            //String filepath=  pictureFile.getPath();

            bmpother = BitmapFactory.decodeFile(mImageFileLocation);
            imageUri= FileProvider.getUriForFile(getApplicationContext(), authorities, photoFile);


            // imageView.setImageBitmap(bitmap);
            //   Bitmap b = decodeFile(pictureFile);
            // Uri mImageUri = Uri.fromFile(pictureFile);



                Matrix mMatrix = new Matrix();

                mMatrix.setRotate(-90);
                bmpother = Bitmap.createBitmap(bmpother, 0, 0, bmpother.getWidth(),
                        bmpother.getHeight(), mMatrix, false);


                // saving location of auto capture image in an array
                imagelocation.add(mImageFileLocation);

                //saving auto capture bitmap image in an array
                autotakenpic.add(bmpother);

//
//            Toast.makeText(context, "Image saved."+imagelocation.size(),
//                    Toast.LENGTH_LONG).show();

            // detectAndFrameother(bmpother);

        }


        public  File createImageFile() throws IOException {

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "IMAGE_" + timeStamp + "_";
            File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
            mImageFileLocation = image.getAbsolutePath();

            return image;

        }

    }
    private class VerificationTask extends AsyncTask<Void, String, VerifyResult> {
        // The IDs of two face to verify.
        private UUID mFaceId0;
        private UUID mFaceId1;

        VerificationTask (UUID faceId0, UUID faceId1) {
            mFaceId0 = faceId0;
            mFaceId1 = faceId1;
        }

        @Override
        protected VerifyResult doInBackground(Void... params) {
            // Get an instance of face service client to detect faces in image.
            try{
                publishProgress("Verifying...");

                // Start verification.
                return faceServiceClient.verify(
                        mFaceId0,      /* The first face ID to verify */
                        mFaceId1);     /* The second face ID to verify */
            }  catch (Exception e) {
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(VerifyResult result) {

            image_face_count++;
            Log.e("result", ""+result);
            // Show the result on screen when verification is done.
            setUiAfterVerification(result);
        }
    }
    //detection
    private void setUiAfterVerification(VerifyResult result) {
        // Verification is done, hide the progress dialog.

        // Enable all the buttons.

        // Show verification result.

        try{
        if (result != null) {
            //   Log.e("facedetection","Result");
            DecimalFormat formatter = new DecimalFormat("0.0%");
            String verificationResult = (result.isIdentical ? "The same person" : "Different persons")
                    + ". The confidence is " + formatter.format(result.confidence);
            //  Log.e("facedetection",verificationResult);


            if (verificationResult.contains("Different")) {

                //    if(i==1)

                // adding mismatch image location in an array
                autocapimgloc.add(auto_capt_count, facedetectedimagelocation.get(count_detect_no));

                Log.e("added", facedetectedimagelocation.get(count_detect_no));

                //getting image url from s3
                new S3getimageurl().execute();

                Log.e("not outside s3", String.valueOf(count_detect_no));

            } else {
                Log.e("outside s3", String.valueOf(count_detect_no));
                count_detect_no++;

                verify();

            }
        }

        }
        catch (Exception e){


            e.printStackTrace();

            try {
                if (verification_progress.isShowing() || verification_progress != null) {

                    verification_progress.dismiss();


                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
            showretryverifyDialogue();

        }
    }
    private void detectAndFrame(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            //     Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","No face detected");

                        }else {
                            //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","face detected");
                            //      startVerification();
                        }


                        // imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                        //   Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                        //imageBitmap.recycle();


                        List<Face> faces;

                        assert result != null;
                        faces = Arrays.asList(result);
                        for (Face face: faces) {
                            mFaceId1 = face.faceId;
                            faceid=mFaceId1.toString();
                            //txt.setText(faceid);
                            Log.e("fid",mFaceId1+"  "+mface);

                        }

                    }
                };
        detectTask.execute(inputStream);
    }
    private void checkfacedetection() {
        if(prefManager.getPic()==" "){
            String profileImageUrl = new DataBaseHelper(this).getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
            // new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);
            Log.e("facedetection","download pic");
        }else {
            faceServiceClient =
                    new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(New_MSQ.this).get_frkey());
            //b37d858b6b584786a74958443e6fcd2a

            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Log.e("facedetection","alreday present");
            detectAndFrame(decodedByte);
        }
    }

    public void displayImage()
    {
        close.setEnabled(false);
        close.setClickable(false);
        //lr.setVisibility(View.GONE);
        frameimageim.setVisibility(View.VISIBLE);
        ImageView imgfullscreenis=(ImageView)findViewById(R.id.imageview_fullview);
        Bitmap bmp = BitmapFactory.decodeFile(path_image);
        imgfullscreenis.setImageBitmap(bmp);
        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imgfullscreenis);
        pAttacher.update();



    }


    public void startanim()
    {
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }

    public void disable_option(){
        para_scroller.setVisibility(View.INVISIBLE);
        rc_options_non_clickable.setVisibility(View.INVISIBLE);
        timer_ques.setVisibility(View.INVISIBLE);
        qno.setVisibility(View.INVISIBLE);



    }

    public static void changeSize(){
        if(flag_sizechanged.equalsIgnoreCase("para")){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                    para_frame.getLayoutParams();
            params.weight = 64.0f;
            para_frame.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)
                    qno_layout.getLayoutParams();
            params1.weight = 6.0f;
            qno_layout.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams)
                    below_layout.getLayoutParams();
            params2.weight = 30.0f;
            below_layout.setLayoutParams(params2);
           /* LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 0.75f;

            below_layout.setLayoutParams(p);
            LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p1.weight = 1.5f;

            para_frame.setLayoutParams(p1);*/
            /*LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.06f;
            qno_layout.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.64f;
            para_frame.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.3f;
            below_layout.setLayoutParams(params2);*/
        }else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                    para_frame.getLayoutParams();
            params.weight = 30.0f;
            para_frame.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)
                    qno_layout.getLayoutParams();
            params1.weight = 6.0f;
            qno_layout.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams)
                    below_layout.getLayoutParams();
            params2.weight = 64.0f;
            below_layout.setLayoutParams(params2);
            /*
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.06f;
            qno_layout.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.64f;
            below_layout.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.3f;
            para_frame.setLayoutParams(params2);*/
        }
    }




    //store getquestion data locally new changes
    private void jsonMakeFile(String s, String responseString) {

        get_quesdata = new File( getFilesDir() +""+s+".json");
        String previousJson = " ";
        try{
            if (get_quesdata.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(get_quesdata);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        previousJson = Charset.defaultCharset().decode(bb).toString();
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                try {
                    get_quesdata.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            Log.e("prejson",previousJson);



        }catch (NullPointerException e){
            try {
                get_quesdata.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }


        if(previousJson.length()>4){
            //json file of questions is not an empty
            Log.e("prejson","not emplty file,second attempt");
            try {
                FileInputStream stream = new FileInputStream(get_quesdata);

                try {
                    FileChannel fc = stream.getChannel();
                    MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                    old_get_ques_data = Charset.defaultCharset().decode(bb).toString();
                    Log.e("prejson ", "getques old data-" + old_get_ques_data);
                } finally {
                    stream.close();
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            //need
            flaggameattempfirst=false;
        }else {
            //json file of questions is an empty
            Log.e("prejson","emplty file,first attempt");
            try {

                FileWriter file = new FileWriter(get_quesdata);
                file.write(responseString);
                file.flush();
                file.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            //need
            flaggameattempfirst=true;

        }


        //  Log.e("Jsonfile write", jarray.toString());

        // jsonRead();
    }


    private void loadofflinedata() {
        String responsestored=old_get_ques_data;
        try {
            JSONArray data = new JSONArray(responsestored);
            Log.e("QUESDATA", data.length() + "");
            // JSONArray sidd = data.getJSONArray(0);
            final JSONObject sidobj = data.getJSONObject(0);
            sid = sidobj.getString("S_Id");
            q_count=sidobj.getInt("question_count");
            currentlevel=sidobj.getInt("currentGameLevel");
            maxlevel=sidobj.getInt("max_level");
            min_level=sidobj.getInt("min_level");
            if(currentlevel<min_level){
                currentlevel=min_level;
            }
            if(currentlevel>maxlevel){
                currentlevel=min_level;
            }
            levelis=currentlevel;
            Log.e("fbwwp", sid);
            JSONArray js1 = data.getJSONArray(1);

            for(int i=0;i<js1.length();i++){
                JSONArray jsnew=js1.getJSONArray(i);
                for(int j=0;j<jsnew.length();j++){
                    JSONObject jobjis=jsnew.getJSONObject(j);
                    audios.put(jobjis);
                }

            }
            Log.e("adddddd", audios.length() + "");

            para_image_count = audios.length();

            questions = data.getJSONArray(2);
            for(int i=0;i<questions.length();i++){
                JSONObject js=questions.getJSONObject(i);
                QuestionModel s=new QuestionModel();
                s.setQT_id(js.getString("QT_Id"));
                s.setQ_id(js.getString("Q_Id"));
                s.setQuestion(js.getString("Q_Question"));
                s.setOpt1(js.optString("Q_Option1"));
                s.setOpt2(js.optString("Q_Option2"));
                s.setOpt3(js.optString("Q_Option3"));
                s.setOpt4(js.optString("Q_Option4"));
                s.setOpt5(js.optString("Q_Option5"));
                s.setCorrectanswer(js.getString("Q_Answer"));
                s.setTime(js.getString("Q_MaxTime"));
                qs.add(s);
            }
            int_qs_size=qs.size();
            if(q_count>qs.size()){
                isques_limit_exceed=true;
            }

            timerrr.setVisibility(View.VISIBLE);
            Log.e("QUES", questions.length() + "");
            tq = questions.length();

            //new changes
            setlevelingparams(cate+game+gdid+ceid+uidd+tgid);


            secLeft=startTime;
            final int MINUTES_IN_AN_HOUR = 60;
            final int SECONDS_IN_A_MINUTE = 60;

            int sec= (int) secLeft/1000;
            int minutes =  (sec / SECONDS_IN_A_MINUTE);
            sec -= minutes * SECONDS_IN_A_MINUTE;

            int hours = minutes / MINUTES_IN_AN_HOUR;
            minutes -= hours * MINUTES_IN_AN_HOUR;
            fulltime= (int) secLeft;

            new CountDownTimer(500, 500) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        startanim();
                    }
                    startTime = questions.length() * 30 * 1000;
                    changeQues();
                    // startTimer();

                    startVerification();

                    slidingLayer.openLayer(true);
                    a1.setImageResource(R.drawable.msq_left_arrow);
                    master_text.setEnabled(false);
                    arrowframe.setEnabled(false);


                    new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            master_text.setEnabled(true);
                            arrowframe.setEnabled(true);

                        }
                    }.start();
                           /* anim_frame.setVisibility(View.VISIBLE);
                            animarrow1.startAnimation(slideUpAnimation);
                            animarrow2.startAnimation(slideUpAnimation);*/

                }
            }.start();


        } catch (Exception e) {
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    qs.clear();
                    try {
                        if (quesLoading.isShowing()) {
                            quesLoading.dismiss();
                        }
                    }catch (Exception e){

                    }
                    try {
                        if ((alertDialog.isShowing())&&(alertDialog!=null)) {

                        } else {
                            showGetQuesAgainDialogue();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        showGetQuesAgainDialogue();
                    }
                }
            });
            Log.e("QUES", "Error parsing data " + e.toString());
        }



    }

    //stored answers and params in file new changes
    private void jsonMakeFileAnswerandParams(String s, String answers) {


        get_ansdata = new File( getFilesDir() +"Answer"+s+".json");
        String previousJson = " ";
        try{
            if (get_ansdata.exists()) {


            } else {
                try {
                    get_ansdata.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            Log.e("prejson",previousJson);



        }catch (NullPointerException e){
            try {
                get_ansdata.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        try {
            Log.e("prejson","putting data");
            FileWriter file = new FileWriter(get_ansdata);
            file.write("");
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        //json file of questions is an empty

        JSONObject jsonObject=new JSONObject();
        try {

            //ans array
            JSONArray main_arrayis=null;

            if(flaggameattempfirst){
                //if first time
                main_arrayis=new JSONArray(answers);
                ans_submission_string=main_arrayis.toString();

            }else {
                //if more than first time
                main_arrayis=new JSONArray(answers);
                ans_submission_string=main_arrayis.toString();

            }



            jsonObject.put("answeres",main_arrayis);
            jsonObject.put("levelis",levelis);
            jsonObject.put("currentquestionis",qc+1);
            jsonObject.put("firstparacount",firstpara);
            //leveling
            jsonObject.put("paraQueslevel",paraques);
            jsonObject.put("correctQueslevel",correctques);

            jsonObject.put("no of ques in list",no_of_ques_in_list);
            jsonObject.put("no of ques in para",no_of_current_ques_per_para);
            jsonObject.put("indexis",qc_index);
            jsonObject.put("main_assests_indexis",index);
            jsonObject.put("correctanswerscountparam",cqa);
            jsonObject.put("paracount",para_count);
            jsonObject.put("acq",acq);
            jsonObject.put("bannerparagraph_count",paragraph_count);
            jsonObject.put("no_of_current_ques_per_para",no_of_current_ques_per_para);
            jsonObject.put("total ques shows",cnt);

            String assetsstring="";
            for(int i=0;i<assets.size();i++){
                if(i==0){
                    assetsstring=assets.get(i);
                }else {
                    assetsstring=assetsstring+"qwerty123"+assets.get(i);
                }
            }

            String levelstring="";
            for(int i=0;i<levels.size();i++){
                if(i==0){
                    levelstring= String.valueOf(levels.get(i));
                }else {
                    levelstring=levelstring+"qwerty123"+String.valueOf(levels.get(i));
                }
            }

            String audiostring="";
            for(int i=0;i<audio.size();i++){
                if(i==0){
                    audiostring= String.valueOf(audio.get(i));
                }else {
                    audiostring=audiostring+"qwerty123"+String.valueOf(audio.get(i));
                }
            }

            String assetsidstring="";
            for(int i=0;i<asset_id.size();i++){
                if(i==0){
                    assetsidstring= String.valueOf(asset_id.get(i));
                }else {
                    assetsidstring=assetsidstring+"qwerty123"+String.valueOf(asset_id.get(i));
                }
            }

            jsonObject.put("assetsarray",assetsstring);
            jsonObject.put("levelsarray",levelstring);
            jsonObject.put("audioarray",audiostring);
            jsonObject.put("assetsid",assetsidstring);

            if(islevelingenable){
                jsonObject.put("islevelingenable","true");
            }else {
                jsonObject.put("islevelingenable","false");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //putting data into file
        try {
            Log.e("klocal_jobjdata",jsonObject.toString());
            FileWriter file = new FileWriter(get_ansdata);
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        //need




        //  Log.e("Jsonfile write", jarray.toString());

        // jsonRead();
    }


    //set leveling params,get params data,answers data new changes
    private void setlevelingparams(String s) {
        getdatafromfile = new File( getFilesDir() +"Answer"+s+".json");
        String levelingparamsandanswers = " ";
        try{
            if (getdatafromfile.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(getdatafromfile);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        ansdatafromfile = Charset.defaultCharset().decode(bb).toString();
                        Log.e("prejson ", "getques old data-" + ansdatafromfile);
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                //new GetQues().execute();
            }
            //get data from file
            Log.e("prejson",ansdatafromfile);



        }catch (NullPointerException e){
            try {
                getdatafromfile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }



        //json file of questions is an empty



        Log.e("klevel_fromfile",ansdatafromfile);

        try {
            JSONObject jsonObjectis=new JSONObject(ansdatafromfile);
            JSONArray jsonarrayanswers=jsonObjectis.getJSONArray("answeres");

            for(int i=0;i<jsonarrayanswers.length();i++){
                JSONObject js=jsonarrayanswers.getJSONObject(i);
                qidj.add(i,js.getString("Q_Id"));
                qtid.add(i,js.getString("QT_Id"));
                ansj.add(i,js.getString("SD_UserAnswer"));
                stj.add(i,js.getString("SD_StartTime"));
                endj.add(i,js.getString("SD_EndTime"));

            }
            //old stored answers
            mainformdatastring=jsonarrayanswers.toString();
            levelis=jsonObjectis.getInt("levelis");
            qc=jsonObjectis.getInt("currentquestionis");
            cqa=jsonObjectis.getInt("correctanswerscountparam");
            firstpara=jsonObjectis.getInt("firstparacount");
            paraques=jsonObjectis.getInt("paraQueslevel");
            correctques=jsonObjectis.getInt("correctQueslevel");
            qc_index=jsonObjectis.getInt("indexis");
            index=jsonObjectis.getInt("main_assests_indexis");
            para_count=jsonObjectis.getInt("paracount");
            no_of_ques_in_list=jsonObjectis.getInt("no of ques in list");
            no_of_current_ques_per_para=jsonObjectis.getInt("no of ques in para");
            no_of_current_ques_per_para=jsonObjectis.getInt("no_of_current_ques_per_para");
            paragraph_count=jsonObjectis.getInt("bannerparagraph_count");
            cnt=jsonObjectis.getInt("total ques shows");



            acq=jsonObjectis.getInt("acq");


            if(jsonObjectis.getString("islevelingenable").equalsIgnoreCase("true")){
                islevelingenable=true;
            }else {
                islevelingenable=false;
            }

            String assetsarray=jsonObjectis.getString("assetsarray");
            String levelsarray=jsonObjectis.getString("levelsarray");
            String audioarray=jsonObjectis.getString("audioarray");
            String assetsid=jsonObjectis.getString("assetsid");

            String assetsarrayis[]=assetsarray.split("qwerty123");
            for(int i=0;i<assetsarrayis.length;i++){
                assets.add(i,assetsarrayis[i]);
            }

            for(int i=0;i<assets.size();i++){
                Log.e("assets",assets.get(i));
            }
            String levelsarrayis[]=levelsarray.split("qwerty123");
            for(int i=0;i<levelsarrayis.length;i++){
                levels.add(i, Integer.valueOf(levelsarrayis[i]));
            }
            for(int i=0;i<levels.size();i++){
                Log.e("levels",""+levels.get(i));
            }
            String audioarrayis[]=audioarray.split("qwerty123");
            for(int i=0;i<audioarrayis.length;i++){
                audio.add(i,Integer.valueOf(audioarrayis[i]));
            }
            for(int i=0;i<audio.size();i++){
                Log.e("audio",""+audio.get(i));
            }
            String assetsidis[]=assetsid.split("qwerty123");
            for(int i=0;i<assetsidis.length;i++){
                asset_id.add(i,Integer.valueOf(assetsidis[i]));
            }
            for(int i=0;i<asset_id.size();i++){
                Log.e("assets_id",""+asset_id.get(i));
            }



            for(int i=0;i<jsonarrayanswers.length();i++){
                JSONObject jsonObject=jsonarrayanswers.getJSONObject(i);
                String ids=jsonObject.getString("Q_Id");
                for(int j=0;j<qs.size();j++){
                    QuestionModel questionModel=qs.get(j);
                    if(ids.equalsIgnoreCase(questionModel.getQ_id())){
                        if(islevelingenable){
                            qs.remove(j);
                        }else {

                        }

                    }
                }
            }

            loadparainresume();
        } catch (JSONException e) {
            Log.e("exception","in get leveling params");
            e.printStackTrace();
        }



        flag_click=true;

    }

    public void getPostData(){

        getdatafromfile = new File( getFilesDir() +"Answer"+cate+game+gdid+ceid+uidd+tgid+".json");
        String levelingparamsandanswers = " ";
        try{
            if (getdatafromfile.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(getdatafromfile);

                    try {
                        FileChannel fc = stream.getChannel();
                        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                        ansdatafromfile = Charset.defaultCharset().decode(bb).toString();
                        Log.e("prejson ", "getques old data-" + ansdatafromfile);
                    } finally {
                        stream.close();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                //new GetQues().execute();
            }
            //get data from file
            Log.e("prejson",ansdatafromfile);



        }catch (NullPointerException e){
            try {
                getdatafromfile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        try {
            JSONObject jsonObjectis=new JSONObject(ansdatafromfile);

            JSONArray jsonarrayanswers=jsonObjectis.getJSONArray("answeres");
            postvaluearray=jsonarrayanswers.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject studentsObj = new JSONObject();
        JSONArray jsonArray= null;
        try {
            jsonArray = new JSONArray(postvaluearray);
            studentsObj.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postvalues=studentsObj.toString();

    }

    //new changes
    public String formdatalocal(int qc,int val){
        String jn="";


        try {
            JSONArray js=new JSONArray();
            JSONObject student=null;
            for(int i=0;i<qc+1;i++) {
                student = new JSONObject();
                student.put("U_Id", uidd);
                student.put("S_Id", sid);
                student.put("TG_Id", tgid);

                if(val==0){
                    if(i==qc) {
                        student.put("Q_Id", qsmodel.getQ_id());
                        student.put("QT_Id", qsmodel.getQ_id());
                        student.put("SD_UserAnswer", " ");
                        student.put("SD_StartTime", now);
                        student.put("SD_EndTime", now);
                    }else {
                        student.put("Q_Id", qidj.get(i));
                        student.put("QT_Id",qtid.get(i));
                        student.put("SD_UserAnswer", ansj.get(i));
                        student.put("SD_StartTime", stj.get(i));
                        student.put("SD_EndTime", endj.get(i));
                    }
                }else {
                    student.put("Q_Id", qidj.get(i));
                    student.put("QT_Id",qtid.get(i));
                    student.put("SD_UserAnswer", ansj.get(i));
                    student.put("SD_StartTime", stj.get(i));
                    student.put("SD_EndTime", endj.get(i));
                }

                js.put(student);

            }

            jn=js.toString();
            Log.e("klocal_qc",""+qc);

            if(val==0){
                Log.e("klocal_formdata0",jn);

            }else {
                Log.e("klocal_formdata1",jn);

            }
            formdatanewstring=jn;
            jsonMakeFileAnswerandParams(cate+game+gdid+ceid+uidd+tgid,jn);

        }catch (Exception e){
            e.printStackTrace();
        }
        return jn;
    }


    public void loadparainresume(){
        try {
            if(slidingLayer.isOpened()) {
                slidingLayer.closeLayer(true);
            }
        }catch (Exception e){
            Log.e("mystatus","closelayer exception");
            e.printStackTrace();
        }
        try{

        }catch (Exception e){
            e.printStackTrace();
        }
        if(islevelingenable){
            if (assets.get(index).contains("7769896014")) {
                String[] valuesare = assets.get(index).split("7769896014");
                String imgis = valuesare[0];
                String parais = valuesare[1];
                final Animation animation_fade =
                        AnimationUtils.loadAnimation(New_MSQ.this,
                                R.anim.fade_in);


                String[] pic = imgis.split("/");
                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                final File image_file = new File(folder, pic[pic.length - 1]);
                path_image = image_file.getPath();
                final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());


                //  paragraph.startAnimation(animation_fade);


                paragraph.setText(parais);
                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.VISIBLE);
                        paragraph.setVisibility(View.VISIBLE);


                        // para_scroller.requestChildFocus(paraframe,paraframe);
                        para_scroller.scrollTo(0, para_scroller.getTop());
                        para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                        para_scroller.smoothScrollTo(0, 0);
                        para_image.startAnimation(animation_fade);

                        paragraph.startAnimation(animation_fade);

                        Log.e("w n h", bmp.getWidth() + "," + bmp.getHeight());
                        //  para_image.setImageBitmap(decodeFile(image_file,300,400));

                        para_image.setImageBitmap(bmp);
                      /*      try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
*/

                    }
                }.start();
            } else if ((assets.get(index).contains(".jpg")) || (assets.get(index).contains(".jpeg")) || (assets.get(index).contains(".png"))) {
                paragraph.setVisibility(View.GONE);


                String[] pic = assets.get(index).split("/");
                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                final File image_file = new File(folder, pic[pic.length - 1]);
                path_image = image_file.getPath();
                final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());

                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.VISIBLE);
                        // para_scroller.requestChildFocus(paraframe,paraframe);
                        para_scroller.scrollTo(0, para_scroller.getTop());
                        para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                        para_scroller.smoothScrollTo(0, 0);
                        final Animation animation_fade =
                                AnimationUtils.loadAnimation(New_MSQ.this,
                                        R.anim.fade_in);
                        para_image.startAnimation(animation_fade);


                        Log.e("w n h", bmp.getWidth() + "," + bmp.getHeight());
                        //para_image.setImageBitmap(decodeFile(image_file,300,400));
                        para_image.setImageBitmap(bmp);
                         /*   try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/

                    }
                }.start();
            } else {


                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.GONE);


                        paragraph.setVisibility(View.VISIBLE);
                        paragraph.requestFocus();
                        // paragraph.scrollTo(0,para_scroller.getTop());
                        //para_scroller.requestChildFocus(paraframe,paraframe);
                        para_scroller.scrollTo(0, para_scroller.getTop());
                        para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                        para_scroller.smoothScrollTo(0, 0);

                        final Animation animation_fade =
                                AnimationUtils.loadAnimation(New_MSQ.this,
                                        R.anim.fade_in);
                        paragraph.startAnimation(animation_fade);
                        paragraph.setText(assets.get(index));

                    }
                }.start();
            }



        }else {
            if(assets.get(acq).contains("7769896014")){
                String [] valuesare=assets.get(acq).split("7769896014");
                String imgis=valuesare[0];
                String parais=valuesare[1];
                final Animation animation_fade =
                        AnimationUtils.loadAnimation(New_MSQ.this,
                                R.anim.fade_in);


                String[] pic = imgis.split("/");
                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                final File image_file = new File(folder, pic[pic.length - 1]);
                path_image=image_file.getPath();
                final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());


                //  paragraph.startAnimation(animation_fade);


                paragraph.setText(parais);
                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.VISIBLE);
                        paragraph.setVisibility(View.VISIBLE);


                        // para_scroller.requestChildFocus(paraframe,paraframe);
                        para_scroller.scrollTo(0,para_scroller.getTop());
                        para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                        para_scroller.smoothScrollTo(0,0);
                        para_image.startAnimation(animation_fade);

                        paragraph.startAnimation(animation_fade);

                        Log.e("w n h", bmp.getWidth()+","+bmp.getHeight());
                        //  para_image.setImageBitmap(decodeFile(image_file,300,400));

                        para_image.setImageBitmap(bmp);
                      /*      try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
*/

                    }
                }.start();
            }else
            if ((assets.get(acq).contains(".jpg")) || (assets.get(acq).contains(".jpeg")) || (assets.get(acq).contains(".png"))) {
                paragraph.setVisibility(View.GONE);



                String[] pic = assets.get(acq).split("/");
                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                final File image_file = new File(folder, pic[pic.length - 1]);
                path_image=image_file.getPath();
                final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());

                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.VISIBLE);
                        // para_scroller.requestChildFocus(paraframe,paraframe);
                        para_scroller.scrollTo(0,para_scroller.getTop());
                        para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                        para_scroller.smoothScrollTo(0,0);
                        final Animation animation_fade =
                                AnimationUtils.loadAnimation(New_MSQ.this,
                                        R.anim.fade_in);
                        para_image.startAnimation(animation_fade);


                        Log.e("w n h", bmp.getWidth()+","+bmp.getHeight());
                        //para_image.setImageBitmap(decodeFile(image_file,300,400));
                        para_image.setImageBitmap(bmp);
                         /*   try {
                                Bitmap b = ((BitmapDrawable) para_image.getBackground()).getBitmap();
                                int w = bmp.getWidth();
                                int h = bmp.getHeight();
                                Log.e("size", w + ", " + h);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/

                    }
                }.start();
            } else {


                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.GONE);


                        paragraph.setVisibility(View.VISIBLE);
                        paragraph.requestFocus();
                        // paragraph.scrollTo(0,para_scroller.getTop());
                        //para_scroller.requestChildFocus(paraframe,paraframe);
                        para_scroller.scrollTo(0,para_scroller.getTop());
                        para_scroller.fullScroll(NestedScrollView.FOCUS_UP);
                        para_scroller.smoothScrollTo(0,0);

                        final Animation animation_fade =
                                AnimationUtils.loadAnimation(New_MSQ.this,
                                        R.anim.fade_in);
                        paragraph.startAnimation(animation_fade);
                        paragraph.setText(assets.get(acq));

                    }
                }.start();
            }




        }
       // qno.setText(no_of_current_ques_per_para + "/" + audio.get(cnt));


    }
    public void showretrydetectDialogue() {
        Log.e("showretrydetectDialogue","showretrydetectDialogue");

        alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(New_MSQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();


                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            detectAndFrameother1(autotakenpic.get(count_detect_no));
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretrydetectDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretryverifyDialogue() {
        Log.e("showretryverifyDialogue","showretryverifyDialogue");

        alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(New_MSQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new VerificationTask(mfaceid1, detectedfaceid.get(image_face_count)).execute();
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryverifyDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretrys3urlDialogue() {
        Log.e("showretrys3urlDialogue","showretrys3urlDialogue");

        alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(New_MSQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new S3getimageurl().execute();
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretrys3urlDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }
    public void showretryimagesendDialogue() {
        Log.e("showretryimgsndDialogue","showretryimagesendDialogue");

        alertDialog = new Dialog(New_MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView)alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){


            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();


                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(New_MSQ.this);

                        if(connectionUtils.isConnectionAvailable()){
                            try {
                                if (!verification_progress.isShowing() || verification_progress != null) {

                                    verification_progress.show();

                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new ImageSend().execute();
                        }else {
                            try {
                                if (!verification_progress.isShowing()) {
                                    assess_progress.show();
                                }
                            }catch (Exception e){
                                assess_progress.show();
                            }
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryimagesendDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            alertDialog.show();
        }catch (Exception e){}
    }

}

