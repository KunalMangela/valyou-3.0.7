package com.globalgyan.getvalyou.MasterSlaveGame;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.globalgyan.getvalyou.R;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.MasterSlaveGame.New_MSQ.blinking_anim;
import static com.globalgyan.getvalyou.MasterSlaveGame.New_MSQ.vibrator;


/**
 * Created by komal on 20-04-2018.
 */

public class Option_clickable extends RecyclerView.Adapter<Option_clickable.MyViewHolder> {

    int c=0;
    private Context mContext;
    private static ArrayList<String> options_array;
    Typeface typeface1;
    static int pos = 0;
    boolean clicked = false;
    long mLastClickTime = 0;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView option;
        ImageView options;
        LinearLayout msq_options;
        RelativeLayout msq_parent;
        public MyViewHolder(View view) {
            super(view);

            option = (FontTextView) view.findViewById(R.id.option_is_clickable);
            options=(ImageView)view.findViewById(R.id.a_opt);
            msq_options=(LinearLayout) view.findViewById(R.id.msq_options);
            msq_parent=(RelativeLayout) view.findViewById(R.id.msq_parent);

        }
    }



    public Option_clickable(Context mContext, ArrayList<String> options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.msq_option_clickable, parent, false);



        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.option.setText(options_array.get(position));



        if(position==0){


            holder.options.setVisibility(View.GONE);

        }
        else if(position == 1){
            holder.options.setImageResource(R.drawable.option_a);

        }
        else if(position == 2){
            holder.options.setImageResource(R.drawable.option_b);

        }
        else if(position == 3){
            holder.options.setImageResource(R.drawable.option_c);

        }
        else if(position == 4){
            holder.options.setImageResource(R.drawable.option_d);

        }
        else if(position == 5){
            holder.options.setImageResource(R.drawable.opt_e);

        }

        holder.msq_options.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

              /*  if(position==0){
                    c++;
                    if(c==1){
                        Log.e("eventis","belowclick");
                        New_MSQ.flag_sizechanged="below";
                      //  New_MSQ.changeSize();
                    }
                    new CountDownTimer(500, 500) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            c=0;
                        }
                    }.start();
                    return true;

                }*/
                if(position==0){
                    return true;

                }
                if(clicked){
                    return true;
                }



                else {
                    return false;
                }
            }


        });
        holder.msq_options.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {



                                                 /*if(New_MSQ.flag_sizechanged.equalsIgnoreCase("para")){

                                                 }else {*/
                                                     if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                                                         return;
                                                     }
                                                     mLastClickTime = SystemClock.elapsedRealtime();
                                                     clicked = true;
                                               /*  try{
                                                     blinking_anim.cancel();
                                                 }
                                                 catch (Exception e){
                                                     e.printStackTrace();
                                                 }*/
                                                     New_MSQ.mp_click.start();
                                                     holder.msq_options.setEnabled(false);
                                                     holder.msq_options.setClickable(false);
                                                     holder.msq_options.setFocusable(false);

//                                                 holder.msq_parent.setEnabled(false);
//                                                 holder.msq_parent.setClickable(false);
//                                                 holder.msq_parent.setFocusable(false);
//
//                                                 holder.option.setEnabled(false);
//                                                 holder.option.setClickable(false);
                                                     //  MSQ.rc_options_non_clickable.addOnItemTouchListener(MSQ.disabler);

                                                     new CountDownTimer(2000, 2000) {
                                                         @Override
                                                         public void onTick(long millisUntilFinished) {

                                                         }

                                                         @Override
                                                         public void onFinish() {
                                                             clicked = false;

                                                             //      MSQ.rc_options_non_clickable.removeOnItemTouchListener(MSQ.disabler);
                                                             holder.msq_options.setEnabled(true);
                                                             holder.msq_options.setClickable(true);
                                                             holder.msq_options.setFocusable(true);

//                                                         holder.msq_parent.setEnabled(true);
//                                                         holder.msq_parent.setClickable(true);
//                                                         holder.msq_parent.setFocusable(true);
//
//                                                         holder.option.setEnabled(true);
//                                                         holder.option.setClickable(true);
                                                         }
                                                     }.start();
                                                     pos = position - 1;


//                                                 if (MSQ.list_tick_show) {
//                                                     if (MSQ.pos_clicked == pos) {
                                                     if (New_MSQ.crct == pos) {
                                                         holder.options.setBackgroundResource(R.drawable.round_ques_ans_green);
                                                         Log.e("ans", "correct" + New_MSQ.crct);
                                                         Log.e("ans", "correct" + pos);
                                                     } else {
                                                        // vibrator.vibrate(500);

                                                         holder.options.setBackgroundResource(R.drawable.round_ques_ans_red);

                                                         Log.e("ans", "wrong" + New_MSQ.crct);
                                                         Log.e("ans", "wrong" + pos);
                                                     }

                                                     New_MSQ.list_tick_show = false;
//                                                     } else {
//                                                         Log.e("matched", pos + "notmatch");
//
//                                                     }
//                                                 }


                                                     Intent brodcast_receiver_screen = new Intent("Show_screen");
                                                     brodcast_receiver_screen.putExtra("pos", String.valueOf(pos));
                                                     Log.e("broad", "msg" + pos);
                                                     mContext.sendBroadcast(brodcast_receiver_screen);
                                                // }


                                                 New_MSQ.flag_sizechanged="below";
                                                // New_MSQ.changeSize();
                                                 Log.e("eventis","belowclick");

                                             }
                                         });



    }

    @Override
    public int getItemCount() {
        return options_array.size();
    }




}
