package com.globalgyan.getvalyou.MasterSlaveGame;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.globalgyan.getvalyou.R;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import static com.thefinestartist.utils.content.ContextUtil.sendBroadcast;

/**
 * Created by Globalgyan on 09-03-2018.
 */

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.MyViewHolder> {

    public static int pos_click=0;
    private Context mContext;
    private static String[] options_array;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView option;
        CircularProgressBar circularProgressBar,circularProgressBar_middle;


        public MyViewHolder(View view) {
            super(view);

            option = (TextView) view.findViewById(R.id.option_is);
            circularProgressBar = (CircularProgressBar)view.findViewById(R.id.circularbar);
            circularProgressBar_middle = (CircularProgressBar)view.findViewById(R.id.circularbar_middle);
        }
    }


    public OptionsAdapter(Context mContext, String[] options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.option_items_layout, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

      /*  circularProgressBar.setColor(ContextCompat.getColor(this, R.color.progressBarColor));
        circularProgressBar.setBackgroundColor(ContextCompat.getColor(this, R.color.backgroundProgressBarColor));
        circularProgressBar.setProgressBarWidth(getResources().getDimension(R.dimen.progressBarWidth));
        circularProgressBar.setBackgroundProgressBarWidth(getResources().getDimension(R.dimen.backgroundProgressBarWidth));
        int animationDuration = 2500; // 2500ms = 2,5s
        circularProgressBar.setProgressWithAnimation(65, animationDuration);*/
        Typeface typeface1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/AppleChancery.ttf");
        holder.option.setTypeface(typeface1,Typeface.BOLD);
        holder.option.setText(options_array[position]);

        holder.option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.option.setEnabled(false);
                holder.option.setClickable(false);
                MasterSlaveGame.rc_options.addOnItemTouchListener(MasterSlaveGame.disabler);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        MasterSlaveGame.rc_options.removeOnItemTouchListener(MasterSlaveGame.disabler);
                        holder.option.setEnabled(true);
                        holder.option.setClickable(true);
                    }
                }.start();
                Log.e("correctans",String.valueOf(MasterSlaveGame.crct));
                if(MasterSlaveGame.crct==position){
                    holder.circularProgressBar.setColor(ContextCompat.getColor(mContext, R.color.lime));
                    holder.circularProgressBar_middle.setColor(ContextCompat.getColor(mContext, R.color.lime));

                }else {
                    holder.circularProgressBar.setColor(ContextCompat.getColor(mContext, R.color.red));
                    holder.circularProgressBar_middle.setColor(ContextCompat.getColor(mContext, R.color.red));
                }
                Intent brodcast_receiver_screen=new Intent("Show_screen");
                brodcast_receiver_screen.putExtra("pos", String.valueOf(position));
                mContext.sendBroadcast(brodcast_receiver_screen);
                int animationDuration = 1000; // 2500ms = 2,5s
                holder.circularProgressBar.setProgressWithAnimation(100, animationDuration);
                holder.circularProgressBar_middle.setProgressWithAnimation(100, animationDuration);


            }
        });


    }
    @Override
    public int getItemCount() {
        return options_array.length;
    }
}




