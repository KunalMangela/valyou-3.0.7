package com.globalgyan.getvalyou.MasterSlaveGame;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.globalgyan.getvalyou.ListeningGame.MAQ;
import com.globalgyan.getvalyou.MasterSlave;
import com.globalgyan.getvalyou.R;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Globalgyan on 09-03-2018.
 */

public class OptionAdapterNonClikable extends RecyclerView.Adapter<OptionAdapterNonClikable.MyViewHolder> {

    private Context mContext;
    private static ArrayList<String> options_array;
    Typeface typeface1;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView option;
        ImageView options;

        public MyViewHolder(View view) {
            super(view);

            option = (FontTextView) view.findViewById(R.id.option_is_non_clikable);
            options=(ImageView)view.findViewById(R.id.opt);

        }
    }


    public OptionAdapterNonClikable(Context mContext, ArrayList<String> options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.options_item_layout_non_clikable, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.option.setText(options_array.get(position));

        if (position == 0) {


            holder.options.setVisibility(View.GONE);

        } else if (position == 1) {
            holder.options.setImageResource(R.drawable.option_a);

        } else if (position == 2) {
            holder.options.setImageResource(R.drawable.option_b);

        } else if (position == 3) {
            holder.options.setImageResource(R.drawable.option_c);

        } else if (position == 4) {
            holder.options.setImageResource(R.drawable.option_d);

        } else if (position == 5) {
            holder.options.setImageResource(R.drawable.opt_e);

        }

//        int pos = position;
//        if (MSQ.list_tick_show) {
//            if (MSQ.pos_clicked == pos) {
//                Log.e("matched", pos + "match");
//                if (MSQ.crct == pos) {
//                    holder.options.setBackgroundResource(R.drawable.round_ques_ans_green);
//                } else {
//                    holder.options.setBackgroundResource(R.drawable.round_ques_ans_red);
//                }
//
//                MSQ.list_tick_show = false;
//            } else {
//                Log.e("matched", pos + "notmatch");
//
//            }
//        }


        holder.option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.option.setEnabled(false);
                holder.option.setClickable(false);
                MSQ.rc_options_non_clickable.addOnItemTouchListener(MSQ.disabler);

                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        MSQ.rc_options_non_clickable.removeOnItemTouchListener(MSQ.disabler);
                        holder.option.setEnabled(true);
                        holder.option.setClickable(true);
                    }
                }.start();
                if (MSQ.crct == position) {
                    holder.options.setBackgroundResource(R.drawable.round_ques_ans_green);

                    Log.e("ans", "correct");

                } else {

                    holder.options.setBackgroundResource(R.drawable.round_ques_ans_red);

                    Log.e("ans", "wrong");
                }
                Intent brodcast_receiver_screen=new Intent("Show_screen");
                brodcast_receiver_screen.putExtra("pos", String.valueOf(position));
                mContext.sendBroadcast(brodcast_receiver_screen);

            }

        });
    }

    @Override
    public int getItemCount() {
        return options_array.size();
    }
}





