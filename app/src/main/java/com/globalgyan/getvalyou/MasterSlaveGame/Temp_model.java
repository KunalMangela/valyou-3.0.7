package com.globalgyan.getvalyou.MasterSlaveGame;

public class Temp_model {
    int count,level;

    public Temp_model() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
