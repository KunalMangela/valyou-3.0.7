package com.globalgyan.getvalyou.MasterSlaveGame;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.androidhiddencamera.CameraConfig;
import com.cunoraz.gifview.library.GifView;
import com.globalgyan.getvalyou.Activity_Fib;
import com.globalgyan.getvalyou.CameraPreview;
import com.globalgyan.getvalyou.CheckP;
import com.globalgyan.getvalyou.HomeActivity;
import com.globalgyan.getvalyou.ListeningGame.MAQ;
import com.globalgyan.getvalyou.R;
import com.globalgyan.getvalyou.apphelper.AppConstant;
import com.globalgyan.getvalyou.apphelper.BatteryReceiver;
import com.globalgyan.getvalyou.apphelper.PrefManager;
import com.globalgyan.getvalyou.helper.DataBaseHelper;
import com.globalgyan.getvalyou.model.QuestionModel;
import com.globalgyan.getvalyou.simulation.core.DownloadManagerPro;
import com.globalgyan.getvalyou.simulation.report.listener.DownloadManagerListener;
import com.globalgyan.getvalyou.utils.ConnectionUtils;
import com.globalgyan.getvalyou.utils.PreferenceUtils;
import com.iambedant.text.OutlineTextView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.HttpGet;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.VerifyResult;
import com.pluscubed.recyclerfastscroll.RecyclerFastScroller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsonbuilder.JsonBuilder;
import org.jsonbuilder.implementations.gson.GsonAdapter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.morrox.fontinator.FontTextView;
import im.delight.android.location.SimpleLocation;
import tyrantgit.explosionfield.ExplosionField;
import uk.co.senab.photoview.PhotoViewAttacher;

import static com.globalgyan.getvalyou.ProPic.mFaceId1;

public class MSQ extends AppCompatActivity implements DownloadManagerListener {
    //masterslave
    Typeface tf1,tf2,tf3;
    static NestedScrollView ques_scrollview;
    TextView paragraph;
    private SimpleLocation location;
    public static String flag_sizechanged="";
    public static RelativeLayout qno_layout;
    int height, width;
    private SimpleGestureFilter detector;
    public static String crcans="";
    public static int crcopt = 0;
    boolean flag_scroll=false;
    public static FrameLayout para_frame;
    public static RelativeLayout below_layout;
    public static int height1,height2;
    static Animation ques_anim;
    static Animation ques_anim_left;
    Animation fadeOut;
    Option_clickable option_clickable;
    RelativeLayout clickable_options_layout;
    static RelativeLayout main_ques_layout_window;
    boolean flag_isvisible=false, flag_rc_scrolling=false,para_touch_boolean=false;
    int count=0;
    static RecyclerView rc_options_non_clickable;
    int para_count=0;
    static Vibrator vibrator;
    boolean close_on_get_ques = false;

    //new
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    Face mface = null;
    //changes new design
    public static int crct;
    boolean flag_is_present_id=false;
    UUID faceid1, faceid2;
    Dialog alertDialog,proceed_dialog,alert_on_close;
    boolean alert_onclose_showing = false;
    boolean checkonp = true;
    CameraConfig mCameraConfig;
    boolean wtf;
    int scrollx,scrolly;
    ArrayList<String> qid_array;
    QuestionModel qsmodel;
    String newquid;
    Dialog dialog,dialoge;
    int fulltime;
    ArrayList<QuestionModel> qs=new ArrayList<>();
    File fol;
    public static int pos_clicked;
    List<Face> faces;
    int audio[] = new int[30];
    String assets[] = new String[30];
    private CountDownTimer countDownTimer;
    private long startTime = 180 * 1000;
    private final long interval = 1 * 1000;
    private long secLeft = startTime;
    DefaultHttpClient httpClient;
    HttpURLConnection urlConnection;
    boolean flagarrow_lick=false;
    HttpPost httpPost;
    HttpGet httpGet;
    int qansr = 0,scrollcount=0;
    Bitmap storedpic;
    //second boolean to prevent nonclickable list from scrolling
    boolean flag_click=false,click_to_prevent_scroll_list=false;
    public static boolean list_tick_show=false;
    public static RecyclerView.OnItemTouchListener disabler;
    int img_count;
    FontTextView timerrr, dqus;
    OutlineTextView qno;
    String tres = "";
    int code1,code2,code3;
    static DownloadManagerPro dm;
    static File folder;
    int cqa = 0;
    IntentFilter filter;
    ImageView para_image;
    //    PhotoViewAttacher photoAttacher;
    static InputStream is = null;
    static JSONObject jObj = null;
    ArrayList<String> qidj, ansj, stj, endj;
    JSONArray questions, audios;
    NestedScrollView para_scroller;
    static String json = "", jso = "", Qus = "", Qs = "", token = "", token_type = "", quesDis = "",assessg="";
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    int score = 0;
    ArrayList<String> optioncard;
    ImageView close;
    URL ppp;
    int pauseCount = 0, qc = 0, qid = 0, tq = 0, ac = -1,cnt=0,no_of_ques_in_list=0,anim_limit,height_scrollview_in_pixel;
    SimpleDateFormat df1;
    Calendar c;
    LinearLayout cl1, cl2, cl3, cl4, cl5;
    FontTextView o1, o2, o3, o4, o5;
    String uidd, adn, ceid, gdid, game, cate, tgid, sid, aid;
    private ExplosionField mExplosionField;
    String candidateId;
    RelativeLayout rel_bg_temp;
    File filepro;
    int optionsize;
    boolean isvisibleoption=false;
    String now;

    //  KProgressHUD assess_progress;

    Dialog assess_progress;

    //no use
    KProgressHUD progressDialog;


    //KProgressHUD quesLoading;

    Dialog quesLoading;


    //KProgressHUD verification_progress;

    Dialog verification_progress;

    FrameLayout frameimageim;

    public static MediaPlayer mp_click;


    RelativeLayout timer_ques;

    boolean proceed_dialog_showing = false;


    public static LinearLayout weightedlinear;
    String faceid;
    public static String mImageFileLocation = "";
    Uri imageUri;
    Bitmap bmpother;

    int count_detect_no=0,image_face_count=0,auto_capt_count=0;
    ArrayList<UUID> detectedfaceid = new ArrayList<>();
    ArrayList<String> imagelocation = new ArrayList<>();
    ArrayList<Bitmap> autotakenpic = new ArrayList<>();
    ArrayList<String > facedetectedimagelocation = new ArrayList<>();
    String autocapimg1loc1, autocapimg1loc2,autocapimg1loc3;
    ArrayList<String > autocapimgloc = new ArrayList<>();
    ArrayList<String > imgurllist = new ArrayList<>();
    String img1,img2="",img3="";
    String replacedurl;
    private Camera camera;
    Timer timer1;
    CountDownTimer waitTimer;
    public int cameraId = 2;
    private CameraPreview mPreview;
    FrameLayout preview;
    FaceServiceClient faceServiceClient;
    PrefManager prefManager;
    Bitmap decodedByte;
    int piccapturecount;



    FrameLayout paraframe;


    float p1_rel_y=0,p2_rel_y=0;
    String path_image="";
    String direction="bottom";
    Animation slideUpAnimation,slidedownAnimation;
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR
            = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR
            = new AccelerateInterpolator();

    ImageView red_planet,yellow_planet,purple_planet,earth, orange_planet;

    ObjectAnimator red_planet_anim_1,red_planet_anim_2,yellow_planet_anim_1,yellow_planet_anim_2,purple_planet_anim_1,purple_planet_anim_2,
            earth_anim_1, earth_anim_2, orange_planet_anim_1,orange_planet_anim_2,astro_outx, astro_outy,astro_inx,astro_iny, alien_outx,alien_outy,
            alien_inx,alien_iny;


    RecyclerFastScroller recyclerFastScroller;
    BatteryReceiver mBatInfoReceiver;
    BatteryManager bm;
    int batLevel;
    AppConstant appConstant;
    boolean isPowerSaveMode;
    PowerManager pm;
    String first_msg = "", quit_msg="";

    @SuppressLint({"NewApi", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msq);
        //masterslave
     /*   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
  */      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        appConstant = new AppConstant(this);
        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        isPowerSaveMode = pm.isPowerSaveMode();
        location = new SimpleLocation(this);
        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        weightedlinear=(LinearLayout)findViewById(R.id.weightedlinear);
        qno_layout=(RelativeLayout)findViewById(R.id.qno_layout);
//        faceServiceClient =
//                new FaceServiceRestClient("https://westcentralus.api.cognitive.microsoft.com/face/v1.0", "9cf4bd3ca93149098a4f60f90d45d7d1");

        tf1 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");
        tf2 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");
        tf3 = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");

        qsmodel=new QuestionModel();

        paragraph=(TextView)findViewById(R.id.paragraphis);
        createFolder();
        para_scroller=(NestedScrollView) findViewById(R.id.para_scrolleris);

        mp_click=MediaPlayer.create(this, R.raw.button_click);

        red_planet = (ImageView)findViewById(R.id.red_planet);
        yellow_planet = (ImageView)findViewById(R.id.yellow_planet);
        purple_planet = (ImageView)findViewById(R.id.purple_planet);
        earth = (ImageView)findViewById(R.id.earth);
        orange_planet = (ImageView)findViewById(R.id.orange_planet);

        //recyclerFastScroller=(RecyclerFastScroller) findViewById(R.id.fastscroll);

        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        timer_ques = (RelativeLayout)findViewById(R.id.rl_above);

        dm = new DownloadManagerPro(this.getApplicationContext());
        dm.init(folder.getPath(), 12, MSQ.this);
        para_image=(ImageView)findViewById(R.id.para_imageis);

        paraframe = (FrameLayout)findViewById(R.id.para_frame);
        findViewById(R.id.para_frame).requestFocus();
        frameimageim=(FrameLayout)findViewById(R.id.frameimageim);
        //  ques=(TextView)findViewById(R.id.qdis);
        //  ques_scrollview=(NestedScrollView)findViewById(R.id.firstscroll);

        //  timer_seek=(IndicatorSeekBar)findViewById(R.id.timer_seek);


//        verification_progress = KProgressHUD.create(MSQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        verification_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        verification_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verification_progress.setContentView(R.layout.planet_loader);
        Window window1 = verification_progress.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        wlp1.gravity = Gravity.CENTER;
        wlp1.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window1.setAttributes(wlp1);
        verification_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        verification_progress.setCancelable(false);
        GifView gifView2 = (GifView) verification_progress.findViewById(R.id.loader);
        gifView2.setVisibility(View.VISIBLE);
        gifView2.play();
        gifView2.setGifResource(R.raw.loader_planet);
        gifView2.getGifResource();


        rc_options_non_clickable=(RecyclerView)findViewById(R.id.non_clickableoptions_listis);
        para_frame=(FrameLayout)findViewById(R.id.para_frameis);
        below_layout=(RelativeLayout)findViewById(R.id.below_layout);
        //options_clickable
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        String[] options_array={"A","B","C","D","E","F","G","H","I","J","K","L","M"};
        int no_options=5;
        String [] confirm_options=new String[no_options];

        for(int i=0;i<5;i++){
            confirm_options[i]=options_array[i];
        }
//        rc_options.setAdapter(new OptionsAdapter(this,confirm_options));
        //options_non_clickable
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_options_non_clickable.setLayoutManager(linearLayoutManager1);
        String[] options_array_non_clikable={"Q1: Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating?","Option A:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option B:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option C:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option D:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option E:Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating","Option F","Option G"};
        final String [] confirm_options_non_clickable=new String[no_options+1];
        for(int i=0;i<6;i++){
            confirm_options_non_clickable[i]=options_array_non_clikable[i];
        }
        // rc_options_non_clickable.setAdapter(new OptionAdapterNonClikable(this,confirm_options_non_clickable));
        filter = new IntentFilter("Show_screen");
        this.registerReceiver(mReceiver, filter);

        //option_non_clickable_temp
        LinearLayoutManager linearLayoutManager11 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        //  rc_options_non_clickable_temp.setAdapter(new OptionAdapterNonClikable(this,confirm_options_non_clickable));

        para_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("eventis","paratouch");
                flag_sizechanged="para";
                changeSize();
                return false;
            }
        });

        paragraph.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                flag_sizechanged="para";
                Log.e("eventis","paratouch");
                changeSize();
                return false;
            }
        });

        /*rc_options_non_clickable.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("eventis","belowtouch");
                return false;
            }
        });*/
        prefManager = new PrefManager(MSQ.this);

        clickable_options_layout=(RelativeLayout)findViewById(R.id.clickable_option_layout_is);

        preview = (FrameLayout) findViewById(R.id.camera_previewis);

        paragraph.setTypeface(tf1);
        disabler = new RecyclerViewDisabler();

//        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.anim.slideup);
//
//        slidedownAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.anim.slidedown);

        //  ques.setTypeface(tf2);
        paragraph.setText("Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating the subconscious, and is offered a chance to have his criminal history erased as payment for the implantation of another person's idea into a target's subconscious.[4] The ensemble cast additionally includes Ken Watanabe, Joseph Gordon-Levitt, Marion Cotillard, Ellen Page, Tom Hardy, Dileep Rao, Cillian Murphy, Tom Berenger, and Michael Caine.\n" +
                "\n" + "After the 2002 completion of Insomnia, Nolan presented to Warner Bros. a written 80-page treatment about a horror film envisioning  based on lucid dreaming.[5] Deciding he needed more experience before tackling a production of this magnitude and complexity, Nolan retired the project and instead worked on 2005's Batman Begins, 2006's The Prestige, and The Dark Knight in 2008.[6] The treatment was revised over 6 months and was purchased by Warner in February 2009.[7] Inception was filmed in six countries, beginning in Tokyo on June 19 and ending in Canada on November 22.[8] Its official budget was $160 million, split between Warner Bros and Legendary.[9] Nolan's reputation and success with The Dark Knight helped secure the film's $100 million in advertising expenditure.\n" +
                " Inception is a 2010 science fiction film written, co-produced, and directed by Christopher Nolan, and co-produced by Emma Thomas. The film stars Leonardo DiCaprio as a professional thief who steals information by infiltrating the subconscious, and is offered a chance to have his criminal history erased as payment for the implantation of another person's idea into a target's subconscious.[4] The ensemble cast additionally includes Ken Watanabe, Joseph Gordon-Levitt, Marion Cotillard, Ellen Page, Tom Hardy, Dileep Rao, Cillian Murphy, Tom Berenger, and Michael Caine.\n" +
                "\n" + "  After the 2002 completion of Insomnia, Nolan presented to Warner Bros.");

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;


        height_scrollview_in_pixel=getPixel(MSQ.this,400);
        //   main_scroller.setLayoutParams(new LinearLayout.LayoutParams(width,height));

        //  para_frame.setLayoutParams(new LinearLayout.LayoutParams(width,height-getPixel(MasterSlaveGame.this,150)));
        // height=height-getPixel(MasterSlaveGame.this,180);
        // ques_frame.setLayoutParams(new LinearLayout.LayoutParams(width,height));
        // para_scroller.scrollTo(0,para_scroller.getTop());
        // main_scroller.scrollTo(0,main_scroller.getTop());

        final Animation animation_right =
                AnimationUtils.loadAnimation(MSQ.this,
                        R.anim.blink);

        ques_anim =
                AnimationUtils.loadAnimation(MSQ.this,
                        R.anim.right);
        ques_anim_left =
                AnimationUtils.loadAnimation(MSQ.this,
                        R.anim.left);
        fadeOut = new AlphaAnimation(0.0f, 1.0f);



        para_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });








        //new
        c = Calendar.getInstance();
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        progressDialog = KProgressHUD.create(MSQ.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Logging in")
                .setDimAmount(0.7f)
                .setCancellable(false);
//        quesLoading = KProgressHUD.create(MSQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("please wait")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        quesLoading = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        quesLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        quesLoading.setContentView(R.layout.assess_ques_loading_loader);
        Window window = quesLoading.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        quesLoading.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        quesLoading.setCancelable(false);
        GifView gifView1 = (GifView) quesLoading.findViewById(R.id.loader);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.raw.loader_planet);
        gifView1.getGifResource();
        ImageView close_btn = (ImageView) quesLoading.findViewById(R.id.close_loader);


//        assess_progress = KProgressHUD.create(MSQ.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait...")
//                .setDimAmount(0.7f)
//                .setCancellable(false);

        assess_progress = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        assess_progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assess_progress.setContentView(R.layout.planet_loader);
        Window window2 = assess_progress.getWindow();
        WindowManager.LayoutParams wlp2 = window2.getAttributes();
        wlp2.gravity = Gravity.CENTER;
        wlp2.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window2.setAttributes(wlp2);
        assess_progress.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        assess_progress.setCancelable(false);
        GifView gifView3 = (GifView) assess_progress.findViewById(R.id.loader);
        gifView3.setVisibility(View.VISIBLE);
        gifView3.play();
        gifView3.setGifResource(R.raw.loader_planet);
        gifView3.getGifResource();



        qid_array=new ArrayList<>();


        candidateId = PreferenceUtils.getCandidateId(MSQ.this);
        qidj = new ArrayList<>();
        ansj = new ArrayList<>();
        stj = new ArrayList<>();
        endj = new ArrayList<>();
        //need to change
        try {
            uidd = getIntent().getExtras().getString("uidg");
            adn = getIntent().getExtras().getString("adn");
            ceid = getIntent().getExtras().getString("ceid");
            gdid = getIntent().getExtras().getString("gdid");
            game = getIntent().getExtras().getString("game");
            cate = getIntent().getExtras().getString("category");
            tgid = getIntent().getExtras().getString("tgid");
            aid = getIntent().getExtras().getString("aid");
            token = getIntent().getExtras().getString("token");
            assessg=getIntent().getExtras().getString("assessgroup");
            startTime= Long.parseLong(getIntent().getExtras().getString("timeis"));
            startTime=startTime*1000;
        } catch (Exception ex) {

        }
        first_msg = "We are submitting your answers and you will not able to answer again the same category";

        quit_msg = first_msg+ " "+ cate;

        timerrr = (FontTextView) findViewById(R.id.timerris);
        qno = (OutlineTextView) findViewById(R.id.qnois);
        qno.setTypeface(tf3);
        // dqus = (FontTextView) findViewById(R.id.qdis);
      //  countDownTimer = new MyCountDownTimer(startTime, interval);
       // timerrr.setText(String.valueOf(startTime / 1000));
        timerrr.setVisibility(View.INVISIBLE);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pauseCount == 0) {
                    //    vdo_performance_ques_timer.cancel();
                    close.setEnabled(false);

                   /* AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MSQ.this, R.style.DialogTheme);
// ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = LayoutInflater.from(MSQ.this);
                    View dialogView = inflater.inflate(R.layout.quit_warning, null);
                    dialogBuilder.setView(dialogView);


                    alertDialog = dialogBuilder.create();

                    alertDialog.setCancelable(false);
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

                    alertDialog.show();*/

                    alert_on_close = new Dialog(MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alert_on_close.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert_on_close.setContentView(R.layout.assess_quit_warning);
                    Window window = alert_on_close.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alert_on_close.setCancelable(false);

                    alert_on_close.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alert_on_close.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView)alert_on_close.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alert_on_close.findViewById(R.id.alien);

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();



                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView)alert_on_close.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView)alert_on_close.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            close.setEnabled(true);
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    alert_onclose_showing = false;

                                    alert_on_close.dismiss();
                                   // changeQues();
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            close.setEnabled(true);
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400,400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        alert_on_close.dismiss();
                                        alert_onclose_showing = false;
                                        countDownTimer.cancel();
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        alert_on_close.dismiss();
                                        alert_onclose_showing = false;

                                        countDownTimer.cancel();
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();


                        }
                    });

                    try{
                        alert_on_close.show();
                        alert_onclose_showing = true;
                    }catch(Exception e){

                    }
                } else {
                    if (qansr > 0) {
                        countDownTimer.cancel();
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();

                    } else {
                        countDownTimer.cancel();
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();

                    }
                }


            }
        });




        para_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayImage();
            }
        });
        ImageView imgfullscreen_close=(ImageView)findViewById(R.id.closeimageis);
        imgfullscreen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close.setEnabled(true);
                close.setClickable(true);
                frameimageim.setVisibility(View.GONE);
            }
        });
        new GetQues().execute();
//        checkfacedetection();

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close_on_get_ques = true;
                try{
                    new GetQues().cancel(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Intent i = new Intent(MSQ.this, HomeActivity.class );
                Log.e("navigation","ass");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);  //this combination of flags would start a new instance even if the instance of same Activity exists.
                i.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tabs","normal");
                startActivity(i);
                finish();

                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
            }
        });


    }












    @SuppressLint("ClickableViewAccessibility")

    private void createFolder() {
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "SimulationData");
        boolean success = true;
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }
    public  void clickMethods(final int position) {
        list_tick_show=true;
        //  click_to_prevent_scroll_list=true;
        flag_click=true;
        //   rc_options_non_clickable.addOnItemTouchListener(disabler);

        // option_clickable.notifyDataSetChanged();

        //if count is equals to question count

        new CountDownTimer(10, 10) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                anim_limit=qc+1;

                //if count is equals to question count
                if(anim_limit==qs.size()) {
                    giveAnswers(position);
                }else {
                    //put dta in temp list
                   /* temp_ques_layout_window.setVisibility(View.VISIBLE);
                    animListdata();
                    giveAnswers(position);
                    main_ques_layout_window.startAnimation(ques_anim_left);
                    Log.e("animstatus","putdata");
                    temp_ques_layout_window.startAnimation(ques_anim);
                    hideArrow();*/
                    giveAnswers(position);


                }
            }
        }.start();


    }

    private void animListdata() {
        if (anim_limit < qs.size()) {
            qsmodel=qs.get(anim_limit);


            qno.setText(anim_limit + 1 + "/" + qs.size());

            try {
                //single question

                ArrayList<String> optioncardis = new ArrayList<>();
                optioncard.add("Q. "+qsmodel.getQuestion());
                if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                    String s1 = qsmodel.getOpt1();
                    s1 = s1.trim();
                    optioncard.add(""+s1);
                }
                if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                    String s2 = qsmodel.getOpt2();
                    s2 = s2.trim();
                    optioncard.add(""+s2);
                }
                if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                    String s3 = qsmodel.getOpt3();
                    s3 = s3.trim();
                    optioncard.add(""+s3);
                }
                if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                    String s4 = qsmodel.getOpt4();
                    s4 = s4.trim();
                    optioncard.add(""+s4);
                }
                if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                    String s5 = qsmodel.getOpt5();
                    s5 = s5.trim();
                    optioncard.add(""+s5);
                }

                OPtionNonclickableAdapterTemp oPtionNonclickableAdapterTemp=new OPtionNonclickableAdapterTemp(this,optioncardis);
                Log.e("animstatus","putdata");
                String[] options_array={"A","B","C","D","E","F","G","H","I","J","K","L","M"};
                int no_options=optioncardis.size();
                String [] confirm_options=new String[no_options-1];

                for(int i=0;i<no_options-1;i++){
                    confirm_options[i]=options_array[i];
                }
                //     rc_options.setAdapter(new NewOptionsAdapter(this,confirm_options));

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private void giveAnswers(int position) {
        countDownTimer.cancel();

        c = Calendar.getInstance();
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
       // now = df1.format(c.getTime()).toString();
        no_of_ques_in_list++;
        if(position==0){
            if (crct == 0) {
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;
                        qc++;

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("A");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()));
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        qc++;
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("A");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

            }
        }

        if(position==1){
            if (crct == 1) {
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;
                        qc++;

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("B");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        qc++;
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("B");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==2){
            if (crct == 2) {
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;
                        qc++;

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("C");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        qc++;
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("C");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==3){
            if (crct == 3) {
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;
                        qc++;

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("D");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            } else {
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        qc++;
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("D");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==4){
            if (crct == 4) {
                cqa++;
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        score = score + 50;
                        qc++;

                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("E");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

            } else {
                Handler han = new Handler();

                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        qc++;
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add("E");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);
            }
        }

        if(position==1000){
                Handler han = new Handler();

                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        qc++;
                        try {
                            qidj.add(qsmodel.getQ_id());
                            qid_array.add(qsmodel.getQ_id());
                            ansj.add(" ");
                            stj.add(now);
                            endj.add(df1.format(c.getTime()).toString());
                            qansr++;
                        } catch (Exception exx) {

                        }
                        changeQues();
                    }
                }, 1000);

        }
    }


    public static void hideArrow(){

    }
    public int getPixel(MSQ simulationActivity, int dps){
        Resources r = simulationActivity.getResources();

        int  px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }

    @Override
    public void OnDownloadStarted(long taskId) {

    }

    @Override
    public void OnDownloadPaused(long taskId) {

    }

    @Override
    public void onDownloadProcess(long taskId, double percent, long downloadedLength) {

    }

    @Override
    public void OnDownloadFinished(long taskId) {
        Log.e("image_count", ""+img_count);

        img_count=img_count-1;
        if(img_count==0){

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    startTime = questions.length() * 10000 * 1000;
                    changeParagraph(-1);
                    //startTimer();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                                   /* anim_frame.setVisibility(View.VISIBLE);
                                    animarrow1.startAnimation(slideUpAnimation);
                                    animarrow2.startAnimation(slideUpAnimation);*/
                        }
                    });

                    if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                        startanim();
                    }

                    // startVerification();
                    //    checkfacedetection();
                    try {
                        if (quesLoading.isShowing()) {
                            quesLoading.dismiss();
                        }
                    }catch (Exception e){

                    }
                }
            });


        }
    }

    @Override
    public void OnDownloadRebuildStart(long taskId) {

    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {

    }

    @Override
    public void OnDownloadCompleted(long taskId) {

    }

    @Override
    public void connectionLost(long taskId) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                qs.clear();
                try {
                    if (quesLoading.isShowing()) {
                        quesLoading.dismiss();
                    }
                }catch (Exception e){

                }
                try {
                    if (alertDialog.isShowing()) {

                    } else {
                        showGetQuesAgainDialogue();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    showGetQuesAgainDialogue();
                }

            }
        });
    }

    public void showGetQuesAgainDialogue() {
        Log.e("respmsg","dialogue");

      /*  final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                MSQ.this, R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(MSQ.this);
        View dialogView = inflater.inflate(R.layout.retry_anim_dialogue, null);

        dialogBuilder.setView(dialogView);


        alertDialog = dialogBuilder.create();


//                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

        alertDialog.show();*/
        alertDialog = new Dialog(MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();



            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();

                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils = new ConnectionUtils(MSQ.this);
                        if (connectionUtils.isConnectionAvailable()) {
                            new GetQues().execute();
                        } else {
                            quesLoading.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (quesLoading.isShowing() && quesLoading != null) {
                                        quesLoading.dismiss();


                                    }
                                    showGetQuesAgainDialogue();
                                }
                            }.start();
                        }

                    }
                }.start();




            }
        });

        try {
            if(!alertDialog.isShowing())
                alertDialog.show();
        }
        catch (Exception e){

        }
    }



    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }

        @Override
        public void onTick(long millisUntilFinished) {
            //  timer_seek.setMin(0);
            timerrr.setText("" + millisUntilFinished / 1000);
            secLeft = millisUntilFinished;
            int sec= (int) (fulltime/1000-secLeft/1000);

            final int MINUTES_IN_AN_HOUR = 60;
            final int SECONDS_IN_A_MINUTE = 60;



            int minutes =  (sec / SECONDS_IN_A_MINUTE);
            sec -= minutes * SECONDS_IN_A_MINUTE;

            int hours = minutes / MINUTES_IN_AN_HOUR;
            minutes -= hours * MINUTES_IN_AN_HOUR;

        }


        @Override

        public void onFinish() {
            if (qc < qs.size()) {

                clickMethods(1000);

            }else {

                if (qansr > 0) {
                    tres = formdata(qansr);
                    checkonp = false;
                    scoreActivityandPostscore();

                } else {
                    tres = formdata();
                    checkonp = false;
                    scoreActivityandPostscore();

                }
            }



        }
    }
    private class GetQues extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            img_count=0;
            try {
                if((quesLoading!=null)&&(!quesLoading.isShowing()))
                quesLoading.show();
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {
                try {
                   /* httpClient = new DefaultHttpClient();
                    httpPost = new HttpPost("http://35.154.93.176/Player/GetQuestions");*/

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("CE_Id", ceid)
                            .object("Game", game)
                            .object("Category", cate)
                            .object("GD_Id", gdid)
                            .build().toString();


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/GetQuestions");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();
                    is = urlConnection.getInputStream();


                    Log.e("dismiss", ""+is);


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alertDialog.isShowing()) {

                                } else {
                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                showGetQuesAgainDialogue();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alertDialog.isShowing()) {

                                } else {
                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                showGetQuesAgainDialogue();
                            }

                        }
                    });
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qs.clear();
                            try {
                                if (quesLoading.isShowing()) {
                                    quesLoading.dismiss();
                                }
                            }catch (Exception e){

                            }
                            try {
                                if (alertDialog.isShowing()) {

                                } else {
                                    showGetQuesAgainDialogue();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                showGetQuesAgainDialogue();
                            }

                        }
                    });
                    e.printStackTrace();
                }

                String responseString = readStream(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result1 = responseString;

            } catch (Exception e) {
                e.getMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        qs.clear();
                        try {
                            if (quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                        }catch (Exception e){

                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                showGetQuesAgainDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            showGetQuesAgainDialogue();
                        }

                    }
                });
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            return result1;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONArray data = new JSONArray(result);
                Log.e("QUESDATA", data.length() + "");
                // JSONArray sidd = data.getJSONArray(0);
                final JSONObject sidobj = data.getJSONObject(0);
                sid = sidobj.getString("S_Id");
                Log.e("fbwwp", sid);
                audios = data.getJSONArray(1);
                Log.e("adddddd", audios.length() + "");
                for (int i = 0; i < audios.length(); i++) {

                    assets[i] = "";
                    JSONObject aaqonj = audios.getJSONObject(i);
                    String aaq = aaqonj.getString("Q_ChildQIds");
                    if(( aaqonj.getString("Q_Asset").length()>4)&&(aaqonj.getString("Q_Question").length()>4)) {
                        String tobe_add=aaqonj.getString("Q_Asset")+"7769896014"+aaqonj.getString("Q_Question");
                        assets[i] = tobe_add;
                        img_count++;
                        String [] pic=aaqonj.getString("Q_Asset").split("/");
                        String last=pic[pic.length-1];
                        String [] lastname=last.split("\\.");
                        String name=lastname[0];
                        downloadData(aaqonj.getString("Q_Asset"),name);
                    }else if( aaqonj.getString("Q_Asset").equalsIgnoreCase(""))
                    {
                        assets[i] = aaqonj.getString("Q_Question");

                    }else {
                        assets[i] = aaqonj.getString("Q_Asset");
                        img_count++;
                        String [] pic=aaqonj.getString("Q_Asset").split("/");
                        String last=pic[pic.length-1];
                        String [] lastname=last.split("\\.");
                        String name=lastname[0];
                        downloadData(aaqonj.getString("Q_Asset"),name);
                    }
                    StringTokenizer st = new StringTokenizer(aaq, ",");
                    audio[i] = st.countTokens();

                }
                questions = data.getJSONArray(2);
                for(int i=0;i<questions.length();i++){
                    JSONObject js=questions.getJSONObject(i);
                    QuestionModel s=new QuestionModel();
                    s.setQT_id(js.getString("QT_Id"));
                    s.setQ_id(js.getString("Q_Id"));
                    s.setQuestion(js.getString("Q_Question"));
                    s.setOpt1(js.optString("Q_Option1"));
                    s.setOpt2(js.optString("Q_Option2"));
                    s.setOpt3(js.optString("Q_Option3"));
                    s.setOpt4(js.optString("Q_Option4"));
                    s.setOpt5(js.optString("Q_Option5"));
                    s.setCorrectanswer(js.getString("Q_Answer"));
                    s.setTime(js.getString("Q_MaxTime"));
                    qs.add(s);
                }
                timerrr.setVisibility(View.VISIBLE);
                Log.e("QUES", questions.length() + "");
                tq = questions.length();

                secLeft=startTime;
                final int MINUTES_IN_AN_HOUR = 60;
                final int SECONDS_IN_A_MINUTE = 60;

                int sec= (int) secLeft/1000;
                int minutes =  (sec / SECONDS_IN_A_MINUTE);
                sec -= minutes * SECONDS_IN_A_MINUTE;

                int hours = minutes / MINUTES_IN_AN_HOUR;
                minutes -= hours * MINUTES_IN_AN_HOUR;
                fulltime= (int) secLeft;

                new CountDownTimer(500, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        Log.e("imagecount", ""+img_count);
                        if(img_count>0){

                        }else {
                            if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){
                                startanim();
                            }
                            startTime = questions.length() * 30 * 1000;
                            changeParagraph(-1);
                           // startTimer();

                            //  startVerification();;
                            // checkfacedetection();
                            if(quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                           /* anim_frame.setVisibility(View.VISIBLE);
                            animarrow1.startAnimation(slideUpAnimation);
                            animarrow2.startAnimation(slideUpAnimation);*/
                        }
                    }
                }.start();


            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        qs.clear();
                        try {
                            if (quesLoading.isShowing()) {
                                quesLoading.dismiss();
                            }
                        }catch (Exception e){

                        }
                        try {
                            if (alertDialog.isShowing()) {

                            } else {
                                showGetQuesAgainDialogue();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            showGetQuesAgainDialogue();
                        }
                    }
                });
                Log.e("QUES", "Error parsing data " + e.toString());
            }




        }
    }
    void startTimer() {
        countDownTimer.start();
    }

    private void changeParagraph(final int acq) {


        para_count++;


        if(para_count>=3){


            Log.e("calledchangepara",""+para_count);
            try {

                countDownTimer.cancel();
               // countDownTimer=null;
            }
            catch (Exception e){

                e.printStackTrace();
            }
            int i =para_count-2;
            String message="";
            String complete_msg = "";
            String total_count = "";

            switch (audios.length()){

                case 1 :

                    total_count = "out of one master slave question";
                    break;

                case 2 :

                    total_count = "out of two master slave questions";
                    break;

                case 3 :

                    total_count = "out of three master slave questions";
                    break;

                case 4 :

                    total_count = "out of four master slave questions";
                    break;
                case 5 :

                    total_count = "out of five master slave questions";
                    break;
                case 6 :

                    total_count = "out of six master slave questions";
                    break;

                case 7 :

                    total_count = "out of seven master slave questions";
                    break;
                case 8 :

                    total_count = "out of eight master slave questions";
                    break;
                case 9 :

                    total_count = "out of nine master slave questions";
                    break;
                case 10 :

                    total_count = "out of ten master slave questions";
                    break;
                case 11 :

                    total_count = "out of eleven master slave questions";
                    break;
                case 12 :

                    total_count = "out of twelve master slave questions";
                    break;
                case 13 :

                    total_count = "out of thirteen master slave questions";
                    break;
                case 14 :

                    total_count = "out of fourteen master slave questions";
                    break;
                case 15 :

                    total_count = "out of fifteen master slave questions";
                    break;
                case 16 :

                    total_count = "out of sixteen master slave questions";
                    break;
                case 17 :

                    total_count = "out of seventeen master slave questions";
                    break;
                case 18 :

                    total_count = "out of eighteen master slave questions";
                    break;
                case 19 :

                    total_count = "out of nineteen master slave questions";
                    break;
                case 20 :

                    total_count = "out of twenty master slave questions";
                    break;
            }
                switch (para_count){

                    case 3:
                         message = "You have completed your first master slave question";
                         complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 4:
                         message = "You have completed your second master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 5:
                        message = "You have completed your third master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 6:
                        message = "You have completed your fourth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 7:
                        message = "You have completed your fifth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 8:
                        message = "You have completed your sixth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 9:
                        message = "You have completed your seventh master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 10:
                        message = "You have completed your eighth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 11:
                        message = "You have completed your ninth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 12:
                        message = "You have completed your tenth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 13:
                        message = "You have completed your eleventh master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 14:
                        message = "You have completed your twelfth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 15:
                        message = "You have completed your thirteenth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 16:
                        message = "You have completed your fifteenth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 17:
                        message = "You have completed your sixteenth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 18:
                        message = "You have completed your seventeenth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 19:
                        message = "You have completed your eighteenth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 20:
                        message = "You have completed your nineteenth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                    case 21:
                        message = "You have completed your twentieth master slave question";
                        complete_msg = message + " " + total_count;
                        showProceedDialog(complete_msg);
                        break;
                }


//            showProceedDialog("You have completed your first audio questions");
        }


        Log.e("qc is",String.valueOf(qc));
        if (acq == -1) {
            changeParagraph(0);
            changeQues();
            return;
        }else {

            //   ObjectAnimator.ofInt(main_scroller, "scrollY",  main_scroller.getTop()).setDuration(10).start();
            Log.e("acq", String.valueOf(acq));
            if(assets[acq].contains("7769896014")){
                String [] valuesare=assets[acq].split("7769896014");
                String imgis=valuesare[0];
                String parais=valuesare[1];
                final Animation animation_fade =
                        AnimationUtils.loadAnimation(MSQ.this,
                                R.anim.fade_in);


                String[] pic = imgis.split("/");
                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                File image_file = new File(folder, pic[pic.length - 1]);
                path_image=image_file.getPath();
                final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());


                //  paragraph.startAnimation(animation_fade);


                paragraph.setText(parais);
                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.VISIBLE);
                        paragraph.setVisibility(View.VISIBLE);

                        para_scroller.scrollTo(0,para_scroller.getTop());

                        para_image.startAnimation(animation_fade);

                        paragraph.startAnimation(animation_fade);
                        para_image.setImageBitmap(bmp);
                    }
                }.start();
            }else
            if ((assets[acq].contains(".jpg")) || (assets[acq].contains(".jpeg")) || (assets[acq].contains(".png"))) {
                paragraph.setVisibility(View.GONE);



                String[] pic = assets[acq].split("/");
                String last = pic[pic.length - 1];
                String[] lastname = last.split("\\.");
                String name = lastname[0];
                File image_file = new File(folder, pic[pic.length - 1]);
                path_image=image_file.getPath();
                final Bitmap bmp = BitmapFactory.decodeFile(image_file.getPath());
                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.VISIBLE);
                        para_scroller.scrollTo(0,para_scroller.getTop());

                        final Animation animation_fade =
                                AnimationUtils.loadAnimation(MSQ.this,
                                        R.anim.fade_in);
                        para_image.startAnimation(animation_fade);

                        para_image.setImageBitmap(bmp);

                    }
                }.start();
            } else {


                new CountDownTimer(600, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                        para_image.setVisibility(View.GONE);


                        paragraph.setVisibility(View.VISIBLE);
                        para_scroller.scrollTo(0,para_scroller.getTop());

                        final Animation animation_fade =
                                AnimationUtils.loadAnimation(MSQ.this,
                                        R.anim.fade_in);
                        paragraph.startAnimation(animation_fade);
                        paragraph.setText(assets[acq]);

                    }
                }.start();
            }
        }


     /*   new CountDownTimer(500, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                main_scroller.post(new Runnable() {
                    @Override
                    public void run() {
                        ObjectAnimator.ofInt(main_scroller, "scrollY",  main_scroller.getTop()).setDuration(10).start();
                        scrollx=main_scroller.getScrollX();
                        scrolly=main_scroller.getScrollY();
                    }
                });
                flag_scroll=false;
                arrow1.setImageResource(R.drawable.arrowup);
                arrow2.setImageResource(R.drawable.arrowup);
            }
        }.start();*/
    }

    private void showProceedDialog(String msg ) {
            //countDownTimer.cancel();
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    countDownTimer.cancel();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();

        try {
                proceed_dialog = new Dialog(MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
            proceed_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            proceed_dialog.setContentView(R.layout.congo_msq);
                Window window = proceed_dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.CENTER;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
            proceed_dialog.setCancelable(false);

            proceed_dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);




                final FontTextView yes = (FontTextView) proceed_dialog.findViewById(R.id.yes_quit);

                FontTextView msgs = (FontTextView)proceed_dialog.findViewById(R.id.txt);
                msgs.setText(msg);

            final LinearLayout banner=(LinearLayout) proceed_dialog.findViewById(R.id.banner_level_page);

            banner.setVisibility(View.GONE);

                    Animation scaleAnimation = new ScaleAnimation(
                            0f, 1f, // Start and end values for the X axis scaling
                            0f, 1f, // Start and end values for the Y axis scaling
                            Animation.REVERSE, 0.5f, // Pivot point of X scaling
                            Animation.REVERSE, 0.5f);
                    scaleAnimation.setDuration(500);


                    banner.startAnimation(scaleAnimation);
                    new CountDownTimer(500, 100) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            banner.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFinish() {

                        }
                    }.start();




                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        yes.setEnabled(false);
                        Animation scaleAnimation = new ScaleAnimation(
                                1f, 0f, // Start and end values for the X axis scaling
                                1f, 0f, // Start and end values for the Y axis scaling
                                Animation.REVERSE, 0.5f, // Pivot point of X scaling
                                Animation.REVERSE, 0.5f);
                        scaleAnimation.setDuration(500);


                        banner.startAnimation(scaleAnimation);
                        new CountDownTimer(400,400) {


                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                close.setEnabled(true);
                                proceed_dialog_showing = false;
                                proceed_dialog.dismiss();

                              //  countDownTimer.cancel();

                                startTime= Long.parseLong(qsmodel.getTime());
                                startTime=startTime*1000;
                                Log.e("timeris",qsmodel.getTime());
                                countDownTimer = new MyCountDownTimer(startTime, interval);
                                startTimer();



                            }
                        }.start();


                    }
                });

                try {
                    proceed_dialog.show();
                    proceed_dialog_showing = true;

                }catch (Exception e){}
            }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void changeQues() {
        c = Calendar.getInstance();
        df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        now = df1.format(c.getTime()).toString();

        if (qc < qs.size()) {

            Log.e("qc size",String.valueOf(qc));
            Log.e("qs size",String.valueOf(qs.size()));
            qsmodel=qs.get(qc);

//            if(qc==2){
//                takePicture();
//            }
            if (audio.length > 1) {
                if (no_of_ques_in_list == audio[cnt]) {
                    changeParagraph(cnt + 1);
                    cnt++;
                    no_of_ques_in_list = 0;

                    Log.e("cnt",""+cnt);

                    direction="bottom";
                }
                //no_of_ques_in_list++;
            }


          /*  for(int i=0;i<qid_array.size();i++){
                if(qsmodel.getQ_id().equalsIgnoreCase(qid_array.get(i))){
                    flag_is_present_id=true;
                }
            }*/


            if(!flag_is_present_id) {

                if(qc==0) {
                    qno.setText(qc + 1 + "/" + qs.size());
                }

                Log.e("valuesis",String.valueOf(Float.parseFloat(String.valueOf(qc+1))));
                try {
                    //single question
                    if(flag_click) {
//                        final Animation animation_fade = AnimationUtils.loadAnimation(MSQ.this, R.anim.fade_in);
//
//                         rc_options_non_clickable.startAnimation(animation_fade);
//
                        //animListdata();
//                        main_ques_layout_window.startAnimation(ques_anim_left);
//                        Log.e("animstatus", "putdata");
//                        temp_ques_layout_window.startAnimation(ques_anim);
//                        hideArrow();
//                        temp_ques_layout_window.setVisibility(View.VISIBLE);
                    }
                    newquid = qsmodel.getQ_id();
                    optioncard = new ArrayList<>();
                    optioncard.add("Q: "+qsmodel.getQuestion());
                    if (!qsmodel.getOpt1().isEmpty() && !qsmodel.getOpt1().equals("null")) {
                        String s1 = qsmodel.getOpt1();
                        s1 = s1.trim();
                        optioncard.add(""+s1);
                    }
                    if (!qsmodel.getOpt2().isEmpty() && !qsmodel.getOpt2().equals("null")) {
                        String s2 = qsmodel.getOpt2();
                        s2 = s2.trim();
                        optioncard.add(""+s2);
                    }
                    if (!qsmodel.getOpt3().isEmpty() && !qsmodel.getOpt3().equals("null")) {
                        String s3 = qsmodel.getOpt3();
                        s3 = s3.trim();
                        optioncard.add(""+s3);
                    }
                    if (!qsmodel.getOpt4().isEmpty() && !qsmodel.getOpt4().equals("null")) {
                        String s4 = qsmodel.getOpt4();
                        s4 = s4.trim();
                        optioncard.add(""+s4);
                    }
                    if (!qsmodel.getOpt5().isEmpty() && !qsmodel.getOpt5().equals("null")) {
                        String s5 = qsmodel.getOpt5();
                        s5 = s5.trim();
                        optioncard.add(""+s5);
                    }

                    crcans = qsmodel.getCorrectanswer();

                    switch (crcans) {
                        case "A":
                            crcopt = 0;
                            break;
                        case "B":
                            crcopt = 1;
                            break;
                        case "C":
                            crcopt = 2;
                            break;
                        case "D":
                            crcopt = 3;
                            break;
                        case "E":
                            crcopt = 4;
                            break;
                        default:
                            System.out.println("Not in 10, 20 or 30");
                    }
                    crct = crcopt;

                    qid = Integer.parseInt(qsmodel.getQ_id());


                    option_clickable =new Option_clickable(this,optioncard);
                    //rc_options_non_clickable_temp.setAdapter(option_clickable);
                    new CountDownTimer(700, 700) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            rc_options_non_clickable.setAdapter(option_clickable);

//                            recyclerFastScroller.attachRecyclerView(rc_options_non_clickable);
//                            recyclerFastScroller.attachAdapter(option_clickable);


                            final Animation animation_fade = AnimationUtils.loadAnimation(MSQ.this, R.anim.fade_in);

                            rc_options_non_clickable.startAnimation(animation_fade);
                            startTime= Long.parseLong(qsmodel.getTime());
                            startTime=startTime*1000;
                            Log.e("timeris",qsmodel.getTime());
                            countDownTimer = new MyCountDownTimer(startTime, interval);
                            startTimer();
                        }
                    }.start();
                    Log.e("timimg","adapter load");

                    String[] options_array={"A","B","C","D","E","F","G","H","I","J","K","L","M"};
                    int no_options=optioncard.size();
                    String [] confirm_options=new String[no_options-1];

                    for(int i=0;i<no_options-1;i++){
                        confirm_options[i]=options_array[i];
                    }
                    //   rc_options.setAdapter(new NewOptionsAdapter(this,confirm_options));
                    if(flag_click) {
                        new CountDownTimer(700, 700) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                //  temp_ques_layout_window.setVisibility(View.VISIBLE);

                            }

                            @Override
                            public void onFinish() {
                                //   rc_options_non_clickable.removeOnItemTouchListener(disabler);
                                if(qc!=0)
                                {
                                    qno.setText(qc + 1 + "/" + qs.size());
                                }
                                //  ques_scrollview.setVerticalScrollBarEnabled(true);


                            }
                        }.start();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else {
                //Collections.shuffle(qs);
               // changeQues();
                //shuffle
            }







        } else {

            try{
                dialoge.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }

            if (qansr > 0) {
                tres = formdata(qansr);
                checkonp = false;
                countDownTimer.cancel();
                scoreActivityandPostscore();
            } else {
                tres = formdata();
                checkonp = false;
                countDownTimer.cancel();
                scoreActivityandPostscore();
            }
        }
    }

    public String readStream(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qus = sb.toString();


            Log.e("JSONStrr", Qus);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qus;
    }
    public String formdata() {
        String jn = "";
        try{

            JSONObject student1 = new JSONObject();
            student1.put("U_Id", uidd);
            student1.put("S_Id", sid);
            student1.put("Q_Id", newquid);
            student1.put("TG_Id", tgid);
            student1.put("SD_UserAnswer", "E");
            student1.put("SD_StartTime", Calendar.getInstance().getTime().toString());
            student1.put("SD_EndTime", Calendar.getInstance().getTime().toString());


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(student1);


            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn = studentsObj.toString();
        } catch(Exception e)

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jn;
    }

    public String formdata(int anan){
        String jn = "";

        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < anan; i++) {
                JSONObject student = new JSONObject();
                student.put("U_Id", uidd);
                student.put("S_Id", sid);
                student.put("Q_Id", qidj.get(i));
                student.put("TG_Id", tgid);
                student.put("SD_UserAnswer", ansj.get(i));
                student.put("SD_StartTime", stj.get(i));
                student.put("SD_EndTime", endj.get(i));
                jsonArray.put(student);
            }

            JSONObject studentsObj = new JSONObject();
            studentsObj.put("data", jsonArray);
            jn=studentsObj.toString();
            Log.e("jsonpostSTring",jn);
        }catch (Exception e){
            e.printStackTrace();
        }

        return jn;

    }

    //detection
    private void scoreActivityandPostscore() {
        //       stop();
        ConnectionUtils connectionUtils=new ConnectionUtils(MSQ.this);
        if(connectionUtils.isConnectionAvailable()) {



            if (piccapturecount != 0) {
                try {
                    if (!verification_progress.isShowing() || verification_progress != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                verification_progress.show();
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                verify();
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        verification_progress.show();
                    }
                });
                // new ImageSend().execute();
                new PostAns().execute();



//                Intent it = new Intent(this, CheckP.class);
//                it.putExtra("qt", questions.length() + "");
//                it.putExtra("cq", cqa + "");
//                it.putExtra("po", score + "");
//                it.putExtra("assessg",assessg);
//                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(it);
//                finish();

            }
//            if(i+1==piccapturecount) {
//                new ImageSend().execute();
//            }



        }else {
            showretryDialogue();
        }


    }
    public void showretryDialogue() {
        Log.e("respmsg","dialogue");

        /*final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                MSQ.this, R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(MSQ.this);
        View dialogView = inflater.inflate(R.layout.retry_anim_dialogue, null);

        dialogBuilder.setView(dialogView);


        alertDialog = dialogBuilder.create();


//                            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);

        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

        alertDialog.show();*/
        alertDialog = new Dialog(MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.retry_anim_dialogue);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        alertDialog.setCancelable(false);

        alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        final FontTextView errormsgg = (FontTextView)alertDialog.findViewById(R.id.erromsg);

        errormsgg.setText("Please check your Internet Connection...!");

        final ImageView astro = (ImageView)alertDialog.findViewById(R.id.astro);
        final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);
        if((batLevel>20&&!appConstant.getDevice().equals("motorola Moto G (5S) Plus")&&!isPowerSaveMode)||batLevel==0){

            astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);
            astro_outx.setDuration(300);
            astro_outx.setInterpolator(new LinearInterpolator());
            astro_outx.setStartDelay(0);
            astro_outx.start();
            astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
            astro_outy.setDuration(300);
            astro_outy.setInterpolator(new LinearInterpolator());
            astro_outy.setStartDelay(0);
            astro_outy.start();


            alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
            alien_outx.setDuration(300);
            alien_outx.setInterpolator(new LinearInterpolator());
            alien_outx.setStartDelay(0);
            alien_outx.start();
            alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
            alien_outy.setDuration(300);
            alien_outy.setInterpolator(new LinearInterpolator());
            alien_outy.setStartDelay(0);
            alien_outy.start();
        }
        final ImageView yes = (ImageView)alertDialog.findViewById(R.id.retry_net);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                yes.setEnabled(false);
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        yes.setEnabled(true);
                    }
                }.start();
                new CountDownTimer(400, 400) {


                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {


                        alertDialog.dismiss();
                        ConnectionUtils connectionUtils=new ConnectionUtils(MSQ.this);
                        if(connectionUtils.isConnectionAvailable()){
                            scoreActivityandPostscore();
                        }else {assess_progress.show();
                            new CountDownTimer(2000, 2000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if(assess_progress.isShowing()&&assess_progress!=null){
                                        assess_progress.dismiss();
                                    }
                                    showretryDialogue();
                                }
                            }.start();
                        }


                    }
                }.start();




            }
        });

        try {
            if(!alertDialog.isShowing())
                alertDialog.show();
        }
        catch (Exception e){

        }
    }

    private class PostAns extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1 = "";
            try {

                Log.e("datais","ip: "+AppConstant.getLocalIpAddressApp()+" os: "+AppConstant.getAndroidVersionApp()
                        +" version: "+AppConstant.getversionofApp());

                try {


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/Answers");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            tres.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                    urlConnection.setRequestProperty("app_version",AppConstant.getversionofApp());
                    urlConnection.setRequestProperty("app_os",AppConstant.getAndroidVersionApp());
                    urlConnection.setRequestProperty("app_ip",AppConstant.getLocalIpAddressApp());
                    urlConnection.setRequestProperty("app_latlong",location.getLatitude()+","+location.getLongitude());

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(tres);
                    Log.e("resp_data",tres);
                    wr.flush();
                    is = urlConnection.getInputStream();
                    code2 = urlConnection.getResponseCode();
                    Log.e("Response1", ""+code2);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (code2 == 200) {

                    String responseString = readStream(is);
                    Log.v("Response1", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(code2==200) {

                new Ends().execute();
            }
            else{

                try{
                    verification_progress.dismiss();
                    disable_option();
                }catch (Exception e){
                    e.printStackTrace();
                }

                Intent it = new Intent(MSQ.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
        }
    }


    private class Ends extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            if(new PrefManager(MSQ.this).getGetFirsttimegame().equalsIgnoreCase("first")){
                new PrefManager(MSQ.this).saveFirsttimeGame("last");
            }else if(new PrefManager(MSQ.this).getGetFirsttimegame().equalsIgnoreCase("last")){

            }else {
                new PrefManager(MSQ.this).saveFirsttimeGame("first");
            }


        }

        ///Authorization
        @Override
        protected String doInBackground(String... urlkk) {
            String result1="";
            try {


                try {

                    String jn = new JsonBuilder(new GsonAdapter())
                            .object("data")
                            .object("U_Id", uidd)
                            .object("S_Id", sid)
                            .object("course_id", Integer.valueOf(new PrefManager(MSQ.this).getcourseid_ass()))
                            .object("lg_id",new PrefManager(MSQ.this).getlgid_ass())
                            .object("certification_id",new PrefManager(MSQ.this).getcertiid_ass())
                            .build().toString();


                    URL urlToRequest = new URL(AppConstant.Ip_url+"Player/EndSession");
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setFixedLengthStreamingMode(
                            jn.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Authorization", "Bearer " +token);

                    urlConnection.setRequestMethod("POST");


                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(jn);
                    wr.flush();

                    code3 = urlConnection.getResponseCode();
                    is = urlConnection.getInputStream();
                    Log.v("Response2", ""+code3);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                verification_progress.dismiss();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if(code3==200){
                    String responseString = readStream1(is);
                    Log.e("Response2", ""+responseString);
                    result1 = responseString;


                }
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return result1;

        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                verification_progress.dismiss();
                disable_option();
                Log.e("result", ""+result);

            }catch (Exception e){
                e.printStackTrace();
            }

            if(code3!=200){

                Toast.makeText(getApplicationContext(),"Error while submitting answers", Toast.LENGTH_SHORT).show();
                Intent it = new Intent(MSQ.this,HomeActivity.class);
                it.putExtra("tabs","normal");
                it.putExtra("assessg",getIntent().getExtras().getString("assessg"));
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();
            }
            else{
                Intent it = new Intent(MSQ.this, CheckP.class);
                it.putExtra("qt", questions.length() + "");
                it.putExtra("cq", cqa + "");
                it.putExtra("po", score + "");
                it.putExtra("assessg",assessg);
                //   it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(it);
                overridePendingTransition(R.anim.fade_in_medium, R.anim.fade_out_medium);
                finish();

            }

        }
    }
    public String readStream1(InputStream in) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            Qs = sb.toString();


            Log.e("JSONStrr", Qs);

        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        return Qs;
    }

    @Override
    protected void onPause() {
        super.onPause();
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);
        if (pauseCount == 0 && checkonp) {
            try {
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (dialoge != null && dialoge.isShowing()) {
                    dialoge.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  vdo_performance_ques_timer.cancel();
            close.setEnabled(false);


            try {
                if (!close_on_get_ques && !proceed_dialog.isShowing() && !proceed_dialog_showing && !alert_on_close.isShowing() && !alert_onclose_showing) {

              /*      AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MSQ.this, R.style.DialogTheme);
// ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = LayoutInflater.from(MSQ.this);
                    View dialogView = inflater.inflate(R.layout.quit_warning, null);
                    dialogBuilder.setView(dialogView);


                    alertDialog = dialogBuilder.create();

                    alertDialog.setCancelable(false);
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

                    alertDialog.show();
*/
                    alertDialog = new Dialog(MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.assess_quit_warning);
                    Window window = alertDialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alertDialog.setCancelable(false);

                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                    if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);

                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();


                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);

                                    alertDialog.dismiss();
                                //    changeQues();
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            close.setEnabled(true);
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();

                                        countDownTimer.cancel();
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();

                                        countDownTimer.cancel();
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();
                        }
                    });

                    try {
                        alertDialog.show();

                    } catch (Exception e) {

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                if (!close_on_get_ques && !alert_onclose_showing) {

        /*        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MSQ.this, R.style.DialogTheme);
// ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = LayoutInflater.from(MSQ.this);
                View dialogView = inflater.inflate(R.layout.quit_warning, null);
                dialogBuilder.setView(dialogView);


                alertDialog = dialogBuilder.create();

                alertDialog.setCancelable(false);
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);


                try {
                    alertDialog.show();
                }catch (Exception ex){
                    ex.printStackTrace();
                }*/

                    alertDialog = new Dialog(MSQ.this, android.R.style.Theme_Translucent_NoTitleBar);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.assess_quit_warning);
                    Window window = alertDialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();

                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    window.setAttributes(wlp);
                    alertDialog.setCancelable(false);

                    alertDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    FontTextView msg = (FontTextView)alertDialog.findViewById(R.id.msg);
                    msg.setText(quit_msg);
                    final ImageView astro = (ImageView) alertDialog.findViewById(R.id.astro);
                    final ImageView alien = (ImageView) alertDialog.findViewById(R.id.alien);

                    if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -400f, 0f);

                        astro_outx.setDuration(300);
                        astro_outx.setInterpolator(new LinearInterpolator());
                        astro_outx.setStartDelay(0);
                        astro_outx.start();
                        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 700f, 0f);
                        astro_outy.setDuration(300);
                        astro_outy.setInterpolator(new LinearInterpolator());
                        astro_outy.setStartDelay(0);
                        astro_outy.start();


                        alien_outx = ObjectAnimator.ofFloat(alien, "translationX", 400f, 0f);
                        alien_outx.setDuration(300);
                        alien_outx.setInterpolator(new LinearInterpolator());
                        alien_outx.setStartDelay(0);
                        alien_outx.start();
                        alien_outy = ObjectAnimator.ofFloat(alien, "translationY", 700f, 0f);
                        alien_outy.setDuration(300);
                        alien_outy.setInterpolator(new LinearInterpolator());
                        alien_outy.setStartDelay(0);
                        alien_outy.start();

                    }
                    final ImageView no = (ImageView) alertDialog.findViewById(R.id.no_quit);
                    final ImageView yes = (ImageView) alertDialog.findViewById(R.id.yes_quit);

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            no.setEnabled(false);
                            yes.setEnabled(false);
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    close.setEnabled(true);

                                    alertDialog.dismiss();
                              //      changeQues();
                                }
                            }.start();


                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            yes.setEnabled(false);
                            no.setEnabled(false);
                            close.setEnabled(true);
                            if ((batLevel > 20 && !appConstant.getDevice().equals("motorola Moto G (5S) Plus") && !isPowerSaveMode) || batLevel == 0) {
                                astro_inx = ObjectAnimator.ofFloat(astro, "translationX", 0f, -400f);
                                astro_inx.setDuration(300);
                                astro_inx.setInterpolator(new LinearInterpolator());
                                astro_inx.setStartDelay(0);
                                astro_inx.start();
                                astro_iny = ObjectAnimator.ofFloat(astro, "translationY", 0f, 700f);
                                astro_iny.setDuration(300);
                                astro_iny.setInterpolator(new LinearInterpolator());
                                astro_iny.setStartDelay(0);
                                astro_iny.start();

                                alien_inx = ObjectAnimator.ofFloat(alien, "translationX", 0f, 400f);
                                alien_inx.setDuration(300);
                                alien_inx.setInterpolator(new LinearInterpolator());
                                alien_inx.setStartDelay(0);
                                alien_inx.start();
                                alien_iny = ObjectAnimator.ofFloat(alien, "translationY", 0f, 700f);
                                alien_iny.setDuration(300);
                                alien_iny.setInterpolator(new LinearInterpolator());
                                alien_iny.setStartDelay(0);
                                alien_iny.start();
                            }
                            new CountDownTimer(400, 400) {


                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    if (qansr > 0) {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();

                                        countDownTimer.cancel();
                                        tres = formdata(qansr);
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    } else {
                                        close.setEnabled(true);

                                        alertDialog.dismiss();

                                        countDownTimer.cancel();
                                        tres = formdata();
                                        checkonp = false;
                                        scoreActivityandPostscore();

                                    }
                                }
                            }.start();
                        }
                    });
                    try {
                        alertDialog.show();
                    } catch (Exception ex) {
                    }
                }
            }
            } else{
                if (checkonp) {
                    Log.e("PAUSE", checkonp + "");
                    countDownTimer.cancel();
                    if (qansr > 0) {
                        tres = formdata(qansr);
                        checkonp = false;
                        scoreActivityandPostscore();
                    } else {
                        tres = formdata();
                        checkonp = false;
                        scoreActivityandPostscore();
                    }
                } else {

                }
            }

    }
    private final BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int position= Integer.parseInt(intent.getStringExtra("pos"));


            if(position==1000){
//                temp_ques_layout_window.setVisibility(View.GONE);
            }else {
                Log.e("broad", "msgget" +position);
                clickMethods(position);
                pos_clicked=position;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        try {
            flag_click=false;
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mReceiver);
//        try{
//            timer1.cancel();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        try {

            if(dialog.isShowing()){
                dialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {

            if(dialoge.isShowing()){
                dialoge.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {

            if(quesLoading.isShowing()&& quesLoading !=null){
                quesLoading.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void downloadData(String uri, String substring) {

        int task= dm.addTask(substring, uri, 12, folder.getPath(), true,false);
        try {

            dm.startDownload(task);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onBackPressed() {

    }
    private  class S3getimageurl extends AsyncTask<Void,Void,Void> {
        URL image1url, image2url,image3url;
        @Override
        protected Void doInBackground(Void... params) {
            String ACCESS_KEY ="AKIAJQTIZUYTX2OYTBJQ";
            String SECRET_KEY = "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";

            Random ramd= new Random();

            AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY,SECRET_KEY));

            Log.e("outsidecond",String.valueOf(count_detect_no));

            if (autocapimgloc.get(auto_capt_count) != null) {
                autocapimg1loc1 = autocapimgloc.get(auto_capt_count);
                int innd = ramd.nextInt(100000);
                File pic1 = new File(autocapimg1loc1);
                PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg", pic1);
                PutObjectResult putResponse = s3Client.putObject(pp);
                String prurl = s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                image1url = s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
                img1 = image1url.toString();
                Log.e("AMAZON RESPONSE", img1);
                replacedurl = img1.replace("https://valyouinputbucket.s3.amazonaws.com/","https://valyouinputbucket.s3.ap-south-1.amazonaws.com/" );

                auto_capt_count++;
            }
            else {
                img1 = "";
                auto_capt_count++;
            }
            imgurllist.add(replacedurl);
            Log.e("detectis in s3",String.valueOf(count_detect_no));
            count_detect_no++;

            verify();
            // i++;


//            if(!autocapimgloc.isEmpty()) {
//                if (autocapimgloc.get(0) != null) {
//                    autocapimg1loc1 = autocapimgloc.get(0);
//                    int innd = ramd.nextInt(100000);
//                    File pic1 = new File(autocapimg1loc1);
//                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg", pic1);
//                    PutObjectResult putResponse = s3Client.putObject(pp);
//                    String prurl = s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
//                    image1url = s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd + ".jpg");
//                    img1 = image1url.toString();
//                    Log.e("AMAZON RESPONSE", img1);
//                } else {
//                    img1 = "";
//                }
//                if (autocapimgloc.get(1) != null) {
//                    autocapimg1loc2 = autocapimgloc.get(1);
//
//                    int innd2 = ramd.nextInt(100000);
//                    File pic2 = new File(autocapimg1loc2);
//                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/" + innd2 + ".jpg", pic2);
//                    PutObjectResult putResponse = s3Client.putObject(pp);
//                    String prurl = s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd2 + ".jpg");
//                    image2url = s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd2 + ".jpg");
//                    img2 = image2url.toString();
//                    Log.e("AMAZON RESPONSE", img2);
//
//                } else {
//                    img2 = "";
//                }
//                if (autocapimgloc.get(2) != null) {
//                    autocapimg1loc3 = autocapimgloc.get(3);
//
//                    int innd3 = ramd.nextInt(100000);
//                    File pic3 = new File(autocapimg1loc3);
//                    PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/" + innd3 + ".jpg", pic3);
//                    PutObjectResult putResponse = s3Client.putObject(pp);
//                    String prurl = s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd3 + ".jpg");
//                    image3url = s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/" + innd3 + ".jpg");
//                    img3 = image3url.toString();
//                    Log.e("AMAZON RESPONSE", img3);
//
//                } else {
//                    img3 = "";
//                }
//            }
//            if(autocapimg1loc1!=null){
//                int innd= ramd.nextInt(100000);
//                File pic1 = new File(autocapimg1loc1);
//                PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/"+innd+".jpg",pic1);
//                PutObjectResult putResponse = s3Client.putObject(pp);
//                String prurl= s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd+".jpg");
//                image1url= s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd+".jpg");
//                img1 = image1url.toString();
//                Log.e("AMAZON RESPOMSE",image1url.toString());
//            }
//            else{
//                img1 = "";
//            }
//
//            if(autocapimg1loc2!=null){
//                int innd2= ramd.nextInt(100000);
//                File pic2 = new File(autocapimg1loc2);
//                PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/"+innd2+".jpg",pic2);
//                PutObjectResult putResponse = s3Client.putObject(pp);
//                String prurl= s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd2+".jpg");
//                image2url= s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd2+".jpg");
//                img2 = image2url.toString();
//                Log.e("AMAZON RESPOMSE",image2url.toString());
//
//            }
//            else{
//                img2 = "";
//            }
//            if(autocapimg1loc3!=null){
//                int innd3= ramd.nextInt(100000);
//                File pic3 = new File(autocapimg1loc3);
//                PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/"+innd3+".jpg",pic3);
//                PutObjectResult putResponse = s3Client.putObject(pp);
//                String prurl= s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd3+".jpg");
//                image3url= s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd3+".jpg");
//                img3 = image3url.toString();
//                Log.e("AMAZON RESPOMSE",image3url.toString());
//
//            }
//            else{
//                img3= "";
//            }
//        File pic = new File(mImageFileLocation);
//        PutObjectRequest pp = new PutObjectRequest("valyouinputbucket", "valyou/facerecognition/images/"+innd+".jpg",pic);
//        PutObjectResult putResponse = s3Client.putObject(pp);
//        String prurl= s3Client.getResourceUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd+".jpg");
//        ppp= s3Client.getUrl("valyouinputbucket", "valyou/facerecognition/images/"+innd+".jpg");
//        Log.e("AMAZON RESPOMSE",ppp.toString());
            return  null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  new SExample(ppp.toString()).execute();
            //GameModel gameModel = new GameModel();
            //String params="U_Id="+candidateId+"&g_id="+12+"&image_url="+image1url+","+image2url+","+image3url+"&fr_status="+0+"&assesment_name="+ "FIB"+"&assesment_id="+2+"&company_name="+"TATACOMMUNICATION";



            // Log.e("params" , params);
            // new ImageSend().execute();

        }
    }
    public class ImageSend extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            String ref_id;
            HttpURLConnection urlConnection = null;
            String result = "";

            String responseString="";


            try {


                JSONObject child = new JSONObject();
                child.put("U_Id",candidateId);
                child.put("GD_Id",gdid);
                child.put("fr_status","0");
                child.put("assesment_name", game);
                child.put("CE_Id", ceid);
                //= child.put("company_name",HomeActivity.tg_group);
                JSONArray jsonArray = new JSONArray();
                for(int i=0;i<imgurllist.size();i++){
                    jsonArray.put(imgurllist.get(i));
                }
                JSONObject mainobj=new JSONObject();
                child.put("image_url",jsonArray);
                mainobj.put("data",child);


                Log.e("datais",mainobj.toString());
                URL urlToRequest = new URL(AppConstant.Ip_url + "Player/UploadFrImages");
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();


                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                urlConnection.setRequestMethod("POST");




                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(mainobj.toString());
                wr.flush();

                responseString = readStream1(urlConnection.getInputStream());
                Log.e("Response", responseString);
                result=responseString;



            }
            catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(urlConnection != null)
                    urlConnection.disconnect();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            //jsonParsing(s);
            super.onPostExecute(s);
            Log.e("datais","post");
            new PostAns().execute();
//            Intent it = new Intent(MasterSlaveGame.this, CheckP.class);
//            it.putExtra("qt", questions.length() + "");
//            it.putExtra("cq", cqa + "");
//            it.putExtra("po", score + "");
//            it.putExtra("assessg",assessg);
//            it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(it);
//            finish();



        }

        private String readStream1(InputStream in) {

            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
    private void startVerification() {
        camera = getCameraInstance();

        waitTimer = new CountDownTimer(60000, 3) {


            @SuppressLint("SimpleDateFormat")
            @Override
            public void onTick(long l) {


            }

            public void onFinish() {

                stop();

            }



        }.start();

    }
    private Camera getCameraInstance() {
        Camera camera = null;
        if (!getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //   Toast.makeText(this, "No camera on this device", Toast.LENGTH_LONG)
            //    .show();
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId < 0) {
                //  Toast.makeText(this, "No front facing camera found.",
                //       Toast.LENGTH_LONG).show();
            } else {

                try {
                    camera = Camera.open(cameraId);
                    mPreview = new CameraPreview(this, camera);
                    preview.addView(mPreview);
                    onClick();

                } catch (Exception e) {
                    // cannot get camera or does not exist
                }
            }
        }

        return camera;
    }
    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d("status", "Camera found");
                cameraId = i;
                break;
            }else {
                Log.e("Status","No front camera found");
            }
        }
        return cameraId;
    }
    public void onClick() {


        timer1 = new Timer();
        timer1.schedule(new TimerTask()
        {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        camera.setDisplayOrientation(90);
                        camera.startPreview();
                        camera.takePicture(null, null, new PhotoHandler(getApplicationContext()));


                    }
                });
            }
        }, 2000, 9000);


    }
    public void stop(){
        preview.setVisibility(View.GONE);

//        waitTimer.cancel();


        try {
            timer1.cancel();
        }catch (Exception e){

        }

    }
    public static Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    private void detectAndFrameother1(Bitmap imageBitmap) {


        if(imageBitmap.getWidth()>imageBitmap.getHeight()){
            imageBitmap=rotateBitmap(imageBitmap,-90);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        //   Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        //if face is not detected in image
                        if (result != null && result.length == 0) {
                            //   Toast.makeText(getApplicationContext(),"other face is not detected",Toast.LENGTH_SHORT).show();
                            // detectedfaceid.add(UUID.fromString(""));
                            facedetectedimagelocation.add(count_detect_no,"");
                            Log.e("outside",String.valueOf(count_detect_no)+"nondetect");
                            count_detect_no++;
                            verify();
                        }

                        //if face is detected in image
                        else {
                            //     Toast.makeText(getApplicationContext(), "other face is  detected", Toast.LENGTH_SHORT).show();

                            Log.e("outside",String.valueOf(count_detect_no)+"detect");
                            //imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                            //  Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                            // imageBitmap.recycle();


                            //imagelocation.add(mImageFileLocation);


                            //    Log.e("imglocation", mImageFileLocation);
//                            switch (piccapturecount){
//
//                                case 1:
//                                    verifiedimagelocation.add(imagelocation.get(0));
//                                    break;
//                                case 2:
//                                    verifiedimagelocation.add(imagelocation.get(1));
//                                    break;
//                                case 3:
//                                    verifiedimagelocation.add(imagelocation.get(2));
//                                    break;
//
//                            }

                            //adding detected image location in an array to get its url from s3
                            facedetectedimagelocation.add(imagelocation.get(count_detect_no));

                            List<Face> faces;

                            UUID mFaceIdother;
                            assert result != null;
                            faces = Arrays.asList(result);
                            for (Face face : faces) {
                                mFaceIdother = face.faceId;
                                String faceid = mFaceIdother.toString();
                                // txt.setText(faceid);


                                //adding detected face uuid in an array

                                detectedfaceid.add(image_face_count,mFaceIdother);



                                UUID[] uuids = new UUID[]{mFaceIdother};

                                //    Log.e("mfaceid", String.valueOf(mFaceIdother));

                                mFaceIdother = UUID.fromString(faceid);
                                //      new VerificationTask(mFaceId1, mFaceIdother).execute();
                                //  Log.e("face id size", String.valueOf(detectedfaceid.size()));
//                                if(detectedfaceid.size()==3){
//

                                //
                                String mfaceid = prefManager.getprofilefaceid();

                                UUID mfaceid1 = UUID.fromString(mfaceid);
                                new VerificationTask(mfaceid1, detectedfaceid.get(image_face_count)).execute();

                                Log.e("mfaceid1 and two", ""+mFaceId1+","+detectedfaceid.get(image_face_count));
//

//
//                                }


                            }


                        }

                        // i++;
//                        if(i<piccapturecount){
//                            detectAndFrameother1(autotakenpic.get(i));
//
//
//                        }
//                        else{
//                            new ImageSend().execute();
//                        }

                    }

                };

        detectTask.execute(inputStream);
    }
    private void verify() {

        Log.e("detectis count",String.valueOf(count_detect_no));
        // detecting the captured images
        if(count_detect_no<piccapturecount) {
            detectAndFrameother1(autotakenpic.get(count_detect_no));
        }

        if(count_detect_no==piccapturecount){
            //send data to server
            for(int i=0;i<imgurllist.size();i++){
                Log.e("outsideurl",i+" "+imgurllist.get(i));
            }
            try{
                verification_progress.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
            // new PostAns().execute();

            if(imgurllist.size()==0){
                new PostAns().execute();
//                Intent it = new Intent(this, CheckP.class);
//                it.putExtra("qt", questions.length() + "");
//                it.putExtra("cq", cqa + "");
//                it.putExtra("po", score + "");
//                it.putExtra("assessg",assessg);
//                it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(it);
//                finish();

            }else {
                new ImageSend().execute();
            }

        }




    }
    public class PhotoHandler implements Camera.PictureCallback {


        private final Context context;

        public PhotoHandler(Context context) {
            this.context = context;
        }

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            piccapturecount++;
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException e) {
                e.printStackTrace();
            }

            String authorities = getApplicationContext().getPackageName() + ".fileprovider";



            try {
                assert photoFile != null;
                FileOutputStream fos = new FileOutputStream(photoFile);
                fos.write(data);
                fos.flush();
                fos.close();
                fos.close();
                //   Toast.makeText(context, "New Image saved:" + photoFile,
                //       Toast.LENGTH_LONG).show();
            } catch (Exception error) {

                //   Toast.makeText(context, "Image could not be saved.",
                //         Toast.LENGTH_LONG).show();
            }

            //String filepath=  pictureFile.getPath();

            bmpother = BitmapFactory.decodeFile(mImageFileLocation);
            imageUri= FileProvider.getUriForFile(this.context, authorities, photoFile);


            // imageView.setImageBitmap(bitmap);
            //   Bitmap b = decodeFile(pictureFile);
            // Uri mImageUri = Uri.fromFile(pictureFile);




            Matrix mMatrix = new Matrix();

            mMatrix.setRotate(-90);
            bmpother = Bitmap.createBitmap(bmpother, 0, 0, bmpother.getWidth(),
                    bmpother.getHeight(), mMatrix, false);


            // saving location of auto capture image in an array
            imagelocation.add(mImageFileLocation);

            //saving auto capture bitmap image in an array
            autotakenpic.add(bmpother);

//
//            Toast.makeText(context, "Image saved."+imagelocation.size(),
//                    Toast.LENGTH_LONG).show();

            // detectAndFrameother(bmpother);

        }


        public  File createImageFile() throws IOException {

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "IMAGE_" + timeStamp + "_";
            File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
            mImageFileLocation = image.getAbsolutePath();

            return image;

        }

    }
    private class VerificationTask extends AsyncTask<Void, String, VerifyResult> {
        // The IDs of two face to verify.
        private UUID mFaceId0;
        private UUID mFaceId1;

        VerificationTask (UUID faceId0, UUID faceId1) {
            mFaceId0 = faceId0;
            mFaceId1 = faceId1;
        }

        @Override
        protected VerifyResult doInBackground(Void... params) {
            // Get an instance of face service client to detect faces in image.
            try{
                publishProgress("Verifying...");

                // Start verification.
                return faceServiceClient.verify(
                        mFaceId0,      /* The first face ID to verify */
                        mFaceId1);     /* The second face ID to verify */
            }  catch (Exception e) {
                publishProgress(e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(VerifyResult result) {

            image_face_count++;
            Log.e("result", ""+result);
            // Show the result on screen when verification is done.
            setUiAfterVerification(result);
        }
    }
    //detection
    private void setUiAfterVerification(VerifyResult result) {
        // Verification is done, hide the progress dialog.

        // Enable all the buttons.

        // Show verification result.

        if (result != null) {
            //   Log.e("facedetection","Result");
            DecimalFormat formatter = new DecimalFormat("0.0%");
            String verificationResult = (result.isIdentical ? "The same person": "Different persons")
                    + ". The confidence is " + formatter.format(result.confidence);
            //  Log.e("facedetection",verificationResult);


            if (verificationResult.contains("Different")){

                //    if(i==1)

                // adding mismatch image location in an array
                autocapimgloc.add(auto_capt_count,facedetectedimagelocation.get(count_detect_no));

                Log.e("added",facedetectedimagelocation.get(count_detect_no));

                //getting image url from s3
                new S3getimageurl().execute();

                Log.e("not outside s3",String.valueOf(count_detect_no));

            }else {
                Log.e("outside s3",String.valueOf(count_detect_no));
                count_detect_no++;

                verify();

            }

        }
    }
    private void detectAndFrame(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());


        @SuppressLint("StaticFieldLeak") AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks 4
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null) {
                                publishProgress("Detection Finished. Nothing detected");

                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");

                            return null;
                        }
                    }

                    @Override
                    protected void onPreExecute() {

                    }

                    @Override
                    protected void onProgressUpdate(String... progress) {

                    }

                    @Override
                    protected void onPostExecute(Face[] result) {



                        // if (result == null)
                        // Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();

                        if (result != null && result.length == 0) {
                            //     Toast.makeText(getApplicationContext(), "No face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","No face detected");

                        }else {
                            //    Toast.makeText(getApplicationContext(), "face detected!", Toast.LENGTH_LONG).show();
                            Log.e("facedetection","face detected");
                            //      startVerification();
                        }


                        // imageView.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                        //   Toast.makeText(getApplicationContext(), "Face detected!", Toast.LENGTH_LONG).show();
                        //imageBitmap.recycle();


                        List<Face> faces;

                        assert result != null;
                        faces = Arrays.asList(result);
                        for (Face face: faces) {
                            mFaceId1 = face.faceId;
                            faceid=mFaceId1.toString();
                            //txt.setText(faceid);
                            Log.e("fid",mFaceId1+"  "+mface);

                        }

                    }
                };
        detectTask.execute(inputStream);
    }
    private void checkfacedetection() {
        if(prefManager.getPic()==" "){
            String profileImageUrl = new DataBaseHelper(this).getDataFromTable(DataBaseHelper.TABLE_PROFILE, DataBaseHelper.KEY_PROFILE_IMAGE_URL);
            // new DownloadImage().execute("http://admin.getvalyou.com/api/candidateMobileReadImage/"+profileImageUrl);
            Log.e("facedetection","download pic");
        }else {
            faceServiceClient =
                    new FaceServiceRestClient(AppConstant.FR_end_points, new PrefManager(MSQ.this).get_frkey());
            //b37d858b6b584786a74958443e6fcd2a

            byte[] decodedString = Base64.decode(prefManager.getPic(), Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Log.e("facedetection","alreday present");
            detectAndFrame(decodedByte);
        }
    }

    public void displayImage()
    {
        close.setEnabled(false);
        close.setClickable(false);
        //lr.setVisibility(View.GONE);
        frameimageim.setVisibility(View.VISIBLE);
        ImageView imgfullscreenis=(ImageView)findViewById(R.id.imageview_fullview);
        Bitmap bmp = BitmapFactory.decodeFile(path_image);
        imgfullscreenis.setImageBitmap(bmp);
        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imgfullscreenis);
        pAttacher.update();



    }


    public void startanim()
    {
        //  red planet animation
        red_planet_anim_1 = ObjectAnimator.ofFloat(red_planet, "translationX", 0f,700f);
        red_planet_anim_1.setDuration(130000);
        red_planet_anim_1.setInterpolator(new LinearInterpolator());
        red_planet_anim_1.setStartDelay(0);
        red_planet_anim_1.start();

        red_planet_anim_2 = ObjectAnimator.ofFloat(red_planet, "translationX", -700,0f);
        red_planet_anim_2.setDuration(130000);
        red_planet_anim_2.setInterpolator(new LinearInterpolator());
        red_planet_anim_2.setStartDelay(0);

        red_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        red_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                red_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        //  yellow planet animation
        yellow_planet_anim_1 = ObjectAnimator.ofFloat(yellow_planet, "translationX", 0f,1100f);
        yellow_planet_anim_1.setDuration(220000);
        yellow_planet_anim_1.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_1.setStartDelay(0);
        yellow_planet_anim_1.start();

        yellow_planet_anim_2 = ObjectAnimator.ofFloat(yellow_planet, "translationX", -200f,0f);
        yellow_planet_anim_2.setDuration(22000);
        yellow_planet_anim_2.setInterpolator(new LinearInterpolator());
        yellow_planet_anim_2.setStartDelay(0);

        yellow_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        yellow_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                yellow_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  purple planet animation
        purple_planet_anim_1 = ObjectAnimator.ofFloat(purple_planet, "translationX", 0f,100f);
        purple_planet_anim_1.setDuration(9500);
        purple_planet_anim_1.setInterpolator(new LinearInterpolator());
        purple_planet_anim_1.setStartDelay(0);
        purple_planet_anim_1.start();

        purple_planet_anim_2 = ObjectAnimator.ofFloat(purple_planet, "translationX", -1200f,0f);
        purple_planet_anim_2.setDuration(100000);
        purple_planet_anim_2.setInterpolator(new LinearInterpolator());
        purple_planet_anim_2.setStartDelay(0);


        purple_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        purple_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                purple_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        //  earth animation
        earth_anim_1 = ObjectAnimator.ofFloat(earth, "translationX", 0f,1150f);
        earth_anim_1.setDuration(90000);
        earth_anim_1.setInterpolator(new LinearInterpolator());
        earth_anim_1.setStartDelay(0);
        earth_anim_1.start();

        earth_anim_2 = ObjectAnimator.ofFloat(earth, "translationX", -400f,0f);
        earth_anim_2.setDuration(55000);
        earth_anim_2.setInterpolator(new LinearInterpolator());
        earth_anim_2.setStartDelay(0);

        earth_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        earth_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                earth_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        //  orange planet animation
        orange_planet_anim_1 = ObjectAnimator.ofFloat(orange_planet, "translationX", 0f,300f);
        orange_planet_anim_1.setDuration(56000);
        orange_planet_anim_1.setInterpolator(new LinearInterpolator());
        orange_planet_anim_1.setStartDelay(0);
        orange_planet_anim_1.start();

        orange_planet_anim_2 = ObjectAnimator.ofFloat(orange_planet, "translationX", -1000f,0f);
        orange_planet_anim_2.setDuration(75000);
        orange_planet_anim_2.setInterpolator(new LinearInterpolator());
        orange_planet_anim_2.setStartDelay(0);

        orange_planet_anim_1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        orange_planet_anim_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                orange_planet_anim_1.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }

    public void disable_option(){
        para_scroller.setVisibility(View.INVISIBLE);
        rc_options_non_clickable.setVisibility(View.INVISIBLE);
        timer_ques.setVisibility(View.INVISIBLE);
        qno.setVisibility(View.INVISIBLE);



    }

    public static void changeSize(){
        if(flag_sizechanged.equalsIgnoreCase("para")){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                    para_frame.getLayoutParams();
            params.weight = 64.0f;
            para_frame.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)
                    qno_layout.getLayoutParams();
            params1.weight = 6.0f;
            qno_layout.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams)
                    below_layout.getLayoutParams();
            params2.weight = 30.0f;
            below_layout.setLayoutParams(params2);
           /* LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 0.75f;

            below_layout.setLayoutParams(p);
            LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p1.weight = 1.5f;

            para_frame.setLayoutParams(p1);*/
            /*LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.06f;
            qno_layout.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.64f;
            para_frame.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.3f;
            below_layout.setLayoutParams(params2);*/
        }else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                    para_frame.getLayoutParams();
            params.weight = 30.0f;
            para_frame.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)
                    qno_layout.getLayoutParams();
            params1.weight = 6.0f;
            qno_layout.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams)
                    below_layout.getLayoutParams();
            params2.weight = 64.0f;
            below_layout.setLayoutParams(params2);
            /*
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.06f;
            qno_layout.setLayoutParams(params);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.64f;
            below_layout.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) weightedlinear.getLayoutParams();
            params.weight = 0.3f;
            para_frame.setLayoutParams(params2);*/
        }
    }

}

