package com.globalgyan.getvalyou.MasterSlaveGame;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.globalgyan.getvalyou.R;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Globalgyan on 10-03-2018.
 */

public class OPtionNonclickableAdapterTemp extends RecyclerView.Adapter<OPtionNonclickableAdapterTemp.MyViewHolder> {

    private Context mContext;
    private static ArrayList<String> options_array;
    Typeface typeface1;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextView option;


        public MyViewHolder(View view) {
            super(view);

            option = (FontTextView) view.findViewById(R.id.option_is_non_clikable);

        }
    }


    public OPtionNonclickableAdapterTemp(Context mContext, ArrayList<String> options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.options_item_layout_non_clikable, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if(position==0){
            typeface1 = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Regular.ttf");
            Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Regular.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(options_array.get(position));
            SS.setSpan(new CustomTypefaceSpan("", typeface1), 0, 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", tf2), 12, options_array.get(position).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new RelativeSizeSpan(1.3f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            // txt.setText(SS);
            holder.option.setText(SS);
        }else {
            typeface1 = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Regular.ttf");
            Typeface tf2 = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Regular.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(options_array.get(position));
            SS.setSpan(new CustomTypefaceSpan("", typeface1), 0, 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", tf2), 2, options_array.get(position).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
          //  SS.setSpan(new RelativeSizeSpan(1.4f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            // txt.setText(SS);
            holder.option.setText(SS);
        }
    }



    @Override
    public int getItemCount() {
        return options_array.size();
    }
}





