package com.globalgyan.getvalyou.MasterSlaveGame;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.globalgyan.getvalyou.R;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import static com.thefinestartist.utils.content.ContextUtil.sendBroadcast;

/**
 * Created by Globalgyan on 09-03-2018.
 */

public class NewOptionsAdapter extends RecyclerView.Adapter<NewOptionsAdapter.MyViewHolder> {

    public static int pos_click=0;
    private Context mContext;
    private static String[] options_array;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView option;


        public MyViewHolder(View view) {
            super(view);

            option = (TextView) view.findViewById(R.id.option_is);

        }
    }


    public NewOptionsAdapter(Context mContext, String[] options_array) {
        this.mContext = mContext;
        this.options_array = options_array;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_msq_options, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

      /*  circularProgressBar.setColor(ContextCompat.getColor(this, R.color.progressBarColor));
        circularProgressBar.setBackgroundColor(ContextCompat.getColor(this, R.color.backgroundProgressBarColor));
        circularProgressBar.setProgressBarWidth(getResources().getDimension(R.dimen.progressBarWidth));
        circularProgressBar.setBackgroundProgressBarWidth(getResources().getDimension(R.dimen.backgroundProgressBarWidth));
        int animationDuration = 2500; // 2500ms = 2,5s
        circularProgressBar.setProgressWithAnimation(65, animationDuration);*/
        Typeface typeface1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Gotham-Medium.otf");
        holder.option.setTypeface(typeface1,Typeface.BOLD);
        holder.option.setText(options_array[position]);

        holder.option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.option.setEnabled(false);
                holder.option.setClickable(false);
                MSQ.rc_options_non_clickable.addOnItemTouchListener(MSQ.disabler);
                new CountDownTimer(1500, 1500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        MSQ.rc_options_non_clickable.removeOnItemTouchListener(MSQ.disabler);
                        holder.option.setEnabled(true);
                        holder.option.setClickable(true);
                    }
                }.start();
                Log.e("correctans",String.valueOf(MSQ.crct));
                if(MSQ.crct==position){


                }else {

                }
                Intent brodcast_receiver_screen=new Intent("Show_screen");
                brodcast_receiver_screen.putExtra("pos", String.valueOf(position));
                mContext.sendBroadcast(brodcast_receiver_screen);


            }
        });


    }
    @Override
    public int getItemCount() {
        return options_array.length;
    }
}




