package com.globalgyan.getvalyou;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;


 import de.morrox.fontinator.FontTextView;

import static com.globalgyan.getvalyou.Welcome.intro2_desc1_string;
import static com.globalgyan.getvalyou.Welcome.intro2_desc2_string;
import static com.globalgyan.getvalyou.Welcome.intro2_desc3_string;
import static com.globalgyan.getvalyou.Welcome.intro2_desc4_string;
import static com.globalgyan.getvalyou.Welcome.intro2_title1_string;
import static com.globalgyan.getvalyou.Welcome.intro3_desc1_string;
import static com.globalgyan.getvalyou.Welcome.intro3_desc2_string;
import static com.globalgyan.getvalyou.Welcome.intro3_desc3_string;
import static com.globalgyan.getvalyou.Welcome.intro3_desc4_string;
import static com.globalgyan.getvalyou.Welcome.intro3_title1_string;
import static com.globalgyan.getvalyou.Welcome.tf2;

public class IntroThree extends Fragment {
    View v;
    Spannable  colored_str_in_intro3;
    static ObjectAnimator astro_outx,astro_outy;
   static ImageView astro;

   static boolean flag_anim_done3 = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.intro_three, container, false);

        setRetainInstance(true);



        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String intro3_title1_string = prefs.getString("colored_str_in_intro3", "colored_str_in_intro3");
        String intro3_desc1_string = prefs.getString("intro3_desc1_string", "intro3_desc1_string");
        String intro3_desc2_string = prefs.getString("intro3_desc2_string", "intro3_desc2_string");
        String intro3_desc3_string = prefs.getString("intro3_desc3_string", "intro3_desc3_string");
        String intro3_desc4_string = prefs.getString("intro3_desc4_string", "intro3_desc4_string");


            colored_str_in_intro3 = new SpannableString(intro3_title1_string);
            String firstWord1 = intro3_title1_string.substring(0, intro3_title1_string.lastIndexOf(" "));
            colored_str_in_intro3.setSpan(new ForegroundColorSpan(Color.parseColor("#d200ff")), firstWord1.length(), intro3_title1_string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);



        FontTextView tv = (FontTextView) v.findViewById(R.id.title_text_intro_page3);
        tv.setText(colored_str_in_intro3);
        FontTextView tv1 = (FontTextView) v.findViewById(R.id.intropage3_text1);
        tv1.setText(intro3_desc1_string);
        FontTextView tv2 = (FontTextView) v.findViewById(R.id.intropage3_text2);
        tv2.setText(intro3_desc2_string);
        FontTextView tv3 = (FontTextView) v.findViewById(R.id.intropage3_text3);
        tv3.setText(intro3_desc3_string);
        FontTextView tv4 = (FontTextView) v.findViewById(R.id.intropage3_text4);
        tv4.setText(intro3_desc4_string);

        tv.setTypeface(tf2);
        tv1.setTypeface(tf2);
        tv2.setTypeface(tf2);
        tv3.setTypeface(tf2);
        tv4.setTypeface(tf2);



         astro = (ImageView)v.findViewById(R.id.rocket_intro3);

         astro.setVisibility(View.INVISIBLE);
        astro_outx = ObjectAnimator.ofFloat(astro, "translationX", -1500f,0f);
        astro_outx.setDuration(300);
        astro_outx.setInterpolator(new LinearInterpolator());
        astro_outx.setStartDelay(0);
      //  astro1_outx.start();
        astro_outy = ObjectAnimator.ofFloat(astro, "translationY", 1100f,0f);
        astro_outy.setDuration(300);
        astro_outy.setInterpolator(new LinearInterpolator());
        astro_outy.setStartDelay(0);
       // astro1_outy.start();



    }
    public static IntroThree newInstance(String text) {

        IntroThree f = new IntroThree();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

}